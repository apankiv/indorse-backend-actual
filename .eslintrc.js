module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "indent": ["error", 4],
        "no-underscore-dangle": "off",
        "camelcase": "off",
        "no-return-await": "off",
        "max-len": "off",
        "no-use-before-define": "off",
    },
    "env": {
        "node": true,
        "mocha": true
    }
};
