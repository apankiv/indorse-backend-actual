# Indorse Backend Actual

## Required Services

### Ganache

Quickly fire up a personal Ethereum blockchain which you can use to run tests, execute commands, and inspect state while controlling how the chain operates.

Link: [Ganache](https://truffleframework.com/ganache)

### MongoDB

MongoDB is a document database with the scalability and flexibility that you want with the querying and indexing that you need.

Link: [MongoDB](https://www.mongodb.com/what-is-mongodb)

### RabbitMQ

RabbitMQ is an open source message broker software (sometimes called message-oriented middleware) that originally implemented the Advanced Message Queuing Protocol (AMQP) and has since been extended with a plug-in architecture to support Streaming Text Oriented Messaging Protocol (STOMP), Message Queuing Telemetry Transport (MQTT), and other protocols.

Link: [RabbitMQ](https://www.rabbitmq.com/)

To run these services on your local:

```closure
cd docker-compose
docker-compose up
```

It will make the above mentioned service available on your local.

## Setup

- Get the config file
- Add it as development.yml (don't use local.yml as it will overide the configuration even for tests and might result in db being dropped)
- Install the dependencies. switch to root directory of the project

```closure
npm install
```

- Start the backend

```closure
npm run dev
```

## Continuos Deployement and Integration

Look for .circleci folder in the root of the project. There is configuration of that.
