const messages = {
    USER_NOT_FOUND_MESSAGE: 'Unable to find the user',
};

module.exports = messages;
