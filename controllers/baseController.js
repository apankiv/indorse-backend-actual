class BaseController {
    static sendSuccessResponse(res, data = null, statusCode = 200) {
        const responseBody = { success: true };
        if (data) {
            responseBody.profile = data;
        }
        return res
            .status(statusCode)
            .send(responseBody);
    }

    static sendFailureResponse(res, errors = null, statusCode = 400) {
        const responseBody = { success: false };
        if (errors) {
            responseBody.errors = errors;
        }
        return res
            .status(statusCode)
            .send(responseBody);
    }
}

module.exports = BaseController;
