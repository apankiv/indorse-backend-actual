const { ObjectId } = require('mongoose').Types;
const BaseController = require('../baseController');
const ProfileModel = require('../../models/users/profile/profile');
const safeObjects = require('../../models/services/common/safeObjects');
const { Criteria, FIELD_TYPE } = require('../../helpers/criteria');
const errorMessage = require('../../constants/errors');
const errorUtils = require('../../models/services/error/errorUtils');

class Profile extends BaseController {
    static async get(req, res) {
        const sfReqObj = safeObjects.safeReqestBodyParser(req, Profile.getRequestFieldList());
        const criteria = new Criteria();
        if (sfReqObj.username) {
            criteria.addField('username', sfReqObj.username, FIELD_TYPE.REGEX);
        } else if (Profile.isMongoId(sfReqObj.user_id)) {
            criteria.addField('_id', new ObjectId(sfReqObj.user_id), FIELD_TYPE.MONGO_ID);
        } else if (!sfReqObj.user_id && (req.email || sfReqObj.email)) {
            criteria.addField('email', req.email || sfReqObj.email, FIELD_TYPE.EXACT);
        }
        if (!criteria.isHavingAnyField()) {
            return errorUtils.throwError(errorMessage.USER_NOT_FOUND_MESSAGE, 404);
        }
        const user = await ProfileModel.get(criteria.get());
        if (!user) {
            return errorUtils.throwError(errorMessage.USER_NOT_FOUND_MESSAGE, 404);
        }
        return Profile.sendSuccessResponse(res, user);
    }

    static getRequestFieldList() {
        return ['email', 'user_id', 'username'];
    }

    /**
     * Legacy Code
     * Keeping it as it is
     * @param id probable mongo id
     * @returns {boolean}
     */
    static isMongoId(id) {
        if (!id) {
            return false;
        }
        if (id.length === 12 || id.length === 24) {
            return true;
        }
        return false;
    }
}

module.exports = Profile;
