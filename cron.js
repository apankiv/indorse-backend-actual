const config = require('config');
const path = require('path');
require('longjohn');
const logger = require('./models/services/common/logger').getLogger();
const errorUtils = require('./models/services/error/errorUtils');
const transactionProcessor = require('./smart-contracts/services/cron/resolverLoop');
const checkClaimsJob = require('./smart-contracts/services/cron/checkClaimsJob');
const votingReminderJob = require('./smart-contracts/services/cron/votingReminderJob');
const gatherGeneralStatistics = require('./smart-contracts/services/cron/gatherGenerlStatistics');
const computeValidatorStats = require('./smart-contracts/services/cron/computeValidatorStats');
const claimReleaserJob = require('./models/services/crons/claimReleaserJob');
const resubmissionReminderJob = require('./models/services/crons/resubmissionReminderJob');
const eventUtils = require("./models/services/common/eventUtils");
const merkleUtils = require("./models/services/common/merkleUtils");
const assignmentReminderJob = require('./models/services/crons/assignmentReminderJob');
const magicLinkAssessmentReminderJob = require('./models/services/crons/magicLinkAssessmentReminderJob');
const sortValidatorRank = require('./smart-contracts/services/cron/sortValidators');
const updateValidatorDownvoteScore = require('./smart-contracts/services/cron/updateValidatorDownvoteScore');

global.appRoot = path.resolve(__dirname);

let send_tick = config.get('cron.send_tick');
let process_tick = config.get('cron.process_tick');
let check_claims_tick = config.get('cron.check_claims_tick');
let check_vote_tx_tick = config.get('cron.check_vote_tx_tick');
let generate_reputation_rewards_tick = config.get('cron.generate_reputation_rewards_tick');
const assignment_reminder_tick = config.get('cron.assignment_reminder_tick');
const assignment_reminder_enabled = config.get('cron.assignment_reminder_enabled');
const magic_link_assessment_reminder_tick = config.get('cron.magic_link_assessment_reminder_tick');
const magic_link_assessment_reminder_enabled = config.get('cron.magic_link_assessment_reminder_enabled');
const queue_release_tick = config.get('cron.queue_release_tick');
const update_downvote_tick = config.get('cron.update_downvote_tick');
// let push_merkle_root_tick = config.get('cron.push_merkle_root_tick');
let push_merkle_root_tick = config.get('cron.push_merkle_root_tick');

if (!send_tick || !process_tick) {
    errorUtils.throwError("Unable to start cron job , configuration missing for send_tick or process_tick")
}

if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'integration') {
    const CronJob = require('cron').CronJob;
    new CronJob(send_tick, async () => await transactionProcessor.sendTransactions(), null, true, 'GMT');
    new CronJob(process_tick, async () => await transactionProcessor.processTransactions(), null, true, 'GMT');
    new CronJob(check_claims_tick, async () => await checkClaimsJob.processOutstandingClaims(), null, true, 'GMT');
    new CronJob(check_vote_tx_tick, async () => await computeValidatorStats.calculate(), null, true, 'GMT');
    new CronJob("0 0 0 * * *", async () => await sortValidatorRank.calculateAndSort(), null, true, 'GMT');
    new CronJob(check_vote_tx_tick, async () => await votingReminderJob.sendReminderEmails(), null, true, 'GMT');
    new CronJob("0 0 0 * * *", async () => await gatherGeneralStatistics.reportStats(), null, true, 'Asia/Singapore');
    new CronJob(generate_reputation_rewards_tick, async () => await eventUtils.geneateReputationRewards(), null, true, 'GMT');
    new CronJob(queue_release_tick, async () => await claimReleaserJob.releaseClaims(), null, true, 'GMT');
    new CronJob(update_downvote_tick, async () => await updateValidatorDownvoteScore.updateValidatorDownvoteScore(), null, true, 'GMT');
    // new CronJob(push_merkle_root_tick, async () => await merkleUtils.pushMerkleRoot(new Date()), null, true, 'GMT');
    new CronJob(push_merkle_root_tick, async () => await merkleUtils.pushMerkleRoot(new Date()), null, true, 'GMT');
    console.log("assignment_reminder_enabled: ", assignment_reminder_enabled);
    if (assignment_reminder_enabled === 'true' || assignment_reminder_enabled === true) {
        logger.debug("[sendAssignmentReminders] scheduled at" + assignment_reminder_tick);
        new CronJob(assignment_reminder_tick, async () => await assignmentReminderJob.sendAssignmentReminders(), null, true, 'GMT');
    }
    console.log("magic_link_assessment_reminder_enabled: ", magic_link_assessment_reminder_enabled);
    if (magic_link_assessment_reminder_enabled === 'true' || magic_link_assessment_reminder_enabled === true) {
        logger.debug("[sendMagicLinkReminders] scheduled at" + magic_link_assessment_reminder_tick);
        new CronJob(magic_link_assessment_reminder_tick, async () => await magicLinkAssessmentReminderJob.sendMagicLinkAssessmentReminders(), null, true, 'GMT');
    }
    new CronJob(assignment_reminder_tick, async () => await resubmissionReminderJob.sendResubmissionReminders(), null, true, 'GMT');
}

console.log('started cron job successfully');
