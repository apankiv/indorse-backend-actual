describe('Testing creation of partner claims', function () {
    it('Should create and approve partner claims', function () {
        //Post a claim
        let body = {
            "skillPrettyId": "Javascript",
            "description": "KK",
            "proof": "https://github.com",
            "partnerUserId": "12"
        }

        let createClaimUrl = Cypress.env("target_be") + "/" + "create-partner-claim"

        let req = {
            method: 'POST',
            url: createClaimUrl,
            body : body,
            headers: Cypress.env("client_app_headers")
        }        

        cy.request(req).then((response) => {
            cy.loginAdmin();
            cy.log(response.body.data.claimId)
            let claimId = response.body.data.claimId    
            //click Admin        
            cy.get('.index-module__menu___3zANZ > .my-auto > span').click()
            //click claims
            cy.get('[href="/admin/claims"] > span').click()
            //type claim id in search
            cy.get('#admin-claims-filters-form-search').type(claimId)
            //click filter 
            cy.get('.mt-3 > .is-ready > .d-flex').click()
            //click approve on the first child returned
            cy.get('.list-inline > :nth-child(1) > div > .btn').first().click()  
            //click in center as tier 1 claim
            cy.get(':nth-child(2) > .form-group > .rc-slider').click(0,0)
            //click submit
            cy.get('.justify-content-end > .btn > .d-flex').click()
            //check if notificaiton contains successful
            cy.get('.notification-text').contains("Succesful")            
            //wait 90 seconds for claims to be released
            cy.wait(90000);
            //visit claim url and check if it contains voting in progress
            let createdClaimUrl = Cypress.env("target_fe") + "/claims/" + claimId
            cy.visit(createdClaimUrl);
            cy.contains('Voting in Progress')            
        })    
    })
})
