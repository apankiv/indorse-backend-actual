// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})


Cypress.Commands.add('loginAdmin',()=>{
    cy.visit(Cypress.env("target_fe"))
    cy.contains('Login').click()
    cy.get('#login-email').type(Cypress.env("admin_email"))
    cy.get("#login-password").type(Cypress.env("admin_pwd"))
    cy.get('.mt-3 > .btn-primary > .d-flex').click()
    cy.contains('Admin')
})