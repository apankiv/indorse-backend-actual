#!/bin/bash

set -e

ECS_REGION="ap-southeast-1"
NAMESPACE="indorse"
IMAGE_BASE="indorse"
IMAGE_NAME="backend"
ECR_URI="${AWS_ACCOUNT_ID}.dkr.ecr.${ECS_REGION}.amazonaws.com"
SHORT_GIT_HASH=$(echo $CIRCLE_SHA1 | cut -c -7)
TAG=$CIRCLE_BRANCH


# helpers

configure_aws_cli() {
  echo "Configuring AWS..."
  aws --version
  aws configure set default.region $ECS_REGION
  aws configure set default.output json
  echo "AWS configured!"
}

tag_and_push_images() {
  echo "Tagging and pushing images..."
  $(aws ecr get-login --no-include-email)
  echo "Tagging..."
  docker tag ${IMAGE_BASE}_${IMAGE_NAME} ${ECR_URI}/${NAMESPACE}/${IMAGE_NAME}:${TAG}
  echo "Pushing..."
  docker push ${ECR_URI}/${NAMESPACE}/${IMAGE_NAME}:${TAG}
  echo "Images tagged and pushed!"
}

# main

configure_aws_cli
tag_and_push_images