/**
 * Graphql module Generator
 */
const camelCase = require('camelcase');

const moduleExists = require('../utils/moduleExists');
const isTestExists = require('../utils/testExists');

module.exports = {
  description: 'Add new graphql module with one new query and/or one new mutation and their test cases.',
  prompts: [
    {
      type: 'input',
      name: 'moduleName',
      message: 'What is the name of new module?',
      validate: (value, data) => {
        if (/.+/.test(value)) {
          return moduleExists(value)
            ? 'A module with this name already exists'
            : true;
        }
        return 'The name is required';
      },
    },
    {
      type: 'input',
      name: 'queryName',
      message: 'What is the name of first query? Type `null` if you do not want a query',
    },
    {
      type: 'input',
      name: 'mutationName',
      message: 'What is the name of first mutation? Type `null` if you do not want a mutation',
    }
  ],
  actions: data => {
    const actions = [];
    const { moduleName, queryName, mutationName } = data;

    const shouldQueryCreated = (queryName.toString().toLowerCase() !== "");
    const shouldMutationCreated = (mutationName.toString().toLowerCase() !== "");

    const shouldQueryTestCreated = !(isTestExists(moduleName, queryName)); // query test should be created if test exist is not true.
    const shouldMutationTestCreated = !(isTestExists(moduleName, mutationName));

    /* CREATE new graphql/resolvers/<moduleName>/<queryName>.js */
    if (shouldQueryCreated) {
      actions.push({
        type: 'add',
        path: `../graphql/resolvers/{{camelCase moduleName}}/{{camelCase queryName}}.js`,
        templateFile: './new-module/queries-firstQuery.hbs',
        abortOnFail: true,
      });
      if(shouldQueryTestCreated){
          actions.push({
              type: 'add',
              path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase queryName}}.spec.js`,
              templateFile: './new-module/tests-firstQueryTest.hbs',
              abortOnFail: true,
          });
      }
    }

    /* CREATE new graphql/resolvers/<moduleName>/<mutationName>.js */
    if (shouldMutationCreated) {
      actions.push({
        type: 'add',
        path: `../graphql/resolvers/{{camelCase moduleName}}/{{camelCase mutationName}}.js`,
        templateFile: './new-module/mutations-firstMutation.hbs',
        abortOnFail: true,
      });
      if(shouldMutationTestCreated){
          actions.push({
              type: 'add',
              path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase mutationName}}.spec.js`,
              templateFile: './new-module/tests-firstMutationTest.hbs',
              abortOnFail: true,
          });
      }
    }
    
    /* CREATE new <moduleName>/models.js */
    actions.push({
      type: 'add',
      path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
      templateFile: './new-module/models.hbs',
      abortOnFail: true,
    });

    /* MODIFY <moduleName>/models.js, insert first Query */
    if (shouldQueryCreated) {
      actions.push({
        type: 'modify',
        path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
        pattern: '# need_first_query (Do not remove this line)',
        templateFile: './new-module/need-first-query.hbs',
        abortOnFail: true,
      });
    }

    /* MODIFY <moduleName>/models.js, insert first mutation */
    if (shouldMutationCreated) {
      actions.push({
        type: 'modify',
        path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
        pattern: '# need_first_mutation (Do not remove this line)',
        templateFile: './new-module/need-first-mutation.hbs',
        abortOnFail: true,
      });
    }
    
    /* INSERT new query in global resolvers */
    if (shouldQueryCreated) {
      actions.push({
        type: 'modify',
        path: `../graphql/resolvers.js`,
        pattern: '// import_new_query/mutation/subscription (Do not modify/delete this line)',
        templateFile: './new-module/import-new-query.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'modify',
        path: `../graphql/resolvers.js`,
        pattern: '// add_new_query (Do not modify/delete this line)',
        templateFile: './new-module/add-new-query.hbs',
        abortOnFail: true,
      });
    }

    /* INSERT new mutation in global resolvers */
    if (shouldMutationCreated) {
      actions.push({
        type: 'modify',
        path: `../graphql/resolvers.js`,
        pattern: '// import_new_query/mutation/subscription (Do not modify/delete this line)',
        templateFile: './new-module/import-new-mutation.hbs',
        abortOnFail: true,
      });

      actions.push({
        type: 'modify',
        path: `../graphql/resolvers.js`,
        pattern: '// add_new_mutation (Do not modify/delete this line)',
        templateFile: './new-module/add-new-mutation.hbs',
        abortOnFail: true,
      });
    }

    /* INSERT new schema in graphql/models.js */
    actions.push({
      type: 'modify',
      path: `../graphql/models.js`,
      pattern: '// import-new-schema (Do not remove/modify this line)',
      templateFile: './new-module/import-new-schema.hbs',
      abortOnFail: true,
    });

    actions.push({
      type: 'modify',
      path: `../graphql/models.js`,
      pattern: '// add-new-schema (Do not remove/modify this line)',
      templateFile: './new-module/add-new-schema.hbs',
      abortOnFail: true,
    });

    return actions;
  },
};
