/**
 * Mutation generator
 */


const containerExists = require('../utils/containerExists');
const getAllParentContainers = require('../utils/getAllParentContainers');
const isTestExists = require('../utils/testExists');

module.exports = {
    description: 'Add new graphql mutation',
    prompts: [
        {
            type: 'list',
            name: 'moduleName',
            message: 'Select the module where you want to put the mutation',
            choices: () => getAllParentContainers(),
        },
        {
            type: 'input',
            name: 'mutationName',
            message: 'What should this mutation be called?',
            default: 'updateUser',
            validate: (value, data) => {
                if (/.+/.test(value)) {
                    return containerExists(data.moduleName, value)
                        ? 'A mutation with this name already exists in this module'
                        : true;
                }

                return 'The name is required';
            },
        }
    ],
    actions: data => {
        const actions = [];
        const { moduleName, mutationName } = data;

        const shouldMutationTestCreated = !(isTestExists(moduleName, mutationName));
        /*-- ADDING MUTATION_RESOLVER FILE --*/
        actions.push({
            type: 'add',
            path: `../graphql/resolvers/{{camelCase moduleName}}/{{camelCase mutationName}}.js`,
            templateFile: './new-mutation/mutation-resolver.hbs',
            abortOnFail: true,
        });

        if(shouldMutationTestCreated){
            /* Adding a test case  */
            actions.push({
                type: 'add',
                path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase mutationName}}.spec.js`,
                templateFile: './new-module/tests-firstMutationTest.hbs',
                abortOnFail: true,
            });
        }

        /* INSERT import statement @ global resolvers */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// import_new_query/mutation/subscription (Do not modify/delete this line)',
            templateFile: './new-mutation/import-new-mutation.hbs',
            abortOnFail: true,
        });

        /* INSERT key and value inside Mutation Object @ global resolvers */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// add_new_mutation (Do not modify/delete this line)',
            templateFile: './new-mutation/add-new-mutation.hbs',
            abortOnFail: true,
        });

        /* INSERT model definition @ <moduleName>/model.js */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
            pattern: '# need_first_mutation (Do not remove this line)',
            templateFile: './new-mutation/need-first-mutation.hbs',
            abortOnFail: true,
        });

        actions.push({
            type: 'modify',
            path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
            pattern: '# new_mutation (Do not remove this line)',
            templateFile: './new-mutation/new-mutation-in-model.hbs',
            abortOnFail: true,
        });

        return actions;
    },
};
