/**
 * Query Generator
 */

const containerExists = require('../utils/containerExists');
const getAllParentContainers = require('../utils/getAllParentContainers');
const isTestExists = require('../utils/testExists');

module.exports = {
    description: 'Add new graphql query',
    prompts: [
        {
            type: 'list',
            name: 'moduleName',
            message: 'Select the module where you want to put the query',
            choices: () => getAllParentContainers(),
        },
        {
            type: 'input',
            name: 'queryName',
            message: 'What should this query be called?',
            default: 'getUserById',
            validate: (value, data) => {
                if (/.+/.test(value)) {
                    return containerExists(data.moduleName, value)
                        ? 'A query with this name already exists in this module'
                        : true;
                }

                return 'The name is required';
            },
        }
    ],
    actions: data => {
        const actions = [];
        const { moduleName, queryName } = data;
        const shouldQueryTestCreated = !(isTestExists(moduleName, queryName));
        /*-- ADDING query_RESOLVER FILE --*/
        actions.push({
            type: 'add',
            path: `../graphql/resolvers/{{camelCase moduleName}}/{{camelCase queryName}}.js`,
            templateFile: './new-query/query-resolver.hbs',
            abortOnFail: true,
        });

        if(shouldQueryTestCreated){
            /*Adding a test case */
            actions.push({
                type: 'add',
                path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase queryName}}.spec.js`,
                templateFile: './new-module/tests-firstQueryTest.hbs',
                abortOnFail: true,
            });
        }



        /* INSERT import statement @ global resolvers */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// import_new_query/mutation/subscription (Do not modify/delete this line)',
            templateFile: './new-query/import-new-query.hbs',
            abortOnFail: true,
        });

        /* INSERT key and value inside query Object @ global resolvers */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// add_new_query (Do not modify/delete this line)',
            templateFile: './new-query/add-new-query.hbs',
            abortOnFail: true,
        });

        /* INSERT model definition @ <moduleName>/model.js */
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
            pattern: '# need_first_query (Do not remove this line)',
            templateFile: './new-query/need-first-query.hbs',
            abortOnFail: true,
        });

        actions.push({
            type: 'modify',
            path: `../graphql/resolvers/{{camelCase moduleName}}/models.js`,
            pattern: '# new_query (Do not remove this line)',
            templateFile: './new-query/new-query-in-model.hbs',
            abortOnFail: true,
        });

        return actions;
    },
};
