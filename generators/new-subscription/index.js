/**
 * Test Generator
 */
const getAllParentContainers = require('../utils/getAllParentContainers');
const getAllContainersWithoutTestcases = require('../utils/getAllResolversWithoutTests');
const fileNameExistsInDirectory = require('../utils/fileNameExistsInDirectory');

module.exports = {
    description: 'Add new graphql subscription',
    prompts: [
        {
            type: 'input',
            name: 'subscriptionName',
            message: 'What should this subscription be called?',
            validate: (value, data) => {
                if (/.+/.test(value)) {
                    return fileNameExistsInDirectory('subscriptions', value)
                        ? 'A subscription with this name already exists in this module'
                        : true;
                }
                return 'The name is required';
            },
        }
    ],
    actions: data => {
        const actions = [];

        // create a subscription file
        actions.push({
            type: 'add',
            path: `../graphQL/subscriptions/{{camelCase subscriptionName}}.js`,
            templateFile: './new-subscription/subscription-resolver.hbs',
            abortOnFail: true,
        });


        // Import it from resolvers.js file in the graphQL root folder
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// import_new_query/mutation/subscription (Do not modify/delete this line)',
            templateFile: './new-subscription/import-new-subscription.hbs',
            abortOnFail: true,
        });
        actions.push({
            type: 'modify',
            path: `../graphql/resolvers.js`,
            pattern: '// add_new_subscription (Do not modify/delete this line)',
            templateFile: './new-subscription/add-new-subscription.hbs',
            abortOnFail: true,
        });

        // Define it in models.js in the subscription folder
        actions.push({
            type: 'modify',
            path: `../graphql/subscriptions/models.js`,
            pattern: '# new_subscription (Do not remove this line)',
            templateFile: './new-subscription/new-subscription-in-model.hbs',
            abortOnFail: true,
        });


        return actions;
    },
};



