/**
 * Test Generator
 */
const getAllParentContainers = require('../utils/getAllParentContainers');
const getAllContainersWithoutTestcases = require('../utils/getAllResolversWithoutTests');


module.exports = {
    description: 'Add new graphql test template corresponding to resolver',
    prompts: [
        {
            type: 'list',
            name: 'moduleName',
            message: 'Select the module',
            choices: () => getAllParentContainers(), // show all the
        },
        {
            type: 'list',
            name: 'resolverName',
            message: 'Select the resolver where you want generate test template',
            choices: (value) => {
                return getAllContainersWithoutTestcases(value.moduleName);
            },

        },
        {
            type: 'list',
            name: 'resolverType',
            message: 'Is it query or mutation?',
            choices: () => {
                return ['query','mutation'];
            },

        }
    ],
    actions: data => {
        const actions = [];
        const {resolverType} = data;

        /*Adding a test case */
        if(resolverType === 'query'){
            actions.push({
                type: 'add',
                path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase resolverName}}.spec.js`,
                templateFile: './new-module/tests-firstQueryTest.hbs',
                abortOnFail: true,
            });
        }
        if(resolverType === 'mutation'){
            actions.push({
                type: 'add',
                path: `../test/graphQL/{{camelCase moduleName}}/{{camelCase resolverName}}.spec.js`,
                templateFile: './new-module/tests-firstMutationTest.hbs',
                abortOnFail: true,
            });
        }


        return actions;
    },
};



