/*
    Input:
    @ directoryPath(String)
    @ fileName (String)
    @ extensionName after .   eg) .txt -> should pass "txt"

    Output:
    Boolean

    Given folder directory, find if there's any file with given fileName.

    const curPath = path.join(__dirname, '../../test/graphQL/'+moduleName+'/'+camelCase(resolverName)+'.spec.js')
    return fs.existsSync(curPath)
 */

const fs = require('fs');
const path = require('path');
const camelCase = require('camelcase');


function fileNameExistsInDirectory(directoryPath, fileName, extensionName ='js') {
    const curPath = path.join(__dirname, '../../graphql/'+directoryPath+'/'+camelCase(fileName)+'.'+extensionName);
    return fs.existsSync(curPath);
}

module.exports = fileNameExistsInDirectory;
