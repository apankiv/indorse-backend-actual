/**
 * getAllParentContainers
 *
 * Check whether the given component exist in either the components or containers directory
 */

const fs = require('fs');
const path = require('path');

function getAllParentContainers() {
  const arr = fs
    .readdirSync(path.join(__dirname, '../../graphql/resolvers'))
    .filter(folder => ['.DS_Store', 'common', 'pubsub.js', 'resolvers.js', 'schema.js', 'mongoose-compose','users','companies','rootModel.js'].indexOf(folder) < 0);

  return arr;
}

module.exports = getAllParentContainers;
