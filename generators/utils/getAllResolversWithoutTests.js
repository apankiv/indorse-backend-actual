/**
 * getAllContainers
 *
 * Given a moduleName, check if there's any resolvers without test cases.
 */

const fs = require('fs');
const path = require('path');
const isTestExists = require('./testExists');
const stringUtils = require('../../models/services/blob/stringUtils');

function getAllResolversWithoutTests(moduleName) {
  const arr = fs
    .readdirSync(path.join(__dirname, `../../graphql/resolvers/${moduleName}`))
    .filter(folder => ['.DS_Store', 'models.js'].indexOf(folder) < 0)
      .filter(file => {
        return !isTestExists(moduleName, stringUtils.removeExtension(file));
      })

    // remove the js extension
    var toReturn = [];
    arr.forEach((fileName)  => {
        toReturn.push(stringUtils.removeExtension(fileName));
    });

  return toReturn;
}


module.exports = getAllResolversWithoutTests;
