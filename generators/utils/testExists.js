/**
 * testExists
 *
 * Check whether the given graphql module exist
 */

const fs = require('fs');
const path = require('path');
const camelCase = require('camelcase');


function testCaseExists(moduleName, resolverName) {
    const curPath = path.join(__dirname, '../../test/graphQL/'+camelCase(moduleName)+'/'+camelCase(resolverName)+'.spec.js')
    return fs.existsSync(curPath)
}

module.exports = testCaseExists;
