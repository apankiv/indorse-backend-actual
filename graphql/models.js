const { mergeTypes } = require('merge-graphql-schemas');
const ScalarModels = require('./services/scalar/models');
const BadgeModels = require('./resolvers/badge/models');
const JobsModels = require('./resolvers/jobs/models');
const LeaderboardsModels = require('./resolvers/leaderboards/models');
const ClaimsDraftsModels = require('./resolvers/claimsDrafts/models');
const GithubAuthModels = require('./resolvers/userAuthGithub/models');
const UserJobsModels = require('./resolvers/userJobs/models');
const UserJobsRoleModels = require('./resolvers/users/jobRoles/models');
const LinkedinArchiveModels = require('./resolvers/userProfileLinkedinarchive/models');
const UserSkillsModels = require('./resolvers/userProfileSkills/models');
const UserProfileModels = require('./resolvers/userProfile/models');
const UserJobsReferrals = require('./resolvers/userJobReferrals/models');
const VotesModels = require('./resolvers/votes/models');
const AssignmentModels = require('./resolvers/assignments/models');
const SignaturesModels = require('./resolvers/signatures/models');
const JobRoleModels = require('./resolvers/jobRoles/models');
const SkillModels = require('./resolvers/skills/models');
const StatsModels = require('./resolvers/stats/models');
const ClaimsModels = require('./resolvers/claims/models');
const CompanyModels = require('./resolvers/companies/models');
const AssessmentMagicLinkModels = require('./resolvers/assessmentMagicLink/models');
const clientApp = require('./resolvers/clientApp/models');
const subscriptionModels = require('./subscriptions/models');
const merkleTreeModels = require('./resolvers/merkleTree/models')
const DeleteUserDataModels = require('./resolvers/users/userData/models');
const VotesAdminModels = require('./resolvers/votesAdmin/models');
const ValidatorModels = require('./resolvers/validator/models');
const UtilsModels = require('./resolvers/utils/models');
const CompanyAssignmentModels = require('./resolvers/companyAssignment/model');
const UsersSkillModels = require('./resolvers/users/skills/models');
const UserEvaluationModels = require('./resolvers/users/evaluation/models');
const UserTaskModels = require('./resolvers/userTask/models');
const root = require('./resolvers/rootModel');
// import-new-schema (Do not remove/modify this line)

module.exports = mergeTypes([
    ScalarModels,
    BadgeModels,
    LeaderboardsModels,
    ClaimsDraftsModels,
    GithubAuthModels,
    JobsModels,
    LinkedinArchiveModels,
    UserJobsModels,
    UserSkillsModels,
    UserProfileModels,
    UserJobsReferrals,
    VotesModels,
    AssignmentModels,
    UserJobsRoleModels,
    JobRoleModels,
    SkillModels,
    SignaturesModels,
    StatsModels,
    ClaimsModels,
    CompanyModels,
    AssessmentMagicLinkModels,
    clientApp,
    subscriptionModels,
    merkleTreeModels,
    DeleteUserDataModels,
    VotesAdminModels,
    ValidatorModels,
    UtilsModels,
    CompanyAssignmentModels,
    UsersSkillModels,
    UserEvaluationModels,
    UserTaskModels,
    root,
    // add-new-schema (Do not remove/modify this line)
], { all: true });

