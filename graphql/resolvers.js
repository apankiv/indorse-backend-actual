const { merge } = require('lodash');
const ScalarResolvers = require('./services/scalar/resolvers');

const jobApplicantsById = require('./resolvers/userJobs/jobApplicantsById');
const createBadge = require('./resolvers/badge/createBadge');
const updateBadge = require('./resolvers/badge/updateBadge');
const deleteBadge = require('./resolvers/badge/deleteBadge');
const massAssignBadges = require('./resolvers/badge/massAssignBadges');
const getBadge = require('./resolvers/badge/getBadge');
const getAllBadges = require('./resolvers/badge/getAllBadges');
const allBadgeUsers = require('./resolvers/badge/getBadgeUsers');
const massAssignBadgesSubscription = require('./subscriptions/massAssignBadgesSubscription');
const companyTxCompleted = require('./subscriptions/companyTxCompleted');
const createClaimDraft = require('./resolvers/claimsDrafts/createClaimDraft');
const submitClaimDraftToken = require('./resolvers/claimsDrafts/submitClaimDraftToken');
const reopenClaim = require('./resolvers/claims/reopenClaim');
const getReleaseDetails = require('./resolvers/claims/getReleaseDetails');
const githubLogin = require('./resolvers/userAuthGithub/githubLogin');
const githubSignup = require('./resolvers/userAuthGithub/githubSignup');
const githubLink = require('./resolvers/userAuthGithub/githubLink');
const githubGetRepos = require('./resolvers/userAuthGithub/githubGetRepos');
const createJobPost = require('./resolvers/jobs/createJobPost');
const updateJobPost = require('./resolvers/jobs/updateJobPost');
const updateJobPostStatus = require('./resolvers/jobs/updateJobPostStatus');
const deleteJobPost = require('./resolvers/jobs/deleteJobPost');
const applyJobPost = require('./resolvers/jobs/applyJobPost');
const adminJobPosts = require('./resolvers/jobs/adminJobPosts');
const jobPosts = require('./resolvers/jobs/jobPosts');
const jobById = require('./resolvers/jobs/jobById');
const adminListReferralsByJobId = require('./resolvers/userJobReferrals/adminListReferralsByJobId');
const referJobToFriend = require('./resolvers/userJobReferrals/referJobToFriend');
const getLeaderboard = require('./resolvers/leaderboards/getLeaderboard');
const getValidatorDetails = require('./resolvers/leaderboards/getValidatorDetails');
const getAdminValidatorsList = require('./resolvers/leaderboards/getAdminValidatorsList');
const uploadLinkedinArchive = require('./resolvers/userProfileLinkedinarchive/uploadLinkedinArchive');
const getSkill = require('./resolvers/userProfileSkills/getSkill');
const skillUpdate = require('./resolvers/userProfileSkills/skillUpdate');
const skillDelete = require('./resolvers/userProfileSkills/skillDelete');
const ethaddressUpdate = require('./resolvers/userProfile/ethaddressUpdate');
const opportunitiesUpdate = require('./resolvers/userProfile/opportunitiesUpdate');
const sociallinksUpdate = require('./resolvers/userProfile/sociallinksUpdate');
const profileUpdate = require('./resolvers/userProfile/profileUpdate');
const startApplicationJobPost = require('./resolvers/jobs/startApplicationJobPost');
const userAppliedJobPosts = require('./resolvers/jobs/userAppliedJobPosts');
const createAssignment = require('./resolvers/assignments/createAssignment');
const getAssignmentsBySkill = require('./resolvers/assignments/getAssignmentsBySkill');
const getAssignment = require('./resolvers/assignments/getAssignment');
const getAssignments = require('./resolvers/assignments/getAssignments');
const startAssignment = require('./resolvers/assignments/startAssignment');
const finishAssignment = require('./resolvers/assignments/finishAssignment');
const jobRoles = require('./resolvers/jobRoles/getJobRoles');
const uploadAssignmentPic = require('./resolvers/assignments/uploadAssignmentPic');
const userJobRoleUpdate = require('./resolvers/users/jobRoles/userJobRoleUpdate');
const createJobRole = require('./resolvers/jobRoles/createJobRole');
const updateJobRole = require('./resolvers/jobRoles/updateJobRole');
const jobRoleById = require('./resolvers/jobRoles/jobRoleById');
const jobRoleSkills = require('./resolvers/jobRoles/jobRoleSkills');
const jobRoleAssignments = require('./resolvers/jobRoles/jobRoleAssignments');
const userJobRoleSkillUpdate = require('./resolvers/users/jobRoles/userJobRoleSkillUpdate');
const createSkill = require('./resolvers/skills/createSkill');
const updateSkillByName = require('./resolvers/skills/updateSkillByName');
const deleteSkillByName = require('./resolvers/skills/deleteSkillByName');
const skillByName = require('./resolvers/skills/skillByName');
const skillParents = require('./resolvers/skills/skillParents');
const updateAssignment = require('./resolvers/assignments/updateAssignment');
const toggleStatusAssignment = require('./resolvers/assignments/toggleStatusAssignment');
const adminStats = require('./resolvers/stats/adminStats');
const payoutClaims = require('./resolvers/claims/payoutClaims');
const sendSurveyResult = require('./resolvers/claims/sendSurveyResult');
const getTopRanks = require('./resolvers/leaderboards/getTopRanks');
const assignmentJobRole = require('./resolvers/assignments/assignmentJobRole');
const getVotePayload = require('./resolvers/signatures/getVotePayload');
const submitVoteSignature = require('./resolvers/signatures/submitVoteSignature');
const viewerAssignments = require('./resolvers/assignments/viewerAssignments');
const lastViewerAssignment = require('./resolvers/assignments/lastViewerAssignment');
const userAssignmentClaimSummary = require('./resolvers/assignments/claimSummary');
const userAssignmentSkills = require('./resolvers/assignments/userAssignmentSkills');
const userAssignmentSkillDetails = require('./resolvers/assignments/userAssignmentSkillDetails');
const getRewardStatement = require('./resolvers/votes/getRewardStatement');
const getMyVotes = require('./resolvers/votes/getPastVotes');
const getPendingVotedClaims = require('./resolvers/votes/getPendingVotedClaims');
const setTags = require('./resolvers/claims/setTags');
const getMyCompanies = require('./resolvers/companies/getMyCompanies');
const getCompanyACL = require('./resolvers/companies/getCompanyACL');
const companyACLMemberUser = require('./resolvers/companies/aclMemberUser');
const getCompanyByPrettyId = require('./resolvers/companies/getCompanyByPrettyId');
const visitorListCompanies = require('./resolvers/companies/visitorListCompanies');
const acceptInviteToCompanyACL = require('./resolvers/companies/acceptInviteToCompanyACL');
const sendInviteToCompanyACL = require('./resolvers/companies/sendInviteToCompanyACL');
const removeCompanyACL = require('./resolvers/companies/removeCompanyACL');
const createAssessmentMagicLink = require('./resolvers/assessmentMagicLink/createAssessmentMagicLink');
const getAssessmentMagicLinkById = require('./resolvers/assessmentMagicLink/getAssessmentMagicLinkById');
const deleteAssessmentMagicLink = require('./resolvers/assessmentMagicLink/deleteAssessmentMagicLink');
const getAllAssessmentMagicLinkByPrettyId = require('./resolvers/assessmentMagicLink/getAllAssessmentMagicLinkByPrettyId');
const startAssessmentMagicLink = require('./resolvers/assessmentMagicLink/startAssessmentMagicLink');
const getViewerUserAssessment = require('./resolvers/assessmentMagicLink/getViewerUserAssessment');
const getAllUserAssessments = require('./resolvers/assessmentMagicLink/getAllUserAssessments');
const getMerkleTree = require('./resolvers/merkleTree/getMerkleTree');
const getAllowedAssignments = require('./resolvers/assessmentMagicLink/getAllowedAssignments');
const allowAssignmentForCandidate = require('./resolvers/assessmentMagicLink/allowAssignmentForCandidate');
const batchInviteCandidates = require('./resolvers/assessmentMagicLink/batchInviteCandidates');
const updateCompanyCredits = require('./resolvers/companies/updateCompanyCredits');
const updateCompanyAIPLimit = require('./resolvers/companies/updateCompanyAIPLimit');
const setAssessmentMagicLinkActive = require('./resolvers/assessmentMagicLink/setAssessmentMagicLinkActive');
const claim_companyChildResolver = require('./resolvers/claims/resolveCompanyObject');
const getClaimById = require('./resolvers/claims/getClaimById');
const claim_claimStatusChildResolver = require('./resolvers/claims/claimStatusChildResolver');
const claim_clientAppChildResolver = require('./resolvers/claims/clientAppChildResoler');
const company_claimsChildResolver = require('./resolvers/companies/claimsChildResolver');
const company_clientAppsChildResolver = require('./resolvers/companies/clientAppsChildResolver');
const company_aipLimitChildResolver = require('./resolvers/companies/aipLimitChildResolver');
const clientApp_claimsChildResolver = require('./resolvers/clientApp/claimsChildResolver');
const clientApp_companyChildResolver = require('./resolvers/clientApp/companyChildResolver');
const clientApp_getClientAppByClientId = require('./resolvers/clientApp/getClientAppByClientId');
const voteSubmitted = require('./subscriptions/voteSubmitted');
const deleteUserData = require('./resolvers/users/userData/deleteUserData')
// const voteTest = require('./subscriptions/voteTest');
const { updateValidatorComment } = require('./resolvers/votesAdmin/updateValidatorComment');
const markAssignmentsViewed = require('./resolvers/assessmentMagicLink/markAssignmentsViewed');
const getAdminClaims = require('./resolvers/claims/getAdminClaims');
const userAssessmentClaimChildResolver = require('./resolvers/userAssessment/claimChildResolver');
const addValidator = require('./resolvers/validator/addValidator');
const updateValidator = require('./resolvers/validator/updateValidator');
const deleteValidator = require('./resolvers/validator/deleteValidator');
const AIPSkillNames = require('../models/services/validator/allowedAIPSkills');
const approveClaim = require('./resolvers/claims/approveClaim');
const assessmentMagicLink_CompanyChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/company');
const assessmentMagicLink_ChatbotSkillsChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/chatbotSkills');
const assessmentMagicLink_ChatbotsPassedChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/chatbotsPassedCount');
const assessmentMagicLink_AssignmentSkillsChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/assignmentSkills');
const assessmentMagicLink_AssignmentIndorseChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/assignmentIndorsedCount');
const assessmentMagicLink_AssignmentEvaluatedChildResolver = require('./resolvers/assessmentMagicLink/childResolvers/assignmentEvaluatedCount');
const recordContactRequest = require('./resolvers/utils/recordContactRequest');
const getAdminClaim = require('./resolvers/claims/getAdminClaim');
const editAssessmentMagicLink = require('./resolvers/assessmentMagicLink/editAssessmentMagicLink');
const restrictAssignmentsForCompany = require('./resolvers/companies/restrictAssignmentsForCompany');
const company_magicLinkChildResolver = require('./resolvers/companies/magicLinkChildResolver');
const getAssignmentsWithCompanyStatus = require('./resolvers/companyAssignment/getAssignmentsWithCompanyStatus');
const getCompanyAssignments = require('./resolvers/companyAssignment/getCompanyAssignments');
const updateCompanyAssignmentLink = require('./resolvers/companyAssignment/updateCompanyAssignmentLink');
const getSkillsByViewerJobRoles = require('./resolvers/users/jobRoles/getSkillsByViewerJobRoles');
const updateViewerJobRoles = require('./resolvers/users/jobRoles/updateViewerJobRoles');
const skill_userHasSkillChildResolver = require('./resolvers/skills/childResolver/userHasSkill');
const jobRole_userHasJobRoleChildResolver = require('./resolvers/jobRoles/childResolver/userHasJobRole');
const updateViewerSkills = require('./resolvers/users/skills/updateViewerSkills');
const getUserEvaluations = require('./resolvers/users/evaluation/getUserEvaluations');
const userEvaluations_chatbotChildResolvers = require('./resolvers/users/evaluation/childResolvers/chatbotsChildResolver');
const userEvaluations_assignmentsChildResolver = require('./resolvers/users/evaluation/childResolvers/assignmentsChildResolver');
const userEvaluations_claimsChildResolver = require('./resolvers/users/evaluation/childResolvers/claimsChildResolver');
const claim_skillsChildResolver = require('./resolvers/claims/skillsChildResolver');
const claim_claimSummaryChildResolver = require('./resolvers/claims/claimSummaryChildResolver');
const getViewerTasks = require('./resolvers/userTask/getViewerTasks');
const userTaskCompleted = require('./subscriptions/userTaskCompleted');
const getUserJobRoles = require('./resolvers/jobRoles/getUserJobRoles');
const accessClaimReport = require('./resolvers/claims/accessClaimReport');
// import_new_query/mutation/subscription (Do not modify/delete this line)

const Resolvers = {
    Query: {
        jobApplicantsById: jobApplicantsById,
        badge: getBadge,
        allBadges: getAllBadges,
        allBadgeUsers: allBadgeUsers,
        githubGetRepos: githubGetRepos,
        getLeaderboard : getLeaderboard,
        getAdminValidatorsList : getAdminValidatorsList,
        leaderboards_getTopRanks: getTopRanks,
        getValidatorDetails: getValidatorDetails,
        adminJobPosts: adminJobPosts,
        jobPosts: jobPosts,
        getReleaseDetails: getReleaseDetails,
        jobById: jobById,
        adminListReferralsByJobId: adminListReferralsByJobId,
        getSkill: getSkill,
        userAppliedJobPosts: userAppliedJobPosts,
        votes_getPastVotedClaims : getMyVotes,
        getAssignment : getAssignment,
        getAssignments : getAssignments,
        getVotePayload : getVotePayload,
        getMerkleTree : getMerkleTree,
        jobRoles : jobRoles,
        jobRoleById,
        getAssignmentsBySkill,
        adminStats,
        skillByName,
        votes_getRewardStatement: getRewardStatement,
        votes_getPendingVotedClaims: getPendingVotedClaims,
        getMyCompanies,
        visitorListCompanies,
        getCompanyACL,
        getCompanyByPrettyId,
        getAssessmentMagicLinkById,
        getAllAssessmentMagicLinkByPrettyId,
        getClaimById,
        getClientAppByClientId: clientApp_getClientAppByClientId,
        getAdminClaims,
        getAdminClaim,
        getAssignmentsWithCompanyStatus,
        getCompanyAssignments,
        getSkillsByViewerJobRoles,
        getUserEvaluations,
        getViewerTasks,
        getUserJobRoles,
        // add_new_query (Do not modify/delete this line)
    },
    Mutation: {
        createBadge: createBadge,
        sendSurveyResult : sendSurveyResult,
        updateBadge: updateBadge,
        deleteBadge: deleteBadge,
        massAssignBadges: massAssignBadges,
        createClaimDraft: createClaimDraft,
        submitClaimDraftToken: submitClaimDraftToken,
        githubLogin: githubLogin,
        githubSignup: githubSignup,
        githubLink: githubLink,
        createJobPost: createJobPost,
        updateJobPost: updateJobPost,
        updateJobPostStatus: updateJobPostStatus,
        deleteJobPost: deleteJobPost,
        applyJobPost: applyJobPost,
        referJobToFriend: referJobToFriend,
        uploadLinkedinArchive: uploadLinkedinArchive,
        ethaddressUpdate: ethaddressUpdate,
        opportunitiesUpdate: opportunitiesUpdate,
        sociallinksUpdate: sociallinksUpdate,
        profileUpdate: profileUpdate,
        skillUpdate: skillUpdate,
        skillDelete: skillDelete,
        startApplicationJobPost: startApplicationJobPost,
        startAssignment,
        finishAssignment: finishAssignment,
        submitVoteSignature : submitVoteSignature,
        createAssignment: createAssignment,
        userJobRoleUpdate: userJobRoleUpdate,
        createJobRole: createJobRole,
        updateJobRole: updateJobRole,
        userJobRoleSkillUpdate: userJobRoleSkillUpdate,
        createSkill: createSkill,
        updateSkillByName: updateSkillByName,
        deleteSkillByName: deleteSkillByName,
        uploadAssignmentPic : uploadAssignmentPic,
        toggleStatusAssignment : toggleStatusAssignment,
        updateAssignment : updateAssignment,
        reopenClaim : reopenClaim,
        payoutClaims: payoutClaims,
        claims_setTags: setTags,
        acceptInviteToCompanyACL,
        sendInviteToCompanyACL,
        removeCompanyACL,
        createAssessmentMagicLink,
        deleteAssessmentMagicLink: deleteAssessmentMagicLink,
        startAssessmentMagicLink,
        allowAssignmentForCandidate,
        batchInviteCandidates,
        updateCompanyCredits,
        setAssessmentMagicLinkActive,
        updateCompanyAIPLimit,
        // voteTest,
        updateValidatorComment,
        markAssignmentsViewed,
        deleteUserData,
        addValidator,
        updateValidator,
        deleteValidator,
        approveClaim,
        recordContactRequest,
        editAssessmentMagicLink,
        restrictAssignmentsForCompany,
        updateCompanyAssignmentLink,
        updateViewerJobRoles,
        updateViewerSkills,
        accessClaimReport,
        // add_new_mutation (Do not modify/delete this line)
    },
    Subscription: {
        massAssignBadgesSubscription,
        companyTxCompleted,
        voteSubmitted,
        userTaskCompleted,
    },
    JobRole: {
        skills: jobRoleSkills,
        assignments: jobRoleAssignments,
        userHasJobRole: jobRole_userHasJobRoleChildResolver,
    },
    Skill: {
        parents: skillParents,
        userHasSkill: skill_userHasSkillChildResolver,
    },
    Assignment: {
        jobRole: assignmentJobRole,
        viewerAssignments,
        lastViewerAssignment,
    },
    UserAssignment: {
        claim: userAssignmentClaimSummary,
        skills: userAssignmentSkills,
    },
    UserAssignmentSkill: {
        skill: userAssignmentSkillDetails,
    },
    AssessmentMagicLink: {
        viewerAssessment: getViewerUserAssessment,
        allUserAssessments: getAllUserAssessments,
        allowedAssignments: getAllowedAssignments,
        company: assessmentMagicLink_CompanyChildResolver,
        chatbotSkills: assessmentMagicLink_ChatbotSkillsChildResolver,
        chatbotsPassedCount: assessmentMagicLink_ChatbotsPassedChildResolver,
        assignmentSkills: assessmentMagicLink_AssignmentSkillsChildResolver,
        assignmentIndorsedCount: assessmentMagicLink_AssignmentIndorseChildResolver,
        assignmentEvaluatedCount: assessmentMagicLink_AssignmentEvaluatedChildResolver,
    },
    Company: {
        claims: company_claimsChildResolver,
        clientApps: company_clientAppsChildResolver,
        aipLimit: company_aipLimitChildResolver,
        magicLink: company_magicLinkChildResolver,
    },
    CompanyACLMember: {
        user: companyACLMemberUser,
    },
    Claim: {
        company: claim_companyChildResolver,
        claimStatus: claim_claimStatusChildResolver,
        clientApp: claim_clientAppChildResolver,
        skills: claim_skillsChildResolver,
        claimSummary: claim_claimSummaryChildResolver,
    },
    ClientApp: {
        claims: clientApp_claimsChildResolver,
        company: clientApp_companyChildResolver,
    },
    UserAssessment: {
        claim: userAssessmentClaimChildResolver,
    },
    UserEvaluations: {
        claims: userEvaluations_claimsChildResolver,
        assignments: userEvaluations_assignmentsChildResolver,
        chatbots: userEvaluations_chatbotChildResolvers,
    },
    AIPSkillNames: AIPSkillNames
};


module.exports = merge(
    ScalarResolvers,
    Resolvers,
);
