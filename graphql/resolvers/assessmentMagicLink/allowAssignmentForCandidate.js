const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const timestampService = require('../../../models/services/common/timestampService');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const authUtils = require('../../../models/services/auth/authChecks');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const emailService = require('../../../models/services/common/emailService');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');

const allowAssignmentForCandidate = async (root, { publicId, candidateId }, { req }) => {
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    assessmentMagicLinkUtils.assessmentMagicLinkSanityCheck(magicLink);
    if (magicLink.active === false) { // dont remove this it will break previous magic links
        errorUtils.throwError('Magic Link is not active', 404);
    }
    const company = await mongooseCompanyRepo.findOneById(magicLink.companyId);
    if (!company) {
        errorUtils.throwError('Company doesnt exist', 404);
    }
    await authUtils.companyPermissionCheck(req, company._id, ['candidate.write']);
    if (!req.user_id) {
        errorUtils.throwError('You are not allowed to perform this action', 403);
    }

    const candidate = await mongooseUserRepo.findOneById(candidateId);

    if (!candidate) {
        errorUtils.throwError('User doesnt exist', 404);
    }

    const criteria = {
        assessment: magicLink._id,
        startedBy: candidateId,
    };
    const userAttempted = await mongooseUserAssessmentRepo.findOne(criteria);
    if (!userAttempted) {
        errorUtils.throwError('Unable to find this candidate', 401);
    }
    const candidateCurrentStatus = userAttempted.status;

    if (candidateCurrentStatus === userAssessmentStates.ASSIGNMENT_ALLOWED) {
        return assessmentMagicLinkUtils.getUserAssessment(userAttempted);
    }

    if (candidateCurrentStatus !== userAssessmentStates.CHATBOTS_PASSED) {
        errorUtils.throwError(`Not allowed to perform this action, status ${candidateCurrentStatus}`, 400);
    }

    const now = timestampService.createTimestamp();

    await mongooseUserAssessmentRepo.update({ _id: userAttempted._id }, { status: userAssessmentStates.ASSIGNMENT_ALLOWED, assignmentAllowedAt: now, assignmentAllowedBy: req.user_id });
    const updatedUserAssessment = await mongooseUserAssessmentRepo.findOneById(userAttempted._id);
    await emailService.sendAssignmentAllowedForMagicLinkEmail(candidate.email, candidate.name, company.pretty_id, publicId);
    return assessmentMagicLinkUtils.getUserAssessment(updatedUserAssessment);
};

module.exports = allowAssignmentForCandidate;
