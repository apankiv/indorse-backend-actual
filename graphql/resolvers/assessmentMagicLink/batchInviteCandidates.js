const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const { inviteCandidateForMagicLink } = require('../../../models/services/common/emailService');
const authUtils = require('../../../models/services/auth/authChecks');
const { validateEmail } = require('../../../models/services/common/safeObjects');
const timestampService = require('../../../models/services/common/timestampService');
const inviteUtils = require('../../../models/services/common/inviteUtils');
const mongooseInviteRepo = require('../../../models/services/mongo/mongoose/mongooseInviteRepo');
const { inviteType } = require('../../../models/services/mongo/mongoose/schemas/invite');

// const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
/*
    Invite Multiple Candidates to take Magic Link
 */
const batchInviteCandidates = async (root, { publicId, inviteEmails }, { req }) => {
    // here we are calling assessment magic link as magic link
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    if (!magicLink) {
        errorUtils.throwError("Oops, this magic link doesn't exist anymore", 404);
    }
    if (magicLink.deleted) {
        errorUtils.throwError('This magic link was deleted by the admin', 404);
    }
    if (magicLink.active === false) {
        errorUtils.throwError('Magic Link is not active', 404);
    }
    const company = await mongooseCompanyRepo.findOneById(magicLink.companyId);
    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }
    // check the user for permission
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);

    // Validate Emails and generate error in case email is not proper
    let errors = inviteEmails.map((email, index) => {
        if (!validateEmail(email)) {
            return {
                index,
                email,
            };
        }
        return false;
    });
    errors = errors.filter(Boolean);
    if (errors.length > 0) {
        // attach custom errors to request
        return {
            success: false,
            errors,
        };
    }

    for (let i = 0; i < inviteEmails.length; i += 1) {
        const email = inviteEmails[i];
        // Create Invite
        const token = inviteUtils.generateToken(12);
        const inviteObject = {
            token,
            email,
            invitedBy: req.user_id,
            invitedAt: timestampService.createTimestamp(),
            invite_type: inviteType.MAGIC_LINK,
            invitedFor: magicLink._id,
        };
        await mongooseInviteRepo.insert(inviteObject); // not using invite token for now.
        const sendEmailObject = {
            email,
            companyName: company.company_name,
            companyPrettyId: company.pretty_id,
            magicLinkPublicId: magicLink.publicId,
            magicLinkTitle: magicLink.title,
        };
        await inviteCandidateForMagicLink(sendEmailObject);
    }
    return {
        success: true,
        errors,
    };
};

module.exports = batchInviteCandidates;
