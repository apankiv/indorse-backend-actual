const mongooseUserAssessmentRepo = require('../../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const { status: userAssessmentStates } = require('../../../../models/services/mongo/mongoose/schemas/userAssessment');

const assignmentEvaluatedCount = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.id) return 0;
    const criteriaAssignmentAssessment = {
        assessment: assessmentMagicLink.id,
        $or: [
            {
                assignmentStatus: userAssessmentStates.ASSIGNMENT_EVALUATED,
            },
            {
                status: userAssessmentStates.ASSIGNMENT_EVALUATED,
            },
            {
                claimStatus: userAssessmentStates.CLAIM_EVALUATED,
            },
            {
                status: userAssessmentStates.CLAIM_EVALUATED,
            },
        ],
    };
    return await mongooseUserAssessmentRepo.countAll(criteriaAssignmentAssessment);
};

module.exports = assignmentEvaluatedCount;
