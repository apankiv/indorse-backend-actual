const mongooseUserAssessmentRepo = require('../../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');

const assignmentIndorsedCount = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.id) return 0;
    const criteriaAssessmentIndorsed = {
        assessment: assessmentMagicLink.id,
        $or: [
            {
                isAssignmentIndorsed: true,
            },
            {
                isClaimIndorsed: true,
            },
        ],
    };
    return await mongooseUserAssessmentRepo.countAll(criteriaAssessmentIndorsed);
};

module.exports = assignmentIndorsedCount;
