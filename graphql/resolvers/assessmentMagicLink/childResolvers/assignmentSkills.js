const mongooseSkillRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo');

const assignmentSkills = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.assignmentSkills) return [];
    const updatedAssignmentSkills = assessmentMagicLink.assignmentSkills || [];
    for (let i = 0; i < updatedAssignmentSkills.length; i += 1) {
        const criteria = {
            _id: {
                $in: updatedAssignmentSkills[i].allowedSkillIds || [],
            },
        };
        updatedAssignmentSkills[i].allowedSkills = await mongooseSkillRepo.findAll(criteria);
        updatedAssignmentSkills[i].allowedSkills = updatedAssignmentSkills[i].allowedSkills || [];
    }
    return updatedAssignmentSkills || [];
};

module.exports = assignmentSkills;
