const mongooseSkillRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo');

const chatbotSkills = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.chatbotSkills) return [];
    // prepare chatbot object
    const criteriaChatbotSkills = {
        _id: {
            $in: assessmentMagicLink.chatbotSkills || [],
        },
    };
    const chatbotSkillsObjects = await mongooseSkillRepo.findAll(criteriaChatbotSkills);
    return chatbotSkillsObjects || [];
};

module.exports = chatbotSkills;
