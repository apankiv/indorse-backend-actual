const mongooseUserAssessmentRepo = require('../../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const { status: userAssessmentStates } = require('../../../../models/services/mongo/mongoose/schemas/userAssessment');

const chatbotsPassedCount = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.id) return 0;
    const criteriaChatbotAssessment = {
        assessment: assessmentMagicLink.id,
        $or: [
            {
                chatbotStatus: userAssessmentStates.CHATBOTS_PASSED,
            },
            {
                status: userAssessmentStates.CHATBOTS_PASSED,
            },
        ],
    };
    return await mongooseUserAssessmentRepo.countAll(criteriaChatbotAssessment);
};

module.exports = chatbotsPassedCount;
