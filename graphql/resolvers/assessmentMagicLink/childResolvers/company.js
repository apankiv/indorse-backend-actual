const getCompanyUtils = require('../../../../models/services/companies/getCompanyUtils');
const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');

const chatbotSkills = async (root, args, {}) => {
    const assessmentMagicLink = root;

    if (!assessmentMagicLink || !assessmentMagicLink.companyId) return null;
    const company = await mongooseCompanyRepo.findOneById(assessmentMagicLink.companyId);
    return await getCompanyUtils.prepareCompanyObject(company);
};

module.exports = chatbotSkills;
