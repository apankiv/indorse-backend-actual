const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
// const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const timestampService = require('../../../models/services/common/timestampService');
// const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const authUtils = require('../../../models/services/auth/authChecks');
const skillUtils = require('../../../models/services/skills/skillUtils');
const shortid = require('shortid');

const verifyAssignmentSkills = async function verifyAssignmentSkills(assignmentSkills) {
    for (let i = 0; i < assignmentSkills.length; i += 1) {
        const { skillTag } = assignmentSkills[i];
        let { allowedSkillIds } = assignmentSkills[i];
        allowedSkillIds = allowedSkillIds || [];
        // console.log("allowedSkillIds: ", allowedSkillIds);
        const criteria = {
            tags: skillTag,
            _id: {
                $in: allowedSkillIds, // this should throw the error if skillId is not in proper format
            },
        };
        let skills = await mongooseSkillRepo.findAll(criteria);
        skills = skills || [];
        // console.log(skills);
        if (skills.length !== allowedSkillIds.length) {
            errorUtils.throwError('Some skills not found', 400);
        }
    }
};

const getClaimSkillsByAssignmentSkills = async (assignmentSkills) => {
    let allAllowedSkillIds = [];
    for (let i = 0; i < assignmentSkills.length; i += 1) {
        const { allowedSkillIds } = assignmentSkills[i];
        allAllowedSkillIds = [...allAllowedSkillIds, ...allowedSkillIds];
    }
    const parentSkills = await skillUtils.findParentSkillsOfMultipleSkills(allAllowedSkillIds);
    const allConcernedSkills = [...allAllowedSkillIds, ...parentSkills];
    const criteria = {
        $and: [
            {
                _id: {
                    $in: allConcernedSkills,
                },
            },
            {
                'validation.aip': true,
            },
        ],
    };
    const validClaimSkills = await mongooseSkillRepo.findAll(criteria);
    return validClaimSkills || [];
};

const createAssessmentMagicLink = async (root, { title, jobPostLink, role, companyPrettyId, chatbotSkills, assignmentSkills, claimAllowed = false }, { req }) => {
    let claimSkills = null;
    const companyCriteria = {
        pretty_id: companyPrettyId,
    };
    const company = await mongooseCompanyRepo.findOne(companyCriteria);
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);
    const isAutoAllowed = (company && company.magicLink && company.magicLink.autoAllowedAssignment) === true;
    if (!req.user_id) {
        errorUtils.throwError('You are not allowed to perform this action', 403); // This could only happen if there's error in auth.js middleware
    }

    if (!company) {
        errorUtils.throwError('Company doesnt exist', 404);
    }

    const jobRole = await mongooseJobRoleRepo.findOneById(role);
    if (!jobRole) {
        errorUtils.throwError('Job role does not exist', 404);
    }

    // chatbot preparation
    const criteria = {
        name: {
            $in: chatbotSkills || [],
        },
    };
    let foundChatbotSkills = await mongooseSkillRepo.findAll(criteria);
    if (!foundChatbotSkills) foundChatbotSkills = [];
    const validChatbotSkills = foundChatbotSkills.filter(skill => skill.validation && skill.validation.chatbot);
    const validChatbotSkillIds = validChatbotSkills.map(skill => skill._id.toString());

    // verify if skilltags are in sync with the skill ids
    await verifyAssignmentSkills(assignmentSkills || []).catch((error) => {
        console.log('Error after verifying the assignment skills: ', error);
        errorUtils.throwError('Send proper skill tags and ids', 400);
    });
    const { skillTags } = jobRole;
    (skillTags || []).map((tag) => {
        // console.log(tag);
        const temporaryAssignmentSkill = (assignmentSkills || []).filter(assignmentSkill => assignmentSkill.skillTag === tag);
        if (temporaryAssignmentSkill.length === 0) {
            errorUtils.throwError('Please add at-least one '+ skillTags +'  skill ' , 400);
        }
        return temporaryAssignmentSkill;
    });

    if (claimAllowed) {
        claimSkills = await getClaimSkillsByAssignmentSkills(assignmentSkills || []);
    }

    const magicLinkObject = {
        title,
        role,
        companyId: company._id,
        companyPrettyId, // Now not used
        createdAt: timestampService.createTimestamp(),
        createdBy: req.user_id,
        chatbotSkills: validChatbotSkillIds || [],
        assignmentSkills: assignmentSkills || [],
        claimSkills: claimSkills || [],
        claimAllowed,
        publicId: shortid.generate(),
        autoAllowedAssignment: isAutoAllowed === true,
        active: true,
    };
    if (jobPostLink) {
        magicLinkObject.jobPostLink = jobPostLink;
    }
    const magicLinkId = await mongooseAssessmentMagicLinkRepo.insert(magicLinkObject);

    if (magicLinkId && magicLinkObject.publicId) {
        return true;
    }
    return false;
};

module.exports = createAssessmentMagicLink;
