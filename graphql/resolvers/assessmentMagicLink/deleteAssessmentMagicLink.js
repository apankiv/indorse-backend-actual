const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const deleteAssessmentMagicLink = async (root, { publicId }, { req }) => {
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    if (!magicLink) {
        errorUtils.throwError('Magic link not found', 404);
    }
    const company = await mongooseCompanyRepo.findOneById(magicLink.companyId);
    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);

    await mongooseAssessmentMagicLinkRepo.softDelete({ publicId }, req.user_id);
    return true;
};

module.exports = deleteAssessmentMagicLink;
