const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const authUtils = require('../../../models/services/auth/authChecks');


const getAllAssessmentMagicLinkByPrettyId = async (root, { prettyId }, { req }) => {
    const company = await mongooseCompanyRepo.findOne({ pretty_id: prettyId });
    if (!company) {
        errorUtils.throwError('Company doesnt exist', 404);
    }
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.read']);
    const criteria = {
        companyId: company._id,
        deleted: { $ne: true },
    };
    let findAllMagicLinks = await mongooseAssessmentMagicLinkRepo.findAll(criteria);
    if (!findAllMagicLinks) findAllMagicLinks = [];

    const companyMagicLinks = [];
    for (let i = 0; i < findAllMagicLinks.length; i += 1) {
        const curMagicLink = findAllMagicLinks[i];
        const jobRoleId = curMagicLink.role;
        const jobRole = await mongooseJobRoleRepo.findOneById(jobRoleId);
        curMagicLink.role = jobRole;
        companyMagicLinks.push(await assessmentMagicLinkUtils.getAssessmentMagicLink(curMagicLink));
    }
    return companyMagicLinks.reverse();
};

module.exports = getAllAssessmentMagicLinkByPrettyId;
