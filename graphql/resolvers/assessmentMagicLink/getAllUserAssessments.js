const authUtils = require('../../../models/services/auth/authChecks');
const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

const getAllUserAssessments = async (magicLink, args, { req }) => {
    const magicLinkFromDB = await mongooseAssessmentMagicLinkRepo.findOneById(magicLink.id);
    if (!magicLinkFromDB) {
        errorUtils.throwError('This magic link was deleted !', 404);
    }
    if (magicLinkFromDB.deleted) {
        errorUtils.throwError('This magic link was deleted by the admin !', 404);
    }

    const company = await mongooseCompanyRepo.findOneById(magicLinkFromDB.companyId);
    if (!company) {
        errorUtils.throwError('Company not found !', 404);
    }
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.read']);

    const criteria = {
        assessment: magicLink.id,
    };
    let usersAttempted = await mongooseUserAssessmentRepo.findAll(criteria);
    if (!usersAttempted) {
        usersAttempted = [];
    }

    const userAssessment = [];
    for (let i = 0; i < usersAttempted.length; i += 1) {
        userAssessment.push(await assessmentMagicLinkUtils.getUserAssessment(usersAttempted[i]));
    }

    let userAssessmentsToReturn = await assessmentMagicLinkUtils.hideCreditLimitReachedAssignments(userAssessment,company);
    return userAssessmentsToReturn;
};

module.exports = getAllUserAssessments;
