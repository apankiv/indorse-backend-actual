const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseCompanyAssignmentsRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyAssignmentsRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const { ObjectID } = require('mongodb');

const getAllowedAssignments = async (magicLink, args, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('You must be logged in for this', 403);
    }
    const criteria = {
        disabled: { $ne: true },
    };
    if (magicLink.role && magicLink.role._id) criteria.jobRole = ObjectID(magicLink.role._id);
    const { companyId } = magicLink;
    const company = await mongooseCompanyRepo.findOneById(companyId);
    const { magicLink: magicLinkConfig = {} } = company;
    const companyAssignmentObject = await mongooseCompanyAssignmentsRepo.findOne({ companyId });
    const { linkedAssignments = [] } = companyAssignmentObject || {};
    const linkedAssignmentIds = linkedAssignments.map(assignment => assignment.assignmentId);
    if (magicLinkConfig.assignmentRestricted) {
        criteria._id = { $in: linkedAssignmentIds };
    }
    const assignments = await mongooseAssignmentRepo.findAll(criteria);
    return assignments || [];
};

module.exports = getAllowedAssignments;
