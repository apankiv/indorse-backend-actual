const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const authUtils = require('../../../models/services/auth/authChecks');
/*
    magic link should have UserAssessment
 */
const getAssessmentMagicLinkById = async (root, { publicId }, { req }) => {
    // here we are calling assessment magic link as magic link
    // create magic link of assessment
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    if (!magicLink) {
        errorUtils.throwError("Oops, this magic link doesn't exist anymore", 404);
    }
    if (magicLink.deleted) {
        errorUtils.throwError('This magic link was deleted by the admin', 404);
    }
    const magicLinkToReturn = await assessmentMagicLinkUtils.getAssessmentMagicLink(magicLink);
    return magicLinkToReturn;
};

module.exports = getAssessmentMagicLinkById;
