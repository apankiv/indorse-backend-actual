const errorUtils = require('../../../models/services/error/errorUtils');
// const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
// const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
// const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
/*
    magic link should have UserAssessment
 */
const getViewerUserAssessment = async (magicLink, {}, { req, res }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in !', 403);
    }
    const magicLinkId = magicLink.id; // We get this because this is graphQL child resolver
    const criteria = {
        assessment: magicLinkId,
        startedBy: req.user_id,
    };
    const userAttempted = await mongooseUserAssessmentRepo.findOne(criteria);
    if (!userAttempted) {
        return null;
    }
    const viewerAssessment = await assessmentMagicLinkUtils.getUserAssessment(userAttempted);
    return viewerAssessment;
};

module.exports = getViewerUserAssessment;
