const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const { userAssessmentStatusUpdate } = require('../../../models/services/companies/notification');
const timestampService = require('../../../models/services/common/timestampService');

const markAssignmentsViewed = async (root, { assessmentId }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User must be logged in for this operation !', 403);
    }
    const criteria = {
        assessment: assessmentId,
        startedBy: req.user_id,
    };
    const userAssessment = await mongooseUserAssessmentRepo.findOne(criteria);
    if (!userAssessment) {
        errorUtils.throwError('Assessment not yet attempted', 404);
    }
    if (userAssessment.status === userAssessmentStates.ASSIGNMENTS_VIEWED) {
        return true;
    }
    if (userAssessment && userAssessment.status !== userAssessmentStates.ASSIGNMENT_ALLOWED) {
        errorUtils.throwError('You are not allowed to view the assignments for this magic link', 403);
    }
    const status = userAssessmentStates.ASSIGNMENTS_VIEWED;
    const dataToSet = {
        $set: {
            status,
            assignmentStatus: status,
            assignmentsViewedAt: timestampService.createTimestamp(),
        },
    };
    await mongooseUserAssessmentRepo.update(criteria, dataToSet);
    await userAssessmentStatusUpdate(status, userAssessment._id);
    return true;
};

module.exports = markAssignmentsViewed;
