module.exports = `
type AssessmentMagicLink {
  id: String!
  title: String!
  role: JobRole
  createdAt: Int
  jobPostLink: String
  publicId: String!
  active: Boolean!
  claimAllowed: Boolean!
  chatbotSkills: [Skill]
  assignmentSkills: [MagicLinkSkill!]!
  company: Company
  chatbotsPassedCount: Int
  assignmentEvaluatedCount: Int
  assignmentIndorsedCount: Int
  viewerAssessment: UserAssessment
  allUserAssessments: [UserAssessment]
  allowedAssignments: [Assignment!]! # Child Resolver.  For now, this dynamically returns all assignment based on magic link job role. In future, it could be used to add additional filters that we allow company admin to specify
}

type Query {
  getAllAssessmentMagicLinkByPrettyId(prettyId: String!): [AssessmentMagicLink]
  getAssessmentMagicLinkById(publicId: String!): AssessmentMagicLink
  # new_query (Do not remove this line)
}

type MagicLinkSkill {
  skillTag: String!  # eg. back-end
  allowedSkills: [Skill!]! # eg. [node.js skill object, ruby-on-rails skill object]
}

# This needs to validated based on job role id passed
input MagicLinkSkillInput {
  skillTag: String!  # eg. back-end
  allowedSkillIds: [String!]! # eg. [id of node.js, id of ruby-on-rails]
}

type InvalidEmailErrorObject {
  email: String!
  index: Int!
}

type BatchInviteResponse {
  success: Boolean!
  errors: [InvalidEmailErrorObject!]!
}

type Mutation {
  createAssessmentMagicLink(title: String!, jobPostLink: String, role: String!, companyPrettyId: String!, chatbotSkills: [String!], assignmentSkills: [MagicLinkSkillInput!]!, claimAllowed: Boolean): Boolean
  deleteAssessmentMagicLink(publicId: String!): Boolean
  editAssessmentMagicLink(publicId: String!, jobPostLink: String, title: String!): AssessmentMagicLink!
  startAssessmentMagicLink(publicId: String!): AssessmentMagicLink
  allowAssignmentForCandidate(publicId: String!, candidateId: String!): UserAssessment # validate that user has passed chatbots and send/resend an email
  setAssessmentMagicLinkActive(publicId: String!, active: Boolean!): AssessmentMagicLink! # update the active parameter of the assessmentMagicLink
  batchInviteCandidates(publicId: String!, inviteEmails: [String!]!): BatchInviteResponse! @loginCheck
  markAssignmentsViewed(assessmentId: String!): Boolean @loginCheck
  # new_mutation (Do not remove this line)
}

type ClaimSummary {
  _id: String
  status: ClaimStatus
  expires_at: Int
  invitedVoteCount: Int
  completedVoteCount: Int
  votesThreshold: Int
  indorsedVotesCount: Int
  flaggedVotesCount: Int
}

type UserAssessment {
  _id: String!
  user: User!
  chatbotStatus: String!
  assignmentStatus: String!
  status: String! # [chatbotsPending, chatbotsPassed, assignmentAllowed, assignmentSubmitted, assignmentEvaluated]
  startedAt: Int!
  userChatbotStatus: [UserChatbotSkill]
  userAssignment: UserAssignment
  isUserAssignmentHidden: Boolean
  claim: ClaimSummary
}

type UserChatbotSkill {
  skill: Skill!
  validated: Boolean!
  validatedAt: Int
}
`;
