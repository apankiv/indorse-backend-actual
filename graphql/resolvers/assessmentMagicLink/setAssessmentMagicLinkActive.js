const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const setAssessmentMagicLinkActive = async (root, { publicId, active }, { req }) => {
    let magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    if (!magicLink) {
        errorUtils.throwError('Magic link not found', 404);
    }
    const company = await mongooseCompanyRepo.findOneById(magicLink.companyId);
    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);
    const criteria = { publicId };
    const dataToSave = {
        $set: {
            active,
        },
    };
    await mongooseAssessmentMagicLinkRepo.update(criteria, dataToSave);
    magicLink = await mongooseAssessmentMagicLinkRepo.findOne(criteria);
    const magicLinkToReturn = await assessmentMagicLinkUtils.getAssessmentMagicLink(magicLink);
    return magicLinkToReturn;
};

module.exports = setAssessmentMagicLinkActive;
