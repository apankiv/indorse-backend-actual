const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const assessmentMagicLinkUtils = require('../../../models/services/assessmentMagicLink/assessmentMagicLinkUtils');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const timestampService = require('../../../models/services/common/timestampService');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const { userAssessmentStatusUpdate, sendAutoAllowedAssignmentEmail } = require('../../../models/services/companies/notification');

async function sendUpdate(userAssessmentId, chatbotStatus, assignmentStatus) {
    if (assignmentStatus && assignmentStatus === userAssessmentStates.ASSIGNMENT_ALLOWED) {
        await sendAutoAllowedAssignmentEmail(userAssessmentId);
    }
    if (assignmentStatus && assignmentStatus === userAssessmentStates.ASSIGNMENTS_VIEWED) {
        await userAssessmentStatusUpdate(assignmentStatus, userAssessmentId);
    }
    if (chatbotStatus && chatbotStatus === userAssessmentStates.CHATBOTS_PASSED) {
        await userAssessmentStatusUpdate(chatbotStatus, userAssessmentId);
    }
}

const startAssessmentMagicLink = async (root, { publicId }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('Unable to identify the viewer', 403);
    }

    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneByPublicId(publicId);
    const isAutoAllowed = (magicLink || {}).autoAllowedAssignment === true;
    if (!magicLink) {
        errorUtils.throwError('Unable to find the magic link', 404);
    }
    if (magicLink.deleted) {
        errorUtils.throwError('This magic link was deleted by the admin', 404);
    }
    if (magicLink.active === false) {
        errorUtils.throwError('Magic Link is not active', 404);
    }

    const company = await mongooseCompanyRepo.findOneById(magicLink.companyId);
    if (!company) {
        errorUtils.throwError('Unable to find the company', 404);
    }

    const criteriaUserAssessment = {
        startedBy: req.user_id,
        assessment: magicLink._id,
    };
    const attemptedAssessment = await mongooseUserAssessmentRepo.findOne(criteriaUserAssessment);
    if (attemptedAssessment) {
        errorUtils.throwError('Already taken the assessment', 403);
    }

    const user = await mongooseUserRepo.findOne({ _id: req.user_id });
    const userSkills = (user.skills || []).map((skill) => {
        const transformedObject = {};
        transformedObject.skillId = skill.skill._id.toString();
        const quizbotValidations = skill.validations.filter(validation => validation && validation.type === 'quizbot');
        transformedObject.level = skill.level;
        if (quizbotValidations[0]) {
            transformedObject.id = quizbotValidations[0].id;
            transformedObject.chatbotScore = quizbotValidations[0].chatbotScore;
            transformedObject.validatedAt = quizbotValidations[0].validatedAt;
            transformedObject.validated = quizbotValidations[0].validated;
        } else {
            transformedObject.validated = false;
        }
        return transformedObject;
    });

    const chatbotSkills = (magicLink.chatbotSkills || []).map(elem => elem.toString());
    let validatedChatbotSkills;
    let chatbotStatus = null;
    let assignmentStatus = null;
    let status;

    // if chatbotSkills length zero -> chatbotStatus shuold be null AND status should be assignmentsViewed
    if (chatbotSkills.length === 0) {
        assignmentStatus = userAssessmentStates.ASSIGNMENTS_VIEWED;
        status = userAssessmentStates.ASSIGNMENTS_VIEWED;
    } else {
        const userSkillsAlreadyValidated = (userSkills || []).filter(skill => chatbotSkills.indexOf(skill.skillId) > -1 && skill.validated);
        validatedChatbotSkills = assessmentMagicLinkUtils.getUniqueAssessmentSkillsWithMaxScores(userSkillsAlreadyValidated);
        chatbotStatus = (validatedChatbotSkills.length === chatbotSkills.length) ? userAssessmentStates.CHATBOTS_PASSED : userAssessmentStates.CHATBOTS_PENDING;
        status = chatbotStatus;
        if (status === userAssessmentStates.CHATBOTS_PASSED && isAutoAllowed === true) {
            assignmentStatus = userAssessmentStates.ASSIGNMENT_ALLOWED;
            status = userAssessmentStates.ASSIGNMENT_ALLOWED;
        }
    }

    if (!validatedChatbotSkills) validatedChatbotSkills = [];

    const userAssessmentObject = {
        startedBy: req.user_id,
        startedAt: timestampService.createTimestamp(),
        assessment: magicLink._id,
        company: company._id,
        chatbotSkills,
        validatedChatbotSkills,
        status,
        autoAllowedAssignment: isAutoAllowed === true,
        // add chat bot skills and ways to track their progress
    };
    if (chatbotStatus) {
        userAssessmentObject.chatbotStatus = chatbotStatus;
    }
    if (assignmentStatus) {
        userAssessmentObject.assignmentStatus = assignmentStatus;
    }
    if (status === userAssessmentStates.ASSIGNMENTS_VIEWED) {
        // add assignment viewed at timestamp to saved userAssessment
        userAssessmentObject.assignmentsViewedAt = timestampService.createTimestamp();
    }
    const userAssessmentId = await mongooseUserAssessmentRepo.insert(userAssessmentObject);
    await sendUpdate(userAssessmentId, chatbotStatus, assignmentStatus);
    const magicLinkToReturn = await assessmentMagicLinkUtils.getAssessmentMagicLink(magicLink);
    return magicLinkToReturn;
};

module.exports = startAssessmentMagicLink;
