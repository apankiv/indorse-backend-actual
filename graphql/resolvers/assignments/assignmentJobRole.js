const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const assignmentJobRole = async (root, args, { req, res }) => {
    const assignment = root;

    let jobRole = null;
    const jobRoleId = assignment.jobRole;

    if (jobRoleId) {
        jobRole = await mongooseJobRoleRepo.findOneById(jobRoleId);

        if (!jobRole) {
            errorUtils.throwError('Assignment Job role not found', 404);
        }
    }

    return jobRole;
};

module.exports = assignmentJobRole;
