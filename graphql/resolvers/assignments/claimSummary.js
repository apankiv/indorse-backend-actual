const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongoVotesRepo = require('../../../models/services/mongo/mongoRepository')('votes');
const getClaimService = require('../../../models/services/claims/getClaimService');
const settings = require('../../../models/settings');

const claimSummary = async (root, args, { req, res }) => {
    const userAssignment = root;

    if (!userAssignment || !userAssignment.claim_id) return null;

    const claim = await mongooseClaimsRepo.findOneById(userAssignment.claim_id.toString());
    if (!claim) return null;

    const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());

    const result = {
        _id: claim._id,
        status: getClaimService.getClaimStatus(claim, votingRound),
        expires_at: votingRound ? votingRound.end_voting : null,
        invitedVoteCount: await mongoVotesRepo.countWithQuery({ claim_id: claim._id.toString() }),
        completedVoteCount: await mongoVotesRepo.countWithQuery({
            claim_id: claim._id.toString(),
            endorsed: { $exists: true },
            sc_vote_exists: true,
        }),
        votesThreshold: claim.aipLimit,
        indorsedVotesCount: await mongoVotesRepo.countWithQuery({
            claim_id: claim._id.toString(),
            endorsed: true,
            sc_vote_exists: true,
        }),
        flaggedVotesCount: await mongoVotesRepo.countWithQuery({
            claim_id: claim._id.toString(),
            endorsed: false,
            sc_vote_exists: true,
        }),
    };

    return result;
};

module.exports = claimSummary;
