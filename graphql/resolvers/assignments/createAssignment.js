const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

const createAssignment = async (root, { assignment }, { req, res }) => {
    const assignmentExists = await mongooseAssignmentRepo.findOne({
        title: assignment.title,
    });

    if (assignmentExists) {
        errorUtils.throwError('Assignment title must be unique!', 400);
    }

    const jobRole = await mongooseJobRoleRepo.findOneById(assignment.jobRoleId);
    if (!jobRole) {
        errorUtils.throwError('Job role does not exist', 404);
    }

    assignment.jobRole = assignment.jobRoleId;
    delete assignment.jobRoleId;

    if (!assignment.listingPosition) {
        assignment.listingPosition = 1000;
    }

    const assignmentId = await mongooseAssignmentRepo.insert(assignment);

    const result = await mongooseAssignmentRepo.findOneById(assignmentId);
    return result;
};

module.exports = createAssignment;
