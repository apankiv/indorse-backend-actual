const errorUtils = require('../../../models/services/error/errorUtils');
const skillUtils = require('../../../models/services/skills/skillUtils');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseAssessmentMagicLinkRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const timestampService = require('../../../models/services/common/timestampService');
const createAssignmentClaimService = require('../../../models/services/claims/createAssignmentClaimService');
const { states: userAssignmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssignment');
const userTaskHandler = require('../../../models/services/userTask/userTaskHandler');

async function confirmProperSkillForAssessment(assessmentId, skillInputs) {
    const assessment = await mongooseAssessmentMagicLinkRepo.findOneById(assessmentId);
    let { assignmentSkills } = assessment;
    assignmentSkills = assignmentSkills || [];
    assignmentSkills.forEach((elem, index) => {
        assignmentSkills[index].status = false;
        const skillMatched = skillInputs.filter(skillInput => skillInput.skillTag === elem.skillTag && elem.allowedSkillIds.indexOf(skillInput.skillId.toString()) > -1);
        if ((skillMatched || []).length) {
            assignmentSkills[index].status = true;
        }
    });
    const foundAssignmentSkillNotFullfilled = assignmentSkills.find(elem => (!elem.status));
    return !foundAssignmentSkillNotFullfilled;
}

const finishAssignment = async (
    root,
    { assignmentID, form },
    { req },
) => {
    if (!req.login) {
        errorUtils.throwError('Not logged in!', 403);
    }

    const assignment = await mongooseAssignmentRepo.findOneById(assignmentID);

    if (!assignment) {
        errorUtils.throwError("Assignment with this ID doesn't exist!", 404);
    }

    if (assignment.disabled) {
        errorUtils.throwError('This assignment is disabled!', 400);
    }

    const userAssignmentSubmittedButNotEvaluated = await mongooseUserAssignmentRepo.findOne({
        assignment_id: assignmentID,
        owner_id: req.user_id,
        status: userAssignmentStates.SUBMITTED,
    });

    if (userAssignmentSubmittedButNotEvaluated) {
        errorUtils.throwError('User assignment already submitted!', 400);
    }

    let userAssignment = await mongooseUserAssignmentRepo.findOne({
        assignment_id: assignmentID,
        owner_id: req.user_id,
        status: userAssignmentStates.STARTED,
    });

    if (!userAssignment) {
        errorUtils.throwError('User assignment does not exist!', 400);
    }

    const { proof, skills: skillInputs } = form;

    // Ensure all skills exist by input skill ids
    const skillIds = skillInputs.map(skillInput => skillInput.skillId.toString());
    const skills = await skillUtils.findExactSkillsByIds(skillIds);

    // Ensure skills have tags mentioned in inputs
    skills.forEach((skill) => {
        const skillInput = skillInputs.find(recievedSkillInput => recievedSkillInput.skillId === skill._id.toString());
        if (skill.tags.indexOf(skillInput.skillTag) < 0) {
            errorUtils.throwError(`Skill ${skill.name} is not a ${skillInput.skillTag} skill`);
        }
    });
    // validate assignment if it belogs to user assessment
    const { userAssessmentId: assessmentId } = form; // Here we are getting assessmentId
    let userAssessment;
    let companyId = null;
    if (assessmentId) {
        const criteria = {
            assessment: assessmentId,
            startedBy: req.user_id,
        };

        userAssessment = await mongooseUserAssessmentRepo.findOne(criteria);
        if (!userAssessment) {
            errorUtils.throwError('You have not attempted the assessment', 403);
        }

        const isSkillProper = await confirmProperSkillForAssessment(userAssessment.assessment, skillInputs);
        if (!isSkillProper) {
            errorUtils.throwError('Send proper skills for assessment', 403);
        }
        ({ company: companyId } = userAssessment); // destructuring without redelaration
    }

    const now = timestampService.createTimestamp();

    let updateObj = {
        proof,
        skills: skillInputs.map(skillInput => ({ skillTag: skillInput.skillTag, skill: skillInput.skillId })),
        finished_on: now,
    };

    userAssignment = await mongooseUserAssignmentRepo.update({ _id: userAssignment._id.toString() }, updateObj);

    const requestingUser = await mongooseUserRepo.findOne({ _id: req.user_id });

    const claimId = await createAssignmentClaimService.createClaim(
        userAssignment,
        requestingUser,
        companyId,
    );

    updateObj = { claim_id: claimId, status: userAssignmentStates.SUBMITTED };
    const userAssignmentId = userAssignment._id.toString();
    userAssignment = await mongooseUserAssignmentRepo.update({ _id: userAssignment._id.toString() }, updateObj);
    await userTaskHandler.updateUserTasksStatus(req.user_id);
    if (userAssessment) {
        // link assessment with user assignment
        const userAssessmentCriteria = {
            _id: userAssessment._id,
        };
        const dataToSet = {
            $set: {
                userAssignmentId,
                assignmentStatus: userAssessmentStates.ASSIGNMENT_SUBMITTED,
                status: userAssessmentStates.ASSIGNMENT_SUBMITTED,
            },
        };
        await mongooseUserAssessmentRepo.update(userAssessmentCriteria, dataToSet);
    }
    return userAssignment;
};

module.exports = finishAssignment;
