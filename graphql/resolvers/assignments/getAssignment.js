const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const getAssignment = async (root, {id}, { req, res }) => {

    let assignment = await mongooseAssignmentRepo.findOne({_id : id});

    if(!assignment) {
        errorUtils.throwError("Assignment not found!",404)
    }

    return assignment;
};

module.exports = getAssignment;