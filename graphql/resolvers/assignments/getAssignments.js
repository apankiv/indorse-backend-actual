const mongoAssignmentsRepo = require('../../../models/services/mongo/mongoRepository')('assignments'); //TODO
const safeObjects = require('../../../models/services/common/safeObjects');
const stringUtils = require('../../../models/services/blob/stringUtils');

const getAssignments = async (root, {pageNumber, pageSize, sort, search}, {req, res}) => {

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allAssCursor = await mongoAssignmentsRepo.findAllWithCursor({_id: {$exists: true}});
    let totalAssignments = await allAssCursor.count();

    let assCursor, matchingAsses;

    if (search) {
        let regex = stringUtils.createSafeRegexObject(search);

        assCursor = await mongoAssignmentsRepo.findAllWithCursor({
            $or: [
                {title: regex},
                {scope: regex},
                {skills: {$elemMatch: {name: regex}}}
            ], disabled: {$ne: true}
        });
    } else {
        assCursor = allAssCursor;
        matchingAsses = totalAssignments;
    }

    let sortObj = prepareSortObj(sort);
    let sortedAsses = await assCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    let assesToReturn = [];

    for (let ass of sortedAsses) {
        assesToReturn.push(ass);
    }

    return {
        assignments: assesToReturn,
        totalAssignments: totalAssignments,
        matchingAssignments: matchingAsses
    };
}

function prepareSortObj(sort) {
    let field = 'listingPosition';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }
    }
    return {[field]: order};
}


module.exports = getAssignments;