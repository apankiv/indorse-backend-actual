const mongooseSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRolesRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const getAssignmentsBySkill = async (root, { skill }, { req, res }) => {
    // Changes only to pass test
    // TODO: Figure out if this endpoint is required
    // TODO: Consider user's selected role
    const skillDoc = await mongooseSkillsRepo.findOne({
        name: skill.toLowerCase(),
    });
    if (!skillDoc) errorUtils.throwError('Skill not found', 404);

    const childSkills = await mongooseSkillsRepo.findAll({ parents: { $in: [skillDoc._id] } });
    let tags = skillDoc.tags || [];
    (childSkills || []).forEach((child) => {
        tags = tags.concat(child.tags || []);
    });

    if (!tags || tags.length <= 0) errorUtils.throwError('Skill or its children have no tags', 404);

    const jobRoles = await mongooseJobRolesRepo.findAll({ skillTags: { $in: skillDoc.tags } });
    if (!jobRoles || !jobRoles.length) errorUtils.throwError('Job roles not found', 404);

    const result = await mongooseAssignmentRepo.findAll({
        jobRole: { $in: jobRoles.map(role => role._id) },
    });

    return result;
};

module.exports = getAssignmentsBySkill;
