const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const viewerAssignments = async (root, args, { req, res }) => {
    const assignment = root;

    const result = await mongooseUserAssignmentRepo
        .findOneEntity({
            assignment_id: assignment._id,
            owner_id: req.user_id,
        })
        .sort({ started_on: 'desc' });

    // Schema does not allow null status
    return (result && result.status) ? result : null;
};

module.exports = viewerAssignments;
