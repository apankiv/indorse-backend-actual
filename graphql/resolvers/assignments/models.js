const { states: userAssignmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssignment');

module.exports = `
    type Skill {
    	_id: String
        name: String
		category: String
		iconUrl: String
		tags: [String!]
		parents: [Skill!]!
		disabledFromFrameworkListing: Boolean
		userHasSkill: Boolean!
    }

	type Assignment {
	    _id: String
	    title: String
	    duration: String
	    description: String
	    img_url: String
	    jobRole: JobRole
	    skills: [Skill] # Legacy
	    scope: String
	    features: [String],
		disabled : Boolean
		listingPosition: Int
		viewerAssignments: [UserAssignment]
		lastViewerAssignment: UserAssignment
	}

	enum UserAssignmentStatus {
		${userAssignmentStates.STARTED},
		${userAssignmentStates.SUBMITTED},
		${userAssignmentStates.EVALUATED}
	}
	
	type UserAssignment {
	    _id: String
	    assignment_id: String
	    started_on: Int
	    finished_on: Int
	    owner_id: String    
	    proof: String
		claim_id: String
		claim: AssignmentClaimSummary
		skills: [UserAssignmentSkill!]!
		status: UserAssignmentStatus!
	}

	enum ClaimStatus {
		claim_submitted
		claim_disapproved
		in_progress
		indorsed
		rejected
		no_consensus
		not_defined
	}

	type AssignmentClaimSummary {
        _id: String
        status: ClaimStatus
        expires_at: Int
        invitedVoteCount: Int
		completedVoteCount: Int
		votesThreshold: Int
		indorsedVotesCount: Int
		flaggedVotesCount: Int
    }

	type UserAssignmentSkill {
		skillTag: String!
		skill: Skill!
		level: String!
		validated: Boolean
	}
	
	input AssignmentInput {
	    title: String!
	    duration: String
		description: String
		jobRoleId: String!
	    scope: String
		features: [String!]!
		listingPosition: Int
	}
	
	type AssignmentsList {
		assignments: [Assignment]
		matchingAssignments: Int
		totalAssignments: Int!
	}

	input FinishAssignmentForm {
        proof: URL
		skills: [FinishAssignmentFormSkill]
		userAssessmentId: String # Optional, used for magic link assignment submission.
    }

    input FinishAssignmentFormSkill {
        skillTag: String!,
        skillId: String!
    }
	
	type Query {
	    getAssignments(pageNumber: Int, pageSize: Int, sort: String, search: String): AssignmentsList
	    getAssignmentsBySkill(skill : String): [Assignment] 
	    getAssignment(id: String): Assignment 
	}
	
	type Mutation {
		createAssignment(assignment: AssignmentInput!): Assignment @permissionCheck(roles: ["admin"])       
		updateAssignment(assignmentID: String!, assignment: AssignmentInput!): Assignment @permissionCheck(roles: ["admin"])    
		uploadAssignmentPic(assignmentID: String!, file: Upload!): Assignment @permissionCheck(roles: ["admin"])         
		toggleStatusAssignment(assignmentID: String!, disabled: Boolean): Assignment @permissionCheck(roles: ["admin"])
		startAssignment(assignmentID: String!, userAssessmentId: String): UserAssignment @loginCheck
	    finishAssignment(assignmentID: String!, form: FinishAssignmentForm!): UserAssignment 
	}
	`

