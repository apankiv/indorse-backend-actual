const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const timestampService = require('../../../models/services/common/timestampService');
const { states: userAssignmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssignment');

const startAssignment = async (root, { assignmentID, userAssessmentId }, { req }) => {
    const assignment = await mongooseAssignmentRepo.findOneById(assignmentID);

    if (!assignment) {
        errorUtils.throwError("Assignment with this ID doesn't exist!", 404);
    }

    if (assignment.disabled) {
        errorUtils.throwError('This assignment is disabled!', 400);
    }

    const userAssignmentExists = await mongooseUserAssignmentRepo.findOne({
        assignment_id: assignmentID,
        owner_id: req.user_id,
        status: { $in: [userAssignmentStates.STARTED, userAssignmentStates.SUBMITTED] },
    });

    if (userAssignmentExists) {
        errorUtils.throwError('User assignment already exists!', 400);
    }

    const now = timestampService.createTimestamp();

    const userAsssignment = {
        assignment_id: assignmentID,
        started_on: now,
        owner_id: req.user_id,
        status: userAssignmentStates.STARTED,
    };

    if (userAssessmentId) {
        const criteria = {
            assessment: userAssessmentId,
            startedBy: req.user_id,
        };

        const userAssessment = await mongooseUserAssessmentRepo.findOne(criteria);
        if (!userAssessment) {
            errorUtils.throwError('You have not attempted the assessment', 403);
        }
        userAsssignment.startedForUserAssessment = userAssessment._id;
    }

    const userAssignmentId = await mongooseUserAssignmentRepo.insert(userAsssignment);

    const newUserAssignment = await mongooseUserAssignmentRepo.findOneById(userAssignmentId);

    return newUserAssignment;
};

module.exports = startAssignment;
