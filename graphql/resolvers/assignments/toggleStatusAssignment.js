const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');

const toggleStatusAssignment = async (root, {assignmentID, disabled}, { req, res }) => {

    let assignmentExists = await mongooseAssignmentRepo.findOne({_id : assignmentID});

    if(!assignmentExists) {
        errorUtils.throwError("Assignment does not exist!",404);
    }

    await mongooseAssignmentRepo.update({_id : assignmentExists._id},{$set : {disabled : disabled}});

    return await mongooseAssignmentRepo.findOne({_id: assignmentExists._id});
};

module.exports = toggleStatusAssignment;