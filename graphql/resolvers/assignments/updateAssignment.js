const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

const updateAssignment = async (root, { assignmentID, assignment }, { req, res }) => {
    const assignmentFromMongo = await mongooseAssignmentRepo.findOneById(assignmentID);

    if (!assignmentFromMongo) {
        errorUtils.throwError('Assignment does not exist!', 404);
    }

    if (!assignmentFromMongo.jobRole || assignmentFromMongo.jobRole.toString() === assignment.jobRoleId.toString()) {
        const jobRole = await mongooseJobRoleRepo.findOneById(assignment.jobRoleId);

        if (!jobRole) {
            errorUtils.throwError('Job role does not exist', 404);
        }

        assignment.jobRole = assignment.jobRoleId;
        delete assignment.jobRoleId;
    } else {
        errorUtils.throwError('Cannot change job role of this assignment', 404);
    }

    if (!assignment.listingPosition) {
        assignment.listingPosition = 1000;
    }

    const assignmentToReturn = await mongooseAssignmentRepo.update({ _id: assignmentFromMongo._id }, { $set: assignment });

    return assignmentToReturn;
};

module.exports = updateAssignment;
