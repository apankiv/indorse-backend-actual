const s3Service = require('../../../models/services/blob/s3Service');
const errorUtils = require('../../../models/services/error/errorUtils');
const config = require('config');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const timestampService = require('../../../models/services/common/timestampService');
const fs = require('fs');


const uploadLinkedinArchive = async (root, {assignmentID, file}, {req, res}) => {
    const {stream, filename, mimetype, encoding} = await file;
    let pathToSave = req.user_id + "-" + timestampService.createTimestamp();

    let ass = await mongooseAssignmentRepo.findOneById(assignmentID);

    if(!ass) {
        errorUtils.throwError("Assignment not found!",404)
    }

    await storeFS({
        stream,
        filename: pathToSave
    });

    let data = fs.readFileSync(pathToSave);

    await s3Service.uploadFile(assignmentID, new Buffer(data));
    await fs.unlink(pathToSave);

    await mongooseAssignmentRepo.update({_id: assignmentID}, {img_url: "https://" + config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + assignmentID});

    return await mongooseAssignmentRepo.findOneById(assignmentID);
};

const storeFS = ({stream, filename}) => {
    return new Promise((resolve, reject) =>
        stream
            .on('error', error => {
                if (stream.truncated)
                // Delete the truncated file
                    fs.unlinkSync(filename);
                reject(error)
            })
            .pipe(fs.createWriteStream(filename))
            .on('error', error => reject(error))
            .on('finish', () => resolve({filename}))
    )
};

module.exports = uploadLinkedinArchive;