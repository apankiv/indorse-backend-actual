const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const userAssignmentSkillDetails = async (root, args, { req, res }) => {
    const userAssignmentSkill = root;

    const { skill: skillId } = userAssignmentSkill;

    const skill = await mongooseSkillRepo.findOneById(skillId);

    return skill;
};

module.exports = userAssignmentSkillDetails;
