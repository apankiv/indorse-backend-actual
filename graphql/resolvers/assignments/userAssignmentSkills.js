const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const userAssignmentSkills = async (root, args, { req, res }) => {
    const userAssignment = root;

    return userAssignment.skills;
};

module.exports = userAssignmentSkills;
