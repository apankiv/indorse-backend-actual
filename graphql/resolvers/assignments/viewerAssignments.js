const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const viewerAssignments = async (root, args, { req, res }) => {
    const assignment = root;

    const result = await mongooseUserAssignmentRepo.findAll({
        assignment_id: assignment._id,
        owner_id: req.user_id,
    });

    return result;
};

module.exports = viewerAssignments;
