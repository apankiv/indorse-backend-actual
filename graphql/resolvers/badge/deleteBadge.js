const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseBadgeRepo = require('../../../models/services/mongo/mongoose/mongooseBadgeRepo');

const deleteBadge = async (root, {id}, { req, res }) => {

    await mongooseBadgeRepo.deleteOne({id: id});

    return id;

}

module.exports = deleteBadge;