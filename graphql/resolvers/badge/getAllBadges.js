const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const badgeUtils = require('../../../models/services/badge/badgeUtils');
const stringUtils = require('../../../models/services/blob/stringUtils')

const getAllBadges = async (root, {fields, pageNumber, pageSize, sort, search} , { req, res }) => {


    let searchParam, safePageNumber, safePageSize, sortObj, safeFields;

    if (fields) {
        safeFields = safeObjects.sanitize(fields).split(',');
    }


    if (search) {
        searchParam = safeObjects.sanitize(search);
    }

    if (pageNumber) {
        safePageNumber = safeObjects.sanitize(pageNumber);
    } else {
        safePageNumber = 1;
    }

    if (pageSize) {
        safePageSize = safeObjects.sanitize(pageSize);
    } else {
        safePageSize = 100;
    }

    let skip = (parseInt(safePageNumber) - 1) * parseInt(safePageSize);
    let limit = parseInt(safePageSize);

    if (sort) {
        let sortParamEnum = badgeUtils.sortParams;

        switch (sort) {
            case sortParamEnum.ascending.badgeName:
                sortObj = {
                    "name": 1
                };
                break;
            case sortParamEnum.ascending.prettyId:
                sortObj = {
                    "id": 1
                };
                break;
            case sortParamEnum.descending.badgeName:
                sortObj = {
                    "name": -1
                };
                break;
            case sortParamEnum.descending.prettyId:
                sortObj = {
                    "id": -1
                };
                break;
        }
    }

/*
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }
*/
    let badgeCursor, matchingBadges;

    if (searchParam) {
        badgeCursor = await mongoBadgeRepo.findAllWithCursor(
            {
                $or: [{id: stringUtils.createSafeRegexObject(searchParam)},
                    {name: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingBadges = await badgeCursor.count();
    } else {
        badgeCursor = await mongoBadgeRepo.findAllWithCursor({id: {$exists: true}});
    }

    let sortedBadges = await badgeCursor.sort(sortObj).skip(skip).limit(limit).toArray();


    let badgesToReturn = sortedBadges.map((badge) => {
        if (safeFields) {
            let filteredBadge = {};
            safeFields.forEach(field => {
                if (badge[field]) {
                    filteredBadge[field] = badge[field];
                }
            });
            return filteredBadge;
        } else {
            return badge;
        }
    });

    let badgeProfileCursor = await mongoBadgeRepo.findAllWithCursor({id: {$exists: true}});

    let responseObj = {
        badges: badgesToReturn,
        totalBadges: await badgeProfileCursor.count()
    };

    if (matchingBadges) {
        responseObj.matchingBadges = matchingBadges;
    }

    return responseObj;
}



module.exports = getAllBadges;