const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const badgeUtils = require('../../../models/services/badge/badgeUtils');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');

const getBadgeUsers = async (root, {id, fields, pageNumber, pageSize, sort, search}, { req, res }) => {

    let searchParam, safePageNumber, safePageSize, sortObj, safeFields;

    let pretty_id = safeObjects.sanitize(id);

    if (fields) {
        safeFields = safeObjects.sanitize(fields).split(',');
    }


    if (search) {
        searchParam = safeObjects.sanitize(search);
    }

    if (pageNumber) {
        safePageNumber = safeObjects.sanitize(pageNumber);
    } else {
        safePageNumber = 1;
    }

    if (pageSize) {
        safePageSize = safeObjects.sanitize(pageSize);
    } else {
        safePageSize = 100;
    }

    let skip = (parseInt(safePageNumber) - 1) * parseInt(safePageSize);
    let limit = parseInt(safePageSize);

    if (sort) {
        let sortParam = safeObjects.sanitize(sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }

    let userCursor, matchingUsers;

    if (searchParam) {
        userCursor = await mongoUserRepo.findAllWithCursor(
            {
                badges : pretty_id,
                $or: [{username: stringUtils.createSafeRegexObject(searchParam)},
                    {name: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingUsers = await userCursor.count();
    } else {
        userCursor = await mongoUserRepo.findAllWithCursor({badges: pretty_id});
    }

    let sortedUsers = await userCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    fields = ['img_url','bio','name','username','_id']; //hardcoding user fields to a limited set for now
    let usersToReturn = sortedUsers.map((user) => {
        if (fields) {
            let filteredUser = {};
            fields.forEach(field => {
                if (user[field]) {
                    filteredUser[field] = user[field];
                }
            });
            return filteredUser;
        } else {
            return user;
        }
    });

    let userProfileCursor = await mongoUserRepo.findAllWithCursor({badges: pretty_id});

    let responseObj = {
        users: usersToReturn,
        totalUsers: await userProfileCursor.count()
    };

    if (matchingUsers) {
        responseObj.matchingUsers = matchingUsers;
    }

    return responseObj;
}


module.exports = getBadgeUsers;