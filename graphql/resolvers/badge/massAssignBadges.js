const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseBadgeRepo = require('../../../models/services/mongo/mongoose/mongooseBadgeRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const routeUtils = require('../../../models/services/common/routeUtils');
const validate = require('../../../models/services/common/validate');
const authChecks = require('../../../models/services/auth/authChecks');
const roles = require('../../../models/services/auth/roles');
const pubsub = require('../../subscriptions/pubsub').pubsub;
const pubsubNative = require('amqp-pubsub');
const amqplib = require('amqplib');
const config = require('config');
const settings = require('../../../models/settings');
const rabbitMqPublish = require('../../../models/services/rabbitMq/publishToExchange');

/*
 ids = an array of badge ID
 emails = an array of users email
 assignType =
 ASSIGN: assign badges
 REMOVE: remove badges
 */
const massAssignBadge = async (root, {badgeIds, assignType, emails}, {req, res}) => {
    for (let email of emails) {
        let user = await mongooseUserRepo.findOneByEmail(email);
        if (!user) {
            errorUtils.throwError("User not found: " + email, 404);
        }
    }
    let badgeArray = [];
    for (let badgeId of badgeIds) {
        let badge = await mongooseBadgeRepo.findOneByPrettyId(badgeId);

        if (!badge) {
            errorUtils.throwError("Badge not found: " + badgeId, 404);
        }
        badgeArray.push(badge);
    }
    


    for (let email of emails) {
        if (assignType === "ASSIGN") {
            await mongooseUserRepo.update({email: email}, {$addToSet: {badges: {$each: badgeIds}}});
            for (let badge of badgeArray) {
                let message = {
                    email: email,
                    assignType: assignType,
                    badge: badge
                }

                await rabbitMqPublish.publishToExchange('Badge.Assigned.DLQ.Exchange', { massAssignBadgesSubscription: message });
            }
        } else if (assignType === "REMOVE") {
            await mongooseUserRepo.update({email: email}, {$pullAll: {badges: badgeIds}});
            for (let badge of badgeArray) {
                let message = {
                    email: email,
                    assignType: assignType,
                    badge: badge
                }

                await rabbitMqPublish.publishToExchange('Badge.Assigned.DLQ.Exchange', { massAssignBadgesSubscription: message });

            }
        }
    }

    return true;
}


module.exports = massAssignBadge;