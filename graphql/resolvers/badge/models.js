const badgeUtils = require('../../../models/services/badge/badgeUtils');

module.exports = `
	type Badge {
	    id: String!
	    name: String
	    type: String
	    description: String
	    logo_data: String
	    issuer: Issuer
	    criteria: Criteria
	    tags: [String]
	    extensions_cta_text: ExtensionsText
	    extensions_cta_link: ExtensionsLink
	    created_at_timestamp: String
	    last_updated_timestamp: String 
	    image: Image
	    chatbot_uuid: String
	}
	
	type AllBadge {
		badges: [Badge]
		totalBadges: Int
	}
	
	type Image {
		icon_s3: String
		icon_ipfs: String
	}

	
	type Criteria {
		narrative: String
	}
	
	type Issuer {
		id: String
		type: String
		name: String
		url: URL
		email: EmailAddress
		verification: Verification
	}
	
	type Verification {
		allowedOrigins: String
	}
	
	type ExtensionsText {
		context: String
		type: String
		text: String
	}	
	
	type ExtensionsLink {
		context: String
		type: String
		link: String
	}
	
	type AllBadgeUsersResp {
		users : [User]
		totalUsers: Int!
	}
	
	enum SortParam {
		  ${badgeUtils.sortParams.ascending.prettyId}
		  ${badgeUtils.sortParams.ascending.badgeName}
		  ${badgeUtils.sortParams.descending.prettyId}
		  ${badgeUtils.sortParams.descending.badgeName}
	}
	
	enum AssignType {
		ASSIGN
		REMOVE
	}
	
	
	type Query {
		badge(id: String!): Badge
		allBadges(fields: String, pageNumber: Int, pageSize: Int, sort: SortParam, search: String): AllBadge @permissionCheck(roles: ["admin"])
		allBadgeUsers(id: String, fields: String, pageNumber: Int, pageSize: Int, sort: SortParam, search: String): AllBadgeUsersResp @permissionCheck(roles: ["admin"])
		# new_query (Do not remove this line)
	}
	
	type Mutation {
		createBadge(id: String! 
		            name: String!
		            description: String!
		            logo_data: String!
		            extensions_cta_text: String!
		            extensions_cta_link: String!
		            chatbot_uuid: String!): Badge @permissionCheck(roles: ["admin"])
		            
		updateBadge(id: String!
		 			name: String
		 			description: String
		 			logo_data: String
		 			extensions_cta_text: String
		 			extensions_cta_link: String
		 			chatbot_uuid: String): Badge @permissionCheck(roles: ["admin"])
		 			
		deleteBadge(id: String!): String @permissionCheck(roles: ["admin"])
		
		massAssignBadges(badgeIds: [String]!
		 				assignType: AssignType
		 				emails: [EmailAddress]!): Boolean @permissionCheck(roles: ["admin"])
		# new_mutation (Do not remove this line)            
	}
	
	type MassAssignedSubscribe {
		badge: Badge
		email: EmailAddress
		assignType: AssignType
	}
	
	type CompanyCreatedTx {
		username: String
		companyName: String
		status: Boolean
	}
	
	`


