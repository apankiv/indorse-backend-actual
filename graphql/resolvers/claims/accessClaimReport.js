const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoose = require('mongoose');
const claimUtils = require('../../../models/services/claims/claimUtils');
const emailService = require('../../../models/services/common/emailService');

const accessClaimReport = async (root, { claimId }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User should be logged in !', 403);
    }

    if (!mongoose.Types.ObjectId.isValid(claimId)) {
        errorUtils.throwError('Claim Id passed in not valid !', 403);
    }
    const criteria = { _id: claimId };
    const dataToSet = { $set: { canOwnerViewFeedback: true } };
    await mongooseClaimsRepo.update(criteria, dataToSet);
    const updatedClaim = await mongooseClaimsRepo.findOneById(claimId);
    if (!updatedClaim) {
        errorUtils.throwError("Oops, this claim doesn't exist anymore", 404);
    }
    const user = await mongooseUserRepo.findOne({ _id: req.user_id });
    // send email to admin and users here.
    await emailService.sendCandidateWillingToPayEmail(user.name, claimId);
    const objectToReturn = await claimUtils.prepareClaimReportAccessObject(updatedClaim);
    return objectToReturn;
};

module.exports = accessClaimReport;
