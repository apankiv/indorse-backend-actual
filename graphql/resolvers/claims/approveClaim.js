const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const emailService = require('../../../models/services/common/emailService');
const queueClaimApprovalService = require('../../../models/claims/queueClaimApprovalService');
const randomValidatorService = require('../../../models/claims/randomValidatorService');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const authChecks = require('../../../models/services/auth/authChecks');
const roles = require('../../../models/services/auth/roles');
const routeUtils = require('../../../models/services/common/routeUtils');
const claims = require('../../../models/services/mongo/mongoose/schemas/claims');
const {Validator} = require('express-json-validator-middleware');
const {creditType} = require('../../../models/services/mongo/mongoose/schemas/company');
const {tiers} = require('../../../models/services/mongo/mongoose/schemas/validator')
const validator = new Validator({allErrors: true});
const {validate} = validator;

const approveClaim = async (root, {claimId, tiers, force}, {req, res}) => {

    let claim = await mongooseClaimsRepo.findOneById(claimId);

    if (!claim) {
        errorUtils.throwError('Claim not found!', 400);
    }

    if (claim.approved || claim.disapproved) {
        errorUtils.throwError('Claim already classified!', 400);
    }

    claim.validatorAssignedBytier = tiers;

    await randomValidatorService.getValidatorsByClaim(claim);

    // refactor after the ID-1630 is merged
    let userAssessmentCompany, limit, used, limitReachedTimestamp;
    if (claim.type === claims.types.ASSIGNMENT) {
        let userAssessment = await mongooseUserAssessmentRepo.findOne({userAssignmentId: claim.user_assignment_id});
        // This following if statement code block is to control magic link user assignment limit.
        if (userAssessment) {
            let associatedCompany = await mongooseCompanyRepo.findOneById(userAssessment.company);
            if (associatedCompany && associatedCompany.credits && associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT]) {
                limit = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].limit;
                used = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].used;
                limitReachedTimestamp = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].limitReachedTimestamp;
                if (limit - used <= 0) {
                    if (!force) {
                        let warningObj = {
                            approveResult: 'creditLow',
                            companyName: associatedCompany.pretty_id
                        };
                        return warningObj;
                    }
                    await emailService.sendAdminCompanyBalanceLow(associatedCompany.pretty_id, limit - used - 1);
                }
            }
            used = used + 1;
            userAssessmentCompany = userAssessment.company;
        }
    } else if (claim.userAssessmentId) {
        let userAssessment = await mongooseUserAssessmentRepo.findOneById(claim.userAssessmentId);
        if (userAssessment) {
            let associatedCompany = await mongooseCompanyRepo.findOneById(userAssessment.company);
            if (associatedCompany && associatedCompany.credits && associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT]) {
                limit = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].limit;
                used = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].used;
                limitReachedTimestamp = associatedCompany.credits[creditType.ASSESSMENT_ASSIGNMENT].limitReachedTimestamp;
                if (limit - used <= 0) {
                    if (!force) {
                        let warningObj = {
                            approveResult: 'creditLow',
                            companyName: associatedCompany.pretty_id
                        };
                        return warningObj;
                    }
                    await emailService.sendAdminCompanyBalanceLow(associatedCompany.pretty_id, limit - used - 1);
                }
            }
            used = used + 1;
            userAssessmentCompany = userAssessment.company;
        }
    }

    await mongooseClaimsRepo.approveClaim(claimId, req.username, tiers);
    claim = await mongooseClaimsRepo.findOneById(claimId);

    if (userAssessmentCompany) {
        if (limit === used) { // inclusively show (in other words, this claim will be shown)
            limitReachedTimestamp = claim.approved;
        }
        const creditLimit = {
            $set: {
                credits: {
                    [creditType.ASSESSMENT_ASSIGNMENT]: {
                        limit,
                        used,
                        limitReachedTimestamp,
                    },
                },
            },
        };
        await mongooseCompanyRepo.update({_id: userAssessmentCompany}, creditLimit);
    }

    let successResp = {
        approveResult: 'success'
    };

    await queueClaimApprovalService.queueClaimApproval(claim);

    return successResp;
};

module.exports = approveClaim;


