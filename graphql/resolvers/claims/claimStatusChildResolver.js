const { getClaimStatus } = require('../../../models/services/claims/getClaimService');

const claimStatusChildResolver = async (root, args, { req }) => {
    const claim = root;
    if (!claim) return null;
    return await getClaimStatus(claim);
};

module.exports = claimStatusChildResolver;
