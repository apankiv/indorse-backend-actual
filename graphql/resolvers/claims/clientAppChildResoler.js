const mongooseClientAppRepo = require('../../../models/services/mongo/mongoose/mongooseClientAppRepo');
const { companyPermissionCheck } = require('../../../models/services/auth/authChecks');

const clientAppChildResolver = async (root, args, { req }) => {
    const claim = root;
    if (!claim) return null;
    const { clientApp: clientAppData } = claim;
    const clientAppDB = await mongooseClientAppRepo.findOneByClientId(clientAppData.clientId);
    clientAppData.client = clientAppDB;
    const allowed = await companyPermissionCheck(req, clientAppDB.company, ['partnerClaims.read'], false);
    if (!allowed) {
        return null;
    }
    return clientAppData;
};

module.exports = clientAppChildResolver;
