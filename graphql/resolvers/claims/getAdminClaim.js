const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const emailService = require('../../../models/services/common/emailService');
const queueClaimApprovalService = require('../../../models/claims/queueClaimApprovalService');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const authChecks = require('../../../models/services/auth/authChecks');
const roles = require('../../../models/services/auth/roles');
const routeUtils = require('../../../models/services/common/routeUtils');
const claims = require('../../../models/services/mongo/mongoose/schemas/claims');
const { Validator } = require('express-json-validator-middleware');
const { creditType } = require('../../../models/services/mongo/mongoose/schemas/company');
const {tiers} = require('../../../models/services/mongo/mongoose/schemas/validator')
const validator = new Validator({ allErrors: true });
const { validate } = validator;
const claimUtils = require('../../../models/services/claims/claimUtils');
const rewardService = require('../../../models/claims/rewardService');

const getAdminClaim = async (root, { claimId }, { req, res }) => {

    let claim = await mongooseClaimsRepo.findOneById(claimId);

    if (!claim) {
        errorUtils.throwError('Claim not found!', 400);
    }

    const { company: companyId } = claim;
    const source = await claimUtils.getClaimSource(claim);
    const validatorCount = await claimUtils.getValidatorsCount({ source, companyId });

    let maxValidator = validatorCount;
    let minValidator = 0;

    console.log(tiers);

    let allTiers = tiers; //if not object key loop

    let tierValidatorDetails = [];
    tierValidatorDetails.push(rewardService.rewardEstimationPerTier(tiers.TIER1, validatorCount));
    tierValidatorDetails.push(rewardService.rewardEstimationPerTier(tiers.TIER2, validatorCount));
    tierValidatorDetails.push(rewardService.rewardEstimationPerTier(tiers.TIER3, validatorCount));

    let returnObj = {
        noOfValidator: validatorCount,
        tierValidatorDetails: tierValidatorDetails
    };

    return returnObj;


};

module.exports = getAdminClaim;


