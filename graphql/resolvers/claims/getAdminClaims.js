const mongoClaimsRepo = require('../../../models/services/mongo/mongoRepository')('claims');
const safeObjects = require('../../../models/services/common/safeObjects');
const AdminVotesModel = require('../../../models/admin/votes');
const AdminClaimsModel = require('../../../models/admin/claims');
const AdminUsersModel = require('../../../models/admin/users');
const getClaimTierStatusService = require('../../../models/claims/getClaimTierStatusService');

class AdminClaims {
    static async get(
        root,
        {
            pageNumber, pageSize, sortParam, sortDir, filters,
        },
    ) {
        const {
            stage,
            source,
            search: searchStr,
        } = filters || {};

        const {
            sntzSearchStr, sntzPageNumber, sntzPageSize, sntzSortParam, sntzSortDir,
        }
            = AdminClaims.getSanitizedParams(searchStr, pageNumber, pageSize, sortParam, sortDir);
        const { skip, limit } = AdminClaims.getSkipLimit(sntzPageNumber, sntzPageSize);
        const { claims: matchingClaims, count: matchingClaimsCount } = await AdminClaimsModel.get({
            searchStr: sntzSearchStr,
            sortParam: sntzSortParam,
            sortDir: sntzSortDir,
            skip,
            limit,
            stage,
            source,
        });
        const [numAllClaims, votes, users, claimsStatuses] = await Promise.all([
            mongoClaimsRepo.count(),
            AdminVotesModel.get(matchingClaims.map(claim => claim._id.toString())),
            AdminUsersModel.get(matchingClaims.map(claim => claim.ownerid)),
            AdminClaimsModel.getStatuses(matchingClaims),
        ]);
        AdminClaims.fillFieldsInClaims(matchingClaims, votes, users, claimsStatuses);
        return {
            success: true,
            totalClaims: numAllClaims,
            matchingClaims: matchingClaimsCount,
            claims: matchingClaims,
        };
    }

    static fillFieldsInClaims(claims, votes, users, claimsStatuses) {
        claims.forEach((matchingClaim, index) => {
            const claim = claims[index];
            claim.votes = votes[claim._id.toString()];
            if (claim.ownerid && users[claim.ownerid]) {
                claim.ownername = users[claim.ownerid].name;
            }
            if (typeof claim.final_status === 'undefined') {
                console.log(
                    `Final status not present for claim id = ${claim._id.toString()}`,
                    typeof claim.final_status,
                );
                claim.final_status = 'ongoing';
            }
            if (claimsStatuses[claim._id.toString()]) {
                claim.status = claimsStatuses[claim._id.toString()];
            }
            claim.createdAt = claim._id.getTimestamp().getTime() / 1000;
        });
    }

    static getSanitizedParams(searchStr, pageNumber, pageSize, sortParam, sortDir) {
        let sntzSearchStr = null;
        if (searchStr) {
            sntzSearchStr = safeObjects.sanitize(searchStr);
        }
        let sntzSortParam = 'title';
        let sntzSortDir = 'asc';
        if (sortParam) {
            sntzSortParam = sortParam;
            if (['asc', 'desc'].indexOf(sortDir) !== -1) {
                sntzSortDir = sortDir;
            }
        }
        let sntzPageNumber = safeObjects.sanitize(pageNumber);
        let sntzPageSize = safeObjects.sanitize(pageSize);

        if (!sntzPageNumber) {
            sntzPageNumber = 1;
        }
        if (!sntzPageSize) {
            sntzPageSize = 10;
        }
        return {
            sntzSearchStr, sntzPageNumber, sntzPageSize, sntzSortParam, sntzSortDir,
        };
    }

    static getSkipLimit(pageNumber, pageSize) {
        return {
            skip: (parseInt(pageNumber, 10) - 1) * parseInt(pageSize, 10),
            limit: parseInt(pageSize, 10),
        };
    }
}

module.exports = AdminClaims.get;
