const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const claimUtils = require('../../../models/services/claims/claimUtils');
const getClaimTierStatusService = require('../../../models/claims/getClaimTierStatusService');

const getClaimById = async (root, { claimId }, { req, res }) => {
    const claim = await mongooseClaimsRepo.findOneById(claimId);
    if (!claim) {
        errorUtils.throwError("Oops, this claim doesn't exist anymore", 404);
    }

    const claimToReturn = await claimUtils.prepareClaimObject(claim);

    claimToReturn.tiers = await getClaimTierStatusService.getClaimTierStatus(claim);

    return claimToReturn;
};

module.exports = getClaimById;
