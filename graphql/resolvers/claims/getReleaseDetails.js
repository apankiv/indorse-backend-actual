const mongooseClaimQueueRepo = require('../../../models/services/mongo/mongoose/mongooseClaimQueueRepo');
const settings = require('../../../models/settings');
const CronJob = require('cron').CronJob;

const reopenClaim = async (root, {}, {req, res}) => {

    let claimReleaseCron = new CronJob(settings.CLAIMS_QUEUE_RELEASE_TICK, async () => {}, null, true, 'GMT');
    let trigger = claimReleaseCron.nextDates();
    claimReleaseCron.stop();

    let claimsInQueue = await mongooseClaimQueueRepo.findAll({released : {$ne : true}});

    return {
        nextDate : trigger.unix(),
        cronString : settings.CLAIMS_QUEUE_RELEASE_TICK,
        queueSize : claimsInQueue.length
    };
};

module.exports = reopenClaim;