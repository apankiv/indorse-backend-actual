const { claimSource, claimStage, claimReportAccessStatus } = require('../../../models/services/mongo/mongoose/schemas/claims');
const {tiers} = require('../../../models/services/mongo/mongoose/schemas/validator');

/* eslint-disable */
module.exports = `

  type TierInfo {
    maxVote : Int,
    numTentativeVotes : Int
  }
  
  type TiersInfo {
    _1 : TierInfo
    _2 : TierInfo
    _3 : TierInfo
  }

  type Claim {
    tiers : TierInfo,
    ownerName : String,
    _id: String,
    title: String,
    type: String,
    desc: String,
    proof: String,
    state: String,
    level: String,
    visible: Boolean,
    approved: Int,
    released_at: Int,
    disapproved: Int,
    ownerid: String,
    endorse_count: Int,
    flag_count: Int,
    final_status: Boolean,
    sc_claim_id: Int,
    claim_expiry: Int,
    is_sc_claim: Boolean,
    source: String,
    created_at: Int,
    rewarded_at : Int,
    company: Company,
    claimStatus: ClaimStatus,
    clientApp: clientAppInfo,
    normalizedRating: Int,
    claimSummary: ClaimSummary,
    skills: [Skill!]!,
  }

  type clientAppInfo {
    client: ClientApp
    userId: String!
  }

  type ClaimReleaseDetails {
    nextDate: Int!
    cronString: String!
    queueSize: Int!
	}
	
  enum AdminClaimSortParam {
    title,
    owner,
    created_at,
  }

  enum SortDir {
    asc,
    desc
  }

  enum Stage {
    ${claimStage.APPROVED},
    ${claimStage.DISAPPROVED},
    ${claimStage.PENDING_APPROVAL}
  }

  enum Source {
    ${claimSource.COMPANY},
    ${claimSource.DIRECT}
  }

  input ClaimFilters {
    stage: Stage,
    source: Source,
    search: String
  }

  type VotesCounts {
    invited: Int!,
    registered: Int!,
    indorsed: Int!,
    flagged: Int!
  }

  type AdminClaim {
    _id : String!
    title: String!
    desc: String!
    type: String
    is_sc_claim: Boolean
    ownerid: String
    created_at: Int
    endorse_count: Int
    flag_count: Int
    submission_event_computed: Boolean
    merkelized: Boolean
    tags: [String]
    approved: Int
    approved_at: Int
    approved_by: Int
    sc_deadline: Int
    signature_ref: String
    released_at: Int
    final_status: String!
    in_consensus: Boolean
    can_be_reopened:Boolean
    votes: VotesCounts!
    ownername: String
    createdAt: Int!
    status: String
    noOfValidator: Int
    tierValidatorDetails: [TierValidatorDetails]
    normalizedRating: Int
  }

  type AdminClaims {
    success: Boolean
    totalClaims: Int
    matchingClaims: Int
    claims: [AdminClaim!]!
	}
	
	enum ApproveResult {
	    creditLow
	    success
	}
	
	type ApproveResp{
	    approveResult: ApproveResult
	    companyName: String
	}
	
	type TierValidatorDetails {
        tier: TierLevel
        minReward: Int
        maxReward: Int
     }
     
    input TierValidatorInput{
        tier: TierLevel!
        noOfValidator: Int!
    }

  enum AccessStatus {
    ${claimReportAccessStatus.ALLOWED}
    ${claimReportAccessStatus.NOT_ALLOWED}
  }

  type ClaimReportAccess {
    status: AccessStatus
  }

	type Query {
		getReleaseDetails(void: Boolean): ClaimReleaseDetails
    getClaimById(claimId: String): Claim!
		getAdminClaims(filters: ClaimFilters, pageNumber: Int, pageSize: Int, sortParam: AdminClaimSortParam, sortDir: SortDir): AdminClaims @permissionCheck(roles: ["admin"])
    getAdminClaim(claimId: String): AdminClaim @permissionCheck(roles: ["admin"])
	}
	
	type Mutation {
		payoutClaims(pay : Boolean!) : Boolean
		reopenClaim(claimId : String!) : Boolean @permissionCheck(roles: ["admin"])
		claims_setTags(claimId: String, tags: [String]) : Boolean @permissionCheck(roles: ["admin"])
		sendSurveyResult(claimId: String!, answer: String!) : Boolean
    approveClaim(claimId: String!, tiers: [TierValidatorInput!]!, force: Boolean): ApproveResp @permissionCheck(roles: ["admin"])
    accessClaimReport(claimId: String!): ClaimReportAccess! @loginCheck
	}
`;
