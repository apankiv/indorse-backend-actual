const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseRewardsRepo = require('../../../models/services/mongo/mongoose/mongooseRewardsRepo');
const emailService = require('../../../models/services/common/emailService');
const errorUtils = require('../../../models/services/error/errorUtils');
const Json2csvParser = require('json2csv').Parser;
const fields = ['address', 'reward'];
const opts = { fields, header : false};
const parser = new Json2csvParser(opts);
const timestampService = require('../../../models/services/common/timestampService')
const logger = require('../../../models/services/common/logger').getLogger();
const adminVoteOnCommentService = require('../../../models/votes/adminVoteOnCommentService');

const payoutClaims = async (root, {pay}, { req, res }) => {

   let user = await mongooseUserRepo.findOneByEmail(req.email);
   let payoutTimestamp = timestampService.createTimestamp();   

   if(!user.isSuperAdmin){
       errorUtils.throwError("Unauthorized",403);
   }

    let unpaidVotes = await mongooseVoteRepo.findAllWithCursor({
        $and: [
            {sc_vote_exists: true},
            {paid: {$ne: true}}]
    });

    let payoutsToMake = {};
    let indPayoutsToMake = {};

    for (let vote = await unpaidVotes.next(); vote != null; vote = await unpaidVotes.next()) {

        let voter = await mongooseUserRepo.findOne({_id: vote.voter_id});
        logger.debug("vote_id " + vote._id.toString() + " is marked as paid as true ");
        // check if vote should be invalidated

        let currentCount = payoutsToMake[voter.ethaddress];

        if (!currentCount) {
            if(!adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(vote)){
                payoutsToMake[voter.ethaddress] = (vote.reward ? vote.reward : 5);
                indPayoutsToMake[voter.ethaddress] = (vote.ind_reward ? vote.ind_reward : 0);
            }

        } else {
            if(!adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(vote)){
                payoutsToMake[voter.ethaddress] += (vote.reward ? vote.reward : 5);
                indPayoutsToMake[voter.ethaddress] += (vote.ind_reward ? vote.ind_reward : 0);
            }
        }

        if (pay) {
            await mongooseVoteRepo.markAsPaid({ _id: vote._id });
        }
    }

    //Add payout by reputation
    /* Disable payout values
    let reputationRewards = await mongooseRewardsRepo.findAll({paid:false})
    for(let rewardRep of reputationRewards){
        console.log("Reward reputation is " + JSON.stringify(rewardRep))
        let rewardedUser =await mongooseUserRepo.findOne({ _id: rewardRep.user_id})
        if (rewardedUser){
            if (payoutsToMake[rewardedUser.ethaddress]){
                payoutsToMake[rewardedUser.ethaddress] += rewardRep.reward
            } else{
                payoutsToMake[rewardedUser.ethaddress] = rewardRep.reward
            }
            
        }

        if(pay){
            await mongooseRewardsRepo.markAsPaid({_id: rewardRep._id},payoutTimestamp);
        }        
    }
    */

    let paymentsArray = [];
    let indPaymentsArray = [];

    Object.keys(payoutsToMake).forEach(function(key) {
        paymentsArray.push({address : key, reward : payoutsToMake[key]});
    });

    Object.keys(indPayoutsToMake).forEach(function(key) {
        indPaymentsArray.push({address : key, reward : indPayoutsToMake[key]});
    });

    const csv = parser.parse(paymentsArray);
    logger.debug("Payment array print out for log purpose " + paymentsArray);
    const indCsv = parser.parse(indPaymentsArray);

    let csvBase64 = Buffer.from(csv).toString('base64');
    let indCsvBase64 = Buffer.from(indCsv).toString('base64');
    
    if(pay){
        await emailService.sendSuperAdminPayoutCSV(csvBase64, indCsvBase64);
    }

    return true;
};

module.exports = payoutClaims;