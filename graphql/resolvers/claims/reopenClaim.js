const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const claimsEmailService = require('../../../models/services/common/claimsEmailService');
const errorUtils = require('../../../models/services/error/errorUtils');
const settings = require('../../../models/settings');
const timestampService = require('../../../models/services/common/timestampService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const claims = require('../../../models/services/mongo/mongoose/schemas/claims');

const reopenClaim = async (root, {claimId}, {req, res}) => {

    let claim = await mongooseClaimsRepo.findOneById(claimId);

    if (!claim) {
        errorUtils.throwError("Claim not found", 404);
    }

    if (!claim.can_be_reopened) {
        errorUtils.throwError("Claim cannot be reopened!", 400);
    }

    if (claim.sc_deadline - settings.CLAIM_VOTE_PERIOD < timestampService.createTimestamp()) {
        await mongooseClaimsRepo.update({_id: claimId}, {can_be_reopened: false});
        errorUtils.throwError("Claim cannot be reopened, smart contract deadline is too short!", 400);
    }

    let votingRound = await mongooseVotingRoundRepo.findOne({claim_id: claimId});

    let now = timestampService.createTimestamp();
    let votingDeadline = now + Number(settings.CLAIM_VOTE_PERIOD);

    await mongooseVotingRoundRepo.update({claim_id: claimId}, {
        number: votingRound.number + 1,
        notification_25_sent: false,
        notification_50_sent: false,
        notification_75_sent: false,
        status: "in_progress",
        end_voting: votingDeadline,
        end_registration: now
    });

    let votes = await mongooseVoteRepo.findAll({claim_id: claimId});

    for (let vote of votes) {
        if (vote.is_signature && !vote.voted_at) {
            let user = await mongooseUserRepo.findOneById(vote.voter_id);
            claimsEmailService.sendVoteInvitationEmail(user.name, claimId, user.email);
        }
    }

    await mongooseVoteRepo.updateMany({$and: [{voting_round_id: String(votingRound._id)},
            {sc_vote_exists: {$ne: true}}]}, {$set: {isVotingRoundOn: true}});

    await mongooseClaimsRepo.update({_id: claimId}, {
        $set: {state: claims.states.IN_PROGRESS, can_be_reopened: false},
        $unset: {final_status: ''}
    });

    amplitudeTracker.publishData('claim_reopened', claim, req);

    return true;
};

module.exports = reopenClaim;