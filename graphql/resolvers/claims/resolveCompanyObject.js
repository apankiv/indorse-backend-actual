const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
const authUtils = require('../../../models/services/auth/authChecks');

const resolveCompanyObject = async (root, args, { req }) => {
    const claim = root;
    if (!claim || !claim.company) return null;
    const criteria = {
        _id: claim.company,
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['partnerClaims.read', 'magicLink.read']);
    return getCompanyUtils.prepareCompanyObject(company);
};

module.exports = resolveCompanyObject;
