const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const sendSurveyResult = async (root, {claimId, answer}, {req, res}) => {

    let claim = await mongooseClaimsRepo.findOneById(claimId);

    if (!claim) {
        errorUtils.throwError("Claim not found", 404)
    }

    if (claim.ownerid !== req.user_id) {
        errorUtils.throwError("Only claimant can send survey result", 403)
    }

    await  mongooseClaimsRepo.update({_id: claimId}, {$set: {survey: answer}});

    return true;
};

module.exports = sendSurveyResult;