const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');

let existingTags = ['v1-naive','v2-merkle','hackathon-diwali']; // we can move this into DB when there's more than 20 items to manage

const setTags = async (root, { claimId, tags }, { req, res }) => {

	let validTags = [];
	for(var i = 0 ; i < tags.length ; i ++){
		let curTag = tags[i];
		for(var j = 0 ; j < existingTags.length ; j ++){
			if(curTag === existingTags[j]){
				validTags.push(curTag);
			}
		}
	}

	await mongooseClaimRepo.update({_id: claimId},{$set: {tags: validTags}});
	
	return true;
}

module.exports = setTags;