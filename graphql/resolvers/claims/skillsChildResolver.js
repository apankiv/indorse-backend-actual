const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');

const skillsChildResolver = async (root, args, { req }) => {
    const claim = root;
    if (!claim) return null;
    const skillName = claim.title;
    const skillFound = await mongooseSkillRepo.findOne({ name: skillName.toLowerCase() });
    return [skillFound].filter(Boolean);
};

module.exports = skillsChildResolver;
