const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const claimsEmailService = require('../../../models/services/common/claimsEmailService');
const timestampService = require('../../../models/services/common/timestampService');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const randtoken = require('rand-token');

// Note: When you make a change in this function, please also check githubSignup.spec.js (should signup a user with Github and a claim) test case, and please modify logic there if needed
const createClaimDraft = async (root, {email, title, desc, proof, source}, {req, res}) => {

    if(!source) {
        source = "page";
    }

    let claimDraft = safeObjects.safeObjectParse({
        title:title,
        desc:desc,
        proof:proof,
        email: email,
        source : source
    },['source','title','desc','proof','email']);

    claimDraft.token = randtoken.generate(16);
    claimDraft.created_at = timestampService.createTimestamp();

    await mongooseClaimDraftsRepo.insert(claimDraft);

    let user = await mongooseUserRepo.findOneByEmail(email);
    let userExists = user ? true : false;

    claimsEmailService.sendClaimDraftLinkEmail(claimDraft.token,email, source);

    amplitudeTracker.publishData('claim_draft_created', {source: source}, req);

    return {
        userExists: userExists
    };
};

module.exports = createClaimDraft;
