const { GROUP: BLACKLIST_GROUP } = require('../../../models/blacklist/blacklist');
module.exports = `
	enum ClaimLevel {
		beginner
		intermediate
		expert
	}
	
	type ClaimDraft {
	    title: String!
	    desc: String!
	    proof: URL
	}
	
	type CreateDraftClaimResp {
		userExists : Boolean!
	}
	
	type SubmitClaimDraftTokenResp {
	    email: EmailAddress!
		draftFinalized: Boolean!
		userExists: Boolean!
	}

	# need_first_query (Do not remove this line)
	
	type Mutation {
		createClaimDraft(email: EmailAddress!, title: String!, desc: String!, proof: URL, source: String): CreateDraftClaimResp @recaptchaAndBlacklistCheck(groupName: "${BLACKLIST_GROUP.CLAIM_DRAFT_CREATE}", limitPerMin: 2),         
		submitClaimDraftToken(token: String!, source: String): SubmitClaimDraftTokenResp  
		# new_mutation (Do not remove this line)
	}
`;
