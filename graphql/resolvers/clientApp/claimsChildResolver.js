const mongooseClientAppRepo = require('../../../models/services/mongo/mongoose/mongooseClientAppRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const claimsResolver = async (root, args, { req }) => {
    const clientApp = root;
    if (!clientApp || !clientApp.client_id) return null;
    const clientAppFound = await mongooseClientAppRepo.findOneByClientId(clientApp.client_id);
    const company = mongooseCompanyRepo.findOneById(clientAppFound.company);
    await authUtils.companyPermissionCheck(req, company._id, ['partnerClaims.read']);
    const criteria = {
        'clientApp.clientId': clientAppFound.client_id,
    };
    const claims = mongooseClaimsRepo.findAll(criteria);
    return claims || [];
};

module.exports = claimsResolver;
