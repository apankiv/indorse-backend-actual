const mongooseClientAppRepo = require('../../../models/services/mongo/mongoose/mongooseClientAppRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
const authUtils = require('../../../models/services/auth/authChecks');

const companyResolver = async (root, args, { req }) => {
    const clientApp = root;
    if (!clientApp || !clientApp.client_id) return null;
    const clientAppFound = await mongooseClientAppRepo.findOneByClientId(clientApp.client_id);
    const company = await mongooseCompanyRepo.findOneById(clientAppFound.company);
    await authUtils.companyPermissionCheck(req, company._id, ['partnerClaims.read']);
    return getCompanyUtils.prepareCompanyObject(company);
};

module.exports = companyResolver;
