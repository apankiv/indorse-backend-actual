const mongooseClientAppRepo = require('../../../models/services/mongo/mongoose/mongooseClientAppRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const getClientByClientId = async (root, { clientId }, { req }) => {
    if (!clientId) return null;
    const clientAppFound = await mongooseClientAppRepo.findOneByClientId(clientId);
    const company = mongooseCompanyRepo.findOneById(clientAppFound.company);
    await authUtils.companyPermissionCheck(req, company._id, ['partnerClaims.read']);
    return clientAppFound;
};

module.exports = getClientByClientId;
