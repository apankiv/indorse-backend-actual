const { authMethods, supportedMethods } = require('../../../models/services/mongo/mongoose/schemas/clientApp');

module.exports = `
  type ClientApp {
    client_id: String
    email: String
    company: Company
    auth: AUTH
    hooks: ClientAppHooks
    claims: [Claim!]!
  }

  type ClientAppHooks {
    verifyPartnerClaim: HookURL
    sendPartnerClaimResult: HookURL
  }

  type HookURL {
    method: METHOD
    url: String
  }

  enum AUTH {
    ${authMethods.BASIC}
    ${authMethods.HANDSHAKE}
  }

  enum METHOD {
    ${supportedMethods.GET}
    ${supportedMethods.POST}
  }

  type Query {
    getClientAppByClientId(clientId: String!): ClientApp!
    # getClientApps(): [ClientApp!]!
  }
`;
