const errorUtils = require('../../../models/services/error/errorUtils');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const mongooseInviteRepo = require('../../../models/services/mongo/mongoose/mongooseInviteRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const timestampService = require('../../../models/services/common/timestampService');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
// const errorUtils = require('../../../models/services/error/errorUtils');

const acceptInviteToCompanyACL = async (root, { token }, { req }) => {
    const decodedData = await cryptoUtils.decodeJWTForInvite(token);
    // find the invite using the invite token
    const criteriaInvite = {
        token: decodedData.token,
        disabled: false,
    };
    const invite = await mongooseInviteRepo.findOne(criteriaInvite);
    if (!invite) {
        errorUtils.throwError('Invite not found or was already used', 403);
    }

    // find associated user to the invite
    const associatedUser = req.user_id;
    if (!associatedUser) {
        errorUtils.throwError('User has not yet logged in', 403);
    }

    // find the company using the company id in the token and update acl
    const criteria = {
        pretty_id: decodedData.prettyId,
    };
    // get Company ACL
    const company = await mongooseCompanyRepo.findOne(criteria);
    if (!company) {
        errorUtils.throwError('Company not found !', 404);
    }
    // get Company ACL Array
    const companyACL = company.acl || [];

    // Check if user already has accepted the invite
    const alreadyExist = companyACL.filter(ACLObject => (ACLObject.userId || '').toString() === associatedUser.toString());
    if (alreadyExist.length) {
        errorUtils.throwError('User is already invited for this company', 403);
    }

    // get array without the considered object
    const ACLArray = companyACL.map((ACLObject) => {
        const updatedACLObject = ACLObject || {};
        if (updatedACLObject.inviteId.toString() && updatedACLObject.inviteId.toString() === invite._id.toString()) {
            updatedACLObject.userId = associatedUser;
            updatedACLObject.acceptedInvite = true;
        }
        return updatedACLObject;
    });

    const dataToSet = {
        $set: {
            acl: ACLArray,
        },
    };
    // update that same company with the new array
    await mongooseCompanyRepo.update(criteria, dataToSet);
    const companyObjectToReturn = await mongooseCompanyRepo.findOne(criteria);

    // update invite
    const inviteUpdate = {
        $set: {
            acceptedAt: timestampService.createTimestamp(),
            disabled: true,
        },
    };
    await mongooseInviteRepo.update(criteriaInvite, inviteUpdate);

    return getCompanyUtils.prepareCompanyObject(companyObjectToReturn);
};

module.exports = acceptInviteToCompanyACL;
