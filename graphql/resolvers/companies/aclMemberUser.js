const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const accessMemberUser = async (root, {}, { req, res }) => {
    const accessMember = root;

    if (!accessMember || !accessMember.userId) return null;
    const user = await mongooseUserRepo.findOneById(accessMember.userId);
    return user;
};

module.exports = accessMemberUser;
