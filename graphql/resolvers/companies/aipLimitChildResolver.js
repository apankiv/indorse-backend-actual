const authUtils = require('../../../models/services/auth/authChecks');

const aipLimitResolver = async (root, args, { req }) => {
    const company = root;
    const permission = await authUtils.companyPermissionCheck(req, company.id, ['aip.read'], false);
    return permission ? company.aipLimit : null;
};

module.exports = aipLimitResolver;
