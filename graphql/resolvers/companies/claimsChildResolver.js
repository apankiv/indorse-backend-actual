const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const claimsResolver = async (root, args, { req }) => {
    const company = root;
    await authUtils.companyPermissionCheck(req, company.id, ['partnerClaims.read', 'magicLink.read']);
    const criteria = {
        company: company.id,
    };
    const claims = mongooseClaimsRepo.findAll(criteria);
    return claims || [];
};

module.exports = claimsResolver;
