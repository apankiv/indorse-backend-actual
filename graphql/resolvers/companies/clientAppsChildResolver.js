const mongooseClientAppRepo = require('../../../models/services/mongo/mongoose/mongooseClientAppRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const clientAppsChildResolver = async (root, args, { req }) => {
    const company = root;
    await authUtils.companyPermissionCheck(req, company.id, ['partnerClaims.read']);
    const criteria = {
        company: company.id,
    };
    const clientApps = await mongooseClientAppRepo.findAll(criteria);
    return clientApps || [];
};

module.exports = clientAppsChildResolver;
