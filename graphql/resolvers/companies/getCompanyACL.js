// This query is not being used
const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const getCompanyACL = async (root, { prettyId }, { req }) => {
    safeObjects.sanitizeMultiple(prettyId);
    const criteria = {
        pretty_id: prettyId,
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['acl.read']);
    return (company && company.acl) || [];
};

module.exports = getCompanyACL;
