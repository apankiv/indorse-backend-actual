const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
const authUtils = require('../../../models/services/auth/authChecks');

const getCompanyByPrettyId = async (root, { prettyId }, { req }) => {
    safeObjects.sanitizeMultiple(prettyId);
    const criteria = {
        pretty_id: prettyId,
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.read', 'partnerClaims.read']);
    return await getCompanyUtils.prepareCompanyObject(company);
};

module.exports = getCompanyByPrettyId;
