// const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');

const getMyCompanies = async (root, args, { req }) => {
    const criteria = {
        acl: {
            $elemMatch: {
                userId: req.user_id,
                acceptedInvite: true,
            },
        },
    };
    const companyList = await mongooseCompanyRepo.findAll(criteria);
    const formattedCompanyList = (companyList || []).map(elem => getCompanyUtils.prepareCompanyObject(elem));
    return formattedCompanyList;
};

module.exports = getMyCompanies;
