const authUtils = require('../../../models/services/auth/authChecks');

const magicLinkChildResolver = async (root, args, { req }) => {
    const company = root;
    const permission = await authUtils.companyPermissionCheck(req, company.id, ['magicLink.write'], false);
    return permission ? company.magicLink : null;
};

module.exports = magicLinkChildResolver;
