const { creditType } = require('../../../models/services/mongo/mongoose/schemas/company');


module.exports = `
enum CompanySortParam {
  asc_company_name
  desc_company_name
}

type SocialLinks {
  type : String!
  url  : String!
}

type UserData {
  id : String
  userName : String
  photo : Photo
  fullName : String
}

type Photo {
  imgUrl : String
  s3Url: String
  ipfsUrl: String
}

type AdditionalData{
  advisors : [UserData]
  teamMembers : [UserData]
}

type CompanyList{
  company : [Company]
  matchingCompanies: Int
  totalCompanies: Int!
}

type CompanyACLMember {
  user: User
  acceptedInvite: Boolean
  email: String
}

type Company {
  id : String!
  prettyId: String!
  email: String
  companyName: String!
  description: String
  acl: [CompanyACLMember]
  claims: [Claim!]!
  clientApps: [ClientApp!]!  
  tagline : String
  socialLinks : [SocialLinks]
  logo : Photo
  teamMembers : [UserData]
  advisors : [UserData]
  additionalData : AdditionalData
  companyCredit: CompanyCredits
  aipLimit: Int
  magicLink: MagicLinkConfiguration
}

type MagicLinkConfiguration {
  autoAllowedAssignment: Boolean!
  assignmentRestricted: Boolean!  
}

type SuccessResponse {
  success: Boolean!
  prettyId: String
}

type Credit { 
    limit: Int!, 
    used: Int!,
    noOfHiddenAssignment: Int,
}

enum CreditUpdateType {
    ${creditType.ASSESSMENT_ASSIGNMENT}
}

type CreditUpdate { 
    creditType: CreditUpdateType!, # 'assessmentAssignment'
    delta: Int!, # +10
    user: User!, 
    reason: String, 
    timestamp: Int!
}

input CreditUpdateForm {
    creditType: CreditUpdateType!,
    delta: Int!,
    reason: String
}

type CompanyCredits {
    assessmentAssignment: Credit
    updates: [CreditUpdate!]
}


type Query {
  visitorListCompanies(pageNumber: Int, pageSize: Int, sort: CompanySortParam, search: String): CompanyList
  getCompanyACL(prettyId: String!): [CompanyACLMember]
  getCompanyByPrettyId(prettyId: String!): Company
  getMyCompanies : [Company]
  # new_query (Do not remove this line)
}

type Mutation {
  sendInviteToCompanyACL(prettyId: String!, email: String!): Company @permissionCheck(roles: ["admin"])
  acceptInviteToCompanyACL(token: String!): Company
  removeCompanyACL(email: String!, prettyId: String!): Company @permissionCheck(roles: ["admin"])
  updateCompanyCredits(form: CreditUpdateForm!, companyPrettyId: String! ): CompanyCredits @permissionCheck(roles: ["admin"])
  updateCompanyAIPLimit(aipLimit: Int!, companyPrettyId: String! ): Company! @permissionCheck(roles: ["admin"])
  restrictAssignmentsForCompany(companyPrettyId: String!, restrictAssignment: Boolean!): Company! @permissionCheck(roles: ["admin"])
  # new_mutation (Do not remove this line)
}
`;
