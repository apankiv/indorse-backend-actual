const mongooseInviteRepo = require('../../../models/services/mongo/mongoose/mongooseInviteRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
const errorUtils = require('../../../models/services/error/errorUtils');

const removeCompanyACL = async (root, { email, prettyId }, { req, res }) => {
    // find the company using the company id in the token and update acl
    const criteriaCompany = {
        pretty_id: prettyId,
    };
    // get Company ACL
    const company = await mongooseCompanyRepo.findOne(criteriaCompany);
    if (!company) {
        errorUtils.throwError('Company not found !', 404);
    }
    // get Company ACL Array
    const companyACL = company.acl || [];
    // get array without the considered object
    let inviteToDisable;
    const ACLArray = companyACL.filter((ACLObject) => {
        if (ACLObject.email.toString() !== email) {
            return true;
        }
        inviteToDisable = ACLObject.inviteId;
        return false;
    });
    const dataToSet = {
        $set: {
            acl: ACLArray,
        },
    };
    // update that same company with the new array
    await mongooseCompanyRepo.update(criteriaCompany, dataToSet);
    const companyObjectToReturn = await mongooseCompanyRepo.findOne(criteriaCompany);

    const criteriaInvite = {
        _id: inviteToDisable,
    };
    // update invite
    const inviteUpdate = {
        $set: {
            disabled: true,
        },
    };
    await mongooseInviteRepo.update(criteriaInvite, inviteUpdate);

    return getCompanyUtils.prepareCompanyObject(companyObjectToReturn);
};

module.exports = removeCompanyACL;
