// This query is not being used
const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');

const restrictAssignmentsForCompany = async (root, { companyPrettyId, restrictAssignment }, { req }) => {
    safeObjects.sanitizeMultiple(companyPrettyId);
    const criteria = {
        pretty_id: companyPrettyId,
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);

    const dataToSet = {
        $set: {
            'magicLink.assignmentRestricted': restrictAssignment,
        },
    };
    await mongooseCompanyRepo.update(criteria, dataToSet);
    const updatedCompany = await mongooseCompanyRepo.findOne(criteria);
    return await getCompanyUtils.prepareCompanyObject(updatedCompany);
};

module.exports = restrictAssignmentsForCompany;
