// const errorUtils = require('../../../models/services/error/errorUtils');
const inviteUtils = require('../../../models/services/common/inviteUtils');
const mongooseInviteRepo = require('../../../models/services/mongo/mongoose/mongooseInviteRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const emailService = require('../../../models/services/common/emailService');
const timestampService = require('../../../models/services/common/timestampService');
const errorUtils = require('../../../models/services/error/errorUtils');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');
// const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const sendInviteToCompanyACL = async (root, { prettyId, email }, { req }) => {
    // Create Invite
    const token = inviteUtils.generateToken(12);
    const inviteObject = {
        token,
        email,
        invitedBy: req.user_id,
        invitedAt: timestampService.createTimestamp(),
    };
    const inviteId = await mongooseInviteRepo.insert(inviteObject);

    // find the company using the company id in the token and update acl
    const criteria = {
        pretty_id: prettyId,
    };
    // get Company ACL
    const company = await mongooseCompanyRepo.findOne(criteria);
    if (!company) {
        errorUtils.throwError('Company not found !', 404);
    }
    // get Company ACL Array
    const companyACL = company.acl || [];
    const alreadyExist = companyACL.filter(ACLObject => ACLObject.email === email);
    if (alreadyExist.length) {
        errorUtils.throwError('User is already invited for this company', 403);
    }
    const ACLObject = {
        email,
        inviteId,
        acceptedInvite: false,
    };
    // push element in the array
    companyACL.push(ACLObject);

    // update the company acl array
    const dataToSet = {
        $set: {
            acl: companyACL,
        },
    };
    // update that same company with the new array
    await mongooseCompanyRepo.update(criteria, dataToSet);
    const companyObjectToReturn = await mongooseCompanyRepo.findOne(criteria);
    const companyName = companyObjectToReturn.company_name;
    const encodeObject = {
        token,
        prettyId,
    };
    // encodedToken is sent in email
    const encodedToken = cryptoUtils.encodeJWTForInvite(encodeObject);
    console.log(encodedToken);
    await emailService.sendInviteToCompanyACL(email, encodedToken, companyName, prettyId);
    return getCompanyUtils.prepareCompanyObject(companyObjectToReturn);
};

module.exports = sendInviteToCompanyACL;
