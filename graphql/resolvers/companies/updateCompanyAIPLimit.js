// This query is not being used
const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authUtils = require('../../../models/services/auth/authChecks');
const errorUtils = require('../../../models/services/error/errorUtils');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');

const updateCompanyAIPLimit = async (root, { companyPrettyId, aipLimit }, { req }) => {
    safeObjects.sanitizeMultiple(companyPrettyId);
    const criteria = {
        pretty_id: companyPrettyId,
    };
    const dataToSet = {
        $set: {
            aipLimit,
        },
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['aip.write']);

    if (aipLimit) {
        const numericAipLimit = parseInt(aipLimit, 10);
        if (numericAipLimit < 4) {
            errorUtils.throwError('Limit cannot be lower than 4 voters', 400);
        }
        if (numericAipLimit > 10) {
            errorUtils.throwError('Limit cannot be greater than 10 voters', 400);
        }
        if (numericAipLimit % 2) {
            errorUtils.throwError('Limit must be even because of assignments', 400);
        }
    }

    company.aipLimit = aipLimit;
    await mongooseCompanyRepo.update(criteria, dataToSet);
    return await getCompanyUtils.prepareCompanyObject(company);
};

module.exports = updateCompanyAIPLimit;
