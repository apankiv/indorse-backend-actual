const mongooseCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names')
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const logger = require('../../../models/services/common/logger').getLogger();
const safeObjects = require('../../../models/services/common/safeObjects');
const timestampService = require('../../../models/services/common/timestampService');
const errorUtils = require('../../../models/services/error/errorUtils');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const {creditType, isValidCreditType} = require('../../../models/services/mongo/mongoose/schemas/company');
const getCompanyUtils = require('../../../models/services/companies/getCompanyUtils');

const updateCompanyCredits = async (root, {form, companyPrettyId}, {req, res}) => {

    const creditUpdateType = form.creditType;
    const delta = form.delta;
    const reason = form.reason;

    const companyFindCriteria = {
        pretty_id: companyPrettyId
    }

    const company = await mongooseCompanyRepo.findOne(companyFindCriteria);

    if (!company) {
        errorUtils.throwError("Company doesnt exist", 404);
    }

    if (!isValidCreditType(creditUpdateType)) {
        errorUtils.throwError("Invalid credit type", 400);
    }

    let currentLimit, currentUsed;
    if (company.credits && company.credits[creditUpdateType]) {
        currentLimit = company.credits[creditUpdateType].limit;
        currentUsed = company.credits[creditUpdateType].used;
    } else {
        logger.debug("Company credit not found  - company name" + companyPrettyId);
        errorUtils.throwError("Company credit not found", 400);
    }


    currentLimit = currentLimit + delta;
    if (currentLimit < 0) {
        errorUtils.throwError("Credit cannot be less than zero", 400);
    }


    const updatedCreditLimit = currentLimit;
    const newBalance = updatedCreditLimit - currentUsed;
    let limitReachedTimestamp, used;

    let result = await getLimitReachedTimestamp(company, currentUsed, updatedCreditLimit);
    limitReachedTimestamp = result.limitReachedTimestamp;
    currentUsed = result.used;
    console.log('limit reached timestamp = ' + limitReachedTimestamp);

    const creditUpdate = {
        creditType: creditType.ASSESSMENT_ASSIGNMENT,
        delta: delta,
        userId: req.user_id,
        reason: reason,
        timestamp: timestampService.createTimestamp()
    };

    const creditLimit = {
        $set: {
            credits: {
                [creditUpdateType]: {
                    limit: updatedCreditLimit,
                    used: currentUsed,
                    limitReachedTimestamp: limitReachedTimestamp
                }
            }
        },
        $push: {credit_updates: creditUpdate}
    };


    // check the limitReachedTimestamp
    await mongooseCompanyRepo.update({_id: company._id}, creditLimit);

    let updatedCompany = await mongooseCompanyRepo.findOne({_id: company._id});

    return await getCompanyUtils.prepareCompanyCreditObject(updatedCompany)
};


async function getLimitReachedTimestamp(company, used, limit) {
    let overUsed = used-limit;

    if (used < limit) {
        return { limitReachedTimestamp: null, used }; // all assignments will shown.
    }
    if(limit == 0){
        return {limitReachedTimestamp: 1, used: used}; // all assignments will be hidden.
    }

    let allAttemptedUserAssessment = await mongooseUserAssessmentRepo.findAll({company: String(company._id)});
    let limitReachedTimestamp;
    let allApprovedClaims = [];
    for (let i = 0; i < allAttemptedUserAssessment.length; i++) {
        let userAssigmentId = allAttemptedUserAssessment[i].userAssignmentId;
        if (userAssigmentId) {
            let userAssignment = await mongooseUserAssignmentRepo.findOneById(userAssigmentId);
            if (userAssignment) {
                let claimId = userAssignment.claim_id;
                if (claimId) {
                    let claim = await mongooseClaimRepo.findOneById(claimId);
                    if (claim && claim.approved) {
                        allApprovedClaims.push(claim);
                    }
                }
            } else {
                logger.debug('User Assignment ID was found but according userAssignment deosnt exist. UserAssignmentId: ' + userAssigmentId);
            }
        } else if (allAttemptedUserAssessment[i].claimId) {
            let claim = await mongooseClaimRepo.findOneById(allAttemptedUserAssessment[i].claimId);
            if (claim && claim.approved) {
                allApprovedClaims.push(claim);
            }    
        }
    }


    allApprovedClaims.sort(compareByApprovedDate);
    used = allApprovedClaims.length;

    // the range of balance here is    0<balance<used
    // set (used - newBalance)th approved claim as a limit reach timestamp
    let adjustedIndexForLimitTimestamp = limit - 1;
    console.log('length of claims ' + allApprovedClaims.length + ' selected index = ' + adjustedIndexForLimitTimestamp);

    let claimSelected = allApprovedClaims[adjustedIndexForLimitTimestamp];
    if (claimSelected) {
        limitReachedTimestamp = claimSelected.approved;
    } else {
        logger.debug('No claim is selected for limitReachedTimestamp when balance is low. Updated balance: ' +  ' current used: ' + used + ' company pretty id: ' + company.pretty_id);
    }

    return {limitReachedTimestamp: limitReachedTimestamp, used: used};
}

function compareByApprovedDate(a, b) {
    if (a.approved < b.approved)
        return -1;
    if (a.approved > b.approved)
        return 1;
    return 0;
}

module.exports = updateCompanyCredits;
module.exports.getLimitReachedTimestamp = getLimitReachedTimestamp;
