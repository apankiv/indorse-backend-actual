const mongooseCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names')
const safeObjects = require('../../../models/services/common/safeObjects');
const visitorListCompaniesUtils = require('../../../models/services/companies/visitorCompanyListUtils')
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

const visitorListCompanies = async (root, { pageNumber, pageSize, sort, search }, { req, res }) => {
    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);
    const skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    const limit = parseInt(pageSize);

    const allCompaniesCursor = await mongooseCompanyRepo.findAllWithCursor({$and : [{ visible_to_public: true }, { pretty_id: { $exists: true } }]});
    const totalCompaniesCount = await allCompaniesCursor.count();

    let companyCursor;
    let matchingCompaniesCount;
    if (search) {
        companyCursor = await visitorListCompaniesUtils.searchCompanies(search);
        matchingCompaniesCount = await companyCursor.count();
    } else {
        companyCursor = allCompaniesCursor;
        matchingCompaniesCount = totalCompaniesCount;
    }

    const sortObj = await visitorListCompaniesUtils.prepareSortObj(sort);

    const sortedCompanies = await companyCursor.collation({ locale: 'en' }).sort(sortObj).skip(skip).limit(limit).toArray();

    const companiesToReturn = [];

    for (company of sortedCompanies) {
        let prepObj = await visitorListCompaniesUtils.prepareCompanyObject(company);
        companiesToReturn.push(prepObj);
    }    

    const responseObj = {
        company: companiesToReturn,
        matchingCompanies: matchingCompaniesCount,
        totalCompanies: totalCompaniesCount,
    };

    amplitudeTracker.publishData('companies_loaded', {}, req);
    return responseObj;
};

module.exports = visitorListCompanies;
