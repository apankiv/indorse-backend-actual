const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseCompanyAssignmentsRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyAssignmentsRepo');
const mongoAssignmentsRepo = require('../../../models/services/mongo/mongoRepository')('assignments'); //TODO
const safeObjects = require('../../../models/services/common/safeObjects');
const stringUtils = require('../../../models/services/blob/stringUtils');

const getCompanyAssignments = async (root, { companyPrettyId, pageNumber = 1, pageSize = 100, sort, search }, {}) => {
    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);
    const skip = (parseInt(pageNumber, 10) - 1) * parseInt(pageSize, 10);
    const limit = parseInt(pageSize, 10);
    // fetch all the company assignments
    const company = await mongooseCompanyRepo.findOneByPrettyId(companyPrettyId);
    const criteriaCompanyAssignments = {
        companyId: company._id,
    };
    const companyAssignmentsObject = await mongooseCompanyAssignmentsRepo.findOne(criteriaCompanyAssignments);
    const { linkedAssignments = [] } = companyAssignmentsObject || {};
    const linkedAssignmentIds = linkedAssignments.map(assignment => assignment.assignmentId);
    const criteria = { _id: { $in: linkedAssignmentIds } };
    const allAssignmentsCursor = await mongoAssignmentsRepo.findAllWithCursor(criteria);
    const totalAssignments = await allAssignmentsCursor.count();

    let assignmentCursor;
    let matchingAssignments;

    if (search) {
        const regex = stringUtils.createSafeRegexObject(search);

        assignmentCursor = await mongoAssignmentsRepo.findAllWithCursor({
            $or: [
                { title: regex },
                { scope: regex },
                { skills: { $elemMatch: { name: regex } } },
            ],
            disabled: { $ne: true },
        });
    } else {
        assignmentCursor = allAssignmentsCursor;
        matchingAssignments = totalAssignments;
    }

    const sortObj = prepareSortObj(sort);
    const sortedAssignments = await assignmentCursor.sort(sortObj).skip(skip).limit(limit).toArray();
    const companyAssignments = sortedAssignments.map(assignment => ({ assignment, companyStatus: true }));
    return {
        companyAssignments,
        totalAssignments,
        matchingAssignments,
    };
};

function prepareSortObj(sort) {
    let field = 'listingPosition';
    let order = -1;
    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }
    }
    return { [field]: order };
}


module.exports = getCompanyAssignments;
