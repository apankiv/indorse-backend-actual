module.exports = `  
type CompanyAssignment {
  assignment: Assignment!
  companyStatus: Boolean!
}

type CompanyAssignmentsList {
  companyAssignments: [CompanyAssignment!]!
  matchingAssignments: Int
  totalAssignments: Int!
}

type Query {
  getAssignmentsWithCompanyStatus(companyPrettyId: String!, pageNumber: Int, pageSize: Int, sort: String, search: String): CompanyAssignmentsList! @permissionCheck(roles: ["admin"])
  getCompanyAssignments(companyPrettyId: String!, pageNumber: Int, pageSize: Int, sort: String, search: String): CompanyAssignmentsList!
  # new_query (Do not remove this line)
}

type Mutation {
  updateCompanyAssignmentLink(companyPrettyId: String!, assignmentId: String!, link: Boolean!): CompanyAssignment! @permissionCheck(roles: ["admin"])
  # new_mutation (Do not remove this line)
}
`;
