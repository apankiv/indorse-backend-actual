// This query is not being used
const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseCompanyAssignmentsRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyAssignmentsRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const authUtils = require('../../../models/services/auth/authChecks');

const updateCompanyAssignmentLink = async (root, { companyPrettyId, assignmentId, link = true }, { req }) => {
    safeObjects.sanitizeMultiple(companyPrettyId);
    const criteria = {
        pretty_id: companyPrettyId,
    };
    const company = await mongooseCompanyRepo.findOne(criteria);
    await authUtils.companyPermissionCheck(req, company._id, ['magicLink.write']);
    if (link) {
        await mongooseCompanyAssignmentsRepo.linkAssignment(company._id, assignmentId, req.user_id);
    } else {
        await mongooseCompanyAssignmentsRepo.unlinkAssignment(company._id, assignmentId, req.user_id);
    }
    const assignment = await mongooseAssignmentRepo.findOneById(assignmentId);
    return {
        assignment,
        companyStatus: link,
    };
};

module.exports = updateCompanyAssignmentLink;
