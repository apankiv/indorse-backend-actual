const jobRolesUtils = require('../../../models/services/jobRoles/jobRolesUtils');

const createJobRole = async (root, { form }) => {
    const jobRole = await jobRolesUtils.upsertJobRole({ form });

    return jobRole;
};

module.exports = createJobRole;
