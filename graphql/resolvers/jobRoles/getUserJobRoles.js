const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

const getUserJobRoles = async (root, { userId }, { req }) => {
    // using the userId in the input
    const user = await mongooseUserRepo.findOneById(userId);

    const criteria = {
        _id: {
            $in: user.job_roles || [],
        },
    };
    const jobRole = await mongooseJobRoleRepo.findAll(criteria);
    return Array.isArray(jobRole) ? jobRole : [];
};

module.exports = getUserJobRoles;
