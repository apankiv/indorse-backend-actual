const errorUtils = require('../../../models/services/error/errorUtils');
const mongoAssignmentsRepo = require('../../../models/services/mongo/mongoRepository')('assignments');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const ObjectID = require('mongodb').ObjectID;

const jobRoleAssignments = async (root, args, { req }) => {
    const jobRole = root;

    const user = await mongoUserRepo.findOneById(req.user_id);
    if (!user) {
        errorUtils.throwError(
            'Logged in user not found. Something went wrong',
            403,
        );
    }

    if (!jobRole) {
        errorUtils.throwError('Role does not exist', 404);
    }

    const cursor = await mongoAssignmentsRepo.findAllWithCursor({ jobRole: ObjectID(jobRole._id.toString()) });
    const assignmentsToReturn = await cursor.sort({ listingPosition: 1 }).toArray();

    return assignmentsToReturn;
};

module.exports = jobRoleAssignments;
