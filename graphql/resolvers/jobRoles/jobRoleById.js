const safeObjects = require('../../../models/services/common/safeObjects');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

const jobRoleById = async (root, { id }) => {
    const roleRequest = safeObjects.safeObjectParse({ id }, ['id']);
    
    const jobRole = await mongooseJobRoleRepo.findOneById(roleRequest.id);

    return jobRole;
};

module.exports = jobRoleById;
