const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const jobRoleSkills = async (root, args, { req }) => {
    const jobRole = root;

    const user = await mongoUserRepo.findOneById(req.user_id);
    if (!user) {
        errorUtils.throwError(
            'Logged in user not found. Something went wrong',
            403,
        );
    }

    if (!jobRole) {
        errorUtils.throwError('Role does not exist', 404);
    }

    let skillsToReturn = [];
    if (jobRole.skillTags) {
        skillsToReturn = await mongooseSkillsRepo.findAll({
            tags: { $in: jobRole.skillTags },
        });
        for (let i = 0; i < skillsToReturn.length && user.skills; i++) {
            skillsToReturn[i].userHasSkill = user.skills.some(
                skill =>
                    skillsToReturn[i]._id.toString() ===
                    skill.skill._id.toString() && !skill.isDeleted,
            );
        }
    }

    return skillsToReturn;
};

module.exports = jobRoleSkills;
