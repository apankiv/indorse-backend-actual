module.exports = `
    type JobRole {
        _id: String!
        position: String
        prettyId: String
        title: String
        description : String
        iconUrl : String
        skillTags: [String]
        skills: [JobRoleSkill]
        assignments: [Assignment!]
        userHasJobRole: Boolean!
    }

    input CreateJobRoleForm {
        prettyId: String!
        title: String!
        description: String!
        iconData: String!
        skillTags: [String!]!
    }

    input UpdateJobRoleForm {
        prettyId: String!
        title: String!
        description: String!
        iconData: String
        skillTags: [String!]!
    }

    type JobRoleSkill {
        _id : String!
        name: String!
        iconUrl: String
        tags :[String]!
        userHasSkill : Boolean
        disabledFromFrameworkListing: Boolean
    }
	
	type Mutation {
		createJobRole(form: CreateJobRoleForm!): JobRole @permissionCheck(roles: ["admin"])
        updateJobRole(form: UpdateJobRoleForm!): JobRole @permissionCheck(roles: ["admin"])
    }
    
    type Query {
        getUserJobRoles(userId: String!): [JobRole!]!
        jobRoles: [JobRole]  @loginCheck
        jobRoleById(id: String!): JobRole   @loginCheck
        getJobRoles(void: Boolean): [JobRole] 
    }        
`;
