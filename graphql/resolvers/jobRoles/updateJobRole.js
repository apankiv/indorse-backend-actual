const jobRolesUtils = require('../../../models/services/jobRoles/jobRolesUtils');

const updateJobRole = async (root, { form }) => {
    const jobRole = await jobRolesUtils.upsertJobRole({ form });

    return jobRole;
};

module.exports = updateJobRole;
