const mongoJobsRepo = require('../../../models/services/mongo/mongoRepository')('jobs'); //TODO
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

const adminJobPosts = async (root, { pageNumber, pageSize, sort, search, campaignEnabled } , {req, res}) => {
    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list jobs!', 403);
    }

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search, campaignEnabled);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }
    
    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allJobsCursor = await mongoJobsRepo.findAllWithCursor({ _id: { $exists: true } }); 
    let totalJobPosts = await allJobsCursor.count()

    let jobsCursor, matchingJobPosts, sortedJobs;

    let sortObj = jobUtils.prepareSortObj(sort);
    if (search) {
        [sortedJobs, matchingJobPosts] = await jobUtils.searchJobs(search, skip, limit, campaignEnabled, false, sortObj);
    } else {
        jobsCursor = allJobsCursor;
        matchingJobPosts = totalJobPosts;
        sortedJobs = await jobsCursor.sort(sortObj).skip(skip).limit(limit).toArray();
    }

    let jobsToReturn = [];

    for(job of sortedJobs){
        await jobUtils.prepareJobObject(job);
        jobsToReturn.push(job);
    }

    const jobDataToTrack = {
        search_query: search,
        user_search: false,
        campaign_enabled: campaignEnabled,
        only_approved_jobs: false,
        total_job_posts: totalJobPosts,
        matching_job_posts: matchingJobPosts
    }

    amplitudeTracker.publishData('job_post_admin_search', jobDataToTrack, req);
    let responseObj = {
        jobPosts: jobsToReturn,
        totalJobPosts: totalJobPosts,
        matchingJobPosts: matchingJobPosts
    };
    
    return responseObj;
}

module.exports = adminJobPosts;