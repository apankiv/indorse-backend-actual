const safeObjects = require('../../../models/services/common/safeObjects');
const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const timestampService = require('../../../models/services/common/timestampService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const emailService = require('../../../models/services/common/emailService')

const applyJobPost = async (root, {id}, {req, res}) => {

    if (!jobUtils.userIsLoggedIn(req)) {
        errorUtils.throwError('Insufficient permission request job', 403);
    }

    let mongoId = safeObjects.sanitize(id);
    let job = await mongoJobsRepo.findOneById(mongoId);

    if (!job.approved.approved)
        errorUtils.throwError('Job is not approved', 403);

    let user = await mongoUserRepo.findOneByEmail(req.email);
    if (!await jobUtils.userMeetsSkillRequirement(user.skills, job.skills)){
        errorUtils.throwError('User does not meet skill criteria', 403);
    }

    let applicationDetails = {
        user: user._id.toString(),
        job : job._id.toString(),
        appliedAt: timestampService.createTimestamp(),
        status: 'user_applied'
    }


    let jobPostApplicationResult = {
        email: job.contactEmail
    }   

    if (job.applicationLink){
        jobPostApplicationResult.url = job.applicationLink
    }
    

    //Trigger amplitude event
    let company = await mongoCompanyRepo.findOneById(job.company.id)

    let jobDataToTrack = {
        job_id : job._id.toString(),
        title : job.title,
        experience_level: job.experienceLevel,
        company_id : job.company.id,
        company_name: company.company_name,
        user_id : user._id.toString(),
        contact_email : job.contactEmail
    }

    amplitudeTracker.publishData('job_post_applied', jobDataToTrack, req);

    let startedApplication = await jobUtils.userStartedApplicationForJob(user._id.toString(), id);
    if(!startedApplication)
        errorUtils.throwError("Job application has not been started",403)


    //Trigger email
    let applied = await jobUtils.userAppliedForJob(user._id.toString(), id);
    if (!applied) {
        await emailService.notifyJobApplication(user.name, user.username,user.email, job.title,job.location, job.contactEmail);
    }

    await mongoUserJobsRepo.update({
        $and: [{ user: user._id.toString() }, { job: job._id.toString() }]
    },{
            appliedAt: timestampService.createTimestamp(),
            status: 'user_applied'
    });
    return jobPostApplicationResult;
}

module.exports = applyJobPost;