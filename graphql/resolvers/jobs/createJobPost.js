const mongooseJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const timestampService = require('../../../models/services/common/timestampService');
const safeObjects = require('../../../models/services/common/safeObjects');
const jobUtils = require('../../../models/services/jobs/jobUtils');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

const createJobPost = async (root, {form}, {req, res}) => {

    let sanitizedDescription = safeObjects.sanitizeHtml(form.description)
    let sanitizedCompanyDescription = safeObjects.sanitizeHtml(form.company.description)
    safeObjects.sanitize(form);
    form.company.description = sanitizedCompanyDescription;

    let jobToCreate = {
        title: form.title,
        experienceLevel: form.experienceLevel,
        description: sanitizedDescription,
        monthlySalary: form.monthlySalary,
        location: form.location,
        contactEmail: form.contactEmail
    };

    if (form.applicationLink) {
        jobToCreate.applicationLink = form.applicationLink;
    }

    if (form.campaign) {
        jobToCreate.campaign = form.campaign;
    }

    let jobDataToTrack = {
        skills_count: form.skills.length,
        feature_category: 'jobs',
        feature_sub_category: 'create-job-post-ID-1104'
    }

    jobToCreate.submitted = {
        at: timestampService.createTimestamp()
    };

    if (req.login && req.permissions) {
        const userFromMongo = await mongooseUserRepo.findOneByEmail(req.email);
        const user_id = userFromMongo._id.toString();
        jobToCreate.submitted.by = user_id;
        jobDataToTrack.user_id = user_id;
    }
    

    if (form.skills) {
        await jobUtils.addSkillsToJob(form, jobToCreate);
    }

    await jobUtils.addCompanyToJob(form, jobToCreate);

    jobToCreate.id = await mongooseJobsRepo.insert(jobToCreate);
    await jobUtils.prepareJobObject(jobToCreate);

    jobDataToTrack.company_id = jobToCreate.company.id;
    jobDataToTrack.company_name = jobToCreate.company.name;
    if (form.company.id === jobToCreate.company.id) {
        jobDataToTrack.new_company = false;
    } else {
        jobDataToTrack.new_company = true;
    }

    amplitudeTracker.publishData('job_post_created', jobDataToTrack, req);
    return jobToCreate;
};

module.exports = createJobPost;