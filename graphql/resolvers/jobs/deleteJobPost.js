const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const ObjectID = require('mongodb').ObjectID;


const deleteJobPost = async (root, { id}, { req, res }) => {
    id = safeObjects.sanitize(id);

    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to delete job!', 403);
    }
    await mongoJobsRepo.deleteOne({ _id: ObjectID(id)});    
    return id;
}

module.exports = deleteJobPost;