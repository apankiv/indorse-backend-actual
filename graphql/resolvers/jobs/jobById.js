const safeObjects = require('../../../models/services/common/safeObjects');
const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')

const jobById = async (root, {id}, { req, res }) => {
    let mongoId = safeObjects.sanitize(id);

    //If user is not logged in show job but without 
    //application link or contact email details
    if (!jobUtils.userIsLoggedIn(req)) {
       let job = await mongoJobsRepo.findOneById(mongoId);

        if (!job.approved || !job.approved.approved)
            errorUtils.throwError('Job is not approved', 403);            
            
       await jobUtils.prepareJobObject(job);    
       delete job.contactEmail;
       delete job.applicationLink;
       return job;
    }
    
    let job = await mongoJobsRepo.findOneById(mongoId);

    let skipApply = true;
    let isAdmin = jobUtils.userIsAuthorized(req);

    let user = await mongoUserRepo.findOneByEmail(req.email);

    if (!isAdmin) {
        if (!job.approved || !job.approved.approved)
            errorUtils.throwError('Job is not approved', 403);
        
        if (!await jobUtils.userMeetsSkillRequirement(user.skills, job.skills)) {
            skipApply = true;
        } else {
            skipApply = false;
        }
    }
    await jobUtils.prepareJobObject(job, req.user_id);        

    if (skipApply && !isAdmin){        
        delete job.contactEmail;
        delete job.applicationLink;
    }   
    //TODO : Give preference to existing user status if exists to save computation
    job.applicationStatus = await jobUtils.getApplicationStatus(user,job);
    return job;
}

module.exports = jobById;