const mongoJobsRepo = require('../../../models/services/mongo/mongoRepository')('jobs'); //TODO
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

const jobPosts = async (root, { pageNumber, pageSize, sort, search } , {req, res}) => {

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }
    
    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allApprovedJobsCursor = await mongoJobsRepo.findAllWithCursor({ $and: [{'approved.approved': true}, {_id: { $exists: true }}] });
    let totalJobPosts = await allApprovedJobsCursor.count()

    let jobsCursor, matchingJobPosts, sortedJobs;

    if (search) {
        [sortedJobs, matchingJobPosts] = await jobUtils.searchJobs(search, skip, limit, false, true);
    } else {
        jobsCursor = allApprovedJobsCursor;
        matchingJobPosts = totalJobPosts;
        let sortObj = jobUtils.prepareSortObj(sort);
        sortedJobs = await jobsCursor.sort(sortObj).skip(skip).limit(limit).toArray();
    }

    let jobsToReturn = [];

    for(job of sortedJobs){
        await jobUtils.prepareJobObject(job);
        jobsToReturn.push(job);
    }

    const jobDataToTrack = {
        search_query: search,
        user_search: true,
        campaign_enabled: false,
        only_approved_jobs: true,
        total_job_posts: totalJobPosts,
        matching_job_posts: matchingJobPosts
    }

    amplitudeTracker.publishData('job_post_search', jobDataToTrack, req);
    let responseObj = {
        jobPosts: jobsToReturn,
        totalJobPosts: totalJobPosts,
        matchingJobPosts: matchingJobPosts
    };
    
    return responseObj;
}

module.exports = jobPosts;
