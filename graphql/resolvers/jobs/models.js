module.exports = `
    enum ExperienceLevel {
		intern
		junior
		intermediate
		senior
	}
	
	enum SkillLevel {
		beginner
		intermediate
		expert
	}
	
	enum JobsSortParam {
		asc_title
		desc_title
		asc_experience_level
		desc_experience_level
		asc_location
		desc_location
		asc_submitted_at
		desc_submitted_at
		asc_updated_at
		desc_updated_at
	}


	enum UserAppliedJobsSortParam{
		asc_applied_at
		desc_applied_at
		asc_application_start_at
		desc_application_start_at
	}


	enum JobApplicationStatus{
		user_applied
		user_pending_validations
		user_can_apply		
	}
	
	input JobsSkillForm {
        id: String!
        level: SkillLevel
    }
    
    input JobsCompany {
		id: String
		name: String
		logo: String
		description: String!
	}
	
	input JobCampaignForm {
	    title: String!
	    description: String
	    enabled: Boolean!
	}
	
	input JobPostForm {
		title: String!
		experienceLevel: ExperienceLevel!
		description: String!
		monthlySalary: String!
		company: JobsCompany!
		contactEmail: EmailAddress!
		applicationLink: String
		location:String
		skills: [JobsSkillForm]
		location: String
		campaign: JobCampaignForm
	}
	
	type JobLogo {
	    s3Url: String
	    ipfsUrl: String
	}
	
	type JobCompany {
        id: String!
        pretty_id: String
        name: String!
        logo: JobLogo
        description: String!
    }
	
	type JobPostApplicationResult {
		url: URL
		email: EmailAddress
	}
	
	type JobPostAuditItem {
		by: String
		at: Int
	}
	
	type JobPostApprovalStatus {
		by: String
		at: Int
		approved: Boolean
	}

	type UserValidationStatus {
		claimInProgress : SkillLevel
		claimValidated : SkillLevel
		quizbot : SkillLevel
	}
	
	type JobSkill {
		id: String!
		name: String!
		category: String!
		level: SkillLevel!
		userStatus : UserValidationStatus
	}
	
	type JobCampaign {
	    title: String!
	    description: String
	    enabled: Boolean!
	}
	
	type JobPost {
		id: String!
		title: String!
		experienceLevel: ExperienceLevel!
		description: String!
		monthlySalary: String!
		company: JobCompany!
		contactEmail: EmailAddress
		applicationLink: String
		skills: [JobSkill]
		location : String
		campaign: JobCampaign
		submitted: JobPostAuditItem
		approved: JobPostApprovalStatus
		updated: JobPostAuditItem
		applicationStatus : JobApplicationStatus
		applicationStarted : Boolean
	}
	
	type JobPostList {
		jobPosts: [JobPost]
		matchingJobPosts: Int
		totalJobPosts: Int!
	}
	
  type Query {
		adminJobPosts(pageNumber: Int, pageSize: Int, sort: JobsSortParam, search: String, campaignEnabled: Boolean): JobPostList
		jobPosts(pageNumber: Int, pageSize: Int, sort: JobsSortParam, search: String): JobPostList
		jobById(id: String!): JobPost
		userAppliedJobPosts(pageNumber: Int, pageSize: Int, sort: UserAppliedJobsSortParam, search: String): JobPostList
		# new_query (Do not remove this line)
	}
	
	type Mutation {
		createJobPost(form: JobPostForm): JobPost
		updateJobPost(id: String!, form: JobPostForm): JobPost
		updateJobPostStatus(id: String!, status : Boolean!): JobPost
		deleteJobPost(id: String!): String
		applyJobPost(id: String!): JobPostApplicationResult
		startApplicationJobPost(id: String!): JobApplicationStatus
		# new_mutation (Do not remove this line)
	}`	