const safeObjects = require('../../../models/services/common/safeObjects');
const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const timestampService = require('../../../models/services/common/timestampService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

const startApplicationJobPost = async (root, {id}, {req, res}) => {
    if (!jobUtils.userIsLoggedIn(req)) {
        errorUtils.throwError('Insufficient permission request job', 403);
    }

    let mongoId = safeObjects.sanitize(id);
    let job = await mongoJobsRepo.findOneById(mongoId);

    if (!job.approved.approved)
        errorUtils.throwError('Job is not approved', 403);
          

    let user = await mongoUserRepo.findOneById(req.user_id);

    //Check if user has started application for job
    let userApplied = await jobUtils.userStartedApplicationForJob(user._id.toString(), id);

    if(userApplied)
        errorUtils.throwError('User has already started application for this job', 403);

    let userMeetsSkill = await jobUtils.userMeetsSkillRequirement(user.skills, job.skills);

    let userJobStatus = 'user_pending_validations'
    if(userMeetsSkill)  {
        userJobStatus = 'user_can_apply'
    }

    let applicationDetails = {
        user: user._id.toString(),
        job : job._id.toString(),
        startedApplicationAt: timestampService.createTimestamp(),
        status: userJobStatus
    }

    let jobPostApplicationResult = {
        email: job.contactEmail
    }   

    if (job.applicationLink){
        jobPostApplicationResult.url = job.applicationLink
    }
  
    let company = await mongoCompanyRepo.findOneById(job.company.id)

    let jobDataToTrack = {
        job_id : job._id.toString(),
        title : job.title,
        experience_level: job.experienceLevel,
        company_id : job.company.id,
        company_name: company.company_name,
        user_id : user._id.toString(),
        contact_email : job.contactEmail
    }

    amplitudeTracker.publishData('job_post_application_started', jobDataToTrack, req);
    await mongoUserJobsRepo.insert(applicationDetails);
    return userJobStatus;
}

module.exports = startApplicationJobPost;