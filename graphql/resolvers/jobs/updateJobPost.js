const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo'); //TODO
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const timestampService = require('../../../models/services/common/timestampService');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const ObjectID = require('mongodb').ObjectID;


const updateJobPost = async (root, {id ,form}, { req, res }) => {
    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to update job!', 403);
    }

    let sanitizedDescription = safeObjects.sanitizeHtml(form.description)
    let sanitizedCompanyDescription = safeObjects.sanitizeHtml(form.company.description)
    safeObjects.sanitize(form);
    form.description = sanitizedDescription;
    form.company.description = sanitizedCompanyDescription;

    id = safeObjects.sanitize(id);

    //TODO Error handling
    form.updated ={}
    form.updated.at = timestampService.createTimestamp();
    form.updated.by = req.user_id;
    
    //Find existing skills
    let recalculateUserApplicationStatus = false;
    let job =await mongoJobsRepo.findOneById(id);

    if(!job){
        errorUtils.throwError("Job not found", 404);
    }

    //Ensure right skills are being added
    if(form.skills){
        await jobUtils.addSkillsToJob(form, form);
    }

    if (job.skills.length !== form.skills.length){
        recalculateUserApplicationStatus = true;
    }        
    else{
        //check if any exiting skills were modified
        for (existingSkill of job.skills) {
            let found = false;
            for (updateSkill of form.skills) {
                if (updateSkill.id === existingSkill.id && updateSkill.level === existingSkill.level)
                    found = true;
            }
            if (!found) {
                recalculateUserApplicationStatus = true;
                break;
            }
        }

    }


    //Ensure creation of company if not present on the object
    await jobUtils.addCompanyToJob(form, form);  
    await mongoJobsRepo.findOneAndUpdatePosting({ _id: ObjectID(id) }, form);    
    form.id =id;
    await jobUtils.prepareJobObject(form);

    if (recalculateUserApplicationStatus) {
        await jobUtils.updateUserAppByJobId(id)
    }    

    return form;
}

module.exports = updateJobPost;