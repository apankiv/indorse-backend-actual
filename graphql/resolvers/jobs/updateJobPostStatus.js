const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const ObjectID = require('mongodb').ObjectID;
const timestampService = require('../../../models/services/common/timestampService');

const updateJobPostStatus = async (root, { id ,status }, { req, res }) => {
    safeObjects.sanitizeMultiple(id, status);

    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to update job status!', 403);
    }
    
    let updateObject ={}
    updateObject.approved = {}    
    updateObject.approved.approved = status
    updateObject.approved.at = timestampService.createTimestamp();
    updateObject.approved.by = req.user_id;

    await mongoJobsRepo.update({ _id: ObjectID(id)}, {$set : updateObject});
    let job = await mongoJobsRepo.findOneById(id);
    await jobUtils.prepareJobObject(job);

    return job;
}

module.exports = updateJobPostStatus;