const mongoJobsRepo = require('../../../models/services/mongo/mongoRepository')('jobs');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoRepository')('userjobs');
const mongooseUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const ObjectID = require('mongodb').ObjectID;

const userAppliedJobPosts = async (root, { pageNumber, pageSize, sort, search } , {req, res}) => {
    if (!jobUtils.userIsLoggedIn(req)) {
        errorUtils.throwError('Insufficient permission to list jobs!', 403);
    }

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }
    
    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let user = await mongooseUserRepo.findOneByEmail(req.email)

    //Find all jobs for this user
    let sortObj = jobUtils.prepareUserJobSortObj(sort);
    let userJobsCursor = await mongoUserJobsRepo.findAllWithCursor({ user: req.user_id});
    let sortedUserJobs = await userJobsCursor.sort(sortObj).skip(skip).limit(limit).toArray();
    let totalJobPosts = await userJobsCursor.count();

    var jobIdToPrepare =[]
    for (sortedJob of sortedUserJobs){
        jobIdToPrepare.push(ObjectID(sortedJob.job));
    }

    //TODO : Improve query to use aggregates to return data maintained in order
    // of jobIdToPrepare
    let unSortedJobs = await mongooseJobsRepo.findAll({ _id: { $in: jobIdToPrepare } });    
    let jobsToReturn = [];

    for (job of unSortedJobs){
        await jobUtils.prepareJobObject(job,req.user_id);
        let skillsMet = await jobUtils.userMeetsSkillRequirement(user.skills,job.skills);
        if(!skillsMet){
            delete job.contactEmail;
            delete job.applicationLink;            
        }
        jobsToReturn.push(job);
    }

    //Re-sort the jobs here as findAll of jobs may not return them in the order we wish
    //TODO: Ideally as a aggregate query under mongoose
    let resortedJobsToReturn = []
    for (jobId of jobIdToPrepare){
        let found = false;
        for(jobToReturn of jobsToReturn){
            if(found)
                continue;

            if(jobId.toString() ===  jobToReturn.id){
                resortedJobsToReturn.push(jobToReturn);
                found = true;
            }
        }
    }
    
    let responseObj = {
        jobPosts: resortedJobsToReturn,
        totalJobPosts: totalJobPosts,
        matchingJobPosts: totalJobPosts
    };
    
    return responseObj;
}

module.exports = userAppliedJobPosts;