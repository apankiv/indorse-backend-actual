const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators'); //TODO
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const stringUtils = require('../../../models/services/blob/stringUtils');
const rewardRoundingHelper = require('./rewardRoundingHelper');

const getLeaderboard = async (root, {pageNumber, pageSize, sort, search, period, skill}, {req, res}) => {

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!period) {
        period = 'global';
    }

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allValidatorCursor = await mongoValidatorsRepo.findAllWithCursor({
        _id: {$exists: true},
        delete: {$ne: true}
    });

    let totalValidators = await allValidatorCursor.count();

    let validatorCursor, matchingValidators;

    if (skill) {
        // let regex = stringUtils.createSafeRegexObject(search);

        let queryObj1 = {
            skills: skill.charAt(0).toUpperCase() + skill.slice(1),
            delete: {$ne: true}
        };

        if (search) {
            queryObj1['$or'] = [{'username': stringUtils.createSafeRegexObject(search)},
                {'ethaddress': stringUtils.createSafeRegexObject(search)},
                {'email': stringUtils.createSafeRegexObject(search)}]
        }

        validatorCursor = await mongoValidatorsRepo.findAllWithCursor(
            queryObj1
        );
        matchingValidators = validatorCursor.count();
    } else if (search) {
        let queryObj1 = {
            delete: {$ne: true}
        };

        queryObj1['$or'] = [{'username': stringUtils.createSafeRegexObject(search)},
            {'ethaddress': stringUtils.createSafeRegexObject(search)},
            {'email': stringUtils.createSafeRegexObject(search)}]

        validatorCursor = await mongoValidatorsRepo.findAllWithCursor(
            queryObj1
        );
        matchingValidators = validatorCursor.count();
    } else {
        validatorCursor = allValidatorCursor;
        matchingValidators = totalValidators;
    }

    let sortObj = prepareSortObj(sort, period, skill);
    let sortedLeaders = await validatorCursor.sort(sortObj).skip(skip).limit(limit).toArray();


    let leaderboardsToReturn = [];

    for (let leader of sortedLeaders) {

        let user = await mongooseUserRepo.findOneById(leader.user_id);


        let leaderToReturn;

        if (skill) {

            if (!leader[period + '_stats']) {
                leaderToReturn = {
                    vote_participation: 0,
                    indorsed: 0,
                    flagged: 0,
                    rewards: 0,
                    consensus: 0,
                    consensus_percentage: 0
                }
            } else {
                leaderToReturn = (leader[period + '_stats'][skill] ? leader[period + '_stats'][skill] : {
                    vote_participation: 0,
                    indorsed: 0,
                    flagged: 0,
                    rewards: 0,
                    consensus: 0,
                    consensus_percentage: 0
                })
            }

        } else {
            let stats = leader[period + '_stats'];

            leaderToReturn = {
                vote_participation: 0,
                indorsed: 0,
                flagged: 0,
                rewards: 0,
                consensus: 0,
                consensus_percentage: 0
            };

            let counter = 0;
            let totalVotes = 0;

            Object.keys(stats).forEach(function (skillName) {
                counter++;
                let voteParticipation = stats[skillName].vote_participation;
                leaderToReturn.vote_participation += voteParticipation;
                totalVotes += voteParticipation;
                leaderToReturn.indorsed += stats[skillName].indorsed;
                leaderToReturn.flagged += stats[skillName].flagged;
                leaderToReturn.rewards += stats[skillName].rewards;
                leaderToReturn.consensus += stats[skillName].consensus;

                //Weighted
                leaderToReturn.consensus_percentage += (voteParticipation * stats[skillName].consensus_percentage);
            });

            if (counter) {
                leaderToReturn.consensus_percentage /= totalVotes;
            }
        }

        leaderToReturn.consensus_percentage = Math.floor(leaderToReturn.consensus_percentage);
        leaderToReturn.user_id = leader.user_id;
        leaderToReturn.skills = leader.skills;
        leaderToReturn.tier = leader.tier;

        if (user) {
            if (user.img_url) {
                leaderToReturn.img_url = 'https://' + user.img_url;
            }

            leaderToReturn.username = user.username;

            leaderboardsToReturn.push(leaderToReturn);
        } else {
            await mongoValidatorsRepo.deleteOne({_id: leader._id});
        }
    }

    return {
        leaderboard: leaderboardsToReturn,
        totalValidators: totalValidators,
        matchingValidators: matchingValidators
    };
};

function prepareSortObj(sort, period, skill) {
    let field = skill ? period + '_stats.' + skill + '.vote_participation' : period + '_stats.vote_participation';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }
    }

    let statFields = ['invited', 'vote_participation', 'indorsed', 'flagged', 'rewards', 'consensus', 'consensus_percentage'];

    for (let i = 0; i < statFields.length; i++) {
        if (field === statFields[i]) {
            field = skill ? period + '_stats.' + skill + '.' + field : field;
            break;
        }
    }

    return {[field]: order};
}


module.exports = getLeaderboard;