const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators'); //TODO
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const stringUtils = require('../../../models/services/blob/stringUtils');
const rewardRoundingHelper = require('./rewardRoundingHelper');

const getLeaderboard = async (root, {pageNumber, pageSize, sort, search}, {req, res}) => {

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allValidatorCursor = await mongoValidatorsRepo.findAllWithCursor({
        _id: {$exists: true},
        vote_participation: {$gt: 0},
        delete: {$ne: true}
    });
    let totalValidators = await allValidatorCursor.count();

    let validatorCursor, matchingValidators;

    if (search) {
        let regex = stringUtils.createSafeRegexObject(search);

        validatorCursor = await mongoValidatorsRepo.findAllWithCursor({
            $or: [
                {voter_id: regex},
                {skills: {$elemMatch: regex}}
            ]
        });
    } else {
        validatorCursor = allValidatorCursor;
        matchingValidators = totalValidators;
    }

    let sortObj = prepareSortObj(sort);
    let sortedLeaders = await validatorCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    let leaderboardsToReturn = [];

    for (let leader of sortedLeaders) {

        let user = await mongooseUserRepo.findOneById(leader.user_id);

        if (user) {
            leader.rewards = rewardRoundingHelper.rewardRounding(leader.rewards);
            if (user.img_url) {
                leader.img_url = 'https://' + user.img_url;
            }

            leader.username = user.username;

            leaderboardsToReturn.push(leader);
        } else {
            await mongoValidatorsRepo.deleteOne({_id: leader._id});
        }
    }

    return {
        leaderboard: leaderboardsToReturn,
        totalValidators: totalValidators,
        matchingValidators: matchingValidators
    };
};

function prepareSortObj(sort) {
    let field = 'vote_participation';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }
    }
    return {[field]: order};
}


module.exports = getLeaderboard;