const mongooseValidatorRanksRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRanksRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const rewardRoundingHelper = require('./rewardRoundingHelper');

const getTopRanks = async (root, {numberOfRankers}, {req, res}) => {
    let rankersDocs = await mongooseValidatorRanksRepo.findRecentRank(); // TODO: add timestamp as index in MongoDB
    let rankersArray = rankersDocs[0].rewardOneWeekRank;

    let rankersWithDetails = [];

    if(numberOfRankers>3){
        numberOfRankers = 3; // to prevent returning all the rewards details
    }

    if(numberOfRankers>rankersArray.length){
        numberOfRankers = rankersArray.length;
    }

    for (var i = 0; i < numberOfRankers; i++) {
        let curRankerId = rankersArray[i].user_id;
        let user = await mongooseUserRepo.findOneById(curRankerId);
        let currentRole;
        if(user.work && user.work[0]){
            let firstWork = user.work[0];
            currentRole = firstWork.title;
        }
        if(!currentRole){
            let validatorDocs = await mongooseValidatorRepo.findOne({user_id: curRankerId});
            if(validatorDocs.skills && validatorDocs.skills[0]){
                currentRole = validatorDocs.skills[0]+' validator'; // TODO: Multiple skills then add them with comma
            }
        }
        if(!currentRole){
            currentRole = 'Indorse validator';
        }


        let detailObj = {
            user_id: curRankerId,
            name: user.name,
            username: user.username,
            current_role: currentRole,
            rewards: rewardRoundingHelper.rewardRounding(rankersArray[i].scoreThisWeek)
        };
        if(user.img_url){
            detailObj.img_url = 'https://'+user.img_url
        }
        rankersWithDetails.push(detailObj);
    }

    return rankersWithDetails;
}



module.exports = getTopRanks;