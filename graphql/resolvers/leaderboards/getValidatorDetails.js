const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const getPastVotesService = require('../../../models/votes/getPastVotesService');
const checkIfAdminVoted = require('../../../models/votes/adminVoteOnCommentService');

const getValidatorDetails = async (root, {userId, indorsed, pageNumber, pageSize}, {req, res}) => {
    let validator = await mongooseValidatorRepo.findOne({user_id: userId});

    if (!validator) {
        errorUtils.throwError("validator does not found", 404);
    }

    if(!pageNumber) {
        pageNumber = 1;
    }

    if(!pageSize) {
        pageSize = 10
    }

    let queryObj = {
        voted_at : {$gt : 0},
        sc_vote_exists : true
    };

    if (indorsed === "indorsed") {
        queryObj.endorsed = true;
    } else if (indorsed === "flagged") {
        queryObj.endorsed = false;
    }

    let user = await mongooseUserRepo.findOneById(validator.user_id);

    if (user) {
        if (user.img_url) {
            validator.img_url = 'https://' + user.img_url;
        }

        validator.username = user.username;
    }

    let returnObj =  await getPastVotesService.getPastVotes(userId, pageNumber, pageSize, queryObj);
    // then attach downvote hidden detail to this votes.
    // returnObj.votedClaims[i].feedback
    console.log('RETURN OBJ ' + JSON.stringify(returnObj));
    for(let i = 0 ; i < returnObj.votedClaims.length ; i ++){
        let vote = returnObj.votedClaims[i];
        let downVoteDetail;
        if(vote.feedback){
            // THEN, Get a vote.
            downVoteDetail = await mongooseVoteRepo.findOneById(vote.voteId);
            let voteHiddenUsername;
            if(downVoteDetail.hide.actionBy[0]){
                let user = await mongooseUserRepo.findOneById(downVoteDetail.hide.actionBy[0]);
                if(user.username){
                    voteHiddenUsername = user.username;
                }else{
                    voteHiddenUsername = downVoteDetail.hide.actionBy[0];
                }
            }else{
                voteHiddenUsername = '';
            }

            returnObj.votedClaims[i].feedback.isAdminVoteOnComment = checkIfAdminVoted.checkIfUserVoted(downVoteDetail, req.user_id);
            returnObj.votedClaims[i].feedback.isCommentHidden = downVoteDetail.hide.isHidden;
            returnObj.votedClaims[i].feedback.voteHiddenUsername = voteHiddenUsername;
        }
    }

    returnObj.validator = validator;

    return returnObj;
};

module.exports = getValidatorDetails;
