module.exports = `

    type Feedback {
        quality: Int
        designPatterns: Int
        gitFlow: Int
        completion: Int
        explanation: String
        isAdminVoteOnComment: Boolean
        isCommentHidden: Boolean
        voteHiddenUsername: String
    }

    type VotedValidatorClaim {
        voteStatus: VoteStatus,
        votedAt: Int,
        adminUpvote : Int,
        adminDownvote : Int,
        voteId: String,
        reward: Int,
        claim: Claim,
        feedback : Feedback
    }

	type Leader {
	    _id: String
	    user_id: String
	    rewards: String
	    vote_participation: Int
	    invited: Int
	    indorsed : Int
	    flagged : Int
	    consensus : Int
	    consensus_percentage: Int
	    img_url : String
	    skills : [String]
	    username : String
	    tier: TierLevel
	}
	
	enum Period {
		daily
		weekly
		monthly
		yearly
	}
	
	type Ranker {
		user_id: String
		name: String
		username: String
		img_url: String
		current_role: String
		rewards: String
	}
	
	type Leaderboard {
		leaderboard: [Leader]
		matchingValidators: Int
		totalValidators: Int!
	}
	
	type ValidatorDetailsResponse {
	    votedClaims: [VotedValidatorClaim]
        totalPage: Int
	    validator : Leader
	}
	
	enum Indorsed {
	    indorsed
	    flagged
	}
	
	type Query {
	    getLeaderboard(pageNumber: Int, pageSize: Int, sort: String, search: String): Leaderboard
	    getAdminValidatorsList(pageNumber: Int, pageSize: Int, sort: String, search: String, period : Period, skill : String): Leaderboard @permissionCheck(roles: ["admin"])
	    leaderboards_getTopRanks(numberOfRankers: Int) : [Ranker]
	    getValidatorDetails(userId: String!, indorsed : Indorsed, pageNumber: Int, pageSize: Int) : ValidatorDetailsResponse @permissionCheck(roles: ["admin"])
		# new_query (Do not remove this line)
	}
	`;