function rewardRounding(rewards) {
    let rewardRoundedString;
    if (rewards < 10) {
        rewardRoundedString = ">$1";
    } else if (rewards < 25) {
        rewardRoundedString = ">$10";
    } else if (rewards < 50) {
        rewardRoundedString = ">$25";
    } else if (rewards < 100) {
        rewardRoundedString = ">$50";
    } else if (rewards < 200) {
        rewardRoundedString = ">$100";
    } else if (rewards < 400) {
        rewardRoundedString = ">$200";
    } else if (rewards < 800) {
        rewardRoundedString = ">$400";
    } else if(rewards < 1500){
        rewardRoundedString = ">$800";
    }else{
        let remainder = rewards % 500;
        rewards = rewards-remainder;
        rewardRoundedString = ">$"+String(rewards)
    }
    return rewardRoundedString;
}

exports.rewardRounding = rewardRounding;
