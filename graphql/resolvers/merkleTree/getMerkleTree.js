const mongooseMerkleTreeRepo = require('../../../models/services/mongo/mongoose/mongooseMerkleTreeRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseClaimSignatureRepo = require('../../../models/services/mongo/mongoose/mongooseClaimSignaturesRepo');
const mongooseVoteSignatureRepo = require('../../../models/services/mongo/mongoose/mongooseVoteSignaturesRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const safeObjects = require('../../../models/services/common/safeObjects');
const claims = require('../../../models/services/mongo/mongoose/schemas/claims');
const timestampService = require('../../../models/services/common/timestampService')
const claimSerivce  = require('../../../models/services/claims/getClaimService')

const getMerkleTree = async (root, { timestamp, claim_id}, {req, res}) => {
    let merkleTreeList = []
    let formattedMerkleTreeList = []
    let timestamp_to;
    let timestamp_from;

    if(timestamp){
        timestamp = safeObjects.sanitize(timestamp);
        let date = new Date(timestamp * 1000);        

        timestamp_from = timestampService.normalizeToStartOfDay(date);
        let futureDate = new Date(date.setDate(date.getDate() + 1))
        timestamp_to  = timestampService.normalizeToStartOfDay(futureDate);

        let merkleTree = await mongooseMerkleTreeRepo.findAll({ timestamp : { $gte: timestamp_from, $lt: timestamp_to}});
        if(!merkleTree)
            errorUtils.throwError("Merkle tree with the key not found",404)

        merkleTreeList = merkleTreeList.concat(merkleTree);
    } else if(claim_id){        
        claim_id  = safeObjects.sanitize(claim_id);
        let merkleTree = await mongooseMerkleTreeRepo.findAll({claim_ids :{$elemMatch : {$eq : claim_id}}});        

        if(!merkleTree)
            errorUtils.throwError("Merkle tree for claim id " + claim_id + " not found",404);

        merkleTreeList = merkleTreeList.concat(merkleTree); 
    }    

    for (merkleTree of merkleTreeList){
       
        let treeToAdd = {
            claim_ids : merkleTree.claim_ids,
            roothash  : merkleTree.rootHash,
            timestamp_to : timestamp_to,
            timestamp_from : timestamp_from     
        }

        let leaves = await formatLeaves(merkleTree.leaves);

        treeToAdd.leaves = leaves;
        formattedMerkleTreeList.push(treeToAdd)
    }
    return formattedMerkleTreeList;  
};


async function formatLeaves(leaves){
    let returnLeaves = []
    for(leaf of leaves){
        let formattedLeaf ={
            data : leaf.data,
            type : leaf.type
        };

        if(leaf.type ==='claim'){
            formattedLeaf.claim =await formatMerkleClaim(leaf);
        }  else if(leaf.type ==='vote'){
            formattedLeaf.vote =await formatMerkleVote(leaf);
        }
        returnLeaves.push(formattedLeaf);
    }
    return returnLeaves;
}


async function formatMerkleClaim(claimLeaf){
    let formattedClaim ={}
    let claim = await mongooseClaimsRepo.findOne({_id: claimLeaf.reference.claim_id});

    if(!claim)
        errorUtils.throwError("Claim not found " + claimLeaf.reference.claim_id)

 
    formattedClaim.type = claim.type
    if(claim.type === claims.types.ASSIGNMENT){
        let userAssignment = await mongooseUserAssignmentRepo.findOneById(claim.user_assignment_id)
        
        if(!userAssignment)
            errorUtils.throwError("Unable to find user assignment for id" + claim.user_assignment_id);

        let skillNames =""
        for(skill of userAssignment.skills){
            let matchedSkill  = await mongooseSkillRepo.findOneById(skill.skill);
            skillNames += matchedSkill.name + ","
        }
        formattedClaim.title = skillNames.substr(0,skillNames.length -1);
        
    } else {
        formattedClaim.title  = claim.title
    }

    formattedClaim.id  = claim._id.toString();
    
    formattedClaim.level = claim.level
    formattedClaim.owner = claim.ownerid

    if(claim.ownerid){
        let user = await mongooseUserRepo.findOne({_id:claim.ownerid})
        if(user){
            formattedClaim.username = user.username
        }        
    }

    formattedClaim.endorse_count = claim.endorse_count
    formattedClaim.flag_count = claim.flag_count
    formattedClaim.created_at = claim.created_at
    formattedClaim.tags = claim.tags
    formattedClaim.proof = claim.proof
    formattedClaim.claim_status = await claimSerivce.getClaimStatus(claim);
    return formattedClaim;
}

async function formatMerkleVote(voteLeaf){
    let formattedVote = {}

    let voteSignature = await mongooseVoteSignatureRepo.findOne({_id : voteLeaf.reference.vote_signature_id});
    if(!voteSignature)
        errorUtils.throwError("Vote signature not found for vote " + voteLeaf.reference.vote_signature_id)

    formattedVote.id = voteLeaf.reference.vote_signature_id
    formattedVote.claim_id = voteLeaf.reference.claim_id
    formattedVote.endorsed = (voteSignature.payload.decision === 'YES') ? true : false
    formattedVote.timestamp = voteSignature.payload.timestamp
    formattedVote.skill = voteSignature.payload.skill
    return formattedVote;
}


module.exports = getMerkleTree;