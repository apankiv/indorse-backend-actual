module.exports = `

	type MerkleClaim{
		id : String!
		type : String!
		title : String!
		level : String!
		owner : String!
		username : String!
		endorse_count : Int
		flag_count : Int
		claim_status : String
		created_at : Int
		tags :[String]
		proof : String!
	}

	enum MerkleVoteType{
		vote
		claim
	}

	type MerkleVote{
		id : String!
		claim_id : String!
		endorsed : Boolean!
		timestamp : Int
		skill : String
	}

	type MerkleLeaf {
		data : String!
		type : MerkleVoteType
		claim : MerkleClaim
		vote :  MerkleVote
	}

	type MerkleTree{
		claim_ids  : [String]!
		roothash : String!
		timestamp_to : Int
		timestamp_from : Int
		leaves : [MerkleLeaf]
	}

	type Query {
		getMerkleTree(timestamp: Int, claim_id : String ): [MerkleTree] @permissionCheck(roles: ["full_access","profile_access","admin"])   
	}

`;