const composeWithMongoose = require('graphql-compose-mongoose/node8').composeWithMongoose;
const schemaComposer = require('graphql-compose').schemaComposer;
const ClaimGithubsSchema = require('../../../models/services/mongo/mongoose/schemas/claimGithub');
const mongoose = require('mongoose');
const ClaimGithubsModel = mongoose.model('claimGithubs', ClaimGithubsSchema);

const customizationOptions = {};


// Auto generate type definition from Mongoose ClaimGithub schema
const ClaimGithubsTC = composeWithMongoose(ClaimGithubsModel, customizationOptions);


// Add customised resolver
ClaimGithubsTC.addResolver({
    kind: 'query',
    name: 'findByClaimId',
    args: {
        filter: `input ClaimGithubInput {
      claim_id: String!
    }`

    },
    type: ClaimGithubsTC.getType(),
    resolve: async ({ args, context }) => {
        return {
            schemaType: await ClaimGithubsModel.findOne({claim_id: args.filter.claim_id}).lean()
        }

    },
});

schemaComposer.rootQuery().addFields({
    getClaimantGithub: ClaimGithubsTC.getResolver('findByClaimId'),
});


const schemaDefault = schemaComposer.buildSchema();
module.exports = schemaDefault;