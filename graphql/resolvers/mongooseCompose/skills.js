/*
    Sample file
 */

const composeWithMongoose = require('graphql-compose-mongoose/node8').composeWithMongoose;
const schemaComposer = require('graphql-compose').schemaComposer;
const SkillsSchema = require('../../../models/services/mongo/mongoose/schemas/skill');
const mongoose = require('mongoose');
const SkillsModel = mongoose.model('skills', SkillsSchema);

const errorUtils = require('../../../models/services/error/errorUtils');

const customizationOptions = {};
const SkillsTC = composeWithMongoose(SkillsModel, customizationOptions);

//TODO extract those to services
const mustBeAdmin = next => req => {
    // req.context.headers.authorization mongoose schema.
    if(!(req.login && req.permissions.admin && req.permissions.admin.write)){
        errorUtils.throwError('Not allowed',403);
    }
    return next(req)
};

schemaComposer.rootQuery().addFields({
    skillById: SkillsTC.getResolver('findById').wrapResolve(mustBeAdmin),
    skillByIds: SkillsTC.getResolver('findByIds'),
    skillOne: SkillsTC.getResolver('findOne'),
    skillMany: SkillsTC.getResolver('findMany'),
    skillCount: SkillsTC.getResolver('count'),
    skillConnection: SkillsTC.getResolver('connection'),
    skillPagination: SkillsTC.getResolver('pagination'),
});



schemaComposer.rootMutation().addFields({
    skillCreate: SkillsTC.getResolver('createOne').wrapResolve(mustBeAdmin),
    skillUpdateById: SkillsTC.getResolver('updateById').wrapResolve(mustBeAdmin),
    skillUpdateOne: SkillsTC.getResolver('updateOne').wrapResolve(mustBeAdmin),
    skillUpdateMany: SkillsTC.getResolver('updateMany').wrapResolve(mustBeAdmin),
    skillRemoveById: SkillsTC.getResolver('removeById').wrapResolve(mustBeAdmin),
    skillRemoveOne: SkillsTC.getResolver('removeOne').wrapResolve(mustBeAdmin),
    skillRemoveMany: SkillsTC.getResolver('removeMany').wrapResolve(mustBeAdmin)
});

const graphqlSchema = schemaComposer.buildSchema();

module.exports = graphqlSchema;