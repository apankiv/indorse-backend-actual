const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const safeObjects = require('../../../models/services/common/safeObjects');
const timestampService = require('../../../models/services/common/timestampService');
const getDomainSchemaService = require('../../../models/services/signatures/getVoteDomainSchemaService');
const getVoteSchemaService = require('../../../models/services/signatures/getVoteSchemaService');
const getDomainService = require('../../../models/services/signatures/getVoteDomainService');

const getVotePayload = async (root, {voteId, decision, skill}, {req, res}) => {

    voteId = safeObjects.sanitize(voteId);
    decision = safeObjects.sanitize(decision);
    skill = safeObjects.sanitize(skill);

    if(skill.match(/^[a-f\d]{24}$/i)) {
        let skillDoc = await mongooseSkillRepo.findOneById(skill);

        if(!skillDoc) {
            throw new Error("Skill not found!");
        }

        skill = skillDoc.name;
    }

    skill = skill.toLowerCase();

    let vote = await mongooseVoteRepo.findOne({_id : voteId});

    if(!vote) {
        errorUtils.throwError("Vote not found!", 404);
    }

    let voteMsg = {
        claim_id : vote.claim_id.toString(),
        decision : decision,
        timestamp : timestampService.createTimestamp(),
        version : "1",
        skill : skill
    };

    let payload = {};
    let types = {};

    types.EIP712Domain = await getDomainSchemaService.getSchema();
    types.Vote = await getVoteSchemaService.getSchema();

    payload.types = types;
    payload.domain = await getDomainService.getDomain();
    payload.primaryType = "Vote";
    payload.message = voteMsg;

    return payload;
};

module.exports = getVotePayload;