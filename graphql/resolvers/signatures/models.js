module.exports = `
	type Argument { 
	    name: String!
	    type: String!
	}
	
	type Types {
	    EIP712Domain : [Argument]!
	    Vote: [Argument]!
	}
	
	type VoteMsg {
	    claim_id: String!
	    decision: String!
	    timestamp: Int!
	    version: String!
	    skill: String!
	}
	
	input VoteMsgInput {
	    claim_id: String!
	    decision: String!
	    timestamp: Int!
	    version: String!
	    skill: String!
	}
	
	type Domain {
	    name: String!
	    version: String!
	    chainId: Int!
	    verifyingContract: String!
	    salt: String!
	}
	
	type VotePayload {
	    types: Types!
	    domain: Domain!
	    primaryType: String!
	    message: VoteMsg
	}
	
	type Query {
		getVotePayload(voteId: String!, decision: String!, skill: String!): VotePayload         
	}
	
	type Mutation {
	    submitVoteSignature(voteId : String!, message : VoteMsgInput!, v : Int!, r : String!, s : String!) : Boolean
	}
`;