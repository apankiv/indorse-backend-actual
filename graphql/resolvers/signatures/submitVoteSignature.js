const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteSignaturesRepo = require('../../../models/services/mongo/mongoose/mongooseVoteSignaturesRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const safeObjects = require('../../../models/services/common/safeObjects');
const timestampService = require('../../../models/services/common/timestampService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const getRewardService = require('../../../models/votes/getRewardService');
const getINDRewardService = require('../../../models/votes/getINDRewardService');
const checkIfRoundIsFinishedService = require('../../../models/votes/checkIfRoundIsFinishedService');
const finishVotingRoundsService = require('../../../models/votes/finishVotingRoundsService');
const updateClaimCountersService = require('../../../models/votes/updateClaimCountersService');
const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const { sendClaimResultToPartner } = require('../../../models/services/claims/sendClaimResultToPartner');

const submitVoteSignature = async (root, {voteId, message, v, r, s}, {req, res}) => {

    voteId = safeObjects.sanitize(voteId);
    v = safeObjects.sanitize(v);
    r = safeObjects.sanitize(r);
    s = safeObjects.sanitize(s);

    let vote = await mongooseVoteRepo.findOneById(voteId);
    let claim = await mongooseClaimsRepo.findOneById(vote.claim_id);

    amplitudeTracker.publishData('vote_signature_submission_initialized', {}, req);

    if (vote.voter_id !== req.user_id) {
        amplitudeTracker.publishData('vote_signature_submission_failed_not_users_vote', {}, req);
        errorUtils.throwError('Users can only modify votes they own!', 403);
    }

    if (!vote.voted_at) {
        amplitudeTracker.publishData('vote_signature_submission_failed_not_voted_yet', {}, req);
        errorUtils.throwError('User has not yet voted on Indorse!', 400);
    }

    if (vote.sc_vote_exists) {
        amplitudeTracker.publishData('vote_signature_submission_failed_sc_vote_exists', {}, req);
        errorUtils.throwError('Vote is already registered in the smart contract!', 400);
    }

    if (vote.signature_ref) {
        amplitudeTracker.publishData('vote_signature_submission_failed_signature exists', {}, req);
        errorUtils.throwError('Vote is already signed!', 400);
    }

    if ((vote.endorsed && message.decision !== "YES") || (!vote.endorsed && message.decision !== "NO")) {
        amplitudeTracker.publishData('vote_signature_submission_failed_decision_mismatch', {}, req);
        errorUtils.throwError('Vote decision mismatch!', 400);
    }

    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(vote.claim_id);

    let now = timestampService.createTimestamp();

    if (now > votingRound.end_voting) {
        amplitudeTracker.publishData('vote_signature_submission_failed_deadline_passed', {}, req);
        errorUtils.throwError('Voting deadline has passed!', 400);
    }

    let user = await mongooseUserRepo.findOneById(req.user_id);

    let signerAddress = await voteSignatureVerifierContract.verify(message.claim_id, message.decision,
        message.timestamp, message.version, message.skill, v, r, s
    );

    if (user.ethaddress.toLowerCase() !== signerAddress.toLowerCase()) {
        amplitudeTracker.publishData('vote_signature_submission_failed_signature invalid', {}, req);
        errorUtils.throwError('The signature is invalid!', 400);
    }

    let signature_ref = await mongooseVoteSignaturesRepo.insert({
        payload: message,
        signature: {
            v: v, r: r, s: s
        },
        claim_id: vote.claim_id,
        user_id: req.user_id,
        ethaddress: user.ethaddress,
        merkelized : false
    });

    let reward = await getRewardService.getReward(vote);
    let indReward = await getINDRewardService.getReward(vote);

    await mongooseVoteRepo.update({_id: voteId}, {
        $set: {
            sc_vote_exists: true,
            tx_timestamp: now,
            signature_ref: signature_ref,
            reward: reward,
            tx_success_timestamp: now,
            ind_reward : indReward
        }
    });

    await updateClaimCountersService.updateCounters(vote, claim);

    amplitudeTracker.publishData('vote_signature_submission_successful', {}, req);

    const roundIsFinished = await checkIfRoundIsFinishedService.check(vote.claim_id);

    if (roundIsFinished) {
        await finishVotingRoundsService.finishVotingRoundByClaimId(vote.claim_id);
    } else {
        await sendClaimResultToPartner(vote.claim_id).catch((error) => {
            console.log(error);
            console.log('Ignoring this error');
        });
    }

    return true;
};

module.exports = submitVoteSignature;