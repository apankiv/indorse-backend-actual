const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

const userHasSkillChildResolver = async (root, args, { req }) => {
    // if user is not logged in return false for this param
    if (!req.user_id) return false;

    const userId = req.user_id;
    const criteria = {
        _id: userId,
        skills: {
            $elemMatch: {
                'skill._id': (root._id || '').toString(),
            },
        },
    };
    const projection = {
        _id: 1,
    };
    const user = await mongooseUserRepo.findOne(criteria, projection);
    return !!user;
};

module.exports = userHasSkillChildResolver;
