const createSkillService = require('../../../models/services/skills/createSkillService');

const createSkill = async (root, { form }) => {
    const skill = await createSkillService.createSkill({ form });

    return skill;
};

module.exports = createSkill;
