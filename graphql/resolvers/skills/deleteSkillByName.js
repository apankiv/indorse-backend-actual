const deleteSkillService = require('../../../models/services/skills/deleteSkillService');

const deleteSkillByName = async (root, { name }) => {
    await deleteSkillService.deleteSkill({ name });
    return true;
};

module.exports = deleteSkillByName;
