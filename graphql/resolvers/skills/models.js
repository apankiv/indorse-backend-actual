module.exports = `
    input CreateSkillForm {
        name: String!
        category: String!
        iconData: String
        tags: [String!]
        parentNames: [String!]
    }

    input UpdateSkillForm {
        name: String!
        iconData: String
        tags: [String!]
        parentNames: [String!]
    }

    type Query {
        skillByName(name: String!): Skill @permissionCheck(roles: ["admin"])
    }

	type Mutation {
	    createSkill(form: CreateSkillForm!): Skill @permissionCheck(roles: ["admin"])
        updateSkillByName(form: UpdateSkillForm!): Skill @permissionCheck(roles: ["admin"])
        deleteSkillByName(id: String!): Boolean! @permissionCheck(roles: ["admin"])
    }
`;
