const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

const skillByName = async (root, { name }) => {
    // eslint-disable-next-line no-param-reassign
    const nameToSearch = name.toLowerCase();
    const skill = await mongooseSkillRepo.findOne({ name: nameToSearch });

    if (!skill) {
        errorUtils.throwError(`Skil with name '${name}' not found`, 404);
    }

    return skill;
};

module.exports = skillByName;
