const utils = require('../../../models/services/skills/skillUtils');

const skillParents = async (root) => {
    const skill = root;
    const parents = await utils.findExactSkillsByIds(skill.parents);
    return parents;
};

module.exports = skillParents;
