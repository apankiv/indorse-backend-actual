const updateSkillService = require('../../../models/services/skills/updateSkillService');

const updateSkillByName = async (root, { form }) => {
    // eslint-disable-next-line no-param-reassign
    form.name = form.name.toLowerCase();
    const skill = await updateSkillService.updateSkill({
        selector: { name: form.name },
        form,
    });

    return skill;
};

module.exports = updateSkillByName;
