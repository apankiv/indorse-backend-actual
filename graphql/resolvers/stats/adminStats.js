const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoClaimRepo = require('../../../models/services/mongo/mongoRepository')('claims');
const mongoVoteRepo = require('../../../models/services/mongo/mongoRepository')('votes');
const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils');
const claimReport = require('../../../smart-contracts/services/cron/gatherClaimStatistics')

const adminStats = async (root, {}, { req, res }) => {
    let date = new Date();

    let yesterday = new Date();
    yesterday.setDate(date.getDate() - 1);
    let oneDayTimestamp = Math.floor(yesterday.getTime() / 1000);
    
    let lastWeek = new Date();
    lastWeek.setDate(date.getDate() - 7);
    let oneWeekTimestamp = Math.floor(lastWeek.getTime() / 1000);
    
    let lastMonth = new Date();
    lastMonth.setMonth(date.getMonth() - 1);
    let oneMonthTimestamp = Math.floor(lastMonth.getTime() / 1000);
    
    let lastYear = new Date();
    lastYear.setFullYear(date.getFullYear() - 1);
    let oneYearTimestamp = Math.floor(lastYear.getTime() / 1000);
    
    // Users stats =========
    let allUsersCursor = await mongoUserRepo.findAllWithCursor({ _id: { $exists: true } });
    let totalUsers = await allUsersCursor.count();

    let oneDayUsersCursor = await mongoUserRepo.findAllWithCursor({ _id: { $exists: true }, timestamp: { $gt: oneDayTimestamp } });
    let oneDayUsers = await oneDayUsersCursor.count();

    let oneWeekUsersCursor = await mongoUserRepo.findAllWithCursor({ _id: { $exists: true }, timestamp: { $gt: oneWeekTimestamp } });
    let oneWeekUsers = await oneWeekUsersCursor.count();

    let oneMonthUsersCursor = await mongoUserRepo.findAllWithCursor({ _id: { $exists: true }, timestamp: { $gt: oneMonthTimestamp } });
    let oneMonthUsers = await oneMonthUsersCursor.count();

    let oneYearUsersCursor = await mongoUserRepo.findAllWithCursor({ _id: { $exists: true }, timestamp: { $gt: oneYearTimestamp } });
    let oneYearUsers = await oneYearUsersCursor.count();

    let UserStats = {
        oneDay: oneDayUsers,
        oneWeek: oneWeekUsers,
        oneMonth: oneMonthUsers,
        oneYear: oneYearUsers,
        overall: totalUsers
    }
    // Users stats done =========

    // Claims created stats start =============
    let allClaimsCreatedCursor = await mongoClaimRepo.findAllWithCursor({ _id: { $exists: true } });
    let totalClaimsCreated = await allClaimsCreatedCursor.count();

    let oneDayClaimsCreatedCursor = await mongoClaimRepo.findAllWithCursor({ $or: [{ created_at: { $gt: oneDayTimestamp } }, { approved: { $gt: oneDayTimestamp } }, { disapproved: { $gt: oneDayTimestamp } }] });
    let oneDayClaimsCreated = await oneDayClaimsCreatedCursor.count();

    let oneWeekClaimsCreatedCursor = await mongoClaimRepo.findAllWithCursor({ $or: [{ created_at: { $gt: oneWeekTimestamp } }, { approved: { $gt: oneWeekTimestamp } }, { disapproved: { $gt: oneWeekTimestamp } }] });
    let oneWeekClaimsCreated = await oneWeekClaimsCreatedCursor.count();

    let oneMonthClaimsCreatedCursor = await mongoClaimRepo.findAllWithCursor({ $or: [{ created_at: { $gt: oneMonthTimestamp } }, { approved: { $gt: oneMonthTimestamp } }, { disapproved: { $gt: oneMonthTimestamp } }] });
    let oneMonthClaimsCreated = await oneMonthClaimsCreatedCursor.count();

    let oneYearClaimsCreatedCursor = await mongoClaimRepo.findAllWithCursor({ $or: [{ created_at: { $gt: oneYearTimestamp } }, { approved: { $gt: oneYearTimestamp } }, { disapproved: { $gt: oneYearTimestamp } }] });
    let oneYearClaimsCreated = await oneYearClaimsCreatedCursor.count();

    let ClaimsCreated = {
        oneDay: oneDayClaimsCreated,
        oneWeek: oneWeekClaimsCreated,
        oneMonth: oneMonthClaimsCreated,
        oneYear: oneYearClaimsCreated,
        overall: totalClaimsCreated
    }
    // Claims created stats done =============

    // Claims Approved stats =============
    let allClaimsApprovedCursor = await mongoClaimRepo.findAllWithCursor({ approved: { $exists: true } });
    let totalClaimsApproved = await allClaimsApprovedCursor.count();

    let oneDayClaimsApprovedCursor = await mongoClaimRepo.findAllWithCursor({ approved: { $exists: true }, $or: [{ created_at: { $gt: oneDayTimestamp } }, { approved: { $gt: oneDayTimestamp } }, { disapproved: { $gt: oneDayTimestamp } }] });
    let oneDayClaimsApproved = await oneDayClaimsApprovedCursor.count();

    let oneWeekClaimsApprovedCursor = await mongoClaimRepo.findAllWithCursor({ approved: { $exists: true }, $or: [{ created_at: { $gt: oneWeekTimestamp } }, { approved: { $gt: oneWeekTimestamp } }, { disapproved: { $gt: oneWeekTimestamp } }] });
    let oneWeekClaimsApproved = await oneWeekClaimsApprovedCursor.count();

    let oneMonthClaimsApprovedCursor = await mongoClaimRepo.findAllWithCursor({ approved: { $exists: true }, $or: [{ created_at: { $gt: oneMonthTimestamp } }, { approved: { $gt: oneMonthTimestamp } }, { disapproved: { $gt: oneMonthTimestamp } }] });
    let oneMonthClaimsApproved = await oneMonthClaimsApprovedCursor.count();

    let oneYearClaimsApprovedCursor = await mongoClaimRepo.findAllWithCursor({ approved: { $exists: true }, $or: [{ created_at: { $gt: oneYearTimestamp } }, { approved: { $gt: oneYearTimestamp } }, { disapproved: { $gt: oneYearTimestamp } }] });
    let oneYearClaimsApproved = await oneYearClaimsApprovedCursor.count();

    let ClaimsApproved = {
        oneDay: oneDayClaimsApproved,
        oneWeek: oneWeekClaimsApproved,
        oneMonth: oneMonthClaimsApproved,
        oneYear: oneYearClaimsApproved,
        overall: totalClaimsApproved
    }
    // Claims Approved stats done =============

    // Claims Indorsed stats =============
    let allClaimsIndorsedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: true });
    let totalClaimsIndorsed = await allClaimsIndorsedCursor.count();

    let oneDayClaimsIndorsedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: true, $or: [{ created_at: { $gt: oneDayTimestamp } }, { approved: { $gt: oneDayTimestamp } }, { disapproved: { $gt: oneDayTimestamp } }] });
    let oneDayClaimsIndorsed = await oneDayClaimsIndorsedCursor.count();

    let oneWeekClaimsIndorsedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: true, $or: [{ created_at: { $gt: oneWeekTimestamp } }, { approved: { $gt: oneWeekTimestamp } }, { disapproved: { $gt: oneWeekTimestamp } }] });
    let oneWeekClaimsIndorsed = await oneWeekClaimsIndorsedCursor.count();

    let oneMonthClaimsIndorsedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: true, $or: [{ created_at: { $gt: oneMonthTimestamp } }, { approved: { $gt: oneMonthTimestamp } }, { disapproved: { $gt: oneMonthTimestamp } }] });
    let oneMonthClaimsIndorsed = await oneMonthClaimsIndorsedCursor.count();

    let oneYearClaimsIndorsedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: true, $or: [{ created_at: { $gt: oneYearTimestamp } }, { approved: { $gt: oneYearTimestamp } }, { disapproved: { $gt: oneYearTimestamp } }] });
    let oneYearClaimsIndorsed = await oneYearClaimsIndorsedCursor.count();

    let ClaimsIndorsed = {
        oneDay: oneDayClaimsIndorsed,
        oneWeek: oneWeekClaimsIndorsed,
        oneMonth: oneMonthClaimsIndorsed,
        oneYear: oneYearClaimsIndorsed,
        overall: totalClaimsIndorsed
    }
    // Claims Indorsed stats done =============

    // Claims Flagged stats =============
    let allClaimsFlaggedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: false });
    let totalClaimsFlagged = await allClaimsFlaggedCursor.count();

    let oneDayClaimsFlaggedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: false, $or: [{ created_at: { $gt: oneDayTimestamp } }, { approved: { $gt: oneDayTimestamp } }, { disapproved: { $gt: oneDayTimestamp } }] });
    let oneDayClaimsFlagged = await oneDayClaimsFlaggedCursor.count();

    let oneWeekClaimsFlaggedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: false, $or: [{ created_at: { $gt: oneWeekTimestamp } }, { approved: { $gt: oneWeekTimestamp } }, { disapproved: { $gt: oneWeekTimestamp } }] });
    let oneWeekClaimsFlagged = await oneWeekClaimsFlaggedCursor.count();

    let oneMonthClaimsFlaggedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: false, $or: [{ created_at: { $gt: oneMonthTimestamp } }, { approved: { $gt: oneMonthTimestamp } }, { disapproved: { $gt: oneMonthTimestamp } }] });
    let oneMonthClaimsFlagged = await oneMonthClaimsFlaggedCursor.count();

    let oneYearClaimsFlaggedCursor = await mongoClaimRepo.findAllWithCursor({ final_status: false, $or: [{ created_at: { $gt: oneYearTimestamp } }, { approved: { $gt: oneYearTimestamp } }, { disapproved: { $gt: oneYearTimestamp } }] });
    let oneYearClaimsFlagged = await oneYearClaimsFlaggedCursor.count();

    let ClaimsFlagged = {
        oneDay: oneDayClaimsFlagged,
        oneWeek: oneWeekClaimsFlagged,
        oneMonth: oneMonthClaimsFlagged,
        oneYear: oneYearClaimsFlagged,
        overall: totalClaimsFlagged
    }
    // Claims Flagged stats done =============

    let responseObj = {
        userStats: UserStats,
        claimsCreated: ClaimsCreated,
        claimsApproved: ClaimsApproved,
        claimsIndorsed: ClaimsIndorsed,
        claimsFlagged: ClaimsFlagged
    }
    return responseObj;
}



module.exports = adminStats;