module.exports = `
    type UserStats {
	    oneDay: Int,
	    oneWeek: Int,
	    oneMonth: Int,
	    oneYear: Int,
	    overall: Int
	}

	type ClaimsCreated {
		oneDay: Int,
		oneWeek: Int,
		oneMonth: Int,
		oneYear: Int,
		overall: Int
	}

	type ClaimsApproved {
		oneDay: Int,
		oneWeek: Int,
		oneMonth: Int,
		oneYear: Int,
		overall: Int
	}

	type ClaimsIndorsed {
		oneDay: Int,
		oneWeek: Int,
		oneMonth: Int,
		oneYear: Int,
		overall: Int
	}

	type ClaimsFlagged {
		oneDay: Int,
		oneWeek: Int,
		oneMonth: Int,
		oneYear: Int,
		overall: Int
	}

	type Stats {
		userStats: UserStats,
		claimsCreated: ClaimsCreated,
		claimsApproved: ClaimsApproved,
		claimsIndorsed: ClaimsIndorsed,
		claimsFlagged: ClaimsFlagged
	}
	
    type Query {
		adminStats: Stats @permissionCheck(roles: ["admin"])
	}
	
`	