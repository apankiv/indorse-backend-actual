const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const createClaimService = require('../../../models/services/claims/createClaimService');
const safeObjects = require('../../../models/services/common/safeObjects');
const socialSignup = require("../../../models/services/social/socialSignupService");
const jwt = require('jsonwebtoken');
const config = require('config');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const mongoUserRepo = require("../../../models/services/mongo/mongoRepository")('users');
const mongooseUserGithubRepo = require("../../../models/services/mongo/mongoose/mongooseUserGithubRepo");
const routeUtils = require('../../../models/services/common/routeUtils');
const validate = require('../../../models/services/common/validate');
const newUserProcedures = require('../../../models/services/auth/newUserProcedure');
const logger = require('../../../models/services/common/logger').getLogger();
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const githubUtils = require("../../../models/services/social/githubService");
const socialLogin = require("../../../models/services/social/socialLoginService");

const githubGetRepos = async (root, {languages}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let languages_filter = [];

    languages.forEach(function(language){
        languages_filter.push(language.toLowerCase());
    })

    let returnAllRepos = false;
    if(languages.length == 0){
        returnAllRepos = true;
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if(!user)
    {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let user_repos = [];

    if(!user.github_uid)
    {
        errorUtils.throwError('User has no connected Github account', 403);
    }

    let github_name = null;
    let github_username = null;
    let github_avatar = null;
    let userGithubData = await mongooseUserGithubRepo.findOneByGithubUid(user.github_uid);
    if(userGithubData && userGithubData.repositories && userGithubData.repositories.edges && userGithubData.repositories.edges.length > 0)
    {

        github_name = userGithubData.name;
        github_username = userGithubData.login;
        github_avatar = userGithubData.avatarUrl;
        let repos = userGithubData.repositories.edges;
        for(let repo of repos){

            let user_repo = {};
            user_repo.title = repo.node.name;
            user_repo.description = repo.node.description;
            user_repo.last_updated_at = repo.node.updatedAt;
            user_repo.url = repo.node.projectsUrl;
            if(user_repo.url.substring(user_repo.url.length - 8,user_repo.url.length) === "projects")
            {
                user_repo.url = user_repo.url.substring(0,user_repo.url.length - 8);
            }
            user_repo.skills = "";
            let filter_pass = false;
            if(languages_filter.length <= 0)
            {
                filter_pass = true;
            }
            repo.node.languages.nodes.forEach(function(language){

                user_repo.skills = user_repo.skills + language.name + ", ";
                if(languages_filter.indexOf(language.name.toLowerCase()) > -1)
                {
                    filter_pass = true;
                }
            });

            if(user_repo.skills.length > 0)
            {
                user_repo.skills = user_repo.skills.substring(0, user_repo.skills.length - 2); // removing , in the last skill string
            }

            user_repo.stars = 0;
            if(repo.node.stargazers[0] && repo.node.stargazers[0].totalCount)
            {
            user_repo.stars = repo.node.stargazers[0].totalCount;
            }
            if(filter_pass)
            {
                user_repos.push(user_repo);
            }else if(returnAllRepos){
                user_repos.push(user_repo);
            }
        }
    }

    return{
            name: github_name,
            username: github_username,
            avatar:github_avatar,
            repos:user_repos
    }
    
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = githubGetRepos;

