const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const createClaimService = require('../../../models/services/claims/createClaimService');
const safeObjects = require('../../../models/services/common/safeObjects');
const socialSignup = require("../../../models/services/social/socialSignupService");
const jwt = require('jsonwebtoken');
const config = require('config');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const routeUtils = require('../../../models/services/common/routeUtils');
const validate = require('../../../models/services/common/validate');
const newUserProcedures = require('../../../models/services/auth/newUserProcedure');
const logger = require('../../../models/services/common/logger').getLogger();
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const githubUtils = require("../../../models/services/social/githubService");
const socialLogin = require("../../../models/services/social/socialLoginService");

const githubLink = async (root, {code, state, redirect_uri}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let linkRequest = safeObjects.safeObjectParse({code: code, state: state, redirect_uri: redirect_uri
    }, ['code', 'state', 'redirect_uri']);


    let accessToken = await githubUtils.exchangeAuthorizationCode(linkRequest.code, linkRequest.state, linkRequest.redirect_uri);
    let githubUserAndRepoDataObj = await githubUtils.getGithubUserAndRepoData(accessToken);
    let authType = "github";

    let githubUid = githubUserAndRepoDataObj.id;
    let profileUrl = githubUserAndRepoDataObj.avatarUrl;

    user.github_uid = githubUid;

    if (!githubUid || !profileUrl || !accessToken) {
        logger.debug('Github authentication error : githubUid ' + githubUid + ' profileUrl ' + profileUrl);
        errorUtils.throwError("Authentication failed", 401); // any error in decodedSignupToken will be catched here. Only valid token will lead to defined variable
    }
    await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, user);
    await mongooseUserRepo.update({email: user.email}, {$set: {githubProfileURL: profileUrl, github_uid: githubUid}});
    let token = await socialLogin.setToken(user, user.email);
    amplitudeTracker.publishData('link', {type: authType}, req);

    return {
        userExists: true,
        token: token
    }

};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = githubLink;

