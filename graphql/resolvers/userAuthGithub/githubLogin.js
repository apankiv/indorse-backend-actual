const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const claimsEmailService = require('../../../models/services/common/claimsEmailService');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const randtoken = require('rand-token');
const githubUtils = require("../../../models/services/social/githubService");
const socialLogin = require("../../../models/services/social/socialLoginService");
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const safeObjects = require('../../../models/services/common/safeObjects');
const AUTH_TYPE = 'github';

const githubLogin = async (root, {code, state, redirect_uri}, {req, res}) => {

    let loginRequest = safeObjects.safeObjectParse({code: code, state: state, redirect_uri: redirect_uri
    }, ['code', 'state', 'redirect_uri']);

    let accessToken = await githubUtils.exchangeAuthorizationCode(loginRequest.code, loginRequest.state, loginRequest.redirect_uri);
    let githubUserAndRepoDataObj = await githubUtils.getGithubUserAndRepoData(accessToken);


    let githubUid = githubUserAndRepoDataObj.id;
    let name = githubUserAndRepoDataObj.name;
    let email = githubUserAndRepoDataObj.email;
    let _profileUrl = githubUserAndRepoDataObj.avatarUrl;


    let authType = 'github'
    let user = await socialLogin.findUserByUid(githubUid, authType);

    if (user) {

        if (!socialLogin.userCanLogin(user) || (loginRequest.username)) {
            errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
        }
        await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, user);
        await mongooseUserRepo.update({email: user.email}, {$set: {githubProfileURL: _profileUrl}});
        let token = await socialLogin.setToken(user, user.email);
        amplitudeTracker.publishData('login', {type: authType}, req);

        return {
            userExists: true,
            token: token
        }
    }
    else {
        user = await socialLogin.findUserByEmail(email);
        // link
        if (user) {
            await socialLogin.linkUserByEmail(user, githubUid, email, authType);
            if (!socialLogin.userCanLogin(user)) {
                errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
            }

            user.github_uid = githubUid; // passing github_uid that is assigned to this user.
            await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, user);
            let token = await socialLogin.setToken(user, user.email);
            amplitudeTracker.publishData('social_link', {type: authType}, req);
            await mongooseUserRepo.update({email: user.email}, {$set: {githubProfileURL: _profileUrl}});

            return {
                userExists: true,
                token: token
            }
        } // create a JWT for signup : 2 cases -> 1. with username 2. without username
        else {
            let indAccessToken = await cryptoUtils.generateGithubJWTTmpUser(githubUid, name, email, _profileUrl, accessToken);
            return {
                userExists: false,
                token: indAccessToken
            }

        }
    }
};

module.exports = githubLogin;
