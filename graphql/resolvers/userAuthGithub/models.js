module.exports = `
	
	type githubLoginResp {
		userExists : Boolean!
		token : String!
	}

	type githubLinkResp {
		userExists : Boolean!
		token : String!
	}
	
	type githubSignupResp {
		token: String!
		message: String!
	}

	type githubGetReposResp {
		name: String
		username: String
		avatar: String
		repos : [repo]
	}

	type repo {
		title: String
		description: String
		skills: String
		last_updated_at: String 
		url: String
		stars: Int
	}
	
	type commit {
	    commitMessage: String
	    commitUrl: String
	}
	
	
	type Mutation {
		githubLogin(code: String!, state: String!,redirect_uri: String!): githubLoginResp,         
		githubSignup(token: String!,username : String!, name : String!, claimToken : String): githubSignupResp,
		githubLink(code: String!, state: String!,redirect_uri: String!): githubLinkResp
		# new_mutation (Do not remove this line)
	}

	type Query {
		githubGetRepos(languages:[String]): githubGetReposResp
		# new_query (Do not remove this line)
	}
`;
