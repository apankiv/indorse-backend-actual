const safeObjects = require('../../../models/services/common/safeObjects');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const mongoUserJobsReferralRepo = require('../../../models/services/mongo/mongoRepository')('userjobreferrals');
const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const adminListReferralsByJobId = async (root, { jobId, pageNumber, pageSize}, { req, res }) => {

    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission refer a friend', 403);
    }

    jobId = safeObjects.sanitize(jobId);
    pageNumber = safeObjects.sanitize(pageNumber);
    pageSize = safeObjects.sanitize(pageSize);

    let job = await mongoJobsRepo.findOneById(jobId);

    if (!job)
        errorUtils.throwError('Job no longer exists!', 404);


    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let jobRefCursor = await mongoUserJobsReferralRepo.findAllWithCursor({ jobId: jobId });
    let totalJobReferrals = await jobRefCursor.count();
    
    let jobReferrals = await jobRefCursor.sort({ referredAt: 1 }).skip(skip).limit(limit).toArray();

    let referralsToReturn = [];

    for (userReferral of jobReferrals) {
        let referralToPush = {}
        let existingUser = await mongooseUserRepo.findOneById(userReferral.userId);

        if (!existingUser)
            errorUtils.throwError('user no longer exists!', 404);

        referralToPush.user =  {
                userId: existingUser._id.toString(),
                email: existingUser.email,
                name : existingUser.name,
                username : existingUser.username
        }               
        
        if (userReferral.candidateUserId){
            let candidate = await mongooseUserRepo.findOneById(userReferral.candidateUserId);
            if(!candidate){
                errorUtils.throwError('candidate no longer exists!', 404);
            }
        
            referralToPush.candidate = {
                userId: candidate._id.toString(),
                email: candidate.email,
                name: candidate.name,
                username: candidate.username
            }
              
        } else {
                referralToPush.candidate = {
                    email: userReferral.candidateEmail,
                    name: userReferral.candidateName
               }        
        }
        referralToPush.referredAt = userReferral.referredAt
        referralsToReturn.push(referralToPush)

    }         

    return {
        referrals: referralsToReturn,
        totalUserJobReferrals : totalJobReferrals
    };
}

module.exports = adminListReferralsByJobId;