module.exports = `
    enum JobReferenceStatus {
		user_referred_sucesfully
		user_cannot_refer_self
		candidate_already_referred_by_user
		candidate_already_referred_by_another_user
		candidate_already_applied		
	}

	type ReferralUserDetails{
		userId : String
		email  : String!
		name : String!	
		username : String
	}

	type ReferralDetails{
		user : ReferralUserDetails!
		candidate :  ReferralUserDetails!
		referredAt : Int!
	}

	type AdminReferralsList {
		referrals : [ReferralDetails]
		totalUserJobReferrals: Int
	}	

	type Query {
		adminListReferralsByJobId(jobId: String!,pageNumber: Int, pageSize: Int): AdminReferralsList
		# new_query (Do not remove this line)
	}

	type Mutation {
		referJobToFriend(email: EmailAddress! , name: String!, jobId : String!): JobReferenceStatus
		# new_mutation (Do not remove this line)
	}`	