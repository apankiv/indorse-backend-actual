const safeObjects = require('../../../models/services/common/safeObjects');
const mongoJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const jobUtils = require('../../../models/services/jobs/jobUtils')
const timestampService = require('../../../models/services/common/timestampService');
const emailService = require('../../../models/services/common/emailService')
const mongoUserJobsReferralRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsReferralRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

const referJobToFriend = async (root, { email,name, jobId}, {req, res}) => {
    if (!jobUtils.userIsLoggedIn(req)) {
        errorUtils.throwError('Insufficient permission refer a friend', 403);
    }

email = safeObjects.sanitize(email);
    jobId = safeObjects.sanitize(jobId);
    name = safeObjects.sanitize(name)

    if(email === req.email){
        return 'user_cannot_refer_self'
    }

    let job = await mongoJobsRepo.findOneById(jobId);

    if(!job)
        errorUtils.throwError('Job no longer exists!', 404);

    if (!job.approved || !job.approved.approved)
        errorUtils.throwError('Job is not approved', 403);

    let user = await mongoUserRepo.findOneByEmail(req.email);


    let candidate = await mongoUserRepo.findOneByEmail(email);

    let insertNewReferral = {
        userId: req.user_id,
        jobId: jobId,
        candidateEmail: email,
        candidateName: name,
        referredAt: timestampService.createTimestamp()
    }

    if(candidate){
        insertNewReferral.candidateUserId = candidate._id.toString();
    }


    //Check if this email has not been referred for this job already
    let applied = await mongoUserJobsReferralRepo.findOne({ candidateEmail: email, jobId : jobId});
    if(applied){
        if(applied.userId === req.user_id){

            return 'candidate_already_referred_by_user'
        }
        //Insert for tracking
        await mongoUserJobsReferralRepo.insert(insertNewReferral);
        return 'candidate_already_referred_by_another_user'        
    }


    await mongoUserJobsReferralRepo.insert(insertNewReferral);
    let company = await mongooseCompanyRepo.findOneById(job.company.id);
    if(!company){
        errorUtils.throwError('Company for this job does not exist', 404);
    }

    if (candidate) {
        let hasApplied = await mongoUserJobsRepo.findOne({ user: candidate._id.toString(), job:jobId });
        if (hasApplied) {
            return 'candidate_already_applied';
        }       
    }

    let campaignTitle, campaignDesc;
    if (job.campaign){
        campaignTitle = job.campaign.title
        campaignDesc = job.campaign.description
    }

    await emailService.notifyJobReccomendation(user.name, name, email, job.title, job.location, company.company_name, campaignTitle, campaignDesc, jobId);   
    return 'user_referred_sucesfully';
}

module.exports = referJobToFriend;