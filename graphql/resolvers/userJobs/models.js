module.exports = `
  type Applicant {
    id: String!
    name: String
    username: String
    email: EmailAddress
    appliedAt: Int
    appliedViaReferral : Boolean
    userStatus : String
  }

  type Applicants {
    applicants: [Applicant],
    totalJobApplicants: Int
  }

  type Query {
    jobApplicantsById(jobId: String!, pageNumber: Int, pageSize: Int): Applicants
    # new_query (Do not remove this line)
  }

  # need_first_mutation (Do not remove this line)
`;