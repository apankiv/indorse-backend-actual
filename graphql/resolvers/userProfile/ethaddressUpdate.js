const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const connectionsFacade = require('../../../smart-contracts/services/connections/connectionsContract');
const config = require('config');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const routeUtils = require('../../../models/services/common/routeUtils');
const validate = require('../../../models/services/common/validate');
const newUserProcedures = require('../../../models/services/auth/newUserProcedure');
const logger = require('../../../models/services/common/logger').getLogger();
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const githubUtils = require("../../../models/services/social/githubService");
const socialLogin = require("../../../models/services/social/socialLoginService");

const Web3 = new require('web3');
const web3 = new Web3();

const ethaddressUpdate = async (root, {address}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    console.log("Here");

    let userToUpdate = await socialLogin.findUserByEmail(req.email);
    amplitudeTracker.publishData('ethereum_address_update',{feature_category: 'profile'},userToUpdate._id);

     if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

     if('is_airbitz_user' in userToUpdate && userToUpdate.is_airbitz_user)
    {
        errorUtils.throwError("User has a connected Airbitz account, hence not allowed to change ETH address.", 403);
    }
    
    if (!web3.isAddress(address)) {
        errorUtils.throwError('Invalid Ethereum address format!', 422);
    }

    let userWithEthaddress = await mongooseUserRepo.findOne({
        $and: [
            {ethaddress: address},
            {email: {$ne: req.email}}]
    });

    if (userWithEthaddress) {
        errorUtils.throwError('This Ethereum address is already taken.', 400);
    }

    console.log("here1");
    if('ethaddress' in userToUpdate && userToUpdate.ethaddress)
    {
        //User already has an Etherum address. Not checking ethaddress value since thats taken care by the model thats inserting the value
        //boolean userAllowed = true;
        let userEntity = await connectionsFacade.getEntity(userToUpdate.ethaddress);
        console.log("here01");
        let userHasEntity = userToUpdate.ethaddress && userEntity && userEntity.active;
        console.log("here001");
        if (userHasEntity) {

            console.log("here123");
            if('owner' in userEntity && userEntity.owner === userToUpdate.ethaddress)
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('User has done active transactions with their current Ethereum address.', 400);
            }
        }
        let newAddressEntity = await connectionsFacade.getEntity(address);
        if(newAddressEntity && newAddressEntity.active)
        {
            if('owner' in newAddressEntity && newAddressEntity.owner === address)
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('The new Ethereum address has been used for active transactions.', 400);
            }
        }
    }
    console.log("here12");
    if('ethaddress' in userToUpdate && userToUpdate.ethaddress)
    {
        //User already has an Etherum address. Not checking ethaddress value since thats taken care by the model thats inserting the value
        //boolean userAllowed = true;
        let userEntity = await connectionsFacade.getEntity(address);
        let userHasEntity = userToUpdate.ethaddress && userEntity && userEntity.active;
        if (userHasEntity) {

            console.log("here1234");
            if('owner' in userEntity && userEntity.owner.toLowerCase() === address.toLowerCase())
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('User has done active transactions with their current Ethereum address', 400);
            }
        }
        let newAddressEntity = await connectionsFacade.getEntity(address);
        if(newAddressEntity && newAddressEntity.active)
        {
            if('owner' in newAddressEntity && newAddressEntity.owner.toLowerCase() === address.toLowerCase())
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('The new Ethereum address has been used for active transactions', 400);
            }
        }
    }

    await mongooseUserRepo.update({_id: user_id}, {$set: {ethaddress : address}});
    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);

    let ip = requestIp.getClientIp(req);
    amplitudeTracker.identify(updatedUser,ip);

    return {
        addressUpdated: true,
    }
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = ethaddressUpdate;

