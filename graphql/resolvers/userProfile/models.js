module.exports = `

	type ethaddressUpdateResp{
		addressUpdated: Boolean
	}

	type opportunitiesUpdateResp{
		opportunitiesUpdated: Boolean
	}

	type sociallinksUpdateResp{
		sociallinksUpdated: Boolean
	}

	type ProfileUpdateRespUser {
		name: String!
		img_url: String
		photo_ipfs: String
		bio: String
		position: String
		yearsOfExperience: String
		employmentStatus: String
	}
	
	type User {
	    _id: String
		username: String
		name: String
		bio: String
		img_url: String 
		email: String
	}

	type profileUpdateResp {
		profileUpdated: Boolean
		user: ProfileUpdateRespUser
	}

	input socialLinks{
		url:URL!
		type: String!
	}
	
	

	# need_first_query (Do not remove this line)
	
	type Mutation {
		ethaddressUpdate(address: String!): ethaddressUpdateResp,
		opportunitiesUpdate(opentoopportunity: Boolean!): opportunitiesUpdateResp,
		sociallinksUpdate(sociallinks: [socialLinks]!): sociallinksUpdateResp,
		profileUpdate(name:String!,imgData:String,bio:String,position:String,yearsOfExperience:Int,employmentStatus:String): profileUpdateResp,
		# new_mutation (Do not remove this line)
	}

`;
