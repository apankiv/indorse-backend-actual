const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const connectionsFacade = require('../../../smart-contracts/services/connections/connectionsContract');
const config = require('config');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const routeUtils = require('../../../models/services/common/routeUtils');
const validate = require('../../../models/services/common/validate');
const newUserProcedures = require('../../../models/services/auth/newUserProcedure');
const logger = require('../../../models/services/common/logger').getLogger();
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const githubUtils = require("../../../models/services/social/githubService");
const socialLogin = require("../../../models/services/social/socialLoginService");
const requestIp = require('request-ip');

const Web3 = new require('web3');
const web3 = new Web3();

const opportunitiesUpdate = async (root, {opentoopportunity}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let userToUpdate = await socialLogin.findUserByEmail(req.email);
    amplitudeTracker.publishData('opportunities_update',{feature_category: 'profile'},userToUpdate._id);

     if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    await mongooseUserRepo.update({_id: userToUpdate._id}, {$set: {isOpenToOpportunities : opentoopportunity}});
    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);

    let ip = requestIp.getClientIp(req);
    amplitudeTracker.identify(updatedUser,ip);

    return {
        opportunitiesUpdated: true,
    }
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = opportunitiesUpdate;

