const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const config = require('config');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const socialLogin = require('../../../models/services/social/socialLoginService');
const requestIp = require('request-ip');
const settings = require('../../../models/settings');
const s3Service = require('../../../models/services/blob/s3Service');
const ipfsService = require('../../../models/services/blob/ipfsService');
const User = require('../../../models/services/mongo/mongoose/schemas/user');
const userTaskHandler = require('../../../models/services/userTask/userTaskHandler');


const Web3 = new require('web3');
const web3 = new Web3();

const sociallinksUpdate = async (root, {name,bio,imgData,position,yearsOfExperience,employmentStatus}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let userToUpdate = await socialLogin.findUserByEmail(req.email);

     if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    amplitudeTracker.publishData('profile_update',{feature_category: 'profile'},userToUpdate._id);

    let profileUpdate = {
        name:name,
        bio:bio,
        position:position,
        yearsOfExperience:yearsOfExperience,
        employmentStatus:employmentStatus
    }

    if(imgData)
    {
        let profile_img_update = {
            img_data:imgData
        }

        await updateImage(profile_img_update, userToUpdate._id.toString());

        profileUpdate.img_url = profile_img_update.img_url;
        profileUpdate.photo_ipfs = profile_img_update.photo_ipfs;
    }

    await mongooseUserRepo.update({ _id: userToUpdate._id }, { $set: profileUpdate });
    const updatedUser = await mongooseUserRepo.findOneByEmail(req.email);
    const ip = requestIp.getClientIp(req);
    amplitudeTracker.identify(updatedUser, ip);

    if (updatedUser.img_url || updatedUser.bio) {
        await userTaskHandler.updateUserTasksStatus(updatedUser._id);
    }

    return {
        profileUpdated: true,
        user: User.toSafeObject(updatedUser, req),
    };
};

function userIsAuthorized(req) {
    return req.login;
}

function validateSocialLinks(socialLinks){
    for (val of socialLinks){
        if(val.value){
            if(val.value.length > 128) 
                errorUtils.throwError('Social links need to be less than 128 characters.', 400);            
            else 
                val.value = val.value.toLowerCase();     
       }   
    }    
    return true;
}

async function updateImage(profileUpdateRequest, user_id) {

    if (uploadsAreEnabled()) {
        let bufferedData = new Buffer(profileUpdateRequest.img_data.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        await s3Service.uploadFile(user_id, bufferedData);
        let hash = await ipfsService.uploadFile(bufferedData);

        profileUpdateRequest.img_url = config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + user_id +
            "?t=" + Math.floor(Date.now() / 1000);
        profileUpdateRequest.photo_ipfs = hash;
    }
}

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
}

module.exports = sociallinksUpdate;

