const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const socialLogin = require("../../../models/services/social/socialLoginService");
const requestIp = require('request-ip');

const Web3 = new require('web3');
const web3 = new Web3();

const sociallinksUpdate = async (root, {sociallinks}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let userToUpdate = await socialLogin.findUserByEmail(req.email);
    amplitudeTracker.publishData('sociallinks_update',{feature_category: 'profile'},userToUpdate._id);

     if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    await mongooseUserRepo.update({_id: userToUpdate._id}, {$set: {social_links : sociallinks}});
    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);

    let ip = requestIp.getClientIp(req);
    amplitudeTracker.identify(updatedUser,ip);

    return {
        sociallinksUpdated: true,
    }
};

function userIsAuthorized(req) {
    return req.login;
}

function validateSocialLinks(socialLinks){
    for (val of socialLinks){
        if(val.value){
            if(val.value.length > 128) 
                errorUtils.throwError('Social links need to be less than 128 characters.', 400);            
            else 
                val.value = val.value.toLowerCase();     
       }   
    }    
    return true;
}

module.exports = sociallinksUpdate;

