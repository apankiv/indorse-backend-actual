const s3Service = require('../../../models/services/blob/s3Service');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseImportedContactsRepo = require('../../../models/services/mongo/mongoose/mongooseImportedContactsRepo');
const mongooseConnectionsRepo = require('../../../models/services/mongo/mongoose/mongooseConnectionsRepo');
const mongooseSchoolRepo = require('../../../models/services/mongo/mongoose/mongooseSchoolRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseMiscSkillRepo = require('../../../models/services/mongo/mongoose/mongooseMiscSkillRepo');
const cryptoUtils = require('../../../models/services/common/cryptoUtils');
const errorUtils = require('../../../models/services/error/errorUtils');
const timestampService = require('../../../models/services/common/timestampService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const moment = require('moment');
const nodeZip = new require('node-zip');
const csv = require('csvtojson');
const fs = require('fs');


const uploadLinkedinArchive = async (root, {file}, {req, res}) => {
    const {stream, filename, mimetype, encoding} = await file;
    let pathToSave = req.user_id + "-" + timestampService.createTimestamp() + ".zip";

    // store file locally, remove later because of PDPA ;zzz
    await storeFS({
        stream,
        filename: pathToSave
    });


    let data = fs.readFileSync(pathToSave);

    await s3Service.uploadLinkedinArchive(data, pathToSave);
    let zip = new nodeZip(data, {base64: false, checkCRC32: true});
    await fs.unlink(pathToSave);
    let requestingUser = await mongooseUserRepo.findOneByEmail(req.email);

    let newEducationEntries;
    let newWorkEntries;
    let newSkillEntries;
    let newMiscSkillEntries;
    let newContactEntries;
    let newConnectionEntries;

    let csvFound = false;

    if (zip.files["Education.csv"]) {
        csvFound = true;
        newEducationEntries = [];

        let educationCSVRows = await csv().fromString(zip.files["Education.csv"]._data);

        for (let csvRow of educationCSVRows) {
            let schoolName = csvRow['School Name'];
            let school_id = await getSchoolId(schoolName);
            let newEducationEntry = {
                school_id: school_id,
                school_name: schoolName,
                item_key: cryptoUtils.generateItemKey(),
                degree: csvRow['Degree Name'],
                activities: csvRow['Activities'],
                description: csvRow['Notes'],
            };

            if (csvRow['Start Date']) {
                newEducationEntry.start_date = {year: Number(csvRow['Start Date'])};
            }

            if (csvRow['End Date']) {
                newEducationEntry.end_date = {year: Number(csvRow['End Date'])};
            }

            newEducationEntries.push(newEducationEntry)
        }
        amplitudeTracker.publishData('linkedin_import_csv_education_success', {feature_category: 'profile'}, req);
    }

    if (zip.files["Positions.csv"]) {
        csvFound = true;
        newWorkEntries = [];

        let workCSVRows = await csv().fromString(zip.files["Positions.csv"]._data);

        for (let csvRow of workCSVRows) {

            let companyName = csvRow['Company Name'];
            let company_id = await getCopmanyID(companyName);
            let newWorkEntry = {
                company_id: company_id,
                item_key: cryptoUtils.generateItemKey(),
                title: csvRow['Title'],
                location: csvRow['Location'],
                description: csvRow['Description'],
            };

            if (csvRow['Started On']) {
                newWorkEntry.start_date = convertStringDate(csvRow['Started On']);
            }

            if (csvRow['Finished On']) {
                newWorkEntry.end_date = convertStringDate(csvRow['Finished On']);
            } else {
                newWorkEntry.currently_working = true;
            }
            newWorkEntries.push(newWorkEntry)
        }
        amplitudeTracker.publishData('linkedin_import_csv_work_success', {feature_category: 'profile'}, req);
    }

    if (zip.files["Skills.csv"]) {
        csvFound = true;
        newSkillEntries = [];
        newMiscSkillEntries = [];

        let skillCSVRows = await csv().fromString(zip.files["Skills.csv"]._data);

        for (let csvRow of skillCSVRows) {
            let skillName = csvRow['Name'];

            let existingSkill = await mongooseSkillRepo.findOne({name: skillName.toLowerCase()});
            if (requestingUser.skills) {
                let userHasSkill = requestingUser.skills.find(skill => skill.skill && skill.skill.name === skillName.toLowerCase());
                if (existingSkill && !userHasSkill) {
                    newSkillEntries.push({skill: existingSkill, level: "beginner"});
                }
            } else if (existingSkill) {
                newSkillEntries.push({skill: existingSkill, level: "beginner"});
            } else if(!existingSkill) {
                newMiscSkillEntries.push({name : skillName.toLowerCase(), user_id : req.user_id});
            }
        }
        amplitudeTracker.publishData('linkedin_import_csv_skills_success', {feature_category: 'profile'}, req);
    }

    if (zip.files["Imported Contacts.csv"]) {
        csvFound = true;
        newContactEntries = [];

        let contactsCSVRows = await csv().fromString(zip.files["Imported Contacts.csv"]._data);

        for (let csvRow of contactsCSVRows) {
            let newContactEntry = {
                user_id: req.user_id,
                first_name: csvRow['First Name'],
                last_name: csvRow['Last Name'],
                companies: csvRow['Companies'],
                title: csvRow['Title'],
                email_address: csvRow['Email Address'],
                phone_numbers: csvRow['Phone Numbers'],
                created_at: csvRow['Created At'],
                addresses: csvRow['Addresses'],
                instant_message_handles: csvRow['Instant Message Handles'],
                sites: csvRow['Sites']
            };

            newContactEntries.push(newContactEntry);
        }
        amplitudeTracker.publishData('linkedin_import_csv_contacts_success', {feature_category: 'profile'}, req);
    }

    if (zip.files["Connections.csv"]) {
        csvFound = true;
        newConnectionEntries = [];

        let connectionsCSVRows = await csv().fromString(zip.files["Connections.csv"]._data);

        for (let csvRow of connectionsCSVRows) {

            let newConnectionEntry = {
                user_id: req.user_id,
                first_name: csvRow['First Name'],
                last_name: csvRow['Last Name'],
                email_address: csvRow['Email Address'],
                company: csvRow['Company'],
                position: csvRow['Position'],
                connected_on: csvRow['Connected On'],
            };

            newConnectionEntries.push(newConnectionEntry);
        }
        amplitudeTracker.publishData('linkedin_import_csv_connections_success', {feature_category: 'profile'}, req);
    }

    if(newMiscSkillEntries && newMiscSkillEntries.length >0) {
        await mongooseMiscSkillRepo.insertMany(newMiscSkillEntries);
    }

    if (newEducationEntries || newWorkEntries || newSkillEntries) {
        let updateObj = {
            $push: {}
        };

        if (newEducationEntries) {
            updateObj["$push"].education = {$each: newEducationEntries};
        }
        if (newWorkEntries) {
            updateObj["$push"].work = {$each: newWorkEntries};
        }
        if (newSkillEntries) {
            updateObj["$push"].skills = {$each: newSkillEntries};
        }
        await mongooseUserRepo.update({email: req.email}, updateObj);
    }

    if (newContactEntries) {
        await mongooseImportedContactsRepo.insertMany(newContactEntries);
    }

    if (newConnectionEntries) {
        await mongooseConnectionsRepo.insertMany(newConnectionEntries);
    }

    if (!csvFound) {
        amplitudeTracker.publishData('linkedin_import_csv_fail', {feature_category: 'profile'}, req);
        amplitudeTracker.publishData('linkedin_import_csv_reject', {feature_category: 'profile'}, req);
        errorUtils.throwError("The uploaded file contains no LinkedIn CSV files!",400);
    } else {
        amplitudeTracker.publishData('linkedin_import_csv_success', {feature_category: 'profile'}, req);
        return true;
    }
};

function convertStringDate(dateString) {
    let date = dateString.split(' ');
    let isYear = moment(date[1], "YYYY").isValid();
    let safeYear;
    if (isYear) {
        safeYear = date[1];
    }
    let safeMonth = moment().month(date[0]).format("M");
    return {month: safeMonth, year: safeYear};
}

async function getCopmanyID(company_name) {
    let company_id;
    let company = await mongooseCompanyRepo.findOne({'company_name': company_name});

    if (company) {
        company_id = company['_id'].toString();
    } else {
        let companyNameItem = {
            company_name: company_name
        };

        company_id = await mongooseCompanyRepo.insert(companyNameItem);
    }
    return company_id;
}

async function getSchoolId(school_name) {
    let school_id;
    let school = await mongooseCompanyRepo.findOne({'school_name': school_name});

    if (school) {
        school_id = school['_id'].toString();
    } else {
        let schoolNameItem = {
            school_name: school_name
        };

        school_id = await mongooseSchoolRepo.insert(schoolNameItem);
    }
    return school_id;
}

const storeFS = ({stream, filename}) => {
    return new Promise((resolve, reject) =>
        stream
            .on('error', error => {
                if (stream.truncated)
                // Delete the truncated file
                    fs.unlinkSync(filename);
                reject(error)
            })
            .pipe(fs.createWriteStream(filename))
            .on('error', error => reject(error))
            .on('finish', () => resolve({filename}))
    )
};

module.exports = uploadLinkedinArchive;
