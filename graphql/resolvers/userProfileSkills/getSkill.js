const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const socialLogin = require("../../../models/services/social/socialLoginService");

const getSkill = async (root, {name}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let getSkillRequest = safeObjects.safeObjectParse({name: name}, ['name', 'level', 'isHighlighted']);

    let skill = await mongooseSkillRepo.findOne({name : getSkillRequest.name});

    if (!skill) {
        errorUtils.throwError("Skill does not exist", 404);
    }

    let userSkill = null;

    if (user.skills && user.skills.length > 0) {
        //
        userSkill = user.skills.find((skill) => skill.skill.name === getSkillRequest.name);
    }

    if (!userSkill) {
        errorUtils.throwError("User does not have that skill", 400);
    }

    let latest_claim_validation = null;
    let latest_quizbot_validation = null;

    if(userSkill.validations && userSkill.validations.length > 0)
    {
        userSkill.validations.forEach(function(user_validation){

            if(user_validation._id && user_validation.validated)
            {
                let validation_time = user_validation._id.toString();
                validation_time = parseInt(validation_time.substring(0, 8), 16);
                if(user_validation.type === "quizbot")
                {
                    if(!latest_quizbot_validation || latest_quizbot_validation < validation_time)latest_quizbot_validation = validation_time;
                    console.log(latest_quizbot_validation);
                }
                else if(user_validation.type === "claim")
                {
                    if(!latest_claim_validation || latest_claim_validation < validation_time)latest_claim_validation = validation_time;
                }
            }

        })
    }
    console.log(latest_quizbot_validation);

    userSkill.latestChatbotTimestamp = latest_quizbot_validation;
    userSkill.latestClaimTimestamp = latest_claim_validation;

    console.log(userSkill);

    return {
        skill: userSkill
    }
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = getSkill;

