module.exports = `
	

	type skillUpdateResp {
		skillUpdated: Boolean
		skill: skill_object
	}

	type skillDeleteResp{
		skillDeleted: Boolean
	}

	type getSkillResp{
		skill: skill_object
	}

	type validation{
		validated: Boolean
		id: String
		level: String
		type: String
		_id: String
	}

	type skill_object {
		level: String
		skill: skill
		_id: String
		validations: [validation]
		isHighlighted: Boolean
		isDeleted: Boolean
		latestChatbotTimestamp: Int
		latestClaimTimestamp: Int
	}

	type skill{
		category: String
		name: String
		_id: String
		parents: [String]
		tags: [String]
	}

	enum levels_type {
		beginner
		intermediate
		expert
	}
	
	type Mutation {
		skillUpdate(name: String!, level: levels_type!,isHighlighted: Boolean!): skillUpdateResp,
		skillDelete(name: String!): skillDeleteResp
		# new_mutation (Do not remove this line)
	}

	type Query {
		getSkill(name:String!) : getSkillResp
		# new_query (Do not remove this line)
	}

`;
