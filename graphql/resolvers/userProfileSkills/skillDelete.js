const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const socialLogin = require("../../../models/services/social/socialLoginService");

const skillDelete = async (root, {name}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let skillUpdateRequest = safeObjects.safeObjectParse({name: name}, ['name', 'level', 'isHighlighted']);

    let skill = await mongooseSkillRepo.findOne({name : skillUpdateRequest.name});

    if (!skill) {
        errorUtils.throwError("Skill does not exist", 404);
    }

    let userSkill = null;

    if (user.skills && user.skills.length > 0) {
        //
        userSkill = user.skills.find((skill) => skill.skill.name === skillUpdateRequest.name);
    }

    if (!userSkill) {
        errorUtils.throwError("User does not have that skill", 400);
    }

    
    // await mongooseUserRepo.update({"_id": ObjectID(user._id)}, { $pull: { "skills" : { "skill.name": skillUpdateRequest.name } } })
    await mongooseUserRepo.update({"skills.skill.name" : skillUpdateRequest.name, email: user.email},{$set: {"skills.$.isDeleted" : true}});
    // Mark: @prasanna what are the points of two lines below if you don't actually update anything in the mongodb?
    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);
    userSkill = updatedUser.skills.find((skill) => skill.skill.name === skillUpdateRequest.name && skill.level === skillUpdateRequest.level);

    return {
        skillDeleted: true
    }
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = skillDelete;

