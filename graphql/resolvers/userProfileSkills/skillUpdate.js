const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../models/services/common/safeObjects');
const socialLogin = require("../../../models/services/social/socialLoginService");
/*
    TODO: Please fix this endpint to use this on Production.
    TODO: skill name need to be lowercased AND test case should exist
 */
const skillUpdate = async (root, {name, level, isHighlighted}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let skillUpdateRequest = safeObjects.safeObjectParse({name: name, level: level, isHighlighted: isHighlighted
    }, ['name', 'level', 'isHighlighted']);

    console.log(skillUpdateRequest);

    let skill = await mongooseSkillRepo.findOne({name : skillUpdateRequest.name});

    if (!skill) {
        errorUtils.throwError("Skill does not exist", 404);
    }

    let userSkill = null;

    if (user.skills && user.skills.length > 0) {
        // 
        userSkill = user.skills.find((skill) => skill.skill.name === skillUpdateRequest.name);
    }

    if (!userSkill) {
        errorUtils.throwError("User does not have that skill", 400);
    }

    await mongooseUserRepo.update({"skills.skill.name" : skillUpdateRequest.name, email: user.email },{$set: {"skills.$.level" : skillUpdateRequest.level, "skills.$.isHighlighted" : skillUpdateRequest.isHighlighted}});

    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);
    userSkill = updatedUser.skills.find((skill) => skill.skill.name === skillUpdateRequest.name && skill.level === skillUpdateRequest.level);

    return {
        skillUpdated: true,
        skill: userSkill
    }
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = skillUpdate;

