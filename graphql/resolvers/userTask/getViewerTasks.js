const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const userTaskHandler = require('../../../models/services/userTask/userTaskHandler');
const errorUtils = require('../../../models/services/error/errorUtils');

const getViewerTasks = async (root, args, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in!', 403);
    }
    const userId = req.user_id;
    const user = await mongooseUserRepo.findOne({ _id: userId });
    if (!user) {
        errorUtils.throwError('User not found in the DB!', 500);
    }
    const viewerTasks = await userTaskHandler.getViewerTasks(userId);
    return Array.isArray(viewerTasks) ? viewerTasks : [];
};

module.exports = getViewerTasks;
