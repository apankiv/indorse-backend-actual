const { taskTypes } = require('../../../models/services/mongo/mongoose/schemas/userTask');

module.exports = `
enum TaskType {
    ${taskTypes.UPDATE_BIO},
    ${taskTypes.UPDATE_PROFILE_PIC},
    ${taskTypes.FIRST_CHATBOT_EVALUATION},
    ${taskTypes.FIRST_ASSIGNMENT_EVALUATION},
}

type UserTask {
    type: TaskType!
    priority: Int!
    completedAt: Int
    completed: Boolean!
}

type Query {
    getViewerTasks: [UserTask!]!
}
`;
