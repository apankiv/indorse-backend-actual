const mongooseAssignmentRepo = require('../../../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const errorUtils = require('../../../../../models/services/error/errorUtils');
const { getSkipLimit } = require('./helper');

const assignmentsChildResolver = async (root, { paginationInput }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in', 403);
    }
    const { pageNo, pageSize } = paginationInput || {};
    const { skip, limit } = getSkipLimit(pageNo, pageSize);
    const user = root;
    const jobRoles = Array.isArray(user.job_roles) ? user.job_roles : [];
    const Assignment = mongooseAssignmentRepo.getModel();
    const queryForMatchingAssignmentCount = [
        {
            $match: {
                jobRole: {
                    $in: jobRoles,
                },
                disabled: {
                    $ne: true,
                },
            },
        },
        {
            $group: {
                _id: null,
                count: {
                    $sum: 1,
                },
            },
        },
    ];
    let [matchingAssignmentsCountResult] = await Assignment.aggregate(queryForMatchingAssignmentCount) || [];
    matchingAssignmentsCountResult = matchingAssignmentsCountResult || {};
    const query = [
        {
            $match: {
                jobRole: {
                    $in: jobRoles,
                },
                disabled: {
                    $ne: true,
                },
            },
        },
        {
            $sort: {
                title: 1,
            },
        },
        {
            $skip: skip || 0,
        },
        {
            $limit: limit || 100,
        },
        {
            $group: {
                _id: null,
                assignments: {
                    $push: '$$ROOT',
                },
            },
        },
    ];
    let [matchingAssignmentsResult] = await Assignment.aggregate(query) || [];
    matchingAssignmentsResult = matchingAssignmentsResult || {};
    const responseToReturn = {
        totalAssignments: matchingAssignmentsCountResult.count || 0,
        matchingAssignments: matchingAssignmentsCountResult.count || 0,
        assignments: Array.isArray(matchingAssignmentsResult.assignments) ? matchingAssignmentsResult.assignments : [],
    };
    return responseToReturn;
};

module.exports = assignmentsChildResolver;
