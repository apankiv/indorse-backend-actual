const mongooseSkillRepo = require('../../../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../../../models/services/error/errorUtils');
const skillUtils = require('../../../../../models/services/skills/skillUtils');
const mongoose = require('mongoose');
const { getSkipLimit } = require('./helper');

const chatbotsChildResolver = async (root, { paginationInput }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in', 403);
    }
    const { pageNo, pageSize } = paginationInput || {};
    const { skip, limit } = getSkipLimit(pageNo, pageSize);
    const user = root;
    const { skills } = user || {};
    const isSkillChatbotValidatedMap = {};
    const skillIds = (skills || []).filter(us => us && !us.isDeleted).map((skill) => {
        const chatbotValidation = skill.validations.find(validation => (validation.type === 'quizbot' && validation.validated === true));
        isSkillChatbotValidatedMap[skill.skill._id] = !!chatbotValidation;
        return mongoose.Types.ObjectId(skill.skill._id);
    });
    const parentSkillIds = await skillUtils.findParentSkillsOfMultipleSkills(skillIds) || [];
    const allSkillIds = [...skillIds, ...parentSkillIds];
    const Skill = mongooseSkillRepo.getModel();
    const queryForMatchingSkillCount = [
        {
            $match: {
                _id: {
                    $in: allSkillIds,
                },
                'validation.chatbot': true,
                'validation.chatbot_uuid': {
                    $exists: true,
                },
            },
        },
        {
            $group: {
                _id: null,
                count: {
                    $sum: 1,
                },
            },
        },
    ];
    let [matchingSkillsCountResult] = await Skill.aggregate(queryForMatchingSkillCount) || [];
    matchingSkillsCountResult = matchingSkillsCountResult || {};

    const query = [
        {
            $match: {
                _id: {
                    $in: allSkillIds,
                },
                'validation.chatbot': true,
                'validation.chatbot_uuid': {
                    $exists: true,
                },
            },
        },
        {
            $skip: skip || 0,
        },
        {
            $limit: limit || 100,
        },
        {
            $project: {
                skill: '$$ROOT',
            },
        },
        {
            $group: {
                _id: null,
                chatbots: {
                    $push: '$$ROOT',
                },
            },
        },
    ];
    let [matchingSkillsResult] = await Skill.aggregate(query);
    matchingSkillsResult = matchingSkillsResult || {};
    const { chatbots = [] } = matchingSkillsResult || {};
    matchingSkillsResult.chatbots = chatbots.map((chatbot) => {
        const dataToReturn = chatbot;
        const skillId = chatbot.skill._id.toString();
        dataToReturn.validated = !!isSkillChatbotValidatedMap[skillId];
        return dataToReturn;
    });
    const responseToReturn = {
        totalChatbots: matchingSkillsCountResult.count || 0,
        matchingChatbots: matchingSkillsCountResult.count || 0,
        chatbots: Array.isArray(matchingSkillsResult.chatbots) ? matchingSkillsResult.chatbots : [],
    };
    return responseToReturn;
};

module.exports = chatbotsChildResolver;
