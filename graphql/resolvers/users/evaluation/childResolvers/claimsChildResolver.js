const { types: claimTypes } = require('../../../../../models/services/mongo/mongoose/schemas/claims');
const mongooseClaimsRepo = require('../../../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../../../../../models/services/error/errorUtils');
const { getSkipLimit } = require('./helper');

const claimsChildResolver = async (root, { paginationInput }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in', 403);
    }
    const { pageNo, pageSize } = paginationInput || {};
    const { skip, limit } = getSkipLimit(pageNo, pageSize);
    const user = root;
    const Claim = mongooseClaimsRepo.getModel();
    const queryForMatchingClaimCount = [
        {
            $match: {
                type: {
                    $ne: claimTypes.ASSIGNMENT,
                },
                ownerid: user._id.toString(),
            },
        },
        {
            $group: {
                _id: null,
                count: {
                    $sum: 1,
                },
            },
        },
    ];
    let [matchingClaimsCountResult] = await Claim.aggregate(queryForMatchingClaimCount) || [];
    matchingClaimsCountResult = matchingClaimsCountResult || {};

    const query = [
        {
            $match: {
                type: {
                    $ne: claimTypes.ASSIGNMENT,
                },
                ownerid: user._id.toString(),
            },
        },
        {
            $sort: {
                title: 1,
            },
        },
        {
            $skip: skip || 0,
        },
        {
            $limit: limit || 100,
        },
        {
            $group: {
                _id: null,
                claims: {
                    $push: '$$ROOT',
                },
            },
        },
    ];
    let [matchingClaimsResult] = await Claim.aggregate(query);
    matchingClaimsResult = matchingClaimsResult || {};
    const responseToReturn = {
        totalClaims: matchingClaimsCountResult.count || 0,
        matchingClaims: matchingClaimsCountResult.count || 0,
        claims: Array.isArray(matchingClaimsResult.claims) ? matchingClaimsResult.claims : [],
    };
    return responseToReturn;
};

module.exports = claimsChildResolver;
