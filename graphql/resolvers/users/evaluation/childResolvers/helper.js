const _helper = {};

_helper.getSkipLimit = function getSkipLimit(pageNo, pageSize) {
    return {
        skip: pageNo ? Math.max(pageNo * pageSize, 0) : 0,
        limit: pageSize ? Math.max(pageSize, 1) : 100,
    };
};

module.exports = _helper;
