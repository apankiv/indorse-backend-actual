const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../../models/services/error/errorUtils');

const getUserEvaluations = async (root, args, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in!', 403);
    }
    const userId = req.user_id;
    const user = await mongooseUserRepo.findOne({ _id: userId }, { job_roles: 1, skills: 1 });
    return user;
};

module.exports = getUserEvaluations;
