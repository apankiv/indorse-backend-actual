module.exports = `
  type PaginatedClaims {
    totalClaims: Int!
    matchingClaims: Int!
    claims: [Claim!]!
  }

  type PaginatedAssignments {
    totalAssignments: Int!
    matchingAssignments: Int!
    assignments: [Assignment!]!
  }

  type PaginatedChatbots {
    totalChatbots: Int!
    matchingChatbots: Int!
    chatbots: [UserChatbotSkill!]!,
  }

  input PaginationInput {
    pageNo: Int!
    pageSize: Int!
  }

  type UserEvaluations {
    claims(paginationInput: PaginationInput): PaginatedClaims!,
    chatbots(paginationInput: PaginationInput): PaginatedChatbots!,
    assignments(paginationInput: PaginationInput): PaginatedAssignments!,
  }

  type Query {
    getUserEvaluations: UserEvaluations!
  }
`;
