const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const errorUtils = require('../../../../models/services/error/errorUtils');

const getSkillsByViewerJobRoles = async (root, args, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in!', 403);
    }
    const userId = req.user_id;
    const user = await mongooseUserRepo.findOne({ _id: userId }, { job_roles: 1 });
    const jobRoleCriteria = {
        _id: user.job_roles,
    };
    const jobRoleObjects = await mongooseJobRoleRepo.findAll(jobRoleCriteria);
    let jobRoleTags = [];
    jobRoleObjects.forEach((jobRole) => {
        jobRoleTags = [...jobRoleTags, ...jobRole.skillTags];
    });
    const skillCriteria = {
        tags: {
            $in: jobRoleTags,
        },
    };
    const skills = await mongooseSkillRepo.findAll(skillCriteria);
    return Array.isArray(skills) ? skills : [];
};

module.exports = getSkillsByViewerJobRoles;
