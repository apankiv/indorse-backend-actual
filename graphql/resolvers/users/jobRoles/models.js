module.exports = `
  type Query {
    getSkillsByViewerJobRoles: [Skill!]! @loginCheck
  }

  type Mutation {
    userJobRoleUpdate(role: String!): Boolean
    userJobRoleSkillUpdate(roleid : String! , skillIds: [String]!): Boolean
    updateViewerJobRoles(jobRoleIds: [String!]!): [JobRole!]! @loginCheck
  }
`;
