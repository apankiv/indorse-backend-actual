const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const errorUtils = require('../../../../models/services/error/errorUtils');
const mongoose = require('mongoose');

const updateViewerJobRoles = async (root, { jobRoleIds = [] }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in!', 403);
    }
    const userId = req.user_id;
    const jobRoleObjectIds = jobRoleIds.map(jobRoleId => mongoose.Types.ObjectId(jobRoleId));
    const jobRoleCriteria = {
        _id: jobRoleObjectIds,
    };
    const jobRoleObjects = await mongooseJobRoleRepo.findAll(jobRoleCriteria);
    if (!Array.isArray(jobRoleObjects) || (jobRoleObjects.length !== jobRoleIds.length)) {
        errorUtils.throwError('Job Role Ids passed is not valid');
    }
    const userCriteria = {
        _id: userId,
    };
    const dataToUpdate = {
        $set: {
            job_roles: jobRoleIds,
        },
    };
    await mongooseUserRepo.update(userCriteria, dataToUpdate);
    return jobRoleObjects || [];
};

module.exports = updateViewerJobRoles;
