/* eslint-disable */
const errorUtils = require('../../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseSkillsRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../../../models/services/common/safeObjects');

const userJobRoleSkillUpdate = async (
    root,
    { roleid, skillIds },
    { req, res }
) => {
    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    const user = await mongooseUserRepo.findOneEntityById(req.user_id);
    if (!user) {
        errorUtils.throwError(
            'Logged in user not found. Something went wrong',
            403
        );
    }

    for (i = 0; i < skillIds.length; i++) {
        skillIds[i] = safeObjects.sanitize(skillIds[i]);
    }

    const safeRoleId = safeObjects.sanitize(roleid);

    const dbRole = await mongooseJobRoleRepo.findOneById(safeRoleId);
    if (!dbRole) {
        errorUtils.throwError('Role does not exist', 404);
    }

    // Ensure skills selected fall in all tags
    const skillTagsNotSelected = [];
    for (skillTag of dbRole.skillTags) {
        const selectedSkillMatchingThisTag = await mongooseSkillsRepo.findOne({
            $and: [
                { tags: { $in: [skillTag] } },
                { _id: { $in: skillIds } },
            ],
        });
        if (!selectedSkillMatchingThisTag) {
            skillTagsNotSelected.push(skillTag);
        }
    }
    if (skillTagsNotSelected.length > 0) {
        errorUtils.throwError(
            `Please select atleast one skill from each tag - ${skillTagsNotSelected.join(", ")}`,
            403
        );
    }

    const skillsToAdd = [];
    const skillsToRemove = [];
    const unselectedSkillButAlreadyValidated = [];

    const skillsInJobRole = await mongooseSkillsRepo.findAll({
        tags: { $in: dbRole.skillTags },
    });

    skillsInJobRole.forEach(skillInJobRole => {
        if (skillIds.indexOf(skillInJobRole._id.toString()) >= 0) {
            // Skill is selected, check in user skills. If not, add it
            const userSkill = user.skills.find(userSkill => userSkill.skill._id.toString() === skillInJobRole._id.toString());
            if (!userSkill) {
                skillsToAdd.push(skillInJobRole);
            }
        } else {
            // Skill is not selected
            const userSkill = user.skills.find(userSkill => userSkill.skill._id.toString() === skillInJobRole._id.toString());
            if (userSkill) {
                const userSkillValidated = (userSkill.validations || []).find(validation => validation.validated === true);
                if (userSkillValidated) {
                    unselectedSkillButAlreadyValidated.push(skillInJobRole);
                } else {
                    skillsToRemove.push(skillInJobRole)
                }
            }
        }
    });

    if (unselectedSkillButAlreadyValidated.length > 0) {
        // Silently fail for now
    }

    if (skillsToAdd.length > 0) {
        // Add skills
        user.skills = user.skills || [];
        skillsToAdd.forEach(skillToAdd => {
            console.log('Adding skill - ', skillToAdd.name);
            user.skills.push({ skill: skillToAdd, level: 'beginner', isHighlighted: false });
        });
    }

    if (skillsToRemove.length > 0) {
        // Remove skills
        if (user.skills && user.skills.length > 0) {
            const skillIdsToRemove = skillsToRemove.map(skill => skill._id.toString());
            user.skills = user.skills.filter(userSkill => skillIdsToRemove.indexOf(userSkill.skill._id.toString()) < 0);
        }
    }

    await user.save();
    return true;
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = userJobRoleSkillUpdate;
