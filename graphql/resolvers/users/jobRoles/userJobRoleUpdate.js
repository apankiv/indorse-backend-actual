/* eslint-disable */
const errorUtils = require('../../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const safeObjects = require('../../../../models/services/common/safeObjects');
const socialLogin = require("../../../../models/services/social/socialLoginService");
const ObjectID = require('mongodb').ObjectID;

const userJobRoleUpdate = async (root, {role}, {req, res}) => {
    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Something went wrong', 403);
    }    
    
    let roleUpdateRequest = safeObjects.safeObjectParse({role:role} ,['role']);
    
    let dbRole = await mongooseJobRoleRepo.findOneById(roleUpdateRequest.role);

    if (!dbRole) {
        errorUtils.throwError("Role does not exist", 404);
    }      

    //Ensure role not already present on user
    if (user.job_roles && user.job_roles.length) {
        for (userJobRole of user.job_roles) {
            if(userJobRole.toString() === role){
                return false;
            }
        }
    }

    //If role not found 
    await mongooseUserRepo.update({ _id: user._id.toString() }, {
        $set: { job_roles: [ObjectID(role)] }
    });    

    return true;    
};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = userJobRoleUpdate;

