module.exports = `
  type Mutation {
    updateViewerSkills(skillIds: [String!]!): [Skill!]!
  }
`;
