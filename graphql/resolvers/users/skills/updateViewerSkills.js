const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const errorUtils = require('../../../../models/services/error/errorUtils');
const mongoose = require('mongoose');

const createSkillMap = (skillIds = []) => {
    const skillMap = {};
    skillIds.forEach((skillId) => {
        skillMap[skillId] = false;
    });
    return skillMap;
};

const updateViewerSkills = async (root, { skillIds = [] }, { req }) => {
    if (!req.user_id) {
        errorUtils.throwError('User not logged in!', 403);
    }
    const userId = req.user_id;

    const User = mongooseUserRepo.getModel();
    const user = await User.findOne({ _id: userId });
    if (!user) {
        errorUtils.throwError('User not found! Something went wrong!');
    }

    // get all jobRole tags from the user collection
    const jobRoleCriteria = {
        _id: user.job_roles,
    };
    const jobRoleObjects = await mongooseJobRoleRepo.findAll(jobRoleCriteria);
    let jobRoleTags = [];
    jobRoleObjects.forEach((jobRole) => {
        jobRoleTags = [...jobRoleTags, ...jobRole.skillTags];
    });
    const skillByTagsCriteria = {
        tags: {
            $in: jobRoleTags,
        },
    };
    const allSkillsByTags = await mongooseSkillRepo.findAll(skillByTagsCriteria, { _id: 1 });
    let allSkillIdsByTags = allSkillsByTags.map(skill => skill && skill._id && skill._id.toString());
    allSkillIdsByTags = allSkillIdsByTags.filter(Boolean);

    const skillObjectIds = skillIds.map(skillId => mongoose.Types.ObjectId(skillId));
    const skillCriteria = {
        _id: skillObjectIds,
        tags: {
            $in: jobRoleTags,
        },
    };
    const skillObjects = await mongooseSkillRepo.findAll(skillCriteria);
    if (!Array.isArray(skillObjects) || (skillObjects.length !== skillIds.length)) {
        errorUtils.throwError('Skill Id passed does not belong to the job role selected by the user', 400);
    }

    const skillMap = createSkillMap(skillIds);
    let skills = ((user && user.skills) || []);

    skills = skills.map((skill) => {
        const concernedSkillId = skill.skill._id;
        const isSkillValidated = (skill.validations || []).find(validation => validation.validated === true);
        if (allSkillIdsByTags.indexOf(concernedSkillId) > -1) {
            if (skillIds.indexOf(concernedSkillId) !== -1 || isSkillValidated) {
                skillMap[skill.skill._id] = true;
                return skill;
            }
            return false;
        }
        return skill;
    });
    skills = skills.filter(Boolean);

    skillIds.forEach((skillId) => {
        if (!skillMap[skillId]) {
            const skillToAdd = skillObjects.find(skill => skill._id.toString() === skillId);
            skills.push({ skill: skillToAdd, level: 'beginner', isHighlighted: false });
        }
    });

    user.skills = skills;
    await user.save();
    return skillObjects || [];
};

module.exports = updateViewerSkills;
