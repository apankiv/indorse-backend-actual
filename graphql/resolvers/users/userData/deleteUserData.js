/* eslint-disable */
const errorUtils = require('../../../../models/services/error/errorUtils');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseUserAssigmentRepo = require('../../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseUserAssesmentRepo = require('../../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseUserJobsRepo = require('../../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const mongooseUserJobsReferralRepo = require('../../../../models/services/mongo/mongoose/mongooseUserJobsReferralRepo');

const mongooseValidatorRepo = require('../../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseRewardsRepo = require('../../../../models/services/mongo/mongoose/mongooseRewardsRepo');
const mongooseValidatorRanksRepo = require('../../../../models/services/mongo/mongoose/mongooseValidatorRanksRepo');


const mongooseVoteRepo = require('../../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseVotingRoundRepo = require('../../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseClaimsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimsRepo');

const mongooseUserGithubRepo = require('../../../../models/services/mongo/mongoose/mongooseUserGithubRepo');

const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseConnectionsRepo = require('../../../../models/services/mongo/mongoose/mongooseConnectionsRepo'); 
const mongooseImportedContactsRepo = require('../../../../models/services/mongo/mongoose/mongooseImportedContactsRepo'); 

const mongooseClaimDraftsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo'); 
const mongooseClaimEventsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimEvents'); 
const mongooseClaimGithubsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimGithubsRepo'); 




const safeObjects = require('../../../../models/services/common/safeObjects');
const socialLogin = require("../../../../models/services/social/socialLoginService");
const cryptoUtils= require('../../../../models/services/common/cryptoUtils')

const ObjectID = require('mongodb').ObjectID;


async function scrambleUserData(userId) {

    let updateObject = {
        name: "Deleted User" + cryptoUtils.genRandomString(15),
        civicParams: "",
        oauth_key: "",
        civic_uid: "",
        uport_uid: "",
        isCivicLinked: false,
        isOpenToOpportunities: false,
        bio: "",
        username: cryptoUtils.genRandomString(32),
        position: "",
        yearsOfExperience: 0,
        employmentStatus: "",
        role: 'no_access',
        isSuperAdmin: false,
        email: "deleted-user@deleted.com",
        ethaddress: "",
        ethaddressverified: false,
        is_pass: false,
        verified: false,
        approved: false,
        keyPair: {
            privateKey: "",
            publicKey: ""
        },
        score_count: 0,
        confidenceScore: 0,
        linkedInParams: [],
        facebookParams: [],
        uportParams: [],
        social_links: [],
        img_url: "",
        photo_ipfs: "",
        last_linkedin_pdf_import_at: "",
        pdf_aws: "",
        is_airbitz_user: "",
        facebook_uid: "",
        linkedIn_uid: "",
        google_uid: "",
        github_uid: "",
        isFacebookLinked: false,
        isGoogleLinked: false,
        isAirbitzLinked: false,
        isLinkedInLinked: false,
        linkedInProfileURL: false,
        isUportLinked: false,
        isDeleted : true,
        pass: "",
        pass_verify_token: "",
        pass_verify_timestamp: 0,
        timestamp: 0,
        salt: "",
        tokens: [],
        verify_token: "",
        ga_id: "",
        skills: [],
        badges: [],
        pending_badges: [],
        professions: [],
        education: [],
        work: [],
        language: [],
        wizards: [],
        interests: [],
        terms_privacy: [],
        data_request: [],
        job_roles: []
    }

    await mongooseUserRepo.update({ _id: ObjectID(userId)},updateObject)
}


async function deleteUserAssignmentData(userId) {
    await mongooseUserAssesmentRepo.deleteOne({ _id: ObjectID(userId) });
    await mongooseUserAssigmentRepo.deleteOne({ _id: ObjectID(userId) });
}


async function deleteUserJobsData(userId){
    await mongooseUserJobsRepo.deleteOne({ user : userId});
    await mongooseUserJobsReferralRepo.deleteOne({ userId : userId});
}


async function deleteValidatorData(userId){
    await mongooseValidatorRepo.deleteOne({ user_id : userId});
    await mongooseRewardsRepo.deleteOne({ user_id : userId});
    //TODO Test this
    await mongooseValidatorRanksRepo.deleteMany({ rewardOneWeekRank: { $elemMatch: { user_id : userId}}});
}

async function scrambleClaimData(userId,email){
    await mongooseClaimDraftsRepo.deleteMany({email : email});
    await mongooseClaimEventsRepo.deleteMany({ user_id : userId});
    //Find all claims by user and delete any in claim github
    //TODO : Check flow with Jae
    let claims =await mongooseClaimsRepo.findAll({ ownerid : userId});

    for(let claim of claims){
        await mongooseClaimGithubsRepo.deleteOne({ claim_id : claim._id.toString()});
    }   
}

async function deleteUser3rdPartyData(username,userId){
    await mongooseUserGithubRepo.deleteMany({ username : username});
    await mongooseImportedContactsRepo.deleteMany({ user_id : userId})
}



const deleteUserData = async (root, {userid}, {req, res}) => {
    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Something went wrong', 403);
    }

    let reqUserId = safeObjects.safeObjectParse({ userid: userid }, ['userid']);

    let userToDelete = await mongooseUserRepo.findOneById(reqUserId.userid);
    if (userToDelete.isDeleted){
        errorUtils.throwError('User is already deleted in system!', 403)
    }

    let idx = userToDelete.data_request.length;

    if (idx === 0 || userToDelete.data_request[idx - 1].request_type != 'delete'){
        errorUtils.throwError('User has not requested deletion!', 403)
    }


    await deleteValidatorData(reqUserId.userid);

    await deleteUserAssignmentData(reqUserId.userid);

    await deleteUserJobsData(reqUserId.userid);

    await scrambleClaimData(reqUserId.userid, userToDelete.email);

    await deleteUser3rdPartyData(userToDelete.username, reqUserId.userid);
    //Last    
    await scrambleUserData(reqUserId.userid);

    return true;    
};


module.exports = deleteUserData;

