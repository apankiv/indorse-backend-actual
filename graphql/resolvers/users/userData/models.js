module.exports = `

	type Mutation {
		deleteUserData(userid : String!) : Boolean @permissionCheck(roles: ["admin"])
	}
`;
