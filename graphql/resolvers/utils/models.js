const { GROUP: BLACKLIST_GROUP } = require('../../../models/blacklist/blacklist');

module.exports = `
input ContactRequestInput {
  firstName: String!
  lastName: String
  email: String!
  phone: String
}

type Mutation {
  recordContactRequest(form: ContactRequestInput!): Boolean! @recaptchaAndBlacklistCheck(groupName: "${BLACKLIST_GROUP.CONTACT_REQUEST}", limitPerMin: 5)
}
`;
