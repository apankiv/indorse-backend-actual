const mongooseContactRequestRepo = require('../../../models/services/mongo/mongoose/mongooseContactRequestRepo');
const { sendCaseStudyEmail } = require('../../../models/services/common/emailService');

// eslint-disable-next-line
const recordContactRequest = async (root, { form }, context) => {
    // check the last request and limit it.
    await mongooseContactRequestRepo.insert(form);
    const { email, firstName } = form;
    await sendCaseStudyEmail(email, firstName);
    return true;
};

module.exports = recordContactRequest;
