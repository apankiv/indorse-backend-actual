const getPastVotesService = require('../../../models/votes/getPastVotesService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const validatorSchema = require('../../../models/services/mongo/mongoose/schemas/validator');

const addValidator = async (root, {userId, skills, tier}, {req, res}) => {
    // check if ethAddress exist
    let user = await mongooseValidatorRepo.findOne({'user_id': userId});
    if(user){
        errorUtils.throwError("This user is already a validator", 400);
    }

    user = await mongooseUserRepo.findOneById(userId);
    if(!user){
        errorUtils.throwError("User with this userId doesnt exist", 404);
    }
    if(!user.ethaddress){
        errorUtils.throwError("This user doesnt have eth address, please ask this user to update one. Users email address: " + user.email, 400);
    }

    let skillNamesToAdd = [];
    if(!skills){
        errorUtils.throwError("No skill name was provided", 400);
    }
    for(let i = 0 ; i < skills.length ; i ++){
        if(!skills[i]){
            errorUtils.throwError("Invalid skill name, skill name null is given", 404);
        }
        let skillName = skills[i].toLowerCase();
        let skill = await mongooseSkillRepo.findOne({name: skillName});
        if(!skill || !skill.validation || !skill.validation.aip){
            errorUtils.throwError("Invalid skill name, please check if this skill name " + skillName + " exist", 404);
        }
        skillNamesToAdd.push(skills[i]);
    }

    //default tier is Tier-2
    if(!tier){
        tier = validatorSchema.tiers.TIER2;
    }
    let validatorObj = {
        user_id: userId,
        skills: skillNamesToAdd,
        tier: tier,
        username : user.username,
        email : user.email,
        ethaddress : user.ethaddress
    };

    if(user.role !== 'admin' || user.role !== 'full_access'){
        await mongooseUserRepo.update({_id: user._id}, {role: 'full_access'});
    }
    await mongooseValidatorRepo.insert(validatorObj);

    return true;
};

module.exports = addValidator;