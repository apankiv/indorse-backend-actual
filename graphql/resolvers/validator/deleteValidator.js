const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');

const deleteValidator = async (root, {userId, skills, isAdd}, {req, res}) => {
    //check if validator exist
    let validator = await mongooseValidatorRepo.findOne({user_id: userId});

    if (!validator) {
        errorUtils.throwError("Validator with this user_id not found", 404);
    }

    if (validator.delete) {
        errorUtils.throwError("This validator was removed", 404);
    }
    let deleteOperator = {
        $set: {
            skills: [],
            delete: true}
    };

    await mongooseValidatorRepo.update({_id: validator._id}, deleteOperator);

    return true;
};

module.exports = deleteValidator;