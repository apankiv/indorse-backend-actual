const {tiers} = require('../../../models/services/mongo/mongoose/schemas/validator');

module.exports = `
    
  
  enum AIPSkillNames{
    Javascript
    Java
    Php
    Ruby
    Python
    Solidity
    CSHARP
    CPLUSPLUS
  } 
  
  
  enum TierLevel {
    ${tiers.TIER1}
    ${tiers.TIER2}
    ${tiers.TIER3}
  }
  
  type Validator {
    userId: String,
    skillName: String,
    consensus: Int,
    flagged: Int,
    indorsed: Int,
    rewards: Int,
    voteParticipation: Int,
    consensusPercentage: Int,
    totalDownvote: Int,
    invited: Int
    tier: TierLevel
  }
    

  type Mutation {
		addValidator(userId : String!, skills: [AIPSkillNames!]! , tier: TierLevel): Boolean @permissionCheck(roles: ["admin"])
		updateValidator(userId: String!, skills: [AIPSkillNames], tier: TierLevel): Boolean @permissionCheck(roles: ["admin"])
		deleteValidator(userId: String!): Boolean @permissionCheck(roles: ["admin"])
		# new_query (Do not remove this line)
	}

  # need_first_query (Do not remove this line)
`;