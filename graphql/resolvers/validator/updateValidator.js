const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');

const updateValidator = async (root, {userId, skills, tier}, {req, res}) => {
    //check if validator exist
    let validator = await mongooseValidatorRepo.findOne({user_id: userId});

    if(!validator){
        errorUtils.throwError("Validator with this user_id not found",404);
    }
    if(validator.delete){
        errorUtils.throwError("This validator was removed", 404);
    }

    let skillNamesToAdd = [];
    if(skills){
        for(let i = 0 ; i < skills.length ; i ++){
            let skillName = skills[i].toLowerCase();
            let skill = await mongooseSkillRepo.findOne({name: skillName});
            if(!skill || !skill.validation || !skill.validation.aip){
                errorUtils.throwError("Invalid skill name, please check if this skill name " + skillName + " exist", 404);
            }
            skillNamesToAdd.push(skills[i]);
        }
    }


    let validatorUpdateObj = {};
    if(typeof skills !== "undefined") {
        validatorUpdateObj = {
            $set: {
                skills:  skillNamesToAdd,
            }
        };

    }

    if(tier){
        validatorUpdateObj.tier = tier;
    }

    if(validatorUpdateObj){
        await mongooseValidatorRepo.update({_id: validator._id}, validatorUpdateObj);
    }

    return true;
};

module.exports = updateValidator;