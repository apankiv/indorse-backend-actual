const getPastVotesService = require('../../../models/votes/getPastVotesService');
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');
const errorUtils = require('../../../models/services/error/errorUtils');

const getPastVotes = async (root, {pageNumber, pageSize}, {req, res}) => {
    if (!req.login) {
        errorUtils.throwError('Must be logged in to perform this action', 403);
    }

    amplitudeTracker.publishData('my_votes_accessed', {}, req);

    return await getPastVotesService.getPastVotes(req.user_id, pageNumber, pageSize);
};

module.exports = getPastVotes;