const getPendingVotesService = require('../../../models/votes/getPendingVotesService');

const getPendingVotedClaims = async (root, {}, { req, res }) => {

    return await getPendingVotesService.getPendingVotes(req.user_id);
};

module.exports = getPendingVotedClaims;