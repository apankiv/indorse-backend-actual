const getUserVotesService = require('../../../models/votes/getUserRewardService');

const getRewardStatement = async (root, { }, { req, res }) => {

    return await getUserVotesService.getRewardStatement(req.user_id);
}

module.exports = getRewardStatement;