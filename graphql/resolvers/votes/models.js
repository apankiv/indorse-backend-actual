module.exports = `  
  enum VoteStatus{
    indorsed
    rejected
    pending_vote
    vote_missing
    tx_pending
  }
  
  type VotedClaim{
    voteStatus: VoteStatus,
    votedAt: Int,
    voteId: String,
    reward: Int,
    claim: Claim
  }

   type PastVotedClaim{
    votedClaims: [VotedClaim]
    totalPage: Int
   }

  type RewardStatement {
    totalPayout : Int,
    totalVotes : Int,
    rewardsDistributed : Int,
    rewardsPending : Int
  }
    
  type Query {
    votes_getPastVotedClaims(pageNumber : Int, pageSize: Int): PastVotedClaim @loginCheck 
    votes_getRewardStatement(void: Boolean): RewardStatement @loginCheck
    votes_getPendingVotedClaims(void: Boolean): [VotedClaim] @loginCheck 
    # new_query (Do not remove this line)
  }
  # need_first_mutation (Do not remove this line)
`;
