const {updateValidatorCommentActionType} = require('./updateValidatorComment');

module.exports = `
  
  
  enum actionType{
		    ${updateValidatorCommentActionType.DOWNVOTE}
		    ${updateValidatorCommentActionType.UPVOTE}
		    ${updateValidatorCommentActionType.HIDE}
		}
		
  type updatedCommentResp{
        noOfUpvote: Int,
        noOfDownvote: Int,
        isCommentHidden: Boolean,
        isAdminVoteOnComment: Boolean,
        voteId: String,
        voteHiddenUsername: String
  }		
		
  type Mutation {
		updateValidatorComment(voteId: String, actionOnComment: actionType): updatedCommentResp @permissionCheck(roles: ["admin"])
		# new_query (Do not remove this line)
	}

  # need_first_query (Do not remove this line)
`;