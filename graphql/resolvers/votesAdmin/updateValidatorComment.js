const errorUtils = require('../../../models/services/error/errorUtils');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const checkIfAdminVoted = require('../../../models/votes/adminVoteOnCommentService');

const DOWNVOTE = 'downvote';
const UPVOTE = 'upvote';
const HIDE = 'hide';

const updateValidatorCommentActionType = {
    DOWNVOTE: DOWNVOTE,
    UPVOTE: UPVOTE,
    HIDE: HIDE
};

const updateValidatorComment = async (root, {voteId, actionOnComment}, {req, res}) => {

    let voteToUpdate = await mongooseVoteRepo.findOneById(voteId);
    if (!voteToUpdate) {
        errorUtils.throwError("Vote not found", 404);
    }

    if(!voteToUpdate.sc_vote_exists){
        errorUtils.throwError("This vote doesnt have signature",400);
    }

    let isAdminVoteOnComment = false;
    let voteHiddenUsername = '';
    if (actionOnComment === updateValidatorCommentActionType.DOWNVOTE) {
        if(checkIfAdminVoted.checkIfUserVoted(voteToUpdate, req.user_id)){
            errorUtils.throwError("You have already down or upvoted", 400);
        }
        voteToUpdate.downvote.count++;
        voteToUpdate.downvote.actionBy.push(req.user_id);
        isAdminVoteOnComment = true;
    }
    if (actionOnComment === updateValidatorCommentActionType.UPVOTE) {
        if(checkIfAdminVoted.checkIfUserVoted(voteToUpdate, req.user_id)){
            errorUtils.throwError("You have already down or upvoted", 400);
        }
        voteToUpdate.upvote.count++;
        voteToUpdate.upvote.actionBy.push(req.user_id);
        isAdminVoteOnComment = true;
    }
    if (actionOnComment === updateValidatorCommentActionType.HIDE) {
        if(voteToUpdate.hide.isHidden === true){
            errorUtils.throwError("This comment is already hidden", 400);
        }
        voteToUpdate.hide.isHidden = true;
        voteToUpdate.hide.actionBy.push(req.user_id);
        let user = await mongooseUserRepo.findOneById(req.user_id);
        if(user.username){
            voteHiddenUsername = user.username;
        }else{
            voteHiddenUsername = req.user_id;
        }
    }

    await mongooseVoteRepo.update({_id: voteId}, voteToUpdate);

    let returnObj = {
        noOfUpvote: voteToUpdate.upvote.count,
        noOfDownvote: voteToUpdate.downvote.count,
        isCommentHidden: voteToUpdate.hide.isHidden,
        isAdminVoteOnComment: isAdminVoteOnComment,
        voteId: voteId,
        voteHiddenUsername : voteHiddenUsername
    };

    return returnObj;
};

module.exports.updateValidatorCommentActionType = updateValidatorCommentActionType;

module.exports.updateValidatorComment = updateValidatorComment;
