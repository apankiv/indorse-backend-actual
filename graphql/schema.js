const { makeExecutableSchema, mergeSchemas } = require('graphql-tools');
const getClaimantGithubSchema = require('./resolvers/mongooseCompose/getClaimaintGithubSchema');
const resolvers = require('./resolvers');
const typeDefs = require('./models');
const authCheckDirectives = require('./services/middleware/authChecks');
const blacklistDirective = require('./services/middleware/blacklistCheck');
const recaptchaDirective = require('./services/middleware/recaptchaCheck');
const recaptchaAndBlacklistDirective = require('./services/middleware/recaptchaAndBlacklistCheck');

const directiveResolvers = Object.assign(authCheckDirectives, blacklistDirective, recaptchaDirective, recaptchaAndBlacklistDirective);
const normalSchema = makeExecutableSchema({
    typeDefs,
    resolvers,
    directiveResolvers,
});

module.exports = mergeSchemas({
    schemas: [normalSchema, getClaimantGithubSchema],
});
