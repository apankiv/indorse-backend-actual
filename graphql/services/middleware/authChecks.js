const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../models/services/error/errorUtils');


module.exports = {
    async permissionCheck(next,
                          src,
                          args,
                          {req},) {

        if (!req.email) {
            errorUtils.throwError("Must be logged in", 403);
        }
        let user = await mongooseUserRepo.findOneByEmail(req.email);
        let role = user.role;
        const expectedRoles = args.roles || [];
        if (user && (expectedRoles.length === 0 || expectedRoles.some((r) => r === role))) {
            return next();
        }

        errorUtils.throwError("Insufficient role, requires:" + args.roles.toString(), 403);
    },

    async loginCheck(next,
                     src,
                     args,
                     {req},) {
        if (!req.login) {
            errorUtils.throwError("Must be logged in", 403);
        }
        return next();
    },


}
