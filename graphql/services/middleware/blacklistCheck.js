const blacklistHelper = require('../../../helpers/blacklist');

module.exports = {
    async blacklistCheck(
        next,
        src,
        { groupName, limitPerMin = 2 },
        { req },
    ) {
        await blacklistHelper.checkBlacklistInReqAndThrowIfFail(req, groupName, limitPerMin);
        return next();
    },

};
