const recaptchaHelper = require('../../../helpers/recaptcha');
const blacklistHelper = require('../../../helpers/blacklist');

module.exports = {
    async recaptchaAndBlacklistCheck(
        next,
        src,
        { groupName, limitPerMin = 2 },
        { req },
    ) {
        await blacklistHelper.checkBlacklistInReqAndThrowIfFail(req, groupName, limitPerMin);
        await recaptchaHelper.checkReCaptchaInReqAndThrowIfFail(req);
        return next();
    },

};
