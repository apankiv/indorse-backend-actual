const recaptchaHelper = require('../../../helpers/recaptcha');

module.exports = {
    async recaptchaCheck(
        next,
        src,
        args,
        { req },
    ) {
        await recaptchaHelper.checkReCaptchaInReqAndThrowIfFail(req);
        return next();
    },
};
