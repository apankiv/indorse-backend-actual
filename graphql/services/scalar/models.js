module.exports = `
    scalar URL
    scalar EmailAddress
    scalar Upload
    scalar MongoID
    scalar EthereumAddress
`;
