/*
    REFERENCE: https://github.com/okgrow/graphql-scalars/tree/master/src
 */

const GraphQLScalarType =  require('graphql').GraphQLScalarType;
const Kind  = require('graphql/language').Kind;

const URL_REGEX = new RegExp(
    /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
);

const EMAIL_ADDRESS_REGEX = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
);

const MONGO_ID_REGEX = new RegExp(
    /^[0-9aA-fF]{24}$/,
);

const ETHADDRESS = new RegExp(
    /^(0x)?[0-9aA-fF]{40}$/,
);

/*
    If you aren't familiar with serialize(), parseValue(), parseLiteral(), please refer to this docs - https://stackoverflow.com/questions/41510880/whats-the-difference-between-parsevalue-and-parseliteral-in-graphqlscalartype

    TODO: SafeString, SafeLongString, EthereumAddress
 */
module.exports = {

    Upload : require('apollo-upload-server').GraphQLUpload,

    MongoID: new GraphQLScalarType({
        name: 'MongoID',

        description:
            'Indorse Mongo UserID',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!MONGO_ID_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid userID: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError('Value is not string');
            }

            if (!MONGO_ID_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid userID: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as userID but got a: ${ast.kind}`,
                );
            }

            if (!MONGO_ID_REGEX.test(ast.value)) {
                throw new TypeError(`Value is not a valid user ID: ${ast.value}`);
            }

            return ast.value;
        },
    }),

    EthereumAddress: new GraphQLScalarType({
        name: 'EthereumAddress',

        description:
            'This type represents EthereumAddress starting by optional 0x with following 40 alphanuemric characters',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!ETHADDRESS.test(value)) {
                throw new TypeError(`Value is not a valid Ethereum Address: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError('Value is not string');
            }

            if (!ETHADDRESS.test(value)) {
                throw new TypeError(`Value is not a valid Ethereum Address: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as Ethereum Address but got a: ${ast.kind}`,
                );
            }

            if (!ETHADDRESS.test(ast.value)) {
                throw new TypeError(`Value is not a valid Ethereum Address: ${ast.value}`);
            }

            return ast.value;
        },
    }),


    EmailAddress: new GraphQLScalarType({
        name: 'EmailAddress',

        description:
            'A field whose value conforms to the standard internet email address format as specified in RFC822: https://www.w3.org/Protocols/rfc822/.',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!EMAIL_ADDRESS_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid email address: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError('Value is not string');
            }

            if (!EMAIL_ADDRESS_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid email address: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as email addresses but got a: ${ast.kind}`,
                );
            }

            if (!EMAIL_ADDRESS_REGEX.test(ast.value)) {
                throw new TypeError(`Value is not a valid email address: ${ast.value}`);
            }

            return ast.value;
        },
    }),

    /*
        Date: 14th May 2018
        Type: Source code from Github
        Availability: https://github.com/okgrow/graphql-scalars/blob/master/src/URL.js
     */
    URL: new GraphQLScalarType({
        name: 'URL',

        description:
            'A field whose value conforms to the standard URL format as specified in RFC3986: https://www.ietf.org/rfc/rfc3986.txt.',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!URL_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid URL: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!URL_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid URL: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as URLs but got a: ${ast.kind}`,
                );
            }

            if (!URL_REGEX.test(ast.value)) {
                throw new TypeError(`Value is not a valid URL: ${ast.value}`);
            }

            return ast.value;
        },
    })
};
