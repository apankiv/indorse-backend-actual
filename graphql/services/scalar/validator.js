/*
    REFERENCE: https://github.com/okgrow/graphql-scalars/tree/master/src
 */

const GraphQLScalarType =  require('graphql').GraphQLScalarType;
const Kind  = require('graphql/language').Kind;

const URL_REGEX = new RegExp(
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/,
);

const EMAIL_ADDRESS_REGEX = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
);


module.exports = {
    /*
        Date: 14th May 2018
        Type: Source code from Github
        Availability: https://github.com/okgrow/graphql-scalars/blob/master/src/EmailAddress.js
     */
    EmailAddress: new GraphQLScalarType({
        name: 'EmailAddress',

        description:
            'A field whose value conforms to the standard internet email address format as specified in RFC822: https://www.w3.org/Protocols/rfc822/.',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!EMAIL_ADDRESS_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid email address: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError('Value is not string');
            }

            if (!EMAIL_ADDRESS_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid email address: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as email addresses but got a: ${ast.kind}`,
                );
            }

            if (!EMAIL_ADDRESS_REGEX.test(ast.value)) {
                throw new TypeError(`Value is not a valid email address: ${ast.value}`);
            }

            return ast.value;
        },
    }),

    /*
        Date: 14th May 2018
        Type: Source code from Github
        Availability: https://github.com/okgrow/graphql-scalars/blob/master/src/URL.js
     */
    URL: new GraphQLScalarType({
        name: 'URL',

        description:
            'A field whose value conforms to the standard URL format as specified in RFC3986: https://www.ietf.org/rfc/rfc3986.txt.',

        serialize(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!URL_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid URL: ${value}`);
            }

            return value;
        },

        parseValue(value) {
            if (typeof value !== 'string') {
                throw new TypeError(`Value is not string: ${value}`);
            }

            if (!URL_REGEX.test(value)) {
                throw new TypeError(`Value is not a valid URL: ${value}`);
            }

            return value;
        },

        parseLiteral(ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError(
                    `Can only validate strings as URLs but got a: ${ast.kind}`,
                );
            }

            if (!URL_REGEX.test(ast.value)) {
                throw new TypeError(`Value is not a valid URL: ${ast.value}`);
            }

            return ast.value;
        },
    })
};
