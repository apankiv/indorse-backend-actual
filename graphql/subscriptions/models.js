
module.exports = `
  type MassAssignedSubscribe {
    badge: Badge
    email: EmailAddress
    assignType: AssignType
  }
  type CompanyCreatedTx {
    username: String
    companyName: String
    status: Boolean
  }
  type VoteSubmittedResult {
    claimId: String
    skill: Skill
    noOfTentativeVotes: Int
    isMaxVoteReached: Boolean
    voteId: String
    tiers : TiersInfo
  }
  type userTaskCompletedResult {
    userId: String
    type: TaskType!
    completed: Boolean!
    priority: Int!
  }
  type Subscription {
    massAssignBadgesSubscription(authorization: String!): MassAssignedSubscribe
    companyTxCompleted(authorization: String!): CompanyCreatedTx
    voteSubmitted(claimId: String!): VoteSubmittedResult
    userTaskCompleted(userId: String!): userTaskCompletedResult
    # new_subscription (Do not remove this line)
  }
`;
