const { AmqpPubSub } = require('graphql-rabbitmq-subscriptions');
const { ConsoleLogger } = require('@cdm-logger/server');
const settings = require('../../models/settings');
const config = require('config');

// const loggerSettings = {
//     level: 'error', // Optional: default 'info' ('trace'|'info'|'debug'|'warn'|'error'|'fatal')
//     mode: 'short', // Optional: default 'short' ('short'|'long'|'dev'|'raw')
// };

// const logger = ConsoleLogger.create("gql-simplified", loggerSettings);

const RABBITMQ_USER = config.get('graphql.rabbitmq.user');
const RABBITMQ_PASS = config.get('graphql.rabbitmq.password');
const RABBITMQ_HOST = config.get('graphql.rabbitmq.host');
const RABBITMQ_PORT = config.get('graphql.rabbitmq.port');
// const { RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_HOST, RABBITMQ_PORT } = process.env;
const logger = ConsoleLogger.create('gql-simplified', settings);
const RABBITMQ_FULL_URL = `${RABBITMQ_USER}:${RABBITMQ_PASS}@${RABBITMQ_HOST}`;


exports.pubsub = new AmqpPubSub({
    // triggerTransform,
    config: {
        host: RABBITMQ_FULL_URL,
        port: RABBITMQ_PORT,
    },
    logger,
});
