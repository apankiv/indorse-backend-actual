const { withFilter } = require('graphql-subscriptions');
const { pubsub } = require('./pubsub');

const userTaskCompleted = {
    subscribe: withFilter(
        (_, args, { userId }) => {
            return pubsub.asyncIterator('UserTask.Completed');
        },
        (payloads, { userId }, context) => {
            if (payloads) {
                console.log('Logs for debugging the iterator issue Sender: ', payloads, userId, context);
            }
            return payloads.userTaskCompleted.userId === userId;
        },
    ),
};

module.exports = userTaskCompleted;
