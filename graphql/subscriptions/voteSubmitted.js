const { withFilter } = require('graphql-subscriptions');
const { pubsub } = require('./pubsub');

const voteSubmitted = {
    subscribe: withFilter(
        (_, args, { claimId }) => {
            return pubsub.asyncIterator('Vote.Updated');
        },

        (payloads, { claimId }, context) => {
            if (payloads) {
                console.log('Logs for debugging the iterator issue Sender: ', payloads, claimId, context);
            }
            return payloads.voteSubmitted.claimId === claimId;
        },
    ),
};

module.exports = voteSubmitted;
