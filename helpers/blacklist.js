const blacklistManager = require('../models/blacklist/manager');
const requestHelper = require('./request');
const errorUtils = require('../models/services/error/errorUtils');
const maliciousUsersRepo = require('../models/services/mongo/mongoose/mongooseMaliciousUsersRepo');
const slackService = require('../models/services/common/slackService');

class Blacklist {
    static async checkBlacklistInReqAndThrowIfFail(req, groupName, limitPerMin) {
        if (!groupName) {
            return;
        }
        const blacklistModel = blacklistManager.getBlacklistModel(groupName, limitPerMin);
        const ip = requestHelper.getIpFromRequest(req);
        if (await blacklistModel.isIpBlacklisted(ip)) {
            slackService.reportAlert({
                title: `Blacklisted ip ${ip} for ${blacklistModel.uniqueBlacklistGroup}`,
                data: requestHelper.getLoggableDataFromRequest(req),
            });
            const data = {
                type: 'BLACKLISTED',
                body: requestHelper.getLoggableDataFromRequest(req),
            };
            if (req.body && req.body.operationName) {
                data.GQL_OPERATION = req.body.operationName;
            }
            await maliciousUsersRepo.logMaliciousUser(ip, req.originalUrl, data);
            errorUtils.throwError(
                `Blacklisted ip ${ip} for ${blacklistModel.uniqueBlacklistGroup}`,
                429,
            );
        }
    }
}

module.exports = Blacklist;
