const errorUtils = require('../models/services/error/errorUtils');

class Collection {
    /**
     *
     * @param collection array of objects
     * @param key should exists in all objects of collection
     *            defaults to _id
     * @param throwIfKeyNotFound set to true if you want to throw
     *                           if key in any of the object is
     *                           not found
     * @param keyNotFoundObjects pass the param by reference to
     *                            get array of objects for which
     *                            the key is not found
     */
    static convertCollectionToMap(
        collection,
        key = '_id',
        throwIfKeyNotFound = false,
        keyNotFoundObjects = [],
    ) {
        if (!Array.isArray(collection)) {
            errorUtils.throwError(`collection param should be array, ${typeof collection} given.`);
        }
        if (collection.length === 0) {
            return [];
        }
        if (throwIfKeyNotFound && !collection.every(object => !!object[key])) {
            errorUtils.throwError(`key[${key}] not found in collection ${JSON.stringify(collection)}`);
        }
        const map = {};
        collection.forEach((object) => {
            if (!object[key]) {
                keyNotFoundObjects.push(object);
            } else {
                map[object[key]] = object;
            }
        });
        return map;
    }
}

module.exports = Collection;
