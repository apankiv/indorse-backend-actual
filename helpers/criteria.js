const stringUtils = require('../models/services/blob/stringUtils');
const { ObjectId } = require('mongoose').Types;
const errorUtils = require('../models/services/error/errorUtils');

const FIELD_TYPE = {
    EXACT: 'exact',
    REGEX: 'regex',
    MONGO_ID: 'mongoId',
    LIST: 'list',
};

class Criteria {
    constructor() {
        this.fieldsMap = {};
    }

    addField(fieldName, fieldValue, fieldType = FIELD_TYPE.EXACT) {
        switch (fieldType) {
        case FIELD_TYPE.EXACT:
            this.fieldsMap[fieldName] = fieldValue;
            break;
        case FIELD_TYPE.REGEX:
            this.fieldsMap[fieldName] = stringUtils.createFullRegexObject(fieldValue);
            break;
        case FIELD_TYPE.MONGO_ID:
            if (Criteria.isObjectIdValid(fieldValue)) {
                this.fieldsMap[fieldName] = new ObjectId(fieldValue);
            }
            break;
        case FIELD_TYPE.LIST: // eslint-disable-line no-case-declarations
            const list = Criteria.getList(fieldValue);
            if (list.length) {
                this.fieldsMap[fieldName] = { $in: list };
            }
            break;
        default:
            errorUtils.throwError(`Unknown field type ${fieldType}`);
        }
    }

    get() {
        return this.fieldsMap;
    }

    isHavingAnyField() {
        return Object.keys(this.fieldsMap).length > 0;
    }

    static isObjectIdValid(probableObjectId) {
        return ObjectId.isValid(probableObjectId);
    }

    static getList(list = []) {
        if (Array.isArray(list)) {
            return list;
        }
        if (typeof list === 'string') {
            return [list];
        }
        return [];
    }
}

module.exports = { Criteria, FIELD_TYPE };
