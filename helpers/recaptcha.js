const recaptchaLib = require('../lib/recaptcha');
const errorUtils = require('../models/services/error/errorUtils');
const requestHelper = require('../helpers/request');
const maliciousUsersRepo = require('../models/services/mongo/mongoose/mongooseMaliciousUsersRepo');
const slackService = require('../models/services/common/slackService');

class Recaptcha {
    static async checkReCaptchaInReqAndThrowIfFail(req) {
        if (process.env.NODE_ENV === 'test') {
            return;
        }
        const recaptchaToken = req.get('x-recaptcha-token');
        if (!recaptchaToken) {
            errorUtils.throwError(JSON.stringify({
                message: `Recaptcha[${requestHelper.getIpFromRequest(req)}] \
x-recaptcha-token header is not set`,
                data: requestHelper.getLoggableDataFromRequest(req),
            }));
        }
        let data = null;
        const ip = requestHelper.getIpFromRequest(req);
        try {
            data = await recaptchaLib.verify(recaptchaToken, ip);
        } catch (err) {
            slackService.reportAlert({
                title: `Recaptcha[${ip}] api call failed ${err.stack}`,
                data: requestHelper.getLoggableDataFromRequest(req),
            });
            return;
        }
        if (!data.success) {
            const logObj = {
                type: 'RECAPTCHA_FAIL',
                body: requestHelper.getLoggableDataFromRequest(req),
            };
            if (req.body && req.body.operationName) {
                logObj.GQL_OPERATION = req.body.operationName;
            }
            maliciousUsersRepo.logMaliciousUser(ip, req.originalUrl, data);
            slackService.reportAlert({
                title: `Recaptcha[${ip}] token invalid [possibly a bot] ${data}`,
                data: requestHelper.getLoggableDataFromRequest(req),
            });
            errorUtils.throwError(
                'Unusual traffic from your computer network, please try again after some time.',
                403,
            );
        }
    }
}
module.exports = Recaptcha;
