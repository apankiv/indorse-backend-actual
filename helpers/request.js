const _ = require('lodash');

const SENSITIVE_KEYS = [
    'password',
    'token',
    'verify_token',
    'pass_token',
];

class Request {
    static getIpFromRequest(req) {
        const ips = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        return ips.split(',').map(ip => ip.trim()).shift();
    }

    static getLoggableDataFromRequest(req) {
        const data = {};
        if (req.body) {
            data.body = _.cloneDeep(req.body);
            data.query = _.cloneDeep(req.query)
            SENSITIVE_KEYS.forEach((sensitiveKey) => {
                if (data.body[sensitiveKey]) {
                    delete data.body[sensitiveKey];
                }
                if (data.query[sensitiveKey]) {
                    delete data.query[sensitiveKey];
                }
            });
        }
        return data;
    }
}

module.exports = Request;
