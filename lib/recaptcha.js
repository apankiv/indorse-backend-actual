const request = require('request-promise-native');
const settings = require('../models/settings');

class Recaptcha {
    static async verify(response, ip = null) {
        if (process.env.NODE_ENV === 'test') {
            return { success: true };
        }
        const config = Recaptcha.getConfig();
        const data = await Recaptcha.sendRequest(response, config, ip);
        if (!data.success) {
            return data;
        }
        if (data.score < config.threshold) {
            data.isScoreLow = true;
            data.success = false;
        }
        return data;
    }

    static async sendRequest(response, config, ip = null) {
        const formData = {
            response,
            secret: config.secret,
        };
        if (ip) {
            formData.ip = ip;
        }
        const body = await request.post({
            url: `https://${config.hostname}${config.path}`,
            formData,
        });
        return JSON.parse(body);
    }

    static getConfig() {
        return {
            secret: settings.RECAPTCHA.SECRET,
            hostname: settings.RECAPTCHA.HOSTNAME,
            path: settings.RECAPTCHA.PATH,
            threshold: parseFloat(settings.RECAPTCHA.THRESHOLD) || 0.1,
        };
    }
}

module.exports = Recaptcha;
