const { GROUP: BLACKLIST_GROUP } = require('../models/blacklist/blacklist');
const blacklistHelper = require('../helpers/blacklist');

class Blacklist {
    static getExpressMiddleware(uniqueGroupName, limitPerMin = 2) {
        return async (req, res, next) => {
            await blacklistHelper
                .checkBlacklistInReqAndThrowIfFail(req, uniqueGroupName, limitPerMin);
            return next();
        };
    }

    static getExpressMiddlewareForSignUp() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.USER_SIGNUP, 5);
    }

    static getExpressMiddlewareForLogin() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.USER_LOGIN, 5);
    }

    static getExpressMiddlewareForForgotPassword() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.USER_FORGOT_PASSWORD, 5);
    }

    static getExpressMiddlewareForVerificationEmail() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.USER_VERIFICATION_EMAIL, 5);
    }

    static getExpressMiddlewareForResendVerificationEmail() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.USER_RESEND_VERIFICATION_EMAIL, 5);
    }

    static getExpressMiddlewareForClaimCreate() {
        return Blacklist.getExpressMiddleware(BLACKLIST_GROUP.CLAIM_CREATE, 5);
    }
}
module.exports = Blacklist;
