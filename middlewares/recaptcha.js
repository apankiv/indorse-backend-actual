const recaptchaHelper = require('../helpers/recaptcha');

class Recaptcha {
    static async verify(req, res, next) {
        await recaptchaHelper.checkReCaptchaInReqAndThrowIfFail(req);
        return next();
    }
}

module.exports = Recaptcha;
