const mongo = require('mongodb');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongoClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../services/error/errorUtils');
const stringUtils = require('../services/blob/stringUtils');
const getClaimService = require('../services/claims/getClaimService');
const { claimStage, claimSource } = require('../services/mongo/mongoose/schemas/claims');
const async = require('async');

class Claims {
    static async get({
        searchStr = '',
        sortParam = 'title',
        sortDir = 'asc',
        skip = 0,
        limit = 10,
        stage,
        source,
    }) {
        const searchFilters = Claims.getSearchClause(searchStr);
        const additionalFilter = Claims.generateAdditionalFilter(source, stage);
        const getQuery = mongoClaimsRepo.getModel()
            .find(
                { ...searchFilters, ...additionalFilter },
                Claims.getExcludedFieldsMap(),
            )
            .skip(skip)
            .limit(limit)
            .sort(Claims.getSortParamClause(sortParam, sortDir))
            .lean()
            .exec();
        const countQuery = mongoClaimsRepo.getModel()
            .find(
                { ...searchFilters, ...additionalFilter },
                Claims.getExcludedFieldsMap(),
            )
            .count();
        const [claims, count] = await Promise.all([getQuery, countQuery]);
        return { claims, count };
    }

    static async getStatuses(claims = []) {
        const votingRoundsMap = await Claims.getVotingRoundsMap(claims);
        return new Promise((resolve, reject) => {
            const statuses = {};
            const q = async.queue(async (claim) => {
                try {
                    statuses[claim._id]
                        = await getClaimService.getClaimStatus(claim, votingRoundsMap[claim._id]);
                } catch (err) {
                    return reject(err);
                }
                return claim._id;
            }, 10); // 10 is concurrency
            q.push(claims);
            q.drain = () => resolve(statuses);
        });
    }

    static async getVotingRoundsMap(claims = []) {
        const votingRounds = await mongooseVotingRoundRepo.findAll({
            claim_id: { $in: claims.map(claim => claim._id) },
        });
        const votingRoundsMap = {};
        votingRounds.forEach((votingRound) => {
            votingRoundsMap[votingRound.claim_id] = votingRound;
        });
        return votingRoundsMap;
    }

    static getExcludedFieldsMap() {
        return { state: 0, visible: 0, tokens: 0 };
    }

    static getSortParamClause(sortParam, sortDir) {
        if (!sortParam) {
            return {};
        }
        return { [sortParam]: sortDir === 'asc' ? 1 : -1 };
    }

    static getSearchAbleFieldsMap() {
        return [
            {
                name: '_id',
                type: 'objectId',
            },
            {
                name: 'title',
                type: 'regex',
            },
            {
                name: 'desc',
                type: 'regex',
            },
            {
                name: 'proof',
                type: 'regex',
            },
        ];
    }

    static getSearchClause(searchStr) {
        if (!searchStr) {
            return {};
        }
        return {
            $or: Claims.getSearchAbleFieldsMap()
                .map(field => Claims.getFieldWhereClause(field, searchStr))
                .filter(clause => !!clause),
        };
    }

    static generateAdditionalFilter(source, stage) {
        const filter = {};
        if (source === claimSource.COMPANY) filter.company = { $exists: true, $ne: null };
        if (stage === claimStage.APPROVED) filter.approved = { $exists: true };
        if (stage === claimStage.DISAPPROVED) filter.disapproved = { $exists: true };
        if (stage === claimStage.PENDING_APPROVAL) {
            filter.disapproved = { $exists: false };
            filter.approved = { $exists: false };
        }
        return filter;
    }

    static getFieldWhereClause(field, value) {
        let fieldWhereClause = {};
        switch (field.type) {
        case 'regex':
            fieldWhereClause[field.name] = stringUtils.createSafeRegexObject(value);
            break;
        case 'objectId':
            if (new RegExp('^[0-9a-fA-F]{24}$').test(value)) { // valid hex of 24 chars
                fieldWhereClause[field.name] = new mongo.ObjectID(value);
            } else {
                fieldWhereClause = null;
            }
            break;
        default:
            errorUtils.throwError(`Admin Claims : unknown field type - ${field.type}`);
        }
        return fieldWhereClause;
    }
}

module.exports = Claims;
