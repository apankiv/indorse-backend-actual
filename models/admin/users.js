const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');

class Users {
    static async get(userIds = []) {
        const users = await mongooseUserRepo.findAll({
            _id: { $in: userIds },
        });
        const usersMap = {};
        users.forEach((user) => {
            usersMap[user._id] = user;
        });
        return usersMap;
    }
}

module.exports = Users;
