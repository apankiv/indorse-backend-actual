const mongoVotesRepo = require('../services/mongo/mongoRepository')('votes');

class Votes {
    static async get(claimIds = []) {
        const votesCounts = Votes.initializeAndGetVotesCountsMap(claimIds);
        const votes = await mongoVotesRepo.findAll({
            claim_id: { $in: claimIds },
        });
        const votesMapWithKeyClaimId = {};
        votes.forEach((vote) => {
            if (!votesMapWithKeyClaimId[vote.claim_id]) {
                votesMapWithKeyClaimId[vote.claim_id] = [];
            }
            votesMapWithKeyClaimId[vote.claim_id].push(vote);
        });
        return Object.assign(votesCounts, Votes.getVotesCounts(votesMapWithKeyClaimId));
    }

    static getVotesCounts(votesMapWithKeyClaimId) {
        const votesCountsMap = {};
        Object.keys(votesMapWithKeyClaimId).forEach((claimId) => {
            if (!votesCountsMap[claimId]) {
                votesCountsMap[claimId] = {};
            }
            const countersMap = Votes.getCountMap();
            votesMapWithKeyClaimId[claimId].forEach((vote) => {
                if (vote.registered === true) {
                    countersMap.registered += 1;
                }
                if (vote.endorsed === true) {
                    countersMap.indorsed += 1;
                }
                if (vote.endorsed === false) {
                    countersMap.flagged += 1;
                }
            });
            countersMap.invited = votesMapWithKeyClaimId[claimId].length;
            votesCountsMap[claimId] = countersMap;
        });
        return votesCountsMap;
    }

    static initializeAndGetVotesCountsMap(claimIds = []) {
        const votesCountsMap = {};
        claimIds.forEach((claimId) => {
            votesCountsMap[claimId] = Votes.getCountMap();
        });
        return votesCountsMap;
    }

    static getCountMap() {
        return {
            invited: 0,
            registered: 0,
            indorsed: 0,
            flagged: 0,
        };
    }
}

module.exports = Votes;
