const replicationUserRepo = require('../services/mongo/userRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseBadgeRepo = require('../services/mongo/mongoose/mongooseBadgeRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const validate = require('../services/common/validate');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');

exports.register = function register(app) {
    app.post('/badges/massassign',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(massAssignBadges));
};

const REQUEST_FIELD_LIST = ['badges', 'assign', 'emails'];

async function massAssignBadges(req, res) {

    let massAssignBadgesRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let emails = massAssignBadgesRequest.emails;
    let badges = massAssignBadgesRequest.badges;
    let assign = massAssignBadgesRequest.assign;

    for(let email of emails) {
        let user = await mongooseUserRepo.findOneByEmail(email);

        if(!user) {
            errorUtils.throwError("User not found: " + email, 404);
        }
    }

    for(let badgeId of badges) {
        let badge = await mongooseBadgeRepo.findOneByPrettyId(badgeId);

        if(!badge) {
            errorUtils.throwError("Badge not found: " + badgeId, 404);
        }
    }

    for (let email of emails) {
        if (assign === 1) {
            await replicationUserRepo.update({email: email}, {$addToSet: {badges: {$each: badges}}})
        } else if (assign === 0) {
            await replicationUserRepo.update({email: email}, {$pullAll: {badges: badges}});
        }
    }

    res.status(200).send();
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            badges: {
                type: 'array',
                items: {
                    type: 'string',
                }
            },
            assign: {
                type: 'number',
                enum: [0,1]
            },
            emails: {
                type: 'array',
                items: {
                    type: 'string',
                    pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
                }
            }
        },
        required: ['badges', 'assign', 'emails'],
        additionalProperties: false
    };
}