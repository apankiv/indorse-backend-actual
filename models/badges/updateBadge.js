const mongooseBadgeRepo = require('../services/mongo/mongoose/mongooseBadgeRepo');
const Badge = require('../services/mongo/mongoose/schemas/badge');
const safeObjects = require('../services/common/safeObjects');
const logoUpdateService = require('../services/blob/logoUpdateService');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const errorUtils = require('../services/error/errorUtils');


const REQUEST_FIELD_LIST = ['badge_name', 'description', 'logo_data', 'cta_text', 'cta_link'];

exports.register = function register(app) {
    app.patch('/badges/:pretty_id',
        validate({body: REQUEST_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(updateBadge));
};

async function updateBadge(req, res) {
    let updateBadgeRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let badgeToUpdate = await mongooseBadgeRepo.findOneByPrettyId(pretty_id);

    if (!badgeToUpdate) {
        errorUtils.throwError("Badge not found!", 404);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    if (updateBadgeRequest.logo_data) {
        await
            logoUpdateService.updateLogo(updateBadgeRequest, updateBadgeRequest.logo_data, pretty_id);
        delete updateBadgeRequest.logo_data;
    }

    if (updateBadgeRequest.badge_name) {
        updateBadgeRequest.name = updateBadgeRequest.badge_name;
    }
    if (updateBadgeRequest.logo_s3) {
        updateBadgeRequest.image = {icon_s3: updateBadgeRequest.logo_s3, icon_ipfs: updateBadgeRequest.logo_ipfs};
        delete updateBadgeRequest.logo_ipfs;
        delete updateBadgeRequest.logo_s3;
    }

    if (updateBadgeRequest.cta_text) {
        updateBadgeRequest.extensions_cta_text = {context: '', type: '', text: updateBadgeRequest.cta_text};
    }
    if (updateBadgeRequest.cta_link) {
        updateBadgeRequest.extensions_cta_link = {context: '', type: '', link: updateBadgeRequest.cta_link};
    }
    updateBadgeRequest.last_updated_timestamp = Date.now();

    await mongooseBadgeRepo.update({id: pretty_id}, {$set: updateBadgeRequest});

    let badgeToReturn = await mongooseBadgeRepo.findOneByPrettyId(pretty_id);

    res.status(200).send(badgeToReturn);
}

function userIsAuthorized(req) {

    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

const REQUEST_SCHEMA = {
    type: 'object',
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
            maxLength: 64
        },
        logo_data: {
            type: 'string',
            minLength: 1,
            maxLength: 1000000
        },
        badge_name: {
            type: 'string',
            minLength: 1,
            maxLength: 255
        },
        description: {
            type: 'string',
            minLength: 1,
            maxLength: 1000
        },
        cta_text: {
            type: 'string',
            minLength: 1,
            maxLength: 64
        },
        cta_link: {
            type: 'string',
            minLength: 1,
            maxLength: 1000
        },
    },
    additionalProperties: false
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};
