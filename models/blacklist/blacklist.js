const errorUtils = require('../services/error/errorUtils');
const blacklistRepo = require('../services/mongo/mongoose/mongooseBlacklistRepo');
const MemoryCache = require('memory-cache');

const BLACKLISTED_TTL_SECS_IN_MEMORY = 60;

const GROUP = {
    USER_SIGNUP: 'user_signup',
    USER_LOGIN: 'user_login',
    USER_FORGOT_PASSWORD: 'user_forgot_password',
    USER_RESEND_VERIFICATION_EMAIL: 'user_resend_verification_email',
    USER_VERIFICATION_EMAIL: 'user_verification_email',
    CONTACT_REQUEST: 'contact_request',
    CLAIM_CREATE: 'claim_create',
    CLAIM_DRAFT_CREATE: 'claim_draft_create',
};

class Blacklist {
    /**
     * Create separate objects for different requests.
     * This means, if we want to use blacklisting for requests /signup and /login,
     * then in this case, we need to create two objects with different values for
     * @param uniqueBlacklistGroup
     *
     * @param uniqueBlacklistGroup uniquely identify the blacklist of one api
     * @param limitPerMin number of requests per minute allowed
     */
    constructor(uniqueBlacklistGroup = null, limitPerMin = 2) {
        if (!uniqueBlacklistGroup) {
            errorUtils.throwError(`uniqueBlacklistId should be string, ${typeof uniqueBlacklistGroup} given.`);
        }
        this.uniqueBlacklistGroup = uniqueBlacklistGroup;
        this.limitPerMin = limitPerMin;
    }

    async isIpBlacklisted(ip) {
        if (
            process.env.NODE_ENV === 'test' ||
            !ip
        ) {
            return false;
        }

        if (this.getBlacklistedIpFromMemory(ip)) {
            return true;
        }
        const data = await blacklistRepo.incIpCounter(ip, this.uniqueBlacklistGroup);
        if (data.request_counter > this.limitPerMin) {
            this.saveBlacklistedIpInMemory(ip);
            return true;
        }
        return false;
    }

    saveBlacklistedIpInMemory(ip) {
        MemoryCache.put(
            this.getInMemoryIpKey(ip),
            true,
            BLACKLISTED_TTL_SECS_IN_MEMORY * 1000,
        );
    }

    getBlacklistedIpFromMemory(ip) {
        return MemoryCache.get(this.getInMemoryIpKey(ip));
    }

    getInMemoryIpKey(ip) {
        return `blacklist_${this.uniqueBlacklistGroup}_ip_${ip}`;
    }
}
module.exports = { Blacklist, GROUP };
