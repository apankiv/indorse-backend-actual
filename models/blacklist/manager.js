const { Blacklist: BlacklistModel } = require('./blacklist');

const blacklistsMap = {};

class Manager {
    static getBlacklistModel(group, limitPerMin = 2) {
        if (!blacklistsMap[group]) {
            blacklistsMap[group] = new BlacklistModel(group, limitPerMin);
        }
        return blacklistsMap[group];
    }
}

module.exports = Manager;
