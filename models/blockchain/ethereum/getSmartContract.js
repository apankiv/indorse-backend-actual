const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const mongooseContractsRepo = require('../../services/mongo/mongoose/mongooseContractsRepo');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.get('/ethereum/:network/contracts/:contract_name',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getCompany));
};

async function getCompany(req, res) {
    let network = safeObjects.sanitize(req.param("network"));
    let contract_name = safeObjects.sanitize(req.param("contract_name"));


    let contract = await mongooseContractsRepo.findOneByNameAndNetwork(contract_name, network);

    if (!contract) {
        errorUtils.throwError("Contract not found", 404);
    }

    res.status(200).send(contract);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['contract_name'],
    properties: {
        contract_name: {
            type: 'string',
            minLength: 1
        },
        network: {
            type: 'string',
            allowedValues: ['RINKEBY', 'LOCAL', 'MAINNET', 'KOVAN']
        }
    }
};
