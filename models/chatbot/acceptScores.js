const safeObjects = require('../services/common/safeObjects');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const routeUtils = require('../services/common/routeUtils');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const assessmentMagicLinkUtils = require('../services/assessmentMagicLink/assessmentMagicLinkUtils');
const errorUtils = require('../services/error/errorUtils');
const settings = require('../settings');
const timestampService = require('../services/common/timestampService');
const userTaskHandler = require('../services/userTask/userTaskHandler');

const REQUEST_FIELD_LIST = ['email', 'payload_type', 'chatbot_uuid', 'total_score', 'results', 'value'];

exports.register = function register(app) {
    app.post(
        '/chatbot/scores',
        // tentatively removed validation schema since we check x_api_key
        // against to chatbot private key.
        routeUtils.asyncMiddleware(acceptScores),
    );
};

/*
    This endpoint is being used by Impress server. When user completes chatbot, this endpoint will be called.
 */
async function acceptScores(req, res) {
    const scoresRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    if (req.x_api_key !== settings.CHATBOT_PRIVATE_KEY) {
        errorUtils.throwError('Chatbot authentication failed', 403);
    }
    const chatbotUuid = scoresRequest.chatbot_uuid.trim();
    const userId = scoresRequest.email.replace('@indorse.io', '').trim();
    let score;
    if (!scoresRequest.total_score) {
        console.log(`score request  ${scoresRequest.results}`);
        const rawScore = scoresRequest.results[0].value;
        console.log(`RAW score ${rawScore}`);
        score = parseInt(rawScore, 10);
        console.log(`score ${score}`);
    } else {
        score = parseInt(scoresRequest.total_score, 10);
    }

    const payloadType = scoresRequest.payload_type;

    const levels_array = ['beginner', 'intermediate', 'expert'];
    let highest_level = -1;


    if ((payloadType !== 'interview completed') && (payloadType !== 'section completed')) {
        console.log(`User has not completed the chatbot yet, payload type: ${payloadType}`);
        res.status(200).send();
        return; // if chatbot is not completed, do nothing.
    }

    let passScoreThreshold = settings.CHATBOT_MIN_PASS_SCORE;
    if (score < passScoreThreshold) {
        res.status(200).send();
        return;
    }
    highest_level = 0;

    const skill_doc = await mongooseSkillRepo.findOne({ 'validation.chatbot_uuid': chatbotUuid });

    if (!skill_doc) {
        errorUtils.throwError(`Unrecognized chatbot uuid from Impress ${chatbotUuid}`, 400);
    }


    const skill_name = skill_doc.name;
    if (!skill_name) {
        errorUtils.throwError(`No skill name ${JSON.stringify(skill_doc)}`, 400);
    }
    const user = await mongooseUserRepo.findOneById(userId);

    let skill_found = 0;

    if (user.skills) {
        let skill_counter = 0;
        user.skills.every((skill) => {
            if (skill.skill.name === skill_name) {
                skill_found = 1;
                return false;
            }
            skill_counter += 1;
            return true;
        });

        // if user has the skill already
        if (skill_found) {
            const user_skill_obj = user.skills[skill_counter];
            // Check for levels mismatch
            if (highest_level >= 0) {
                let highest_existing = 0;
                if ('level' in user_skill_obj) {
                    highest_existing = user_skill_obj.level;
                    highest_existing = levels_array.indexOf(highest_existing);
                }
                const new_highest = highest_level > highest_existing ? highest_level : highest_existing;
                user_skill_obj.level = levels_array[new_highest]; // set the skill level to the new highest

                let validation_found = 0;

                // Check if any new validations need to be inserted in the skill object.
                if ('validations' in user_skill_obj) {
                    user_skill_obj.validations.forEach((validation) => {
                        if (validation.id === chatbotUuid) {
                            const val_level = levels_array.indexOf(validation.level);
                            if (val_level === highest_level) {
                                validation_found = 1;
                            }
                        }
                    });
                }
                // insert new validation array for those scores left behind
                const skillIdForAssessmentUpdate = user_skill_obj.skill._id;
                const userIdForAssessmentUpdate = userId;
                let validationObject;
                if (!validation_found) {
                    const val_obj = {};
                    val_obj.type = 'quizbot';
                    val_obj.id = chatbotUuid;
                    val_obj.level = levels_array[highest_level];
                    val_obj.validated = true;
                    val_obj.chatbotScore = score;
                    val_obj.validatedAt = timestampService.createTimestamp();
                    user_skill_obj.validations.push(val_obj);
                    // update validation object
                    validationObject = val_obj;
                }

                // Skill object is ready to be updated in the DB
                await mongooseUserRepo.update({
                    email: user.email,
                    'skills.skill.name': skill_name,
                }, { $set: { 'skills.$': user_skill_obj } });
                assessmentMagicLinkUtils.updateUserAssessmentsWithSkill(userIdForAssessmentUpdate, skillIdForAssessmentUpdate, validationObject);
            }
        }
    }


    // if user don't have the skill
    if (!skill_found) {
        // create skill obj and push to skills array in user document
        const skill_obj = {};
        skill_obj.skill = skill_doc;
        skill_obj.validations = [];
        highest_level = levels_array[highest_level];
        skill_obj.level = highest_level;

        // insert new validation array for those scores left behind
        const skillIdForAssessmentUpdate = skill_doc._id;
        const userIdForAssessmentUpdate = userId;
        let validationObject;
        const val_obj = {};
        val_obj.type = 'quizbot';
        val_obj.id = chatbotUuid;
        val_obj.level = highest_level;
        val_obj.validated = true;
        val_obj.chatbotScore = score;
        val_obj.validatedAt = timestampService.createTimestamp();
        skill_obj.validations.push(val_obj);
        validationObject = val_obj;
        await mongooseUserRepo.update({ email: user.email }, { $addToSet: { skills: skill_obj } }, { safe: true });
        assessmentMagicLinkUtils.updateUserAssessmentsWithSkill(userIdForAssessmentUpdate, skillIdForAssessmentUpdate, validationObject);
    }
    await userTaskHandler.updateUserTasksStatus(userId);
    res.status(200).send();
}

// Not being used for now
// function getRequestSchema() {
//     return {
//         type: 'object',
//         properties: {
//             email: {
//                 type: 'string',
//                 minLength: 1,
//                 maxLength: 64,
//             },
//             payload_type: {
//                 type: 'string',
//             },
//             chatbot_uuid: {
//                 type: 'string',
//                 minLength: 1,
//                 maxLength: 64,
//             },
//             total_score: {
//                 type: 'number',
//             },
//             results: {
//                 type: 'object',
//                 properties: {
//                     value: {
//                         type: 'number',
//                     },
//                 },
//                 required: [],
//                 additionalProperties: true,
//             },
//         },
//         required: ['email', 'payload_type', 'chatbot_uuid', 'results'],
//         additionalProperties: true,
//     };
// }
