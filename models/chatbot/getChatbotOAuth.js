const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const settings = require('../settings');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const cryptoUtils = require('../services/common/cryptoUtils');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../services/error/errorUtils');


exports.register = function register(app) {
    app.get(
        '/chatbot/get/:skill_name',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getChatbot));
};

async function getChatbot(req, res) {
    let skillName = safeObjects.sanitize(req.params.skill_name);

    let user = await mongooseUserRepo.findOneById(req.user_id);
    let userChatbotToken = user.oauth_key;

    if(!userChatbotToken){
        userChatbotToken = cryptoUtils.genRandomString(16);
        await mongooseUserRepo.update({_id: req.user_id}, {'oauth_key':userChatbotToken});
    }

    let skill = await mongooseSkillRepo.findOne({name: skillName});

    if(skill && skill.validation && skill.validation.chatbot_uuid){
        let chatbotUuid = skill.validation.chatbot_uuid;
        let response = {oauth_key: userChatbotToken, chatbotUuid: chatbotUuid};
        res.status(200).send(response);
    }else{
        errorUtils.throwError("invalid skill name: "  + skillName + "  - skill object details " + JSON.stringify(skill), 400);
    }
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['skill_name'],
    properties: {
        skill_name: {
            type: 'string',
            minLength: 1,
        }
    },
    additionalProperties: false
};