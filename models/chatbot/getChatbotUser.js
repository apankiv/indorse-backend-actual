const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const settings = require('../settings');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const errorUtils = require('../services/error/errorUtils');
const REQUEST_FIELD_LIST = ['oAuthKey'];

/*
    Input: chatbotToken
    Return user_id and name

 */
exports.register = function register(app) {
    app.post(
        '/chatbot/user',
        validate({body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(getUserDetails));

};

async function getUserDetails(req, res) {
    // check request header

    if(req.x_api_key !== settings.CHATBOT_PRIVATE_KEY){
        errorUtils.throwError('Chatbot authentication failed', 403);
    }
    let requestFromImpress = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    let tmpKey = requestFromImpress.oAuthKey;


    let user = await mongooseUserRepo.findOne({'oauth_key':tmpKey});

    let response = {userID : user._id+"", name: user.name }
    let stringResponse = JSON.stringify(response);

    res.status(200).send(JSON.stringify(stringResponse)); // need to stringify twice
}

const BODY_SCHEMA = {
    type: 'object',
    required: ['oAuthKey'],
    properties: {
        oAuthKey: {
            type: 'string',
            minLength: 1,
        }
    },
    additionalProperties: false
};