const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimSignaturesRepo = require('../services/mongo/mongoose/mongooseClaimSignaturesRepo');
const claimUtils = require('../services/claims/claimUtils');
const VotingRound = require('../services/mongo/mongoose/schemas/votingRound');
const claimsEmailService = require('../services/common/claimsEmailService');
const settings = require('../settings');
const randomValidatorService = require('./randomValidatorService');
const timestampService = require('../services/common/timestampService');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const createClaimPayloadService = require('../services/signatures/createClaimPayloadService');
const lambdaSignerClient = require('../../smart-contracts/services/lambdaSignerClient');

module.exports.approveClaim = async function approveClaim(claim) {
    const claimantUserId = claim.ownerid;
    const randomValidators = await randomValidatorService.getValidatorsByClaim(claim);

    let claimant;
    if (claimantUserId) {
        claimant = await mongooseUserRepo.findOneById(claimantUserId);
    }
    const now = timestampService.createTimestamp();
    const votingDeadline = now + Number(settings.CLAIM_VOTE_PERIOD);

    const votingRound = {
        claim_id: claim._id.toString(),
        end_registration: now,
        end_voting: votingDeadline,
        status: VotingRound.statuses.IN_PROGRESS,
        number: 1,
    };

    const createdVotingRoundId = await mongooseVotingRoundRepo.insert(votingRound);

    const voterAddresses = [];

    for (let randomValidator of randomValidators) {

        let newVote = {
            claim_id: claim._id.toString(),
            voter_id: randomValidator.user_id.toString(),
            voting_round_id: createdVotingRoundId,
            isVotingRoundOn: true,
            is_signature: true,
            notified_at: timestampService.createTimestamp(),
            tier : randomValidator.tier
        };

        const voteId = await mongooseVoteRepo.insert(newVote);

        const user = await mongooseUserRepo.findOneById(randomValidator.user_id);

        if(!user || !user.ethaddress) {
            continue;
        }

        voterAddresses.push(user.ethaddress);
        await claimsEmailService.sendVoteInvitationEmail(user.name, claim._id.toString(), user.email);

        amplitudeTracker.publishData('validation_email_sent', {
            claim_id: claim._id.toString(),
            voting_round_id: createdVotingRoundId,
            vote_id: voteId
        }, {user_id: user._id.toString()});
    }

    const scDeadline = votingDeadline + (10 * Number(settings.CLAIM_VOTE_PERIOD));

    const validatorCount = await claimUtils.getValidatorsCountByClaim(claim);
    await mongooseClaimsRepo.update({ _id: claim._id.toString() }, { $set: { sc_deadline: scDeadline, aipLimit: validatorCount } });

    claim = await mongooseClaimsRepo.findOneById(claim._id.toString());

    const claimPayload = await createClaimPayloadService.createPayload(claim, voterAddresses);
    const signature = await lambdaSignerClient.claimPayloadLambda(claimPayload);

    const signatureId = await mongooseClaimSignaturesRepo.insert({
        payload: claimPayload.message,
        signature,
        merkelized: false,
    });
    await mongooseClaimsRepo.update({ _id: claim._id.toString() }, { $set: { signature_ref: signatureId } });

    const claimEvent = {};

    claimEvent._id = claim._id.toString();
    claimEvent.ownerid = claimantUserId;
    claimEvent.claim_id = claim._id.toString();

    amplitudeTracker.publishData('claim_approved', claimEvent, {});
    if (!claim.clientApp) {
        claimsEmailService.sendClaimApprovedEmail(claimant.name, claim._id.toString(), claimant.email);
    }
};
