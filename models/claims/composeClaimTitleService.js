const stringUtils = require('../services/blob/stringUtils');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');

module.exports.composeClaimTitle = async function composeClaimTitle(claimSkillName, skillLevel, isAssignment, assignmentScope) {
    // I can code in Python(Skill title) at the level of Beginner(Level)
    // I have solved a Full Stack assignment Java using Play Framework
    if (isAssignment) {
        const claimSkill = await mongooseSkillRepo.findOne({ name: claimSkillName.toLowerCase() });
        let parentSkillTitle;
        if (claimSkill && claimSkill.parents) {
            for (let i = 0; i < claimSkill.parents.length; i += 1) {
                const curParentSkill = await mongooseSkillRepo.findOneById(String(claimSkill.parents[i]));
                const curParentSkillTitle = curParentSkill.name;
                if (i > 0) {
                    parentSkillTitle += `, ${curParentSkillTitle}`;
                } else {
                    parentSkillTitle = curParentSkillTitle;
                }
            }
        }
        if (!parentSkillTitle) {
            parentSkillTitle = claimSkillName;
        }
        return `I have solved a ${assignmentScope} assignment in ${claimSkillName} using ${parentSkillTitle}`;
    }
    if (skillLevel) {
        return `I can code in ${claimSkillName} at the level of ${stringUtils.toTitleCase(skillLevel)}`;
    }
    return `I can code in ${claimSkillName}`;
};
