const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const { Validator } = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const createClaimService = require('../services/claims/createClaimService');
const { UserAssessmentClass } = require('../services/userAssessments/userAssessmentClass');
const blacklistMiddleware = require('../../middlewares/blacklist');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const REQUEST_FIELD_LIST = ['title', 'desc', 'proof', 'assessmentId'];

exports.register = function register(app) {
    app.post(
        '/claims',
        routeUtils.asyncMiddleware(blacklistMiddleware.getExpressMiddlewareForClaimCreate()),
        validate({ body: BODY_SCHEMA }),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(createClaim),
    );
};

async function createClaim(req, res) {
    const createClaimRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    createClaimRequest.feature_category = 'skill_validation';
    amplitudeTracker.publishData('claim_creation_initialized', createClaimRequest, req);
    delete createClaimRequest.feature_category;
    const requestingUserId = req.user_id;

    const requestingUser = await mongooseUserRepo.findOneById(requestingUserId);

    const claimId = await createClaimService.createClaim(createClaimRequest, requestingUser);
    const { assessmentId } = createClaimRequest;
    if (assessmentId) {
        const UserAssessment = new UserAssessmentClass(assessmentId, req.user_id);
        await UserAssessment.associateWithClaim(claimId);
    }
    res.status(200).send({ claim: createClaimRequest });
}

const BODY_SCHEMA = {
    type: 'object',
    required: ['title', 'desc', 'proof'],
    properties: {
        title: {
            type: 'string',
            allowedValues: ['Javascript', 'Java', 'Solidity', 'Python'],
        },
        desc: {
            type: 'string',
            minLength: 1,
        },
        proof: {
            type: 'string',
            allowedValues: 1,
        },
        assessmentId: { // added for magic liink functionality
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};
