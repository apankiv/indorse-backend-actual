const errorUtils = require('../services/error/errorUtils');
const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const { Validator } = require('express-json-validator-middleware');
const createClaimService = require('../services/claims/createClaimService');
const { authMethods } = require('../services/mongo/mongoose/schemas/clientApp');
const { verifyPartnerClaim } = require('../services/partners/verifyPartnerClaim');
// const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const REQUEST_FIELD_LIST = ['skillPrettyId', 'description', 'proof', 'partnerClientId', 'partnerUserId', 'state'];

const { attachRequestTracker } = require('../services/tracking/requestTracking');
const { directions } = require('../services/mongo/mongoose/schemas/requestLog');

exports.register = function register(app) {
    app.post(
        '/create-partner-claim',
        validate({ body: BODY_SCHEMA }),
        routeUtils.asyncMiddleware(authChecks.clientAppCheck({ required: true })), // add client_id and clientApp
        routeUtils.asyncMiddleware(allowCreateClaimRequest),
        routeUtils.asyncMiddleware(createPartnerClaim),
    );
};

async function allowCreateClaimRequest(req, res, next) {
    // needed for keeping track if the request is coming from legitimate client app
    await attachRequestTracker(req, res, { direction: directions.INCOMING });

    const { clientApp } = req;

    if (!clientApp) errorUtils.throwError('ClientApp not found in DB', 401);

    if (clientApp.auth === authMethods.HANDSHAKE) {
        const createClaimRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST); // get the request data
        await verifyPartnerClaim(clientApp, createClaimRequest.partnerUserId, createClaimRequest.state);
        next();
    } else if (clientApp.auth === authMethods.BASIC) {
        next();
    } else {
        errorUtils.throwError('Authentication method not yet supported', 403);
    }
}

async function createPartnerClaim(req, res) {
    const createClaimRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    if (!createClaimRequest.description) createClaimRequest.description = 'Partner Claim';
    const requestingClientApp = req.clientApp;
    createClaimRequest.partnerClientId = createClaimRequest.partnerClientId || requestingClientApp.client_id;
    let claimReturned = null;
    try {
        claimReturned = await createClaimService.createPartnerClaim(createClaimRequest, requestingClientApp);
    } catch (error) {
        // Explicitly throwing 500 to partner claim errors
        // These errors will be reported in slack and avoids
        // user friendly messages from being sent to the partners
        error.code = 500;
        throw error;
    }
    // return 500 in case claim is not created
    if (!claimReturned) {
        errorUtils.throwError('Claim not created', 500);
    }
    return res.status(200).json({
        success: true,
        message: 'Claim created successfully',
        data: {
            claimId: (claimReturned || {})._id,
        },
    });
}


const BODY_SCHEMA = {
    type: 'object',
    required: ['skillPrettyId', 'proof', 'partnerUserId'],
    properties: {
        skillPrettyId: {
            type: 'string',
            allowedValues: ['Javascript', 'Java', 'Solidity', 'Python'],
        },
        description: {
            type: 'string',
        },
        proof: {
            type: 'string',
            minLength: 1,
        },
        partnerClientId: { // needed only in case client app is configured for HANDSHAKE auth method in partner DB
            type: 'string',
            minLength: 1,
        },
        partnerUserId: {
            type: 'string',
            minLength: 1,
        },
        state: { // Needed only if client app is configured for HANDSHAKE auth method
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};
