const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const claimsEmailService = require('../services/common/claimsEmailService');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const { Validator } = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const { states: userAssignmentStates } = require('../services/mongo/mongoose/schemas/userAssignment');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const { ObjectID } = require('mongodb');
const { types: claimTypes } = require('../services/mongo/mongoose/schemas/claims');
const { sendClaimResultToPartner } = require('../services/claims/sendClaimResultToPartner');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const REQUEST_FIELD_LIST = ['disapprovalReason'];

exports.register = function register(app) {
    app.post(
        '/claims/:claim_id/disapprove',
        validate({ params: PARAMS_SCHEMA, body: BODY_SCHEMA }),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(disapproveClaim),
    );
};

async function disapproveClaim(req, res) {
    const claim_id = safeObjects.sanitize(req.params.claim_id);
    const rejectionRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    const claim = await mongooseClaimsRepo.findOneById(claim_id);

    if (!claim) {
        errorUtils.throwError('Claim not found!', 400);
    }

    if (claim.approved || claim.disapproved) {
        errorUtils.throwError('Claim already classified!', 400);
    }

    const claimantUserId = claim.ownerid;
    let claimant;
    if (claimantUserId) {
        claimant = await mongooseUserRepo.findOneById(claimantUserId);
    }

    const claimEvent = {};

    claimEvent._id = claim_id;
    claimEvent.ownerid = claimantUserId;
    claimEvent.claim_id = claim_id;

    await mongooseClaimsRepo.disapproveClaim(claim_id, rejectionRequest.disapprovalReason);

    if (claim.type === claimTypes.ASSIGNMENT && claim.user_assignment_id) {
        await mongooseUserAssignmentRepo.update({ _id: ObjectID(claim.user_assignment_id) }, { status: userAssignmentStates.EVALUATED });
    }

    amplitudeTracker.publishData('claim_disapproved', claimEvent, req);
    if (claimant) {
        claimsEmailService.sendClaimDisapprovedEmail(claimant.name, claim_id, claimant.email, rejectionRequest.disapprovalReason);
    } else if (claim.clientApp) {
        await sendClaimResultToPartner(claim_id).catch((error) => {
            console.log(error);
            console.log('Ignoring this error');
        });
    }
    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['claim_id'],
    properties: {
        claim_id: {
            type: 'string',
            minLength: 1,
        },
    },
};

const BODY_SCHEMA = {
    type: 'object',
    required: ['disapprovalReason'],
    properties: {
        disapprovalReason: {
            type: 'string',
            minLength: 1,
        },
    },
};

