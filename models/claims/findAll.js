const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongoClaimsRepo = require('../services/mongo/mongoRepository')('claims');
const mongoVotesRepo = require('../services/mongo/mongoRepository')('votes');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const stringUtils = require('../services/blob/stringUtils');
const getClaimService = require('../services/claims/getClaimService');
const { claimStage, claimSource } = require('../services/mongo/mongoose/schemas/claims');
const getClaimTierStatusService = require('./getClaimTierStatusService');
const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const { Validator } = require('express-json-validator-middleware');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const mongo = require('mongodb');

exports.register = function register(app) {
    app.get(
        '/claimsall',
        validate({ query: QUERY_SCHEMA }),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(findAll),
    );
};

async function findAll(req, res) {
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list claims!', 403);
    }

    const { source, stage } = req.query;
    const additionalFilter = generateFilter(source, stage);

    let searchParam = null;
    let sortParam = 'title';
    let sortOrder = 1;
    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.sort) {
        sortParam = req.query.sort;
        if (sortParam[0] === '-') {
            sortOrder = -1;
            sortParam = sortParam.substring(1);
        }
    }

    const sortObject = {};
    sortObject[sortParam] = sortOrder;
    let pageNo = safeObjects.sanitize(req.query.pageNo);
    let perPage = safeObjects.sanitize(req.query.perPage);

    if (!pageNo) {
        pageNo = 1;
    }
    if (!perPage) {
        perPage = 10;
    }

    const skip = (parseInt(pageNo, 10) - 1) * parseInt(perPage, 10);
    const limit = parseInt(perPage, 10);

    if (searchParam) {
        let o_id;

        if (mongo.ObjectID.isValid(searchParam)) {
            o_id = new mongo.ObjectID(searchParam);
        } else {
            o_id = searchParam;
        }

        const filters = {
            $or: [
                { title: stringUtils.createSafeRegexObject(searchParam) },
                { desc: stringUtils.createSafeRegexObject(searchParam) },
                { _id: o_id },
                { proof: stringUtils.createSafeRegexObject(searchParam) },
            ],
        };
        const combinedFilters = { ...filters, ...additionalFilter };
        const claimsCursor = await mongoClaimsRepo.findAllWithCursor(
            combinedFilters,
            { state: 0, visible: 0, tokens: 0 },
        );

        const sortedClaims = await claimsCursor.sort(sortObject).skip(skip).limit(limit).toArray();

        const matchingClaims = await mongoClaimsRepo.countWithQuery(combinedFilters);

        const claimsCount = await mongoClaimsRepo.count();

        // let totalVerifiedUsers =  await mongoUserRepo.countVerifiedUsers();
        // eslint-disable-next-line
        for (const key of Object.keys(sortedClaims)) {
            const claim = sortedClaims[key];
            sortedClaims[key].votes = await getVotes(claim._id);
            if ('ownerid' in claim && claim.ownerid !== undefined) {
                const owner = await getOwner(claim.ownerid);
                sortedClaims[key].ownername = owner ? owner.name : '';
            }
            if (!('final_status' in claim)) {
                console.log('Final status not present');
                sortedClaims[key].final_status = 'ongoing';
            }
            sortedClaims[key].createdAt = claim._id.getTimestamp().getTime() / 1000;
        }

        // eslint-disable-next-line
        for (const claim of sortedClaims) {
            await calculateClaimStatus(claim);
            if (claim && claim.clientApp && req.role !== 'admin') delete claim.clientApp;
        }

        const returnObj = {
            success: true,
            totalClaims: claimsCount,
            matchingClaims,
            claims: sortedClaims,
        };

        res.status(200).send(returnObj);
    } else {
        const claimsCursor = await mongoClaimsRepo.findAllWithCursor(
            additionalFilter,
            { state: 0, visible: 0, tokens: 0 },
        );

        const sortedClaims = await claimsCursor.sort(sortObject).skip(skip).limit(limit).toArray();

        const claimsCount = await mongoClaimsRepo.count();
        const matchingClaims = await mongoClaimsRepo.countWithQuery(additionalFilter);

        // eslint-disable-next-line
        for (const key of Object.keys(sortedClaims)) {
            const claim = sortedClaims[key];
            sortedClaims[key].votes = await getVotes(claim._id);
            if ('ownerid' in claim && claim.ownerid !== undefined) {
                const owner = await getOwner(claim.ownerid);
                sortedClaims[key].ownername = owner ? owner.name : '';
            }
            if (!('final_status' in claim)) {
                console.log('Final status not present');
                sortedClaims[key].final_status = 'ongoing';
            }
            sortedClaims[key].createdAt = claim._id.getTimestamp().getTime() / 1000;
        }

        // eslint-disable-next-line
        for (const claim of sortedClaims) {
            await calculateClaimStatus(claim);
            if (claim && claim.clientApp && req.role !== 'admin') delete claim.clientApp;
            claim.tiers = await getClaimTierStatusService.getClaimTierStatus(claim);
        }

        const returnObj = {
            success: true,
            totalClaims: claimsCount,
            matchingClaims,
            claims: sortedClaims,
        };

        res.status(200).send(returnObj);
    }
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

async function getOwner(userId) {
    const user = await mongooseUserRepo.findOneById(userId);
    return user;
}

async function getVotes(claim_id) {
    const votes_invited = await mongoVotesRepo.findAll({ claim_id: claim_id.toString() });
    const invited_count = votes_invited.length;
    let registered_count = 0;
    let indorsed_count = 0;
    let flagged_count = 0;
    votes_invited.forEach((vote) => {
        if ('registered' in vote && vote.registered === true) {
            registered_count += 1;
        }
        if ('endorsed' in vote && vote.endorsed === true) {
            indorsed_count += 1;
        }
        if ('endorsed' in vote && vote.endorsed === false) {
            flagged_count += 1;
        }
    });
    const vote_details = {};
    vote_details.invited = invited_count;
    vote_details.registered = registered_count;
    vote_details.indorsed = indorsed_count;
    vote_details.flagged = flagged_count;
    return vote_details;
}

async function calculateClaimStatus(claim) {
    const votingRound = await mongooseVotingRoundRepo.findOne({ claim_id: claim._id.toString() });

    claim.status = await getClaimService.getClaimStatus(claim, votingRound);
    if (claim.company) {
        claim.company = await mongooseCompanyRepo.findOneById(claim.company);
    }
}

function generateFilter(source, stage) {
    console.log(source, stage);
    const filter = {};
    if (source === claimSource.COMPANY) filter.company = { $exists: true, $ne: null };
    if (stage === claimStage.APPROVED) filter.approved = { $exists: true };
    if (stage === claimStage.DISAPPROVED) filter.disapproved = { $exists: true };
    if (stage === claimStage.PENDING_APPROVAL) {
        filter.disapproved = { $exists: false };
        filter.approved = { $exists: false };
    }
    return filter;
}

const QUERY_SCHEMA = {
    type: 'object',
    properties: {
        search: {
            type: 'string',
        },
        sort: {
            type: 'string',
        },
        pageNo: {
            type: 'string',
        },
        perPage: {
            type: 'string',
        },
        stage: {
            type: 'string',
            enum: claimStage.values,
        },
        source: {
            type: 'string',
            enum: [claimSource.COMPANY, claimSource.DIRECT],
        },
    },
    additionalProperties: true,
};
