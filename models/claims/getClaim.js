const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const getVoteService = require('../votes/getVoteService');
const { Validator } = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const validator = new Validator({ allErrors: true });
const { validate } = validator;
const getClaimService = require('../services/claims/getClaimService');
const getAssignmentFeedbackService = require('../services/claims/getAssignmentFeedbackService');
const mongooseClaimantGithub = require('../services/mongo/mongoose/mongooseClaimGithubsRepo');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseAssignmentRepo = require('../services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseClaimEventsRepo = require('../services/mongo/mongoose/mongooseClaimEvents');
const timestampService = require('../../models/services/common/timestampService');
const eventUtils = require('../../models/services/common/eventUtils');
const claims = require('../services/mongo/mongoose/schemas/claims');
const settings = require('../settings');
const getClaimTierStatusService = require('./getClaimTierStatusService');
const composeClaimTitleService = require('./composeClaimTitleService');
const authUtils = require('../services/auth/authChecks');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');

exports.register = function register(app) {
    app.get(
        '/claims/:claim_id',
        validate({ params: PARAMS_SCHEMA }),
        routeUtils.asyncMiddleware(getClaim),
    );
};

async function getClaim(req, res) {
    const claim_id = safeObjects.sanitize(req.params.claim_id);

    const claim = await mongooseClaimsRepo.findOneById(claim_id);

    if (!claim) {
        errorUtils.throwError('Claim not found', 404);
    }

    amplitudeTracker.publishData('claim_accessed', { claim_id }, req);

    const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());

    // Add claimEvent for viewed
    if (votingRound) {
        const claimEvent = {
            user_id: req.user_id,
            claim_id,
            voting_round_id: votingRound._id.toString(),
            event: 'claim_first_view',
            timestamp: timestampService.createTimestamp(),
        };

        // Only add when invited to vote
        const invitedToVote = await mongooseVoteRepo.findOne({
            voter_id: req.user_id,
            voting_round_id: votingRound._id.toString(),
        });
        if (invitedToVote && !(await eventUtils.isEventRecorded(claimEvent))) {
            await mongooseClaimEventsRepo.insert(claimEvent);
        }
    }

    const returnObj = {
        claim,
    };

    if (claim.company) claim.company = { _id: claim.company };

    if (claim.type === claims.types.ASSIGNMENT) {
        claim.userAssignment = await mongooseUserAssignmentRepo.findOneWithSkillDetails({ _id: claim.user_assignment_id });
        claim.assignment = await mongooseAssignmentRepo.findOneWithJobRole({ _id: claim.assignment_id });
    }

    if (!returnObj.claim.full_title) {
        if (claim.clientApp && claim.clientApp.clientId) {
            returnObj.claim.full_title = await composeClaimTitleService.composeClaimTitle(claim.title, false, false);
        } else if (claim.assignment) {
            returnObj.claim.full_title = await composeClaimTitleService.composeClaimTitle(claim.title, claim.level, true, claim.assignment.jobRole.title);
        } else {
            returnObj.claim.full_title = await composeClaimTitleService.composeClaimTitle(
                claim.title,
                claim.level,
                false,
            );
        }
        mongooseClaimsRepo.update(
            { _id: claim._id.toString() },
            { full_title: returnObj.claim.full_title },
        );
    }
    if (votingRound) {
        const votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id.toString());
        let numberOfTentativeVoteCompleted = 0;
        let numberOfVoteCompleted = 0;
        // eslint-disable-next-line no-restricted-syntax
        for (const vote of votes) {
            if (vote.sc_vote_exists) {
                numberOfVoteCompleted += 1;
            }
            if (vote.voted_at > 0) {
                numberOfTentativeVoteCompleted += 1;
            }
        }
        const companyOrAdminPermission = await authUtils.companyPermissionCheck(req, claim.company, ['magicLink.read', 'partnerClaims.read'], false);
        if ((claim.ownerid && req.user_id === claim.ownerid) || companyOrAdminPermission) {
            // Its a quickfix and need to refactored
            const feedback = await getAssignmentFeedbackService.getClaimFeedback(claim, null, req.user_id);
            if ((claim.ownerid && req.user_id === claim.ownerid) && !claim.canOwnerViewFeedback) {
                feedback.hiddenFeedbackCount = Math.max(feedback.explanation.length - 2, 0);
                if (feedback.hiddenFeedbackCount) feedback.explanation = feedback.explanation.slice(0, 2); // send only first two comments
            } else {
                feedback.hiddenFeedbackCount = 0;
            }
            claim.feedback = feedback;
        } else {
            for (const vote of votes) {
                if (vote.voter_id === req.user_id && vote.feedback && vote.feedback.explanation) {
                    claim.feedback = vote.feedback;
                    claim.feedback.explanation = [
                        {
                            feedback: vote.feedback.explanation,
                            endorsed: vote.endorsed,
                        },
                    ];
                    break;
                }
            }
        }


        let skills = await getTentativeVoteForSkills(claim, votes);


        const currentUserVote = votes.find(vote => vote.voter_id.toString() === req.user_id);
        if (currentUserVote) {
            returnObj.vote = await extractVote(currentUserVote);
            delete returnObj.vote.downvote;
            delete returnObj.vote.upvote;
            delete returnObj.vote.hide;
        }

        returnObj.claim.numberOfVoteCompleted = numberOfVoteCompleted;
        returnObj.claim.totalNumberOfVotes = claim.aipLimit;
        returnObj.claim.skills = skills;
        returnObj.votinground = votingRound;
    }

    claim.hasGithubDetails = false;

    const isClaimantHasGithubDetail = await mongooseClaimantGithub.findOneByClaimId(claim._id.toString());
    if (isClaimantHasGithubDetail) {
        claim.hasGithubDetails = true;
    }
    returnObj.claim.status = await getClaimService.getClaimStatus(claim, votingRound);
    returnObj.claim.tiers =  await getClaimTierStatusService.getClaimTierStatus(claim);

    // removing as client doesn't want to show their info on vote page
    if (returnObj && returnObj.claim && returnObj.claim.clientApp) delete returnObj.claim.clientApp;

    res.status(200).send(returnObj);
}

async function extractVote(voteDoc) {
    return await getVoteService.getVote(voteDoc._id.toString());
}

async function getTentativeVoteForSkills(claim, votes){
    let skills = [];
    let numberOfTentativeVoteCompleted = 0;
    if (claim.type === claims.types.ASSIGNMENT) {
        let assignmentSkills = claim.userAssignment.skills; // skills to iterate
        for(let i = 0 ; i < assignmentSkills.length ; i ++){
            let skillTentativeVoteObj = {};
            let assignmentSkillId = assignmentSkills[i].skill._id;
            skillTentativeVoteObj.skill = assignmentSkills[i].skill;
            let noOfTentativeVotes = 0;
            for(let j = 0 ; j < votes.length ; j ++){
                let curVote = votes[j];
                if((String(curVote.skillId) ===  String(assignmentSkillId)) && curVote.voted_at > 0 ){
                    noOfTentativeVotes = noOfTentativeVotes+1;
                }
            }
            let isMaxthresholdReached = false;
            if(noOfTentativeVotes >= claim.aipLimit / assignmentSkills.length){
                isMaxthresholdReached = true;
            }
            skillTentativeVoteObj.isMaxVoteReached = isMaxthresholdReached;
            skillTentativeVoteObj.noOfTentativeVotes = noOfTentativeVotes;
            skillTentativeVoteObj.maxTentativeVotes = claim.aipLimit / assignmentSkills.length;
            skills.push(skillTentativeVoteObj);
        }

    }else{

        let skillTentativeVoteObj = {};
        let isMaxthresholdReached = false;
        let skill = await mongooseSkillRepo.findOne({name: claim.title.toLowerCase()});
        skillTentativeVoteObj.skill = skill;
        for (let vote of votes) {
            if (vote.voted_at > 0) {
                numberOfTentativeVoteCompleted++;
            }
        }
        skillTentativeVoteObj.noOfTentativeVotes = numberOfTentativeVoteCompleted;
        if(skillTentativeVoteObj.noOfTentativeVotes >= claim.aipLimit){
            isMaxthresholdReached = true;
        }
        skillTentativeVoteObj.isMaxVoteReached = isMaxthresholdReached;
        skillTentativeVoteObj.maxTentativeVotes = claim.aipLimit;
        skills.push(skillTentativeVoteObj);
    }

    return skills;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['claim_id'],
    properties: {
        claim_id: {
            type: 'string',
            minLength: 1,
        },
    },
};
