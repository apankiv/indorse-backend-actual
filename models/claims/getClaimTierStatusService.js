const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');

module.exports.getClaimTierStatus = async function getClaimTierStatus(claim) {

    let tiersStatus = {};

    console.log("tier start");

    if (claim.validatorAssignedBytier) {
        for (let tierInfo of claim.validatorAssignedBytier) {
            if (tierInfo.noOfValidator === 0) {
                console.log("0 tier ", tierInfo)
                tiersStatus[tierInfo.tier] = {
                    maxVote: 0,
                    numTentativeVotes: 0
                }
            } else {
                let votesWithTierCount = await mongooseVoteRepo.count({
                    claim_id: claim._id, tier: tierInfo.tier, voted_at: {$gt: 0}
                });

                console.log("ok tier ", tierInfo);

                tiersStatus[tierInfo.tier] = {
                    maxVote: tierInfo.noOfValidator,
                    numTentativeVotes: votesWithTierCount
                }
            }
        }
    }

    console.log("TIERSTATUS", tiersStatus)

    return tiersStatus;
};
