const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseAssignmentRepo = require('../services/mongo/mongoose/mongooseAssignmentRepo');
const Claims = require('../services/mongo/mongoose/schemas/claims');
const routeUtils = require('../services/common/routeUtils');
const validate = require('../services/common/validate');
const safeObjects = require('../services/common/safeObjects');

exports.register = function register(app) {
    app.get('/users/:user_id/claims',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getUserClaims));
};

async function getUserClaims(req, res) {
    let user_id = safeObjects.sanitize(req.params.user_id);

    let claims = await mongooseClaimsRepo.findByOwnerId(user_id);

    let claimsToReturn = [];

    for (let claimIndex = 0; claimIndex < claims.length; claimIndex++) {
        let claim = claims[claimIndex];

        if(claim.type === Claims.types.ASSIGNMENT) {
            claim.userAssignment = await mongooseUserAssignmentRepo.findOneWithSkillDetails({_id : claim.user_assignment_id});
            claim.assignment = await mongooseAssignmentRepo.findOneWithJobRole({_id : claim.assignment_id});
        }

        let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());
        if (claim && claim.clientApp && req.role !== 'admin') delete claim.clientApp;
        claimsToReturn.push(
            {
                claim : claim,
                votinground: votingRound
            }
        );
    }

    res.status(200).send({claims : claimsToReturn});
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{24}$"
        }
    }
};