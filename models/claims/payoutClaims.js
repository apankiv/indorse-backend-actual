const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['pay'];

exports.register = function register(app) {
    app.post(
        '/payoutClaims',
        validate({body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(payoutClaims),
    );
};

async function payoutClaims(req, res) {
    let claimPayoutRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let unpaidVotes = await mongooseVoteRepo.findAllWithCursor({
        $and: [
            {sc_vote_exists: true},
            {paid: {$ne: true}}]
    });

    let payoutsToMake = {};

    for (let vote = await unpaidVotes.next(); vote != null; vote = await unpaidVotes.next()) {

        let voter = await mongooseUserRepo.findOne({_id: vote.voter_id});

        let currentCount = payoutsToMake[voter.ethaddress];

        if (!currentCount) {
            payoutsToMake[voter.ethaddress] = (vote.reward ? vote.reward : 5);
        } else {
            payoutsToMake[voter.ethaddress] += (vote.reward ? vote.reward : 5);
        }

        if (claimPayoutRequest.pay) {
            await mongooseVoteRepo.markAsPaid({_id: vote._id});
        }
    }

    res.status(200).send(payoutsToMake);
}

const BODY_SCHEMA = {
    type: 'object',
    required: ['pay'],
    properties: {
        pay: {
            type: 'boolean'
        }
    }
};

