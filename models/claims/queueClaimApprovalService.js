const timestampService = require('../services/common/timestampService');
const mongooseClaimQueueRepo = require('../services/mongo/mongoose/mongooseClaimQueueRepo');

exports.queueClaimApproval = async function queueClaimApproval(claim) {
    let now = timestampService.createTimestamp();

    await mongooseClaimQueueRepo.insert({
        claim_id: claim._id.toString(),
        released: false,
        approved_timestamp: now
    })
};