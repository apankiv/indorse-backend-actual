const mongooseValidatorRepo = require('../services/mongo/mongoose/mongooseValidatorRepo');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const errorUtils = require('../services/error/errorUtils');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const claims = require('../services/mongo/mongoose/schemas/claims');
const claimUtils = require('../services/claims/claimUtils');

const _ = require('lodash');

const _service = {};

function capitaliseFirstLetter(word) {
    return `${word.charAt(0).toUpperCase()}${word.substr(1)}`;
}

_service.getValidatorsByClaimTier = async function getValidatorsByClaimTier(skillName, requestingUserId,
                                                                            {source, companyId} = {}, claimTiers) {

    let selectedValidators = [];

    if (claimTiers) {

        for (let tierInfo of claimTiers) {
            if (tierInfo.noOfValidator !== 0) {
                let randomValidators = await mongooseValidatorRepo.getRandomSampleBySkillAndTier(
                    capitaliseFirstLetter(skillName),
                    requestingUserId,
                    tierInfo.tier
                );

                selectedValidators = selectedValidators.concat(randomValidators);
            }
        }
    }

    return selectedValidators;
};

_service.getAssignmentValidators = async function getAssignmentValidators(skills, requestingUserId,
                                                                          {source, companyId} = {}, tiers) {
    const validatorCount = await claimUtils.getValidatorsCount({source, companyId});
    const quotaPerSkill = validatorCount / skills.length;
    let returnListOfValidators = [];
    // eslint-disable-next-line no-restricted-syntax
    for (let i = 0; i < skills.length; i += 1) {
        const skill = skills[i];
        let listOfValidators = [];
        // 0. Get skill from mongo
        // eslint-disable-next-line no-await-in-loop
        const skillInDB = await mongooseSkillRepo.findOneById(skill.skill);

        if (!skillInDB) {
            errorUtils.throwError('Skill not found', 404);
        }

        console.log(`Sampling for ${skillInDB.name}`);
        console.log(`Skill object ${JSON.stringify(skillInDB)}`);

        // Create an array with all the skill which a validator can have for the skill validation
        // including parent skill as well
        const parentSkillIds = (skillInDB.parents || []).map(parentId => parentId.toString());
        const validSkillIds = [skillInDB._id.toString(), ...parentSkillIds];
        // eslint-disable-next-line no-await-in-loop
        const validSkillsFromDB = await mongooseSkillRepo.findAll({
            $and: [
                {
                    _id: {
                        $in: validSkillIds,
                    },
                },
                {
                    'validation.aip': true,
                },
            ],
        });

        // eslint-disable-next-line no-restricted-syntax
        for (let j = 0; j < validSkillsFromDB.length; j += 1) {
            const skillFromDB = validSkillsFromDB[j];
            console.log(`Sampling for ${skillFromDB.name}`);

            // Get validator count for each skill
            const randomValidators = await _service.getValidatorsByClaimTier(skillFromDB.name, requestingUserId,
                {source, companyId}, tiers);

            console.log(`Validators of parent are ${randomValidators.length}`);
            listOfValidators = _.unionWith(listOfValidators, randomValidators, _.isEqual);
        }

        // 2. get count by quota and throw error if it is less then the current list of validators
        if (listOfValidators.length < quotaPerSkill) {
            amplitudeTracker.publishData('claim_creation_failed_not_enough_validators', {skill: skillInDB.name}, {user_id: requestingUserId});
            errorUtils.throwError('Not enough validators for this skill in the database', 400);
        }

        returnListOfValidators = _.unionWith(returnListOfValidators, listOfValidators, _.isEqual);
    }
    console.log(`All Validators are ${JSON.stringify(returnListOfValidators)}`);

    if (returnListOfValidators.length < validatorCount) {
        amplitudeTracker.publishData('claim_creation_failed_not_enough_validators', '', {user_id: requestingUserId});
        errorUtils.throwError('Not enough validators for this skill in the database', 400);
    }

    return returnListOfValidators;
};

_service.getValidatorsByClaim = async function getValidatorsByClaim(claimData) {
    let randomValidators;
    const claimantUserId = claimData.ownerid;
    const {company: companyId} = claimData;
    const sourceOfClaim = await claimUtils.getClaimSource(claimData);
    if (claimData.type === claims.types.ASSIGNMENT) {
        const userAssignment = await mongooseUserAssignmentRepo.findOneById(claimData.user_assignment_id);
        const options = {source: sourceOfClaim, companyId};
        randomValidators = await _service.getAssignmentValidators(userAssignment.skills, claimantUserId, options,
            claimData.validatorAssignedBytier);
    } else {
        const options = {source: sourceOfClaim, companyId};
        randomValidators = await _service.getValidatorsByClaimTier(claimData.title, claimantUserId, options,
            claimData.validatorAssignedBytier);

        const validatorCount = await claimUtils.getValidatorsCount(options);

        if (randomValidators.length < validatorCount) {
            amplitudeTracker.publishData(
                'claim_creation_failed_not_enough_validators',
                {skill: claimData.title},
                {user_id: claimantUserId},
            );
            errorUtils.throwError('Not enough validators for this skill in the database', 400);
        }
    }

    return randomValidators;
};

module.exports = _service;