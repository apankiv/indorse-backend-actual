const { tiers } = require('../services/mongo/mongoose/schemas/validator');


/*
   Indicative prices for the tiers
    Tier 1 - Min = $20, Max = $20
    Tier 2 - Min = $4, Max = $9
    Tier 3 - Min = $3, Max = $6
 */
module.exports.rewardEstimationPerTier = function rewardEstimationPerTier(tier, noOfValidator) {
    let minReward;
    let maxReward;
    if(tier === tiers.TIER1){
        minReward = noOfValidator*20;
        maxReward = noOfValidator*20;
    }
    if(tier === tiers.TIER2){
        minReward = noOfValidator*4;
        maxReward = noOfValidator*9;
    }
    if(tier === tiers.TIER3){
        minReward = noOfValidator*3;
        maxReward = noOfValidator*6;
    }

    return {tier: tier, minReward: minReward, maxReward: maxReward};
};
