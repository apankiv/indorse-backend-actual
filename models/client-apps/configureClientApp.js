const errorUtils = require('../services/error/errorUtils');
const safeObjects = require('../services/common/safeObjects');
const routeUtils = require('../services/common/routeUtils');
const cryptoUtils = require('../services/common/cryptoUtils');
const mongooseInviteRepo = require('../services/mongo/mongoose/mongooseInviteRepo');
const mongooseClientAppRepo = require('../services/mongo/mongoose/mongooseClientAppRepo');
const { secureMethods } = require('../services/mongo/mongoose/schemas/clientApp');

const { Validator } = require('express-json-validator-middleware');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const { attachRequestTracker } = require('../services/tracking/requestTracking');
const { directions } = require('../services/mongo/mongoose/schemas/requestLog');

const REQUEST_FIELD_LIST = ['invite_token', 'display_name', 'client_secret', 'claim_result_hook'];

exports.register = function register(app) {
    app.post(
        '/client-app/configure',
        validate({ body: BODY_SCHEMA }),
        routeUtils.asyncMiddleware(configureClient),
    );
};

async function configureClient(req, res) {
    await attachRequestTracker(req, res, { direction: directions.INCOMING });
    const configureClientRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    const salt = cryptoUtils.genRandomString(12);
    const hashedObject = cryptoUtils.sha256(configureClientRequest.client_secret, salt);
    configureClientRequest.client_secret = hashedObject.passwordHash;
    const decodedData = await cryptoUtils.decodeJWTForInvite(configureClientRequest.invite_token);
    // find the invite using the invite token
    const criteriaInvite = {
        token: decodedData.invite_token,
        disabled: false,
    };
    const invite = await mongooseInviteRepo.findOne(criteriaInvite);
    if (!invite) {
        errorUtils.throwError('Invite not found or was already used', 403);
    }
    const clientApp = await mongooseClientAppRepo.findOneByClientId(decodedData.client_id);
    const criteria = {
        _id: clientApp._id,
    };
    const dataToSet = {
        $set: {
            client_secret: configureClientRequest.client_secret,
            salt,
            'hooks.sendPartnerClaimResult.url': configureClientRequest.claim_result_hook,
            'hooks.sendPartnerClaimResult.method': secureMethods.POST,
            display_name: configureClientRequest.display_name,
        },
    };
    await mongooseClientAppRepo.update(criteria, dataToSet);
    const savedClientApp = await mongooseClientAppRepo.findOneById(clientApp._id);
    delete savedClientApp.salt;
    delete savedClientApp.client_secret;
    return res.status(200).json(savedClientApp);
}


const BODY_SCHEMA = {
    type: 'object',
    required: ['invite_token', 'display_name', 'client_secret', 'claim_result_hook'],
    properties: {
        invite_token: {
            type: 'string',
            minLength: 3,
        },
        display_name: {
            type: 'string',
            minLength: 1,
        },
        client_secret: {
            type: 'string',
            minLength: 1,
        },
        claim_result_hook: {
            type: 'string',
            // /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
            minLength: 1,
        },
    },
    additionalProperties: false,
};
