exports.register = function register(app) {
    app.post('/fake-partner-result-reciever', fakePartnerClaimResultReciever);
};

// This route was added to test fakePartnerClaimResult hook (ID-1582)
// It returns the result that it recieves on this endpoint
function fakePartnerClaimResultReciever(req, res) {
    res.status(200).send(req.body);
}
