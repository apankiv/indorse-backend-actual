exports.register = function register(app) {
    app.get('/fake-partner-verify', fakePartnerVerify);
    app.post('/fake-partner-verify', fakePartnerVerify);
};

// This route was added to test verifyPartnerClaim hook (ID-1582)
// It just returns whatever http status is passed in query string
function fakePartnerVerify(req, res) {
    const body = { success: req.query.status.toString() === '200' };
    res.status(req.query.status).send(body);
}
