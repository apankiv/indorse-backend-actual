const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const { Validator } = require('express-json-validator-middleware');
const cryptoUtils = require('../services/common/cryptoUtils');
const timestampService = require('../services/common/timestampService');
const mongooseInviteRepo = require('../services/mongo/mongoose/mongooseInviteRepo');
const mongooseClientAppRepo = require('../services/mongo/mongoose/mongooseClientAppRepo');
const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const { authMethods } = require('../services/mongo/mongoose/schemas/clientApp');
const emailService = require('../services/common/emailService');
const authUtils = require('../services/auth/authChecks');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

const REQUEST_FIELD_LIST = ['email', 'display_name', 'companyPrettyId'];

const { attachRequestTracker } = require('../services/tracking/requestTracking');
const { directions } = require('../services/mongo/mongoose/schemas/requestLog');

exports.register = function register(app) {
    app.post(
        '/client-app/send-invite',
        validate({ body: BODY_SCHEMA }),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(sendClientAppInvite),
    );
};

async function sendClientAppInvite(req, res) {
    await attachRequestTracker(req, res, { direction: directions.INCOMING });
    const clientAppRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    let company = null;
    if (clientAppRequest.companyPrettyId) {
        company = await mongooseCompanyRepo.findOneByPrettyId(clientAppRequest.companyPrettyId);
    }
    await authUtils.companyPermissionCheck(req, (company && company._id), ['clientApps.write']);

    // create client id
    const clientId = cryptoUtils.genRandomString(24);
    // Create Invite
    const token = cryptoUtils.genRandomString(12);
    const inviteObject = {
        token,
        email: clientAppRequest.email,
        invitedBy: req.user_id,
        invitedAt: timestampService.createTimestamp(),
    };
    await mongooseInviteRepo.insert(inviteObject);
    const clientAppObject = {
        email: clientAppRequest.email,
        display_name: clientAppRequest.display_name,
        client_id: clientId,
        auth: authMethods.BASIC, // making it explicit for now
        company: (company && company._id && company._id.toString()) || null,
    };
    const clientAppId = await mongooseClientAppRepo.insert(clientAppObject);
    const encodeObject = {
        invite_token: token,
        client_id: clientId,
        display_name: clientAppRequest.display_name,
    };
    // encodedToken is sent in email
    const encodedToken = cryptoUtils.encodeJWTForInvite(encodeObject);
    await emailService.sendClientAppInvite(clientAppRequest.email, encodedToken, clientAppRequest.display_name, clientId);
    const clientApp = await mongooseClientAppRepo.findOneById(clientAppId);
    return res.status(200).send(clientApp);
}


const BODY_SCHEMA = {
    type: 'object',
    required: ['email', 'display_name'],
    properties: {
        email: {
            type: 'string',
            minLength: 3,
        },
        display_name: {
            type: 'string',
            minLength: 1,
        },
        companyPrettyId: {
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};
