const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const acceptInvitationProxy = require('../../services/ethereum/connections/proxy/acceptInvitationProxy');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const timestampService = require('../../services/common/timestampService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const emailService = require('../../services/common/emailService');

const REQUEST_FIELD_LIST = ['tx_hash'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/accept',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getAdvisors));
};

async function getAdvisors(req, res) {
    let acceptInvitationRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    let acceptingUser = await mongooseUserRepo.findOneByEmail(req.email);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorToAccept = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToAccept) {
        errorUtils.throwError("Invitation not found", 404);
    }

    if (advisorToAccept.verification_timestamp) {
        errorUtils.throwError("This invitation was been finalized", 400);
    }

    if (advisorToAccept.rejected_timestamp) {
        errorUtils.throwError("This invitation was already rejected", 400);
    }

    if (!advisorToAccept.user_id || advisorToAccept.user_id !== acceptingUser._id.toString()) {
        errorUtils.throwError("No permission to accept other user's invitation", 403);
    }

    await mongooseCompanyRepo.setAcceptedTimestamp(pretty_id, advisor_id, timestampService.createTimestamp());


    let metaData = {
        type: "COMPANY_ADVISOR",
        user_id: acceptingUser._id.toString(),
        company_id: company._id.toString()      
    }

    let acceptanceResult = await acceptInvitationProxy.acceptInvitation(acceptingUser._id, company._id, ConnectionType.IS_ADVISOR_OF,
         req.user_id,metaData);

    if (acceptInvitationRequest.tx_hash) {
        await mongooseCompanyRepo.setUserTxHash(pretty_id, advisor_id, acceptInvitationRequest.tx_hash);

        if (company.email) {
            await emailService.sendAdvisorAdvisorAcceptedInvitationToCompanyEmail(acceptingUser.name, company.company_name, company.email);
        }
        await emailService.sendYouConfirmedAdvisorConnectionInfoToUserEmail(acceptingUser.name, company.company_name, req.email, pretty_id);
    }

    if (acceptanceResult.statusCode === 5) {
        await mongooseCompanyRepo.setVerificationTimestamp(pretty_id, advisor_id, timestampService.createTimestamp(),);
    }

    res.status(200).send(acceptanceResult);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        tx_hash: {
            type: 'string',
            pattern: '^(0x)?[0-9aA-fF]{64}$'
        }
    },
    additionalProperties: false
};
