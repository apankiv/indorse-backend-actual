const companyToUserChecker = require('../../services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

exports.extractAdvisor = async function extractAdvisor(advisor, companyId) {
    let advisorToReturn = {};

    advisorToReturn._id = advisor._id;
    advisorToReturn.creation_timestamp = advisor.creation_timestamp;

    if (advisor.verification_timestamp) {
        advisorToReturn.verification_timestamp = advisor.verification_timestamp;
    }

    if (advisor.rejected_timestamp) {
        advisorToReturn.rejected_timestamp = advisor.rejected_timestamp;
    }

    if (advisor.accepted_timestamp) {
        advisorToReturn.accepted_timestamp = advisor.accepted_timestamp;
    }

    if (advisor.softConnection) {
        advisorToReturn.softConnection = advisor.softConnection;
    }

    if (advisor.name) {
        advisorToReturn.name = advisor.name;
    }

    if (advisor.imgUrl) {
        advisorToReturn.imgUrl = advisor.imgUrl;
    }

    if (!advisor.user_id) {
        advisorToReturn.status = 'unverified';
        advisorToReturn.signed_by_user = false;
        advisorToReturn.signed_by_company = false;
    } else {

        [advisorToReturn.signed_by_user, advisorToReturn.signed_by_company] = await Promise.all([
            userToCompanyChecker.checkIfConnectionExists(advisor.user_id, companyId, ConnectionType.IS_ADVISOR_OF),
            companyToUserChecker.checkIfConnectionExists(advisor.user_id, companyId, ConnectionType.IS_ADVISOR_OF)
        ]);


        if (advisor.company_tx_hash && advisorToReturn.signed_by_company) {
            advisorToReturn.company_tx_hash = advisor.company_tx_hash;
        }

        if (advisor.user_tx_hash && advisorToReturn.signed_by_user) {
            advisorToReturn.user_tx_hash = advisor.user_tx_hash;
        }

        if (advisorToReturn.signed_by_user && advisorToReturn.signed_by_company) {
            advisorToReturn.status = 'verified';
        } else {
            advisorToReturn.status = 'unverified';
        }
    }
    return advisorToReturn;
};

