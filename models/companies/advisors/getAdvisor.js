const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const advisorExtractor = require('./advisorExtractor');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/advisor/:advisor_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getAdvisor));
};

async function getAdvisor(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    if (!company.advisors) {
        errorUtils.throwError("Advisor not found", 404);
    }

    let advisor = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisor) {
        errorUtils.throwError("Advisor not found", 404);
    }

    let advisorToReturn = await advisorExtractor.extractAdvisor(advisor, company._id);
    let viewerIdentity, viewerRole;
    if (advisor.user_id) {
        let user = await mongooseUserRepo.findOneById(advisor.user_id);
        advisorToReturn.user = {
            img_url: user.img_url,
            photo_ipfs: user.photo_ipfs,
            name: user.name,
            username: user.username,
            id: user._id
        }

    }
    // viewer_identity - owner (the owner of this invitation) , not-logged-in, another user
    // viewer_role - admin / profile_access / full_access
    if(req.login){
        let viewer = await mongooseUserRepo.findOneByEmail(req.email);
        viewerRole = viewer.role;
        if(req.user_id === advisor.user_id){// advisor won't be null, hence error won't be thrown in this comparison.
            viewerIdentity = 'owner';
        }else{
            viewerIdentity = 'another_user'
        }

    }else{
        viewerIdentity = 'not_logged_in';
        viewerRole = 'not_logged_in';
    }

    let invitationDataToTrack = {
        viewer_identity: viewerIdentity,
        viewer_role: viewerRole,
        company_name: pretty_id,
        advisor_id: advisor_id,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994'
    }


    amplitudeTracker.publishData('advisor_viewed_invitation',invitationDataToTrack, req);

    res.status(200).send(advisorToReturn);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};