const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const authChecks = require('../../services/auth/authChecks');
const routeUtils = require('../../services/common/routeUtils');
const companyToUserChecker = require('../../services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const advisorExtractor = require('./advisorExtractor');

exports.register = function register(app) {
    app.get('/me/advisories',
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(myAdvisories));
};

async function myAdvisories(req, res) {
    let user = await mongooseUserRepo.findOneByEmail(req.email);

    let companiesInvitedTo = await mongooseCompanyRepo.findAll({'advisors.user_id' : user._id});

    let advisorsToReturn = [];

    for(let company of companiesInvitedTo) {
        let userAdvisories = company.advisors.filter(advisor => advisor.user_id ===  user._id.toString() && !advisor.rejected);

        for (let advisor of userAdvisories) {
            let advisorToReturn = await advisorExtractor.extractAdvisor(advisor, company._id, false);

            advisorToReturn.company = {
                pretty_id : company.pretty_id,
                id : company._id,
                logo_s3 : company.logo_s3,
                logo_ipfs : company.logo_ipfs,
                company_name : company.company_name
            };

            advisorsToReturn.push(advisorToReturn);
        }
    }

    res.status(200).send(advisorsToReturn);
}