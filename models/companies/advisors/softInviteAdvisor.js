const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const emailService = require('../../services/common/emailService');
const timestampService = require('../../services/common/timestampService');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const validator = new Validator({allErrors: true});
const validate = validator.validate;
const inviteAdvisorService = require('./inviteAdvisor');

const REQUEST_FIELD_LIST = ['email', 'imgUrl', 'name'];

exports.register = function register(app) {
    app.post(
        '/companies/:pretty_id/advisor/soft-invite',
        validate({body: BODY_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(softInviteAdvisor),
    );
};

async function softInviteAdvisor(req, res) {
    const inviteAdvisorRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    const pretty_id = safeObjects.sanitize(req.params.pretty_id);
    const company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    await inviteAdvisorService.checkIfAdvisorAlreadyExists(inviteAdvisorRequest, pretty_id);
    const userToInvite = await inviteAdvisorService.findInvitedUser(inviteAdvisorRequest);

    const creationTimestamp = timestampService.createTimestamp();

    let advisorID;
    let userName;

    if (!inviteAdvisorRequest.email) {
        errorUtils.throwError('Email must be provided for non-registered users', 400);
    }

    let advisor = {
        email: inviteAdvisorRequest.email,
        creation_timestamp: creationTimestamp            
    };        
    advisor.softConnection = {};
    advisor.softConnection.status = 'PENDING';
    advisor.softConnection.email = inviteAdvisorRequest.email;
    advisor.softConnection.statusUpdatedAt = creationTimestamp;
    advisor.name = inviteAdvisorRequest.name;
    advisor.imgUrl = inviteAdvisorRequest.imgUrl;

    let userTypeForTracking;
    if (userToInvite){
        advisor.user_id = userToInvite._id.toString()
        userName = userToInvite.name
        userTypeForTracking = userToInvite.role;
    }else{
        userTypeForTracking = 'new_user';
    }

    advisorID = await mongooseCompanyRepo.addAdvisor(pretty_id, advisor);

    let invitationDataToTrack = {
        invited_user_type: userTypeForTracking,
        company_name: company.company_name,
        advisor_id: advisorID,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994'
    }

    amplitudeTracker.publishData('advisor_invitation_sent',invitationDataToTrack, req);

    await emailService.softAdvisorInvitationEmail(company.company_name, pretty_id, advisorID, userName, inviteAdvisorRequest.email);

    const connectionToReturn = {
        _id: advisorID,
        creation_timestamp: creationTimestamp,
        name: inviteAdvisorRequest.name,
        imgUrl: inviteAdvisorRequest.imgUrl,
        softConnection : advisor.softConnection
    };

    if (userToInvite) {
        connectionToReturn.user = User.toSafeObject(userToInvite, req);
    }

    res.status(200).send(connectionToReturn);
}


const BODY_SCHEMA = {
    type: 'object',
    required: ['email', 'name'],
    properties: {
        email: {
            type: 'string',
            minLength: 1
        },
        imgUrl: {
            type: 'string',
            test: /^$|(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/
        },
        name: {
            type: 'string',
            length: {
                min: 1,
                max: 50
            }
        }
    },
    additionalProperties: false,
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};
