const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

const REQUEST_FIELD_LIST = ['name','imgUrl'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/update',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(updateAdvisorDetails));
};

async function updateAdvisorDetails(req, res) {
    let updateAdvisorDetailsReq = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);    

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorToUpdate = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToUpdate) {
        errorUtils.throwError("Advisor not found", 404);
    }

    if (advisorToUpdate.user_id){
        errorUtils.throwError("Advisor has already signed up", 422);
    }

    await mongooseCompanyRepo.setCompanyAdvisorDetails(pretty_id, advisor_id, updateAdvisorDetailsReq.imgUrl,updateAdvisorDetailsReq.name);  
    res.status(200).send(updateAdvisorDetailsReq);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
             minLength: 1,
             maxLength: 50           
        },
        imgUrl :{
            type : 'string',
            maxLength: 512,
            test: /^$|(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/
        }
    },
    additionalProperties: false
};
