const safeObjects = require('../services/common/safeObjects');
const mongoCompanyRepo = require('../services/mongo/mongoRepository')('company_names');
const routeUtils = require('../services/common/routeUtils');
const errorUtils = require('../services/error/errorUtils');

exports.register = function register(app) {
    app.get('/companies-validation',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {
    console.log("Returning aggregate data for companies")
    let pageNumber, pageSize, sortObj;

    if (req.query.pageNumber) {
        pageNumber = safeObjects.sanitize(req.query.pageNumber);
    } else {
        pageNumber = 1;
    }

    if (req.query.pageSize) {
        pageSize = safeObjects.sanitize(req.query.pageSize);
    } else {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    if (req.query.sort) {
        let sortParam = safeObjects.sanitize(req.query.sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }

    let companyCursor, totalCompanies;
    companyCursor = await mongoCompanyRepo.findAllWithCursor({ pretty_id: { $exists: true } });
    totalCompanies = await companyCursor.count();
    let sortedCompanies = await companyCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    let companyIndex = sortedCompanies.map((company) => {
        
        console.log("Building response for " + JSON.stringify(company));
        let allTeamMembers = 0;
        let allAdvisors = 0
        let verified_team_member  = 0;
        let verified_advisor = 0;


        if(company.team_members){
            allTeamMembers = company.team_members.length
            for (let member of company.team_members){
                if (member.company_tx_hash && member.user_tx_hash)
                    ++verified_team_member;
            }
        }


        if(company.advisors)  {
            allAdvisors = company.advisors.length
            for (let advisor of company.advisors) {
                if (advisor.company_tx_hash && advisor.user_tx_hash)
                    ++verified_advisor;
            }
        }
            


        let updatedAggregate = {
            pretty_id: company.pretty_id,
            url: company.url,
            id: company._id,
            name: company.company_name,
            total_team_members: allTeamMembers,
            total_advisors: allAdvisors,
            verified_team_members : verified_team_member,
            verified_advisors : verified_advisor,
            logo_s3: company.logo_s3,
            logo_ipfs: company.logo_ipfs
        }
        return updatedAggregate;
    });

    let responseObj = {
        companies: companyIndex,
        totalCompanies: totalCompanies
    };

    res.status(200).send(200, responseObj);
}