const errorUtils = require('../services/error/errorUtils');
const mongoCompanyRepo = require('../services/mongo/mongoRepository')('company_names');
const mongoUserRepo = require('../services/mongo/mongoRepository')('users');
const ObjectID = require('mongodb').ObjectID;

exports.getAdminId = async function getAdminId(request) {

    let companyAdmin = await mongoUserRepo.findOne({username: request.admin});

    if (!companyAdmin) {
        errorUtils.throwError('This admin does not exist!', 404);
    }

    if (companyAdmin.role !== 'admin') {
        errorUtils.throwError('This admin is not an admin!', 400);
    }

    return companyAdmin._id;
};

exports.userIsCompanyAdmin = async function userIsCompanyAdmin(email, pretty_id) {
    let company = await mongoCompanyRepo.findOne({pretty_id: pretty_id});

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    if (company.admin) {
        let companyAdmin = await mongoUserRepo.findOne({_id: new ObjectID(company.admin)});

        if (companyAdmin && companyAdmin.email !== email) {
            errorUtils.throwError('Requester is not the company admin', 403);
        }
    }
};