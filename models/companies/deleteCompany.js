const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseJobsRepo = require('../services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const companyAdminService = require('./companyAdminService');

exports.register = function register(app) {
    app.delete('/companies/:pretty_id',
        routeUtils.asyncMiddleware(deleteCompany));
};

async function deleteCompany(req, res) {
    let pretty_id = safeObjects.sanitize(req.param("pretty_id"));

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    const mongooseCompany = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    if (!mongooseCompany) {
        errorUtils.throwError('Company not found', 404);
    }
    const jobWithCompany = await mongooseJobsRepo.findOne({"company.id": mongooseCompany._id});
    if (jobWithCompany) {
        errorUtils.throwError('This company is linked to one or more jobs, please change the company in the jobs first', 400);
    }

    await companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    await mongooseCompanyRepo.deleteOne({_id: mongooseCompany});

    res.status(200).send();
}

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}