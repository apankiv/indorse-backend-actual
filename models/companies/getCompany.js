const mongocompanyRepo = require('../services/mongo/mongoRepository')('company_names');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const Company = require('../services/mongo/mongoose/schemas/company');
const { getAccessPermissions } = require('../services/companies/getAccessPermissions');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const { Validator } = require('express-json-validator-middleware');
const mongoose = require('mongoose');

const validator = new Validator({ allErrors: true });
const { validate } = validator;

exports.register = function register(app) {
    app.get(
        '/companies/:pretty_id',
        validate({ params: PARAMS_SCHEMA }),
        routeUtils.asyncMiddleware(getCompany),
    );
};

async function getCompany(req, res) {
    const pretty_id = safeObjects.sanitize(req.params.pretty_id);

    const company = await mongocompanyRepo.findOne({ pretty_id });

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    const safeCompany = Company.toSafeObject(company);

    if (req.permissions && req.permissions.admin && req.permissions.admin.read && company.email) {
        safeCompany.email = company.email;
    }

    safeCompany.has_admin_email = !!company.email;

    const companyConnectedUsers = [];

    if (safeCompany.advisors) { // due to the legacy copmany collections
        for (const advisor of safeCompany.advisors) {
            if (advisor.user_id) {
                const id = mongoose.Types.ObjectId(advisor.user_id);
                companyConnectedUsers.push(id);
            }
        }
    }
    if (safeCompany.team_members) {
        for (const teamMember of safeCompany.team_members) {
            if (teamMember.user_id) {
                const id = mongoose.Types.ObjectId(teamMember.user_id);
                companyConnectedUsers.push(id);
            }
        }
    }

    const companyUsersArray = await mongooseUserRepo.findAllWithKeys(companyConnectedUsers);

    for (const userObject of companyUsersArray) {
        if (safeCompany.advisors) {
            const userAdvisorUpdated = safeCompany.advisors.find(advisors => advisors.user_id === (userObject._id).toString());
            if (userAdvisorUpdated) {
                assignUserObject(userAdvisorUpdated, userObject);
            }
        }
        if (safeCompany.team_members) {
            const userTeamUpdated = safeCompany.team_members.find(teamMembers => teamMembers.user_id === (userObject._id).toString());
            if (userTeamUpdated) {
                assignUserObject(userTeamUpdated, userObject);
            }
        }
    }

    let userId;
    if (req.login && req.permissions) {
        userId = req.user_id;
    }
    const companyTrackingData = {
        company_id: company._id.toString(),
        pretty_id: company.pretty_id,
        company_name: company.company_name,
    };

    amplitudeTracker.publishData('company_loaded', companyTrackingData, req);

    // add permission object to company
    safeCompany.permissions = await getAccessPermissions({ userId: req.user_id, companyId: company._id });
    // check if the user has permission to read feature
    const permission = safeCompany.permissions;
    if (permission && permission.features && !permission.features.read) delete safeCompany.features;
    if (permission && permission.aip && !permission.aip.read) delete safeCompany.aipLimit;
    if (permission && permission.magicLink && !permission.magicLink.read) delete safeCompany.magicLink;
    res.status(200).send(safeCompany);
}

function assignUserObject(companyConnection, userObject) {
    companyConnection.user = {};
    companyConnection.user.id = userObject._id;
    companyConnection.user.img_url = userObject.img_url;
    companyConnection.user.photo_ipfs = userObject.photo_ipfs;
    companyConnection.user.name = userObject.name;
    companyConnection.user.username = userObject.username;
    companyConnection.user.ethaddress = userObject.ethaddress;
    companyConnection.user.bio = userObject.bio;
    delete companyConnection.user_id;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};

