const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const acceptInvitationProxy = require('../../services/ethereum/connections/proxy/acceptInvitationProxy');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const timestampService = require('../../services/common/timestampService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const emailService = require('../../services/common/emailService');

const REQUEST_FIELD_LIST = ['tx_hash'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/team_members/:teamMember_id/accept',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getTeamMembers));
};

async function getTeamMembers(req, res) {
    let acceptInvitationRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let teamMember_id = safeObjects.sanitize(req.params.teamMember_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    let acceptingUser = await mongooseUserRepo.findOneByEmail(req.email);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMemberToAccept = company.team_members.find(teamMember => teamMember._id.toString() === teamMember_id);

    if (!teamMemberToAccept) {
        errorUtils.throwError("Invitation not found", 404);
    }

    if (teamMemberToAccept.verification_timestamp) {
        errorUtils.throwError("This invitation was been finalized", 400);
    }

    if (teamMemberToAccept.rejected_timestamp) {
        errorUtils.throwError("This invitation was already rejected", 400);
    }

    if (!teamMemberToAccept.user_id || teamMemberToAccept.user_id !== acceptingUser._id.toString()) {
        errorUtils.throwError("No permission to accept other user's invitation", 403);
    }

    await mongooseCompanyRepo.teamMember.setAcceptedTimestamp(pretty_id, teamMember_id, timestampService.createTimestamp());

    let metaData = {
        type: "COMPANY_TEAM_MEMBER",
        user_id: acceptingUser._id.toString(),
        company_id: company._id.toString()
    }

    let acceptanceResult = await acceptInvitationProxy.acceptInvitation(acceptingUser._id, company._id, ConnectionType.IS_TEAM_MEMBER_OF, req.user_id,metaData);

    if (acceptInvitationRequest.tx_hash) {
        await mongooseCompanyRepo.teamMember.setUserTxHash(pretty_id, teamMember_id, acceptInvitationRequest.tx_hash);

        if (company.email) {
            await emailService.sendTeamMemberTeamMemberAcceptedInvitationToCompanyEmail(acceptingUser.name, company.company_name, company.email);
        }
        await emailService.sendTeamMemberTeamMemberConfirmedConnectionInfoToUserEmail(acceptingUser.name, company.company_name, req.email);
    }

    if (acceptanceResult.statusCode === 5) {
        await mongooseCompanyRepo.teamMember.setVerificationTimestamp(pretty_id, teamMember_id, timestampService.createTimestamp(),);
    }

    res.status(200).send(acceptanceResult);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'teamMember_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        teamMember_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        tx_hash: {
            type: 'string',
            pattern: '^(0x)?[0-9aA-fF]{64}$'
        }
    },
    additionalProperties: false
};