const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const teamMemberExtractor = require('./teamMemberExtractor');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/team_member/:teamMember_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getTeamMember));
};

async function getTeamMember(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let teamMember_id = safeObjects.sanitize(req.params.teamMember_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    if (!company.team_members) {
        errorUtils.throwError("TeamMember not found", 404);
    }

    let teamMember = company.team_members.find(teamMember => teamMember._id.toString() === teamMember_id);

    if (!teamMember) {
        errorUtils.throwError("TeamMember not found", 404);
    }

    let teamMemberToReturn = await teamMemberExtractor.extractTeamMember(teamMember, company._id);

    if (teamMember.user_id) {
        let user = await mongooseUserRepo.findOneById(teamMember.user_id);

        teamMemberToReturn.user = {
            img_url: user.img_url,
            photo_ipfs: user.photo_ipfs,
            name: user.name,
            username: user.username,
            id: user._id
        }
    }

    res.status(200).send(teamMemberToReturn);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'teamMember_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        teamMember_id: {
            type: 'string',
            minLength: 1
        }
    }
};