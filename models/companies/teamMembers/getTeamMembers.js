const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const teamMemberExtractor = require('./teamMemberExtractor');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/team_members',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getTeamMembers));
};

async function getTeamMembers(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMembersToReturn = [];

    let companyTeamMembers = [];

    if (company.team_members) {
        for (let teamMember of company.team_members) {
            companyTeamMembers.push([teamMember, company._id]);
        }
    }
    teamMembersToReturn = await Promise.all(companyTeamMembers.map((userCompnayTeamMemberPair) => extractTeamMemberWrapper(userCompnayTeamMemberPair[0], userCompnayTeamMemberPair[1])));

    res.status(200).send(teamMembersToReturn);
}

async function extractTeamMemberWrapper(teamMember, companyId) {
    let teamMemberToReturn = await teamMemberExtractor.extractTeamMember(teamMember, companyId);
    if (teamMember.user_id) {
        let user = await mongooseUserRepo.findOneById(teamMember.user_id);

        teamMemberToReturn.user = {
            img_url: user.img_url,
            photo_ipfs: user.photo_ipfs,
            name: user.name,
            username: user.username,
            id: user._id
        }
    }
    return teamMemberToReturn;
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};