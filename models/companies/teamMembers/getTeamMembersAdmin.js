const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const adminTeamMemberExtractor = require('./adminTeamMemberExtractor');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/team_members/admin',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(getTeamMembersAdmin));
};

async function getTeamMembersAdmin(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMembersToReturn = [];
    let adminTeamMembers = [];

    if (company.team_members) {
        for (let teamMember of company.team_members) {
            adminTeamMembers.push([teamMember,company._id]);
        }
    }
    teamMembersToReturn =  await Promise.all(adminTeamMembers.map((adminTeamMembersPair) =>{
        return extractTeamMemberWrapper(adminTeamMembersPair[0],adminTeamMembersPair[1]);
    }));
    res.status(200).send(teamMembersToReturn);
}

async function extractTeamMemberWrapper(teamMember, companyId){
    return await adminTeamMemberExtractor.extractTeamMember(teamMember, companyId);
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};