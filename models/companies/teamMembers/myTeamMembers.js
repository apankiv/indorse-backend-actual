const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const authChecks = require('../../services/auth/authChecks');
const routeUtils = require('../../services/common/routeUtils');
const companyToUserChecker = require('../../services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const teamMemberExtractor = require('./teamMemberExtractor');

exports.register = function register(app) {
    app.get('/me/team_memberships',
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(myTeamMemberies));
};

async function myTeamMemberies(req, res) {
    let user = await mongooseUserRepo.findOneByEmail(req.email);

    let companiesInvitedTo = await mongooseCompanyRepo.findAll({'team_members.user_id' : user._id});

    let teamMembersToReturn = [];

    for(let company of companiesInvitedTo) {
        let userTeamMemberies = company.team_members.filter(teamMember => teamMember.user_id ===  user._id.toString() && !teamMember.rejected);

        for (let teamMember of userTeamMemberies) {
            let teamMemberToReturn = await teamMemberExtractor.extractTeamMember(teamMember, company._id, false);

            teamMemberToReturn.company = {
                pretty_id : company.pretty_id,
                id : company._id,
                logo_s3 : company.logo_s3,
                logo_ipfs : company.logo_ipfs,
                company_name : company.company_name
            };

            teamMembersToReturn.push(teamMemberToReturn);
        }
    }

    res.status(200).send(teamMembersToReturn);
}