const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const timestampService = require('../../services/common/timestampService');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const emailService = require('../../services/common/emailService');

exports.register = function register(app) {
    app.post('/companies/:pretty_id/team_members/:teamMember_id/reject',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(rejectInvitation));
};

async function rejectInvitation(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let teamMember_id = safeObjects.sanitize(req.params.teamMember_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    let rejectingUser = await mongooseUserRepo.findOneByEmail(req.email);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMemberToReject = company.team_members.find(teamMember => teamMember._id.toString() === teamMember_id);

    if (!teamMemberToReject) {
        errorUtils.throwError("Invitation not found", 404);
    }

    if (teamMemberToReject.rejected_timestamp) {
        errorUtils.throwError("This invitation was already rejected", 400);
    }

    if (teamMemberToReject.verification_timestamp) {
        errorUtils.throwError("This invitation was already verified", 400);
    }

    if (!teamMemberToReject.user_id || teamMemberToReject.user_id !== rejectingUser._id.toString()) {
        errorUtils.throwError("No permission to reject other user's invitation", 403);
    }

    await mongooseCompanyRepo.teamMember.setRejectedTimestamp(pretty_id, teamMember_id, timestampService.createTimestamp());

    if (company.email) {
        await emailService.sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail(rejectingUser.name, company.company_name, company.email);
    }

    await emailService.sendTeamMemberTeamMemberRejectedInvitationToUserEmail(rejectingUser.name, company.company_name, req.email, pretty_id);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'teamMember_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        teamMember_id: {
            type: 'string',
            minLength: 1
        }
    }
};