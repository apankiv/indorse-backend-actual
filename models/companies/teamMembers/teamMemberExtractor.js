const companyToUserChecker = require('../../services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

exports.extractTeamMember = async function extractTeamMember(teamMember, companyId) {
    let teamMemberToReturn = {};

    teamMemberToReturn._id = teamMember._id;
    teamMemberToReturn.creation_timestamp = teamMember.creation_timestamp;

    if (teamMember.verification_timestamp) {
        teamMemberToReturn.verification_timestamp = teamMember.verification_timestamp;
    }

    if (teamMember.rejected_timestamp) {
        teamMemberToReturn.rejected_timestamp = teamMember.rejected_timestamp;
    }

    if (teamMember.accepted_timestamp) {
        teamMemberToReturn.accepted_timestamp = teamMember.accepted_timestamp;
    }

    if (teamMember.designation) {
        teamMemberToReturn.designation = teamMember.designation;
    }

    if (teamMember.name) {
        teamMemberToReturn.name = teamMember.name;
    }

    if (teamMember.imgUrl) {
        teamMemberToReturn.imgUrl = teamMember.imgUrl;
    }

    if (!teamMember.user_id) {
        teamMemberToReturn.status = 'unverified';
        teamMemberToReturn.signed_by_user = false;
        teamMemberToReturn.signed_by_company = false;
    } else {

        [teamMemberToReturn.signed_by_user, teamMemberToReturn.signed_by_company] = await Promise.all([
            userToCompanyChecker.checkIfConnectionExists(teamMember.user_id, companyId, ConnectionType.IS_TEAM_MEMBER_OF),
            companyToUserChecker.checkIfConnectionExists(teamMember.user_id, companyId, ConnectionType.IS_TEAM_MEMBER_OF)
        ]);


        if (teamMember.company_tx_hash && teamMemberToReturn.signed_by_company) {
            teamMemberToReturn.company_tx_hash = teamMember.company_tx_hash;
        }

        if (teamMember.user_tx_hash && teamMemberToReturn.signed_by_user) {
            teamMemberToReturn.user_tx_hash = teamMember.user_tx_hash;
        }

        if (teamMemberToReturn.signed_by_user && teamMemberToReturn.signed_by_company) {
            teamMemberToReturn.status = 'verified';
        } else {
            teamMemberToReturn.status = 'unverified';
        }
    }
    return teamMemberToReturn;
};

