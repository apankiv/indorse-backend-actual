const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseDesignationRepo = require('../../services/mongo/mongoose/mongooseDesignationRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

const REQUEST_FIELD_LIST = ['name','imgUrl','designation'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/team_members/:team_member_id/update',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(updateAdvisorDetails));
};

async function updateAdvisorDetails(req, res) {
    let updateTeamMemberDetailsReq = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let teamMember_id = safeObjects.sanitize(req.params.team_member_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);    

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMember = company.team_members.find(teamMember => teamMember._id.toString() === teamMember_id);       

    if (!teamMember) {
        errorUtils.throwError("Team member not found", 404);
    }

    if (updateTeamMemberDetailsReq.designation){
        const results = await mongooseDesignationRepo.findAll({ designation: updateTeamMemberDetailsReq.designation })
        if (results.length === 0) 
            errorUtils.throwError('Designation was not found', 404);    

    }

    //We only allow modification of designation if a user has signed up
    //If any other attributes are received with designation for update
    //we error out
    if (teamMember.user_id){
        if (!updateTeamMemberDetailsReq.designation)
            errorUtils.throwError("Team member has already signed up", 422);

        if (updateTeamMemberDetailsReq.imgUrl || updateTeamMemberDetailsReq.name)
                errorUtils.throwError("Team member has already signed up", 422);
    }

    
    await mongooseCompanyRepo.teamMember.setTeamMemberDetails(pretty_id, teamMember_id,
                                                    updateTeamMemberDetailsReq.imgUrl, 
                                                    updateTeamMemberDetailsReq.name, 
                                                    updateTeamMemberDetailsReq.designation);  

    res.status(200).send(updateTeamMemberDetailsReq);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'team_member_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        teamMember_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
             minLength: 1,
             maxLength: 50           
        },
        imgUrl :{
            type : 'string',
            maxLength: 512,
            test: /^$|(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/
        },
        designation: {
            type: 'string',
            minLength: 1,
            maxLength: 128
        }        
    },
    additionalProperties: false
};
