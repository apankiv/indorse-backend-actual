const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseJobsRepo = require('../services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const Company = require('../services/mongo/mongoose/schemas/company');
const coverUpdateService = require('./coverUpdateService');
const logoUpdateService = require('./logoUpdateService');
const routeUtils = require('../services/common/routeUtils');
const { Validator } = require('express-json-validator-middleware');

const validator = new Validator({ allErrors: true });
const { validate } = validator;
const { getAccessPermissions } = require('../services/companies/getAccessPermissions');

const companyAdminService = require('./companyAdminService');
const authUtils = require('../services/auth/authChecks');

const REQUEST_FIELD_LIST = ['company_name', 'cover_data', 'logo_data', 'description', 'admin', 'social_links',
    'additional_data', 'email', 'visible_to_public', 'tagline', 'features'];

exports.register = function register(app) {
    app.patch(
        '/companies/:pretty_id',
        validate({ body: REQUEST_SCHEMA, params: PARAMS_SCHEMA }),
        routeUtils.asyncMiddleware(updateCompany),
    );
};

async function updateCompany(req, res) {
    if (!req.user_id) {
        errorUtils.throwError('User not authorised', 403);
    }
    const updateCompanyRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    const pretty_id = safeObjects.sanitize(req.params.pretty_id);

    if (!pretty_id) {
        errorUtils.throwError('Invalid pretty_id', 422);
    }

    const companyCriteria = {
        pretty_id,
    };
    const companyToUpdate = await mongooseCompanyRepo.findOne(companyCriteria);
    const featuresUpdatePermission = await authUtils.companyPermissionCheck(req, companyToUpdate._id, ['features.write'], false);
    if (!featuresUpdatePermission) delete updateCompanyRequest.features;
    if (!companyToUpdate) {
        errorUtils.throwError('Company not found !', 404);
    }
    await authUtils.companyPermissionCheck(req, companyToUpdate._id, ['basic.write']);

    if (updateCompanyRequest.email) {
        const emailAlreadyInUse = await mongooseCompanyRepo.findOne({
            $and: [
                { email: updateCompanyRequest.email },
                { pretty_id: { $ne: pretty_id } },
            ],
        });

        if (emailAlreadyInUse) {
            errorUtils.throwError('This company email is already in use', 400);
        }
    }

    await companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    if (updateCompanyRequest.admin) {
        const adminID = await companyAdminService.getAdminId(updateCompanyRequest);
        updateCompanyRequest.admin_username = updateCompanyRequest.admin;
        updateCompanyRequest.admin = adminID;
    }

    if (updateCompanyRequest.logo_data) {
        await logoUpdateService.updateLogo(updateCompanyRequest, updateCompanyRequest.logo_data, pretty_id);
        delete updateCompanyRequest.logo_data;
    }

    if (updateCompanyRequest.cover_data) {
        await coverUpdateService.updateCover(updateCompanyRequest, updateCompanyRequest.cover_data, pretty_id);
        delete updateCompanyRequest.cover_data;
    }

    updateCompanyRequest.last_updated_by = req.username;
    updateCompanyRequest.last_updated_timestamp = Date.now();

    const oldCompany = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    await mongooseCompanyRepo.update({ pretty_id }, { $set: updateCompanyRequest });
    let companyToReturn = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (updateCompanyRequest.company_name !== oldCompany.company_name) {
        await updateJobsWithCompanyName(companyToReturn._id, companyToReturn.company_name);
    }
    const companyId = companyToReturn._id;
    companyToReturn = Company.toSafeObject(companyToReturn);
    // add permission object to company
    companyToReturn.permissions = await getAccessPermissions({ userId: req.user_id, companyId });
    // check if the user has permission to read feature
    const permission = companyToReturn.permissions;
    if (permission && permission.features && !permission.features.read) delete companyToReturn.features;

    res.status(200).send(companyToReturn);
}

// function userIsAuthorized(req) {
//     // Only superuser can add additional_data to company
//     if (req.body.additional_data && !(req.permissions && req.permissions.admin && req.permissions.admin.write)) {
//         return false;
//     }

//     return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
// }

async function updateJobsWithCompanyName(companyId, newCompanyName) {
    const jobsCursor = await mongooseJobsRepo.findAllWithCursor({ 'company.id': companyId });
    for (let job = await jobsCursor.next(); job !== null; job = await jobsCursor.next()) {
        job.company.name = newCompanyName;
        await mongooseJobsRepo.update({ _id: job._id.toString() }, job);
    }
}

const REQUEST_SCHEMA = {
    type: 'object',
    properties: {
        logo_data: {
            type: 'string',
            minLength: 1,
        },
        email: {
            type: 'string',
            pattern: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
        },
        tagline: {
            type: 'string',
            maxLength: 255,
        },
        cover_data: {
            type: 'string',
            minLength: 1,
        },
        company_name: {
            type: 'string',
            minLength: 1,
            maxLength: 255,
        },
        description: {
            type: 'string',
            minLength: 1,
        },
        admin: {
            type: 'string',
            minLength: 1,
        },
        additional_data: {
            type: 'object',
        },
        visible_to_public: {
            type: 'boolean',
        },
        // TODO social_links is a temporary hack, in the future a separate endpoint will be used
        social_links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    type: {
                        type: 'string',
                        minLength: 1,
                        maxLength: 64,
                    },
                    url: {
                        type: 'string',
                        minLength: 1,
                    },
                },
                required: ['type', 'url'],
                additionalProperties: false,
            },
        },
        features: {
            type: 'object',
            properties: {
                magicLink: {
                    type: 'boolean',
                },
                partnerClaims: {
                    type: 'boolean',
                },
            },
            additionalProperties: false,
        },
    },
    additionalProperties: false,
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};
