const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeUserObj = require('../../services/common/safeObjects');
const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo');
const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const getCompanyUtils = require('../../services/companies/getCompanyUtils');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const skillUtils = require('../../../models/services/skills/skillUtils');
const logger = require('../../../models/services/common/logger').getLogger();
const errorUtils = require('../../services/error/errorUtils');
const { userAssessmentStatusUpdate, sendAutoAllowedAssignmentEmail } = require('../../../models/services/companies/notification');

async function getStatsForMagicLink(assessmentId) {
    const defaultValues = {};
    // const query = [];
    // const stats = await mongooseUserAssessmentRepo.aggregate(query);
    return defaultValues;
}

module.exports.assessmentMagicLinkSanityCheck = function assessmentMagicLinkSanityCheck(assessmentMagicLink) {
    if (!assessmentMagicLink) {
        errorUtils.throwError("Oops, this magic link doesn't exist anymore", 404);
    }
    if (assessmentMagicLink.deleted) {
        errorUtils.throwError('This magic link was deleted by the admin !', 404);
    }
};

module.exports.getAssessmentMagicLink = async function getAssessmentMagicLink(assessmentMagicLink) {
    const objectToReturn = {};
    objectToReturn.id = assessmentMagicLink._id.toString();
    objectToReturn.title = assessmentMagicLink.title;
    objectToReturn.role = await mongooseJobRoleRepo.findOneById(assessmentMagicLink.role);
    objectToReturn.jobPostLink = assessmentMagicLink.jobPostLink;
    objectToReturn.createdAt = assessmentMagicLink.createdAt;
    objectToReturn.active = assessmentMagicLink.active !== false;
    objectToReturn.claimAllowed = assessmentMagicLink.claimAllowed === true;
    objectToReturn.chatbotSkills = assessmentMagicLink.chatbotSkills || [];
    objectToReturn.companyId = assessmentMagicLink.companyId;
    objectToReturn.publicId = assessmentMagicLink.publicId;
    objectToReturn.assignmentSkills = assessmentMagicLink.assignmentSkills;
    objectToReturn.stats = await getStatsForMagicLink(assessmentMagicLink._id);
    return objectToReturn;
};

module.exports.getUserAssessment = async function getUserAssessment(assessmentAttempted) {
    const userAssessment = {};
    const user = await mongooseUserRepo.findOneById(assessmentAttempted.startedBy);
    userAssessment._id = assessmentAttempted._id.toString();
    userAssessment.user = safeUserObj.safeUserObject(user);
    userAssessment.startedAt = assessmentAttempted.startedAt;
    userAssessment.claimId = assessmentAttempted.claimId;
    if (!user) {
        return userAssessment; // This could happen if user removed their account. We don't want to throw an error for that, instead silently ignore.
    }
    let {assignmentSkills} = assessmentAttempted;
    assignmentSkills = assignmentSkills || [];

    let {validatedChatbotSkills} = assessmentAttempted;
    validatedChatbotSkills = validatedChatbotSkills || [];
    for (let i = 0; i < validatedChatbotSkills.length; i += 1) {
        validatedChatbotSkills[i].skill = await mongooseSkillRepo.findOneById(validatedChatbotSkills[i].skillId);
    }
    for (let i = 0; i < assignmentSkills.length; i += 1) {
        assignmentSkills[i].allowedSkills = await skillUtils.findExactSkillsByIds(assignmentSkills[i].allowedSkillIds);
        assignmentSkills[i].allowedSkills = assignmentSkills[i].allowedSkills || [];
    }
    userAssessment.userChatbotStatus = assessmentAttempted.validatedChatbotSkills || [];
    if (assessmentAttempted.userAssignmentId) {
        userAssessment.userAssignment = await mongooseUserAssignmentRepo.findOneById(assessmentAttempted.userAssignmentId);
    }
    userAssessment.status = assessmentAttempted.status;
    return userAssessment;
};

function getUniqueAssessmentSkillsWithMaxScores(skillArray) {
    const object = {};

    // get uniques
    const uniqueSkills = (skillArray || []).filter((elem) => {
        if (object[elem.skillId] === undefined || object[elem.skillId] === null) {
            object[elem.skillId] = elem.chatbotScore;
            return true;
        }
        object[elem.skillId] = Math.max(object[elem.skillId], elem.chatbotScore);
        return false;
    });

    // maximize scores
    const uniqueSkillsWithMaxScores = (uniqueSkills || []).map((elem) => {
        const newObjectToReturn = elem;
        newObjectToReturn.chatbotScore = object[elem.skillId];
        return newObjectToReturn;
    });
    return uniqueSkillsWithMaxScores || [];
}

module.exports.updateUserAssessmentsWithSkill = async function updateUserAssessmentsWithSkill(userId, skillId, validationObject) {
    // add skillId to validationObject
    const objectToPush = {
        ...validationObject,
        skillId: skillId.toString(),
    };
    const criteria = {
        startedBy: userId,
        chatbotSkills: skillId.toString(),
        status: userAssessmentStates.CHATBOTS_PENDING, // fetch only those assignments where the status is chatbotsPending
    };
    const affectedAssessments = await mongooseUserAssessmentRepo.findAll(criteria);
    // please note that we are also updating the status for soft-deleted chatbots.
    affectedAssessments.forEach((userAssessment) => {
        userAssessment.validatedChatbotSkills.push(objectToPush);
        const updatedValidatedChatbotSkills = getUniqueAssessmentSkillsWithMaxScores(userAssessment.validatedChatbotSkills);
        const { chatbotSkills } = userAssessment;
        if (userAssessment.status === userAssessmentStates.CHATBOTS_PENDING) {
            const updateCriteria = {
                _id: userAssessment._id,
            };
            const chatbotStatus = (updatedValidatedChatbotSkills.length === chatbotSkills.length) ? userAssessmentStates.CHATBOTS_PASSED : userAssessmentStates.CHATBOTS_PENDING;
            let status = chatbotStatus;
            // Auto Allow Candidate to next level if enable for a magic link
            if (status === userAssessmentStates.CHATBOTS_PASSED && userAssessment.autoAllowAssignment === true) {
                status = userAssessmentStates.ASSIGNMENT_ALLOWED;
                sendAutoAllowedAssignmentEmail(userAssessment._id);
            }
            const statusUpdate = {
                $set: {
                    validatedChatbotSkills: updatedValidatedChatbotSkills,
                    chatbotStatus,
                    status,
                },
            };
            mongooseUserAssessmentRepo.update(updateCriteria, statusUpdate);
            // send email only if chatbot passed
            if (chatbotStatus === userAssessmentStates.CHATBOTS_PASSED) {
                userAssessmentStatusUpdate(chatbotStatus, userAssessment._id);
            }
        }
    });
};

module.exports.markAssignmentEvaluated = async function markAssignmentEvaluated(userAssignmentId, indorsed) {
    const criteria = {
        userAssignmentId: userAssignmentId.toString(),
    };
    let affectedAssessments = await mongooseUserAssessmentRepo.findAll(criteria);
    console.log('affectedAssessments: ', affectedAssessments);
    // below is done to keep the array iterable
    if (!Array.isArray(affectedAssessments)) affectedAssessments = [];
    // Return if no assessments are affected
    if (!affectedAssessments.length) return;
    // update proper fields
    const status = userAssessmentStates.ASSIGNMENT_EVALUATED;
    const dataToSet = {
        status,
        assignmentStatus: status,
        isAssignmentIndorsed: indorsed,
    };
    for (let i = 0; i < affectedAssessments.length; i += 1) {
        const { _id: userAssessmentId } = affectedAssessments[i];
        const criteriaToUpdate = { _id: userAssessmentId };
        // eslint-disable-next-line
        await mongooseUserAssessmentRepo.update(criteriaToUpdate, dataToSet);
        // eslint-disable-next-line
        await userAssessmentStatusUpdate(status, userAssessmentId);    
    }
};

module.exports.markClaimEvaluated = async function markClaimEvaluated(claimId, indorsed) {
    const criteria = {
        claimId: claimId.toString(),
    };
    let affectedAssessments = await mongooseUserAssessmentRepo.findAll(criteria);
    console.log('affectedAssessments: ', affectedAssessments);
    // below is done to keep the array iterable
    if (!Array.isArray(affectedAssessments)) affectedAssessments = [];
    // Return if no assessments are affected
    if (!affectedAssessments.length) return;
    // update proper fields
    const status = userAssessmentStates.CLAIM_EVALUATED;
    const dataToSet = {
        status,
        claimStatus: status,
        isClaimIndorsed: indorsed,
    };
    for (let i = 0; i < affectedAssessments.length; i += 1) {
        const { _id: userAssessmentId } = affectedAssessments[i];
        const criteriaToUpdate = { _id: userAssessmentId };
        // eslint-disable-next-line
        await mongooseUserAssessmentRepo.update(criteriaToUpdate, dataToSet);
        // eslint-disable-next-line
        await userAssessmentStatusUpdate(status, userAssessmentId);    
    }
};

module.exports.getUniqueAssessmentSkillsWithMaxScores = getUniqueAssessmentSkillsWithMaxScores;

/*
    hide all the assignments approved after limitReachedTimestamp
 */
module.exports.hideCreditLimitReachedAssignments = async function hideCreditLimitReachedAssignments(userAssessments, company) {

    if(!company){
        errorUtils.throwError('Company not found company_pretty ID = ' + company.pretty_id, 400);
    }

    let credits = company.credits.assessmentAssignment;
    let limitReachedTimestamp = credits.limitReachedTimestamp;

    if(!limitReachedTimestamp){
        return userAssessments;
    }
    for (let i = 0; i < userAssessments.length; i++) {
        let userAssessment = userAssessments[i];
        if (userAssessment && userAssessment.userAssignment) {
            let userAssignmentId = userAssessment.userAssignment._id;
            if (userAssignmentId) {
                let userAssignment = await mongooseUserAssignmentRepo.findOneById(userAssignmentId);
                if (userAssignment) {
                    let claimId = userAssignment.claim_id;
                    if (claimId) {
                        let claim = await mongooseClaimRepo.findOneById(claimId);
                        if (claim && claim.approved && claim.approved > limitReachedTimestamp) {
                            userAssessments[i].userAssignment = null;
                            userAssessments[i].isUserAssignmentHidden = true;
                        }
                    }
                }else{
                    logger.debug('User Assignment ID was found but according userAssignment deosnt exist. UserAssignmentId: ' + userAssignmentId);
                }
            }
        } else if (userAssessment && userAssessment.claimId) { // case for magic link with boast claims
            const claim = await mongooseClaimRepo.findOneById(userAssessment.claimId);
            if (claim && claim.approved && claim.approved > limitReachedTimestamp) {
                userAssessments[i].claimId = null;
                userAssessments[i].isUserAssignmentHidden = true;
            }
        }
    }
    return userAssessments;
};

module.exports.getUniqueAssessmentSkillsWithMaxScores = getUniqueAssessmentSkillsWithMaxScores;
