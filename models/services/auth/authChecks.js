const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../mongo/mongoose/mongooseCompanyRepo');
const { checkAccessPermissions: checkCompanyAccessPermission } = require('../companies/checkAccessPermissions');
const mongooseClientAppRepo = require('../mongo/mongoose/mongooseClientAppRepo');
const { authMethods } = require('../mongo/mongoose/schemas/clientApp');
const errorUtils = require('../error/errorUtils');
const cryptoUtils = require('../common/cryptoUtils');

/**
 *
 * @param role name
 * @returns {requiresRole}
 */
exports.roleCheck = function roleCheck(role) {
    return async function requiresRole(req, res, next) {
        const user = await mongooseUserRepo.findOneByEmail(req.email);
        if (user.role !== role.name && user.role !== 'admin') {
            errorUtils.throwError(`Insufficient role, requires:${role.toString()}`, 403);
        }

        next();
    };
};

/**
 *
 * @returns {loginCheck}
 */
exports.loginCheck = function loginCheck() {
    return async function loginCheck(req, res, next) {
        if (!req.login) {
            errorUtils.throwError('Must be logged in', 403);
        }
        next();
    };
};

/**
 *
 * @param permissions array of objects {role,permission}
 * @returns {requiresPermissions}
 */
exports.permissionCheck = function permissionCheck(permissions) {
    return async function requiresPermissions(req, res, next) {
        for (let index = 0; index < permissions.length; index += 1) {
            if (!req.permissions[permissions[index].role][permissions[index].operation]) {
                errorUtils.throwError(
                    `Insufficient permissions, requires:${permissions[index].role}.${permissions[index].operation}`,
                    403,
                );
            }
        }

        next();
    };
};

/*
   Checks the company permission for a user given his role
   @param userId, companyId and permissionPath
   @returns true or false
 */

exports.companyPermissionCheck = async function companyPermissionCheck({ user_id: userId, role }, companyId, permissionPaths, throwError = true) {
    if (role === 'admin') return true;
    if (!companyId) {
        if (throwError) {
            errorUtils.throwError('Insufficient Permission', 403);
        }
        return false;
    }

    const allowed = await checkCompanyAccessPermission({ userId, companyId, permissionPaths });
    if (!allowed && throwError) {
        errorUtils.throwError('Insufficient Permission', 403);
    }
    return allowed;
};

function secretIsCorrect(secret, clientApp) {
    try {
        const hashingResult = cryptoUtils.sha256(secret, clientApp.salt);
        return hashingResult.passwordHash === clientApp.client_secret;
    } catch (e) {
        return false;
    }
}

/**
 *
 * @returns {clientAppCheck}
 */
exports.clientAppCheck = function clientAppCheck({ required = true } = {}) {
    return async function validate(req, res, next) {
        // we can refactor this into methods
        if (req.client_app_credentials) {
            // if client-app is using basic auth method
            const credentials = Buffer.from(req.client_app_credentials, 'base64').toString();
            const [clientId, clientSecret] = credentials.split(':');
            const criteria = {
                client_id: clientId,
            };
            const clientApp = await mongooseClientAppRepo.findOne(criteria);
            if (secretIsCorrect(clientSecret, clientApp) && clientApp.auth === authMethods.BASIC) {
                if (clientApp.secret) delete clientApp.secret;
                if (clientApp.salt) delete clientApp.salt;
                req.clientApp = clientApp;
                next();
            } else {
                errorUtils.throwError(
                    `ClientApp not found with client id: ${clientId} or poorly configured client app`,
                    404,
                );
            }
        } else {
            const { partnerClientId: clientId } = req.body || {};
            if (required && !clientId) errorUtils.throwError('Client id not passed', 403);
            req.client_id = clientId;

            if (clientId) {
                const clientApp = await mongooseClientAppRepo.findOneByClientId(clientId);

                if (!clientApp) {
                    errorUtils.throwError(`ClientApp not found with client id: ${clientId}`, 404);
                }

                if (clientApp.secret) delete clientApp.secret;
                if (clientApp.salt) delete clientApp.salt;

                req.clientApp = clientApp;
                next();
            } else {
                // we don't want request to proceed if none of the cases/auth is satisfied
                errorUtils.throwError('No client id found for the request', 404);
                next();
            }
        }
    };
};

/*
   @param req , companyId
   @returns
 */

exports.adminOrCompanyAdminPermCheck = async function adminOrCompanyAdminPermCheck(req, companyId) {
    if (req.role === 'admin') {
        return true;
    }

    const company = await mongooseCompanyRepo.findOneById(companyId);
    const companyACL = company.acl || [];
    for (let i = 0; i < companyACL.length; i += 1) {
        const ACLUserId = (companyACL[i].userId || '').toString();
        if (ACLUserId && ACLUserId === req.user_id && companyACL[i].acceptedInvite) {
            return true;
        }
    }
    errorUtils.throwError('Insufficient permissions', 403);
};
