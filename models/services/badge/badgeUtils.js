exports.sortParams = {
    ascending : {
        prettyId: "ASCPRETTYID",
        badgeName: "ASCBADGENAME"
    },
    descending : {
        prettyId: "DESPRETTYID",
        badgeName: "DESBADGENAME"
    }
};