const imageService = require('./imageService');

exports.updateLogo = async function updateLogo(request, logo_data, pretty_id) {
    if (imageService.uploadsAreEnabled()) {
        let logoUploadResult = await imageService.uploadImage('company_logo_', logo_data, pretty_id);

        request.logo_s3 = logoUploadResult.s3;
        request.logo_ipfs = logoUploadResult.ipfs;
    }
};