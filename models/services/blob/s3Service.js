const settings = require('../../settings');
const config = require('config');
const parseDataUrl = require('parse-data-url');

let s3Bucket;
let s3ArchiveBucket;

if (uploadsAreEnabled()) {
    const AWS = require('aws-sdk');
    s3Bucket = new AWS.S3({
        params: { Bucket: config.get('aws.s3Bucket') },
        region: settings.AWS_REGION,
    });
    s3ArchiveBucket = new AWS.S3({
        params: { Bucket: config.get('aws.s3LinkedinArchiveBucket') },
        region: settings.AWS_REGION,
    });
}

/**
 *
 * @param bufferedData
 * @returns {Promise.<void>}
 */
exports.uploadFile = async function uploadFile(key, bufferedData) {
    if (uploadsAreEnabled()) {
        let data = {
            Key: key,
            Body: bufferedData,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
        };

        await new Promise((resolve, reject) => {
            s3Bucket.putObject(data, function(error, result) {
                if (!error) {
                    resolve(result);
                } else {
                    console.log('S3 ERROR:' + JSON.stringify(error));
                    reject(error);
                }
            });
        });
    }
};

exports.uploadFileFromDataURL = async function uploadFile(key, dataUrl) {
    if (uploadsAreEnabled()) {
        const parsed = parseDataUrl(dataUrl);

        if (parsed) {
            const data = {
                ContentEncoding: parsed.base64 ? 'base64' : 'utf-8',
                ContentType: parsed.contentType,
                Key: key,
                Body: parsed.toBuffer(),
            };

            await new Promise((resolve, reject) => {
                s3Bucket.putObject(data, (error, result) => {
                    if (!error) {
                        // Extra copyObject call required to update content encoding and type
                        const copyData = { ...data, CopySource: `${config.get('aws.s3Bucket')}/${key}`, MetadataDirective: 'REPLACE' };
                        delete copyData.Body;
                        s3Bucket.copyObject(copyData, (copyError, copyResult) => {
                            if (copyError) {
                                console.log(`S3 ERROR:${JSON.stringify(copyError)}`);
                                reject(copyResult);
                            } else {
                                resolve(result);
                            }
                        });
                    } else {
                        console.log(`S3 ERROR:${JSON.stringify(error)}`);
                        reject(error);
                    }
                });
            });
        }
    }
};

exports.getUploadedFileUrl = function getUploadedFileUrl(key) {
    if (uploadsAreEnabled()) {
        const bucketName = config.get('aws.s3Bucket');
        const timestamp = Math.floor(Date.now() / 1000);

        return `${bucketName}.s3.amazonaws.com/${key}?t=${timestamp}`;
    }

    return '';
};

exports.uploadLinkedinArchive = async function uploadLinkedinArchive(
    data,
    key
) {
    let base64data = new Buffer(data, 'binary');

    let params = {
        Key: key,
        Body: base64data,
        ContentEncoding: 'base64',
        ContentType: 'application/zip',
        ACL: 'authenticated-read',
    };

    if (uploadsAreEnabled()) {
        await new Promise((resolve, reject) => {
            s3ArchiveBucket.putObject(params, function(error, result) {
                if (!error) {
                    resolve(result);
                } else {
                    console.log('S3 ERROR:' + JSON.stringify(error));
                    reject(error);
                }
            });
        });
    }
};

function uploadsAreEnabled() {
    return (
        settings.ENVIRONMENT === 'staging' ||
        settings.ENVIRONMENT === 'production' ||
        settings.ENVIRONMENT === 'test_tmp' ||
        settings.ENVIRONMENT === 'ecs-integration' ||
        settings.ENVIRONMENT === 'integration'
    );
}
