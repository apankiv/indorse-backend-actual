const slugify = require('slugify');

/*
    Reference: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
    This function will go through an input String and will insert \ for special characters, so that we the String becomes regex-able.
    Please refer to the above Stackoverflow docs before testing this function.
    Input: String
    Output: Object {$regex: new RegExp(String)}
 */
exports.createSafeRegexObject = function escapeRegExp(str) {
    if(!str){
        return {$regex: new RegExp(str, "i")};
    }
    return {$regex: new RegExp(str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), "i")};
}

exports.truncate = function truncate(str, maxLength) {
	if (!str || typeof str !== 'string' || str.length <= maxLength) return str;

  	return str.substr(0, maxLength) + suffix;
}

exports.generateSlug = function generateSlug(parts = [], options = { replacement: '-', remove: null, lower: true }) {
  return slugify((parts || []).filter(Boolean).join(' '), options);
}

exports.createFullRegexObject = function fullRegExp(str) {
    return {$regex: new RegExp("^" + str + "$", "i")};
}
/*
    Reference: https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript <Accessed on 1st Oct 2018>
 */
exports.toTitleCase = function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

exports.removeExtension = function removeExtension(input){
    return input.substr(0, input.lastIndexOf('.')) || input;
}