const claims = require('../mongo/mongoose/schemas/claims');
const mongooseVotesRepo = require('../mongo/mongoose/mongooseVoteRepo');
const getAssignmentFeedbackService = require('./getAssignmentFeedbackService');
const mongooseUserAssignmentRepo = require('../mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../error/errorUtils');
const settings = require('../../settings');
const ObjectID = require('mongodb').ObjectID;

const levelThresholdsDescending = [
    { level: claims.levels.EXPERT, minimum: 24 },
    { level: claims.levels.INTERMEDIATE, minimum: 18 },
    { level: claims.levels.BEGINNER, minimum: 0 },
];

exports.calculateAssignmentResult = async function calculateAssignmentResult(claim) {
    if (!claim || !claim.type || claim.type !== claims.types.ASSIGNMENT) {
        return [];
    }
    claim = await mongooseClaimsRepo.findOneById(claim._id);
    // #region claim.type is ASSIGNMENT
    const result = [];
    
    // get user assignment
    const userAssignment = await mongooseUserAssignmentRepo.findOneById(claim.user_assignment_id);

    if (!userAssignment) {
        errorUtils.throwError('User assignment not found', 404);
    }

    if (!userAssignment.skills || userAssignment.skills.length <= 0) {
        errorUtils.throwError('User assignment has no skills', 404);
    }

    for (const assignmentSkill of userAssignment.skills) {
        const positiveVotesForSkill = await mongooseVotesRepo.countAll({
            claim_id: claim._id.toString(),
            voted_at: { $gt: 0 },
            endorsed: true,
            skillId: ObjectID(assignmentSkill.skill),
            sc_vote_exists: true,
        });

        // Currently there can be max two skills per user assg
        // And claim.aipLimit is always even
        // So no need to handle fractions
        const maxVotesForSkill = (claim.aipLimit / userAssignment.skills.length);
        const validated = positiveVotesForSkill > (maxVotesForSkill / 2);

        const feedback = await getAssignmentFeedbackService.getClaimFeedback(
            claim,
            assignmentSkill.skill
        );
        const sum =
            feedback.quality +
            feedback.designPatterns +
            feedback.readability +
            feedback.extensibility +
            feedback.completion +
            feedback.testCoverage;

        const levelItem = levelThresholdsDescending.find(item => sum >= item.minimum);
        const level = levelItem ? levelItem.level : claims.levels.BEGINNER;

        result.push({
            skill: assignmentSkill.skill,
            level,
            validated,
        });
    }

    // #endregion

    return result;
};
