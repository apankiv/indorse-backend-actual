const claims = require('../mongo/mongoose/schemas/claims');
const getAssignmentFeedbackService = require('./getAssignmentFeedbackService');

const levelThresholdsDescending = [
    {level: claims.levels.EXPERT, minimum: 20},
    {level: claims.levels.INTERMEDIATE, minimum: 15},
    {level: claims.levels.BEGINNER, minimum: 0},
];

exports.calculateAssignmentResult = async function calculateAssignmentResult(claim) {
    const feedback = await getAssignmentFeedbackService.getClaimFeedback(claim);

    const sum =
        feedback.quality +
        feedback.designPatterns +
        feedback.readability +
        feedback.extensibility +
        feedback.testCoverage;

    const levelItem = levelThresholdsDescending.find(item => sum >= item.minimum);
    return levelItem ? levelItem.level : claims.levels.BEGINNER;
};
