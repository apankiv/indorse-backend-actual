const mongooseVoteRepo = require('../mongo/mongoose/mongooseVoteRepo');
const { claimStatusForPartners } = require('../mongo/mongoose/schemas/claims');
const { getClaimStatusForPartnerClaim } = require('./getClaimService');
const mongooseClientAppRepo = require('../mongo/mongoose/mongooseClientAppRepo');
const mongooseCompanyRepo = require('../mongo/mongoose/mongooseCompanyRepo');
const { claimSource, claimReportAccessStatus } = require('../mongo/mongoose/schemas/claims');
const errorUtils = require('../../../models/services/error/errorUtils');
const settings = require('../../settings');

const _claimUtils = {};
const MIN_RATING_PER_SCORE = 1;
const MAX_RATING_PER_SCORE = 5;
// eslint-disable-next-line
const linearScale = (num, in_min, in_max, out_min, out_max) => {
    // Here:
    // in_min and in_max is the range from which the number is given
    // out_min and out_max is the range on which the number is mapped
    return ((num - in_min) * ((out_max - out_min) / (in_max - in_min))) + out_min;
};

_claimUtils.normalizeRating = function normalizeRating(averageRatingArray) {
    if (!Array.isArray(averageRatingArray)) throw new Error('Pass array of numbers');
    const numberOfRatings = averageRatingArray.length;
    const minScorePerClaim = numberOfRatings * MIN_RATING_PER_SCORE;
    const maxScorePerClaim = numberOfRatings * MAX_RATING_PER_SCORE;
    let sum = 0;
    averageRatingArray.forEach((elem) => {
        sum += parseFloat(elem);
    });
    const normalisedRating = linearScale(sum, minScorePerClaim, maxScorePerClaim, 1, 10);
    return Math.round(normalisedRating) || 1;
};

_claimUtils.formatResultForSendingToClientApp = async function formatResultForSendingToClientApp(claim) {
    const status = await getClaimStatusForPartnerClaim(claim); // it either returns the statuses to be sent to partners or false
    if (!status || status === claimStatusForPartners.IN_PROGRESS) return false;
    const noUpdateStatuses = [claimStatusForPartners.TENTATIVELY_FLAGGED, claimStatusForPartners.TENTATIVELY_INDORSED];
    if (noUpdateStatuses.indexOf(status) > -1) {
        return false;
    }
    const noRatingStatuses = [claimStatusForPartners.CLAIM_DISAPPROVED, claimStatusForPartners.CLAIM_SUBMITTED];
    if (noRatingStatuses.indexOf(status) > -1) {
        const responseToReturn = {
            id: claim._id,
            status,
            proof: claim.proof,
            reason: claim.disapprove_reason || '', // setting to disapprove_reason in case of disapproved.
            feedback: {}, // empty feedback will be sent to the service
        };
        return responseToReturn;
    }
    const filters = [{ claim_id: claim._id }, { voted_at: { $gt: 0 } }, { sc_vote_exists: true }];
    let votes = await mongooseVoteRepo.findAll({
        $and: filters,
    });
    votes = votes || [];
    const entries = [];
    const accumulater = {
        quality: 0,
        designPatterns: 0,
        testCoverage: 0,
        endorsed: 0,
        readability: 0,
        extensibility: 0,
        explanation: [],
    };
    votes.forEach((vote) => {
        if (vote.feedback) {
            let { feedback } = vote;
            feedback = feedback || {};
            const objectToPush = {
                rating: {
                    quality: feedback.quality,
                    designPatterns: feedback.designPatterns,
                    testCoverage: feedback.testCoverage,
                    readability: feedback.readability,
                    extensibility: feedback.extensibility,
                },
                endorsed: vote.endorsed,
                explanation: (vote && vote.hide && vote.hide.isHidden) ? '' : feedback.explanation,
            };
            entries.push(objectToPush);
            accumulater.quality += feedback.quality;
            accumulater.designPatterns += feedback.designPatterns;
            accumulater.testCoverage += feedback.testCoverage;
            accumulater.readability += feedback.readability;
            accumulater.extensibility += feedback.extensibility;
            accumulater.endorsed += vote.endorsed;
            accumulater.explanation.push({ // this is not sent at the moment
                explanation: feedback.explanation,
                endorsed: vote.endorsed,
            });
        }
    });
    const averageRating = {
        quality: (votes.length && Math.floor(accumulater.quality / votes.length)) || 0,
        designPatterns: (votes.length && Math.floor(accumulater.designPatterns / votes.length)) || 0,
        testCoverage: (votes.length && Math.floor(accumulater.testCoverage / votes.length)) || 0,
        readability: (votes.length && Math.floor(accumulater.readability / votes.length)) || 0,
        extensibility: (votes.length && Math.floor(accumulater.extensibility / votes.length)) || 0,
    };
    const filteredNonZeroRating = (Object.values(averageRating) || []).filter(Boolean);
    const response = {
        id: claim._id,
        status,
        proof: claim.proof,
        reason: '', // setting empty in other cases.
        feedback: {
            normalizedRating: _claimUtils.normalizeRating(filteredNonZeroRating),
            averageRating,
            entries,
        },
    };
    return response;
};

_claimUtils.findClaimClientApp = async function findClaimClientApp(claim) {
    if (!claim || !claim.clientApp || !claim.clientApp.clientId) return null;

    const { clientId } = claim.clientApp;
    const clientApp = await mongooseClientAppRepo.findOneByClientId(clientId);

    if (!clientApp) errorUtils.throwError('Client App not found', 404);

    return clientApp;
};

_claimUtils.prepareClaimClientApp = async (claim) => {
    const clientApp = await this.findClaimClientApp(claim);
    let company = null;
    if (clientApp && clientApp.company) {
        company = await mongooseCompanyRepo.findOneById(clientApp.company);
    }

    if (!clientApp) {
        return {};
    }
    return {
        displayName: (company && company.company_name) || clientApp.display_name,
    };
};

_claimUtils.prepareClaimObject = async claim => claim;

_claimUtils.prepareClaimReportAccessObject = async (claim) => {
    const ClaimReportAccessObject = {
        status: claim.canOwnerViewFeedback ? claimReportAccessStatus.ALLOWED : claimReportAccessStatus.NOT_ALLOWED,
    };
    return ClaimReportAccessObject;
};

_claimUtils.getValidatorsCount = async function getValidatorsCount({ source, companyId } = {}) {
    // enabling
    if (source && ([claimSource.SME, claimSource.PARTNER].indexOf(source) > -1)) { // Types that are supported
        // find company
        const companyFound = await mongooseCompanyRepo.findOneById(companyId);
        return companyFound.aipLimit || Number(settings.CLAIM_VALIDATOR_COUNT);
    }
    return Number(settings.CLAIM_VALIDATOR_COUNT);
};

_claimUtils.getClaimSource = async function getClaimSource({ company: companyId, ownerid: claimantUserId } = {}) {
    let sourceOfClaim = null;
    if (companyId) {
        sourceOfClaim = claimantUserId ? claimSource.SME : claimSource.PARTNER;
    } else {
        sourceOfClaim = claimSource.NORMAL;
    }
    return sourceOfClaim;
};

_claimUtils.getValidatorsCountByClaim = async function getValidatorsCountByClaim(claim) {
    if (!claim.aipLimit) {
        const source = await _claimUtils.getClaimSource(claim);
        return await _claimUtils.getValidatorsCount({ source, companyId: claim.company });
    }
    return claim.aipLimit;
};

module.exports = _claimUtils;
