const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../error/errorUtils');
const randomValidatorService = require('../../claims/randomValidatorService');
const claimsEmailService = require('../common/claimsEmailService');
const amplitudeTracker = require('../tracking/amplitudeTracker');
const slackService = require('../common/slackService');
const githubService = require('../social/githubService');
const timestampService = require('../common/timestampService');
const claims = require('../mongo/mongoose/schemas/claims');
const mongooseAssignmentRepo = require('../mongo/mongoose/mongooseAssignmentRepo');
const mongooseSkillRepo = require('../mongo/mongoose/mongooseSkillRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');

exports.createClaim = async function createClaim(userAssignment, requestingUser, companyId) {
    const assignment = await mongooseAssignmentRepo.findOne({ _id: userAssignment.assignment_id });

    if (!assignment) {
        errorUtils.throwError('This assignment does not exist!', 404);
    }

    if (assignment.disabled) {
        errorUtils.throwError('This assignment is disabled!', 400);
    }
    const createClaimRequest = {};
    // attach company id if companyId param is passed
    if (companyId) {
        createClaimRequest.company = companyId;
    }

    createClaimRequest.state = claims.states.NEW;

    // TODO: Change the logic
    createClaimRequest.title = 'Assignment';
    createClaimRequest.proof = userAssignment.proof;
    createClaimRequest.level = claims.levels.BEGINNER;
    createClaimRequest.type = claims.types.ASSIGNMENT;
    createClaimRequest.user_assignment_id = userAssignment._id;
    createClaimRequest.assignment_id = assignment._id;
    createClaimRequest.desc = `<b>${assignment.title}</b><br/>${assignment.description}`;
    createClaimRequest.visible = true;
    createClaimRequest.is_sc_claim = true;
    createClaimRequest.ownerid = requestingUser._id;
    createClaimRequest.created_at = timestampService.createTimestamp();
    createClaimRequest.endorse_count = 0;
    createClaimRequest.flag_count = 0;
    createClaimRequest.submission_event_computed = false;
    createClaimRequest.merkelized = false;
    const createdClaimId = await mongooseClaimsRepo.insert(createClaimRequest);

    createClaimRequest._id = createdClaimId;
    createClaimRequest.ownerid = requestingUser._id;
    createClaimRequest.claim_id = createdClaimId;

    amplitudeTracker.publishData('assignment_claim_creation_successful', createClaimRequest, { user_id: requestingUser._id });
    githubService.parseGithubUrlExtractClaimantData(requestingUser.github_uid, userAssignment.proof, createdClaimId);

    claimsEmailService.sendClaimAwaitingApproval(createdClaimId);

    // Add skills on user
    for (assignmentSkill of userAssignment.skills) {
        let userHasSkill;
        if (requestingUser.skills) {
            userHasSkill = requestingUser.skills.find(s => s.skill && s.skill._id === assignmentSkill.skill.toString());
        }

        const validationToAdd = {
            type: 'claim',
            level: 'beginner',
            id: createdClaimId,
            validated: false,
        };

        if (userHasSkill) {
            await mongooseUserRepo.addValidationToSkill(requestingUser.email, userHasSkill.skill.name, validationToAdd);
        } else {
            const skill = await mongooseSkillRepo.findOneById(assignmentSkill.skill.toString());
            await mongooseUserRepo.addSkill(requestingUser.email, {
                skill,
                level: createClaimRequest.level,
                validations: [validationToAdd],
            });
        }
    }

    slackService.reportClaim(createClaimRequest, requestingUser.img_url, requestingUser.username);
    return createdClaimId;
};
