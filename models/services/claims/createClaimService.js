const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const mongooseSkillRepo = require('../mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../error/errorUtils');
const mongooseVotingRoundRepo = require('../mongo/mongoose/mongooseVotingRoundRepo');
const VotingRound = require('../mongo/mongoose/schemas/votingRound');
const randomValidatorService = require('../../claims/randomValidatorService');
const claimsEmailService = require('../common/claimsEmailService');
const amplitudeTracker = require('../tracking/amplitudeTracker');
const slackService = require('../common/slackService');
const githubService = require('../social/githubService');
const timestampService = require('../common/timestampService');
const claims = require('../mongo/mongoose/schemas/claims');

module.exports.createClaim = async function createClam(claimRequest, requestingUser) {
    const createClaimRequest = claimRequest;
    const existingClaimsSameLevel = await mongooseClaimsRepo.findAll({
        $and: [
            {title: createClaimRequest.title},
            {level: "expert"},
            {ownerid : requestingUser._id}
        ]
    });

    for (let i = 0; i < existingClaimsSameLevel.length; i += 1) {
        const existingClaimSameLevel = existingClaimsSameLevel[i];
        if (existingClaimSameLevel.final_status) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_obtained', createClaimRequest, {user_id: requestingUser._id});
            errorUtils.throwError("A claim for that skill with maximum level has already been obtained!", 400)
        } else if (!existingClaimSameLevel.approved && !existingClaimSameLevel.disapproved && !existingClaimSameLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, { user_id: requestingUser._id });
            errorUtils.throwError('A claim for that skill and level is already pending approval!', 400);
        }
    }

    const existingClaimsAnyLevel = await mongooseClaimsRepo.findAll({ $and: [{ title: createClaimRequest.title }, { ownerid: requestingUser._id }] });

    for (let i = 0; i < existingClaimsAnyLevel.length; i += 1) {
        const existingClaimAnyLevel = existingClaimsAnyLevel[i];

        if (!existingClaimAnyLevel.approved && !existingClaimAnyLevel.disapproved && !existingClaimAnyLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, { user_id: requestingUser._id });
            errorUtils.throwError('A claim for that skill and level is already pending approval!', 400);
        }
        // eslint-disable-next-line
        const existingVotingRound = await mongooseVotingRoundRepo.findOne({
            $and: [
                { claim_id: existingClaimAnyLevel._id },
                { status: VotingRound.statuses.IN_PROGRESS }],
        });
        if (existingVotingRound) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_pending', createClaimRequest, { user_id: requestingUser._id });
            errorUtils.throwError('Pending claim with this title already exists!', 400);
        }
    }

    createClaimRequest.state = claims.states.NEW;
    createClaimRequest.type = claims.types.BOAST;
    createClaimRequest.visible = true;
    createClaimRequest.is_sc_claim = true;
    createClaimRequest.ownerid = requestingUser._id;
    createClaimRequest.created_at = timestampService.createTimestamp();
    createClaimRequest.endorse_count = 0;
    createClaimRequest.flag_count = 0;
    createClaimRequest.submission_event_computed = false;
    createClaimRequest.merkelized = false;
    createClaimRequest.level = "...";
    // Passing the name of the skill
    await mongooseSkillRepo.hasAIPEnabled(createClaimRequest.title, true);
    const createdClaimId = await mongooseClaimsRepo.insert(createClaimRequest);

    createClaimRequest._id = createdClaimId;
    createClaimRequest.ownerid = requestingUser._id;
    createClaimRequest.claim_id = createdClaimId;

    amplitudeTracker.publishData('claim_creation_succesful', createClaimRequest, { user_id: requestingUser._id });
    githubService.parseGithubUrlExtractClaimantData(requestingUser.github_uid, createClaimRequest.proof, createdClaimId);

    claimsEmailService.sendClaimAwaitingApproval(createdClaimId);

    slackService.reportClaim(createClaimRequest, requestingUser.img_url, requestingUser.username);
    return createdClaimId;
};

module.exports.createPartnerClaim = async function createPartnerClaim(createClaimRequest, requestingClientApp) {
    const claimRequest = {};
    claimRequest.title = createClaimRequest.skillPrettyId;
    claimRequest.desc = createClaimRequest.description;
    claimRequest.proof = createClaimRequest.proof;
    // Not setting level of the claim for clientApp created claim for now
    claimRequest.state = claims.states.NEW;
    claimRequest.type = claims.types.BOAST;
    claimRequest.visible = true;
    claimRequest.is_sc_claim = true;
    claimRequest.created_at = timestampService.createTimestamp();
    claimRequest.endorse_count = 0;
    claimRequest.flag_count = 0;
    claimRequest.submission_event_computed = false;
    claimRequest.merkelized = false;
    claimRequest.clientApp = {
        clientId: requestingClientApp.claim_id || createClaimRequest.partnerClientId,
        userId: createClaimRequest.partnerUserId,
        state: createClaimRequest.state,
    };
    claimRequest.company = requestingClientApp.company || null;
    // Passing the name of the skill
    await mongooseSkillRepo.hasAIPEnabled(claimRequest.title, true);
    const createdClaimId = await mongooseClaimsRepo.insert(claimRequest);
    claimRequest._id = createdClaimId;
    claimRequest.claim_id = createdClaimId;
    claimsEmailService.sendClaimAwaitingApproval(createdClaimId);
    return claimRequest;
};
