const mongooseVoteRepo = require('../mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const claims = require('../mongo/mongoose/schemas/claims');
const ObjectID = require('mongodb').ObjectID;
const checkIfAdminVoted = require('../../votes/adminVoteOnCommentService');

exports.getClaimFeedback = async function getClaimFeedback(claim, skillid, userId) {
    const filters = [{ claim_id: claim._id }, { voted_at: { $gt: 0 } }, { sc_vote_exists: true }];
    if (claim.type === claims.types.ASSIGNMENT && skillid) {
        filters.push({ skillId: ObjectID(skillid) });
    }

    let votes = await mongooseVoteRepo.findAll({
        $and: filters,
    });

    let explanation = [];
    let quality = 0;
    let designPatterns = 0;
    let gitFlow = 0; // Legacy param
    let completion = 0;
    let testCoverage = 0;
    let readability = 0;
    let extensibility = 0;

    for (let vote of votes) {
        if (vote.feedback) {
            let voteHiddenUsername;
            if(vote.hide.actionBy[0]){
                let user = await mongooseUserRepo.findOneById(vote.hide.actionBy[0]);
                if(user.username){
                    voteHiddenUsername = user.username;
                }else{
                    voteHiddenUsername = vote.hide.actionBy[0];
                }
            }else{
                voteHiddenUsername = '';
            }

            explanation.push({
                feedback: vote.feedback.explanation,
                endorsed: vote.endorsed,
                noOfUpvote: vote.upvote.count,
                noOfDownvote: vote.downvote.count,
                isCommentHidden: vote.hide.isHidden,
                isAdminVoteOnComment: checkIfAdminVoted.checkIfUserVoted(vote, userId),
                voteId: vote._id.toString(),
                voteHiddenUsername: voteHiddenUsername
            });
            quality += vote.feedback.quality;
            designPatterns += vote.feedback.quality;
            gitFlow += vote.feedback.gitFlow;
            completion += vote.feedback.completion;
            testCoverage += vote.feedback.testCoverage;
            readability += vote.feedback.readability;
            extensibility += vote.feedback.extensibility;
        }
    }



    return {
        quality: Math.floor(quality / votes.length),
        designPatterns: Math.floor(designPatterns / votes.length),
        completion: Math.floor(completion / votes.length),
        testCoverage: Math.floor(testCoverage / votes.length),
        readability: Math.floor(readability / votes.length),
        extensibility: Math.floor(extensibility / votes.length),
        explanation,
    };
};
