const mongooseVotingRoundRepo = require('../mongo/mongoose/mongooseVotingRoundRepo');
const { statuses: votingRoundStatus } = require('../mongo/mongoose/schemas/votingRound');
const mongooseVoteRepo = require('../mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseVoteRepo');
const getVotingResultService = require('../../votes/getVotingResultService');
const { claimStatusForPartners } = require('../mongo/mongoose/schemas/claims');

exports.getClaimStatus = async function getClaimStatus(claim) {

    let votingRound = await mongooseVotingRoundRepo.findOne({claim_id: claim._id.toString()});

    if (votingRound) {
        if (votingRound.status === 'in_progress') {
            return 'in_progress'
        } else if (votingRound.status === 'finished') {
            if (claim.final_status) {
                return 'indorsed'
            } else {
                if(typeof claim.in_consensus === 'undefined'){
                    let votingResult = await getVotingResultService.getVotingResult(claim._id.toString());
                    await mongooseClaimsRepo.update({_id: claim._id}, {in_consensus: votingResult.consensus});
                    console.log('Update Claim ID ' +  claim._id);
                    claim.in_consensus = votingResult.consensus;
                }
                if (claim.in_consensus === true) {
                    return 'rejected';
                } else {
                    return 'no_consensus';
                }
            }
        }
    } else {
        if (claim.disapproved) {
            return 'claim_disapproved'
        } else {
            return 'claim_submitted'
        }
    }
};

exports.getClaimStatusForPartnerClaim = async function getClaimStatusForPartnerClaim(claim) {
    const votingRound = await mongooseVotingRoundRepo.findOne({ claim_id: claim._id.toString() });
    if (!votingRound) {
        if (claim.disapproved) {
            return claimStatusForPartners.CLAIM_DISAPPROVED;
        }
        return claimStatusForPartners.CLAIM_SUBMITTED;
    }

    const votingResult = await getVotingResultService.getVotingResult(claim._id.toString());
    if (!votingResult.consensus && votingRound.status === votingRoundStatus.FINISHED) {
        return claimStatusForPartners.NO_CONSENSUS;
    } else if (!votingResult.consensus && votingRound.status === votingRoundStatus.IN_PROGRESS) {
        return claimStatusForPartners.IN_PROGRESS;
    }

    if (claim.final_status === true) {
        return claimStatusForPartners.INDORSED;
    }
    if (claim.final_status === false) {
        return claimStatusForPartners.FLAGGED;
    }
    return votingResult.indorsed ? claimStatusForPartners.TENTATIVELY_INDORSED : claimStatusForPartners.TENTATIVELY_FLAGGED;
};
