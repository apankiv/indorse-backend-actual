const request = require('request');
const errorUtils = require('../error/errorUtils');
const { formatResultForSendingToClientApp } = require('./claimUtils');
const mongooseClientAppRepo = require('../mongo/mongoose/mongooseClientAppRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const { attachRequestTracker } = require('../../services/tracking/requestTracking');
const { directions } = require('../../services/mongo/mongoose/schemas/requestLog');

const _service = {};

_service.sendClaimResultToPartner = async function sendClaimResultToPartner(claimId) {
    if (!claimId) return;
    const claim = await mongooseClaimsRepo.findOneById(claimId);

    const { clientApp: clientAppClaimData } = claim;
    if (!clientAppClaimData) return;

    const claimData = await formatResultForSendingToClientApp(claim);
    if (!claimData) return;
    const dataToSend = {
        userId: clientAppClaimData.userId,
        claim: claimData,
    };
    console.log(`Data to send for the claim: (id: ${claimId}) clientApp (clientId: ${(clientAppClaimData && clientAppClaimData.clientId) || 'Client Not Found'}) :`, dataToSend);
    // get clientApp details
    const clientApp = await mongooseClientAppRepo.findOneByClientId(clientAppClaimData.clientId);
    const { sendPartnerClaimResult: sendPartnerClaimResultHook } = (clientApp && clientApp.hooks) || {};
    if (!sendPartnerClaimResultHook || !sendPartnerClaimResultHook.url) {
        errorUtils.throwError(
            `sendPartnerClaimResult hook not configured for clientApp ${
                clientApp.display_name
            } (id: ${clientApp._id.toString()})`,
            404,
        );
    }
    return await new Promise((resolve, reject) => {
        const requestOptions = {
            url: sendPartnerClaimResultHook.url,
            method: sendPartnerClaimResultHook.method,
            json: true,
        };
        requestOptions.body = dataToSend;
        request(requestOptions, (error, response, body) => {
            attachRequestTracker(requestOptions, body, { direction: directions.OUTGOING, meta: { error, statusCode: response.statusCode } })
                .catch(errorInTracker => { 
                    console.log(errorInTracker);
                    console.log("Ignoring this error");
                });

            if (error) {
                reject(error);
            } else if (response.statusCode === 200) {
                resolve(true);
            } else {
                reject(new Error('No response'));
            }
        });
    });
};

module.exports = _service;
