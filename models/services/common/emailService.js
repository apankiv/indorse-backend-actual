const settings = require('../../settings.js');
const config = require('config');
const mongooseMandrillConfig = require('../mongo/mongoose/mongooseMandrillConfig');
const mandrill = require('mandrill-api/mandrill');
const logger = require('./../common/logger').getLogger();
const stringUtils = require('./../blob/stringUtils');
const request = require('request');

function isEmailEnabled() {
    if (settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'integration'){
        return true;
    }else {
        return false;
    }
}


exports.isEmailEnabled = isEmailEnabled;

async function getTemplateAndMessage(action,email,name){
    let mandrillTemplate = await mongooseMandrillConfig.findOneByAction(action);

    if(!mandrillTemplate){
        logger.error("Template not found for action " + action );
        throw new Error('Unable to find template for action : ' + action);
    }

    let toValue = {};
    toValue.email = email;
    if(name){
        toValue.name  = name;
    }

    let to = [];
    to.push(toValue);

    let msg = {};

    if(mandrillTemplate.bcc_email){
        msg.bcc_address = mandrillTemplate.bcc_email;
    }

    msg.subject = mandrillTemplate.subject;
    msg.from_email = mandrillTemplate.from_email;
    msg.from_name = mandrillTemplate.from_name;
    msg.merge_language = "mailchimp";
    msg.to = to;
    return {templateName : mandrillTemplate.template_name, message: msg}
}

exports.getTemplateAndMessage = getTemplateAndMessage;

async function sendEmail(templateName,message,action) {
    console.log("Calling sendEmail : ");
    console.log("template Name : " + JSON.stringify(templateName));
    console.log("Message : "+ JSON.stringify(message));
    let templateContent = [];
    let async = true;
    let ip_pool = "Main Pool";
    let mandrill_client = new mandrill.Mandrill(settings.MANDRILL_API_KEY, debug=true);

    mandrill_client.messages.sendTemplate({"template_name": templateName, "template_content": templateContent, "message": message, "async": async, "ip_pool": ip_pool}, function(result) {
        if(result[0].status!=='sent'){
            logger.error('Status is not sent for email to : \nEmail: ' + JSON.stringify(message.to) + "\nAction : " + action +
            '\nMandrill ID : ' + result[0]._id + 'Reason : ' + result[0].reject_reason);
        }
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        logger.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        logger.error('\nUnable to send email to \nEmail: ' + JSON.stringify(message.to) + "\nAction : " + action);

    });

}

exports.sendSuperAdminPayoutCSV = async function sendSuperAdminPayoutCSV(csv, indCsv) {

    if (!isEmailEnabled())
        return;

    let action = 'VALIDATOR_PAYOUT';

    let mandrillTemplate = await mongooseMandrillConfig.findOneByAction(action);

    if (!mandrillTemplate) {
        logger.error("Template not found for action " + action);
        throw new Error('Unable to find template for action : ' + action);
    }
    let subscribers = mandrillTemplate.subscriberEmails;

    for (var i = 0; i < subscribers.length; i++) {
        let data = await getTemplateAndMessage(action, subscribers[i], 'superAdmin');
        data.message.attachments = [{
            type: "text/csv",
            name: "validator.csv",
            content: csv
        },
            {
                type: "text/csv",
                name: "ind.csv",
                content: indCsv
            }];
        await sendEmail(data.templateName, data.message, action);
    }
};

exports.sendEmail = sendEmail;



exports.sendUserDataRequestEmail = async function sendUserDataRequestEmail(username,userid,user_email,useraction) {
    if (!isEmailEnabled())
        return;

    let action = 'USER_DATA_REQUEST';


    var emailsToSend = settings.USER_DELETION_NOTIFICATION.split(',');


    for (let admin_email of emailsToSend){
        let data = await getTemplateAndMessage(action, admin_email.replace(/'/g, ""));

        if (settings.ENVIRONMENT !== 'production') {
            data.message.subject = "TEST ENV " + data.message.subject
        }

        data.message.global_merge_vars = [
            {
                "name": "INDORSE_USERNAME",
                "content": username
            },
            {
                "name": "INDORSE_USER_ID",
                "content": userid
            },
            {
                "name": "INDORSE_USER_EMAIL",
                "content": user_email
            },
            {
                "name": "USER_DATA_ACTION",
                "content": useraction
            }
        ];
        await sendEmail(data.templateName, data.message, action);
    }
};




//Verified
exports.sendWelcomeEmail = async function sendWelcomeEmail(name, email, username) {
    if(!isEmailEnabled())
        return;
    let action = 'WELCOME_EMAIL';
    let data  = await getTemplateAndMessage(action,email,name);
    let username_link = "https://indorse.io/" + username;
    let ref_link  = '<a href="' + username_link + '">' + username_link + '</a>';

    data.message.global_merge_vars = [
            {
                "name": "INDORSE_FIRSTNAME",
                "content": name
            },
            {
                "name" : "INDORSE_USERNAME",
                "content" : ref_link
            }
        ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendVerificationEmail = async function sendVerificationEmail(name, email, verifyToken, source) {
    if(!isEmailEnabled())
        return;

    let action = 'VERIFICATION_LINK_EMAIL';
    let data  = await getTemplateAndMessage(action,email,name);
    //TODO : Check if this needs to be based on server & https is needed?
    let verify_link_text =  config.get('server.hostname') + "verify-email?email=" + email + "&token=" + verifyToken;

    if(source) {
        verify_link_text += "&source=" + source;
    }

    let verify_link_href  = '<a href="' + verify_link_text + '">' + "Verification link" + '</a>';

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_VERIFICATION_LINK",
            "content" : verify_link_href
        },
        {
            "name": "INDORSE_VERIFICATION_LINK_TEXT",
            "content" : verify_link_text
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendPasswordResetEmail = async function sendVerificationEmail(name, email, passVerifyToken) {
    if(!isEmailEnabled())
        return;

    let action = 'PASSWORD_RESET_EMAIL';
    let data  = await getTemplateAndMessage(action,email,name);
    let passwd_link = config.get('server.hostname') + "password/reset?email=" + email + "&pass_token=" + passVerifyToken;
    let passwd_link_href  = '<a href="' + passwd_link + '">' + "Password reset link" + '</a>';

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "PASSWORD_RESET_LINK",
            "content" : passwd_link_href
        },
        {
            "name": "INDORSE_PASSWORD_RESET_TEXT",
            "content": passwd_link
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

exports.sendUserApprovedEmail = async function sendUserApprovedEmail(name, email) {
    if(!isEmailEnabled())
        return;


    let data  = await getTemplateAndMessage('USER_APPROVED_EMAIL',email,name);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        }
    ];
    await sendEmail(data.templateName,data.message);
};

//Verified
exports.sendAdvisorInvitationEmail = async function sendAdvisorInvitationEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;
    //TODO : Check server.hostname or use indorse.io
    let data  = await getTemplateAndMessage('INVITE_ADVISOR_IN_INDORSE',email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message);
};
//Verified
exports.sendNonExistentAdvisorInvitationEmail = async function sendNonExistentAdvisorInvitationEmail(companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let data  = await getTemplateAndMessage('INVITE_ADVISOR_NOT_IN_INDORSE',email);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message);
};

///Verified
exports.sendAdvisorRemovedByUserInfoToCompany = async function sendAdvisorRemovedByUserInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let data  = await getTemplateAndMessage('ADVISOR_REVOKED_NOTIFY_TO_ADMIN',email);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }

    ];
    await sendEmail(data.templateName,data.message);
};
//Verified
exports.sendAdvisorRemovedByAdminInfoToCompany = async function sendAdvisorRemovedByAdminInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REMOVED_BY_ADMIN_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }

    ];
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendAdvisorRemovedInfoToAdvisor = async function sendAdvisorRemovedInfoToAdvisor(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REMOVED_BY_ADMIN_NOTIFY_ADVISOR';
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};
//verified
exports.sendYouRemovedInfoToAdvisor = async function sendYouRemovedInfoToAdvisor(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REVOKED_NOTIFY_ADVISOR';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_COMPANY_NAME",
            "content" : company_link_href
        }

    ];
    await sendEmail(data.templateName,data.message,action);
};
//Verified
exports.sendAdvisorAdvisorAcceptedInvitationToCompanyEmail = async function sendAdvisorAdvisorAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_ACCEPTED_REQ_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

exports.sendAdvisorAdvisorApprovedEmail = async function sendAdvisorAdvisorApprovedEmail(name, companyName, companyPrettyId, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_APPROVED_REQ_NOTIFY_ADVISOR';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href = '<a href="' + company_link + '">' + companyName + '</a>';

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

exports.sendAdvisorAdvisorDisapprovedEmail = async function sendAdvisorAdvisorDisapprovedEmail(name, companyName, companyPrettyId, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_DISAPPROVED_REQ_NOTIFY_ADVISOR';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href = '<a href="' + company_link + '">' + companyName + '</a>';

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendYouConfirmedAdvisorConnectionInfoToUserEmail = async function sendYouConfirmedAdvisorConnectionInfoToUserEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_ACCEPTED_REQ_NOTIFY_ADVISOR';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendAdvisorAdvisorRejectedInvitationToCompanyEmail = async function sendAdvisorAdvisorRejectedInvitationToCompanyEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REJECTED_REQ_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendAdvisorAdvisorRejectedInvitationToUserEmail = async function sendAdvisorAdvisorRejectedInvitationToCompanyEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REJECTED_REQ_NOTIFY_ADVISOR';
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

//verified
exports.sendTeamMemberInvitationEmail = async function sendTeamMemberInvitationEmail(name, companyName, email, companyPrettyId, designation) {
    if(!isEmailEnabled())
        return;

    let action = 'INVITE_TEAM_MEMBER_IN_INDORSE';
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';

    let designationAndArticle = getDesignationWithArticle(designation);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_COMPANY",
            "content" : company_link_href
        },
        {
            "name" : "INDORSE_DESIGNATION",
            "content" : designationAndArticle
        }

    ];
    await sendEmail(data.templateName, data.message, action);
};
//verified
exports.sendNonExistentTeamMemberInvitationEmail = async function sendNonExistentTeamMemberInvitationEmail(companyName, email, companyPrettyId, designation) {
    if(!isEmailEnabled())
        return;


    let action = 'INVITE_TEAM_MEMBER_NOT_IN_INDORSE';
    let data  = await getTemplateAndMessage(action,email);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';

    let designationAndArticle = getDesignationWithArticle(designation);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_DESIGNATION",
            "content": designationAndArticle
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

function getDesignationWithArticle(designation) {
    let article;
    let firstLetter = designation.toLowerCase().charAt(0);
    if (firstLetter === 'a' ||
        firstLetter === 'e' ||
        firstLetter === 'i' ||
        firstLetter === 'o' ||
        firstLetter === 'u') {
        article = 'an';
    } else {
        article = 'a';
    }
    return article + ' ' + designation;
}
//Verified
exports.sendTeamMemberRemovedByUserInfoToCompany = async function sendTeamMemberRemovedByUserInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REVOKED_NOTIFY_TO_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};
//verified
exports.sendTeamMemberRemovedByAdminInfoToCompany = async function sendTeamMemberRemovedByAdminInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;


    let action = 'TEAM_MEMBER_REMOVED_BY_ADMIN_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};
//verified
exports.sendTeamMemberRemovedInfoToTeamMember = async function sendTeamMemberRemovedInfoToTeamMember(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REMOVED_BY_ADMIN_NOTIFY_TEAM_MEMBER';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberAcceptedInvitationToCompanyEmail = async function sendTeamMemberTeamMemberAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_ACCEPTED_REQ_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberConfirmedConnectionInfoToUserEmail = async function sendTeamMemberTeamMemberAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_ACCEPTED_REQ_NOTIFY_TEAM_MEMBER';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);

};

//verified
exports.sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail = async function sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REJECTED_REQ_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberRejectedInvitationToUserEmail = async function sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEM_REJECTED_REQ_NOTIFY_TEAM_MEMBER';
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>';


    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};


exports.sendEtherCheckEmail = async function sendEtherCheckEmail(address, environment) {
    if(!isEmailEnabled())
        return;

    let action = 'LOW_ETH_BALANCE';
    let data  = await getTemplateAndMessage(action,settings.ETHEREUM_TECH_SUPPORT_EMAIL,"Admin");
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADDRESS",
            "content": address
        },
        {
            "name": "INDORSE_CURRENCY",
            "content":"Ether"
        },
        {
            "name": "INDORSE_ENV",
            "content": settings.ENVIRONMENT
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};


exports.sendINDCheckEmail = async function sendINDCheckEmail(address, environment) {
    if(!isEmailEnabled())
        return;

    let action ='LOW_IND_BALANCE';
    let data  = await getTemplateAndMessage(action,settings.ETHEREUM_TECH_SUPPORT_EMAIL,"Admin");
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADDRESS",
            "content": address
        },
        {
            "name": "INDORSE_CURRENCY",
            "content":"IND"
        },
        {
            "name": "INDORSE_ENV",
            "content": settings.ENVIRONMENT
        }
    ];
    await sendEmail(data.templateName,data.message,action);
};
                                                //softAdvisorInvitationEmail(company.company_name, pretty_id, advisorID, userName, inviteAdvisorRequest.email)
exports.softAdvisorInvitationEmail = async function softAdvisorInvitationEmail(companyName, companyPrettyId, advisorId,userName,email) {
    if (!isEmailEnabled())
        return;

    let action = 'SOFT_INVITE_ADVISOR';
    if (!userName) {
        userName = "User";
    }

    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "companies/" + companyPrettyId + "/advisors/" + advisorId;

    if(!userName){
        userName = "User";
    }

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_COMPANY_ADVISOR_SOFT_SHOW_URL",
            "content": company_link
        },
        {
            "name": "INDORSE_COMPANY_ADVISOR_SOFT_REJECT_LINK",
            "content": '<a href="' + company_link + '">here</a>'
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};


exports.softAdvisorAcceptNotifyAdmin = async function softAdvisorAcceptNotifyAdmin(userName,companyName, companyPrettyId,email) {
    if (!isEmailEnabled())
        return;

    let action = 'SOFT_ACCEPT_ADVISOR_NOTIFY_ADMIN';
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>';

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

//TODO : Refactor
exports.softAdvisorValidatedNotifyAdmin = async function softAdvisorValidatedNotifyAdmin(userName, companyName, companyPrettyId,
    profileUrl, email,status) {

    if (!isEmailEnabled())
        return;

    let action = 'SOFT_VALIDATE_ADVISOR_NOTIFY_ADMIN';
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>';
    let profile_link_href = '<a href="' + profileUrl + '">' + 'here' + '</a>';
    let indorse_flag = "rejected";

    if(status === "ACCEPTED"){
        indorse_flag = "accepted";
    }

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "LINKED_IN_PROFILE",
            "content": profile_link_href
        },
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        },
        {
            "name": "INDORSE_ACCEPT_FLAG",
            "content": indorse_flag
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

//TODO : Refactor
exports.softAdvisorRejectNotifyAdmin = async function softAdvisorRejectNotifyAdmin(userName, companyName, companyPrettyId, email) {
    if (!isEmailEnabled())
        return;


    let action = 'SOFT_REJECT_ADVISOR_NOTIFY_ADMIN';
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>';

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.notifyJobApplication = async function notifyJobApplication(name,indorseUserName,userEmail,jobTitle,jobLocation, email) {
    if (!isEmailEnabled())
        return;

    let action = 'NEW_JOB_APPLICATION_NOTIFY_COMPANY';
    let data = await getTemplateAndMessage(action, email);

    let username_link = config.get('server.hostname') + indorseUserName;

    data.message.subject = name + " applied for your " + jobTitle + "(" + jobLocation + ")";

    data.message.headers ={};
    data.message.headers["Reply-To"] = userEmail;


    data.message.global_merge_vars = [
        {
            "name": "INDORSE_JOB_TITLE",
            "content": jobTitle
        },
        {
            "name": "INDORSE_USERNAME",
            "content": name
        },
        {
            "name": "INDORSE_USER_PROFILE",
            "content": username_link
        },
        {
            "name": "INDORSE_JOB_LOCATION",
            "content": jobLocation
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};



exports.notifyJobReccomendation = async function notifyJobReccomendation(name, candidateName, candidateEmail,
                                                jobDesignation, jobLocation, companyName, campaignTitle, campaignDesc, jobId ) {
    if (!isEmailEnabled())
        return;

    let action = 'NEW_JOB_RECOMMENDATION_NOTIFY_USER';
    let data = await getTemplateAndMessage(action, candidateEmail);

    let job_slug = stringUtils.truncate(stringUtils.generateSlug([jobDesignation, jobLocation], { lower: true }), 100, '');
    let utm_content = stringUtils.truncate(stringUtils.generateSlug([jobDesignation], { lower: true }), 100, '');
    let href_job_link = config.get('server.hostname') + "jobs/" + job_slug + "/" + jobId + "?utm_source=email&utm_campaign=gig-recruitment&utm_content=" + utm_content;

    data.message.subject = name + " recommends you to join " + companyName + " as " +  jobDesignation;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_RECOMMENDED_USER",
            "content": candidateName
        },
        {
            "name": "INDORSE_USERNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": companyName
        },
        {
            "name": "INDORSE_JOB_DESIG",
            "content": jobDesignation
        },
        {
            "name": "INDORSE_JOB_LINK",
            "content": href_job_link
        },
        {
            "name": "INDORSE_CAMPAIGN_TITLE",
            "content": campaignTitle
        },
        {
            "name": "INDORSE_CAMPAIGN_DESCRIPTION",
            "content": campaignDesc
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendAssignmentReminderToUser = async function sendAssignmentReminderToUser(user, assignmentId) {
  // const { name, email } = user;
  // console.log(`sending email to ${name} at ${email}`)
  if(!isEmailEnabled())
    return;

  const { name, email } = user;
  let action = 'NOTIFY_USER_TO_FINISH_ASSIGNMENT';
  let data  = await getTemplateAndMessage(action, email, name);
  
  let assignment_link = `${config.get('server.hostname')}assignments/${assignmentId}`;
  // let assignment_link_href  = `<a href="${assignment_link}" >${assignment_link}</a>`;

  data.message.global_merge_vars = [
    {
      "name": "INDORSE_FIRSTNAME",
      "content": name
    },
    {
      "name": "INDORSE_RESUME_LINK",
      "content": assignment_link
    }
  ];
  console.log('[sendAssignmentReminderToUser]: email sent to-', name);
  await sendEmail(data.templateName, data.message, action );
};

exports.sendInviteToCompanyACL = async function sendInviteToCompanyACL(email, inviteToken, companyName, prettyId) {
    if (!isEmailEnabled())
        return;
    const action = 'INVITE_USER_FOR_SME_ACCESS';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const inviteLink =  `${hostname}company/${inviteToken}`;
    // const invite_href = `<a href="${inviteLink}"> this </a>`;
    const companyLink =  `${hostname}c/${prettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    data.message.subject = `You are invited to manage ${companyName} company on Indorse`;
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_INVITE_LINK",
            "content": inviteLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendClientAppInvite = async function sendClientAppInvite(email, inviteToken, displayName, clientId) {
    if (!isEmailEnabled())
        return;
    const action = 'INVITE_PARTNER_FOR_CLIENT_APP_ACCESS';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const inviteLink =  `${hostname}partner/accept-invite?invite_token=${inviteToken}&display_name=${displayName}&client_id=${clientId}`;
    data.message.subject = `Configure your Indorse Evaluation Service`;
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_INVITE_LINK",
            "content": inviteLink,
        },
        {
            "name": "DISPLAY_NAME",
            "content": displayName,
        },
        {
            "name": "CLIENT_ID",
            "content": clientId,
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};
exports.sendAssignmentAllowedForMagicLinkEmail = async function sendAssignmentAllowedForMagicLinkEmail(email, name, companyName, magicLinkPublicId) {
    if(!isEmailEnabled())
        return;
    let action = 'MAGICLINK_ASSIGNEMNT_ALLOWED';
    let data  = await getTemplateAndMessage(action,email,name);
    const hostname = config.get('server.hostname');
    const magicLinkUrl =  `${hostname}magic/${magicLinkPublicId}`;


    data.message.global_merge_vars = [
        {
            "name" : "MAGIC_LINK",
            "content" : magicLinkUrl
        },{
            "name" : "COMPANY_NAME",
            "content" : companyName
        }
    ];
    await sendEmail(data.templateName,data.message,action);
}

exports.sendAutoAllowedAssignmentForMagicLinkEmail = async function sendAutoAllowedAssignmentForMagicLinkEmail(email, name, companyName, magicLinkPublicId, magicLinkTitle) {
    if(!isEmailEnabled())
        return;
    let action = 'MAGICLINK_ASSIGNMENT_AUTO_ALLOWED';
    let data  = await getTemplateAndMessage(action,email,name);
    const hostname = config.get('server.hostname');
    const magicLinkUrl =  `${hostname}magic/${magicLinkPublicId}`;


    data.message.global_merge_vars = [
        {
            "name" : "MAGIC_LINK",
            "content" : magicLinkUrl
        },
        {
            "name" : "COMPANY_NAME",
            "content" : companyName
        },
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle
        },
    ];
    await sendEmail(data.templateName, data.message, action);
}

exports.sendAdminCompanyBalanceLow = async function sendAdminCompanyBalanceLow(companyName, balance) {
    if(!isEmailEnabled())
        return;

    let action = 'COMPANY_MAGICLINK_ASSIGNMENT_BALANCE_LOW';

    let mandrillTemplate = await mongooseMandrillConfig.findOneByAction(action);

    if(!mandrillTemplate){
        logger.error("Template not found for action " + action );
        throw new Error('Unable to find template for action : ' + action);
    }
    let subscribers = mandrillTemplate.subscriberEmails;


    for(var i = 0 ; i < subscribers.length ; i ++){
        let data  = await getTemplateAndMessage(action,subscribers[i],'Indorse Admins');
        data.message.global_merge_vars = [
            {
                "name" : "BALANCE",
                "content" : balance
            },{
                "name" : "COMPANY_NAME",
                "content" : companyName
            }
        ];
        await sendEmail(data.templateName,data.message,action);
    }
}

exports.inviteCandidateForMagicLink = async function inviteCandidateForMagicLink({ email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle }) {
    if (!isEmailEnabled())
        return;
    const action = 'INVITE_CANDIDATE_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}magic/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    data.message.subject = `${companyName} is interested in hiring you`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": magicLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendAdminValidatorWarning = async function sendAdminValidatorWarning(username, noOfBadComments, email, commentArr) {
    if(!isEmailEnabled())
        return;

    let action = 'VALIDATOR_WARNING';

    let mandrillTemplate = await mongooseMandrillConfig.findOneByAction(action);

    if(!mandrillTemplate){
        logger.error("Template not found for action " + action );
        throw new Error('Unable to find template for action : ' + action);
    }
    let subscribers = mandrillTemplate.subscriberEmails;
    let commentPrettyPrint = 'no comment found';
    if(commentArr){
        let comments = [];
        for(let i = 0 ; i < commentArr.length ; i ++){
            let line;
            let commentObj = commentArr[i];
            let claimId = commentObj.claimId || 'not found';
            let explanation = commentObj.comment || '';
            let votedAt = commentObj.votedAt || 0;
            line = 'claim_id: ' + claimId +' comment: ' + explanation + ' voted at ' + new Date((votedAt*1000)).toISOString().split('T')[0];
            comments.push(line);
        }
        commentPrettyPrint = comments.join('<br/>');
    }

    for(var i = 0 ; i < subscribers.length ; i ++){
        let data  = await getTemplateAndMessage(action,subscribers[i],'Indorse Admins');
        data.message.global_merge_vars = [
            {
                "name" : "VALIDATOR_USERNAME",
                "content" : username
            },
            {
                "name" : "NUMBER_OF_BAD_COMMENTS",
                "content" : noOfBadComments
            },
            {
                "name" : "VALIDATOR_EMAIL",
                "content" : email
            },
            {
                "name" : "COMMENTS",
                "content" : commentPrettyPrint
            }
        ];
        await sendEmail(data.templateName,data.message,action);
    }
}
exports.chatbotsPassedCompanyUpdate = async function chatbotsPassedCompanyUpdate(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName }
) {
    if (!isEmailEnabled())
        return;
    const action = 'CHATBOT_PASSED_BY_CANDIDATE_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}magic/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    data.message.subject = `${magicLinkTitle} - A candidate has cleared the quizbot stage for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": magicLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.assignmentViewedCompanyUpdate = async function assignmentViewedCompanyUpdate(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName }
) {
    if (!isEmailEnabled())
        return;
    const action = 'ASSIGNMENT_VIEWED_BY_CANDIDATE_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}magic/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    data.message.subject = `${magicLinkTitle} - A candidate has started an assignment for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": magicLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.assignmentEvaluatedCompanyUpdate = async function assignmentEvaluatedCompanyUpdate(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName, claimId }
) {
    if (!isEmailEnabled())
        return;
    const action = 'ASSIGNMENT_EVALUATED_OF_CANDIDATE_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}companies/${companyPrettyId}/edit/candidates/view/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    const claimURL = `${hostname}claims/${(claimId || '').toString()}`;
    data.message.subject = `${magicLinkTitle} - A candidate has completed an assignment for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": claimURL
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.claimEvaluatedCompanyUpdate = async function claimEvaluatedCompanyUpdate(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName, claimId }
) {
    if (!isEmailEnabled())
        return;
    const action = 'CLAIM_EVALUATED_OF_CANDIDATE_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}companies/${companyPrettyId}/edit/candidates/view/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    const claimURL = `${hostname}claims/${(claimId || '').toString()}`;
    data.message.subject = `${magicLinkTitle} - A candidate has completed an assignment for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": claimURL
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendReminderToViewMagicLinkAssignment = async function sendReminderToViewMagicLinkAssignment(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName }
) {
    if (!isEmailEnabled())
        return;
    const action = 'REMINDER_TO_VIEW_ASSG_LIST_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}magic/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    const buttonLink =  `${hostname}magic/${magicLinkPublicId}`;
    data.message.subject = `${magicLinkTitle} - Please submit an assignment to apply for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": magicLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        },
        {
            "name": "BUTTON_LINK",
            "content": buttonLink,
        },
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendReminderToSolveMagicLinkAssignment = async function sendReminderToSolveMagicLinkAssignment(
    { email, companyName, companyPrettyId, magicLinkPublicId, magicLinkTitle, candidateName, assignments }
) {
    const multiple = assignments && assignments.length > 1 ? true : false;
    if (!isEmailEnabled())
        return;
    const action = multiple ? 'REMINDER_TO_SUBMIT_ONE_OF_MANY_ASSG_FOR_MAGIC_LINK': 'REMINDER_TO_SUBMIT_ONE_OF_ONE_ASSG_FOR_MAGIC_LINK';
    const data = await getTemplateAndMessage(action, email);
    const hostname = config.get('server.hostname');
    const magicLink =  `${hostname}magic/${magicLinkPublicId}`;
    const companyLink =  `${hostname}c/${companyPrettyId}`;
    const company_href = `<a href="${companyLink}">${companyName}</a>`;
    let buttonLink =  `${hostname}magic/${magicLinkPublicId}`;
    const assignmentTags = assignments.map(assignment => {
        const assignmentLink = `${buttonLink}?assignmentId=${assignment._id}`;
        if (!multiple) buttonLink = assignmentLink;
        return `<a href="${assignmentLink}">${assignment.title}</a>`;
    });
    data.message.subject = `${magicLinkTitle} - Please submit an assignment to apply for ${companyName}`;
    data.message.global_merge_vars = [
        {
            "name": "MAGIC_LINK_TITLE",
            "content": magicLinkTitle,
        },
        {
            "name": "MAGIC_LINK",
            "content": magicLink
        },
        {
            "name": "COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "COMPANY_LINK",
            "content": company_href
        },
        {
            "name": "CANDIDATE_NAME",
            "content": candidateName,
        },
        {
            "name": "BUTTON_LINK",
            "content": buttonLink,
        },
        {
            "name": "ASSIGNMENT_LIST",
            "content": assignmentTags.join('<br/>'),
        },
    ];
    await sendEmail(data.templateName, data.message, action);
};

const downloadFileToBase64 = (url) => {
    return new Promise((resolve, reject) => {
        const optionsStart = {
            uri: url,
            method: "GET",
            encoding: null,
            headers: {
                "Content-type": "application/pdf"
            }
        };
        request(optionsStart, (error, response, body) => {
            if (error) {
                reject(error);
            } else if (response.statusCode === 200) {
                resolve(Buffer.from(body).toString("base64"));
            } else {
                errorUtils.throwError('No response', 500);
            }
        });
    });
}

exports.sendCaseStudyEmail = async function sendCaseStudyEmail(email, name) {
    if (!isEmailEnabled())
        return;
    const action = 'CASE_STUDY_EMAIL';
    const data = await getTemplateAndMessage(action, email);
    const pdfLink = settings.DEFAULT_CASE_STUDY_URL;
    if (!pdfLink)
        return;
    const caseStudy = await downloadFileToBase64(pdfLink);
    data.message.subject = `Indorse case study`;
    data.message.global_merge_vars = [
        {
            "name": "NAME",
            "content": name,
        },
        {
            "name": "PDF_LINK",
            "content": pdfLink,
        }
    ];
    data.message.attachments = [
        {
            type: "application/pdf",
            name: "CaseStudy.pdf",
            content: caseStudy
        },
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendClaimResubmissionReminderToUser = async function sendClaimResubmissionReminderToUser(user) {
    if(!isEmailEnabled())
        return;

    const { name, email } = user;
    let action = 'CLAIM_REJECTED_REMINDER';
    let data  = await getTemplateAndMessage(action, email, name);

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        }
    ];
    await sendEmail(data.templateName, data.message, action);
};

exports.sendCandidateWillingToPayEmail = async function sendCandidateWillingToPayEmail(candidateName, claimId)  {
    if(!isEmailEnabled())
        return;
    const hostname = config.get('server.hostname');
    const claimURL = `${hostname}claims/${(claimId || '').toString()}`;
    let action = 'CANDIDATE_WILLING_TO_PAY';
    const toEmailList = ['avad@indorse.io', 'gaurang@indorse.io', 'harsh@indorse.io'];
    // eslint-disable-next-line
    for (let i = 0; i < toEmailList.length; i++) {
        const email = toEmailList[i];
        let data  = await getTemplateAndMessage(action, email);
        data.message.global_merge_vars = [
            {
                "name": "CANDIDATE_NAME",
                "content": candidateName,
            },
            {
                "name": "CLAIM_LINK",
                "content": claimURL,
            },
        ];
        await sendEmail(data.templateName, data.message, action);    
    }
};
