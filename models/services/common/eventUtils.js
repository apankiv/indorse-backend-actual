const mongooseClaimEventsRepo = require('../../services/mongo/mongoose/mongooseClaimEvents');
const mongooseValidatorRepo = require('../../services/mongo/mongoose/mongooseValidatorRepo');
const mongooseRewardsRepo = require('../../services/mongo/mongoose/mongooseRewardsRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../services/mongo/mongoose/mongooseVoteRepo');
const settings = require('../../../models/settings')
const timestampService = require('../../../models/services/common/timestampService')
const indorseTokenContract = require('../../../smart-contracts/services/indorseToken/indorseTokenContract')

const Web3 = require('web3');


module.exports.isEventRecorded =  async function isEventRecorded(claimEvent){
    let event =await mongooseClaimEventsRepo.findOne({$and: 
        [{ user_id: claimEvent.user_id }, { claim_id: claimEvent.claim_id }, { voting_round_id: claimEvent.voting_round_id }, { event: claimEvent.event} ]
    })

    if(event){
        return true
    }
    return false;
}

async function calculateValidatorReputation(lookbackTimestamp, currentTimestamp){
    let allValidators = await mongooseValidatorRepo.findAll({});
 
     
    //Recompute only for valid set
    for (validator of allValidators){ 
        let user = await mongooseUserRepo.findOneById(validator.user_id);
        if(!user || !user.ethaddress)
            continue;
        
        let balance = await indorseTokenContract.balanceOf(user.ethaddress);

        //if(balance.toNumber() >= 0  && (settings.MIN_IND_BALANCE <= balance.toNumber())){ 

        //Find all votes
        //$gt : 14 Jan 2019 when the upvote/downvote functionality was added to production. So consider only votes which 
        //fall in this period
        let votes = await mongooseVoteRepo.findAll({ sc_vote_exists: true, voted_at: { $gt: 1547424000 }, voter_id: validator.user_id});
        let totalVotes = votes.length;
        let totalDownVotes = 0;
        let totalDownVoteScore =0;
        let totalUpVotes = 0;
        let totalUpVoteScore = 0;
        let totalHides = 0;      
        let totalHideVoteScore = 0; 

        for(vote of votes){
            let score = vote.upvote.count - vote.downvote.count;
            if (score < 0){
                totalDownVotes +=1
            } else if(score > 0){
                totalUpVotes +=1
            }

            if (vote.hide.isHidden){
                totalHides +=2                
            }
        }

        if (totalVotes > 0){
            totalDownVoteScore = (totalDownVotes / totalVotes) * 100;
            totalUpVoteScore = (totalUpVotes / totalVotes) * 100;
            totalHideVoteScore = (totalHides / totalVotes) * 100;
        }

       
        let totalVoteScore = 0;
        let consensusResult = await mongooseClaimEventsRepo.findAll({ user_id: validator.user_id, event: 'vote_in_consensus'});
        let nonConsensusResult = await mongooseClaimEventsRepo.findAll({ user_id: validator.user_id, event: 'vote_not_in_consensus'});
        let consensusCount = consensusResult.length;
        let nonConsensusCount = nonConsensusResult.length;
        if (consensusCount + nonConsensusCount > settings.MIN_NUMBER_VOTES) {
            let reputation = consensusCount / (consensusCount + nonConsensusCount) * 100;
            //Update reputation for validator
            totalVoteScore = reputation
        }


        let totalScore = (totalVoteScore - totalHideVoteScore - totalDownVoteScore + totalUpVoteScore) /2

        let reputationUpdate = {
            params : {
                totalUpVotes: totalUpVotes,
                totalDownVotes: totalDownVotes,
                totalVotes: totalVotes,
                totalHides: totalHides
            },
            score_params : {
                vote_score: totalVoteScore.toFixed(2),
                hide_score: totalHideVoteScore.toFixed(2),
                downvote_score: totalDownVoteScore.toFixed(2),
                upvote_score : totalUpVoteScore.toFixed(2)                
            },
            score: totalScore.toFixed(2),
            end_timestamp: currentTimestamp
        }        
        await mongooseValidatorRepo.update({ user_id: validator.user_id }, { $push: { reputation_data: reputationUpdate } });        
    }
}

module.exports.geneateReputationRewards =async function geneateReputationRewards(){
    let currentTimestamp = await timestampService.createTimestamp();

    //Compute validator reputation
    await calculateValidatorReputation(currentTimestamp)
    /*
    let validators = await mongooseValidatorRepo.getTopValidatorsByReputation(2);    
    if(validators.length >= 2){
        await rewardValidator(validators[0], 20, currentTimestamp)
        await rewardValidator(validators[1], 10, currentTimestamp)
    }
    */
}

async function rewardValidator(validator,reward,currentTimestamp){
    //Only reward if score is greater than threshold    
    if (validator && validator.reputation && validator.reputation.score > settings.MIN_REPUATION_SCORE){
        console.log("Rewarding " + validator)
        let rewardData = {
            user_id: validator.user_id,
            reward: reward,
            calculation_timestamp: currentTimestamp,
            properties:{
                reputation :validator.reputation.score
            },
            paid : false
        }
        await mongooseRewardsRepo.insert(rewardData );
    }
}