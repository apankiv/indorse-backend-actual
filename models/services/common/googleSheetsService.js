let GoogleSpreadsheet = require('google-spreadsheet');
const async = require('async');
const config = require('config');
const settings = require('../../settings')
const spreadSheetLink = config.get('goolgesheet.sheetLink');
const logger = require("../common/logger").getLogger();
const slackService = require("./slackService");
let doc = new GoogleSpreadsheet(spreadSheetLink);
let request = require('request');
let sheet;

/*
    Called from gatherClaimStatistic with input -> write.
 */
exports.writeDailyReport =  async function writeDailyReport (report, reportType) {
    if (settings.ENVIRONMENT !== 'test'){
        try{
            let cred = await getPrivateKey();
            let credJson = JSON.parse(cred);
            await setAuth(credJson);
            await getInfoAndWorksheets(reportType); // Instantiate different report by reportType, possible value) 'D', 'W'
            await writeNewRow(report);
        }catch(error){
            logger.debug("Error while reporting to Google Sheets :" + error)
            slackService.reportCronBug(error);
        }
    }

}

async function getPrivateKey() {
    let url = 'https://s3-ap-southeast-1.amazonaws.com/google-sheet-service/Indorse-dev-b01211663ec5.json'; // S3 location of private key of Indorse service account
    return await new Promise((resolve, reject) => {
        request.get({
            url,
            headers: {
                'content-type': 'application/json',
                'User-Agent': 'Indorse'
            }
        }, function (error, response, body) {
            if (error) {
                reject("Error while accessing privte key " + error);
            }

            resolve(body);
        })
    });
}

async function setAuth(cred) {
    await new Promise((resolve, reject) => {
        doc.useServiceAccountAuth(cred, function(callback){
            resolve();
        });
    })
}

async function getInfoAndWorksheets(reportType) {
    await new Promise((resolve, reject) => {
        doc.getInfo(function(err, info) {
            if(reportType === 'D'){
                sheet = info.worksheets[0];
            }else if(reportType === 'W'){
                 sheet = info.worksheets[1];
            }else{
                sheet = info.worksheets[2]; // dummy or test data
            }
            logger.debug('Loaded doc: '+info.title+' by '+info.author.email);
            logger.debug('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
            resolve();
        });
    })
}

async function writeNewRow(report) {
    let reportToGoogleSheets = report;
    await new Promise((resolve, reject) => {
        sheet.addRow(reportToGoogleSheets, function(err, resp){
            logger.debug("Writing a row... Data: " + reportToGoogleSheets);
            logger.debug("Response: " + resp);
            resolve();
        })
    })
}

