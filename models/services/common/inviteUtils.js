const _inviteUtils = {};
const randtoken = require('rand-token');

// Create a string of random alphanumeric characters, of a given length
_inviteUtils.generateToken = function generateToken(strLength) {
    return randtoken.generate(strLength || 16);
};

module.exports = _inviteUtils;
