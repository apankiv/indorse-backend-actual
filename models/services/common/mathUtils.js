// raondomly select N items
exports.selectRandomNItems = function selectRandomNItems(arr, n) {
    const shuffled = arr.sort(() => .5 - Math.random());
    return shuffled.slice(0, n);
}



/* fairness measurement code written by Varun
const arr = [];
for (let i = 1; i <= 50; i += 1) {
    arr.push(i);
}


const counterMap = {};
for (let i = 0; i < 1000000; i += 1) {
    const randomArr = randomSelection(arr, 10);
    randomArr.forEach((el) => {
        if (!counterMap[el]) {
            counterMap[el] = 0;
        }
        counterMap[el] += 1;
    });
}

console.log(counterMap);
*/