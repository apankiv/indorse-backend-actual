const MerkleTree = require('merkletreejs')
const crypto = require('crypto')
const settings = require('../../settings')
const timestampService = require('../../../models/services/common/timestampService')
const mongooseClaimsRepo = require('../../services/mongo/mongoose/mongooseClaimsRepo')
const mongooseClaimSignaturesRepo = require('../../services/mongo/mongoose/mongooseClaimSignaturesRepo')
const mongooseVoteSignaturesRepo = require('../../services/mongo/mongoose/mongooseVoteSignaturesRepo');
const mongooseMerkleTreeRepo = require('../../services/mongo/mongoose/mongooseMerkleTreeRepo')
const errorUtils  = require('../../../models/services/error/errorUtils')
const ethereumjsUtil = require('ethereumjs-util')
const merkleClaims = require('../../services/ethereum/merkleClaims/proxy/merkleClaimsProxy')
const ObjectID = require('mongodb').ObjectID;

const NULL_HASH   = "0x0000000000000000000000000000000000000000000000000000000000000000"
const HIGHWM_HASH = "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"


function sha256(data) {
    //TODO : Change hash function
    return crypto.createHash('sha256').update(data).digest()
}

module.exports.getMerkleRoot = async function getMerkleRoot(leaves){
    let hashedLeaves = leaves.map(leaf => sha256(leaf))
    const tree = new MerkleTree(hashedLeaves, sha256)    
    return tree.getRoot().toString('hex');
}

function concatenateSignature(signature){
    if(!signature.r ||!signature.s)
        errorUtils.throwError("Invalid signature " + JSON.stringify(signature));

    let sig = signature.r.substring(2) + signature.s.substring(2) + signature.v.toString(16);
    return sig;
}


function sortyByClaimId(a,b){
    if (a.reference.claim_id > b.reference.claim_id){        
        return 1
    } else if (a.reference.claim_id < b.reference.claim_id){
        return -1
    } else{
        return 0;
    }   
}

module.exports.pushMerkleRoot = async function pushMerkleRoot(date){
    let normalizedDate = Math.floor(date/1000);

    console.log("\n\nMerkle root params\nPush  Date: " + normalizedDate + " , Call Date : "+date);

    //ensure this key hash is not already commited into the SC
    let root = await merkleClaims.getRootByTimeKey(normalizedDate);    
    
    if (root !== NULL_HASH){
        console.log("This root is already published");
        return;
    }

    //Get all vote signatures for the claim
    let leaves = []
    let mongoLeavesDoc = []
    let claim_ids = []
    let merk_vote_sig_ids = []
    let merk_claim_sig_ids = []

  
    let voteSignatures = await mongooseVoteSignaturesRepo.findAll({ merkelized : false});
    for(voteSig of voteSignatures){
        let sig = concatenateSignature(voteSig.signature);       
        let data ={                
            data: sig,
            type :'vote',
            reference :{
                claim_id: voteSig.payload.claim_id,
                user_id: voteSig.user_id,
                vote_signature_id : voteSig._id.toString()
            }
        }        
            
        mongoLeavesDoc.push(data);

        if(claim_ids.indexOf(voteSig.payload.claim_id) === -1){
            claim_ids.push(voteSig.payload.claim_id);
        }

        merk_vote_sig_ids.push(voteSig._id);

    }



    //Get claim signature
    let claimSigs = await mongooseClaimSignaturesRepo.findAll({merkelized : false})
    for(claimSig of claimSigs){
        if (!claimSig || !claimSig.signature)
           errorUtils.throwError("Claim signature not found for claim id : " + claim._id.toString());
    
        let sig = concatenateSignature(claimSig.signature);
        let data = {
            data: sig,
            type: 'claim',
            reference: {
                claim_id: claimSig.payload.claim_id,
                claim_signature_id: claimSig._id.toString()
            }
        }

        mongoLeavesDoc.push(data);

        if(claim_ids.indexOf(claimSig.payload.claim_id) ===-1){
            claim_ids.push(claimSig.payload.claim_id);
        }  
        
        merk_claim_sig_ids.push(claimSig._id);
       
    }
    
    //Sory by claim id in the data
    mongoLeavesDoc.sort(sortyByClaimId)

    for (leaf of mongoLeavesDoc){
        leaves.push(leaf.data)
    }


    //Create root hash
    let rootHash = HIGHWM_HASH;    
    if (leaves.length > 0){
        rootHash = "0x" + await this.getMerkleRoot(leaves);    
    } 

    //Create merkle tree data to store
    let merkleTree = {
        leaves: mongoLeavesDoc,
        rootHash: rootHash,
        claim_ids : claim_ids,
        timestamp: normalizedDate                
    }

    await mongooseMerkleTreeRepo.insert(merkleTree);
    
    let claim_ids_metadata = {
        claim_ids: claim_ids,
        merk_claim_sig_ids: merk_claim_sig_ids,
        merk_vote_sig_ids: merk_vote_sig_ids
    }
    
    await merkleClaims.addMerkleRoot(normalizedDate, rootHash, claim_ids_metadata);
       
}
