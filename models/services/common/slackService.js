const Slack = require('slack-node');
const settings = require('../../settings');
let slackBugs;
let slackClaims;

if (process.env.NODE_ENV === 'production') {
    const SLACK_BUGS_WEBHOOK = settings.SLACK_BUGS_WEBHOOK;
    const SLACK_CLAIMS_WEBHOOK = settings.SLACK_CLAIMS_WEBHOOK;
    webhookUri = "__uri___";
    slackBugs = new Slack();
    slackClaims = new Slack();
    slackBugs.setWebhook(SLACK_BUGS_WEBHOOK);
    slackClaims.setWebhook(SLACK_CLAIMS_WEBHOOK);
}

exports.reportBug = function reportBug(bug) {

    if (process.env.NODE_ENV === 'production') {
        try {
            slackBugs.webhook({
                channel: "#i_backend_bugs",
                text: "BUG ON PRODUCTION\n url: " + bug.url + "\nmethod: " + bug.method + "\nRequestId: "
                + bug.requestID + "\nStack: " + bug.stack + "\nBody: " + bug.body,
            }, function(err, response) {
                if(err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to report slack bug!", e)
        }
    }
};

exports.reportAlert = function reportEvent({ title, data }) {
    if (process.env.NODE_ENV !== 'production') {
        console.log(`ALERT ON NON PRODUCTION ${new Date().toString()}`, title, JSON.stringify(data));
        return;
    }
    try {
        slackBugs.webhook(
            {
                channel: '#i_backend_bugs',
                text: `ALERT ON PRODUCTION : ${title}\n${JSON.stringify(data)}`,
            },
            (err, response) => {
                if (err) {
                    return console.log(`SLACK ERR: ${err}`);
                }
                return console.log(response);
            },
        );
    } catch (err) {
        console.log('Unable to report event on slack!', err);
    }
};

exports.reportCronBug = reportCronBug;

function reportCronBug(bug) {
    if (process.env.NODE_ENV === 'production') {
        try {
            slackBugs.webhook({
                channel: "#i_backend_bugs",
                text: "BUG IN CRON @ivo\n url: " + bug.url + "\nmethod: " + bug.method + "\nRequestId: "
                + bug.requestID + "\nStack: " + bug.stack,
            }, function (err, response) {
                if (err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to report slack bug!", e)
        }
    } else {
        console.log("CRON BUG", bug)
    }
}

function reportDeadCron(lastSeen) {
    if (process.env.NODE_ENV === 'production') {
        try {
            slackBugs.webhook({
                channel: "#i_backend_bugs",
                text: "CRON IS DEAD! LAST SEEN " + lastSeen,
            }, function (err, response) {
                if (err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to report slack bug!", e)
        }
    } else {
        console.log("CRON IS DEAD!")
    }
}

exports.reportDeadCron = reportDeadCron;

exports.reportClaim = function reportClaim(claim, img_url, username) {

    if (process.env.NODE_ENV === 'production') {
        try {
            slackClaims.webhook({
                channel: "#i_claims",
                title: "Claim awaits approval",
                attachments: [
                    {
                        "title": username,
                        "title_link": "https://indorse.io/users/" + claim.ownerid,
                        "image_url": "https://" + img_url,
                        "color": "#111FA5"
                    },
                    {
                        "title": "Title",
                        "text": claim.title,
                        "color": "#764FA5"
                    },
                    {
                        "title": "Description",
                        "text": claim.desc,
                        "color": "#01FF45"
                    },
                    {
                        "title": "Level",
                        "text": claim.level,
                        "color": "#01FFFF"
                    },
                    {
                        "text": "Choose action",
                        "fallback": "You are unable to act",
                        "color": "#000000",
                        "attachment_type": "default",
                        actions: [
                            {
                                "type": "button",
                                "text": "Decide",
                                "url": "https://indorse.io/admin/claims?search=" + claim.claim_id.toString() + "&page=1"
                            },
                            {
                                "type": "button",
                                "text": "Proof",
                                "url": claim.proof
                            }
                        ]
                    }]
            }, function(err, response) {
                if(err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to send claim notification to slack!", e)
        }
    }
};

exports.reportStats = function reportStats(report) {
    if (process.env.NODE_ENV === 'production') {
        try {
            slackClaims.webhook({
                channel: "#i_claims",
                title: "Daily system stats",
                attachments: [
                    {
                        "title": "Validators notified yesterday",
                        "color": "#111FA5",
                        "text": report.validatorsNotified
                    },
                    {
                        "title": "Validators voted",
                        "color": "#111FA5",
                        "text": report.validatorsVoted
                    },
                    {
                        "title": "Transactions submitted from metamask yesterday",
                        "color": "#111FA5",
                        "text": report.txSubmitted
                    },
                    {
                        "title": "Succesful transactions from metamask",
                        "color": "#111FA5",
                        "text": report.txSuccessful
                    },
                    {
                        "title": "Claims created (normal + finalized drafts)",
                        "color": "#111FA5",
                        "text": report.claimsCreated
                    },
                    {
                        "title": "User signups",
                        "color": "#111FA5",
                        "text": report.signups
                    },
                    {
                        "title": "Claim drafts created",
                        "color": "#111FA5",
                        "text": report.claimDraftsCreated
                    },
                    {
                        "title": "Claim drafts finalized",
                        "color": "#111FA5",
                        "text": report.claimDraftsFinalized
                    },
                    {
                        "title": "Claims approved",
                        "color": "#111FA5",
                        "text": report.claimsApproved
                    },
                    {
                        "title": "Claims disapproved",
                        "color": "#111FA5",
                        "text": report.claimsDisapproved
                    }
                ]
            }, function (err, response) {
                if (err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to send claim notification to slack!", e);
            reportCronBug(e);
        }
    }
};


exports.reportClaimStats = function reportClaimStats(report) {
    if (process.env.NODE_ENV === 'production') {
        try {
            slackClaims.webhook({
                channel: "#i_statistics_report",
                title: "Daily Claim report",
                attachments: [
                    {
                        "title": "What is this report about?",
                        "color": "#000000",
                        "text": "This is a daily claim report. Following data is calculated from the claims created from " + report.startDateTime +
                            " to " + report.endDateTime
                    },
                    {
                        "title": "# Claims created successfully",
                        "color": "#111FA5",
                        "text": report.submitted
                    },
                    {
                        "title": "# Claims approved",
                        "color": "#01FF45",
                        "text": report.approved
                    },
                    {
                        "title": "# Claims disapproved",
                        "color": "#FF0000",
                        "text": report.disapproved
                    },
                    {
                        "title": "# Claims Indorsed",
                        "color": "#01FF45",
                        "text": report.indorsed
                    },
                    {
                        "title": "# Claims flagged",
                        "color": "#FF0000",
                        "text": report.flagged
                    },
                    {
                        "title": "% Claims Indorsed",
                        "color": "#111FA5",
                        "text": report.indorsePecentage
                    },
                    {
                        "title": "# Votes sent",
                        "color": "#111FA5",
                        "text": report.votesSent
                    },
                    {
                        "title": "# Votes (DB)",
                        "color": "#111FA5",
                        "text": report.votesDatabase
                    },
                    {
                        "title": "# Votes (Blockchain)",
                        "color": "#111FA5",
                        "text": report.votesBlockchain
                    },
                    {
                        "title": "Avg. Votes/Claim",
                        "color": "#111111",
                        "text": report.avgVotesPerClaim
                    },

                ]
            }, function (err, response) {
                if (err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to send claim notification to slack!", e);
            reportCronBug(e);
        }
    }
};


