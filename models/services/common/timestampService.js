exports.createTimestamp = function createTimestamp() {
    return Math.floor(Date.now() / 1000);
};

exports.timestampDaysBack = function (days) {
    var date = new Date();
    var year = date.getFullYear()
    var month = date.getMonth()
    var day = date.getDate();
    var resultDate = new Date(year, month, day - parseInt(days))

    return Math.floor(resultDate / 1000);
}

exports.createPastTimestamp = function createPastTimestamp(date,days) {
    let lookback = new Date(date.getTime());
    lookback.setDate(lookback.getDate() - days)
    return Math.floor(lookback/ 1000);
};

exports.normalizeToStartOfDay = function normalizeToStartOfDay(date){
    var year = date.getFullYear();
    var month = date.getMonth()
    var day = date.getDate();
    var normDate = new Date(year, month, day)
    return Math.floor(normDate / 1000);
}

exports.timestampFromDate = function timestampFromDate(date) {
    return Math.floor(date.getTime() / 1000);
};
