const INDORSE_ADMIN = 'indorse_admin';
const COMPANY_ADMIN = 'admin'; // Made - admin to match the role in mongo
const NO_PERMISSION = 'no_permission';
const roles = {
    INDORSE_ADMIN,
    COMPANY_ADMIN,
    NO_PERMISSION,
    values: [
        INDORSE_ADMIN,
        COMPANY_ADMIN,
        NO_PERMISSION,
    ],
};

function GeneratePermissions({ magicLink, partnerClaims }) {
    this.magicLink = magicLink;
    this.partnerClaims = partnerClaims;
    this.permissions = {
        [INDORSE_ADMIN]: {
            features: {
                read: true,
                write: true,
            },
            basic: {
                read: true,
                write: true,
            },
            magicLink: {
                read: this.magicLink,
                write: this.magicLink,
            },
            candidates: {
                read: this.magicLink,
                write: false,
            },
            acl: {
                read: true,
                write: true,
            },
            clientApps: {
                read: this.partnerClaims,
                write: this.partnerClaims,
            },
            partnerClaims: {
                read: this.partnerClaims,
                write: false,
            },
            credits: {
                read: this.magicLink,
                write: this.magicLink,
            },
            aip: {
                read: true,
                write: true,
            },
        },
        [COMPANY_ADMIN]: {
            features: {
                read: true,
                write: false,
            },
            basic: {
                read: true,
                write: true,
            },
            magicLink: {
                read: this.magicLink,
                write: this.magicLink,
            },
            candidates: {
                read: this.magicLink,
                write: false,
            },
            acl: {
                read: true,
                write: false,
            },
            clientApps: {
                read: this.partnerClaims,
                write: this.partnerClaims,
            },
            partnerClaims: {
                read: this.partnerClaims,
                write: false,
            },
            credits: {
                read: this.magicLink,
                write: false,
            },
            aip: {
                read: true,
                write: false,
            },
        },
        [NO_PERMISSION]: {
            features: {
                read: false,
                write: false,
            },
            basic: {
                read: true,
                write: false,
            },
            magicLink: {
                read: false,
                write: false,
            },
            candidates: {
                read: false,
                write: false,
            },
            acl: {
                read: false,
                write: false,
            },
            clientApps: {
                read: false,
                write: false,
            },
            partnerClaims: {
                write: false,
                read: false,
            },
            credits: {
                read: false,
                write: false,
            },
            aip: {
                read: false,
                write: false,
            },
        },
    };
    this.getPermission = (role) => {
        if (roles.values.indexOf(role) === -1) {
            return this.permissions[roles.NO_PERMISSION];
        }
        return this.permissions[role];
    };
}

module.exports = {
    GeneratePermissions,
    roles,
};
