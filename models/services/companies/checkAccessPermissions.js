const _ = require('lodash');
const { getAccessPermissions } = require('./getAccessPermissions');

module.exports.checkAccessPermissions = async ({ userId, companyId, permissionPaths }) => {
    if (!userId) return false;
    const permission = await getAccessPermissions({ userId, companyId });
    let allowed = false;
    permissionPaths.forEach((permissionPath) => {
        const value = _.get(permission, permissionPath, false);
        allowed = (allowed || value);
    });
    return allowed;
};
