const mongooseCompanyRepo = require('../mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../error/errorUtils');
const accessRole = require('./accessRoles');

const _handler = {};

_handler.getAccessPermissions = async ({ userId, companyId }) => {
    const company = await mongooseCompanyRepo.findOneById(companyId);
    if (!company || !company._id) {
        errorUtils.throwError('Company not found !', 404);
    }
    let companyFeatures;
    if (company.features) {
        companyFeatures = company.features;
    } else {
        // assign feature if feature is not found in the company object
        companyFeatures = {
            magicLink: true, // enabling magic link feature for now
            partnerClaims: false,
        };
    }
    const permissionGenerator = new accessRole.GeneratePermissions(companyFeatures);

    const user = await mongooseUserRepo.findOneById(userId);
    if (!user || !user._id) {
        // Returning permission model of Access Role - No Permission if user is null
        return permissionGenerator.getPermission(accessRole.roles.NO_PERMISSION);
    }
    const companyACL = company.acl;
    const foundUserInACL = (companyACL || []).find(acl => (acl.userId || '').toString() === user._id.toString());

    let permissions = {};
    if (user.role === 'admin') {
        permissions = permissionGenerator.getPermission(accessRole.roles.INDORSE_ADMIN);
    } else if (foundUserInACL && foundUserInACL.role) {
        permissions = permissionGenerator.getPermission(foundUserInACL.role);
    } else {
        permissions = permissionGenerator.getPermission(accessRole.roles.NO_PERMISSION);
    }
    console.log('permissions: ', permissions);
    return permissions;
};

module.exports = _handler;
