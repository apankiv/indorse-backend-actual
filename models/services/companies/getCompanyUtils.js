const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const settings = require('../../settings');

module.exports.prepareCompanyObject = async function prepareCompanyObject(company) {
    const companyToReturn = {};
    companyToReturn.id = company._id.toString();
    companyToReturn.prettyId = company.pretty_id;
    companyToReturn.tagline = company.tagline;
    companyToReturn.companyName = company.company_name;
    companyToReturn.description = company.description;
    companyToReturn.socialLinks = company.social_links;
    companyToReturn.logo = {};
    companyToReturn.logo.s3Url = company.logo_s3;
    companyToReturn.logo.ipfsUrl = company.logo_ipfs;
    companyToReturn.teamMembers = [];
    companyToReturn.acl = company.acl;
    companyToReturn.aipLimit = company.aipLimit || Number(settings.CLAIM_VALIDATOR_COUNT);
    companyToReturn.magicLink = {
        assignmentRestricted: (company.magicLink && company.magicLink.assignmentRestricted) === true,
        autoAllowedAssignment: (company.magicLink && company.magicLink.autoAllowedAssignment) === true,
    };
    companyToReturn.companyCredit = await prepareCompanyCreditObject(company);
    return companyToReturn;
};

async function prepareCompanyCreditObject(company) {
    let companyCreditToReturn = {};
    let companyCredits = company.credits;
    companyCreditToReturn = companyCredits;
    for (var key in companyCreditToReturn) {
        let limit = companyCreditToReturn[key].limit;
        let used = companyCreditToReturn[key].used;
        let hidden = limit - used;
        if (hidden > 0) {
            hidden = 0;
        }
        companyCreditToReturn[key].noOfHiddenAssignment = -hidden;
    }
    if (!companyCreditToReturn) {
        return {};
    }
    companyCreditToReturn.updates = company.credit_updates;
    if (companyCreditToReturn.updates) {
        for (let i = 0; i < companyCreditToReturn.updates.length; i++) {
            let creditUpdatedUserId = companyCreditToReturn.updates[i].userId;
            companyCreditToReturn.updates[i].user = await mongooseUserRepo.findOneById(creditUpdatedUserId);
        }
        companyCreditToReturn.updates = companyCreditToReturn.updates.reverse();
    }

    return companyCreditToReturn;
};


module.exports.prepareCompanyCreditObject = prepareCompanyCreditObject;