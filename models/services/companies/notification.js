const mongooseUserAssessmentRepo = require('../mongo/mongoose/mongooseUserAssessmentRepo');
const mongooseCompanyRepo = require('../mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const mongooseAssessmentMagicLinkRepo = require('../mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseAssignmentRepo = require('../mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../mongo/mongoose/mongooseUserAssignmentRepo');

const mongoose = require('mongoose');
const { status: userAssessmentStatus } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');
const _ = require('lodash');
const timestampService = require('../common/timestampService');
const emailService = require('../common/emailService');

const _notify = {};

async function adminsToBeNotifiedForCompany(companyId) {
    // find company
    const company = await mongooseCompanyRepo.findOneById(companyId);
    const { acl } = company;
    let admins = (acl || []).map((aclMember) => {
        if (aclMember.userId && aclMember.role === 'admin') {
            return aclMember.userId;
        }
        return null;
    });
    admins = admins.filter(Boolean);
    const criteria = {
        _id: {
            $in: admins,
        },
    };
    const users = await mongooseUserRepo.findAll(criteria);
    return (users || []).map(user => ({ userId: user._id, address: user.email, role: 'admin' }));
}

async function sendAssessmentStatusUpdateEmail(
    purpose,
    email,
    { name: candidateName },
    { title: magicLinkTitle, publicId: magicLinkPublicId },
    { company_name: companyName, pretty_id: companyPrettyId },
    claimId,
) {
    const objectForEmail = {
        email,
        candidateName,
        companyName,
        companyPrettyId,
        magicLinkPublicId,
        magicLinkTitle,
        claimId,
    };
    console.log('purpose: ', purpose);
    console.log('objectForEmail: ', objectForEmail);
    switch (purpose) {
    case `COMPANY_${userAssessmentStatus.CHATBOTS_PASSED}`:
        console.log('send email for chatbot passed case');
        await emailService.chatbotsPassedCompanyUpdate(objectForEmail);
        break;
    case `COMPANY_${userAssessmentStatus.ASSIGNMENTS_VIEWED}`:
        console.log('send email for assignment viewed case');
        await emailService.assignmentViewedCompanyUpdate(objectForEmail);
        break;
    case `COMPANY_${userAssessmentStatus.ASSIGNMENT_EVALUATED}`:
        console.log('send email for assignment evaluated case');
        await emailService.assignmentEvaluatedCompanyUpdate(objectForEmail);
        break;
    case `COMPANY_${userAssessmentStatus.CLAIM_EVALUATED}`:
        console.log('send email for claim evaluated case');
        await emailService.claimEvaluatedCompanyUpdate(objectForEmail);
        break;
    default:
        console.log('No case matched');
    }
}

async function markNotificationSent(userAssessmentId, { purpose, to }) {
    // Record the update
    const updateToPush = {
        purpose,
        to,
        notifiedAt: timestampService.createTimestamp(),
    };
    const criteria = {
        _id: userAssessmentId,
    };
    const dataToSet = {
        $push: {
            notification: updateToPush,
        },
    };
    await mongooseUserAssessmentRepo.update(criteria, dataToSet);
}

async function findNotification(userAssessmentId, { purpose, to }) {
    const userAssessment = await mongooseUserAssessmentRepo.findOneById(userAssessmentId);
    const { notification } = userAssessment;
    const toForCompare = (to || []).map(elem => ({ address: elem.address, role: elem.role }));
    const toSorted = _.sortBy(toForCompare, ['address', 'role']);
    return notification.find((n) => {
        const secondToForCompare = (n.to || []).map(elem => ({ address: elem.address, role: elem.role }));
        const n_toSorted = _.sortBy(secondToForCompare, ['address', 'role']);
        return n.purpose === purpose && _.isEqual(toSorted, n_toSorted);
    });
}

_notify.userAssessmentStatusUpdate = async (status, userAssessmentId, shouldResend = false) => {
    const purpose = `COMPANY_${status}`;
    // Assemble Required Data
    const userAssessment = await mongooseUserAssessmentRepo.findOneById(userAssessmentId);
    if (!userAssessment) {
        throw new Error('User assessment should not be empty till here');
    }
    const startedByUser = await mongooseUserRepo.findOneById(userAssessment.startedBy);
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneById(userAssessment.assessment);
    const company = await mongooseCompanyRepo.findOneById(userAssessment.company);
    const userAssignment = await mongooseUserAssignmentRepo.findOne({ _id: userAssessment.userAssignmentId });
    const claimId = (userAssignment || {}).claim_id || userAssessment.claimId;
    const adminsToBeNotified = await adminsToBeNotifiedForCompany(userAssessment.company);
    // format notification
    const notificationObject = { purpose, to: adminsToBeNotified };

    // find notification
    const doesNotificationExist = await findNotification(userAssessmentId, notificationObject);

    if (doesNotificationExist && !shouldResend) return;
    // send notification to each user
    // eslint-disable-next-line
    for (let i = 0; i < adminsToBeNotified.length; i += 1) {
        // send emails
        const adminEmail = adminsToBeNotified[i].address;
        // eslint-disable-next-line
        await sendAssessmentStatusUpdateEmail(purpose, adminEmail, startedByUser, magicLink, company, claimId);
    }
    // eslint-disable-next-line
    await markNotificationSent(userAssessmentId, notificationObject);
};

async function sendUserAssessmentReminder(
    purpose,
    email,
    { name: candidateName },
    { title: magicLinkTitle, publicId: magicLinkPublicId },
    { company_name: companyName, pretty_id: companyPrettyId },
    assignments,
) {
    const objectForEmail = {
        email,
        candidateName,
        companyName,
        companyPrettyId,
        magicLinkPublicId,
        magicLinkTitle,
        assignments,
    };
    console.log('purpose: ', purpose);
    console.log('objectForEmail: ', objectForEmail);
    if (assignments && assignments.length) {
        await emailService.sendReminderToSolveMagicLinkAssignment(objectForEmail);
    } else {
        await emailService.sendReminderToViewMagicLinkAssignment(objectForEmail);
    }
}

async function getUserToBeNotifed(startedBy) {
    const startedByUser = await mongooseUserRepo.findOneById(startedBy);
    const userToBeNotifed = {
        address: startedByUser.email,
        userId: startedByUser._id,
        role: 'candidate',
    };
    return [userToBeNotifed];
}

_notify.userAssessmentStatusReminder = async (status, userAssessmentId, shouldResend = false) => {
    const purpose = `CANDIDATE_${status}`;
    // Assemble Required Data
    const userAssessment = await mongooseUserAssessmentRepo.findOneById(userAssessmentId);
    console.log('userAssessment: ', userAssessment);
    if (!userAssessment) {
        throw new Error('User assessment should not be empty till here');
    }
    const startedByUser = await mongooseUserRepo.findOneById(userAssessment.startedBy);
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneById(userAssessment.assessment);
    const company = await mongooseCompanyRepo.findOneById(userAssessment.company);
    const userToBeNotifed = await getUserToBeNotifed(userAssessment.startedBy);
    console.log('userToBeNotifed: ', userToBeNotifed);
    const criteriaForAssignment = {
        startedForUserAssessment: mongoose.Types.ObjectId(userAssessmentId),
    };
    const linkedUserAssignments = await mongooseUserAssignmentRepo.findAll(criteriaForAssignment);
    const linkedAssignmentIds = (linkedUserAssignments || []).map(elem => elem.assignment_id);
    // find assignment metadata
    const assignments = await mongooseAssignmentRepo.findAll({
        _id: {
            $in: linkedAssignmentIds,
        },
    });
    // format notification
    const notificationObject = { purpose, to: userToBeNotifed };
    // find notification
    const doesNotificationExist = await findNotification(userAssessmentId, notificationObject);
    console.log('doesNotificationExist: ', doesNotificationExist);
    if (doesNotificationExist && !shouldResend) return;
    console.log('company.prettt_id: ', company.prettt_id);
    console.log('magicLink.title: ', magicLink.title);
    console.log('assignments: ', assignments);
    for (let i = 0; i < userToBeNotifed.length; i += 1) {
        // send emails
        const userEmail = userToBeNotifed[i].address;
        // eslint-disable-next-line
        await sendUserAssessmentReminder(purpose, userEmail, startedByUser, magicLink, company, assignments);
    }
    await markNotificationSent(userAssessmentId, notificationObject);
};

_notify.sendAutoAllowedAssignmentEmail = async (userAssessmentId) => {
    const userAssessment = await mongooseUserAssessmentRepo.findOneById(userAssessmentId);
    console.log('userAssessment: ', userAssessment);
    if (!userAssessment) {
        return;
    }
    const startedByUser = await mongooseUserRepo.findOneById(userAssessment.startedBy) || {};
    const company = await mongooseCompanyRepo.findOneById(userAssessment.company) || {};
    const magicLink = await mongooseAssessmentMagicLinkRepo.findOneById(userAssessment.assessment) || {};
    await emailService.sendAutoAllowedAssignmentForMagicLinkEmail(startedByUser.email, startedByUser.name, company.pretty_id, magicLink.publicId, magicLink.title);
};

module.exports = _notify;
