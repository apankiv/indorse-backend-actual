const mongooseCompanyRepo = require('../mongo/mongoRepository')('company_names')
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../error/errorUtils');
const stringUtils = require('../blob/stringUtils')
var shortid = require('shortid');


module.exports.searchCompanies = async function searchCompanies(search) {
    const searchRegex = stringUtils.createSafeRegexObject(search);
    const companiesCursor = await mongooseCompanyRepo.findAllWithCursor({ $and: [{ company_name: searchRegex }, 
                                                                                { visible_to_public: true },
                                                                                { pretty_id: { $exists: true } }] });
    return companiesCursor;
}

module.exports.prepareSortObj = async function prepareSortObj(sort) {    
    let field = 'company_name';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);            
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }
    }
    
    if (order && field) {    
        return { [field]: order };
    }
    return {};
}

function formatExistingUserList(users){
    let userList = []
    for(user of users){
        let userToPush = {}
        if(user._id){
            userToPush.id = user._id.toString()
        } else {
            continue;
        }

        if (user.username){
            userToPush.userName = user.username
        }
       
        userToPush.photo = {}
        if (user.img_url){
            userToPush.photo.imgUrl = user.img_url
        }

        if (user.photo_ipfs){
            userToPush.photo.ipfsUrl = user.photo_ipfs
        }

        if (user.name){
            userToPush.fullName = user.name
        }
        userList.push(userToPush)        
    }
    return userList;
}

function formatUnregisterdUsersList(users) {
    let userList = []
    for (user of users) {
        let userToPush = {};
        //Generate shortid for FE to cache gql response
        userToPush.id = shortid.generate();

        if (user.name) {
            userToPush.fullName = user.name;
        }

        userToPush.photo = {}
        if (user.imgUrl) {
            userToPush.photo.imgUrl = user.imgUrl;
        }
        userList.push(userToPush);
    }
    return userList;
}


function formatAdditionalDataUserList(users) {
    let userList = []
    for (user of users) {   
        if(user.user){
            let userToPush = {};

            //Generate shortid for FE to cache gql response
            userToPush.id = shortid.generate();

            if(user.user.name){
                userToPush.fullName = user.user.name;
            }

            userToPush.photo = {}
            if(user.user.img_url){
                userToPush.photo.imgUrl = user.user.img_url;
            }
            userList.push(userToPush);
        }
    }    
    return userList;
}



module.exports.prepareCompanyObject = async function prepareCompanyObject(company){
    let companyToReturn ={};
    companyToReturn.id = company._id.toString();
    companyToReturn.prettyId = company.pretty_id;
    companyToReturn.tagline = company.tagline;
    companyToReturn.companyName = company.company_name;
    companyToReturn.description = company.description;
    companyToReturn.socialLinks = company.social_links;
    companyToReturn.logo ={}
    companyToReturn.logo.s3Url = company.logo_s3;
    companyToReturn.logo.ipfsUrl = company.logo_ipfs;
    //team members   
    companyToReturn.teamMembers = []
    let unregisterdTeamMbrs = []
    if (company.team_members && company.team_members.length > 0){
        let teamMemberIdsToSearch = []
        for (teamMember of company.team_members) {
            if(!hasUserRejected(teamMember)){
                if (teamMember.user_id) {
                    teamMemberIdsToSearch.push(teamMember.user_id)
                } else {
                    unregisterdTeamMbrs.push(teamMember);
                }
            }              
        }
        let teamMemberList = await mongooseUserRepo.findAll({ _id: { $in: teamMemberIdsToSearch } });
        companyToReturn.teamMembers = formatExistingUserList(teamMemberList);
        if (unregisterdTeamMbrs.length > 0){            
            let unregTeamMembers = formatUnregisterdUsersList(unregisterdTeamMbrs);            
            companyToReturn.teamMembers.push.apply(companyToReturn.teamMembers,unregTeamMembers)            
        }     
    } 


    //Advisors   
    companyToReturn.advisors = []
    let unregisteredAdvisors = []
    if (company.advisors && company.advisors.length > 0){
        let advisorIdsToSearch = []
        for (advisor of company.advisors) {
            if (!hasUserRejected(advisor)){
                if (advisor.user_id) {
                    advisorIdsToSearch.push(advisor.user_id)
                } else {
                    unregisteredAdvisors.push(advisor);
                }
            }                
        }
        let advisorList = await mongooseUserRepo.findAll({ _id: { $in: advisorIdsToSearch } });
        companyToReturn.advisors = formatExistingUserList(advisorList);
        if (unregisteredAdvisors.length > 0){
            let unregAdvisors = formatUnregisterdUsersList(unregisteredAdvisors);
            companyToReturn.advisors.push.apply(companyToReturn.advisors,unregAdvisors);            
        }
    } 

    let additionalData = {
        teamMembers: [],
        advisors: []
    };    

    if (company.additional_data){
        if (company.additional_data.team && company.additional_data.team.length > 0){
            additionalData.teamMembers = formatAdditionalDataUserList(company.additional_data.team);
        }
        if (company.additional_data.advisors && company.additional_data.advisors.length > 0) {
            additionalData.advisors = formatAdditionalDataUserList(company.additional_data.advisors);
        }        
    }
    companyToReturn.additionalData = additionalData;  
    return companyToReturn;
}


function hasUserRejected(user){
    if (user.softConnection && user.softConnection.status) {
        if (user.softConnection.status == 'ACCEPTED')
            return false;

        if (user.softConnection.status == 'REJECTED'){            
            return true;
        }           
    }

    if (user.rejected_timestamp){
        return true;
    }
    return false;        
}