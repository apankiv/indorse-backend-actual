// https://attores.atlassian.net/browse/ID-1545

const config = require('config');
const mongooseUserAssignmentRepo = require('../mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const { sendAssignmentReminderToUser } = require('../common/emailService');
const timestampService = require('../common/timestampService');
const logger = require('../../../models/services/common/logger').getLogger();

exports.sendAssignmentReminders = async function sendAssignmentReminders() {
    const assignment_reminder_enabled = config.get('cron.assignment_reminder_enabled');
    if (assignment_reminder_enabled === 'true' || assignment_reminder_enabled === true) {
        logger.debug('[sendAssignmentReminders]: Job started ');
        console.log('[sendAssignmentReminders]: Job started ');
        try {
            let userAssignments = await mongooseUserAssignmentRepo.getAssignmentsForReminder();
            userAssignments = userAssignments || [];
            logger.debug(`[sendAssignmentReminders]: Sending reminders to finish assignments -${userAssignments.length}`);
            console.log(`[sendAssignmentReminders]: Sending reminders to finish assignments -${userAssignments.length}`);

            for (let i = 0; i < userAssignments.length; i += 1) {
                if (userAssignments[i].reminderSentAt) {
                    // const { reminderSentAt } = userAssignments[i];
                    // const lastToBeSentTime = timestampService.timestampDaysBack(7);
                    // if (lastToBeSentTime < reminderSentAt) {
                    //     continue;
                    // }
                    continue;
                }
                // console.log('Finding owner - ', userAssignments[i].owner_id);
                const user = await mongooseUserRepo.findOneById(userAssignments[i].owner_id);
                // console.log('User is -', user);
                if (user) {
                    logger.debug(`[sendAssignmentReminders]: Sending reminder to -${user.name}`);
                    console.log(`[sendAssignmentReminders]: Sending reminder to -${user.name}`);
                    await sendAssignmentReminderToUser(user, userAssignments[i].assignment_id);
                    await mongooseUserAssignmentRepo.markReminderSent(userAssignments[i]._id);
                }
            }
        } catch (error) {
            console.log(error);
        }
    } else {
        logger.debug('[sendAssignmentReminders]: cron.assignment_reminder_enabled is false');
        console.log('[sendAssignmentReminders]: cron.assignment_reminder_enabled is false');
    }
};
