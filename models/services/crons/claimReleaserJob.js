const config = require('config');
const mongooseClaimQueueRepo = require('../mongo/mongoose/mongooseClaimQueueRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const approveClaimService = require('../../claims/approveClaimService');
const slackService = require('../common/slackService');
const timestampService = require('../common/timestampService');

exports.releaseClaims = async function releaseClaims() {

    try {
        let batchSize = Number(config.get("claims.approvedBatchSize"));
        let claimsToApprove = await mongooseClaimQueueRepo.getLatestApprovedBatch(batchSize);

        for (let queuedClaim of claimsToApprove) {
            let claim = await mongooseClaimsRepo.findOneById(queuedClaim.claim_id);
            await approveClaimService.approveClaim(claim);
            await mongooseClaimQueueRepo.update({claim_id: queuedClaim.claim_id}, {released: true});
            await mongooseClaimsRepo.update({_id: queuedClaim.claim_id},
                {released_at: timestampService.createTimestamp()})
        }
    } catch (e) {
        slackService.reportCronBug(e);
    }
};