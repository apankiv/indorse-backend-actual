/* eslint-disable no-console */
/* eslint-disable no-await-in-loop, max-len */
// https://attores.atlassian.net/browse/ID-1545

const config = require('config');
const mongooseUserAssessmentRepo = require('../mongo/mongoose/mongooseUserAssessmentRepo');
const { userAssessmentStatusReminder } = require('../companies/notification');
// const { } = require('../common/emailService');
const logger = require('../../../models/services/common/logger').getLogger();

exports.sendMagicLinkAssessmentReminders = async function sendMagicLinkAssessmentReminders() {
    const magic_link_assessment_reminder_enabled = config.get('cron.magic_link_assessment_reminder_enabled');
    console.log('magic_link_assessment_reminder_enabled: ', magic_link_assessment_reminder_enabled);
    if (
        magic_link_assessment_reminder_enabled === 'true' ||
        magic_link_assessment_reminder_enabled === true
    ) {
        logger.debug('[sendMagicLinkAssessmentReminders]: Job started ');
        console.log('[sendMagicLinkAssessmentReminders]: Job started ');
        try {
            let userAssessments = await mongooseUserAssessmentRepo.getMagicLinkAssessmentsForReminder();
            userAssessments = userAssessments || [];
            logger.debug(`[sendMagicLinkAssessmentReminders]: Sending reminders to finish assesments -${userAssessments.length}`);
            console.log(`[sendMagicLinkAssessmentReminders]: Sending reminders to finish assesments -${userAssessments.length}`);

            for (let i = 0; i < userAssessments.length; i += 1) {
                const userAssessment = userAssessments[i];
                await userAssessmentStatusReminder(userAssessment.status, userAssessment._id);
            }
        } catch (error) {
            console.log(error);
        }
    } else {
        logger.debug('[sendMagicLinkAssessmentReminders]: cron.magic_link_assessment_reminder_enabled is false');
        console.log('[sendMagicLinkAssessmentReminders]: cron.magic_link_assessment_reminder_enabled is false');
    }
};
