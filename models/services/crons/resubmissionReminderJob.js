const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const timestampService = require('../common/timestampService');
const slackService = require('../common/slackService');
const emailService = require('../common/emailService');

const SECONDS_IN_DAY = 86400;

exports.sendResubmissionReminders = async function sendResubmissionReminders() {

    try {
        console.log("sendResubmissionReminders start");


        let claims = await mongooseClaimsRepo.findAll({
            resubmission_reminder_sent_at: {$exists: false},
            $or: [
                {disapproved: {$gt: 1548259395 }},
                {final_status: false, approved: {$gt: 1548259395 }}
            ]
        });

        console.log("sendResubmissionReminders fetched ", claims.length);

        let now = timestampService.createTimestamp();

        for (let claim of claims) {
            console.log("Processing claim ", claim._id);

            if ((claim.disapproved && (claim.disapproved < (now - 2 * SECONDS_IN_DAY))) ||
                (claim.final_status === false && (claim.approved < (now - 4 * SECONDS_IN_DAY)))) {
                console.log("Sending disapproval reminder for claim ", claim._id);
                await mongooseClaimsRepo.update({_id: claim._id.toString()}, {$set: {resubmission_reminder_sent_at: now}});
                let user = await mongooseUserRepo.findOne({_id: claim.ownerid});

                if (user) {
                    emailService.sendClaimResubmissionReminderToUser(user);
                }
            }
        }

    } catch (error) {
        slackService.reportCronBug(error);
    }
};
