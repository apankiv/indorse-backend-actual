exports.createError = createError;
exports.throwError = throwError;

exports.falsyGuard = function falsyGuard(...args) {
    for(let arg of args) {
        if(!arg) {
            throwError("Invalid function argument",500);
        }
    }
};

exports.isHttpCodeAllowedInResponse = function isHttpCodeAllowedInResponse(httpCode) {
    const allowedCodesMap = {
        400: true,
        401: true,
        403: true,
        404: true,
        422: true,
        429: true,
    };
    return !!allowedCodesMap[httpCode];
};

function createError(message, code) {
    const error = new Error(message);
    if (code) {
        error.code = code;
        Object.assign(error, { extensions: code });
    }
    return error;
}

function throwError(message, code) {
    const error = createError(message, code);

    throw error;
}
