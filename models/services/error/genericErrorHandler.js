const logger = require('../common/logger').getLogger('ErrorMiddleware');
const slackService = require('../common/slackService');
const errorUtils = require('./errorUtils');
const uuidv1 = require('uuid/v1');

exports.handleError = function handleError(err, req, res, next) {
    const requestID = req.requestID || uuidv1();

    let bug = {
        stack: err.stack,
        url: req.url,
        method: req.method,
        requestID: requestID,
        email : req.email
    };

    logger.error(err.message, bug);

    const responseData = {
        message: err.message,
        requestID: requestID,
        success: false
    };
    if (errorUtils.isHttpCodeAllowedInResponse(err.code)) {
        res.status(err.code);
    } else {
        responseData.message = "Oops something went wrong! Please send this identifier to our support email and we will investigate " +
            "the issue: " + requestID;
        res.status(500);
        slackService.reportBug(bug);
    }

    res.send(responseData);
};
