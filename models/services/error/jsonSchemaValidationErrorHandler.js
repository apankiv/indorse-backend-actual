const logger = require('../common/logger').getLogger('SchemaValidation');
const uuidv1 = require('uuid/v1');

exports.handleError = function handleError(err, req, res, next) {
    if (err.name === 'JsonSchemaValidationError') {

        res.status(422);

        const requestID = uuidv1();

        // Format the response body however you want
        let responseData = {
            message: err.message,
            jsonSchemaValidation: true,
            validations: err.validationErrors,
            requestID: requestID,
            success: false
        };

        logger.error('Request did not pass validation', {
            url: req.url,
            method: req.method,
            body: req.body,
            params: req.params,
            validations: err.validationErrors,
            requestID: requestID,
        });

        res.send(responseData);
    } else {
        // pass error to next error middleware handler
        next(err);
    }
};