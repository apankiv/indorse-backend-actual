const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const connectionsContract = require('../../../../../smart-contracts/services/connections/connectionsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');

exports.execute = async function execute(companyId, companyEntityAddress, userEntityAddress, connectionType, direction, initiatingUserId,metaData=undefined) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "Connections",
        function_name: 'addConnection',
        function_args: [companyEntityAddress, userEntityAddress, connectionType.value, direction.value],
        callback_data: {
            company_id: companyId.toString()
        },
    };

    if (metaData && metaData.user_id && metaData.type){
        tx_metadata.callback_data.user_id = metaData.user_id;
        tx_metadata.callback_data.type = metaData.type;
    }

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, initiatingUserId);    
    if(!dupTx){
        let result = await connectionsContract.addConnection(companyEntityAddress, userEntityAddress, connectionType.value, direction.value);
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: initiatingUserId,
            tx_metadata: tx_metadata,
            status: 'QUEUED'
        }); 
    }
};