const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const connectionsContract = require('../../../../../smart-contracts/services/connections/connectionsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')

exports.execute = async function execute(companyId, initiatingUserId) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "Connections",
        function_name: 'createVirtualEntity',
        function_args: [],
        callback_data: {
            company_id: companyId
        }
    };
    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, initiatingUserId);

    if (!dupTx) {
        let result = await connectionsContract.createVirtualEntity();
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: initiatingUserId,
            tx_metadata: tx_metadata,
            status : 'QUEUED'
        });
        return result;
    }

};