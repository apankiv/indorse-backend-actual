const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const connectionsContract = require('../../../../../smart-contracts/services/connections/connectionsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')

exports.execute = async function execute(companyEntityAddress, userEntityAddress, connectionType, initiatingUserId) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "Connections",
        function_name: 'removeConnection',
        function_args: [companyEntityAddress, userEntityAddress, connectionType.value]
    };

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, initiatingUserId);

    if(!dupTx){
        let result = await connectionsContract.removeConnection(companyEntityAddress, userEntityAddress, connectionType.value);
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: initiatingUserId,
            tx_metadata: tx_metadata,
            status: 'QUEUED'
        });
    }
};