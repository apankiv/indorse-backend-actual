const mongooseTransactionQueueRepo = require('../mongo/mongoose/mongooseTransactionQueueRepo');
const errorUtils = require('../error/errorUtils');
const logger = require('../common/logger').getLogger();
const checkConnectionDuplicates = require('./connections/txExecutors/checkConnectionDuplicates');
const checkIndorseTokenDuplicates = require('./indorseToken/txExecutors/checkIndorseTokenDuplicates');
const checkMerkleClaimsDuplicates = require('./merkleClaims/txExecutors/checkClaimsDuplicates');

exports.checkForDuplicates = async function checkForDuplicates(tx_metadata, initiatingUserId) {
    console.log("Checking for duplicate transactions in " + JSON.stringify(tx_metadata));
    errorUtils.falsyGuard(arguments);

    let transaction = await mongooseTransactionQueueRepo.findOne({
        initiating_user_id: initiatingUserId,
        'tx_metadata.callback_data': tx_metadata.callback_data,
        'tx_metadata.contract': tx_metadata.contract,
        'tx_metadata.function_name': tx_metadata.function_name,
        'tx_metadata.function_args': tx_metadata.function_args,
        status: 'QUEUED'
    });

    if (transaction) {
        logger.error("A duplicate transaction was detected : " + JSON.stringify(tx_metadata))
        return true;
    }

    //Per contract case : TODO : Might be more sensible to do this before catch all
    if (tx_metadata.contract === 'Connections'){
        return await checkConnectionDuplicates.checkDuplicates(tx_metadata, initiatingUserId);
    } else if (tx_metadata.contract === 'IndorseToken') {
        return await checkIndorseTokenDuplicates.checkDuplicates(tx_metadata, initiatingUserId);
    } else if (tx_metadata.contract === 'MerkleClaims'){
        return await checkMerkleClaimsDuplicates.checkDuplicates(tx_metadata, initiatingUserId)
    } else {
        logger.error("Duplicate guard contract name was not found, please check the tx settings : " + JSON.stringify(tx_metadata))
        errorUtils.throwError("Contract to call was not found", 400)
    }
};