const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../error/errorUtils');
const transferExecutor = require('../txExecutors/transferExecutor');

exports.sendIndorseTokensToUserForClaim = async function sendIndorseTokensToUserForClaim(user_id, value, initiatingUserId, claim_id) {
    let userRecipient = await mongooseUserRepo.findOneById(user_id);

    if (!userRecipient) {
        errorUtils.throwError("User to send to not found!", 500);
    }

    if (!userRecipient.ethaddress) {
        return;
    }

    await transferExecutor.execute(user_id, userRecipient.ethaddress, value, initiatingUserId, {claim_id : claim_id});
};