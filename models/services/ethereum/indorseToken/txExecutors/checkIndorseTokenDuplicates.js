const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const logger = require('../../../common/logger').getLogger();
const errorUtils = require('../../../error/errorUtils');

module.exports.checkDuplicates = async function checkDuplicates(tx_metadata, initiatingUserId){
    if (tx_metadata.function_name === 'transfer'){
        let userID = tx_metadata.callback_data.user_id;
        let claimID = tx_metadata.callback_data.claim_id;

        let txQueueCount = await mongooseTransactionQueueRepo.count({
                'tx_metadata.callback_data.user_id': userID,
                'tx_metadata.callback_data.claim_id': claimID,
                'tx_metadata.function_name': 'transfer',
                'tx_metadata.function_args': tx_metadata.function_args,
                status: 'QUEUED'
            }
        );
        if (txQueueCount > 0) {
            logger.error("A duplicate transaction was detected : " + JSON.stringify(tx_metadata))
            return true;
        }

        let txCount = await mongooseTransactionRepo.count({
                'tx_metadata.callback_data.user_id': userID,
                'tx_metadata.callback_data.claim_id': claimID,
                'tx_metadata.function_name': 'transfer',
                status: { $in: ['PENDING','SUCCESSFUL']}
            }
        )

        if (txCount > 0) {
            errorUtils.throwError("A duplicate transaction was detected...please try again in a few minutes", 400)
        }
        return false;
    } else {
        errorUtils.throwError("Duplicate guard function name was not found, please check the tx settings", 400)
    }
}