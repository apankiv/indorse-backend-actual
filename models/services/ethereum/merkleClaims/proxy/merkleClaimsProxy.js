const errorUtils = require('../../../error/errorUtils');
const createClaimTxExecutor = require('../txExecutors/createMerkleClaimTxExecutor');
const claimsFacade = require('../../../../../smart-contracts/services/merkleClaims/merkleClaimsContract')
const mongoContractRepo = require('../../../../../models/services/mongo/mongoose/mongooseContractsRepo')
const settings = require('../../../../../models/settings')

const merkleUtils = require('../../../../../models/services/common/merkleUtils')

exports.addMerkleRoot = async function addMerkleRoot(timeKey, rootHash,claim_ids_metadata) {
    let metadata =  {
        timestamp: timeKey,
        rootHash : rootHash,
        userId : "cron",
        claim_ids_metadata: claim_ids_metadata
    };
    await createClaimTxExecutor.execute(timeKey,rootHash,metadata); 
};

exports.getOwner = async function getOwner() {    
    await claimsFacade.getOwner();
};


exports.getRootByTimeKey = async function getRootByTimeKey(key) {
    let root = await claimsFacade.getRootByTimeKey(key);
    return root;
};

