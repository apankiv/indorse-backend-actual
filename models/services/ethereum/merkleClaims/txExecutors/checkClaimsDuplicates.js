const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')
const logger = require('../../../common/logger').getLogger();

exports.checkDuplicates = async function checkDuplicates(tx_metadata, initiatingUserId) {
    if (tx_metadata.function_name === 'addMerkleRoot') {
        let txSubmitted = await mongooseTransactionRepo.findOne({
            initiating_user_id: initiatingUserId,
            'tx_metadata.contract': tx_metadata.contract,
            'tx_metadata.function_name': tx_metadata.function_name,
            'tx_metadata.function_args': tx_metadata.function_args,
            status: { $in: ['PENDING', 'SUCCESSFUL'] }
        });

        if (txSubmitted) {
            logger.error("A duplicate transaction was detected : " + JSON.stringify(tx_metadata))
            return true;
        }
        return false;
    } else {
        errorUtils.throwError("Duplicate guard function name was not found, please check the tx settings", 400)
    }
};