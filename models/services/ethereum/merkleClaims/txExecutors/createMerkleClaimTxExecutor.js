const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const merkleClaimsContract = require('../../../../../smart-contracts/services/merkleClaims/merkleClaimsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')

exports.execute = async function execute(timeKey,rootHash,metadata) {
    errorUtils.falsyGuard(arguments);
    let claim_id_metadata = metadata.claim_ids_metadata    

    let tx_metadata = {
        contract: "MerkleClaims",
        function_name: 'addMerkleRoot',
        function_args: [timeKey,rootHash],
        callback_data : {
            claim_ids: claim_id_metadata.claim_ids ? claim_id_metadata.claim_ids :[],
            merk_claim_sig_ids: claim_id_metadata.merk_claim_sig_ids ? claim_id_metadata.merk_claim_sig_ids : [],
            merk_vote_sig_ids: claim_id_metadata.merk_vote_sig_ids ? claim_id_metadata.merk_vote_sig_ids : []                     
        }
    };

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, metadata.userId);   

    if(!dupTx){
        let result = await merkleClaimsContract.addMerkleRoot(timeKey,rootHash);
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: "cron",
            tx_metadata: tx_metadata,
            status: 'QUEUED'
        });
        return result;
    }
};