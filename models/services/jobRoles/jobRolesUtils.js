const mongooseJobRolesRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const s3Service = require('../../../models/services/blob/s3Service');

module.exports.updateIcon = async function updateIcon(jobRoleId, iconData) {
    const iconFileS3Key = `job_role_icon_${jobRoleId}`;
    await s3Service.uploadFileFromDataURL(iconFileS3Key, iconData);

    const iconUrl = s3Service.getUploadedFileUrl(iconFileS3Key);
    const updatedJobRole = await mongooseJobRolesRepo.update(
        { _id: jobRoleId },
        { iconUrl },
    );
    return updatedJobRole;
};

module.exports.upsertJobRole = async function upsertJobRole({ form }) {
    const { prettyId, iconData, ...rawData } = form;

    let jobRole = await mongooseJobRolesRepo.upsert({ prettyId }, rawData);

    if (iconData) {
        jobRole = await this.updateIcon(jobRole._id, iconData);
    }

    return jobRole;
};
