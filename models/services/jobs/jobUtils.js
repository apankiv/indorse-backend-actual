
//Needed for sorting
//Traditional mongoose API
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');

const mongooseJobsRepo = require('../mongo/mongoose/mongooseJobsRepo')
const mongooseCompanyRepo = require('../mongo/mongoose/mongooseCompanyRepo');
const mongooseSkillRepo = require('../mongo/mongoose/mongooseSkillRepo');
const mongooseUserJobRepo = require('../mongo/mongoose/mongooseUserJobsRepo');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo')
const mongooseUserJobsRepo = require('../mongo/mongoose/mongooseUserJobsRepo'); // TODO: duplicate, need to be removed
const mongooseVotingRoundRepo = require('../mongo/mongoose/mongooseVotingRoundRepo');
const logger = require('../../../models/services/common/logger').getLogger();
const errorUtils = require('../error/errorUtils');
const settings = require('../../settings');
const s3Service = require('../blob/s3Service');
const ipfsService = require('../blob/ipfsService');
const config = require('config');
const ObjectID = require('mongodb').ObjectID;
const stringUtils = require('../blob/stringUtils');

let searchLevels = ['expert', 'intermediate', 'beginner'];

module.exports.addSkillsToJob = async function addSkillsToJob(form, jobToCreate) {
    let skillsToCreate = [];
    for(let i = 0; i < form.skills.length; i++) {
        const skillForm = form.skills[i];
        const skillFromMongo = await mongooseSkillRepo.findOneById(skillForm.id);

        if (!skillFromMongo) {
            errorUtils.throwError("Skill was not found", 400);
        }

        let skillToCreate = {
            id: skillForm.id,
            name: skillFromMongo.name,
            level: skillForm.level,
            required: true
        };
        skillsToCreate.push(skillToCreate);
    }
    jobToCreate.skills = skillsToCreate;
}

let formatCompanyFromMongo = (companyForm, companyFromMongo)=> {
    let id, name
    id = companyFromMongo._id.toString();    
    name = companyFromMongo.company_name;
    return [id, name];
}

module.exports.addCompanyToJob = async function addCompanyToJob(form, jobToCreate) {
    const companyForm = form.company;
    let companyToCreate;
    let id, name;
    if (companyForm.id) {
        const companyFromMongo = await mongooseCompanyRepo.findOneById(form.company.id);

        if (!companyFromMongo) {
            errorUtils.throwError("Company was not found", 400);
        }

        [id, name] = formatCompanyFromMongo(companyForm, companyFromMongo);
    } else {
        const companyFromMongo = await mongooseCompanyRepo.findOne({
            company_name:
                {$regex: '^' + companyForm.name + '$', $options: 'i'}
        });

        if (companyFromMongo) {
            [id, name] = formatCompanyFromMongo(companyForm, companyFromMongo);
        } else {
            let companyNameToInsert = {
                company_name: companyForm.name
            };

            const company_id = await mongooseCompanyRepo.insert(companyNameToInsert);
            id = company_id.toString();
            name = companyForm.name;
        }
    }

    companyToCreate = {
        id: id,
        name: name,
        description: companyForm.description
    };

    await updateImage(form, companyToCreate);
    jobToCreate.company = companyToCreate;
}

module.exports.prepareJobObject =async function prepareJobObject(job,userId){   
    //Needed as a guard for create , when id is already populated and it might not
    //have a _id attribute common to all mongo objects
    if (!job.id) {
        job.id = job._id.toString();
    }

    let applicationStatus = 'user_pending_validations'
    let user;
    if(userId){
        user = await mongooseUserRepo.findOneById(userId);
        if(!user){
            errorUtils.throwError("User not found",404)
        }
        applicationStatus =await this.getApplicationStatus(user,job);
        let userStartedApplication = await mongooseUserJobsRepo.findOne({ user: user._id.toString(), job: job._id.toString()});
        if (userStartedApplication)
            job.applicationStarted = true;
        else
            job.applicationStarted = false;

    }

    job.applicationStatus = applicationStatus;

    //Get company details //TODO error handling
    let company = await mongooseCompanyRepo.findOneById(job.company.id);
    job.company.name = company.company_name;
    job.company.pretty_id = company.pretty_id;

    //Get skill details 
    for (skill of job.skills) {
        let mongoSkill = await mongooseSkillRepo.findOneById(skill.id);
        skill.name = mongoSkill.name;
        skill.category = mongoSkill.category;  
        if(user) {
            [claimValidated,claimInProgress, quizbot] = this.getHighestValidationLevelForSkill(user, skill.id)
            skill.userStatus = {}
            skill.userStatus.quizbot = quizbot
            skill.userStatus.claimValidated = claimValidated
            skill.userStatus.claimInProgress = claimInProgress
        }
    }
}
/*
    This function matches the user skill to the required skill level
*/
function matchUserSkillLevel(userSkill,requiredSkillId,requiredSkillLevelArr){
    //Check if the required skill is same as we are iterating over
    //Check if the required skill is same as we are iterating over
    if (userSkill.skill._id.toString() !== requiredSkillId) {
        return false;
    }

    let requiredSkillLevel = requiredSkillLevelArr[userSkill.skill._id];
    let userSkillLevel;

    if(!userSkill.validations || !userSkill.validations.length){
        logger.error("Users validation array is not present")
        return false;
    }

    //Find highest validations
    let claimValidation = findHighestValidationForType(userSkill.validations, 'claim');
    let quizbotValidation = findHighestValidationForType(userSkill.validations, 'quizbot');

    if (!claimValidation && quizbotValidation){
        userSkillLevel = quizbotValidation;
    } else if (!quizbotValidation && claimValidation){
        userSkillLevel = claimValidation;
    } else if (claimValidation && quizbotValidation){
        if (searchLevels.indexOf(claimValidation) > searchLevels.indexOf(quizbotValidation)){
            userSkillLevel = claimValidation
        } else {
            userSkillLevel = quizbotValidation
        }
    }

    //Ensure users skill level is same or better than
    if (requiredSkillLevel === "expert" && userSkillLevel=="expert"){
        return true
    } else if (requiredSkillLevel === "intermediate" && (userSkillLevel === "expert" ||
              userSkillLevel ==="intermediate")){
        return true;
    } else if (requiredSkillLevel === "beginner" && 
               ((userSkillLevel === "expert" || userSkillLevel === "intermediate" || userSkillLevel === "beginner"))){
        return true;
    }
    return false;
}

module.exports.userMeetsSkillRequirement = async function userMeetsSkillRequirement(userSkills,jobSkills){
    //A job will require atleast 1 skill
    if (!userSkills){
        return false;
    }

    var skillIds = jobSkills.map(function (e) {        
        return ObjectID(e.id);
    });

    let skillLevel = {};
    for (jobSkill of jobSkills){
        skillLevel[jobSkill.id] = jobSkill.level
    }   

    let allReqValidatedSkills = await mongooseSkillRepo.findAll( {$and :
    [
        { $or: [{ 'validation.chatbot': true }, { 'validation.aip': true }] },
        { _id: {$in : skillIds}}

    ]});

    let filteredSkills =[];
    for(reqSkill of allReqValidatedSkills){
        if(reqSkill.validation.chatbot && !reqSkill.validation.aip){
            if (skillLevel[reqSkill._id.toString()] ==='beginner'){
                filteredSkills.push(reqSkill);
            }
        } else {
            filteredSkills.push(reqSkill);
        }
    }

    for (requiredSkill of filteredSkills){
        let userIsValidated = false;
        let requiredSkillId = requiredSkill._id.toString();        

        for(userSkill of userSkills){
            //let requiredSkillLevel = skillLevel[userSkill.skill._id]; //Can be null if user skill is not a required skill
            if (matchUserSkillLevel(userSkill, requiredSkillId, skillLevel)){
                userIsValidated = true;
            }
        }
        if(!userIsValidated){
            return false;
        }            
    }   
    return true;
}

module.exports.userAppliedForJob = async function userAppliedForJob(userId, jobId) {
    let userJob = await mongooseUserJobRepo.findOne({
        $and: [{ user: userId }, { job: jobId }, { status:'user_applied'}]           
    })

    if(userJob)
        return true;
    
    return false;
}

module.exports.userStartedApplicationForJob = async function userStartedApplicationForJob(userId, jobId) {
    let userJob = await mongooseUserJobRepo.findOne({
        $and: [{ user: userId }, { job: jobId }]
    })

    if (userJob)
        return true;

    return false;
}




async function updateImage(form, companyToCreate) {
    
    if (form.company.logo && uploadsAreEnabled()) {
        let timeKey = Math.floor(Date.now() / 1000);
        let key = companyToCreate.id + timeKey;

        let bufferedData = new Buffer(form.company.logo.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        await s3Service.uploadFile(key, bufferedData);
        let hash = await ipfsService.uploadFile(bufferedData);

        companyToCreate.logo = {
            s3Url: config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + key +
            "?t=" + timeKey,
            ipfsUrl: hash
        }       
    }
}

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
}


module.exports.userIsAuthorized = function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

module.exports.prepareSortObj = function prepareSortObj(sort) {
    let field = 'submitted.at';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }

        if (order) {
            switch(field) {
                case 'experience_level':
                    field = 'experienceLevel';
                    break;
                case 'submitted_at':
                    field = 'submitted.at';
                    break;
                case 'updated_at':
                    field = 'updated.at';
                    break;
                case 'title':
                case 'location':
                    break;
                default:
                    field = 'submitted.at'
            }
        }
    }

    if (order && field) {
        return { [field]: order };
    }

    return {};
}

module.exports.prepareUserJobSortObj = function prepareSortObj(sort) {
    let field = 'startedApplicationAt';
    let order = -1; 
    
    if(sort){
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }


        switch (field) {
            case 'applied_at':
                field = 'appliedAt';
                break;
            case 'application_start_at':
                field = 'startedApplicationAt';
                break;
            default:
                field = 'startedApplicationAt'
        }    
    }

    if (order && field) {
        return { [field]: order };
    }

    return {};    
}




module.exports.searchJobs = async function searchJobs(search, skip, limit, campaignEnabled, onlyApprovedJobs, sort = {score: {$meta: "textScore"}}) {
    let sortedCursor;
    if (onlyApprovedJobs) {
        if (campaignEnabled) {
            sortedCursor = await mongooseJobsRepo.textQueryCursor({'campaign.enabled': true, 'approved.approved': true, $text: {$search: search}});
        } else {
            sortedCursor = await mongooseJobsRepo.textQueryCursor({'approved.approved': true, $text: {$search: search}});
        }
    } else {
        if (campaignEnabled) {
            sortedCursor = await mongooseJobsRepo.textQueryCursor({'campaign.enabled': true, $text: {$search: search}});

        } else {
            sortedCursor = await mongooseJobsRepo.textSearchCursor(search);
        }
    }

    const matchingJobsCount = await sortedCursor.count();
    const matchingJobs = await sortedCursor.sort(sort).skip(skip).limit(limit).toArray();
    return [matchingJobs, matchingJobsCount];
}

module.exports.userIsLoggedIn = function userIsLoggedIn(req) {
    return req.login && req.permissions;
}


module.exports.getApplicationStatus =async function getApplicationStatus(user,job){
    
    //Check if user has applied already
    let userJobApplied = await mongooseUserJobsRepo.findOne({ user : user._id.toString() , job : job._id.toString() , status : 'user_applied' });
    if (userJobApplied){
        return 'user_applied'
    }

    let userMatchSkill = await this.userMeetsSkillRequirement(user.skills, job.skills);
    if (userMatchSkill){
        return 'user_can_apply'
    }
    return 'user_pending_validations'
}
module.exports.updateUserJobApplicationStatus = async function  updateUserJobApplicationStatus(user,job,currentStatus){

    //See if skills match
    let userMatchsSkill = await this.userMeetsSkillRequirement(user.skills,job.skills);

    //User no longer matches criteria
    if (!userMatchsSkill && currentStatus ==='user_can_apply')
        await mongooseUserJobsRepo.update({user: user._id.toString(), job : job._id.toString()},{$set :{status:'user_pending_validations'}})

    //User could not apply before but can now
    if (userMatchsSkill && currentStatus !== 'user_can_apply')
        await mongooseUserJobsRepo.update({ user: user._id.toString(), job: job._id.toString() }, { $set: { status: 'user_can_apply' } })

}


module.exports.updateUserAppByUserId = async function updateUserJobsByUserId(userId){
    //Find all jobs a user has applied
    //Re-calculate their status
    try{
        let appliedUserJobs = await mongooseUserJobRepo.findAll({
            $and: [
                { user: userId },
                { status: { $ne: 'user_applied' } }
            ]
        });

        if (!appliedUserJobs.length) {
            logger.error("No user jobs found for User :" + userId)
            return;
        }

        let user = await mongooseUserRepo.findOneById(userId);
        if (!user) {
            errorUtils.throwError("User not found! " + userId,404)
        }

        for (userJob of appliedUserJobs) {
            let job = await mongooseJobsRepo.findOneById(userJob.job)
            if (!job) {
                logger.error("Job not found! Unable to update application status " + userId + " : " + userJob.job);
                continue;
            }
            await this.updateUserJobApplicationStatus(user, job, userJob.status);
        }
    }catch(err){
        logger.error("Update user application by user error!");
        logger.error(JSON.stringify(err));
    }   
}


module.exports.updateUserAppByJobId = async function updateUserJobsByJobId(jobId){
    //Find all users who have not yet applied
    //Re-calculate their status
    try{
        let appliedUserJobs = await mongooseUserJobRepo.findAll({
            $and: [
                { job: jobId },
                { status: { $ne: 'user_applied' } }
            ]
        });

        if (!appliedUserJobs.length)
            return;

        let job = await mongooseJobsRepo.findOneById(jobId);
        if (!job) {
            errorUtils.throwError("Job not found! Unable to update application status " + jobId,404)
        }

        for (userJob of appliedUserJobs) {
            let user = await mongooseUserRepo.findOneById(userJob.user)
            if (!user) {
                logger.error("User not found! Unable to update application status " + userJob.user + " : " + jobId);
                continue;
            }
            await this.updateUserJobApplicationStatus(user, job, userJob.status);
        }  
    }catch(err){
        logger.error("Update user application by Job error!");
        logger.error(JSON.stringify(err));        
    }
}


function matchValidaitonForSkill(validations, type,searchLevel){    
    for (validation of validations) {
        if (validation.level === searchLevel) {
            if((validation.type === type) && (validation.validated === true)){
                return true;
            }              
        }
    }
    return false;
}


function findHighestValidationForType(validations,type){    
    
    for(searchLevel of searchLevels){
        if(matchValidaitonForSkill(validations, type, searchLevel))
            return searchLevel;          
    }
    return 
}


async function isClaimInProgress(user, validations, searchLevel, skillId, skillName){
    for (validation of validations) {
        if (validation.level === searchLevel && validation.type === 'claim' && validation.validated === false) {
            let existingClaimsSameLevel = await mongooseClaimsRepo.findAll({
                $and: [
                    { title: skillName[0].toUpperCase() + skillName.substr(1) },
                    { level: searchLevel },
                    { ownerid: user._id }]
            });

            for (existingClaimSameLevel of existingClaimsSameLevel){
                if (!existingClaimSameLevel.approved && !existingClaimSameLevel.disapproved && !existingClaimSameLevel.sc_claim_id) {
                    return true;
                }

                let existingVotingRound = await mongooseVotingRoundRepo.findOne({
                    $and: [
                        { claim_id: existingClaimSameLevel._id },
                        { status: 'in_progress' }]
                });
                if (existingVotingRound) {
                    return true;
                }
            }            
        }
    }
    return false;    
}


async function findHigestClaimInProgressForSkill(user,validations, skillId) {
    let result;
    //Find all expert claim validations
    let skill  = await mongooseSkillRepo.findOneById(skillId)

    if(!skill){
        errorUtils.throwError("Unable to find skill", 404)
    }   

    for (level of searchLevels){
        result =await isClaimInProgress(user, validations, level, skillId, skill.name);
        if (result)
            return level;
    }
    return
}

module.exports.getHighestValidationLevelForSkill = function getHighestValidationLevelForSkill(user,skillId){
    let userSkills = user.skills;
    let claimLevelValidated, claimLevelInProgress, quizbotLevel;

    if (!userSkills){
        //Null value check
        return [claimLevelValidated, claimLevelInProgress, quizbotLevel]
    }
  
    //To return chatbot level & AIP Level
    for(userSkill of userSkills){       
        if (!userSkill.validations || !userSkill.validations.length)
            continue;

        if (userSkill.skill._id.toString() === skillId){
            claimLevelValidated = findHighestValidationForType(userSkill.validations,'claim');
            quizbotLevel = findHighestValidationForType(userSkill.validations, 'quizbot');
            claimLevelInProgress = findHigestClaimInProgressForSkill(user, userSkill.validations, skillId);
            return [claimLevelValidated,claimLevelInProgress,quizbotLevel]
        }         
    }    
    return [claimLevelValidated, claimLevelInProgress, quizbotLevel]    
}
