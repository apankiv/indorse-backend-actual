let db;

(async function () {
    db = await require("./mongoDB").getDB()
})();

module.exports = function init(collection) {

    async function getCollection() {
        return await db.collection(collection);
    }

    return {

        /**
         * //TODO throw exception if more than one is returned?
         * @param queryObj
         * @returns {Promise.<*>}
         */
        findOne: async function findOne(queryObj) {

            let collection = await getCollection();

            return await collection.findOne(queryObj);
        },

        /**
         *
         * @param queryObj
         * @returns {Promise.<*>}
         */
        findAll: async function findAll(queryObj) {

            let collection = await getCollection();

            return await collection.find(queryObj).toArray();
        },

        /**
         *
         * @param queryObj
         * @returns {Promise.<*>}
         */
        find: async function find() {

            let collection = await getCollection();

            return await collection.find();
        },

        /**
         *
         * @param queryObj1
         * @param queryObj2
         * @returns {Promise.<*>}
         */
        findAllWithCursor: async function findAllWithCursor(queryObj1, queryObj2) {

            let collection = await getCollection();

            return await collection.find(queryObj1, queryObj2);
        },

        /**
         *
         * @param mongo object
         * @returns {Promise.<void>}
         */
        insert: async function insert(document) {

            let collection = await getCollection();

            return await collection.insert(document, {safe: true});
        },

        /**
         *
         * @param selector
         * @returns {Promise.<void>}
         */
        deleteOne: async function deleteOne(selector) {

            let collection = await getCollection();

            return await collection.deleteOne(selector, {safe: true});
        },

        /**
         *
         * @param selector
         * @param updateObj
         * @returns {Promise.<void>}
         */
        update: async function update(selector, updateObj) {

            let collection = await getCollection();

            await collection.update(selector, updateObj, {safe: true});
        },

        /**
         *
         * @param selector
         * @returns {Promise.<void>}
         */
        deleteOne: async function deleteOne(selector) {

            let collection = await getCollection();

            await collection.remove(selector, true);
        },

        /**
         *
         * @returns {Promise.<Collection Size>}
         */
        count: async function count() {

            let collection = await getCollection();

            return await collection.count();
        },

        /**
         *
         * @returns {Promise.<*>}
         */
        countVerifiedUsers: async function countVerifiedUsers(){

            let collection = await getCollection();

            return await collection.count({verified:true});

        },

        /**
         *
         * @returns {Promise.<Collection Size>}
         */
        countWithQuery: async function countWithQuery(queryObj) {

            let collection = await getCollection();

            return await collection.count(queryObj);
        }
    }
};

