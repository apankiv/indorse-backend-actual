let mongoose = require('./mongooseDB');
let schema = require('./schemas/assignment').schema;

let Aggregation = mongoose.model('aggregations', schema);

module.exports.insert = async function insert(data) {
    let doc = new Aggregation(data);
    await doc.save();
    return doc._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
    return await Aggregation.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Aggregation.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Aggregation.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Aggregation.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Aggregation.findOneAndUpdate(selector, updateObj);
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Aggregation.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Aggregation.find(selector).remove();
};
