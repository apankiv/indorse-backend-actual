const mongoose = require('./mongooseDB');
const assessmentMagicLinksSchema = require('./schemas/assessmentMagicLink').schema;
const timestampService = require('../../../services/common/timestampService');

const AssessmentMagicLink = mongoose.model('assessmentmagiclinks', assessmentMagicLinksSchema);

module.exports.insert = async function insert(assessmentMagicLinksData) {
    const assessmentMagicLink = new AssessmentMagicLink(assessmentMagicLinksData);
    await assessmentMagicLink.save();
    return assessmentMagicLink._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return AssessmentMagicLink.findById(id).lean();
};

module.exports.findOneByPublicId = async function findOneByPublicId(publicId) {
    return AssessmentMagicLink.findOne({ publicId }).lean();
};

module.exports.findOne = async function findOne(selector) {
    return AssessmentMagicLink.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return AssessmentMagicLink.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await AssessmentMagicLink.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return AssessmentMagicLink.findOne(selector).remove();
};

module.exports.softDelete = async function softDelete(selector, deletedBy) {
    const updateObj = {
        deletedBy,
        deletedAt: timestampService.createTimestamp(),
        deleted: true,
    };
    return AssessmentMagicLink.findOneAndUpdate(selector, updateObj);
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return AssessmentMagicLink.find(selector).cursor();
};
