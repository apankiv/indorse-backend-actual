let mongoose = require('./mongooseDB');
let schema = require('./schemas/assignment').schema;

let Assignment = mongoose.model('assignments', schema);

module.exports.insert = async function insert(data) {
    let doc = new Assignment(data);
    await doc.save();
    return doc._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
    return await Assignment.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Assignment.findById(id).lean();
};

module.exports.findOneWithJobRole = async function findOneWithJobRole(selector) {
    return await Assignment.findOne(selector).populate({ path: 'jobRole' });
};

module.exports.findAll = async function findAll(selector) {
    return await Assignment.find(selector).lean();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await Assignment.find(selector).cursor();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Assignment.find(selector);
};

module.exports.update = function update(selector, updateObj) {
    return Assignment.findOneAndUpdate(selector, updateObj, { new: true });
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Assignment.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Assignment.find(selector).remove();
};

module.exports.getModel = () => Assignment;
