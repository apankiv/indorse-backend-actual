const mongoose = require('./mongooseDB');
const blacklistSchema = require('./schemas/blacklist').schema;
const errorUtils = require('../../error/errorUtils');

const Blacklist = mongoose.model('blacklist', blacklistSchema);

module.exports.incIpCounter = async function incIpCounter(ip, group, inc = 1) {
    if (!ip || !group) {
        errorUtils.throwError(`ip, group cannot be empty, ip = ${ip}, group = ${group}`);
    }
    return await Blacklist.findOneAndUpdate(
        { ip, group },
        {
            $inc: { request_counter: inc },
            updated_at: new Date(),
            $setOnInsert: {
                created_at: new Date(),
            },
        },
        { upsert: true, new: true },
    );
};

module.exports.model = Blacklist;

