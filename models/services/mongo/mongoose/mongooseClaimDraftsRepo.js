let mongoose = require('./mongooseDB');
let claimsDraftsSchema = require('./schemas/claimDrafts').schema;
let timestampService = require('../../common/timestampService');
let ClaimDraft = mongoose.model('claim-drafts', claimsDraftsSchema);

module.exports.insert = async function insert(claimsData) {
    let claim = new ClaimDraft(claimsData);
    await claim.save();
    return claim._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await ClaimDraft.findById(id).lean();
};

module.exports.findOneByEmail = async function findOneByEmail(email) {
    return await ClaimDraft.findOne({email : email}).lean();
};

module.exports.findOneByToken = async function findOneByToken(token) {
    return await ClaimDraft.findOne({token : token}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await ClaimDraft.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await ClaimDraft.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await ClaimDraft.findOneAndUpdate(selector, updateObj);
};

module.exports.updateClaimID = async function updateClaimID(mongoClaimID, scClaimId) {
    await ClaimDraft.update({_id: mongoClaimID}, {sc_claim_id: scClaimId});
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await ClaimDraft.findOne(selector).remove();
};

module.exports.markAsFinalized = async function markAsFinalized(email) {
    return await ClaimDraft.update({email : email}, {finalized: true,
        finalized_at : timestampService.createTimestamp()});
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await ClaimDraft.find(selector).cursor();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await ClaimDraft.deleteMany(selector);
};
