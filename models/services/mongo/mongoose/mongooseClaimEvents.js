let mongoose = require('./mongooseDB');
let claimEventSchema = require('./schemas/claimEvents').schema;
let ClaimEvent = mongoose.model('claimevents', claimEventSchema);

module.exports.insert = async function insert(claimEventData) {
    let claimEvent = new ClaimEvent(claimEventData);
    await claimEvent.save();
    return claimEvent._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await ClaimEvent.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await ClaimEvent.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await ClaimEvent.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await ClaimEvent.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await ClaimEvent.find(selector).remove();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await ClaimEvent.deleteMany(selector);
};