let mongoose = require('./mongooseDB');
let claimGithubSchema = require('./schemas/claimGithub').schemaType;
let ClaimGithub = mongoose.model('claimgithubs', claimGithubSchema);

module.exports.insert = async function insert(doc) {
    let claimGithubDoc = new ClaimGithub(doc);
    await claimGithubDoc.save();
    return claimGithubDoc._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
    return await ClaimGithub.findOne(selector).lean();
};

module.exports.findOneByClaimId = async function findOneByClaimId(claim_id) {
    return await ClaimGithub.findOne({claim_id: claim_id}).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await ClaimGithub.find(selector).lean();
};


module.exports.deleteOne = async function deleteOne(selector) {
    await ClaimGithub.find(selector).remove();
};
