let mongoose = require('./mongooseDB');
let claimsQueueSchema = require('./schemas/claimQueue').schema;
let timestampService = require('../../common/timestampService');
let ClaimQueue = mongoose.model('claimqueues', claimsQueueSchema);

module.exports.insert = async function insert(claimsData) {
    let claim = new ClaimQueue(claimsData);
    await claim.save();
    return claim._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await ClaimQueue.findById(id).lean();
};

module.exports.findOneByEmail = async function findOneByEmail(email) {
    return await ClaimQueue.findOne({email : email}).lean();
};

module.exports.findOneByToken = async function findOneByToken(token) {
    return await ClaimQueue.findOne({token : token}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await ClaimQueue.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await ClaimQueue.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await ClaimQueue.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await ClaimQueue.findOne(selector).remove();
};

module.exports.getLatestApprovedBatch = async function getLatestApprovedBatch(batchSize) {
    return await ClaimQueue.find({released : false}).sort( { approved_timestamp: 1 } ).limit(batchSize)
};

module.exports.markAsFinalized = async function markAsFinalized(email) {
    return await ClaimQueue.update({email : email}, {finalized: true,
        finalized_at : timestampService.createTimestamp()});
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await ClaimQueue.find(selector).cursor();
};