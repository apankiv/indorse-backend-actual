/* eslint-disable */
let mongoose = require('./mongooseDB');
let claimSignatureSchema = require('./schemas/claimSignatures').schema;
let ClaimSignature = mongoose.model('claimsignatures', claimSignatureSchema);

module.exports.insert = async function insert(claimSigData) {
    let claimSig = new ClaimSignature(claimSigData);
    await claimSig.save();
    return claimSig._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await ClaimSignature.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await ClaimSignature.findOne(selector).lean();
};

module.exports.deleteOne = async function deleteOne(selector) {
    await ClaimSignature.deleteOne(selector);
};

module.exports.findAll = async function findAll(selector) {
    return await ClaimSignature.find(selector).lean();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await ClaimSignature.find(selector).cursor();
};

module.exports.updateMany = async function updateMany(selector, updateObj) {
    await ClaimSignature.updateMany(selector, updateObj);
};
