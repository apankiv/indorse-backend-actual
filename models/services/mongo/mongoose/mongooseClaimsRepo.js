const mongoose = require('./mongooseDB');
const timestampService = require('../../common/timestampService');
const claimsSchema = require('./schemas/claims').schema;

const Claim = mongoose.model('claims', claimsSchema);

module.exports.insert = async function insert(claimsData) {
    const claim = new Claim(claimsData);
    await claim.save();
    return claim._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Claim.findById(id).lean();
};

module.exports.findByOwnerId = async function findByOwnerId(ownerID) {
    return await Claim.find({ ownerid: ownerID.toString() }).lean();
};

module.exports.findOneByTitleAndOwnerId = async function findOneByTitleAndOwnerId(title, ownerID) {
    return await Claim.findOne({ $and: [{ title }, { ownerid: ownerID }] }).lean();
};

module.exports.findOneByTitleLevelAndOwnerId = async function findOneByTitleLevelAndOwnerId(title, level, ownerID) {
    return await Claim.findOne({ $and: [{ title }, { level }, { ownerid: ownerID }] }).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Claim.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Claim.find(selector).lean();
};

module.exports.getModel = () => Claim;

module.exports.update = async function update(selector, updateObj) {
    await Claim.findOneAndUpdate(selector, updateObj);
};

module.exports.updateClaimID = async function updateClaimID(mongoClaimID, scClaimId) {
    await Claim.update({ _id: mongoClaimID }, { sc_claim_id: scClaimId });
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await Claim.findOne(selector).remove();
};

module.exports.approveClaim = async function approveClaim(mongoClaimID, username, claimTier) {
    let approved = timestampService.createTimestamp();
    return await Claim.update({_id: mongoClaimID}, {
        approved: approved,
        approved_at: approved,
        approved_by: username,
        validatorAssignedBytier: claimTier
    });
};

module.exports.disapproveClaim = async function disapproveClaim(mongoClaimID, disapproveReason) {
    return await Claim.update({ _id: mongoClaimID }, {
        disapproved: timestampService.createTimestamp(),
        disapprove_reason: disapproveReason,
    });
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await Claim.find(selector).cursor();
};

module.exports.updateMany = async function updateMany(selector, updateObj) {
    await Claim.updateMany(selector, updateObj);
};
