const mongoose = require('./mongooseDB');
const clientAppsSchema = require('./schemas/clientApp').schema;
const timestampService = require('../../common/timestampService');

const ClientApp = mongoose.model('clientapps', clientAppsSchema);

module.exports.insert = async function insert(clientAppsData) {
    const clientApp = new ClientApp(clientAppsData);
    await clientApp.save();
    return clientApp._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return ClientApp.findById(id).lean();
};

module.exports.findOneByClientId = async function findOneByClientId(client_id) {
    return ClientApp.findOne({ client_id }).lean();
};

module.exports.findOne = async function findOne(selector) {
    return ClientApp.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return ClientApp.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await ClientApp.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return ClientApp.findOne(selector).remove();
};

module.exports.softDelete = async function softDelete(selector, deletedBy) {
    const updateObj = {
        deletedBy,
        deletedAt: timestampService.createTimestamp(),
        deleted: true,
    };
    return ClientApp.findOneAndUpdate(selector, updateObj);
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return ClientApp.find(selector).cursor();
};
