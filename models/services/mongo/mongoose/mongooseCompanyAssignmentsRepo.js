const mongoose = require('./mongooseDB');
const companyAssignmentsSchema = require('./schemas/companyAssignments').schema;
const timestampService = require('../../../services/common/timestampService');

const CompanyAssignments = mongoose.model('companyassignments', companyAssignmentsSchema);

const _service = {};
_service.findOne = async function findOne(criteria, projection = {}) {
    return await CompanyAssignments.findOne(criteria, projection);
};

_service.linkAssignment = async function linkAssignment(companyId, assignmentId, linkedBy) {
    const criteria = { companyId };
    let companyAssignmentsObject = await CompanyAssignments.findOne(criteria);
    if (!companyAssignmentsObject) {
        const newObject = {
            companyId,
            linkedAssignments: [],
        };
        companyAssignmentsObject = new CompanyAssignments(newObject);
    }
    if (!Array.isArray(companyAssignmentsObject.linkedAssignments)) {
        companyAssignmentsObject.linkedAssignments = [];
    }
    const linkedAssignment = companyAssignmentsObject.linkedAssignments.find(elem => elem.assignmentId.toString() === assignmentId.toString());
    if (linkedAssignment) {
        return companyAssignmentsObject.toObject();
    }
    const linkedAt = timestampService.createTimestamp();
    const objectToPush = {
        assignmentId,
        linkedAt,
        linkedBy,
    };
    companyAssignmentsObject.linkedAssignments.push(objectToPush);
    companyAssignmentsObject.updatedBy = linkedBy;
    companyAssignmentsObject.updatedAt = linkedAt;
    const objectToReturn = await companyAssignmentsObject.save();
    return objectToReturn.toObject();
};

_service.unlinkAssignment = async function unlinkAssignment(companyId, assignmentId, unlinkedBy) {
    const criteria = { companyId };
    let companyAssignmentsObject = await CompanyAssignments.findOne(criteria);
    if (!companyAssignmentsObject) {
        const newObject = {
            companyId,
            linkedAssignments: [],
        };
        companyAssignmentsObject = new CompanyAssignments(newObject);
    }
    if (!Array.isArray(companyAssignmentsObject.linkedAssignments)) {
        companyAssignmentsObject.linkedAssignments = [];
    }
    const unlinkedAt = timestampService.createTimestamp();
    companyAssignmentsObject.linkedAssignments = companyAssignmentsObject.linkedAssignments.filter(elem => elem.assignmentId.toString() !== assignmentId.toString());
    companyAssignmentsObject.updatedBy = unlinkedBy;
    companyAssignmentsObject.updatedAt = unlinkedAt;
    const objectToReturn = await companyAssignmentsObject.save();
    return objectToReturn.toObject();
};

module.exports = _service;
