let mongoose = require('./mongooseDB');
let companySchema = require('./schemas/company').schema;
let Company = mongoose.model('company_names', companySchema);
let logger = require('../../../../models/services/common/logger').getLogger();

module.exports.insert = async function insert(companyData) {
    let company = new Company(companyData);

    await company.save();

    return company._doc._id.toString();
};

module.exports.findOneByPrettyId = async function findOneByPrettyId(pretty_id) {
    return await Company.findOne({pretty_id: pretty_id}).lean();
};

module.exports.findOneByEmail = async function findOneByEmail(email) {
    return await Company.findOne({email: email}).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Company.findById(id).lean();
};

module.exports.findOne = async function findOne(selector, projection = {}) {
    return await Company.findOne(selector, projection).lean();
};

module.exports.findAll = async function findAll(selector, projection = {}) {
    return await Company.find(selector, projection).lean();
};

module.exports.findAllWithProjection = async function findAllWithProjection(conditions, projection) {
    return await Company.find(conditions).select(projection).lean();
}

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Company.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Company.findOneAndUpdate(selector, updateObj);
};

module.exports.setEntityEthaddress = async function setEntityEthaddress(companyId, entityEthaddress) {
    await Company.findOneAndUpdate({_id :companyId}, {$set :{entity_ethaddress : entityEthaddress}});
};

// _____________Advisor connections
async function getAdvisor(pretty_id, advisorId) {
    let companyToUpdate = await Company.findOne({pretty_id: pretty_id});
    let advisorToUpdate = companyToUpdate._doc.advisors.find(advisor => advisor._id.toString() === advisorId);
    return {companyToUpdate, advisorToUpdate};
}

module.exports.setRejectedTimestamp = async function setRejectedTimestamp(pretty_id, advisorId, timestamp) {
    let {companyToUpdate, advisorToUpdate} = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.rejected_timestamp = timestamp;
    await companyToUpdate.save();
};

module.exports.setAcceptedTimestamp = async function setAcceptedTimestamp(pretty_id, advisorId, timestamp) {
    let {companyToUpdate, advisorToUpdate} = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.accepted_timestamp = timestamp;
    await companyToUpdate.save();
};

module.exports.setVerificationTimestamp = async function setVerificationTimestamp(pretty_id, advisorId, timestamp) {
    let {companyToUpdate, advisorToUpdate} = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.verification_timestamp = timestamp;
    await companyToUpdate.save();
};

module.exports.setVerificationTimestampAndUserTxHash = async function setVerificationTimestampAndUserTxHash(pretty_id, advisorId, timestamp, tx_hash) {
    let {companyToUpdate, advisorToUpdate} = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.verification_timestamp = timestamp;
    advisorToUpdate.user_tx_hash = tx_hash;
    await companyToUpdate.save();
};

module.exports.setUserTxHash = async function setUserTxHash(pretty_id, advisorId, tx_hash) {
    let {companyToUpdate, advisorToUpdate} = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.user_tx_hash = tx_hash;
    await companyToUpdate.save();
};

module.exports.setCompanyAdvisorTxHash  = async function setCompanyAdvisorTxHash(companyId, userId, tx_hash) {
    let companyToUpdate = await Company.findById(companyId);
    let advisorToUpdate = companyToUpdate._doc.advisors.find(advisor => advisor.user_id === userId);
    advisorToUpdate.company_tx_hash = tx_hash;
    await companyToUpdate.save();
};

module.exports.setCompanyAdvisorDetails = async function setCompanyAdvisorDetails(pretty_id, advisorId, imgUrl,name) {
    let { companyToUpdate, advisorToUpdate } = await getAdvisor(pretty_id, advisorId);
    if(imgUrl){
        advisorToUpdate.imgUrl = imgUrl;
    }
    advisorToUpdate.name = name;
    await companyToUpdate.save();
};

module.exports.setCompanyAdvisorSoftConnection = async function setCompanyAdvisorSoftConnection(pretty_id, advisorId, softConnection) {
    let { companyToUpdate, advisorToUpdate } = await getAdvisor(pretty_id, advisorId);
    advisorToUpdate.softConnection = softConnection;
    await companyToUpdate.save();
};

module.exports.removeAdvisor = async function removeAdvisor(pretty_id, advisorId, tx_hash) {
    let companyToUpdate = await Company.findOne({pretty_id: pretty_id});

    let advisorToRemove = companyToUpdate.advisors.id(advisorId);

    if(tx_hash) {
        advisorToRemove._doc.revoked_tx_hash = tx_hash;
    }

    companyToUpdate.revoked_advisors.push(advisorToRemove);

    companyToUpdate.advisors.id(advisorId).remove();

    await companyToUpdate.save();
};

module.exports.addAdvisor = async function addAdvisor(pretty_id, advisor) {
    let result = await Company.findOneAndUpdate({pretty_id: pretty_id}, {$push: {advisors: advisor}}, {new: true});

    return result._doc.advisors[result._doc.advisors.length -1]._id;
};


// __________ Team member connections
async function getTeamMember(pretty_id, teamMemberId) {
    let companyToUpdate = await Company.findOne({pretty_id: pretty_id});
    let teamMemberToUpdate = companyToUpdate._doc.team_members.find(teamMember => teamMember._id.toString() === teamMemberId);
    return {companyToUpdate, teamMemberToUpdate};
}

let teamMember = {};

teamMember.setRejectedTimestamp = async function setRejectedTimestamp(pretty_id, teamMemberId, timestamp) {
    let {companyToUpdate, teamMemberToUpdate} = await getTeamMember(pretty_id, teamMemberId);
    teamMemberToUpdate.rejected_timestamp = timestamp;
    await companyToUpdate.save();
};

teamMember.setAcceptedTimestamp = async function setAcceptedTimestamp(pretty_id, teamMemberId, timestamp) {
    let {companyToUpdate, teamMemberToUpdate} = await getTeamMember(pretty_id, teamMemberId);
    teamMemberToUpdate.accepted_timestamp = timestamp;
    await companyToUpdate.save();
};

teamMember.setVerificationTimestamp = async function setVerificationTimestamp(pretty_id, teamMemberId, timestamp) {
    let {companyToUpdate, teamMemberToUpdate} = await getTeamMember(pretty_id, teamMemberId);
    teamMemberToUpdate.verification_timestamp = timestamp;
    await companyToUpdate.save();
};

teamMember.setVerificationTimestampAndUserTxHash = async function setVerificationTimestampAndUserTxHash(pretty_id, teamMemberId, timestamp, tx_hash) {
    let {companyToUpdate, teamMemberToUpdate} = await getTeamMember(pretty_id, teamMemberId);
    teamMemberToUpdate.verification_timestamp = timestamp;
    teamMemberToUpdate.user_tx_hash = tx_hash;
    await companyToUpdate.save();
};

teamMember.setUserTxHash = async function setUserTxHash(pretty_id, teamMemberId, tx_hash) {
    let {companyToUpdate, teamMemberToUpdate} = await getTeamMember(pretty_id, teamMemberId);
    teamMemberToUpdate.user_tx_hash = tx_hash;
    await companyToUpdate.save();
};

teamMember.setCompanyTxHash = async function setCompanyTxHash(companyId, userId, tx_hash) {
    let companyToUpdate = await Company.findById(companyId);
    let teamMemberToUpdate = companyToUpdate._doc.team_members.find(teamMember => teamMember.user_id === userId);
    teamMemberToUpdate.company_tx_hash = tx_hash;
    await companyToUpdate.save();
};

teamMember.setTeamMemberDetails = async function setTeamMemberDetails(pretty_id, teamMemberId, imgUrl,name,designation ) {
    let { companyToUpdate, teamMemberToUpdate } = await getTeamMember(pretty_id, teamMemberId);
    if (imgUrl){
        teamMemberToUpdate.imgUrl = imgUrl;
    }
    if (name){
        teamMemberToUpdate.name = name;
    }
    if (designation){
        teamMemberToUpdate.designation = designation;
    }
    await companyToUpdate.save();
};


teamMember.removeTeamMember = async function removeTeamMember(pretty_id, teamMemberId, tx_hash) {
    let companyToUpdate = await Company.findOne({pretty_id: pretty_id});

    let teamMemberToRemove = companyToUpdate.team_members.id(teamMemberId);

    if(tx_hash) {
        teamMemberToRemove._doc.revoked_tx_hash = tx_hash;
    }

    companyToUpdate.revoked_team_members.push(teamMemberToRemove);

    companyToUpdate.team_members.id(teamMemberId).remove();

    await companyToUpdate.save();
};

teamMember.addTeamMember = async function addTeamMember(pretty_id, teamMember) {
    let result = await Company.findOneAndUpdate({pretty_id: pretty_id}, {$push: {team_members: teamMember}}, {new: true});

    return result._doc.team_members[result._doc.team_members.length -1]._id;
};


module.exports.teamMember = teamMember;


module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Company.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Company.find(selector).remove();
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Company.find(selector).remove();
};

module.exports.removeFromTeamMembersAndAdvisors = async function removeFromTeamMembersAndAdvisors(userId) {
    let companiesWithAdvisor = await Company.find({"advisors.user_id" : userId});

    for(let companyToModify of companiesWithAdvisor) {
        for(let advisor of companyToModify.advisors) {
            if(advisor.user_id === userId) {
                advisor.remove();
            }
        }
    await companyToModify.save();
    }


    let companiesWithTeamMember = await Company.find({"team_members.user_id" : userId});

    for(let companyToModify of companiesWithTeamMember) {
        for(let teamMember of companyToModify.team_members) {
            if(teamMember.user_id === userId) {
                teamMember.remove();
            }
        }
        await companyToModify.save();
    }
};
