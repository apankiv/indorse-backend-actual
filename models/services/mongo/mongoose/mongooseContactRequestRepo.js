const mongoose = require('./mongooseDB');
const contactRequestsSchema = require('./schemas/contactRequest').schema;
const timestampService = require('../../../services/common/timestampService');

const ContactRequest = mongoose.model('contactrequests', contactRequestsSchema);

module.exports.insert = async function insert(contactRequestsData) {
    const contactRequest = new ContactRequest(contactRequestsData);
    await contactRequest.save();
    return contactRequest._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return ContactRequest.findById(id).lean();
};

module.exports.findOneByPublicId = async function findOneByPublicId(publicId) {
    return ContactRequest.findOne({ publicId }).lean();
};

module.exports.findOne = async function findOne(selector) {
    return ContactRequest.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return ContactRequest.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await ContactRequest.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return ContactRequest.findOne(selector).remove();
};

module.exports.softDelete = async function softDelete(selector, deletedBy) {
    const updateObj = {
        deletedBy,
        deletedAt: timestampService.createTimestamp(),
        deleted: true,
    };
    return ContactRequest.findOneAndUpdate(selector, updateObj);
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return ContactRequest.find(selector).cursor();
};
