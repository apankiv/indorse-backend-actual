let mongoose = require('./mongooseDB');
let designationSchema = require('./schemas/designation');

let Designation = mongoose.model('designations', designationSchema);

module.exports.insert = async function insert(skillData) {
    let designation = new Designation(skillData);

    await designation.save();

    return designation._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Designation.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Designation.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Designation.find(selector).lean();
};

module.exports.findAllWithLimit = async function findAllWithLimit(selector, limit) {
    return await Designation.find(selector).limit(limit).lean();
}

module.exports.update = async function update(selector, updateObj) {
    await Designation.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Designation.find(selector).remove();
};