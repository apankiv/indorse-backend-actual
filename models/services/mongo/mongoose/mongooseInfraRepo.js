let mongoose = require('./mongooseDB');
let timestampService = require('../../common/timestampService');
let infra = require('./schemas/infra').schema;
let Infra = mongoose.model('infras', infra);

module.exports.insert = async function insert(data) {
    let doc = new Infra(data);
    await doc.save();
    return doc._doc._id.toString();
};

module.exports.findOneByName = async function findOneByName(name) {
    return await Infra.findOne({name : name}).lean();
};

module.exports.updateHeartbeat = async function update(name) {
    await Infra.findOneAndUpdate({name : name},
        {last_seen_timestamp : timestampService.createTimestamp()});
};