const mongoose = require('./mongooseDB');
const invitesSchema = require('./schemas/invite').schema;

const Invite = mongoose.model('invites', invitesSchema);

module.exports.insert = async function insert(invitesData) {
    const invite = new Invite(invitesData);
    await invite.save();
    return invite._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return Invite.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return Invite.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return Invite.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Invite.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return Invite.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return Invite.find(selector).cursor();
};
