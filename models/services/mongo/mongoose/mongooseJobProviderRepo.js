let mongoose = require('./mongooseDB');
let schema = require('./schemas/jobProvider').schema;

let JobProvider = mongoose.model('jobproviders', schema);

module.exports.insert = async function insert(data) {
    let doc = new JobProvider(data);
    await doc.save();
    return doc._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
    return await JobProvider.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await JobProvider.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await JobProvider.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await JobProvider.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await JobProvider.findOneAndUpdate(selector, updateObj);
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await JobProvider.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await JobProvider.find(selector).remove();
};