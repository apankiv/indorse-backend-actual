let mongoose = require("./mongooseDB");
let schema = require("./schemas/jobRole").schema;

let JobRole = mongoose.model("jobroles", schema);

module.exports.insert = async function insert(data) {
  let doc = new JobRole(data);
  await doc.save();
  return doc._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
  return await JobRole.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
  return await JobRole.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
  return await JobRole.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
  return await JobRole.find(selector);
};

module.exports.update = function update(selector, updateObj) {
  return JobRole.findOneAndUpdate(selector, updateObj, { new: true });
};

module.exports.upsert = function upsert(selector, updateObj) {
  return JobRole.findOneAndUpdate(selector, updateObj, {
    new: true,
    upsert: true,
  });
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
  await JobRole.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
  await JobRole.find(selector).remove();
};
