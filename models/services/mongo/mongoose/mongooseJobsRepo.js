let mongoose = require('./mongooseDB');
let jobsSchema = require('./schemas/job').schema;
let Job = mongoose.model('jobs', jobsSchema);

module.exports.insert = async function insert(jobsData) {
    let job = new Job(jobsData);
    await job.save();
    return job._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Job.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Job.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Job.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Job.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await Job.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await Job.find(selector).cursor();
};

module.exports.textSearchCursor = async function textSearchCursor(text) {
    const jobsCursor = await Job.find({$text: {$search: text}}, {score: {$meta: "textScore"}}).cursor();
    await Job.findOne({});
    return jobsCursor.cursor;
};

module.exports.textQueryCursor = async function textQueryCursor(query) {
    const jobsCursor = await Job.find(query, {score: {$meta: "textScore"}}).cursor();
    await Job.findOne({});
    return jobsCursor.cursor;
};

module.exports.findOneAndUpdatePosting = async function findOneAndUpdatePosting(selector,job) {
    let jobToUpdate = await Job.findOne(selector);  

    jobToUpdate.title = job.title;    
    jobToUpdate.experienceLevel = job.experienceLevel;
    jobToUpdate.location = job.location;
    jobToUpdate.description = job.description;
    jobToUpdate.monthlySalary = job.monthlySalary;    
    jobToUpdate.company.id = job.company.id;
    jobToUpdate.company.description = job.company.description;
    if (job.company.logo)
        jobToUpdate.company.logo = job.company.logo
    
    if(job.campaign)
        jobToUpdate.campaign = job.campaign;
       
    jobToUpdate.updated = job.updated;    
    jobToUpdate.contactEmail = job.contactEmail;
    jobToUpdate.applicationLink = job.applicationLink;
    jobToUpdate.skills = job.skills;
    await jobToUpdate.save();
};