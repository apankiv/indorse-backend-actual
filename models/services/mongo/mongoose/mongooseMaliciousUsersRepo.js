const mongoose = require('./mongooseDB');
const maliciousUsersSchema = require('./schemas/maliciousUsers').schema;
const errorUtils = require('../../error/errorUtils');
const slackService = require('../../common/slackService');

const MaliciousUsers = mongoose.model('malicious_users', maliciousUsersSchema);
const NUMBER_OF_WRITES_ALLOWED_PER_MIN = 100;
const NUMBER_OF_ATTACKS_LIMIT = 10;
const MILLIS_IN_A_DAY = 86400000;
const MILLIS_IN_A_MINUTE = 60000;

let numberOfWritesHappenedInLastMinute = 0;
let attackCounter = 0;
let bufferedAlerts = [];

setInterval(() => {
    if (numberOfWritesHappenedInLastMinute >= NUMBER_OF_WRITES_ALLOWED_PER_MIN) {
        attackCounter += 1;
    }
    numberOfWritesHappenedInLastMinute = 0;
}, MILLIS_IN_A_MINUTE);

setInterval(() => {
    // reset attack counter every day
    attackCounter = 0;
}, MILLIS_IN_A_DAY);

setInterval(() => {
    if (!bufferedAlerts.length) {
        return;
    }
    const firstTenAlerts = bufferedAlerts.splice(0, 10);
    bufferedAlerts = [];
    firstTenAlerts.forEach(alert => slackService.reportAlert(alert));
}, MILLIS_IN_A_MINUTE);

function isUnderAttack() {
    return attackCounter >= NUMBER_OF_ATTACKS_LIMIT;
}

function sendSlackAlert(alert) {
    if (isUnderAttack()) {
        console.log('UnderAttack');
        return bufferedAlerts.push(alert);
    }
    return slackService.reportAlert(alert);
}

module.exports.logMaliciousUser = async function logMaliciousUser(ip, url, data = null) {
    if (numberOfWritesHappenedInLastMinute >= NUMBER_OF_WRITES_ALLOWED_PER_MIN
        || attackCounter >= NUMBER_OF_ATTACKS_LIMIT) {
        sendSlackAlert({
            title: 'please check production, possibly an attack is happening and/or has happened',
            data: { ip, url, data },
        });
        return null;
    }
    if (!ip || !url) {
        errorUtils.throwError(`ip, url cannot be empty, ip = ${ip}, url = ${url}`);
    }
    const upsertObject = {
        $inc: { request_counter: 1 },
        updated_at: new Date(),
        $setOnInsert: {
            created_at: new Date(),
        },
    };
    if (data) {
        upsertObject.data = data;
    }
    const maliciousUser = await MaliciousUsers
        .findOneAndUpdate({ ip, url }, upsertObject, { upsert: true, new: true });
    numberOfWritesHappenedInLastMinute += 1;
    return maliciousUser;
};

module.exports.model = MaliciousUsers;

