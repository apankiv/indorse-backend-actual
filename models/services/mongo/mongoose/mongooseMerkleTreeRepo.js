/* eslint-disable */
let mongoose = require('./mongooseDB');
let merkleTreeSchema = require('./schemas/merkleTree').schema;

let MerkleTree = mongoose.model('merkletrees', merkleTreeSchema);

module.exports.insert = async function insert(merkleTreeData) {
    let merkleTree = new MerkleTree(merkleTreeData);
    await merkleTree.save();
    return merkleTree._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await MerkleTree.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await MerkleTree.findOne(selector).lean();
};

module.exports.deleteOne = async function deleteOne(selector) {
    await MerkleTree.deleteOne(selector);
};

module.exports.findAll = async function findAll(selector) {
    return await MerkleTree.find(selector).lean();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await MerkleTree.find(selector).cursor();
};
