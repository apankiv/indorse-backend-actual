let mongoose = require('./mongooseDB');
let miscSkillsSchema = require('./schemas/miscSkill');

let MiscSkill = mongoose.model('miscskills', miscSkillsSchema);

module.exports.insert = async function insert(languageData) {
    let school = new MiscSkill(languageData);

    await school.save();

    return school._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await MiscSkill.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await MiscSkill.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await MiscSkill.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await MiscSkill.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await MiscSkill.find(selector).remove();
};

module.exports.insertMany = async function insertMany(docs) {
    return await MiscSkill.insertMany(docs);
};