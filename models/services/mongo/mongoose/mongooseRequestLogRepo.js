const mongoose = require('./mongooseDB');
const requestLogSchema = require('./schemas/requestLog').schema;
const timestampService = require('../../../services/common/timestampService');

const RequestLog = mongoose.model('requestlogs', requestLogSchema);

module.exports.logRequest = async function logRequest(request, direction, meta) {
    const requestObjectToSave = {
        request,
        meta,
        direction,
        requestLoggedAt: timestampService.createTimestamp(),
    };
    const requestLog = new RequestLog(requestObjectToSave);
    await requestLog.save();
    return requestLog._doc._id.toString();
};

module.exports.logResponse = async function logResponse(requestLogId, response) {
    const criteria = {
        _id: requestLogId,
    };
    const updateObj = {
        $set: {
            response: response || {},
            responseLoggedAt: timestampService.createTimestamp(),
        },
    };
    await RequestLog.findOneAndUpdate(criteria, updateObj);
};

module.exports.findOneById = async function findOneById(id) {
    return RequestLog.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return RequestLog.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return RequestLog.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await RequestLog.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return RequestLog.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return RequestLog.find(selector).cursor();
};
