let mongoose = require('./mongooseDB');
let rewardsSchema = require('./schemas/rewards').schema;
let Reward = mongoose.model('rewards', rewardsSchema);

module.exports.insert = async function insert(rewardData) {
    let reward = new Reward(rewardData);
    await reward.save();
    return reward._doc._id.toString();
};

module.exports.find = async function findAll(selector) {
    return await Reward.find(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Reward.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Reward.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Reward.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Reward.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Reward.find(selector).remove();
};

module.exports.markAsPaid = async function markAsPaid(selector,timeStamp) {
    await Reward.findOneAndUpdate(selector, { $set: { paid: true, payout_timestamp: timeStamp} });
};


module.exports.getLastRewardsTimeStamp = async function getLastRewardsTimeStamp(){
    let result = await Reward.find({}).sort({ calculation_timestamp: -1 }).limit(1);
    if(result.length){
        return result[0].calculation_timestamp
    }       
    return 0;        
}