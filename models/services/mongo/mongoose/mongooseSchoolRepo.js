let mongoose = require('./mongooseDB');
let schoolSchema = require('./schemas/school');

let School = mongoose.model('school_names', schoolSchema);

module.exports.insert = async function insert(schoolData) {
    let school = new School(schoolData);

    await school.save();

    return school._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await School.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await School.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await School.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await School.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await School.find(selector).remove();
};