let mongoose = require('./mongooseDB');
let skillSchema = require('./schemas/skill');

let Skill = mongoose.model('skills', skillSchema);

module.exports.insert = async function insert(skillData) {
    let skill = new Skill(skillData);

    await skill.save();

    return skill._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Skill.findById(id).lean();
};

module.exports.findDistinctCategories = async function findDistinctCategories() {
    return await Skill.find().distinct('category');
};

module.exports.findOne = async function findOne(selector) {
    return await Skill.findOne(selector).lean();
};
    
module.exports.findAll = async function findAll(selector, projection = {}) {
    return await Skill.find(selector, projection).lean();
};

module.exports.findAllWithProjection = async function findAllWithProjection(conditions, projection) {
    return await Skill.find(conditions).select(projection).lean();
}

module.exports.findAllWithLimit = async function findAllWithLimit(selector, limit) {
    return await Skill.find(selector).limit(limit).lean();
}

module.exports.update = function update(selector, updateObj) {
    return Skill.findOneAndUpdate(selector, updateObj, { new: true });
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Skill.find(selector).remove();
};

module.exports.getModel = () => Skill;

module.exports.hasAIPEnabled = async function hasAIPEnabled(skillName, throwError = true) {
    const lowercaseSkillName = skillName.toLowerCase();
    const criteria = {
        name: lowercaseSkillName,
    };
    const projection = {
        validation: 1,
    };
    const option = {
        lean: true,
    };
    const skill = await Skill.findOne(criteria, projection, option);
    if (!skill && throwError) {
        throw new Error('Skill not found !');
    }
    const aipEnabled = (skill && skill.validation && skill.validation.aip) || false;
    if (!aipEnabled && throwError) {
        throw new Error('Skill with aip enabled not found !');
    }
    return aipEnabled;
};
