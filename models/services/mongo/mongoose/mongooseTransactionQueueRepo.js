let mongoose = require('./mongooseDB');
let transactionQueueSchema = require('./schemas/transactionqueue');
let TransactionQueue = mongoose.model('transactionqueue', transactionQueueSchema);

module.exports.insert = async function insert(txData) {
    let transactionQueue = new TransactionQueue(txData);
    await transactionQueue.save();
    return transactionQueue._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await TransactionQueue.findById(id).lean();
};

module.exports.count = async function count(selector) {
    return await TransactionQueue.find(selector).count();
};

module.exports.findOne = async function findOne(selector) {
    return await TransactionQueue.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await TransactionQueue.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await TransactionQueue.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await TransactionQueue.find(selector).remove();
};

module.exports.countAllWithStatus = async function countAllWithStatus(status) {
    return await TransactionQueue.find({ status: status }).count();
};

module.exports.deleteAll = async function deleteAll() {
    await TransactionQueue.remove({});
};
