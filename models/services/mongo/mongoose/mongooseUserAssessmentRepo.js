const config = require('config');
const mongoose = require('./mongooseDB');
const userAssessmentsSchema = require('./schemas/userAssessment').schema;

const UserAssessment = mongoose.model('userassessments', userAssessmentsSchema);
const userAssessmentStates = require('./schemas/userAssessment').status;

const timestampService = require('../../common/timestampService');

const _service = {};

_service.insert = async function insert(userAssessmentsData) {
    const userAssessment = new UserAssessment(userAssessmentsData);
    await userAssessment.save();
    return userAssessment._doc._id.toString();
};

_service.findOneById = async function findOneById(id) {
    return UserAssessment.findById(id).lean();
};

_service.findOne = async function findOne(selector) {
    return UserAssessment.findOne(selector).lean();
};

_service.findAll = async function findAll(selector) {
    return UserAssessment.find(selector).lean();
};

_service.countAll = async function countAll(criteria) {
    const count = await UserAssessment.find(criteria).count();
    return count || 0;
};

_service.update = async function update(selector, updateObj) {
    await UserAssessment.findOneAndUpdate(selector, updateObj);
};

_service.deleteOne = async function deleteOne(selector) {
    return UserAssessment.findOne(selector).remove();
};

_service.findAllWithCursor = async function findAllWithCursor(selector) {
    return UserAssessment.find(selector).cursor();
};

// This function accepts a daysBackArr param, which accepts array of numbers
// For eg, if daysBackArr = [3, 6, 9], the reminder will be sent on 3rd, 6th and 9th day
// By default, reminder is only sent on third day
// eslint-disable-next-line max-len
const defaultValue = process.env.NODE_ENV === 'production' ? [3] : [0];
_service.getMagicLinkAssessmentsForReminder = async function getMagicLinkAssessmentsForReminder(daysBackArr = defaultValue) {
    console.log('daysBackArr: ', daysBackArr);
    const now = new Date();
    const days = daysBackArr.map((daysBack) => {
        const start = new Date(new Date(new Date().setDate(now.getDate() - daysBack)).setHours(0, 0, 0, 0));
        const end = new Date(new Date(new Date().setDate(now.getDate() - daysBack)).setHours(23, 59, 59, 999));
        return { start, end };
    });
    const filters = {
        $and: [
            { status: userAssessmentStates.ASSIGNMENTS_VIEWED },
            {
                $or: days.map(day => ({
                    assignmentsViewedAt: {
                        $gte: timestampService.timestampFromDate(day.start),
                        $lte: timestampService.timestampFromDate(day.end),
                    },
                })),
            },
        ],
    };
    return await _service.findAll(filters);
};

module.exports = _service;
