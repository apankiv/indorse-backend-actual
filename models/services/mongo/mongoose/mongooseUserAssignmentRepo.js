const logger = require('../../common/logger').getLogger();
const config = require('config');
let mongoose = require('./mongooseDB');
let schema = require('./schemas/userAssignment').schema;
let userRepo = require('./mongooseUserRepo')
let assignmentRepo = require('./mongooseAssignmentRepo');
const timestampService = require('../../common/timestampService');

const userAssignmentStates = require('./schemas/userAssignment').states;

let UserAssignment = mongoose.model('userassignments', schema);

module.exports.insert = async function insert(data) {
    let doc = new UserAssignment(data);
    await doc.save();
    return doc._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await UserAssignment.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await UserAssignment.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await UserAssignment.find(selector).lean();
};

module.exports.findOneEntity = function findOneEntity(selector) {
    return UserAssignment.findOne(selector);
};

module.exports.findOneWithSkillDetails = function findOneWithSkillDetails(selector) {
    return UserAssignment.findOne(selector).populate({ path: 'skills.skill' });
};

module.exports.update = function update(selector, updateObj) {
    return UserAssignment.findOneAndUpdate(selector, updateObj, { new: true });
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await UserAssignment.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await UserAssignment.find(selector).cursor();
};


module.exports.updateAssignmentResult = async function updateAssignmentResult(assignmentId,status,results){
    let assignment = await UserAssignment.findOne({ _id: assignmentId });
    assignment.status = status;
    for(result of results){
        let assignmentSkill = assignment.skills ? assignment.skills.find(s => s.skill.toString() === result.skill.toString()) : null
        if(assignmentSkill){
            assignmentSkill.level = result.level;
            assignmentSkill.validated = result.validated;
        }        
    }
    await assignment.save();
}

module.exports.getAssignmentsForReminder = async function getAssignmentsForReminder() {
    const date = new Date();
    const seventhDay = new Date();

    seventhDay.setDate(date.getDate() - 7);

    const seventhDayStart = seventhDay.setHours(0, 0, 0, 0);
    const seventhDayEnd = seventhDay.setHours(23, 59, 59, 999);

    const userAssignments = UserAssignment.find({
        status: { $eq: userAssignmentStates.STARTED },
        started_on: { $gte: seventhDayStart / 1000, $lt: seventhDayEnd / 1000 },
        startedForUserAssessment: { $exists: false },
    }).lean();
    return userAssignments || [];
};

module.exports.markReminderSent = async function markReminderSent(userAssignmentId) {
    const selector = {
        _id: userAssignmentId,
    };
    const updateObj = {
        $set: {
            reminderSentAt: timestampService.createTimestamp(),
        },
    };
    await UserAssignment.findOneAndUpdate(selector, updateObj, { new: true });
};

module.exports.getAssignmentsWithUsersStartedOnOrBeforeDate = async function getAssignmentsWithUsersStartedOnOrBeforeDate(tillDateInput, status = userAssignmentStates.STARTED ) {
  console.log('[getAssignmentsWithUsersStartedOnOrBeforeDate] started');
  
  let tillDate = new Date(tillDateInput);
  console.log('\n Tilldate : ', tillDate);
  
  let tillDateTimestamp = Math.floor(tillDate/ 1000);
  console.log('\n Tilldate Timestamp :', tillDateTimestamp)

  let results =  [];

  const userAssignments =  await this.findAll({ status: status, started_on: { $lte: tillDateTimestamp} });
  console.log('[getAssignmentsWithUsersStartedOnOrBeforeDate]', userAssignments.length);
  for (let i = 0; i < userAssignments.length; i++) {
    const assignmentLink =  `${config.get('server.hostname')}assignments/${userAssignments[i].assignment_id}`;
    const user = await userRepo.findOneById(userAssignments[i].owner_id);
    const assignment = await assignmentRepo.findOneWithJobRole({_id: userAssignments[i].assignment_id});
   
    if(user) {
      let skills = [];
      let jobRole = '';

      if(user.skills) {
        skills = user.skills && user.skills.filter(obj => !obj.validations || obj.validations.length == 0 || obj.validations.filter(validation => validation.validated).length <= 0 ).map(skill => {
          return skill.skill && skill.skill.name;
        });
      }
      
      if (assignment) {
        jobRole = assignment.jobRole && assignment.jobRole.title;
      }

      results.push({ 
        assignmentLink, 
        startedOn: new Date(userAssignments[i].started_on * 1000),
        username: user.username,
        displayName: user.name,
        email: user.email,
        jobRole: jobRole,
        nonValidatedSkills: skills,
      })
    }
  }
  console.log('[getAssignmentsWithUsersStartedOnOrBeforeDate] results -', results.length);
  return results;
}

module.exports.statuses = {
    ONGOING: "ongoing",
    FINISHED: "finished",
    TIMED_OUT: "timed out",
    FAILED: "failed",
    SUCCESSFUL: "successful"
};
