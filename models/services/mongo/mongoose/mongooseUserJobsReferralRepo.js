//This collection tracks all the jobs a user has referred for
let mongoose = require('./mongooseDB');
let userJobsReferralSchema = require('./schemas/userJobReferral').schema;
let UserJobReferral = mongoose.model('userJobReferrals', userJobsReferralSchema);

module.exports.insert = async function insert(referralData) {
    let userJobReferral = new UserJobReferral(referralData);
    await userJobReferral.save();
    return userJobReferral._doc._id.toString();
};

module.exports.findAllWithCursorByUserId = async function findAllWithCursorByUserId(id) {
    return await UserJobReferral.find({userId : ObjectID(id)}).cursor();
};

module.exports.findAllWithCursorByJobId = async function findAllWithCursorByJobId(id) {
    return await UserJobReferral.find({jobId : ObjectID(id)}).cursor();
};

module.exports.findAllByJobId = async function findAllByJobId(id) {
    return await UserJobReferral.find({jobId : ObjectID(id)});
};

module.exports.findOne = async function findOne(selector) {
    return await UserJobReferral.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await UserJobReferral.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await UserJobReferral.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await UserJobReferral.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await UserJobReferral.find(selector).cursor();
};

module.exports.count = async function count(selector) {
    return await UserJobReferral.find(selector).count();
};