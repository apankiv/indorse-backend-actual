/* eslint-disable */
let mongoose = require('./mongooseDB');
let userSchema = require('./schemas/user').schema;

let User = mongoose.model('users', userSchema);

module.exports.insert = async function insert(userData) {
    let user = new User(userData);

    await user.save();

    return user._doc._id.toString();
};
// email will alwyas be stored as lowercase according to Mongoose Schema
module.exports.findOneByEmail = async function findOneByEmail(email) {
    return await User.findOne({email: email.toLowerCase()}).lean();
};

module.exports.findOneByUsername = async function findOneByUsername(username) {
    return await User.findOne({username: username}).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await User.findById(id).lean();
};

module.exports.findOneEntityById = async function findOneById(id) {
    return await User.findById(id);
};

module.exports.findOne = async function findOne(selector, projection = {}) {
    if(selector.email){
        selector.email = selector.email.toLowerCase();
    }
    return await User.findOne(selector, projection).lean();
};

module.exports.findOneEntity = async function findOneEntity(selector) {
    if(selector.email){
        selector.email = selector.email.toLowerCase();
    }
    return await User.findOne(selector);
};

module.exports.findAll = async function findAll(selector, projection = {}) {
    return await User.find(selector, projection).lean();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await User.find(selector).cursor();
};

module.exports.findAllWithKeys = async function findAllWithKeys(selectorArray) {

    return await User.find({
        '_id': {
            $in:
            selectorArray
        }
    }).lean();
};

module.exports.update = async function update(selector, updateObj) {
    if(selector.email){ // If there is the email field in a selector object, lowercase the value.
        selector.email = selector.email.toLowerCase();
    }
    await User.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await User.deleteOne(selector);
};

module.exports.setCurrentWizardStep = async function setCurrentWizardStep(userId, wizardName, stepNumber) {
    let user = await User.findById(userId);

    let wizard = user._doc.wizards.find(wizard => wizard.name === wizardName);

    if (!wizard) {

        await User.update({_id: userId}, {
            $push: {
                wizards: {
                    name: wizardName,
                    current_step: stepNumber
                }
            }
        })
    } else {
        await User.update(
            {'wizards.name': wizardName, _id: userId},
            {$set: {'wizards.$.current_step': stepNumber}});
    }
};

module.exports.addSkill = async function addSkill(email, skill) {
    await User.findOneAndUpdate({email: email}, {$push: {skills: skill}});
};

let skillLevels = ['beginner', 'intermediate', 'expert'];

function shouldUpdateSkillLevel(skillLevel, validationLevel) {
    if (!skillLevel && validationLevel) return true;
    if(!skillLevel && !validationLevel) return false;

    const skillLevelIndex = skillLevels.indexOf(skillLevel);
    const validationLevelIndex = skillLevels.indexOf(validationLevel);

    return validationLevelIndex >=0 && skillLevelIndex >=0 && validationLevelIndex > skillLevelIndex;
}

module.exports.addValidationToSkill = async function addValidationToSkill(email, skillName, validation) {
    let user = await User.findOne({email: email});
    let skill = user.skills.find((s) => s.skill && s.skill.name === skillName)

    if (!skill.validations)
        skill.validations = [];
    skill.validations.push(validation);

    if (validation.validated && shouldUpdateSkillLevel(skill.level, validation.level)) {
        skill.level = validation.level;
    }
    
    await user.save();

    // await User.findOneAndUpdate({email: email, "skills.level" : skillLevel, "skills.name" : skillName},
    //     {$push: {"skills.$.validations": validation}});
};

module.exports.setAssignmentValidationResult = async function setAssignmentValidationResult(userId, validationId, validationResults) {
    let user = await User.findById(userId);
    let skill, validation;    
    for(let validationResult of validationResults){
        let validation;
        for (let i = 0; i < user.skills.length; i++) {           
            userSkill = user.skills[i];          
            if (validationResult.skill.toString() === userSkill.skill._id.toString()) {
                validation = userSkill.validations ? userSkill.validations.find(v => v.id === validationId.toString()) : null;
                validation.validated = validationResult.validated;
                validation.level = validationResult.level;
                if (validationResult.validated && shouldUpdateSkillLevel(userSkill.level, validationResult.level)) {
                    userSkill.level = validationResult.level;
                } 
            }
        }
        if(!validation){
            throw new Error("Validation not found:" + email + " " + validationId)
        }    
    }
    await user.save();
};





module.exports.setCurrentWizardFinished = async function setCurrentWizardStep(userId, wizardName) {
    let user = await User.findById(userId);

    let wizard = user._doc.wizards.find(wizard => wizard.name === wizardName);

    if (!wizard) {

        await User.update({_id: userId}, {
            $push: {wizards: {
                    name: wizardName,
                    current_step: 1,
                    isFinished : true
                }
            }
        })
    } else {
        await User.update(
            {'wizards.name': wizardName, _id: userId},
            {$set: {'wizards.$.isFinished': true}});
    }
};

module.exports.getModel = () => User;
