const mongoose = require('./mongooseDB');
const userTaskSchema = require('./schemas/userTask').schema;
const timestampService = require('../../common/timestampService');

const UserTask = mongoose.model('usertasks', userTaskSchema);

const _service = {};

_service.insert = async function insert(userTaskData) {
    const userTask = new UserTask(userTaskData);
    await userTask.save();
    return userTask._doc._id.toString();
};

_service.getUserTask = async function getUserTask(userId) {
    const criteria = {
        userId,
    };
    let userTaskFound = await UserTask.findOne(criteria);
    if (!userTaskFound) {
        const userTaskObject = { userId };
        userTaskFound = new UserTask(userTaskObject);
    }
    const userTaskSaved = await userTaskFound.save();
    return userTaskSaved.toObject();
};

_service.markTaskCompleted = async function markTaskCompleted(userId, taskType) {
    if (!userId || !taskType) return false;
    const criteria = {
        userId,
    };
    let userTaskFound = await UserTask.findOne(criteria);
    if (!userTaskFound) {
        const userTaskObject = { userId };
        userTaskFound = new UserTask(userTaskObject);
    }

    const { taskList = [] } = userTaskFound || {};
    const taskFound = taskList.find(task => task.taskType === taskType && task.completedAt);
    if (taskFound) {
        return false;
    }

    // Save the task type only if its not already complete
    const objectToInsert = {
        taskType,
        completedAt: timestampService.createTimestamp(),
    };
    taskList.push(objectToInsert);
    userTaskFound.taskList = taskList;
    const userTaskSaved = await userTaskFound.save();
    return userTaskSaved.toObject();
};

_service.markMultipleTasksCompleted = async function markMultipleTasksCompleted(userId, taskTypes = []) {
    if (!userId) return false;
    const criteria = {
        userId,
    };
    let userTaskFound = await UserTask.findOne(criteria);
    if (!userTaskFound) {
        const userTaskObject = { userId };
        userTaskFound = new UserTask(userTaskObject);
    }

    const { taskList = [] } = userTaskFound || {};
    const filteredTaskTypes = taskTypes.filter(taskType => !taskList.some(task => task.taskType === taskType));
    if (!filteredTaskTypes.length) {
        return false;
    }
    filteredTaskTypes.forEach((taskType) => {
        const objectToInsert = {
            taskType,
            completedAt: timestampService.createTimestamp(),
        };
        taskList.push(objectToInsert);
    });

    // Save the task type only if its not already complete
    userTaskFound.taskList = taskList;
    const userTaskSaved = await userTaskFound.save();
    return userTaskSaved.toObject();
};

_service.findOneById = async function findOneById(id) {
    return UserTask.findById(id).lean();
};

_service.findOne = async function findOne(selector, projection = {}, option = { lean: true }) {
    return UserTask.findOne(selector, projection, option);
};

module.exports = _service;
