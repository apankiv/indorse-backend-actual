let mongoose = require('./mongooseDB');
let validatorRank = require('./schemas/validatorRank').schema;

let ValidatorRanks = mongoose.model('validatorranks', validatorRank);

module.exports.insert = async function insert(templateData) {
    let validatorRankDoc = new ValidatorRanks(templateData);
    await validatorRankDoc.save();
    return validatorRankDoc._doc._id.toString();
};

module.exports.findRecentRank = async function findRecentRank() {
    return await ValidatorRanks.find({}).sort({"timestamp":-1}).limit(1).lean();
};


module.exports.update = async function update(selector, updateObj) {
    await ValidatorRanks.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await ValidatorRanks.find(selector).remove();
};

module.exports.findAll = async function findAll(selector) {
    return await ValidatorRanks.find(selector).lean();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await ValidatorRanks.deleteMany(selector);
};