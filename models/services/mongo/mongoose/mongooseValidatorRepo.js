let mongoose = require('./mongooseDB');
let validatorSchema = require('./schemas/validator').schema;
let Validator = mongoose.model('validators', validatorSchema);

module.exports.insert = async function insert(validatorData) {
    let validator = new Validator(validatorData);
    await validator.save();
    return validator._doc._id.toString();
};

module.exports.getRandomSampleBySkill = async function getRandomSampleBySkill(skill,claimantUserId, tier) {
    return await Validator
        .aggregate({$match : {
            $and:
                [
                    {skills: { $elemMatch: { $eq : skill}}},
                    { user_id: { $ne: claimantUserId } },
                    {delete: {$ne: true}},
                    {tier : tier}
                ]
            }});
};

module.exports.getRandomSampleBySkillAndTier = async function getRandomSampleBySkillAndTier(skill, claimantUserId, tier) {
    return await Validator
        .aggregate({$match : {
                $and:
                    [
                        {skills: { $elemMatch: { $eq : skill}}},
                        { user_id: { $ne: claimantUserId } },
                        {delete: {$ne: true}},
                        {tier: tier}
                    ]
            }});
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await Validator.find(selector).cursor();
};

module.exports.findOneById = async function findOneById(id) {
    return await Validator.findById(id).lean();
};

module.exports.findByClaimID = async function findByClaimID(claimId) {
    return await Validator.find({claim_id : claimId}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Validator.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Validator.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Validator.findOneAndUpdate(selector, updateObj);
};

module.exports.updateMany = async function update(selector, updateObj) {
    await Validator.updateMany(selector, updateObj);
};

module.exports.remove = async function remove(selector) {
    await Validator.remove(selector);
};

module.exports.getTopValidatorsByReputation = async function getTopValidatorsByReputation(num){
    return await Validator.find({"reputation.score" : {$exists :true}}).sort({ "reputation.score": -1 }).limit(num);
}

module.exports.deleteOne = async function deleteOne(selector) {
    await Validator.deleteOne(selector);
};