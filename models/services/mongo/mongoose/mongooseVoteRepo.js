let mongoose = require('./mongooseDB');
let votesSchema = require('./schemas/vote').schema;
let Vote = mongoose.model('votes', votesSchema);

module.exports.insert = async function insert(voteData) {
    let vote = new Vote(voteData);
    await vote.save();
    return vote._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Vote.findById(id).lean();
};

module.exports.findByVoterId = async function findByVoterId(voterId) {
    return await Vote.find({voter_id : voterId}).lean();
};

module.exports.findByVoterIdWithSorting = async function findByVoterIdWithSorting(voterId, sortCriteria) {
    return await Vote.find({voter_id : voterId}).sort(sortCriteria).lean();
};

module.exports.findByVotingRoundId = async function findByVotingRoundId(votingRoundId) {
    return await Vote.find({voting_round_id : votingRoundId}).lean();
};

module.exports.findByVotingRoundIdAndVoter = async function findByVotingRoundIdAndVoter(votingRoundId, voterId) {
    return await Vote.find({voting_round_id : votingRoundId, voter_id : voterId}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Vote.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Vote.find(selector).lean();
};

module.exports.findAllSkipLimitPromise = async function findAllSkipLimitPromise(selector, skip, limit) {
    return Vote.find(selector).sort({ $natural: -1 }).skip(skip).limit(limit).lean();
};

module.exports.findAllWithStream = async function findAllWithStream(selector) {
    return await Vote.find(selector).stream();
};

module.exports.countAll = async function countAll(selector) {
    return await Vote.count(selector);
};

module.exports.countAllPromise = async function countAllPromise(selector) {
    return Vote.count(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Vote.findOneAndUpdate(selector, updateObj);
};

module.exports.updateMany = async function updateMany(selector, updateObj) {
    await Vote.updateMany(selector, updateObj);
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await Vote.find(selector).cursor();
};

module.exports.count = async function count(selector) {
    return await Vote.count(selector);
};

module.exports.markAsPaid = async function markAsPaid(selector) {
    await Vote.findOneAndUpdate(selector, {$set : {paid : true}});
};

module.exports.deleteMany = async function deleteMany(selector) {
    await Vote.deleteMany(selector);
};

// eslint-disable-next-line max-len
module.exports.countClaimTentativeVotes = async function countClaimTentativeVotes(claimId, skillId) {
    const criteria = { claim_id: claimId, voted_at: { $gt: 0 } };
    if (skillId) criteria.skillId = skillId;

    return await Vote.count(criteria);
};