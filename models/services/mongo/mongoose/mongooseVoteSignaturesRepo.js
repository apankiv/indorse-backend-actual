/* eslint-disable */
let mongoose = require('./mongooseDB');
let voteSignatureSchema = require('./schemas/voteSignatures').schema;
let VoteSignature = mongoose.model('votesignatures', voteSignatureSchema);

module.exports.insert = async function insert(voteSigData) {
    let voteSig = new VoteSignature(voteSigData);
    await voteSig.save();
    return voteSig._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await VoteSignature.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await VoteSignature.findOne(selector).lean();
};

module.exports.deleteOne = async function deleteOne(selector) {
    await VoteSignature.deleteOne(selector);
};

module.exports.findAll = async function findAll(selector) {
    return await VoteSignature.find(selector).lean();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await VoteSignature.find(selector).cursor();
};

module.exports.updateMany = async function updateMany(selector, updateObj) {
    await VoteSignature.updateMany(selector, updateObj);
};
