const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const aggregation = new Schema({
    key: {
        type: String
    },
    value: Object
}, {usePushEach: true}, {runSettersOnQuery: true});

module.exports.schema = aggregation;