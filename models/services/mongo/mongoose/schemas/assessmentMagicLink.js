const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const assessmentMagicLink = new Schema({
    title: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    },
    jobPostLink: {
        type: String,
    },
    chatbotSkills: { type: Array },
    assignmentSkills: [{
        skillTag: String,
        allowedSkillIds: [{ type: String }],
    }],
    claimSkills: [{ type: Schema.Types.ObjectId, ref: 'skills' }],
    claimAllowed: { type: Boolean, default: false },
    companyPrettyId: { type: String }, // now not used but stored
    companyId: { type: Schema.Types.ObjectId, ref: 'company_names' },
    createdAt: {
        type: Number,
    },
    createdBy: { type: String },
    disabled: {
        type: Boolean,
        default: false,
    },
    publicId: {
        type: String,
        required: true,
    },
    deletedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    deletedAt: { type: Number },
    deleted: { type: Boolean, default: false },
    active: { type: Boolean, default: true }, // default is true because we want magicLinks to be active on creation

    userAssessmentReferences: [
        {
            user_id: {
                type: String,
            },
            startedAt: {
                type: Number,
            },
        },
    ],
    started: [{ type: Schema.Types.ObjectId, ref: 'users' }],
    // Added for the case if assignments is allowed specific to magic link
    // allowedAssignments: [{ type: Schema.Types.ObjectId, ref: 'assignments' }],
    // This property is inherited from company.magicLink Object
    autoAllowedAssignment: { type: Boolean, default: false },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = assessmentMagicLink;
