const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const assignment = new Schema(
    {
        duration: String,
        title: String,
        description: String,
        jobRole: { type: Schema.Types.ObjectId, ref: 'jobroles' }, // ref should be equal to whatever is passed to mongoose.model() for job role
        skills: [], // Obselete, Pre-1426
        img_url: String,
        scope: String,
        features: [String],
        disabled: Boolean,
        listingPosition: Number, // Small number mean
    },
    { runSettersOnQuery: true },
);

module.exports.schema = assignment;
