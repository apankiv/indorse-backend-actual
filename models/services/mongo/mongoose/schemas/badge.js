const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const SAFE_BADGE_FIELDS = ['name', 'type', 'description', 'id',
    'image', 'criteria',
    'tags', 'issuer','extensions:cta_text','extensions:cta_link'];

const badges = new Schema({

    id: {
        type: String,
        minlength: 1,
        maxlength: 64,
        lowercase: true
    },
    name: {
        type: String
    },
    type: {
        type: String
    },
    created_at_timestamp:{
        type:Date
    },
    last_updated_timestamp:{
        type:Date
    },
    chatbot_uuid:{
        type: String
    },
    description: {
        type: String,
        minlength: 1,
        maxlength: 1000
    },
    image: {
        type: Object
    },
    criteria: {
        type: Object
    },
    tags: {
        type: Object
    },
    issuer: {
        type: Object
    },
    extensions_cta_link: {
        type: Object
    },
    extensions_cta_text: {
        type: Object
    }
}, {runSettersOnQuery: true});


module.exports.schema = badges;
