const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const blacklist = new Schema({
    ip: {
        type: String,
    },
    group: {
        type: String,
    },
    request_counter: {
        type: Number,
    },
    created_at: {
        type: Date,
        expires: 60,
    },
    updated_at: {
        type: Date,
    },
});

blacklist.index({ ip: 1, group: 1 });
module.exports.schema = blacklist;

