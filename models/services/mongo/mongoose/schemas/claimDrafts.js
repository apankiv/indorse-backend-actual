const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claims = new Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    proof: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    finalized: {
        type: Boolean
    },
    source : {
        type: String
    },
    created_at : {
        type : Number
    },
    finalized_at : {
        type : Number
    }
});

module.exports.schema = claims;
