const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claimEvents = new Schema({
    user_id :{
        type:String,
        required : true
    },
    claim_id: {
        type: Schema.Types.ObjectId, ref: 'claims',
        required: true
    }, 
    voting_round_id :{
        type: Schema.Types.ObjectId, ref: 'votes',
        required: true        
    },    
    event: {
        type: String,
        enum: ['vote_submitted', 'claim_first_view', 'vote_in_consensus', 'vote_not_in_consensus'],
    },
    timestamp: {
        type: Number,
    }, 
    properties: {
        type :Object
    }
})
module.exports.schema = claimEvents;