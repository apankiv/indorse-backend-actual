const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claimGithubs = new mongoose.Schema({
    claim_id: {
        type: String,
        required: true
    },
    githubUsername: {
        type: String,
        required: true
    },
    claimantProfileURL: {
        type: String,
        required: false
    },
    githubRepoURL: {
        type: String,
        required: true
    },
    forkUrl: {
      type: String,
      required: false
    },
    recentFiveCommitLinks: [new mongoose.Schema({
        commitMessage: {
            type: String,
        },
        commitUrl: {
            type: String,
        }
    })],
    commitHistoryArray: [new mongoose.Schema({
        week: {
            type: Number,
        },
        noOfCommits: {
            type: Number,
        }
    })],
    commitHistoryAllArray: [Number],
    isReturnAllCommit: {
      type: Boolean,
      required: true
    },
    totalNumberOfCommits: {
        type: Number,
        required: false
    },
    totalNumberOfLinesAdded: {
        type: Number,
        required: false
    },
    totalNumberOfLinesDeleted: {
        type: Number,
        required: false
    },
    createdAt: {
        type: Number,
        required: false
    },
    isFork: {
        type: Boolean,
        required: false
    },
    isMirror: {
        type: Boolean,
        required: false
    },
    isClaimantOwner: {
        type: Boolean,
        required: true
    }
});



module.exports.schemaType = claimGithubs;
