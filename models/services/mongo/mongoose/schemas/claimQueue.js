const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claimQueue = new Schema({

    claim_id: {
        type: String,
        required: true
    },
    released : {
        type : Boolean,
        required : true
    },
    approved_timestamp : {
        type : Number,
        required : true
    }
});

module.exports.schema = claimQueue;
