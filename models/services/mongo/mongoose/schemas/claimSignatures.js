const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claimSignatures = new Schema({
    payload: new Schema({
        claimant_id: {
            type: String,
            required : true
        },
        proof: {
            type: String,
            required : true
        },
        timestamp: {
            type: Number,
            required : true
        },
        deadline: {
            type: Number,
            required : true
        },
        skills: {
            type: String,
            required : true
        },
        kind: {
            type: String,
            enum: ['ASSIGNMENT', 'BOAST'],
            required : true
        },
        voters: {
            type: String,
            required : true
        },
        claim_id: {
            type: String,
            required : true
        },
        version: {
            type: String,
            required : true
        }
    }, { _id: false }),

    signature: new Schema({
        v: {
            type: Number,
        },
        r: {
            type: String,
            match: /^0x[0-9aA-fF]{64}$/,
        },
        s: {
            type: String,
            match: /^0x[0-9aA-fF]{64}$/,
        }
    }, { _id: false }),
    merkelized : {
        type : Boolean
    }

}, { usePushEach: true }, { runSettersOnQuery: true });


module.exports.schema = claimSignatures;
