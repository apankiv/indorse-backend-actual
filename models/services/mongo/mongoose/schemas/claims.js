const mongoose = require('../mongooseDB');
const setting = require('../../../../settings');
const {tiers} = require('./validator');

const { Schema } = mongoose;

const claims = new Schema({
    title: {
        type: String,
        required: true,
    },
    desc: {
        type: String,
        required: true,
    },
    can_be_reopened: Boolean,
    sc_deadline: Number,
    proof: {
        type: String,
        required: true,
    },
    survey: String,
    state: {
        type: String,
        required: true,
    },
    level: {
        type: String,
    },
    visible: {
        type: Boolean,
    },
    approved: {
        type: Number,
    },
    approved_by : String,
    released_at : Number,
    approved_at : Number,
    resubmission_reminder_sent_at : Number,
    disapproved: {
        type: Number,
    },
    ownerid: {
        type: String,
    },
    endorse_count: {
        type: Number,
    },
    flag_count: {
        type: Number,
    },
    final_status: {
        type: Boolean,
    },
    sc_claim_id: {
        type: Number,
    },
    claim_expiry: {
        type: Number,
    },
    is_sc_claim: {
        type: Boolean,
    },
    source: {
        type: String,
    },
    disapprove_reason: {
        type: String,
    },
    created_at: {
        type: Number,
    },
    rewarded_at: {
        type: Number,
    },
    tags: {
        type: [String],
    },
    full_title: {
        type: String,
    },
    type: String,
    user_assignment_id: String,
    assignment_id: String,
    submission_event_computed: Boolean,
    in_consensus: Boolean,
    signature_ref: String, //MonogID of the related signature object
    merkelized: Boolean, //Flag to identify if claim has been merkelized
    merkle_ref: String, // MongoID of the merkle tree object set after its creation
    // added for ID-1582
    clientApp: {
        clientId: { type: String },
        userId: { type: String },
        state: { type: String },
    },
    company: { type: Schema.Types.ObjectId, ref: 'company_names' },
    aipLimit: { type: Number }, // it is not set until approved
    userAssessmentId: { type: Schema.Types.ObjectId, ref: 'userassessments' },
    validatorAssignedBytier: [{
        tier: {
            type: String,
            enum: tiers
        },
        noOfValidator: Number
    }],

    // Added after ID-1663
    normalizedRating: { type: Number },

    // Added after ID-1728
    canOwnerViewFeedback: { type: Boolean },
});

module.exports.schema = claims;

const assignment = 'ASSIGNMENT';
const boast = 'BOAST';

module.exports.types = {
    ASSIGNMENT: assignment,
    BOAST: boast,
    values: [assignment, boast],
};

const New = 'new';
const inProgress = 'in_progress';
const finished = 'finished';

module.exports.states = {
    NEW: New,
    IN_PROGRESS: inProgress,
    FINISHED: finished,
    values: [New, inProgress, finished],
};

const beginner = 'beginner';
const intermediate = 'intermediate';
const expert = 'expert';

module.exports.levels = {
    BEGINNER: beginner,
    INTERMEDIATE: intermediate,
    EXPERT: expert,
    values: [beginner, intermediate, expert],
};

const PARTNER = 'PARTNER';
const SME = 'SME';
const NORMAL = 'NORMAL';
const COMPANY = 'COMPANY';
const DIRECT = 'DIRECT';

const claimSource = {
    PARTNER,
    SME,
    NORMAL,
    COMPANY,
    DIRECT,
    values: [
        PARTNER,
        SME,
        NORMAL,
        COMPANY,
        DIRECT,
    ],
};
module.exports.claimSource = claimSource;

const APPROVED = 'approved';
const DISAPPROVED = 'disapproved';
const PENDING_APPROVAL = 'pending_approval';

const claimStage = {
    APPROVED,
    DISAPPROVED,
    PENDING_APPROVAL,
    values: [
        APPROVED,
        DISAPPROVED,
        PENDING_APPROVAL,
    ],
};
module.exports.claimStage = claimStage;


const IN_PROGRESS = 'in_progress';
const NO_CONSENSUS = 'no_consensus';
const CLAIM_DISAPPROVED = 'claim_disapproved';
const CLAIM_SUBMITTED = 'claim_submitted';
const INDORSED = 'indorsed';
const FLAGGED = 'flagged';
const TENTATIVELY_INDORSED = 'tentatively_indorsed';
const TENTATIVELY_FLAGGED = 'tentatively_flagged';

const claimStatusForPartners = {
    IN_PROGRESS,
    NO_CONSENSUS,
    CLAIM_DISAPPROVED,
    CLAIM_SUBMITTED,
    INDORSED,
    FLAGGED,
    TENTATIVELY_INDORSED,
    TENTATIVELY_FLAGGED,
    values: [
        IN_PROGRESS,
        NO_CONSENSUS,
        CLAIM_DISAPPROVED,
        CLAIM_SUBMITTED,
        INDORSED,
        FLAGGED,
        TENTATIVELY_INDORSED,
        TENTATIVELY_FLAGGED,
    ],
};

module.exports.claimStatusForPartners = claimStatusForPartners;

const ALLOWED = 'allowed';
const NOT_ALLOWED = 'not_allowed';
const claimReportAccessStatus = {
    ALLOWED,
    NOT_ALLOWED,
    values: [ALLOWED, NOT_ALLOWED],
};

module.exports.claimReportAccessStatus = claimReportAccessStatus;
