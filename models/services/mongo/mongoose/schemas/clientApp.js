const mongoose = require('../mongooseDB');

const { Schema } = mongoose;
const POST = 'POST';
const GET = 'GET';

const secureMethods = {
    POST,
    values: [POST],
};
const supportedMethods = {
    POST,
    GET,
    values: [POST, GET],
};

const BASIC = 'basic';
const HANDSHAKE = 'handshake';
const authMethods = {
    BASIC,
    HANDSHAKE,
    values: [BASIC, HANDSHAKE],
};

const clientApp = new Schema({
    email: { type: String }, // if created directly from email
    company: { type: Schema.Types.ObjectId, ref: 'company_name' }, // added for linking company with client apps
    display_name: { type: String, required: true },
    client_id: { type: String, required: true },
    client_secret: { type: String },
    salt: { type: String },
    auth: { type: String, enum: authMethods.values, default: authMethods.BASIC },
    hooks: {
        verifyPartnerClaim: {
            method: {
                type: String,
                enum: supportedMethods.values,
                default: supportedMethods.POST,
            },
            url: {
                type: String,
            },
        },
        sendPartnerClaimResult: {
            method: {
                type: String,
                enum: secureMethods.values,
                default: secureMethods.POST,
            },
            url: {
                type: String,
            },
        },
    },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = clientApp;
module.exports.authMethods = authMethods;
module.exports.secureMethods = secureMethods;
module.exports.supportedMethods = supportedMethods;
