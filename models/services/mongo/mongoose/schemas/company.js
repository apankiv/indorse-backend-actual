const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const SAFE_COMPANY_FIELDS = ['admin', 'company_name', 'created_by_admin', 'description', 'item_key',
    'last_updated_by', 'last_updated_timestamp', 'pretty_id', 'social_links', 'admin_username', 'logo_s3',
    'logo_ifps', 'cover_s3', 'cover_ipfs', 'additional_data', 'entity_ethaddress', 'advisors',
    'team_members', 'visible_to_public', 'tagline', 'credits', 'assessmentAssignment', 'credit_updates',
    'creditType', 'delta', 'reason', 'timestamp', 'features', 'aipLimit', 'magicLink'];

const ASSESSMENT_ASSIGNMENT = 'assessmentAssignment';
const creditType = {
    ASSESSMENT_ASSIGNMENT,
    values: [ASSESSMENT_ASSIGNMENT],
};

const company_names = new Schema({

    pretty_id: {
        type: String,
        minlength: 1,
        maxlength: 64,
        lowercase: true,
    },

    email: {
        type: String,
        lowercase: true,
    },

    entity_ethaddress: {
        type: String,
        match: /^(0x)?[0-9aA-fF]{40}$/,
    },

    company_name: {
        type: String,
    },

    tagline: {
        type: String,
    },

    item_key: {
        type: String,
    },

    last_updated_by: {
        type: String,
    },

    last_updated_timestamp: {
        type: Number,
    },

    created_by_admin: {
        type: Boolean,
    },

    admin_username: {
        type: String,
    },

    logo_s3: {
        type: String,
    },

    logo_ipfs: {
        type: String,
    },

    cover_s3: {
        type: String,
    },

    cover_ipfs: {
        type: String,
    },

    description: {
        type: String,
        minlength: 1,
        maxlength: 1000,
    },

    admin: {
        type: String,
        validate: /\w{12}|[aA-fF0-9]{24}/,
    },
    visible_to_public: {
        type: Boolean,
    },

    additional_data: {
        type: Object,
    },

    social_links: [new Schema(
        {
            type: {
                type: String,
                minlength: 1,
                maxlength: 64,
                required: true,
            },

            url: {
                type: String,
                minlength: 1,
                required: true,
            },

        },
        { _id: false },
    ),
    ],


    advisors: [{
        user_id: {
            type: String,
            minlength: 1,
            lowercase: true,
        },

        verification_timestamp: {
            type: Number,
        },

        creation_timestamp: {
            type: Number,
        },

        rejected_timestamp: {
            type: Number,
        },

        accepted_timestamp: {
            type: Number,
        },

        email: {
            type: String,
            lowercase: true,
        },

        company_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        user_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },
        imgUrl: {
            type: String,
        },
        name: {
            type: String,
        },
        softConnection: {
            type: {
                status: {
                    type: String,
                    required: true,
                    enum: ['ACCEPTED', 'REJECTED', 'PENDING'],
                },
                statusUpdatedAt: {
                    type: Number,
                },
                statusValidatedByUserAt: {
                    type: Number,
                },
                statusValidatedByCompanyAt: {
                    type: Number,
                },
                companyStatus: {
                    type: String,
                    required: true,
                    enum: ['APPROVED', 'DISAPPROVED'],
                },
                companyStatusUpdatedBy: {
                    type: {
                        id: {
                            type: String,
                            minlength: 1,
                            lowercase: true,
                        },
                    },
                },
                companyStatusUpdatedAt: {
                    type: Number,
                },
                email: {
                    type: String,
                    lowercase: true,
                },
            },
        },
    }],

    revoked_advisors: [{
        user_id: {
            type: String,
            minlength: 1,
            lowercase: true,
        },

        verification_timestamp: {
            type: Number,
        },

        creation_timestamp: {
            type: Number,
        },

        rejected_timestamp: {
            type: Number,
        },

        accepted_timestamp: {
            type: Number,
        },

        email: {
            type: String,
            lowercase: true,
        },

        company_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        user_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        revoked_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },
    }],

    team_members: [{
        user_id: {
            type: String,
            minlength: 1,
            lowercase: true,
        },

        verification_timestamp: {
            type: Number,
        },

        creation_timestamp: {
            type: Number,
        },

        rejected_timestamp: {
            type: Number,
        },

        accepted_timestamp: {
            type: Number,
        },

        email: {
            type: String,
            lowercase: true,
        },

        company_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        user_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        designation: {
            type: String,
        },

        imgUrl: {
            type: String,
        },

        name: {
            type: String,
        },
    }],

    revoked_team_members: [{
        user_id: {
            type: String,
            minlength: 1,
            lowercase: true,
        },

        verification_timestamp: {
            type: Number,
        },

        creation_timestamp: {
            type: Number,
        },

        rejected_timestamp: {
            type: Number,
        },

        accepted_timestamp: {
            type: Number,
        },

        email: {
            type: String,
            lowercase: true,
        },

        company_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        user_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        revoked_tx_hash: {
            type: String,
            match: /^(0x)?[0-9aA-fF]{64}$/,
            lowercase: true,
        },

        designation: {
            type: String,
        },
    }],

    acl: [{
        email: { type: String },
        userId: { type: Schema.Types.ObjectId, ref: 'users' },
        role: { type: String, enum: ['admin'], default: 'admin' },
        acceptedInvite: { type: Boolean, default: false },
        inviteId: { type: Schema.Types.ObjectId, ref: 'invites' },
    }],

    credits: {
        assessmentAssignment: {
            limit: {
                type: Number,
                required: true,
                default: 10
            },
            used: {
                type: Number,
                required: true,
                default: 0
            },
            limitReachedTimestamp: {
                type: Number,
                required: false,
                default: null
            }
        }
    },

    credit_updates: [{
        creditType: {
            type: String,
            enum: creditType
        },
        delta: {
            type: Number
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },
        reason: {
            type: String
        },
        timestamp: {
            type: Number
        }
    }],

    // Added for partner claims and after ID-1604
    // updated for feature toggle
    features: {
        magicLink: { type: Boolean, default: true },
        partnerClaims: { type: Boolean, default: false },
    },

    // ID-1630 - AIP Default validators per claim for partner and sme claims
    aipLimit: { type: Number },
    magicLink: {
        autoAllowedAssignment: { type: Boolean, default: true },
        assignmentRestricted: { type: Boolean, default: false },
    },
}, {runSettersOnQuery: true});

module.exports.toSafeObject = function toSafeObject(company) {
    const safeCompany = {};

    SAFE_COMPANY_FIELDS.forEach((fieldName) => {
        if (typeof (company[fieldName]) === 'boolean') {
            safeCompany[fieldName] = company[fieldName];
        } else if (company[fieldName]) {
            safeCompany[fieldName] = company[fieldName];
        }
    });

    return safeCompany;
};

module.exports.isValidCreditType = function isValidCreditType(argCreditType) {
    const values = creditType.values;
    return values.indexOf(argCreditType) > -1;

};

module.exports.schema = company_names;
module.exports.creditType = creditType;
