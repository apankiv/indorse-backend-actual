const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const companyAssignments = new Schema({
    companyId: {
        type: Schema.Types.ObjectId,
        ref: 'company_names',
        required: true,
        unique: true,
    },
    linkedAssignments: [new Schema({
        assignmentId: {
            type: Schema.Types.ObjectId,
            ref: 'assignments',
            required: true,
        },
        linkedBy: {
            type: Schema.Types.ObjectId,
            ref: 'users',
            required: false,
        },
        linkedAt: { type: Number },
        // isHidden: { type: Boolean, default: false },
    })],
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = companyAssignments;
