const mongoose = require('../mongooseDB');

const { Schema } = mongoose;
const CASE_STUDY = 'caseStudy';
const typeOfRequest = {
    CASE_STUDY,
    values: [CASE_STUDY],
};
const contactRequest = new Schema({
    typeOfRequest: {
        type: String,
        required: true,
        enum: typeOfRequest.values,
        default: typeOfRequest.CASE_STUDY,
    },
    email: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String },
    phone: { type: String },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = contactRequest;
