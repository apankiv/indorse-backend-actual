const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const designations = new Schema({

    designation: {
        type: String,
        required: true
    }
});

module.exports = designations;
