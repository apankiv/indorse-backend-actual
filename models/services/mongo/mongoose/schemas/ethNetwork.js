const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const ethNetwork = new Schema({
    network: {
        type: String,
        required: true
    },
    http_provider: {
        type: String,
        required: true
    },
    max_gas_price: { //In Gwei
        type: Number,
        required: true        
    },
    max_inspection_count : {
        type: Number,
        required: true                
    }
});

module.exports = ethNetwork;