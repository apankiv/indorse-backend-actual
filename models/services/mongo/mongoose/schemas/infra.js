const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const infras = new Schema({
    last_seen_timestamp : Number,
    name : String
}, {runSettersOnQuery: true});

module.exports.schema = infras;
