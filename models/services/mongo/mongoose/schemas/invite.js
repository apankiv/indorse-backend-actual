const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const COMPANY_ACCESS = 'company_access';
const MAGIC_LINK = 'magic_link';

const inviteType = {
    COMPANY_ACCESS,
    MAGIC_LINK,
    values: [
        COMPANY_ACCESS,
        MAGIC_LINK,
    ],
};

const invite = new Schema({
    email: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    invitedAt: {
        type: Number,
    },
    invitedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    invitedFor: { type: Schema.Types.ObjectId },
    acceptedAt: Number,
    invite_type: {
        type: String,
        enum: inviteType.values,
        default: inviteType.COMPANY_ACCESS,
    },
    disabled: {
        type: Boolean,
        default: false,
    },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = invite;
module.exports.inviteType = inviteType;
