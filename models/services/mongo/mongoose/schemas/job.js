const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const jobs = new Schema({

    title: {
        type: String,
        minlength: 1,
        maxlength: 256,
        required: true
    },

    experienceLevel: {
        type: String,
        enum: ['intern', 'junior', 'intermediate', 'senior'],
        required: true
    },

    description:{
        type:String,
        minlength:10,
        maxlength:5000, //TO confirm
        required: true
    },

    monthlySalary:{
        type:String,
        minLength:3, //TO confirm
        required: true
    },

    skills: [new Schema({
        id: {
            type: String
        },
        name: {
            type: String,
            required: true
        },
        level: {
            type: String,
            enum: ['beginner', 'intermediate', 'expert'],
        },
        required: {
            type: Boolean
        }
    }, { _id: false })], 

    location: {
        type: String
    },
    company: new Schema({
        id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        description: {
            type: String
        },
        logo: new Schema({
            s3Url: {
                type: String
            },
            ipfsUrl: {
                type: String
            }
        },{_id:false}),
    }, { _id: false }),

    contactEmail : {
        type: String
    },

    applicationLink:{
        type: String
    },

    campaign: new Schema({
        title: {
            type: String
        },
        description: {
            type: String
        },
        enabled: {
            type: Boolean,
            required: true
        }
    }),

    submitted: new Schema({
        by: {
            type: String
        },
        at: {
            type: Number
        }
    }, { _id: false }),

    approved: new Schema({
        approved: {
            type: Boolean
        },
        by: {
            type: String
        },
        at: {
            type: Number
        }
    }, { _id: false }),

    updated: new Schema({
            by: {
                type: String
            },
            at: {
                type: Number
        }
    }, { _id: false })
},{usePushEach: true},{runSettersOnQuery: true});

module.exports.schema = jobs;
