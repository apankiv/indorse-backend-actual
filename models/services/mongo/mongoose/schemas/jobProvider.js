const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const jobProvider = new Schema({
    name: String
}, {runSettersOnQuery: true});

module.exports.schema = jobProvider;