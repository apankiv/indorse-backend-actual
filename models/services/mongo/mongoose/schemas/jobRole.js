const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const jobRole = new Schema(
  {
    skills: [], // For legacy purposes
    position: String, // Legacy, Pre-1426
    prettyId: { // Used by 1538990972258-seedJobRolesAndSkillTagsMapping.js
      type: String,
      required: true,
      unique: true,
    },
    title: String,
    description: String,
    iconUrl: String,
    skillTags: [String],
  },
  { runSettersOnQuery: true },
);

module.exports.schema = jobRole;