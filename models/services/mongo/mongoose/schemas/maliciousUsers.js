const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const maliciousUsers = new Schema({
    ip: {
        type: String,
        index: true,
    },
    url: {
        type: String,
        index: true,
    },
    request_counter: {
        type: Number,
    },
    data: {
        type: Schema.Types.Mixed,
        of: String,
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

module.exports.schema = maliciousUsers;

