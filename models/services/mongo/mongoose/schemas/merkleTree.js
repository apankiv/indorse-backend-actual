const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const merkleTree = new Schema({
    leaves: [new Schema({        
        data: {
            type: String,
            required: true
        },

        type :{
            type: String,
            required: true,
            enum: ['claim', 'vote']
        },

        reference: {
            claim_id: String,
            claim_signature_id : String,
            user_id : String,
            vote_signature_id : String           
        },

    },{_id: false})],
    claim_ids : [String],
    rootHash :{
        type : String
    },
    timestamp :{
        type : Number
    },
    tx_hash :{
        type: String
    }
}, {
    usePushEach: true
},{runSettersOnQuery: true});


module.exports.schema = merkleTree;