const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const miscSkill = new Schema({

    name: {
        type: String,
        minlength: 1,
        required: true
    },

    user_id: {
        type: String,
        minlength: 1,
        required: true
    }
});

module.exports = miscSkill;
