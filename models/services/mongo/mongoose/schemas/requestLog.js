const mongoose = require('../mongooseDB');

const { Schema } = mongoose;
const INCOMING = 'incoming';
const OUTGOING = 'outgoing';

const directions = {
    INCOMING,
    OUTGOING,
    values: [INCOMING, OUTGOING],
};

const requestLog = new Schema({
    direction: {
        type: String,
        default: directions.INCOMING, // if incoming then the request came from different server to indorse server
        enum: directions.values,
    },
    request: {
        type: Object,
        required: true,
    },
    requestLoggedAt: {
        type: Number,
        default: Date.now,
    },
    response: {
        type: Object,
        default: null,
    },
    meta: {
        type: Object,
    },
    responseLoggedAt: {
        type: Number,
        default: Date.now,
    },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = requestLog;
module.exports.directions = directions;
