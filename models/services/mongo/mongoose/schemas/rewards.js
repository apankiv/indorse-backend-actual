const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const rewards = new Schema({
    user_id :{
        type:String,
        required : true
    },
    reward: {
        type: Number,
        required: true
    },
    calculation_timestamp: {
        type: Number,
    }, 
    payout_timestamp: {
        type: Number,
    },
    properties: {
        type :Object
    },
    paid : {
        type : Boolean
    }
})
module.exports.schema = rewards;