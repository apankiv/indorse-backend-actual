const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const skills = new Schema({

    name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    validation: new Schema({
        chatbot: {
            type: Boolean
        },
        aip: {
            type: Boolean
        },
        chatbot_uuid: {
            type: String
        }
    }, {_id: false}),
    tags: [String],
    parents: [{ type: Schema.Types.ObjectId, ref: 'skills' }], // ref has to be whatever is passed to mongoose.model() for skill
    iconUrl: {
        type: String,
    },
    disabledFromFrameworkListing: {
        type: Boolean,
    }, // TODO: Have a ability for admin to manage this
});

module.exports = skills;
