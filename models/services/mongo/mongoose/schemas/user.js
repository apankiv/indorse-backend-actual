const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const SAFE_USER_FIELDS = [
    'name', 'bio', 'ethaddressverified', 'username', 'user_id', 'work', 'education', '_id',
    'img_url', 'photo_ipfs', 'score_count', 'verified', 'isCivicLinked', 'is_airbitz_user',
    'language', 'isFacebookLinked', 'isGoogleLinked', 'isLinkedInLinked', 'isAirbitzLinked',
    'isUportLinked', 'badges', 'interests', 'professions', 'pending_badges', 'social_links',
    'skills', 'linkedInProfileURL', 'github_uid', 'isOpenToOpportunities', 'position',
    'employmentStatus', 'yearsOfExperience', 'isDeleted'
];

const UNSAFE_PRIVATE_FIELDS = [
    'ethaddress', 'email', 'myCompanies', 'oauth_key', 'ethaddressverified', 'role',
    'timestamp', 'job_roles', 'last_linkedin_pdf_import_at', 'is_pass', 'terms_privacy',
    'data_request', 'confidenceScore', 'approved', 'wizards',
];

const users = new Schema({

    name: {
        type: String,
        minlength: 1,
        maxlength: 64,
    },
    civicParams: [],
    oauth_key: {
        type: String,
    },
    civic_uid: {
        type: String,
    },

    uport_uid: {
        type: String,
    },

    isCivicLinked: {
        type: Boolean,
    },

    isOpenToOpportunities: {
        type: Boolean,
    },

    bio: {
        type: String,
    },

    username: {
        type: String,
    },

    position: {
        type: String,
    },

    yearsOfExperience: {
        type: Number,
    },

    employmentStatus: {
        type: String,
    },

    role: {
        type: String,
        enum: ['admin', 'full_access', 'profile_access', 'no_access'],
    },

    isSuperAdmin: {
        type: Boolean,
    },

    email: {
        type: String,
        lowercase: true,
    },

    ethaddress: {
        type: String,
        match: /^(0x)?[0-9aA-fF]{40}$/,
    },

    ethaddressverified: {
        type: Boolean,
    },

    is_pass: {
        type: Boolean,
    },

    verified: {
        type: Boolean,
    },

    approved: {
        type: Boolean,
    },

    keyPair: {

        type: {

            privateKey: {
                type: String,
                required: true,
            },

            publicKey: {
                type: String,
                required: true,
            },
        },
    },

    score_count: {
        type: Number,
    },

    confidenceScore: {
        type: Object, // TODO add field schema
    },

    // TODO add item schema
    linkedInParams: [],

    // TODO add item schema
    facebookParams: [],

    // TODO : May be not an array
    uportParams: [],

    social_links: [],

    img_url: {
        type: String,
    },

    photo_ipfs: {
        type: String,
    },

    last_linkedin_pdf_import_at: {
        type: String,
    },

    pdf_aws: {
        type: String,
    },

    is_airbitz_user: {
        type: Boolean,
    },

    facebook_uid: {
        type: String,
    },

    linkedIn_uid: {
        type: String,
    },

    google_uid: {
        type: String,
    },

    github_uid: {
        type: String,
    },

    isFacebookLinked: {
        type: Boolean,
    },

    isGoogleLinked: {
        type: Boolean,
    },

    isAirbitzLinked: {
        type: Boolean,
    },

    isLinkedInLinked: {
        type: Boolean,
    },

    linkedInProfileURL: {
        type: String,
    },

    isUportLinked: {
        type: Boolean,
    },

    isDeleted : {
        type : Boolean
    },

    pass: {
        type: String,
    },

    pass_verify_token: {
        type: String,
    },

    pass_verify_timestamp: {
        type: Number,
    },

    timestamp: {
        type: Number,
    },

    salt: {
        type: String,
    },

    tokens: [String],

    verify_token: {
        type: String,
    },

    ga_id: {
        type: String,
    },

    skills: [new Schema({
        skill: {
            name: {
                type: String,
                required: true,
            },
            category: {
                type: String,
                required: true,
            },
            _id: {
                type: String,
                required: true,
            },
        },
        level: {
            type: String,
            enum: ['beginner', 'intermediate', 'expert'],
        },
        isHighlighted: {
            type: Boolean,
            required: false,
        },
        isDeleted: {
            type: Boolean,
            required: false,
        },
        validations: [new Schema({
            type: {
                type: String,
                required: true,
            },
            level: {
                type: String,
                enum: ['beginner', 'intermediate', 'expert'],
            },
            id: {
                type: String,
                required: true,
            },
            validated: {
                type: Boolean,
                required: true,
            },
            validatedAt: {
                type: Number,
                required: false,
            },
            chatbotScore: {
                type: Number,
                required: false,
            },
        })],
    })],

    badges: [String],

    pending_badges: [String],

    professions: [String],

    education: [new Schema({

        skills: [String],

        school_name: {
            type: String,
            required: true,
        },

        item_key: {
            type: String,
            required: true,
        },

        school_id: {
            type: String,
            minLength: 1,
        },

        degree: {
            type: String,
        },

        field_of_study: {
            type: String,
            minLength: 1,
        },

        grade: {
            type: String,
            minLength: 1,
        },

        activities: {
            type: String,
            minLength: 1,
        },

        description: {
            type: String,
            minLength: 1,
        },

        start_date: {
            year: {
                type: Number,
            },
            month: {
                type: Number,
            },
        },

        end_date: {
            year: {
                type: Number,
            },
            month: {
                type: Number,
            },
        },
    }, { _id: false })],

    work: [new Schema({

        skills: [String],

        item_key: {
            type: String,
            required: true,
        },

        company_id: {
            type: String,
            minLength: 1,
        },

        title: {
            type: String,
            minLength: 1,
        },

        currently_working: {
            type: Boolean,
        },

        location: {
            type: String,
            minLength: 1,
        },

        activities: {
            type: String,
            minLength: 1,
        },

        description: {
            type: String,
            minLength: 1,
        },

        start_date: {
            year: {
                type: Number,
            },
            month: {
                type: Number,
            },
        },

        end_date: {
            year: {
                type: Number,
            },
            month: {
                type: Number,
            },
        },
    }, { _id: false })],

    language: [new Schema({

        language: {
            type: String,
            required: true,
        },

        item_key: {
            type: String,
            required: true,
        },

        language_id: {
            type: String,
            required: true,
        },

        proficiency: {
            type: String,
            required: true,
            enum: ['elementary', 'limited_working', 'general_professional', 'advanced_professional', 'native'],
        },

    }, { _id: false })],

    wizards: [new Schema({

        name: {
            type: String,
            required: true,
        },

        current_step: {
            type: Number,
            required: true,
        },

        isFinished: {
            type: Boolean,
        },

    }, { _id: false })],

    interests: [String],

    terms_privacy: [new Schema({

        tc_version: {
            type: String,
            required: true,
        },

        privacy_version: {
            type: String,
            required: true,
        },

        timestamp: {
            type: Number,
            required: true,
        },

    }, { _id: false })],

    interests: [String],

    terms_privacy: [new Schema({

        tc_version: {
            type: String,
            required: true,
        },

        privacy_version: {
            type: String,
            required: true,
        },

        timestamp: {
            type: Number,
            required: true,
        },

    }, { _id: false })],

    data_request: [new Schema({

        request_type: {
            type: String,
            required: true,
        },

        timestamp: {
            type: Number,
            required: true,
        },

    }, { _id: false })],

    job_roles: [{ type: Schema.Types.ObjectId, ref: 'jobroles' }], // ref should be equal to whatever is passed to mongoose.model() for job role
}, {
    usePushEach: true,
}, { runSettersOnQuery: true });


module.exports.toSafeObject = function toSafeObject(user, { user_id: userId, role } = {}) {
    let USER_FIELDS_TO_SEND = SAFE_USER_FIELDS;
    if ((userId && (user._id.toString() === userId.toString())) || role === 'admin') {
        USER_FIELDS_TO_SEND = [...USER_FIELDS_TO_SEND, ...UNSAFE_PRIVATE_FIELDS];
    }

    const safe_user = {};
    USER_FIELDS_TO_SEND.forEach((fieldName) => {
        if (user[fieldName]) {
            safe_user[fieldName] = user[fieldName];
        }
    });
    return safe_user;
};

module.exports.schema = users;
