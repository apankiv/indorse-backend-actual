const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const validatedChatbotSkillsSchema = new Schema({
    skillId: {
        type: String,
        required: true,
    },
    id: {
        type: String,
        required: false,
    },
    validated: {
        type: Boolean,
        required: true,
    },
    validatedAt: {
        type: Number,
        required: false,
    },
    chatbotScore: {
        type: Number,
        required: false,
    },
});

const assignmentSkillsSchema = new Schema();

const CHATBOTS_PENDING = 'chatbotsPending';
const CHATBOTS_PASSED = 'chatbotsPassed';
const ASSIGNMENT_PENDING = 'assignmentPending';
const ASSIGNMENT_ALLOWED = 'assignmentAllowed';
const ASSIGNMENTS_VIEWED = 'assignmentsViewed';
const ASSIGNMENT_SUBMITTED = 'assignmentSubmitted';
const ASSIGNMENT_EVALUATED = 'assignmentEvaluated';
const CLAIM_PENDING = 'claimPending';
const CLAIM_SUBMITTED = 'claimSubmitted';
const CLAIM_EVALUATED = 'claimEvaluated';

const claimStatus = {
    CLAIM_PENDING,
    CLAIM_SUBMITTED,
    CLAIM_EVALUATED,
    values: [
        CLAIM_PENDING,
        CLAIM_SUBMITTED,
        CLAIM_EVALUATED,
    ],
};

const assignmentStatus = {
    ASSIGNMENT_PENDING,
    ASSIGNMENT_ALLOWED,
    ASSIGNMENTS_VIEWED,
    ASSIGNMENT_SUBMITTED,
    ASSIGNMENT_EVALUATED,
    values: [
        ASSIGNMENT_PENDING,
        ASSIGNMENT_ALLOWED,
        ASSIGNMENTS_VIEWED,
        ASSIGNMENT_SUBMITTED,
        ASSIGNMENT_EVALUATED,
    ],
};

const chatbotStatus = {
    CHATBOTS_PENDING,
    CHATBOTS_PASSED,
    values: [CHATBOTS_PENDING, CHATBOTS_PASSED],
};

const status = {
    CHATBOTS_PENDING,
    CHATBOTS_PASSED,
    ASSIGNMENT_ALLOWED,
    ASSIGNMENTS_VIEWED,
    ASSIGNMENT_SUBMITTED,
    ASSIGNMENT_EVALUATED,
    CLAIM_SUBMITTED,
    CLAIM_EVALUATED,
    values: [
        CHATBOTS_PENDING,
        CHATBOTS_PASSED,
        ASSIGNMENT_ALLOWED,
        ASSIGNMENTS_VIEWED,
        ASSIGNMENT_SUBMITTED,
        ASSIGNMENT_EVALUATED,
        CLAIM_SUBMITTED,
        CLAIM_EVALUATED,
    ],
};

const userAssessment = new Schema({
    // reference of attempted assessment - magic link id
    assessment: {
        type: Schema.Types.ObjectId,
        ref: 'assessmentmagiclinks',
        required: true,
    },

    // company which issued the magic link
    company: {
        type: Schema.Types.ObjectId,
        ref: 'company_names',
        required: true,
    },


    // chatbot related fields
    chatbotSkills: { type: Array, required: false },
    validatedChatbotSkills: [validatedChatbotSkillsSchema],
    chatbotStatus: {
        type: String,
        enum: chatbotStatus.values,
        default: chatbotStatus.CHATBOTS_PENDING,
    },

    // assignment related fields
    assignmentSkills: [assignmentSkillsSchema], // no use for now - remove this comment if you use it
    assignmentStatus: {
        type: String,
        enum: assignmentStatus.values,
        default: assignmentStatus.ASSIGNMENT_PENDING,
    },
    isAssignmentIndorsed: {
        type: Boolean,
    },
    userAssignmentId: { type: String },
    assignmentAllowedAt: {
        type: Number,
    },
    assignmentAllowedBy: {
        type: Schema.Types.ObjectId,
        ref: 'users',
    },
    assignmentsViewedAt: {
        type: Number,
    },

    // claim related fields
    claimSkills: [{ type: Schema.Types.ObjectId, ref: 'skills' }], // Not needed - remove this comment if you use it
    claimStatus: { type: String, enum: claimStatus.values },
    isClaimIndorsed: { type: Boolean },
    claimId: { type: Schema.Types.ObjectId, ref: 'claims' },

    // started user
    startedBy: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    startedAt: {
        type: Number,
    },

    // main status for magic link attempt of the user
    status: {
        type: String,
        enum: status.values,
        required: true,
        default: status.CHATBOTS_PENDING,
    },

    // keep track of updates sent to companies/candidates
    notification: [{
        channel: { type: String, default: 'email' },
        notifiedAt: { type: Number, default: Date.now },
        purpose: { type: String },
        to: [{
            address: { type: String },
            userId: {
                type: Schema.Types.ObjectId,
                ref: 'users',
            },
            role: { type: String },
        }],
    }],

    // This property is inherited from magic links
    autoAllowedAssignment: { type: Boolean, default: false },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = userAssessment;
module.exports.status = status;
