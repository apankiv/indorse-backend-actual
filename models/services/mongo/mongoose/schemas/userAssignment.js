const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const STARTED = 'started';
const SUBMITTED = 'submitted';
const EVALUATED = 'evaluated';

const states = {
    STARTED,
    SUBMITTED,
    EVALUATED,
    values: [STARTED, SUBMITTED, EVALUATED],
};

const userAssignment = new Schema(
    {
        assignment_id: String,
        owner_id: String,
        proof: String,
        started_on: Number,
        claim_id: String,
        finished_on: String, // Introdcued in ID-1426
        skills: [
            new Schema({
                skillTag: String,
                skill: { type: Schema.Types.ObjectId, ref: 'skills' }, // ref has to be whatever is passed to mongoose.model() for skill
                level : String,
                validated: Boolean,
            }),
        ],
        status: {
            type: String,
            enum: states,
            required: true,
        },
        reminderSentAt: { // introduced in ID-
            type: Number,
        },
        startedForUserAssessment: { type: Schema.Types.ObjectId, ref: 'userassessments' }, // ref has to whatever is passed to mongoose.model() for userassessments
    },
    { runSettersOnQuery: true },
);

module.exports.schema = userAssignment;

module.exports.states = states;
