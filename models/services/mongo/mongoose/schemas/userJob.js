const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const userJobs = new Schema({
    user :{
        type:String,
        required : true
    },

    job :{
        type: String,
        required: true
    },

    status: {
        type: String,
        enum: ['user_applied', 'user_pending_validations','user_can_apply'],
    },
   
    appliedAt: {
        type: Number,
    },
    startedApplicationAt: {
        type: Number,
    } 
})
module.exports.schema = userJobs;
