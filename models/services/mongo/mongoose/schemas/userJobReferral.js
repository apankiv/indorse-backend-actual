const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const userJobReferral = new Schema({
    userId :{
        type:String,
        required : true
    },
    jobId :{
        type: String,
        required: true
    },
    candidateEmail :{
        type : String,
        required: true
    },
    candidateName :{
        type: String,
        required: true
    },
    candidateUserId : {
        type: String,        
    },    
    referredAt: {
        type: Number,
        required: true
    }   
})
module.exports.schema = userJobReferral;
