const mongoose = require('../mongooseDB');

const { Schema } = mongoose;

const UPDATE_BIO = 'update_bio';
const UPDATE_PROFILE_PIC = 'update_profile_pic';
const FIRST_CHATBOT_EVALUATION = 'first_chatbot_evaluation';
const FIRST_ASSIGNMENT_EVALUATION = 'first_assignment_evaluation';
const taskTypes = {
    UPDATE_BIO,
    UPDATE_PROFILE_PIC,
    FIRST_CHATBOT_EVALUATION,
    FIRST_ASSIGNMENT_EVALUATION,
    values: [
        UPDATE_BIO,
        UPDATE_PROFILE_PIC,
        FIRST_CHATBOT_EVALUATION,
        FIRST_ASSIGNMENT_EVALUATION,
    ],
};

const userTask = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
        unique: true,
    },

    taskList: [new Schema({
        taskType: {
            type: String,
            enum: taskTypes.values,
            required: true,
        },
        completedAt: {
            type: Number,
        },
        isHidden: { type: Boolean, default: false },
    })],
    showAssignedTasks: { type: Boolean, default: true },
}, { usePushEach: true }, { runSettersOnQuery: true });

module.exports.schema = userTask;

module.exports.taskTypes = taskTypes;
