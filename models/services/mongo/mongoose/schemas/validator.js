const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const TIER1 = '_1';
const TIER2 = '_2';
const TIER3 = '_3';

const tiers = {
    TIER1,
    TIER2,
    TIER3,
    values: [TIER1, TIER2, TIER3],
};


const validator = new Schema({
    user_id: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    ethaddress : {
        type: String,
        required: true
    },
    vote_participation : Number,
    indorsed : Number,
    flagged: Number,
    rewards : Number,
    consensus : Number,
    consensus_percentage: Number,
    invited: Number,
    global_stats : Object,
    daily_stats : Object,
    weekly_stats : Object,
    monthly_stats : Object,
    yearly_stats : Object,
    reputation_data : [new Schema({
        params : {
            totalUpVotes : Number,
            totalDownVotes : Number,
            totalVotes  : Number,
            totalHides : Number
        },
        score_params : {
            vote_score : Number,
            hide_score : Number,
            downvote_score : Number,
            upvote_score : Number
        },
        score: Number,
        start_timestamp : Number,
        end_timestamp : Number
    })],
    skills : [String],
    totalDownvote: Number,
    tier: {
        type: String,
        enum: tiers,
        default: tiers.TIER2
    },
    delete: {
        type: Boolean
    }
});




module.exports.schema = validator;
module.exports.tiers = tiers;