const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const validatorRank = new Schema({
    rewardOneWeekRank: [new Schema({
        user_id: {
            type: String,
            required: true
        },
        scoreThisWeek: {
            type: Number,
            required: true
        }

    })],
    timestamp: {
        type: Number,
        required: true
    }
});



module.exports.schema = validatorRank;
