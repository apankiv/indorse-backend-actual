const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const vote = new Schema({
    claim_id: {
        type: String,
        required: true
    },
    voter_id: {
        type: String,
        required: true
    },
    voting_round_id: {
        type: String,
        required: true
    },
    endorsed: {
        type: Boolean
    },
    voted_at: {
        type: Number
    },
    /**
     * Token is received on vote submission to the server, must be hashed and sent to SC
     */
    voting_token: {
        type: String
    },
    tx_hash: {
        type: String
    },
    tx_timestamp: {
        type: Number
    },
    sc_vote_exists: {
        type: Boolean
    },
    paid: {
        type: Boolean
    },
    notified_at: {
        type: Number
    },
    tx_success_timestamp: {
        type: Number
    },
    reward: {
        type: Number
    },
    ind_reward: {
        type: Number
    },
    feedback: {
        type: {
            quality: Number,
            designPatterns: Number,
            gitFlow: Number,
            completion: Number,
            explanation: String,
            yearsOfExperience: String
        }
    },
    skillId: {
        type: Schema.Types.ObjectId, ref: 'skills',
    },
    isVotingRoundOn: {
        type: Boolean
    },
    signature_ref: String, //MonogID of the related signature object
    merkle_ref: String, //MongoID of the merkle tree object set after its creation
    merkelized: Boolean, //Flag stating if the vote/claim has already been included in a merkle tree
    is_signature: Boolean,
    downvote: {
        type: new Schema({
            count: {type: Number, default: 0},
            actionBy: [String],
        }),
        default: new Schema({})
    },
    upvote: {
        type: new Schema({
            count: {type: Number, default: 0},
            actionBy: [String],
        }),
        default: new Schema({})
    },
    hide: {
        type: new Schema({
            isHidden: {type: Boolean, default: false},
            actionBy: [String]
        }),
        default: new Schema({})
    },
    timeTakenForFeedback: { type: String },
    tier : String
});



module.exports.schema = vote;

let pending_vote = "pending_vote";
let vote_missing = "vote_missing";
let indorsed = "indorsed";
let rejected = "rejected";
let signature_missing = "signature_missing";

module.exports.statuses = {
    PENDING_VOTE: pending_vote,
    VOTE_MISSING: vote_missing,
    REJECTED: rejected,
    INDORSED: indorsed,
    SIGNATURE_MISSING : "signature_missing"
};
