const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const voteSignatures = new Schema({
    payload: {
        type: new Schema({
            claim_id: {
                type: String,
                required: true
            },
            decision: {
                type: String,
                required: true
            },
            timestamp: {
                type: Number,
                required: true
            },
            skill: {
                type: String,
                required: true
            },
            version: {
                type: String,
                required: true
            }
        }, { _id: false }),
        required: true
    },

    signature: {
        type: new Schema({
            v: {
                type: Number,
                required: true
            },
            r: {
                type: String,
                match: /^0x[0-9aA-fF]{64}$/,
                required: true
            },
            s: {
                type: String,
                match: /^0x[0-9aA-fF]{64}$/,
                required: true
            }
        }, { _id: false }),
        required: true
    },

    user_id: {
        type: String,
        required: true
    },

    ethaddress: {
        type: String,
        match: /^(0x)?[0-9aA-fF]{40}$/,
        required: true
    },
    merkelized : {
        type : Boolean
    }
}, { usePushEach: true }, { runSettersOnQuery: true });


module.exports.schema = voteSignatures;