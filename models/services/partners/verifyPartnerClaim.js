const request = require('request');
const errorUtils = require('../error/errorUtils');

// clientApp - clientApp object
// userId - userId sent by clientApp, used by them to identify the user
// state - one time password
exports.verifyPartnerClaim = async function verifyPartnerClaim(clientApp, userId, state) {
    if (!clientApp) errorUtils.throwError('ClientApp not found', 404);

    const { verifyPartnerClaim: verifyPartnerClaimHook } = clientApp.hooks || {};
    if (!verifyPartnerClaimHook) {
        errorUtils.throwError(
            `verifyClientAppClaim hook not configured for Client App ${
                clientApp.display_name
            } (id: ${clientApp._id.toString()})`,
            404,
        );
    }

    // eslint-disable-next-line no-return-await
    return await new Promise((resolve, reject) => {
        const requestOptions = {
            url: verifyPartnerClaimHook.url,
            method: verifyPartnerClaimHook.method,
            json: true,
        };

        if (requestOptions.method === 'GET') {
            requestOptions.qs = { userId, state };
        } else {
            requestOptions.body = { userId, state };
        }

        request(requestOptions, (error, response, result) => {
            if (error) {
                reject(error);
            } else if (response.statusCode === 200) {
                if (result && result.success && result.success.toString() === 'true') {
                    resolve(true);
                } else {
                    reject(errorUtils.createError('Partner claim could not be verified, response is 200 but success flag is not true in returned from verify claim hook result', 403));
                }
            } else {
                reject(errorUtils.createError('Partner claim could not be verified', 403));
            }
        });
    });
};
