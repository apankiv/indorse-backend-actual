const logger = require('../common/logger').getLogger('error');
const rabbitMq = require('./rabbitMq');

/*
    Publish to exchange. Routing_key name will be equal to exchange using this function.
 */
module.exports.publishToExchange = async function publishToExchange(exchangeName, message) {
    try {
        let amqpConnection = await rabbitMq.getRabbitMq();

        await new Promise(async (res, rej) => {
            try {
                amqpConnection.exchange(exchangeName, {
                    type: 'fanout',
                    autoDelete: false,
                }, (exchange, error) => {
                    if (error) rej(error);

                    console.log('Creating a queue', exchangeName);
                    amqpConnection.queue(exchangeName, (queue) => {
                        console.log('Binding the queue to the exchange');
                        queue.bind(exchangeName, 'myRoutingKey', (_, errorInQueue) => {
                            if (errorInQueue) rej(errorInQueue);

                            console.log('Publishing message to exchange', exchangeName);
                            exchange.publish(exchangeName, message, {});

                            console.log('Message sent to RabbitMQ');
                            res();
                        });
                    });
                });
            } catch (error) {
                rej(error);
            }
        });
    } catch (err) {
        logger.error(err);
    }
};

