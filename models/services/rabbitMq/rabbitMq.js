const amqp = require('amqp');
const settings = require('../../settings');

let rabbitMqConnection;

async function connectToRabbitMq() {
        return new Promise((res, rej) => {
            try {
                let amqpConnection = amqp.createConnection(settings.RABBITMQ_CONFIG);

                amqpConnection.on('error', function (err) {
                    console.log('Error from rabbitMQ connection ' + err);
                    rej(err)
                });

                amqpConnection.on('ready', function () {
                    rabbitMqConnection = amqpConnection;
                    console.log('Indorse Backend to RabbitMQ connection has established');
                    res()
                })
            } catch (error) {
                rej(error)
            }
        })
}

module.exports.getRabbitMq = async function getRabbitMq() {
    if(!rabbitMqConnection){
        await connectToRabbitMq();
    }
    return rabbitMqConnection;
}