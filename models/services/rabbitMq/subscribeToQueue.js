const server = require('../../../server');
const rabbitMq = require('./rabbitMq');


module.exports.subscribeToQueue = async function subscribeToQueue(queueName, watcher) {
    let amqpConnection = await rabbitMq.getRabbitMq();

    console.log('Connecting to queue', queueName);
    await new Promise(async (res, rej) => {
        try {
            amqpConnection.queue(queueName, null, function(queue, error) {
                if (error) rej(error)

                queue.subscribe(watcher);
                res()
            });
        } catch(error) {
            rej(error)
        }
    })
}