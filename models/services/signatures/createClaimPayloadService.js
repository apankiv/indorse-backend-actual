const timestampService = require('../common/timestampService');
const claims = require('../mongo/mongoose/schemas/claims');
const mongooseUserAssignmentRepo = require('../mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../mongo/mongoose/mongooseSkillRepo');
const getClaimDomainSchemaService = require('./getClaimDomainSchemaService');
const getClaimSchemaService = require('./getClaimSchemaService');
const getClaimDomainService = require('./getClaimDomainService');

exports.createPayload = async function createPayload(claim, validatorAddresses) {
    const clientId = (claim.clientApp && claim.clientApp.clientId) || '';
    const userId = (claim.clientApp && claim.clientApp.userId) || '';
    const claimant = claim.ownerid || `${clientId}_${userId}`;
    let skills = '';

    if (claim.type === claims.types.ASSIGNMENT) {
        let userAssignment = await mongooseUserAssignmentRepo.findOneById(claim.user_assignment_id);

        for (let assignmentSkill of userAssignment.skills) {
            let skill = await mongooseSkillRepo.findOneById(assignmentSkill.skill.toString());
            skills += skill.name + ','
        }

        skills = skills.substring(0, skills.length - 1);
    } else {
        skills = claim.title.toLowerCase()
    }

    let voters = '';

    for(let ethaddress of validatorAddresses) {
        voters += ethaddress + ',';
    }

    voters = voters.substring(0, voters.length - 1);

    let claimMsg = {
        claimant_id: claimant,
        proof: claim.proof,
        timestamp: timestampService.createTimestamp(),
        deadline: claim.sc_deadline,
        skills: skills,
        kind : claim.type,
        voters : voters,
        claim_id : claim._id.toString(),
        version : "1"
    };

    let payload = {};
    let types = {};

    types.EIP712Domain = await getClaimDomainSchemaService.getSchema();
    types.Claim = await getClaimSchemaService.getSchema();

    payload.types = types;
    payload.domain = await getClaimDomainService.getDomain();
    payload.primaryType = "Claim";
    payload.message = claimMsg;

    return payload;
};
