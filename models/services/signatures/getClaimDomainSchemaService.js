const claimSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/claimSignatureVerifierContract');
const schemaToJsonParser = require('../../../smart-contracts/services/common/schemaToJsonParser');

exports.getSchema = async function getSchema() {
   let schemaStr = await claimSignatureVerifierContract.getEIP712_DOMAIN();
   return schemaToJsonParser.parse(schemaStr);
};
