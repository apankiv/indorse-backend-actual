const claimSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/claimSignatureVerifierContract');
const mongooseContractsRepo = require('../mongo/mongoose/mongooseContractsRepo');

let cached;


exports.getDomain = async function getDomain() {
    let contract = await mongooseContractsRepo.findOne({contract_name: "ClaimSignatureVerifier"});

    if (!cached) {
        cached = {
            name: await claimSignatureVerifierContract.getDAPP_NAME(),
            version: await await claimSignatureVerifierContract.getVERSION(),
            chainId: parseInt(await claimSignatureVerifierContract.getChainId(), 10),
            verifyingContract: contract.contract_address,
            salt: await claimSignatureVerifierContract.getSalt()
        }
    }

    return cached;
};
