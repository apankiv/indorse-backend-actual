const claimSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/claimSignatureVerifierContract');
const schemaToJsonParser = require('../../../smart-contracts/services/common/schemaToJsonParser');

exports.getSchema = async function getSchema() {
    let schemaStr = await claimSignatureVerifierContract.getCLAIM_TYPE();
    return schemaToJsonParser.parse(schemaStr);
};
