const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const schemaToJsonParser = require('../../../smart-contracts/services/common/schemaToJsonParser');

exports.getSchema = async function getSchema() {
   let schemaStr = await voteSignatureVerifierContract.getEIP712_DOMAIN();
   return schemaToJsonParser.parse(schemaStr);
};
