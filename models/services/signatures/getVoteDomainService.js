const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const mongooseContractsRepo = require('../mongo/mongoose/mongooseContractsRepo');

let cached;

exports.getDomain = async function getDomain() {
    let contract = await mongooseContractsRepo.findOne({contract_name: "VoteSignatureVerifier"});

    if (!cached) {
        cached = {
            name: await voteSignatureVerifierContract.getDAPP_NAME(),
            version: await await voteSignatureVerifierContract.getVERSION(),
            chainId: parseInt(await voteSignatureVerifierContract.getChainId(), 10),
            verifyingContract: contract.contract_address,
            salt: await voteSignatureVerifierContract.getSalt()
        }
    }

    return cached;
};
