const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const schemaToJsonParser = require('../../../smart-contracts/services/common/schemaToJsonParser');

exports.getSchema = async function getSchema() {
    let schemaStr = await voteSignatureVerifierContract.getVOTE_TYPE();
    return schemaToJsonParser.parse(schemaStr);
};
