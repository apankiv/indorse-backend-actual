const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const utils = require('./skillUtils');

module.exports.createSkill = async function createSkill({ form }) {
    const { iconData, parentNames, ...data } = form;

    const skillInMongo = await mongooseSkillRepo.findOne({
        name: form.name.toLowerCase(),
    });

    if (skillInMongo) {
        errorUtils.throwError('Skill already exists in collection', 400);
    }

    const parentSkillsInMongo = await utils.findExactSkillsByNames(parentNames);
    const parentSkillIdsInMongo = parentSkillsInMongo.map(parentSkill => parentSkill._id);
    data.parents = parentSkillIdsInMongo;

    const skillId = await mongooseSkillRepo.insert(data);
    const skill = { _id: skillId, ...data };

    if (iconData) {
        data.iconUrl = await utils.uploadIconAndSaveSkill(skill._id, iconData);
    }

    return skill;
};
