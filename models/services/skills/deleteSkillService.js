const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseJobsRepo = require('../../services/mongo/mongoose/mongooseJobsRepo');
const errorUtils = require('../../../models/services/error/errorUtils');

module.exports.deleteSkill = async function deleteSkill({ selector }) {
    const skill = await mongooseSkillRepo.findOne(selector);
    if (!skill) errorUtils.throwError('Skill not found', 404);

    const userWithSkill = await mongooseUserRepo.findOne({
        'skills.skills._id': skill._id,
    });
    if (userWithSkill) {
        errorUtils.throwError(
            'This skill is linked to one or more users, cannot be deleted',
            400,
        );
    }

    const jobWithSkill = await mongooseJobsRepo.findOne({
        'skills.id': skill._id,
    });
    if (jobWithSkill) {
        errorUtils.throwError(
            'This skill is linked to one or more jobs, please delete the skills in the jobs first',
            400,
        );
    }

    return skill;
};
