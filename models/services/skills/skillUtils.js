const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const s3Service = require('../../../models/services/blob/s3Service');

module.exports.uploadIconAndGetUrl = async function uploadIconAndGetUrl(skillId, iconData) {
    const iconFileS3Key = `skill_icon_${skillId}`;
    await s3Service.uploadFileFromDataURL(iconFileS3Key, iconData);

    const iconUrl = s3Service.getUploadedFileUrl(iconFileS3Key);
    return iconUrl;
};

module.exports.uploadIconAndSaveSkill = async function updateIcon(skillId, iconData) {
    const iconUrl = await this.uploadIconAndGetUrl(skillId, iconData);
    const updatedSkill = await mongooseSkillRepo.update(
        { _id: skillId },
        { iconUrl },
    );
    return updatedSkill;
};

module.exports.findExactSkillsByIds = async function findExactSkillsByIds(ids) {
    if (ids && ids.length > 0) {
        const normalizedIds = ids.map(parentId => parentId.toString());
        const skillsInMongo = await mongooseSkillRepo.findAll({
            _id: {
                $in: normalizedIds,
            },
        });

        const skillIdsInMongo = skillsInMongo.map(skill => skill._id.toString());
        const skillIdsNotInMongo = normalizedIds.filter(parentId => !skillIdsInMongo.includes(parentId));
        if (
            skillIdsNotInMongo &&
            skillIdsNotInMongo.length > 0
        ) {
            errorUtils.throwError(
                `Some skills not found - ${skillIdsNotInMongo.join(', ')}`,
                400,
            );
        }
        return skillsInMongo;
    }

    return [];
};

module.exports.findExactSkillsByNames = async function findExactSkillsByNames(names) {
    if (names && names.length > 0) {
        const normalizedNames = names.map(parentName => parentName.toLowerCase());
        const skillsInMongo = await mongooseSkillRepo.findAll({
            name: {
                $in: normalizedNames,
            },
        });

        const skillNamesInMongo = skillsInMongo.map(skill => skill.name);
        const skillNamesNotInMongo = normalizedNames.filter(parentName => !skillNamesInMongo.includes(parentName));
        if (
            skillNamesNotInMongo &&
            skillNamesNotInMongo.length > 0
        ) {
            errorUtils.throwError(
                `Some skills not found - ${skillNamesNotInMongo.join(', ')}`,
                400,
            );
        }

        return skillsInMongo;
    }

    return [];
};


module.exports.findValidatableSkills = async function findValidatableSkills(ids){
    if (ids && ids.length > 0) {
        const normalizedIds = ids.map(parentId => parentId.toString());
        const skillsInMongo = await mongooseSkillRepo.findAll({
            _id: {
                $in: normalizedIds,
            },
        });

        //first check if any of these skills are validatable
        let validatableSkills =  skillsInMongo.map((skill) => {
            if (skill.validation && skill.validation.aip == true){
                return skill;
            }
        });

        //find all validatable parents
        for(skill of skillsInMongo){
            if (skill.parents && skill.parents.length > 0){
                const parentSkillsInMongo = await mongooseSkillRepo.findAll({
                    $and: [{_id: {$in: skill.parents}} , {"validation.aip" : true}]
                });
                
                for(parentSkill of parentSkillsInMongo){
                    let found = false;
                    for (validatableSkill of validatableSkills){
                        if(parentSkill._id.toString() === validatableSkill._id.toString()){
                            found = true; 
                        }
                    }
                    if(!found){
                        validatableSkills.push(parentSkill);
                    }
                }
            }
        }
        return validatableSkills;
    }
    return [];
}

module.exports.canUserVoteForParentSkill = async function canUserVoteForParentSkill(userid,skillid){
    //get skill
    let skill = await mongooseSkillRepo.findOneById(skillid);

    if (!skill || !skill.parents || skill.parents.length === 0) {
        return false;
    }
    //get parents
    const parentIds = skill.parents.map(parentId => parentId.toString());

    let allParentSkills = await mongooseSkillRepo.findAll({_id : {$in : parentIds}});
    if(!allParentSkills){
        return false;
    }

    let allParentSkillNames = allParentSkills.map(skill => skill.name.charAt(0).toUpperCase() + skill.name.slice(1));  

    //check if user is a validator for any of those parents
    const validatorExists = await mongooseValidatorRepo.findOne({
        $and: [{ user_id: userid }, { skills: { $in: allParentSkillNames }}]
    });

    if (validatorExists){        
        return true;
    }

    return false;
}

module.exports.canUserVoteForSkillOrParentSkill = async function canUserVoteForSkillOrParentSkill(userid, skillid) {
    //get skill
    let skill = await mongooseSkillRepo.findOneById(skillid);

    if (!skill) return false;

    //get parents
    const parentIds = (skill.parents || []).map(parentId => parentId.toString());

    let allParentSkills = await mongooseSkillRepo.findAll({_id : {$in : parentIds}});

    const skills = [skill].concat(allParentSkills || []);

    let skillNames = skills.map(skill => skill.name.charAt(0).toUpperCase() + skill.name.slice(1));  

    //check if user is a validator for any of those parents
    const validatorExists = await mongooseValidatorRepo.findOne({
        $and: [{ user_id: userid }, { skills: { $in: skillNames }}]
    });

    if (validatorExists){        
        return true;
    }    
    return false;
}

module.exports.allowedBoastSkillsForAIP = async function allowedBoastSkillsForAIP() {
    const criteria = {
        'validation.aip': true,
    };
    const projection = {
        name: 1,
    };
    const skills = await mongooseSkillRepo.findAll(criteria, projection);
    const listOfSkillNames = (skills || []).map(elem => elem.name);
    return listOfSkillNames || [];
};

module.exports.findParentSkillsOfMultipleSkills = async function findParentSkillsOfMultipleSkills(arrayOfIds) {
    let parentSkillArray = [];
    const criteria = {
        _id: {
            $in: arrayOfIds,
        },
    };
    let skillsFromDB = await mongooseSkillRepo.findAll(criteria);
    skillsFromDB = skillsFromDB || [];
    skillsFromDB.forEach((skill) => {
        let { parents: parentsOfSkill } = skill;
        parentsOfSkill = parentsOfSkill || [];
        parentSkillArray = [...parentSkillArray, ...parentsOfSkill];
    });
    return parentSkillArray;
};
