const { ObjectID } = require('mongodb');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const utils = require('./skillUtils');

module.exports.updateSkill = async function updateSkill({ selector, form }) {
    const { iconData, parentNames, ...data } = form;

    let skill = await mongooseSkillRepo.findOne(selector);
    if (!skill) errorUtils.throwError('Skill not found', 404);

    if (iconData) {
        data.iconUrl = await utils.uploadIconAndGetUrl(skill._id, iconData);
    }

    const parentSkillsInMongo = await utils.findExactSkillsByNames(parentNames);
    const parentSkillIdsInMongo = parentSkillsInMongo.map(parentSkill => parentSkill._id);
    data.parents = parentSkillIdsInMongo;

    skill = await mongooseSkillRepo.update({ _id: ObjectID(skill._id.toString()) }, data);

    return skill;
};
