const ethUtils = require('ethereumjs-util');

/**
 * Signature validation. Receiving  signature (ethaddress signed by private key) and ethaddress.
 *
 * @param signature
 * @param ethAddressMsg
 * @returns {boolean}
 */
exports.validateSignature = function validateSignature(signature, ethAddressMsg) {

    let bufR = signature.buf_r;
    let bufS = signature.buf_s;
    let bufV = signature.buf_v;

    let buffR = new Buffer(bufR, 'base64');
    let buffS = new Buffer(bufS, 'base64');

    let message = ethUtils.toBuffer(ethAddressMsg);
    let msgHash = ethUtils.hashPersonalMessage(message);

    let publicKey = ethUtils.ecrecover(msgHash, bufV, buffR, buffS);

    let ethAddressSigned = ethUtils.pubToAddress(publicKey);
    let ethAddressSignedHex = ethUtils.bufferToHex(ethAddressSigned);

    return ethAddressMsg === ethAddressSignedHex;
};
