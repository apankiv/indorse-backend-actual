const config = require('config');
const errorUtils = require('../error/errorUtils');
const linkedInAuth = require('google-auth-library');
const clientID = config.get('auth.githubClientID');
const clientSecret = config.get('auth.githubClientSecret');
const request = require('request');
const logger = require("../common/logger").getLogger();
const mongooseUserRepo = require("../mongo/mongoose/mongooseUserRepo");
const mongooseUserGithubRepo = require("../mongo/mongoose/mongooseUserGithubRepo");
const mongooseUserClaimGithubRepo = require("../mongo/mongoose/mongooseClaimGithubsRepo");


exports.getGithubUserAndRepoData = async function getGithubUserAndRepoData(accessToken) {
    let email = await getEmailFromGithub(accessToken);
    let query = `query { ` +
        `viewer { ` +
        `  login ` +
        ` id ` +
        ` bio ` +
        `name ` +
        `company ` +
        `createdAt ` +
        `avatarUrl ` +
        `isBountyHunter ` +
        `isHireable ` +
        `isEmployee ` +
        `websiteUrl ` +
        `  repositories (first:100){ ` +
        `    edges{` +
        `     node { ` +
        `name ` +
        `isFork ` +
        `isMirror ` +
        `projectsUrl ` +
        `description ` +
        `isArchived ` +
        `isPrivate ` +
        `forkCount ` +
        `createdAt ` +
        `updatedAt ` +
        `owner {id} ` +
        `stargazers(){ totalCount } ` +
        `    owner { ` +
        `     login` +
        `  } ` +
        ` languages(first: 7){ ` +
        `  nodes{ ` +
        `   name ` +
        `}} ` +
        `}} ` +
        `} ` +
        `} ` +
        `} `;
    let queryToSend = `{"query" : "${query}" } `;

    const data = await new Promise((resolve, reject) => {
        request.post({
            headers: {
                'content-type': 'application/json',
                'Authorization': 'bearer ' + accessToken,
                'User-Agent': 'Indorse'
            },
            url: 'https://api.github.com/graphql',
            body: queryToSend
        }, function (error, response, body) {
            if (error) {
                logger.debug('Github API responded with error ' + error);
                reject("Github authentication failed");
            }
            let resp = JSON.parse(body).data;
            if (!resp) {
                logger.debug('Github API responded with error ' + body);
                reject("Github authentication failed");
            }
            resolve(resp);
        });
    })

    data.viewer.email = email;
    return data.viewer;
}

function extractGithubUrl(proofUrl) {
    //return false if not github
    let pattern = new RegExp('github.com/');
    if (!pattern.test(proofUrl)) {
        return false;
    }
    let urlSplit = proofUrl.split(pattern);
    let extractRepoOwnerName = urlSplit[1].split(new RegExp('/'));
    if (!extractRepoOwnerName[0] || !extractRepoOwnerName[1]) {
        return false;
    }
    return [extractRepoOwnerName[0], extractRepoOwnerName[1]];
}


exports.parseGithubUrlExtractClaimantData = async function parseGithubUrlExtractClaimantData(githubUid, proofUrl, claimId) {

    let repoOwner, repoName;
    try {
        let repoInfo = extractGithubUrl(proofUrl);
        repoOwner = repoInfo[0];
        repoName = repoInfo[1];
        // Check if URL is github
        if (!repoOwner) {
            logger.debug("Invalid Github URL: " + proofUrl);
            return; // silently return
        }

        // Check if claimant is authenticated with Github. If not, return.
        if (!githubUid) {
            return;
        }

        let userGithubs = await mongooseUserGithubRepo.findOneByGithubUid(githubUid);
        if (!userGithubs) {
            logger.debug("User Github data not found");
            return;
        }
        let claimantLogin = userGithubs.login;

        let recentCommits, isClaimantOwner, isFork, createdAt, forkUrl;
        let repoDetailSnapShot = {
            claim_id: claimId,
            githubUsername: claimantLogin,
            isReturnAllCommit: false,
            githubRepoURL: proofUrl
        }

        let statResponse;
        for (var i = 0; i < 5; i++) {
            statResponse = await getRepoStats(repoOwner, repoName, claimantLogin);
            if (statResponse !== "retry") {
                i = 5;
            }
            await wait(3000);
            logger.debug("Retrying fetching repository statastics");
        }

        if (statResponse) {
            let totalCommit, totalLineAdded, totalLineDeleted, commitHistoryArray;
            [totalCommit, totalLineAdded, totalLineDeleted, commitHistoryArray] = statResponse;
            repoDetailSnapShot.totalNumberOfCommits = totalCommit;
            repoDetailSnapShot.totalNumberOfLinesAdded = totalLineAdded;
            repoDetailSnapShot.totalNumberOfLinesDeleted = totalLineDeleted;
            repoDetailSnapShot.commitHistoryArray = commitHistoryArray;
        }

        let claimantProfile = await getProfileURL(repoOwner);
        if (claimantProfile) {
            repoDetailSnapShot.claimantProfileURL = claimantProfile;
        }
        let totalCommitHistory = await getRecentCommits(repoOwner, repoName, claimantLogin);

        repoDetailSnapShot.recentFiveCommitLinks = totalCommitHistory.slice(0, 5);

        if (totalCommitHistory.length > 0) {
            let latestCommitDate = Date.parse(totalCommitHistory[0].commitTimestamp);
            let oldestCommitDate = Date.parse(totalCommitHistory[totalCommitHistory.length - 1].commitTimestamp);
            let diff = latestCommitDate - oldestCommitDate;
            let fourWeeksInMilliSecs = 2419200000;
            if (diff < fourWeeksInMilliSecs || diff == 0) {
                repoDetailSnapShot.commitHistoryAllArray = totalCommitHistory.map(a => Date.parse(a.commitTimestamp));
                repoDetailSnapShot.isReturnAllCommit = true;
            }
        }

        [isClaimantOwner, isFork, createdAt, forkUrl] = await getRepoDetails(repoOwner, repoName, claimantLogin);
        repoDetailSnapShot.isClaimantOwner = isClaimantOwner;

        if (typeof isFork !== 'undefined') {
            repoDetailSnapShot.isFork = isFork;
        }
        if (typeof forkUrl !== 'undefined') {
            repoDetailSnapShot.forkUrl = forkUrl;
        }
        if (createdAt) {
            repoDetailSnapShot.createdAt = Date.parse(createdAt);
        }


        await mongooseUserClaimGithubRepo.insert(repoDetailSnapShot);
        logger.debug("Successfully store repository detail for claim " + JSON.stringify(repoDetailSnapShot));
    } catch (error) {
        errorUtils.throwError("Error - parseGithubUrlExtractClaimantData: " + error.message, 400)
    }

}


/*
    https://developer.github.com/v3/repos/statistics/#get-contributors-list-with-additions-deletions-and-commit-counts

    3 types of outputs
    1. [totalCommit , totalLineAdded, totalLineRemoved, commitHistoryArray]  (when parsed correctly)
    2. false (When claimant github username is not found among the contributor list of the repo)
    3. "retry" (When github server returend 202)
 */

async function getRepoStats(repositoryOwner, repositoryName, claimantGithubLogin) {
    let url = 'https://api.github.com/repos/' + repositoryOwner + '/' + repositoryName + '/stats/contributors';
    let totalNumberCommits = 0;
    let totalLineAdded = 0;
    let totalLineRemoved = 0;


    const stats = await new Promise((resolve, reject) => {
        request.get({
            url,
            headers: {
                'content-type': 'application/json',
                'User-Agent': 'Indorse'
            }
        }, function (error, response, body) {
            if (error) {
                logger.debug("Error in getting repository contributor stats " + JSON.stringify(error));
                reject("Error in getting repository contributor stats");
            }
            response = JSON.stringify(response)
            let statusCode = JSON.parse(response).statusCode;

            if (statusCode === 202) { // 202 is being returned when data is still being computed
                resolve("retry");
            }

            if (statusCode !== 200) {
                logger.debug("Error in fetching repository data - Reference - repoowner " + repositoryOwner + " reponame " + repositoryName + " claimant github username: " + claimantGithubLogin);
                logger.debug("Github response: " + response);
                reject("Unable to fetch repository data from Github");
            }

            resolve(body);
        })
    });

    if (stats === "retry") {
        logger.debug("202 returned - make a request again");
        return stats;
    }

    let statsJson = JSON.parse(stats);

    let isContributorFound = false;
    let commitHistoryArray = [];
    // Array of number of commits per week
    for (var i = 0; i < statsJson.length; i++) {
        var contributor = statsJson[i];
        if (claimantGithubLogin === contributor.author.login) {
            isContributorFound = true;
            totalNumberCommits = contributor.total;
            var commitHistory = contributor.weeks;
            for (var j = 0; j < commitHistory.length; j++) {
                var curWeek = commitHistory[j];

                if (curWeek.c !== 0) {
                    let commitHistoryObj = {
                        week: parseInt(curWeek.w) * 1000,
                        noOfCommits: parseInt(curWeek.c)
                    };
                    commitHistoryArray.push(commitHistoryObj);
                }

                totalLineAdded = totalLineAdded + parseInt(curWeek.a);
                totalLineRemoved = totalLineRemoved + parseInt(curWeek.d);
            }
        }
    }

    if (!isContributorFound) {
        logger.debug('Claimant Github username = ' + claimantGithubLogin + ' doesnt match with any of contributors');
        return false;
    }

    return [totalNumberCommits, totalLineAdded, totalLineRemoved, commitHistoryArray];
}

/*
    Output: an array of 5 commit links
 */
async function getRecentCommits(repositoryOwner, repositoryName, claimantGithubLogin) {
    let url = 'https://api.github.com/repos/' + repositoryOwner + '/' + repositoryName + '/commits?author=' + claimantGithubLogin;

    const commits = await new Promise((resolve, reject) => {
        request.get({
            url,
            headers: {
                'content-type': 'application/json',
                'User-Agent': 'Indorse'
            }
        }, function (error, response, body) {
            if (error) {
                logger.debug("Error in getting repository contributor stats " + JSON.stringify(error));
                reject("Error in getting repository contributor stats");
            }
            response = JSON.stringify(response)
            logger.debug("Recording response to count rate availability " + response);
            let statusCode = JSON.parse(response).statusCode;

            if (statusCode !== 200) {
                logger.debug("Error in fetching repository data - Reference - repoowner " + repositoryOwner + " reponame " + repositoryName + " claimant github username: " + claimantGithubLogin);
                logger.debug("Github response: " + response)
                reject("Unable to fetch repository data from Github");
            }

            resolve(body);
        })
    });

    let commitsJson = JSON.parse(commits);
    let recentCommits = [];
    for (var k = 0; k < commitsJson.length; k++) {
        var curCommit = commitsJson[k];

        if (curCommit) {
            let curCommitObj = {
                commitUrl: curCommit.html_url
            }
            if (curCommit.commit) {
                curCommitObj.commitMessage = curCommit.commit.message;
                curCommitObj.commitTimestamp = curCommit.commit.author.date;
            }
            recentCommits.push(curCommitObj);
        }

    }
    return recentCommits;
}

async function getProfileURL(repoOwnerLogin) {
    let url = 'https://api.github.com/users/' + repoOwnerLogin;

    const userProfile = await new Promise((resolve, reject) => {
        request.get({
            url,
            headers: {
                'content-type': 'application/json',
                'User-Agent': 'Indorse'
            }
        }, function (error, response, body) {
            if (error) {
                logger.debug("Error in getting user profile " + JSON.stringify(error));
                reject("Error in getting user profile");
            }
            response = JSON.stringify(response)

            let statusCode = JSON.parse(response).statusCode;

            if (statusCode !== 200) {
                logger.debug("Github response: " + response)
                reject("Unable to fetch user data");
            }

            if (!response) {
                reject("Unable to fetch user data - response null");
            }
            resolve(body);
        })
    });
    let parsedResponse = JSON.parse(userProfile);
    return parsedResponse.avatar_url;


}


async function getRepoDetails(repositoryOwner, repositoryName, claimantGithubLogin) {
    let url = 'https://api.github.com/repos/' + repositoryOwner + '/' + repositoryName;

    const repoDetail = await new Promise((resolve, reject) => {
        request.get({
            url,
            headers: {
                'content-type': 'application/json',
                'User-Agent': 'Indorse'
            }
        }, function (error, response, body) {
            if (error) {
                logger.debug("Error in getting repository details" + JSON.stringify(error));
                reject("Error in getting repository details");
            }
            response = JSON.stringify(response)

            let statusCode = JSON.parse(response).statusCode;

            if (statusCode !== 200) {
                logger.debug("Error in fetching repository data - Reference - repoowner " + repositoryOwner + " reponame " + repositoryName + " claimant github username: " + claimantGithubLogin);
                logger.debug("Github response: " + response)
                reject("Unable to fetch repository data from Github");
            }

            resolve(body);
        })
    });

    let repoDetailJson = JSON.parse(repoDetail);
    if (!repoDetailJson) {
        return false;
    }

    let isClaimantOwner = false;
    if (repoDetailJson.owner && repoDetailJson.owner.login === claimantGithubLogin) {
        isClaimantOwner = true;
    }
    let forkUrl;
    if (repoDetailJson.parent) {
        forkUrl = repoDetailJson.parent.full_name;
    }
    let isFork = repoDetailJson.fork;
    let createdAt = repoDetailJson.created_at;

    return [isClaimantOwner, isFork, createdAt, forkUrl];
}


/*
    Input: Object obtained from Github API AND user object
    Output: Void
 */
exports.updateGithubDataOfUser = async function updateGithubDataOfUser(gitDataObj, user) {
    let githubUid = gitDataObj.id;
    let githubUidFromUser = user.github_uid;

    if (githubUid !== githubUidFromUser) {
        errorUtils.throwError('Github UID mismatch', 500);
    }

    let userGithubData = await mongooseUserGithubRepo.findOneByGithubUid(githubUid);

    if (!userGithubData) {
        // Create a new one
        let id = await mongooseUserGithubRepo.insert(gitDataObj);
    } else {
        await mongooseUserGithubRepo.update({id: githubUid}, gitDataObj);
    }
}


/**
 *
 * @param idToken
 * @returns email (promise object)
 */

async function getEmailFromGithub(access_token) {

    let url = 'https://api.github.com/user/emails?access_token=' + access_token;

    const email = await new Promise((resolve, reject) => {

        request.get({
            url,
            headers: {'Accept': 'application/json', 'User-Agent': 'Indorse'}
        }, function (error, response, body) {
            if (error) {
                reject(error);
            }
            body = body.substring(1, body.length - 1);
            let emails = body.split(",{");

            if (body[0] == "{" && body[body.length - 1] == "}") {
                let email = JSON.parse(emails[0]);
                resolve(email.email);
            }
            else {
                reject("Github authentication failed");
            }


        })
    });

    if (!email) {
        errorUtils.throwError('Github authentication failed', 400);
    }

    //profile.email = email;

    return email;
};

exports.exchangeAuthorizationCode = async function exchangeAuthorizationCode(code, state, redirect_uri) {


    let url = 'https://github.com/login/oauth/access_token?client_id=' + clientID + '&redirect_uri=' + redirect_uri + '&client_secret=' + clientSecret + '&code=' + code + '&state=' + state;


    const accessToken = await new Promise((resolve, reject) => {

        request.post({
            url,
            headers: {'Accept': 'application/json', 'User-Agent': 'Indorse'}
        }, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let content = JSON.parse(body);
            if (content.error) {
                resolve();
            }
            resolve(content.access_token);

        })
    });
    if (!accessToken) {
        errorUtils.throwError('Github authentication failed', 400);
    }

    return accessToken;
};

function wait(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}