const mongooseUserRepo = require("../mongo/mongoose/mongooseUserRepo");
const cryptoUtils = require('../common/cryptoUtils');
const errorUtils = require('../error/errorUtils');
const fbConfidenceScore = require('../../users/auth/social/confidenceScore/facebookConfidenceScore')
const linkedInConfidenceScore = require('../../users/auth/social/confidenceScore/linkedInConfidenceScore')
const civicConfidenceScore = require('../../users/auth/social/confidenceScore/civicConfidenceScore')
const uportConfidenceScore = require('../../users/auth/social/confidenceScore/uportConfidenceScore')
const amplitudeTracker = require('../tracking/amplitudeTracker')

const logger = require("../common/logger").getLogger();

/**
*
* @param inputUid
* @param inputEmail
* @param authType
* @returns {Promise.<*>}
*/
async function findUserByUid(inputUid, authType) {
    if(authType === 'airbitz'){
        return await mongooseUserRepo.findOne({'ethaddress': inputUid}); // Airbitz user doesn't have airbitz_uid but is_airbitz_user
    }
    authType = authType + '_uid';
    return await mongooseUserRepo.findOne({[authType]: inputUid});
};

async function findUserByEmail(inputEmail){
    return await mongooseUserRepo.findOneByEmail(inputEmail);
};

async function linkUserByEmail(user,inputUid, inputEmail, authType){
    if(!user){
        errorUtils.throwError("Account not found, Would you like to create a new account?", 404);         // shuould return 404 so that FE can redirect user to signup page
    }
    if(authType === 'airbitz'){
        await mongooseUserRepo.update({'email': inputEmail}, {'$set': {'ethaddress': inputUid , 'is_airbitz_user' : true}});
        return;
    }
    authType = authType + '_uid';
    await mongooseUserRepo.update({'email': inputEmail}, {'$set': {[authType]: inputUid}});
};

function userCanLogin(user) {
    return user.verified && user.approved;
}

async function setToken(user,inputEmail){
    let token = await cryptoUtils.generateJWT(user);
    user.tokens.push(token);
    await mongooseUserRepo.update({email: inputEmail}, {$set: {tokens: user.tokens}});
    return token;
}

async function updateConfidenceScore(user, inputEmail, socialParams, authType){
    if(authType === 'facebook'){
        await fbConfidenceScore.updateFacebookScore(socialParams,inputEmail);
        logger.debug("Calculating Facebook Score" + inputEmail + "  " + JSON.stringify(socialParams));
    }
    if(authType === 'linkedIn'){
        await linkedInConfidenceScore.updateLinkedInScore(socialParams, inputEmail);
        logger.debug("Calculating LinkedIn Score" + inputEmail + "  " + JSON.stringify(socialParams));
    }
    if (authType === 'civic') {
        await civicConfidenceScore.updateCivicScore(socialParams, inputEmail);
        logger.debug("Calculating Civic Score" + inputEmail + "  " + JSON.stringify(socialParams));
    } 
    if (authType === 'uport') {
        await uportConfidenceScore.updateUportScore(socialParams, inputEmail);
        logger.debug("Calculating uPort Score" + inputEmail + "  " + JSON.stringify(socialParams));
    }         
}


//use AuthType if needed
async function updateAdditionalData(authType,email,additionalData){
    console.log("Updating additional data for linked in profile URL " + email);
    await mongooseUserRepo.update({ 'email': email }, { $set: { linkedInProfileURL: additionalData.profileUrl}});
}

exports.socialLinkTemplate = async function socialLinkTemplate(inputEmail, inputUid, authType, socialParams, res, additionalData=undefined){

    let user = await findUserByUid(inputUid, authType);

    if(user){
        errorUtils.throwError("User with this "+ authType +" account exists", 400);
    }

    user = await findUserByEmail(inputEmail);
    await linkUserByEmail(user, inputUid, inputEmail, authType);
    if (socialParams) {
        await updateConfidenceScore(user, inputEmail, socialParams, authType);
    }

    if (!userCanLogin(user)) {
        errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
    }

    if (additionalData){
        await updateAdditionalData(authType, inputEmail, additionalData)
    }    
    
    let token = await setToken(user,inputEmail);
    amplitudeTracker.publishData('social_link', {type: authType}, {user_id: user._id});

    res.send(200, {
        success: true,
        message: 'User account is linked to '+ authType +' successfully',
        token: token
    });

}

exports.socialLoginTemplate = async function socialLoginTemplate(user, res, authType){

    if (!userCanLogin(user)) {
        errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
    }
    amplitudeTracker.publishData('login', {type: authType}, {user_id: user._id});

    let token = await setToken(user,user.email);
    res.send(200, {
        success: true,
        message: 'User logged in successfully',
        token: token
    });
};

exports.userCanLogin = userCanLogin;
exports.setToken = setToken;
exports.findUserByUid = findUserByUid;
exports.findUserByEmail = findUserByEmail;
exports.linkUserByEmail = linkUserByEmail;
