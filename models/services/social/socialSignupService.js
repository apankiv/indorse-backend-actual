const mongooseUserRepo = require("../mongo/mongoose/mongooseUserRepo");
const errorUtils = require('../error/errorUtils');
const emailService = require("../common/emailService");
const replicationUserRepo = require('../mongo/userRepo');
const cryptoUtils = require('../common/cryptoUtils');
const request = require('request');
const amplitudeTracker = require('../tracking/amplitudeTracker');
const jwt = require('jsonwebtoken');
const config = require('config');
const logger = require('../common/logger').getLogger();

/*
    Following file is using this function
    - googleSignup / linkedInSignup / civicSignup / githubSignup
    Input
        req
        newUserRequest
        authType
        signupFlow - following is a list of possible flows ['signupPage','claimToken']
        Please add a signupFlow value if it is not a default signup. (signing up from the signup page). So that the different flow could be identified by tracker.

 */
exports.completeSocialSignup = async function completeSocialSignup(req, newUserRequest, authType, signupFlow) {
    let uid = authType + '_uid';

    let userInMongo = await mongooseUserRepo.findOne({
        $or: [
            {email: newUserRequest.email},
            {username: newUserRequest.username},
            {[uid]: newUserRequest[uid]}]
    });

    if (userInMongo) {
        errorUtils.throwError("User with this email ,username or uid exists", 400);
    }

    let signupTrackerData = {type: authType, flow: 'signupPage'};
    if(signupFlow === 'claimToken'){
        signupTrackerData.flow = 'claimToken';
    }

    let newUserId = await replicationUserRepo.insertSocial(newUserRequest, authType);
    let newUser = await mongooseUserRepo.findOneById(newUserId);
    let userDataToAmplitude = {signup_date: new Date((newUser.timestamp * 1000) + (8 * 60 * 60 * 1000)).toISOString()}; // converting UTC timestamp to SGT
    req.user_id = newUserId;
    amplitudeTracker.publishData('sign_up', signupTrackerData, req, userDataToAmplitude);

    await emailService.sendWelcomeEmail(newUserRequest.name, newUserRequest.email, newUserRequest.username);

};

exports.getProfileBufferFromUrl = async function getProfileBufferFromUrl(profileImageUrl) {
    return await new Promise((resolve, reject) => {
        request.get(profileImageUrl, {encoding: 'base64'}, function (err, res, body) {
            //process exit here
            if (!err && res.statusCode == 200) {
                image_buffer = new Buffer(body, 'base64');
                resolve(image_buffer);
            } else {
                reject();
            }
        })
    });
};

exports.decodeGithubSignupToken = async function decodeGithubSignupToken(signupToken) {
    let email, githubUid, profileUrl, name, accessToken;
    return new Promise((resolve, reject) => {
        jwt.verify(signupToken, config.get('JWT.jwtSecret'), function (err, decoded) {

            console.log(decoded);
            if (err) {
                logger.debug('Github JWT error' + JSON.stringify(err));
                reject(new Error("Github indAccess Token error"));
            }
            else {
                if (decoded && decoded.email && decoded.githubUid && decoded.profileUrl && decoded.accessToken) {
                    email = decoded.email.toLowerCase();
                    githubUid = decoded.githubUid;
                    profileUrl = decoded.profileUrl;
                    name = decoded.name;
                    accessToken = decoded.accessToken;
                    resolve([email, githubUid, profileUrl, accessToken]);
                } else {
                    logger.debug('Github decoded token error : githubUid ' + githubUid + ' email ' + email + ' profileUrl ' + profileUrl + ' name ' + name);
                    reject(new Error("Github indAccess Token error"));
                }
            }
        });
    })
};