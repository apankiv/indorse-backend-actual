const Amplitude = require('amplitude')
const settings = require('../../settings');
const User = require('../mongo/mongoose/schemas/user');
let amplitude;

if (settings.ENVIRONMENT !== 'test') {
    amplitude = new Amplitude(settings.AMPLITUDE_API_TOKEN);
}


/*
    Please follow following standards
    Input arg1: event_type (event name)
          arg2: event_properties (data that needs to be tracked with event)
          arg3: req (If it is a request coming from FrontEnd, we should always put req object as it is.)
          arg4(optional): user_properties (data that needs to be tracked with user making the request)

    If cron server is making an event on behalf of user, request object should contain user_id of that user. eg. {user_id: requestingUserID}
    If admin is inviting user_A to become an advisor, you should still pass request object from FrontEnd, instead of passing advisor_id.
    You should instead put advisor_id in an event property so that Amplitude user can track the advisor invitation by advisor_id.
 */

exports.publishData = function publishData(event_type, event_properties, {user_id, device_id, session_id, role}, user_properties){


    let amplitudeData = {
        event_type: event_type,
        event_properties: event_properties,
        user_properties: user_properties || {}
    };
    if(user_id){
        amplitudeData.user_id = user_id;
    }
    if(device_id){
        amplitudeData.device_id = device_id;
    }
    if(session_id){
        amplitudeData.session_id = session_id;
    }
    if(role){
        amplitudeData.user_properties.role = role;
    }

    console.log('Sending data to Amplitude' + JSON.stringify(amplitudeData));

    if (settings.ENVIRONMENT !== 'test') {
        amplitude.track(amplitudeData);
    }

}

exports.identify = function identify(user, ip) {

    let userObj = User.toSafeObject(user);
    userObj.ip = ip;

    let amplitudeData = {
        user_id: user._id.toString(),
        user_properties: userObj
    };

    if (settings.ENVIRONMENT !== 'test') {
        amplitude.identify(amplitudeData);
    }
};

exports.identifyExistingUser = function identifyExistingUser(userId, userObj) {

    let amplitudeData = {
        user_id: userId,
        user_properties: userObj
    };
    console.log('Sending data to Amplitude(Identifying existing user) ' + JSON.stringify(amplitudeData));
    if (settings.ENVIRONMENT !== 'test') {
        amplitude.identify(amplitudeData);
    }
};

exports.publishSuccessResponse = function publishSuccessResponse(req, res, next) {
    if (!req.user_id) {
        req.user_id = 'unidentified user';
    }
    let pureUrl = extractPureUrl(req.url, req.params);

    let amplitudeData = {
        event_type: pureUrl,
        user_id: req.user_id,
        method: req.method,
        event_properties: {result: 'success'},
    };

    console.log('Sending data to Amplitude' + JSON.stringify(amplitudeData));

    if (settings.ENVIRONMENT !== 'test') {
        amplitude.track(amplitudeData);
    }

}

// This function is deprecated
function extractPureUrl(rawUrl, urlParams){
    let pureUrl = rawUrl;
    for (param in urlParams) {
        let pattern = new RegExp('/' + urlParams[param]);
        rawUrl = rawUrl.replace(pattern, "");
    }
    return rawUrl;
}