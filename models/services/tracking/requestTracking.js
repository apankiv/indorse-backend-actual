const { directions } = require('../mongo/mongoose/schemas/requestLog');
const mongooseRequestLogRepo = require('../mongo/mongoose/mongooseRequestLogRepo');

const _handler = {};

function recordRequest(req, direction) {
    if (direction === directions.INCOMING) {
        const { requestID } = req;
        return {
            requestID,
            time: new Date().toUTCString(),
            fromIP: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            method: req.method,
            originalUri: req.originalUrl,
            uri: req.url,
            requestData: req.body,
            referer: req.headers.referer || '',
            ua: req.headers['user-agent'],
        };
    }
    return req;
}

function recordResponse(res, direction, setResponseBody) {
    if (direction === directions.INCOMING) {
        const oldWrite = res.write;
        const oldEnd = res.end;
        const chunks = [];
        res.write = (...restArgs) => {
            chunks.push(Buffer.from(restArgs[0]));
            oldWrite.apply(res, restArgs);
        };
        res.end = (...restArgs) => {
            if (restArgs[0]) {
                chunks.push(Buffer.from(restArgs[0]));
            }
            setResponseBody(Buffer.concat(chunks).toString('utf8'));
            oldEnd.apply(res, restArgs);
        };
    } else {
        setResponseBody(res);
    }
}

function Tracker({ req, res, options }) {
    const { direction, meta } = options;
    const request = recordRequest(req, direction);
    const response = {};
    recordResponse(res, direction, (responseBody) => {
        response.responseBody = responseBody;
    });
    this.getRequest = () => request;
    this.getResponse = () => response;
    this.saveRequest = async () => {
        this.logId = await mongooseRequestLogRepo.logRequest(request, direction, meta);
    };
    this.saveResponse = async (directResponse) => {
        await mongooseRequestLogRepo.logResponse(this.logId, directResponse || response);
    };
}

_handler.attachRequestTracker = async function attachRequestTracker(req, res, options) {
    const tracker = new Tracker({ req, res, options });
    await tracker.saveRequest();
    if (options.direction === directions.OUTGOING) {
        // Directly save the response if the request is outgoing
        await tracker.saveResponse(res);
        return;
    }
    const cleanup = () => {
        res.removeListener('finish', logFn);
        res.removeListener('close', abortFn);
        res.removeListener('error', errorFn);
    };

    const logFn = () => {
        cleanup();
        tracker.saveResponse();
    };

    const abortFn = () => {
        cleanup();
        tracker.saveResponse();
    };

    const errorFn = () => {
        cleanup();
        tracker.saveResponse();
    };

    res.on('finish', logFn); // successful pipeline (regardless of its response)
    res.on('close', abortFn); // aborted pipeline
    res.on('error', errorFn); // pipeline internal error
};

module.exports = _handler;
