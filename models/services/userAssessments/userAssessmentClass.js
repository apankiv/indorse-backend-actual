const errorUtils = require('../error/errorUtils');
const mongooseUserAssessmentRepo = require('../mongo/mongoose/mongooseUserAssessmentRepo');
const { status: assessmentStatus } = require('../mongo/mongoose/schemas/userAssessment');
const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const mongooseAssessmentMagicLinkRepo = require('../mongo/mongoose/mongooseAssessmentMagicLinkRepo');

class UserAssessmentClass {
    constructor(assessmentId, userId) {
        if (!assessmentId) errorUtils.throwError('assessmentId not passed', 400);
        if (!userId) errorUtils.throwError('userId not passed', 400);
        this.assessmentId = assessmentId;
        this.userId = userId;
        this.userAssessmentId = null;
        this.userAssessment = null;
        this.assessment = null;
    }

    async populateUserAssessment() {
        const criteria = {
            assessment: this.assessmentId,
            startedBy: this.userId,
        };
        this.userAssessment = await mongooseUserAssessmentRepo.findOne(criteria);
        this.userAssessmentId = this.userAssessment && this.userAssessment._id;
        if (!this.userAssessment) errorUtils.throwError('User has not yet attempted the assignment !', 404);
    }

    async populateAssessment() {
        this.assessment = await mongooseAssessmentMagicLinkRepo.findOneById(this.assessmentId);
        if (!this.assessment) errorUtils.throwError('Assessment Magic Link not found !', 404);
    }

    async get() {
        await this.populateUserAssessment();
        return this.userAssessment;
    }

    async isClaimSkillEligible(skillId, throwError = false) {
        await this.populateAssessment();
        const { claimSkills } = this.assessment;
        const matchedSkills = (claimSkills || []).filter(claimSkillId => claimSkillId.toString() === skillId.toString());
        if (throwError && matchedSkills.length === 0) {
            errorUtils.throwError('Skill not eligible for claim', 400);
        }
        return matchedSkills.length > 0;
    }

    async associateWithClaim(claimId) {
        await this.populateAssessment();
        await this.populateUserAssessment();
        // check whether claim is allowed for a magic link
        const claimAllowed = this.assessment && this.assessment.claimAllowed;
        if (!claimAllowed) return;

        const companyId = (this.assessment && this.assessment.companyId) || null;
        const claim = await mongooseClaimsRepo.findOneById(claimId);
        const criteriaClaim = {
            _id: claimId,
        };
        const dataToSetClaim = {
            $set: {
                userAssessmentId: this.userAssessmentId,
                company: companyId,
            },
        };
        await mongooseClaimsRepo.update(criteriaClaim, dataToSetClaim);
        let claimStatus = assessmentStatus.CLAIM_SUBMITTED;
        if (claim.final_status === true || claim.final_status === false) {
            claimStatus = assessmentStatus.CLAIM_EVALUATED;
        }
        const criteriaUserAssessment = {
            _id: this.userAssessmentId,
        };
        const dataToSetUserAssessment = {
            $set: {
                claimId,
                status: claimStatus,
                claimStatus,
            },
        };
        await mongooseUserAssessmentRepo.update(criteriaUserAssessment, dataToSetUserAssessment);
    }
}

module.exports = { UserAssessmentClass };
