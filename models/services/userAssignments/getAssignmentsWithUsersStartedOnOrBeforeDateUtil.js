const Json2csvParser = require('json2csv').Parser;
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const errorUtils = require('../../../models/services/error/errorUtils');
const validate = require('../../services/common/validate');
const routeUtils = require('../../services/common/routeUtils');
const safeObjects = require('../../services/common/safeObjects');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

exports.register = function register(app) {
  app.get(
    '/oldAssignments/:tillDate',
    validate({params: PARAMS_SCHEMA}),
    routeUtils.asyncMiddleware(authChecks.loginCheck()),
    routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
    routeUtils.asyncMiddleware(getAssignmentsWithUsersStartedOnOrBeforeDate)
  );
}
const fields = ['assignmentLink', 'startedOn', 'username', 'displayName', 'email', 'jobRole', 'nonValidatedSkills' ];
const opts = { fields };
const parser = new Json2csvParser(opts);

async function getAssignmentsWithUsersStartedOnOrBeforeDate(req, res) {
  const tillDate = safeObjects.sanitize(req.params.tillDate);
  if (!tillDate) {
    errorUtils.throwError(`Please enter a valid date`, 400);
  }
  let results = await mongooseUserAssignmentRepo.getAssignmentsWithUsersStartedOnOrBeforeDate(tillDate);

  const csv = parser.parse(results);
  res.setHeader('Content-disposition', 'attachment; filename=results.csv');
  res.set('Content-Type', 'text/csv');
  
  res.status(200).send(csv);
}

const PARAMS_SCHEMA = {
  type: 'object',
  required: ['tillDate'],
  properties: {
    tillDate: {
          type: 'string',
      }
  }
};