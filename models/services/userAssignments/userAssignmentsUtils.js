const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const { states: userAssignmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssignment');
const { states: claimStates } = require('../../../models/services/mongo/mongoose/schemas/claims');
const errorUtils = require('../../../models/services/error/errorUtils');

module.exports.calculateUserAssignmentStatus = async function calculateUserAssignmentStatus(userAssignment) {
    if (!userAssignment) return null;

    if (!userAssignment.finished_on) return userAssignmentStates.STARTED;

    const claimId = userAssignment.claim_id;
    if (!claimId) {
        errorUtils.throwError('User assignment submitted but claim not found', 404);
    }

    const claim = await mongooseClaimsRepo.findOneById(claimId);
    if (!claim) {
        errorUtils.throwError(`User assignment submitted but claim with id ${claimId} not found`, 404);
    }

    return claim.state === claimStates.FINISHED ? userAssignmentStates.EVALUATED : userAssignmentStates.SUBMITTED;
};

module.exports.calculateUserAssignmentStatusAndSave = async function calculateUserAssignmentStatusAndSave(userAssignment) {
    if (!userAssignment) return userAssignment;

    const status = await this.calculateUserAssignmentStatus(userAssignment);

    return mongooseUserAssignmentRepo.update({ _id: userAssignment._id }, { status });
};
