const { taskTypes } = require('../../mongo/mongoose/schemas/userTask');
const mongooseUserAssignmentRepo = require('../../mongo/mongoose/mongooseUserAssignmentRepo');
const { states } = require('../../mongo/mongoose/schemas/userAssignment');

async function isComplete(userFromDB) {
    const taskType = taskTypes.FIRST_ASSIGNMENT_EVALUATION;
    if (!userFromDB) {
        return false;
    }
    // find the user assessment status by the user
    const criteria = {
        $or: [
            {
                owner_id: userFromDB._id.toString(),
                status: states.EVALUATED,
            },
            {
                owner_id: userFromDB._id.toString(),
                status: states.SUBMITTED,
            },
        ],
    };
    const userAssignmentByUser = await mongooseUserAssignmentRepo.findOne(criteria);
    if (!userAssignmentByUser) {
        return false;
    }
    return taskType;
}

module.exports = { isComplete };
