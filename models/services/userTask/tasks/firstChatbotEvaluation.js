const { taskTypes } = require('../../mongo/mongoose/schemas/userTask');

async function isComplete(userFromDB) {
    const taskType = taskTypes.FIRST_CHATBOT_EVALUATION;
    if (!userFromDB || !userFromDB.skills) {
        return false;
    }
    const { skills: userSkills } = userFromDB;
    let isChatbotPassed = false;
    userSkills.forEach((userSkill) => {
        const { validations } = userSkill || {};
        const chatbotPassedValidations = validations.filter(validation => validation && validation.type === 'quizbot' && validation.validated === true);
        if (chatbotPassedValidations.length > 0) {
            isChatbotPassed = true;
        }
    });
    if (!isChatbotPassed) return false;
    return taskType;
}

module.exports = { isComplete };
