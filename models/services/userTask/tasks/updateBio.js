const { taskTypes } = require('../../mongo/mongoose/schemas/userTask');

async function isComplete(userFromDB) {
    const taskType = taskTypes.UPDATE_BIO;
    if (!userFromDB || !userFromDB.bio) {
        return false;
    }
    return taskType;
}

module.exports = { isComplete };
