const { taskTypes } = require('../../mongo/mongoose/schemas/userTask');

async function isComplete(userFromDB) {
    const taskType = taskTypes.UPDATE_PROFILE_PIC;
    if (!userFromDB || !userFromDB.img_url) {
        return false;
    }
    return taskType;
}

module.exports = { isComplete };
