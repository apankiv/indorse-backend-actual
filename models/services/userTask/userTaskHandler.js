const { taskTypes } = require('../mongo/mongoose/schemas/userTask');
// const errorUtils = require('../error/errorUtils');
const updateBio = require('./tasks/updateBio');
const updateProfilePicture = require('./tasks/updateProfilePicture');
const firstChatbotEvaluation = require('./tasks/firstChatbotEvaluation');
const firstAssignmentEvaluation = require('./tasks/firstAssignmentEvaluation');
const mongooseUserTaskRepo = require('../mongo/mongoose/mongooseUserTaskRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const { publishTaskUpdate } = require('./userTaskPublisher');
const mongoose = require('mongoose');

// These isComplete functions accept userSpecificTask State from DB
const taskHandlers = [
    {
        taskType: taskTypes.UPDATE_BIO,
        isComplete: updateBio.isComplete,
        priority: 1,
    },
    {
        taskType: taskTypes.UPDATE_PROFILE_PIC,
        isComplete: updateProfilePicture.isComplete,
        priority: 2,
    },
    {
        taskType: taskTypes.FIRST_CHATBOT_EVALUATION,
        isComplete: firstChatbotEvaluation.isComplete,
        priority: 3,
    },
    {
        taskType: taskTypes.FIRST_ASSIGNMENT_EVALUATION,
        isComplete: firstAssignmentEvaluation.isComplete,
        priority: 4,
    },
];

function formatTask(taskObjectFromDB = {}, taskObjectFromTemplate = {}) {
    return {
        type: taskObjectFromTemplate.taskType || taskObjectFromDB.taskType,
        priority: taskObjectFromTemplate.priority || 0,
        completedAt: taskObjectFromDB.completedAt,
        completed: !!taskObjectFromDB.completedAt,
    };
}

async function getViewerTasks(userId) {
    const userTaskObject = await mongooseUserTaskRepo.getUserTask(userId);
    const { taskList: userTaskList } = userTaskObject || {};
    const taskListToReturn = [];
    taskHandlers.forEach((taskObjectFromTemplate) => {
        const taskObjectFromDB = userTaskList.find(tasks => tasks.taskType === taskObjectFromTemplate.taskType);
        taskListToReturn.push(formatTask(taskObjectFromDB, taskObjectFromTemplate));
    });
    return taskListToReturn;
}

async function publishTaskUpdates(userId, completedTasks = [], subscription = true) {
    if (!subscription) return;
    try {
        const arrayOfPromises = [];
        completedTasks.forEach((taskType) => {
            const particularTaskHandler = taskHandlers.find(taskHandler => taskHandler.taskType === taskType);
            arrayOfPromises.push(publishTaskUpdate(userId, taskType, particularTaskHandler.priority));
        });
        await Promise.all(arrayOfPromises);
    } catch (error) {
        console.log('Could not publish result');
        console.log('Error: ', error);
    }
}

async function updateUserTasksStatus(userId, subscription = true) {
    if (!userId || !mongoose.Types.ObjectId.isValid(userId)) return false;
    const userFromDB = await mongooseUserRepo.findOne({ _id: userId });
    if (!userFromDB) return false;
    const viewerTasks = await getViewerTasks(userId);
    const arrayOfPromises = [];
    viewerTasks.forEach((task) => {
        if (!task.completed) {
            const particularTaskHandler = taskHandlers.find(taskHandler => taskHandler.taskType === task.type);
            arrayOfPromises.push(particularTaskHandler.isComplete(userFromDB));
        }
    });
    try {
        let completedTaskList = await Promise.all(arrayOfPromises);
        completedTaskList = completedTaskList.filter(Boolean);
        await mongooseUserTaskRepo.markMultipleTasksCompleted(userId, completedTaskList);
        await publishTaskUpdates(userId, completedTaskList, subscription);
        return true;
    } catch (error) {
        // eslint-disable-next-line
        console.log(`Unable to update task status for (user: ${userId})\nError: `, error);
        return false;
    }
}

module.exports = {
    taskHandlers,
    getViewerTasks,
    updateUserTasksStatus,
};
