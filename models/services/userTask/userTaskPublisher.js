const rabbitMqPublish = require('../rabbitMq/publishToExchange');

async function publishTaskUpdate(userId, taskType, priority) {
    const pubishMessage = {
        userId,
        type: taskType,
        priority,
        completed: true,
    };
    console.log(`Debug Update User Task Status publish msg ${JSON.stringify(pubishMessage)}`);
    try {
        await rabbitMqPublish.publishToExchange('UserTask.Completed.DLQ.Exchange', { userTaskCompleted: pubishMessage });
    } catch (error) {
        console.log('Unable to publish message for user task completed');
    }
}

module.exports = { publishTaskUpdate };
