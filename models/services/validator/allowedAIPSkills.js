const AIPSkillNames = {

        Javascript: 'Javascript',
        Java: 'Java',
        Php: 'Php',
        Ruby: 'Ruby',
        Python: 'Python',
        Solidity: 'Solidity',
        CSHARP: 'C#',
        CPLUSPLUS: 'C++'
};

module.exports = AIPSkillNames;