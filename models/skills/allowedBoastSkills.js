// const safeObjects = require('../services/common/safeObjects');
// const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
// const { Validator } = require('express-json-validator-middleware');
const skillUtils = require('../services/skills/skillUtils');

// const validator = new Validator({ allErrors: true });
// const { validate } = validator;

exports.register = function register(app) {
    app.post(
        '/allowed-claim-skills',
        // validate({ body: BODY_SCHEMA }),
        // routeUtils.asyncMiddleware(authChecks.validationForPartners()), // should add partner_id
        routeUtils.asyncMiddleware(allowedBoastSkillsForAIP),
    );
};


async function allowedBoastSkillsForAIP(req, res) {
    const skills = await skillUtils.allowedBoastSkillsForAIP();
    return res.status(200).json({ data: skills });
}


// const BODY_SCHEMA = {
//     type: 'object',
//     required: ['partnerClientId'],
//     properties: {
//         partnerClientId: {
//             type: 'string',
//             minLength: 1,
//         },
//     },
//     additionalProperties: false,
// };
