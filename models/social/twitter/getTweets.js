const mongoUserRepo = require('../../services/mongo/mongoRepository')('users');
const mongoTweetsRepo = require('../../services/mongo/mongoRepository')('tweets');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const settings = require('../../settings');


const REQUEST_FIELD_LIST = ['username'];

const Twitter = require('twitter');

exports.register = function register(app) {
    app.get('/gettweets',routeUtils.asyncMiddleware(getTweets));
};


async function getTweets(req, res) {
    
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to fetch skills', 403);
    }

    let tweetTimestamp = await mongoTweetsRepo.findOne({'last_pull_timestamp' : {$exists : 1}});
    let current_time = Math.floor(Date.now() / 1000);
    let needToPullTweets = true;

    if(tweetTimestamp)
    {
        console.log(tweetTimestamp);
        let timestamp = tweetTimestamp['last_pull_timestamp'];
        
        if(!(current_time - timestamp >= 900))
        {
            needToPullTweets = false;
        }
    }
    console.log(needToPullTweets);
    if(needToPullTweets)
    {
        //pull tweets and dump in to DB
        var client = new Twitter({
          consumer_key: settings.TWITTER_CONSUMER_KEY,
          consumer_secret: settings.TWITTER_CONSUMER_SECRET,
          access_token_key: settings.TWITTER_ACCESS_KEY,
          access_token_secret: settings.TWITTER_ACCESS_SECRET
        });
        
        await mongoTweetsRepo.deleteOne({'tweets' : {$exists : 1}});
        await mongoTweetsRepo.deleteOne({'last_pull_timestamp' : {$exists : 1}})

        var params = {screen_name: 'joinIndorse'};

        let tweets = await new Promise((resolve, reject) =>client.get('statuses/user_timeline', params, function(error, tweets, response) {
                if (!error) {
                    resolve(tweets);
                } else {
                reject(error);
                }
        }));

        await mongoTweetsRepo.insert({'tweets' : tweets});
        await mongoTweetsRepo.insert({'last_pull_timestamp' : current_time});
    }


    let tweetsFromDb = await mongoTweetsRepo.findOne({'tweets' : {$exists : 1}});
    tweetsFromDb = tweetsFromDb['tweets'];
    let last_pull_timestamp = await mongoTweetsRepo.findOne({'last_pull_timestamp' : {$exists : 1}});
    last_pull_timestamp = last_pull_timestamp['last_pull_timestamp'];
    res.send(200, {
            success: true,
            tweets: tweetsFromDb,
            lastUpdatedTimestamp: last_pull_timestamp
    });
    
};

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}