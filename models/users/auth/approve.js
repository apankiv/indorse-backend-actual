const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const emailService = require('../../services/common/emailService');
const errorUtils = require('../../services/error/errorUtils');
const ObjectID = require('mongodb').ObjectID;
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['approve_user_id'];

exports.register = function register(app) {
    app.post('/users/approve',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(approve));
};

/**
 * @swagger
 * definitions:
 *   ApproveUserRequest:
 *     type: object
 *     properties:
 *       approve_user_id:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /users/approve:
 *   post:
 *     description: Marks user as approved
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/ApproveUserRequest'
 *     responses:
 *       200:
 *         description: Approval successful
 *         schema:
 *           $ref: '#/definitions/Response'
 *       403:
 *         description: On insufficient permissions
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function approve(req, res) {
    let approveUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to approve user', 403);
    }

    await mongooseUserRepo.update({_id: new ObjectID(approveUserRequest.approve_user_id)}, {$set: {approved: true}});

    let approvedUserProfile = await mongooseUserRepo.findOne({_id: new ObjectID(approveUserRequest.approve_user_id)});

    await emailService.sendUserApprovedEmail(approvedUserProfile.name, approvedUserProfile.email);

    res.status(200).send({
        success: true,
        message: 'User approved successfully'
    });
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.write;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            approve_user_id: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['approve_user_id'],
        additionalProperties: false
    };
}
