const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const ObjectID = require('mongodb').ObjectID;
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['approve_user_id'];

const routeUtils = require('../../services/common/routeUtils');

exports.register = function register(app) {
    app.post('/users/disapprove',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(disapprove));
};

/**
 * @swagger
 * definitions:
 *   DisapproveUserRequest:
 *     type: object
 *     properties:
 *       approve_user_id:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /users/disapprove:
 *   post:
 *     description: Marks user as disapproved
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/DisapproveUserRequest'
 *     responses:
 *       200:
 *         description: Disapproval successful
 *         schema:
 *           $ref: '#/definitions/Response'
 *       403:
 *         description: On insufficient permissions
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function disapprove(req, res) {
    let approveUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to disapprove user', 403);
    }

    await mongooseUserRepo.update({_id: new ObjectID(approveUserRequest.approve_user_id)}, {$set: {approved: false}});

    res.status(200).send({
        success: true,
        message: 'User disapproved successfully'
    });
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.write;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            approve_user_id: {
                type: 'string'
            }
        },
        required: ['approve_user_id'],
        additionalProperties: false
    };
}