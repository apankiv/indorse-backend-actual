const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');

exports.register = function register(app) {
    app.post('/logout', routeUtils.asyncMiddleware(logout));
};

/**
 * The request body is actually empty - just the token from the header is invalidated
 */

/**
 * @swagger
 * /logout:
 *   post:
 *     description: Logs out the user
 *     responses:
 *       200:
 *         description: Logout successful
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       403:
 *         description: On unauthorized request
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function logout(req, res) {

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    await mongooseUserRepo.update({email: req.email}, {$pull: {tokens: req.token}});

    res.status(200).send({
        success: true,
        message: 'User logged out successfully',
    });
}
