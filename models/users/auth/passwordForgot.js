const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const randtoken = require('rand-token');
const emailService = require('../../services/common/emailService');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const blacklistMiddleware = require('../../../middlewares/blacklist');
const recaptchaMiddleware = require('../../../middlewares/recaptcha');

const REQUEST_FIELD_LIST = ['email'];

exports.register = function register(app) {
    app.post('/password/forgot',
        routeUtils.asyncMiddleware(blacklistMiddleware.getExpressMiddlewareForForgotPassword()),
        routeUtils.asyncMiddleware(recaptchaMiddleware.verify),
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(passwordForgot));
};

async function passwordForgot(req, res) {
    let passwordForgotRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOneByEmail(passwordForgotRequest.email);

    if (!user) {
        errorUtils.throwError('User does not exist', 404);
    }

    if (!user.verified) {
        errorUtils.throwError('User is not verified', 400);
    }

    let pass_verify_token = randtoken.generate(16);
    let pass_verify_timestamp = Math.floor(Date.now() / 1000);

    if(user.pass_verify_timestamp && ((pass_verify_timestamp - user.pass_verify_timestamp) < 3600)) {
        errorUtils.throwError('Password reset email already sent!', 400);
    }

    await mongooseUserRepo.update({email: passwordForgotRequest.email},
        {$set: {pass_verify_token: pass_verify_token, pass_verify_timestamp: pass_verify_timestamp}});

    await emailService.sendPasswordResetEmail(user.name, user.email, pass_verify_token);

    res.status(200).send({
        success: true,
        message: 'Forgot password email sent successfully',
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            }
        },
        required: ['email'],
        additionalProperties: false
    };
}
