const safeObjects = require('../../../services/common/safeObjects');
const civicUtils = require("../../../services/social/civicUtils");
const socialLogin = require("../../../services/social/socialLoginService");
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const errorUtils = require('../../../services/error/errorUtils');
const civicConfidenceScore = require('./confidenceScore/civicConfidenceScore')
const cryptoUtils = require('../../../services/common/cryptoUtils');

const REQUEST_FIELD_LIST_LOGIN = ['civic'];
const AUTH_TYPE = 'civic';

exports.register = function register(app) {
    app.post('/auth/civic',
        validate({ body: getRequestSchema() }),
        routeUtils.asyncMiddleware(civicLogin));
};

async function civicLogin(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);
    let [userEmail, userUid, userData] = await civicUtils.validateAndGetUserData(loginRequest.civic.id_token);
    let user = await socialLogin.findUserByUid(userUid, AUTH_TYPE);
  
    if (!user) {        
        if (!userEmail) {
            errorUtils.throwError("No email address provided by Civic", 404);
        }               
        user = await socialLogin.findUserByEmail(userEmail);

        if (!user) {
            let signupToken = await cryptoUtils.generateJWTCivic(userEmail, userUid, userData);

            res.status(404).send({
                success: false,
                message: "Redirecting to signup page",
                token: signupToken
            });

            return;
        }

        await socialLogin.linkUserByEmail(user, userUid, userEmail, AUTH_TYPE);
    }    
    await civicConfidenceScore.updateCivicScore(userData,user.email);
    req.user_id = user._id;
    await socialLogin.socialLoginTemplate(user, res, AUTH_TYPE);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            civic: {
                type: 'object',
                properties: {
                    id_token: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['id_token'],
                additionalProperties: false
            }
        },
        required: ['civic'],
        additionalProperties: false
    };
}
