const civicUtils = require("../../../services/social/civicUtils");
const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const civicConfidenceScore = require('./confidenceScore/civicConfidenceScore')
const confidenceScore = require('./confidenceScore/confidenceScore')
const errorUtils = require('../../../services/error/errorUtils');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const createClaimService = require('../../../services/claims/createClaimService');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const jwt = require('jsonwebtoken');
const config = require('config');

const AUTH_TYPE = 'civic';
const REQUEST_FIELD_LIST_SIGNUP = ['name', 'username','email', 'civic', 'claimToken'];

exports.register = function register(app) {
    app.post('/signup/civic',
        validate({ body: getRequestSchema() }),
        routeUtils.asyncMiddleware(civicSignup));
};

async function civicSignup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_SIGNUP);

    let decodedToken = await decodeSignupToken(newUserRequest.civic.signupToken);

    if(!decodedToken.email){
        errorUtils.throwError("Sign up failed, no civic email present", 400);
    }

    newUserRequest.civic_uid = decodedToken.userUid;
    newUserRequest.civic_email = decodedToken.email;
    newUserRequest.email = decodedToken.email;
  
    delete newUserRequest.civic; 

    let civicScore = civicConfidenceScore.calculateCivicScore(decodedToken.userData, decodedToken.email);
    let aggregateScore = confidenceScore.aggregateConfidenceScore(newUserRequest);
    newUserRequest.confidenceScore  = {};
    newUserRequest.confidenceScore.civicScore  = civicScore;
    newUserRequest.confidenceScore.aggregateScore = aggregateScore;
    newUserRequest.civicParams = decodedToken.userData;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE, 'claimToken');

        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: userToLogin._id}, {
            tokens: [],
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, userToLogin);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        res.status(200).send({
            success: true,
            message: "Successful signup and claim creation!",
            token: token
        })
    } else {

        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE);

        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        res.status(200).send({
            success: true,
            message: "User successfully logged in",
            token: token
        })
    }
}

async function decodeSignupToken(signupToken) {
    let email, userUid, userData;
    return new Promise((resolve, reject) => {
        jwt.verify(signupToken, config.get('JWT.jwtSecret'), function (err, decoded) {
            if (err) {
                logger.debug('Civic JWT error' + JSON.stringify(err));
                reject(new Error('Civic JWT token error'));
            }
            else {
                if (decoded && decoded.email && decoded.civic_uid && decoded.civicData) {
                    resolve({
                        email: decoded.email.toLowerCase(),
                        userUid: decoded.civic_uid,
                        userData: decoded.civicData
                    });
                }else{
                    logger.debug('Civic decoded token error : civic_uid ' + userUid + ' email ' + email + ' civicData ' + JSON.stringify(userData));
                    reject(new Error('Civic decoded token error'));
                }
            }
        });
    } )

};

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            claimToken: {
                type: 'string'
            },
            civic: {
                type: 'object',
                properties: {
                    signupToken: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['signupToken'],
                additionalProperties: false
            }
        },
        required: ['civic', 'username', 'name'],
        additionalProperties: false
    };
}

