const mongooseUserRepo = require("../../../../services/mongo/mongoose/mongooseUserRepo");
const replicationUserRepo = require("../../../../services/mongo/userRepo");
const confidenceScore = require("./confidenceScore")
const logger = require("../../../../services/common/logger").getLogger();
var _ = require('lodash');


let version = "0.1";

//TODO async
exports.calculateCivicScore = function calculateCivicScore(params, email) {    
    return 10;
}

exports.updateCivicScore = async function updateCivicScore(inputCivicParams, inputEmail) {
    let score = this.calculateCivicScore(inputCivicParams, inputEmail);
    let user = await mongooseUserRepo.findOneByEmail(inputEmail);

    if (user.confidenceScore) {
        user.confidenceScore.civicScore = score;
    } else {
        user.confidenceScore = { "civicScore": score };
    }
    let aggregateScore = confidenceScore.aggregateConfidenceScore(user);
    await replicationUserRepo.update({ 'email': inputEmail },
        {
            '$set': {
                'confidenceScore.civicScore': score,
                'confidenceScore.aggregateScore': aggregateScore,
                'civicParams': inputCivicParams
            }
        });
    return;
}