const logger = require("../../../../services/common/logger").getLogger();


var _ = require('lodash');

async function findUserByEmail(inputEmail) {
    return await mongoUserRepo.findOne({email: inputEmail});
    ;
};


function calculateTotalWeight(params) {
    let weight = 0;
    for (let field of params) {
        if (field.weight !== undefined) {
            weight += field.weight;
        }
    }
    return weight;
}

function calculateAverage(total, params) {
    if (params.length === 0)
        return 0;

    let totalWeight = calculateTotalWeight(params);
    logger.debug("Total " + total + " weight " + totalWeight);
    let average = ((total / totalWeight) * 10).toFixed(2)
    logger.debug("Average : " + average);
    return average;
}

exports.aggregateConfidenceScore = function aggregateConfidenceScore(user) {
    let fbScore = 0
    let linkedInScore = 0;

    let fbVerified = false;
    let linkedInVerified = false;

    let totalScore = 0;

    let aggregateScore = {
        score : 0,
        isVerified : false
    }

    if (user.confidenceScore){
        if (user.confidenceScore.facebookScore){
            totalScore += Number(user.confidenceScore.facebookScore.score);
            fbVerified = user.confidenceScore.facebookScore.isVerified;
        }
        if (user.confidenceScore.linkedInScore) {
            totalScore += Number(user.confidenceScore.linkedInScore.score);
            linkedInVerified = user.confidenceScore.linkedInScore.isVerified;
        }        
    }

    aggregateScore.score = (Number(totalScore)/2).toFixed(2);
    logger.debug("Aggregate Score " + aggregateScore.score);

    if (fbVerified || linkedInVerified || Number(aggregateScore.score) >= Number(5)){
        aggregateScore.isVerified = true;
    }
    return aggregateScore;
}

exports.calculateTotalWeight = calculateTotalWeight;
exports.calculateAverage = calculateAverage;
