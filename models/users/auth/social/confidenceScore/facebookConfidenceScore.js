const mongooseUserRepo = require("../../../../services/mongo/mongoose/mongooseUserRepo");
const replicationUserRepo = require("../../../../services/mongo/userRepo");
const confidenceScore = require("./confidenceScore")
const logger = require("../../../../services/common/logger").getLogger();

var _ = require('lodash');


let version = "0.1";

//Facebook Params
let facebookParams = [    
    { name: 'friends', threshold: 75, weight: 1 },
    //{ name: 'likes', threshold: 100, weight: 1 }, // as it is not implemented yet, this shouldn't be calculated in calculateAverage
    { name: 'picture', weight: 1 }
];



exports.calculateFacebookScore = async function calculateFacebookScore(params,email) {
    logger.debug("Calculating facebook Score");
    let fbScore = Object({ isVerified: false, score: 0, version: version});
    try{
        if (params === undefined || params.length === 0) {
            return Object({ isVerified: false, score: 0 });
        }

        let isVerified = false;
        let totalScore = 0;
        for (let field of params) {
            if (field.name === "is_verified" || field.name === "verified"){
                if (field.value){
                    isVerified = true;
                    logger.debug('verified , updating flag');
                }
                continue;
            }            

            let fbParam = _.find(facebookParams, { name: field.name });
            if (!fbParam)
                continue;

            switch (field.name) {             
                case "friends":
                    if (field.value >= fbParam.threshold) {
                        totalScore += fbParam.weight;
                        logger.debug('friends, adding weight :' + fbParam.weight);
                        logger.debug('Total score is ' + totalScore);
                    }
                    break;

                case "likes":
                    if (field.value >= fbParam.threshold) {
                        totalScore += fbParam.weight;
                        logger.debug('likes, adding weight :' + fbParam.weight);
                        logger.debug('Total score is ' + totalScore);
                    }
                    break;

                case "picture":
                    if (field.value === false) {
                        totalScore += fbParam.weight;
                        logger.debug('pictures, adding weight :' + fbParam.weight);
                        logger.debug('Total score is ' + totalScore);
                    }
                    break;
            }
        }
        fbScore.isVerified = isVerified;
        fbScore.score = confidenceScore.calculateAverage(totalScore, facebookParams);

    }catch(error){
        logger.debug("Unable to create Facebook confidence score for user email :" + email + " Params: " + JSON.stringify(params));
        logger.debug(error);
    }
    return fbScore;
}

exports.updateFacebookScore = async function updateFacebookScore(inputFacebookParams,inputEmail){
    let score = await this.calculateFacebookScore(inputFacebookParams, inputEmail);
    let user = await mongooseUserRepo.findOneByEmail(inputEmail);

    if (user.confidenceScore) {
        user.confidenceScore.facebookScore = score;
    } else { 
        user.confidenceScore = {"facebookScore" : score};
    }      
    let aggregateScore = confidenceScore.aggregateConfidenceScore(user);
    await replicationUserRepo.update({ 'email': inputEmail }, 
                                    { '$set': { 'confidenceScore.facebookScore': score,
                                                'confidenceScore.aggregateScore' : aggregateScore,
                                                'facebookParams': inputFacebookParams }});    
    return;
}