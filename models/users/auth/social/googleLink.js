const safeObjects = require('../../../services/common/safeObjects');
const gUtils = require("../../../services/social/googleService");
const socialLogin = require("../../../services/social/socialLoginService");
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_LOGIN = ['google'];

exports.register = function register(app) {
    app.post('/link/google',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(googleLink));
};

async function googleLink(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);

    if (!req.login) {
        errorUtils.throwError("User is not logged in", 400);
    }

    let [email, google_uid] = await gUtils.validateAndGetEmail(loginRequest.google.id_token);

    await socialLogin.socialLinkTemplate(req.email,google_uid,'google','',res);

}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            google: {
                type: 'object',
                properties: {
                    id_token: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['id_token'],
                additionalProperties: false
            }
        },
        required: ['google'],
        additionalProperties: false
    };
}
