const safeObjects = require('../../../services/common/safeObjects');
const gUtils = require("../../../services/social/googleService");
const socialLogin = require("../../../services/social/socialLoginService");
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_LOGIN = ['google'];
const AUTH_TYPE = 'google';

exports.register = function register(app) {
    app.post('/auth/google',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(googleLogin));
};

async function googleLogin(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);
    let [email, googleUid] = await gUtils.validateAndGetEmail(loginRequest.google.id_token);

    let user = await socialLogin.findUserByUid(googleUid, AUTH_TYPE);
    if (!user) {
        user = await socialLogin.findUserByEmail(email);
        await socialLogin.linkUserByEmail(user, googleUid, email, AUTH_TYPE);
    }
    req.user_id = user._id;
    await socialLogin.socialLoginTemplate(user, res, AUTH_TYPE);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            google: {
                type: 'object',
                properties: {
                    id_token: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['id_token'],
                additionalProperties: false
            }
        },
        required: ['google'],
        additionalProperties: false
    };
}
