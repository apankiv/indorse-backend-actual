const gUtils = require("../../../services/social/googleService");
const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const createClaimService = require('../../../services/claims/createClaimService');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../../../services/error/errorUtils');

const REQUEST_FIELD_LIST_SIGNUP = ['name', 'username', 'google','claimToken'];
const AUTH_TYPE = 'google';

exports.register = function register(app) {
    app.post('/signup/google',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(googleSignup));
};

async function googleSignup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_SIGNUP);

    let [email, google_uid] = await gUtils.validateAndGetEmail(newUserRequest.google.id_token);

    newUserRequest.google_uid = google_uid;
    newUserRequest.email = email;

    delete newUserRequest.google;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE, 'claimToken');

        let userToLogin =  await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: userToLogin._id}, {
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, userToLogin);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        res.status(200).send({
            success: true,
            message: "Successful signup and claim creation!",
            token: token
        })
    } else {

        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE);

        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        res.status(200).send({
            success: true,
            message: "User successfully logged in",
            token: token
        })
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            claimToken: {
                type: 'string'
            },
            google: {
                type: 'object',
                properties: {
                    id_token: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['id_token'],
                additionalProperties: false
            }
        },
        required: ['google', 'username', 'name'],
        additionalProperties: false

    };
}

