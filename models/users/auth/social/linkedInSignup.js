const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const errorUtils = require('../../../services/error/errorUtils');
const jwt = require('jsonwebtoken');
const config = require('config');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const linkedInConfidenceScore = require('./confidenceScore/linkedInConfidenceScore');
const confidenceScore = require('./confidenceScore/confidenceScore');
const logger = require('../../../services/common/logger').getLogger();
const createClaimService = require('../../../services/claims/createClaimService');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_SIGNUP = ['name', 'username', 'linkedIn','claimToken'];

exports.register = function register(app) {
    app.post('/signup/linked-in',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(linkedInSignup));
};

async function linkedInSignup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_SIGNUP);

    let [linkedInUid, email, linkedInParams] = await decodeSignupToken(newUserRequest.linkedIn.signupToken);

    if(!linkedInUid|| !email || !linkedInParams){
        logger.debug('LinkedIn authentication error : linkedInUid ' + linkedInUid + ' email ' + email + ' linkedInParams ' + JSON.stringify(linkedInParams));
        errorUtils.throwError("Authentication failed", 401); // any error in decodedSignupToken will be catched here. Only valid token will lead to defined variable
    }
    newUserRequest.linkedIn_uid = linkedInUid;
    newUserRequest.email = email;
    delete newUserRequest.linkedIn;

    let linkedInScore = await linkedInConfidenceScore.calculateLinkedInScore(linkedInParams, newUserRequest.email);

    newUserRequest.confidenceScore  = {};
    newUserRequest.confidenceScore.linkedInScore  = linkedInScore;
    let aggregateScore = confidenceScore.aggregateConfidenceScore(newUserRequest);
    newUserRequest.confidenceScore.aggregateScore = aggregateScore;
    newUserRequest.linkedInParams = linkedInParams;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        await socialSignup.completeSocialSignup(req, newUserRequest, 'linkedIn', 'claimToken');
        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);

        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: userToLogin._id}, {
            tokens: [],
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, userToLogin);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        res.status(200).send({
            success: true,
            message: "Successful signup and claim creation!",
            token: token
        })
    } else {

        await socialSignup.completeSocialSignup(req, newUserRequest, 'linkedIn');
        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        res.status(200).send({
            success: true,
            message: "User successfully logged in",
            token: token
        })
    }
}

async function decodeSignupToken(signupToken) {
    let email, linkedInUid, linkedInParams;
    return new Promise((resolve, reject) => {
        jwt.verify(signupToken, config.get('JWT.jwtSecret'), function (err, decoded) {
            if (err) {
                logger.debug('LinkedIn JWT error' + JSON.stringify(err));
                reject();
            }
            else {
                if (decoded && decoded.email && decoded.linkedIn_uid && decoded.linkedInParams) {
                    email = decoded.email.toLowerCase();
                    linkedInUid = decoded.linkedIn_uid;
                    linkedInParams = decoded.linkedInParams;
                    resolve([linkedInUid, email, linkedInParams]);
                }else{
                    logger.debug('LinkedIn decoded token error : linkedInUid ' + linkedInUid + ' email ' + email + ' linkedInParams ' + JSON.stringify(linkedInParams));
                    reject();
                }
            }
        });
    } )

};

module.exports.decodeSignupToken = decodeSignupToken;

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            claimToken: {
                type: 'string'
            },
            linkedIn: {
                type: 'object',
                properties: {
                    signupToken: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['signupToken'],
                additionalProperties: false
            }
        },
        required: ['linkedIn', 'username', 'name'],
        additionalProperties: false

    };
}

