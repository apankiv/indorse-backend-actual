const safeObjects = require('../../../services/common/safeObjects');
const linkedInUtils = require("../../../services/social/linkedInService");
const socialLogin = require("../../../services/social/socialLoginService");
const socialSignup = require("../../../services/social/socialSignupService");
const errorUtils = require('../../../services/error/errorUtils');
const linkedInConfidenceScore = require('./confidenceScore/linkedInConfidenceScore');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const confidenceScore = require('./confidenceScore/confidenceScore')
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo')
const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo')
const timestampService = require('../../../services/common/timestampService');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const newUser  = require('../../../../models/services/auth/newUserProcedure')
const emailService = require('../../../../models/services/common/emailService')

const REQUEST_FIELD_LIST = ['linked_in'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/validate/linked-in',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(linkedInLogin));
};

async function generateUserName(name){
    let userName = name.replace(/\s/g, '');
    for(i =0; i < 1000; ++i){
        let calcUserName = userName;
        calcUserName = calcUserName + i.toString();
        calcUserName = calcUserName.toLowerCase();
        let dbUser = await mongooseUserRepo.findOneByUsername(calcUserName);
        if(!dbUser){
            return calcUserName;
        }
    }
    throw Error('Unable to generate a user!');
}


//TODO : Refactor linkedInLogin and reuse functionality
async function linkedInLogin(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let accessToken = await linkedInUtils.exchangeAuthorizationCode(loginRequest.linked_in.code , loginRequest.linked_in.redirect_uri);
    let [linkedInUid, name, email, profileUrl] = await linkedInUtils.getLinkedInSignupVariables(accessToken);

    let authType = 'linkedIn'
    let user = await socialLogin.findUserByUid(linkedInUid, authType);

    if(user){
        let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
        await linkedInConfidenceScore.updateLinkedInScore(linkedInParams, user.email);
        let token = await socialLogin.setToken(user,user.email);
        req.user_id = user._id;
        amplitudeTracker.publishData('login', {type: authType}, req);
        await mongooseUserRepo.update({ email: user.email }, { $set: { linkedInProfileURL: profileUrl, approved : true, verified : true} });
        user.linkedInProfileURL = profileUrl;
        await validateInvitation(pretty_id, advisor_id, user, req);
        res.status(200).send({
            success: true,
            message: 'User logged in successfully',
            token: token
        });
    }
    else{
        
        user = await socialLogin.findUserByEmail(email);
        // link
        if(user){    
            await socialLogin.linkUserByEmail(user, linkedInUid, email, authType);            
            let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
            await linkedInConfidenceScore.updateLinkedInScore(linkedInParams, user.email);
            let token = await socialLogin.setToken(user,user.email);
            req.user_id = user._id;
            amplitudeTracker.publishData('social_link', {type: authType}, req);
            await mongooseUserRepo.update({ email: user.email }, { $set: { linkedInProfileURL: profileUrl, approved: true, verified: true} });
            user.linkedInProfileURL = profileUrl;
            await validateInvitation(pretty_id, advisor_id, user, req);
            res.status(200).send({
                success: true,
                message: 'User account is linked to '+ authType +' successfully',
                token: token
            });
        }
        else{        
             // create a JWT for signup : generate user name
            let username = await generateUserName(name);
            let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
            let linkedInScore = await linkedInConfidenceScore.calculateLinkedInScore(linkedInParams, email);
            let signupRequest = {
                linkedIn_uid : linkedInUid,
                email : email,
                name : name,
                username: username,
                confidenceScore: {linkedInScore : linkedInScore},
                linkedInProfileURL: profileUrl
            };
            let aggregateScore = confidenceScore.aggregateConfidenceScore(signupRequest);
            signupRequest.confidenceScore.aggregateScore = aggregateScore;
            signupRequest.linkedInParams = linkedInParams;
            await socialSignup.completeSocialSignup(req, signupRequest, 'linkedIn');
            let userToLogin = await mongooseUserRepo.findOneByEmail(email);
            let userItem = Object.assign({}, userToLogin);
            let token = await cryptoUtils.generateJWT(userItem);
            userToLogin.tokens.push(token);
            await mongooseUserRepo.update({ email: email }, { $set: { tokens: userToLogin.tokens, approved: true, verified: true } });
            userToLogin.linkedInProfileURL = profileUrl;   
            await validateInvitation(pretty_id, advisor_id, userToLogin, req);
            res.status(200).send({
                success: true,
                message: "User successfully logged in",
                token: token
            });        
        }
    }
}


async function validateInvitation(pretty_id, advisor_id,user, req) {
    
    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorToAccept = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToAccept) {
        errorUtils.throwError("Invitation not found", 404);
    }

    //Soft connections struct must exist for any soft invited advisor
    if (!advisorToAccept.softConnection) {
        errorUtils.throwError("Invitation not found", 404);
    } else {
        if (advisorToAccept.softConnection.statusValidatedByUserAt) {
            errorUtils.throwError("This invitation was already validated", 400);
        }
    }

    let validatedTimestamp = timestampService.createTimestamp();

    let softConnectionUpdate = advisorToAccept.softConnection;
    softConnectionUpdate.statusUpdatedAt = validatedTimestamp;
    softConnectionUpdate.statusValidatedByUserAt = validatedTimestamp;


    let invitationDataToTrack = {
        company_name: pretty_id,
        advisor_id: advisor_id,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994',
        is_user_already_validated: 'false'
    }
    if(advisorToAccept.softConnection.status === 'ACCEPTED'){
        amplitudeTracker.publishData('advisor_soft_accepted_validated', invitationDataToTrack, req);
    }else{
        amplitudeTracker.publishData('advisor_soft_rejected_validated', invitationDataToTrack, req);
    }

    await mongooseCompanyRepo.setCompanyAdvisorSoftConnection(pretty_id, advisor_id, softConnectionUpdate);
    //Only if the email is present on advisor object , populate this advisor with this user id & clear his email
    if (advisorToAccept.email){
        await newUser.linkUserWithAdvisors(user, advisorToAccept.email);
    }

    if (company.email) {
        await emailService.softAdvisorValidatedNotifyAdmin(user.name, company.company_name, pretty_id, user.linkedInProfileURL, 
            company.email,advisorToAccept.softConnection.status);
    }
}


function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            username: {
                type: 'string',
                minLength: 1
            },
            linked_in: {
                type: 'object',
                properties: {
                    code: {
                        type: 'string',
                        minLength: 1
                    },
                    state: {
                        type: 'string',
                        minLength: 1
                    },
                    redirect_uri: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['code', 'state', 'redirect_uri'],
                additionalProperties: false
            }
        },
        required: ['linked_in'],
        additionalProperties: true
    };
}
