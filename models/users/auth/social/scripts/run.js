const airbitzUpdate = require('./airbitzEthaddressverifiedUpdate');

(async function run() {
    console.log('!!!   IMPORTANT   !!!')
    console.log('1. Pause the nodejs application.')
    console.log('2. Do a full backup of MongoDB.')
    console.log('3. Do a full backup of BigchainDB.')

    await waitHack(1000)
    console.log()
    console.log('Script will run in 20 seconds... Press Ctr+C to cancel if you have not done the above.')
    await waitHack(20000)


    try {
        await airbitzUpdate.updateAllAirbitz()
    } catch (error) {
        console.log(error)
    }

    console.log()
    console.log('Don\'t forget to restart the nodejs application!')

})()

async function waitHack(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}