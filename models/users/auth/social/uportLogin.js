const safeObjects = require('../../../services/common/safeObjects');
const uportUtils = require("../../../services/social/uportUtils");
const socialLogin = require("../../../services/social/socialLoginService");
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const errorUtils = require('../../../services/error/errorUtils');
const uportConfidenceScore = require('./confidenceScore/uportConfidenceScore')

const REQUEST_FIELD_LIST_LOGIN = ['uport'];
const AUTH_TYPE = 'uport';

exports.register = function register(app) {
    app.post('/auth/uport',
        validate({ body: getRequestSchema() }),
        routeUtils.asyncMiddleware(uportLogin));
};

async function uportLogin(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);
    let [userUid, userData] = await uportUtils.validateAndGetUserData(loginRequest.uport);
    let user = await socialLogin.findUserByUid(userUid, AUTH_TYPE);
    //TODO : Revist this logic once certain of attributes
    if (!user) {            
            errorUtils.throwError("Account not found, Would you like to create a new account?", 404);
    }    
    await uportConfidenceScore.updateUportScore(userData,user.email);
    await socialLogin.socialLoginTemplate(user, res, AUTH_TYPE);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            uport: {
                type: 'object'
                }         
            },
        required: ['uport'],
        additionalProperties: false
    };
}
