const uportUtils = require("../../../services/social/uportUtils");
const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const uportConfidenceScore = require('./confidenceScore/uportConfidenceScore');
const confidenceScore = require('./confidenceScore/confidenceScore');
const mongooseUserRepo = require("../../../services/mongo/mongoose/mongooseUserRepo");
const errorUtils = require('../../../services/error/errorUtils');
const createClaimService = require('../../../services/claims/createClaimService');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_SIGNUP = ['name', 'username','email', 'uport','claimToken'];
const AUTH_TYPE = 'uport';

exports.register = function register(app) {
    app.post('/signup/uport',
        validate({ body: getRequestSchema() }),
        routeUtils.asyncMiddleware(uportSignup));
};

async function uportSignup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_SIGNUP);

    console.log("Request is :   " + JSON.stringify(newUserRequest));

    let [userUid, userData]  = await uportUtils.validateAndGetUserData(newUserRequest.uport);

    if (!newUserRequest.email){
        errorUtils.throwError("Sign up failed, no email present", 400);
    }

    newUserRequest.uport_uid = userUid;
  
    delete newUserRequest.uport; 

    let uportScore = uportConfidenceScore.calculateUportScore(userData, newUserRequest.email);  
    let aggregateScore = confidenceScore.aggregateConfidenceScore(newUserRequest);
    newUserRequest.confidenceScore  = {};
    newUserRequest.confidenceScore.uportScore  = uportScore;
    newUserRequest.confidenceScore.aggregateScore = aggregateScore;
    newUserRequest.uportParams = userData;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE, 'claimToken');

        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: userToLogin._id}, {
            tokens: [],
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, userToLogin);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        res.status(200).send({
            success: true,
            message: "Successful signup and claim creation!",
            token: token
        })
    } else {
        await socialSignup.completeSocialSignup(req, newUserRequest, AUTH_TYPE);

        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = await cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        res.status(200).send({
            success: true,
            message: "User successfully logged in",
            token: token
        })
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            email:{
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            },
            claimToken: {
                type: 'string'
            },
            uport: {
                type: 'object'              
            }
        },
        required: ['uport', 'username', 'email', 'name'],
        additionalProperties: false
    };
}
