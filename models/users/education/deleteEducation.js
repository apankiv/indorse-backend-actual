const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['item_key'];

exports.register = function register(app) {
    app.post('/deleteEducation',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(deleteEducation));
};

async function deleteEducation(req, res) {
    let deleteEducationRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    amplitudeTracker.publishData('education_delete',{feature_category: 'profile'}, req);
    await userRepo.update({email: req.email}, {$pull: {education: {item_key: deleteEducationRequest.item_key}}});

    res.status(200).send({
        success: true,
        message: 'Deleted successfully'
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            item_key: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['item_key'],
        additionalProperties: false
    };
}