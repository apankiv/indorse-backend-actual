const mongooseSchoolRepo = require('../../services/mongo/mongoose/mongooseSchoolRepo');
const userRepo = require('../../services/mongo/userRepo');
const cryptoUtils = require('../../services/common/cryptoUtils');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const checkSkillInMongo = require('../skills/checkSkillInMongo');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUESTED_FIELD_LIST = ['school_name', 'item_key', 'school_id', 'degree', 'field_of_study', 'grade', 'start_date', 'end_date',
    'activities', 'description', 'email','skills'];

exports.register = function register(app) {
    app.post('/updateeducation',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(updateEducation));
};

async function updateEducation(req, res) {
    let educationRequest = safeObjects.safeReqestBodyParser(req, REQUESTED_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    if (educationRequest.skills)
        educationRequest.skills = await checkSkillInMongo.checkThatSkillsInMongo(educationRequest.skills);

    //Find out if key exists. If yes then its a request to update
    if (educationRequest.item_key) {
        amplitudeTracker.publishData('education_update',{feature_category: 'profile'},req);
        await userRepo.update(
            {'education.item_key': educationRequest.item_key},
            {$set: {'education.$': educationRequest}});
    } else {
        amplitudeTracker.publishData('education_add',{feature_category: 'profile'},req);
        //Find out if school name exists in the School Names collection
        let schoolFromMongo = await mongooseSchoolRepo.findOne({
            school_name:
                {$regex: '^' + educationRequest.school_name + '$', $options: 'i'}
        });

        if (schoolFromMongo) {
            educationRequest.school_id = schoolFromMongo._id.toString();
        } else {
            let schoolNameItem = {
                school_name: educationRequest.school_name
            };

            educationRequest.school_id = await mongooseSchoolRepo.insert(schoolNameItem);
        }

        educationRequest.item_key = cryptoUtils.generateItemKey();
        await userRepo.update({email: req.email}, {$push: {education: educationRequest}});
    }

    res.status(200).send({
        success: true,
        education: educationRequest,
        message: 'Updated successfully'
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            skills : {
                type:'array',
                items: {
                    type : 'string',
                    pattern: "^([\\w\\d\\.\\-\\s]){2,}$",
                }
            },
            school_name: {
                type: 'string',
                minLength: 1
            },
            item_key: {
                type: 'string',
                minLength: 1
            },
            school_id: {
                type: 'string',
                minLength: 1
            },
            degree: {
                type: 'string',
                minLength: 1
            },
            field_of_study: {
                type: 'string',
                minLength: 1
            },
            grade: {
                type: 'string',
                minLength: 1
            },
            activities: {
                type: 'string',
                minLength: 1
            },
            description: {
                type: 'string',
                minLength: 1
            },
            start_date: {
                type: 'object',
                properties: {
                    year: {
                        type: 'number'
                    },
                    month: {
                        type: 'number'
                    }
                },
                required: ['year'],
                additionalProperties: false
            },
            end_date: {
                type: 'object',
                properties: {
                    year: {
                        type: 'number'
                    },
                    month: {
                        type: 'number'
                    }
                },
                required: ['year'],
                additionalProperties: false
            },
        },
        required: ['school_name'],
        additionalProperties: false
    };
}