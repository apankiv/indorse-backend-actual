const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const Web3 = new require('web3');
const web3 = new Web3();
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');
const connectionsFacade = require('../../../smart-contracts/services/connections/connectionsContract');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');
const requestIp = require('request-ip');

const REQUEST_FIELD_LIST = ['address'];

exports.register = function register(app) {
    app.post('/users/:user_id/ethaddress',
        validate({body: BODY_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(updateEthaddress));
};

async function updateEthaddress(req, res) {
    let profileUpdateRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user_id = safeObjects.sanitize(req.params.user_id);

    let userToUpdate = await mongooseUserRepo.findOneById(user_id);
    amplitudeTracker.publishData('ethereum_address_update',{feature_category: 'profile'}, req);
    if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    if((req.user_id !== userToUpdate._id.toString()) && !req.permissions.admin.write) {
        errorUtils.throwError("Insufficient permissions to update this user's ethaddress.", 403);
    }

    if('is_airbitz_user' in userToUpdate && userToUpdate.is_airbitz_user)
    {
        errorUtils.throwError("User has a connected Airbitz account, hence not allowed to change ETH address.", 403);
    }

    if (!web3.isAddress(profileUpdateRequest.address)) {
        errorUtils.throwError('Invalid Ethereum address format!', 422);
    }

    let userWithEthaddress = await mongooseUserRepo.findOne({
        $and: [
            {ethaddress: profileUpdateRequest.address},
            {email: {$ne: req.email}}]
    });

    if (userWithEthaddress) {
        errorUtils.throwError('This Ethereum address is already taken.', 400);
    }

    //Check SC entity for current ETH Address

    if('ethaddress' in userToUpdate && userToUpdate.ethaddress)
    {
        //User already has an Etherum address. Not checking ethaddress value since thats taken care by the model thats inserting the value
        //boolean userAllowed = true;
        let userEntity = await connectionsFacade.getEntity(userToUpdate.ethaddress);
        let userHasEntity = userToUpdate.ethaddress && userEntity && userEntity.active;
        if (userHasEntity) {

            if('owner' in userEntity && userEntity.owner === userToUpdate.ethaddress)
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('User has done active transactions with their current Ethereum address.', 400);
            }
        }
        let newAddressEntity = await connectionsFacade.getEntity(profileUpdateRequest.address);
        if(newAddressEntity && newAddressEntity.active)
        {
            if('owner' in newAddressEntity && newAddressEntity.owner === profileUpdateRequest.ethaddress)
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('The new Ethereum address has been used for active transactions.', 400);
            }
        }
    }

    //Check SC entity for current ETH Address

    if('ethaddress' in userToUpdate && userToUpdate.ethaddress)
    {
        //User already has an Etherum address. Not checking ethaddress value since thats taken care by the model thats inserting the value
        //boolean userAllowed = true;
        let userEntity = await connectionsFacade.getEntity(userToUpdate.ethaddress);
        let userHasEntity = userToUpdate.ethaddress && userEntity && userEntity.active;
        if (userHasEntity) {

            if('owner' in userEntity && userEntity.owner.toLowerCase() === userToUpdate.ethaddress.toLowerCase())
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('User has done active transactions with their current Ethereum address', 400);
            }
        }
        let newAddressEntity = await connectionsFacade.getEntity(profileUpdateRequest.address);
        if(newAddressEntity && newAddressEntity.active)
        {
            if('owner' in newAddressEntity && newAddressEntity.owner.toLowerCase() === profileUpdateRequest.address.toLowerCase())
            {
                //User owns an entity that has done an transaction. So deny
                errorUtils.throwError('The new Ethereum address has been used for active transactions', 400);
            }
        }
    }

    await userRepo.update({_id: user_id}, {$set: {ethaddress : profileUpdateRequest.address}});
    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);


    res.status(200).send();
}

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        address: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{40}$"
        }
    },
    required: ['address'],
    additionalProperties: false
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{24}$"
        }
    }
};
