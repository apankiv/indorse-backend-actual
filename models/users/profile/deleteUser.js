const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteRepo = require('../../services/mongo/mongoose/mongooseVoteRepo');
const mongooseVotingRoundRepo = require('../../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseValidatorRepo = require('../../services/mongo/mongoose/mongooseValidatorRepo');
const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseConnectionsRepo = require('../../services/mongo/mongoose/mongooseConnectionsRepo');
const mongooseImportedContactsRepo = require('../../services/mongo/mongoose/mongooseImportedContactsRepo');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

exports.register = function register(app) {
    app.delete('/users/:user_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(deleteUser));
};

async function deleteUser(req, res) {
    let user_id = safeObjects.sanitize(req.params.user_id);

    //CLAIMS
    let claimsToRemove = await mongooseClaimsRepo.findAll({ownerid : user_id});

    for(let claim of claimsToRemove) {
        await mongooseVoteRepo.deleteMany({claim_id : claim._id});
        await mongooseVotingRoundRepo.deleteMany({claim_id : claim._id});
        await mongooseClaimsRepo.deleteOne({_id : claim._id});
    }

    await mongooseValidatorRepo.remove({user_id : user_id});
    await mongooseVoteRepo.deleteMany({voter_id : user_id});

    //COMPANIES

    await mongooseCompanyRepo.removeFromTeamMembersAndAdvisors(user_id);

    //USERS

    await mongooseUserRepo.deleteOne({_id : user_id});

    await mongooseConnectionsRepo.deleteMany({user_id : user_id})
    await mongooseImportedContactsRepo.deleteMany({user_id : user_id})

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{24}$"
        }
    }
};
