const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseBadgeRepo = require('../../services/mongo/mongoose/mongooseBadgeRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const addCompanyToWork = require('../work/addCompanyToWork');
const helperCollection = require('../../../helpers/collection');

class Profile {
    static async get(criteria) {
        const user = await mongooseUserRepo.findOne(criteria);
        if (!user) {
            return null;
        }
        const { badges, pendingBadges } = await Profile.getUserBadges(user);
        user.badges = badges;
        user.pending_badges = pendingBadges;
        Object.assign(user, Profile.getSocialLinkedFlags(user));

        if (user.pass) {
            user.is_pass = true;
        }

        if (user.work) {
            user.work = await Profile.getWorkCompanyExperiences(user);
        }
        user.myCompanies = await Profile.getCompanies(user);
        return User.toSafeObject(user, {
            user_id: user._id,
            role: user.role,
        });
    }

    static async getUserBadges(user) {
        const badgePrettyIds = [].concat(user.badges, user.pending_badges);
        if (!badgePrettyIds.length) {
            return { badges: [], pendingBadges: [] }; // no badges  available
        }
        const allBadges = helperCollection.convertCollectionToMap(
            await mongooseBadgeRepo.findAllByPrettyIds(badgePrettyIds),
            'id',
        );
        const badges = [];
        const pendingBadges = [];
        if (user.badges) {
            user.badges.forEach((badgePrettyId) => {
                if (allBadges[badgePrettyId]) {
                    badges.push(allBadges[badgePrettyId]);
                }
            });
        }
        if (user.pending_badges) {
            user.pending_badges.forEach((badgePrettyId) => {
                if (allBadges[badgePrettyId]) {
                    pendingBadges.push(allBadges[badgePrettyId]);
                }
            });
        }
        return { badges, pendingBadges };
    }


    static getCompanies(user) {
        return mongooseCompanyRepo.findAll({
            acl: {
                $elemMatch: {
                    userId: user._id,
                    acceptedInvite: true,
                },
            },
        }, { pretty_id: 1 });
    }

    static getSocialLinkedFlags(user) {
        const socialIdProperties = [
            { idName: 'facebook_uid', flagName: 'isFacebookLinked' },
            { idName: 'google_uid', flagName: 'isGoogleLinked' },
            { idName: 'linkedIn_uid', flagName: 'isLinkedInLinked' },
            { idName: 'is_airbitz_user', flagName: 'isAirbitzLinked' },
            { idName: 'civic_uid', flagName: 'isCivicLinked' },
            { idName: 'uport_uid', flagName: 'isUportLinked' },
        ];

        const socialLinkFlagsMap = {};
        socialIdProperties.forEach((socialProperty) => {
            if (user[socialProperty.idName]) {
                socialLinkFlagsMap[socialProperty.flagName] = true;
            }
        });
        return socialLinkFlagsMap;
    }

    static async getWorkCompanyExperiences(user) {
        const wrkExps = user.work;
        const setCompanyPromises = [];
        wrkExps.forEach((wrkExp) => {
            setCompanyPromises.push(addCompanyToWork.addCompany(wrkExp));
        });
        await Promise.all(setCompanyPromises);
        return wrkExps;
    }
}
module.exports = Profile;
