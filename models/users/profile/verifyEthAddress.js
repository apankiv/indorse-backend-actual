const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const verifySignature = require('../../services/ethereum/verifySignature')
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');
const settings = require('../../settings')
const Web3 = new require('web3');
const web3 = new Web3();

const REQUEST_FIELD_LIST = ['ethaddress', 'signature'];

exports.register = function register(app) {
    app.post('/users/ethaddress',
        validate({body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(addEthAddress));
};

async function addEthAddress(req, res) {
    let ethVerifyRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let userToUpdate = await mongooseUserRepo.findOne({email: req.email});

    if(!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    if(userToUpdate.ethaddressverified && userToUpdate.ethaddressverified === true) {
        errorUtils.throwError("User has already verified their Ethereum address.", 400);
    }

    if (!web3.isAddress(ethVerifyRequest.ethaddress)) {
        errorUtils.throwError('Invalid Ethereum address format!', 422);
    }

    let userWithEthaddress = await mongooseUserRepo.findOne({
        $and: [
            {ethaddress: ethVerifyRequest.ethaddress},
            {email: {$ne: req.email}}]
    });

    if (userWithEthaddress) {
        errorUtils.throwError('This Ethereum address is in use by another user.', 400);
    }

    verifySignature.verifySignature(ethVerifyRequest.signature, ethVerifyRequest.ethaddress, settings.INDORSE_SIGN_MSG);

    if(userToUpdate.ethaddress) {
        if (userToUpdate.ethaddress.toLowerCase() !== ethVerifyRequest.ethaddress.toLowerCase()) {
            errorUtils.throwError("Ether address does not match that on user profile.", 400);
        }
        await userRepo.update({email: req.email}, {$set: {ethaddressverified: true}});
    }
    else {
        await userRepo.update({email: req.email}, {$set: {ethaddress: ethVerifyRequest.ethaddress, ethaddressverified: true}});
    }

    res.status(200).send({
        success: true,
        ethaddress: ethVerifyRequest.ethaddress,
        ethaddressVerified: true,
    });
}

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        ethaddress: {
            type: 'string',
            pattern: "^(0x)?[0-9a-fA-F]{40}$",
        },
        signature: {
            type: 'string',
            pattern: "^(0x)?[0-9a-fA-F]{130}$"
        },
    },
    required: ['ethaddress', 'signature'],
    additionalProperties: false
};