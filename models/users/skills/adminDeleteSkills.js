const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobsRepo = require('../../services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const validate = require('../../services/common/validate');
const errorUtils = require('../../services/error/errorUtils');

exports.register = function register(app) {
    app.delete('/skills/:id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(deleteSkill));
};

async function deleteSkill(req, res) {
    let id = safeObjects.sanitize(req.params.id);

    let skillInMongo = await mongooseSkillRepo.findOneById(id);

    if (!skillInMongo) {
        errorUtils.throwError('Skill does not exists in collection', 400);
    }

    const jobWithSkill = await mongooseJobsRepo.findOne({"skills.id": req.params.id});
    if (jobWithSkill) {
        errorUtils.throwError('This skill is linked to one or more jobs, please delete the skills in the jobs first', 400);
    }

    await mongooseSkillRepo.deleteOne({_id: id});

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['id'],
    properties: {
        id: {
            type: 'string',
            minLength: 1
        }
    }
};

