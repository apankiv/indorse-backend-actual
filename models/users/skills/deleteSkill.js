const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.delete('/users/:id/skills/:skill_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(deleteSkill));
};

async function deleteSkill(req, res) {
    let id = safeObjects.sanitize(req.params.id);
    let skill_id = safeObjects.sanitize(req.params.skill_id);

    let user = await mongooseUserRepo.findOneById(id);

    if(!user) {
        errorUtils.throwError("User does not exist", 404);
    }

    if (user.username !== req.username) {
        errorUtils.throwError("Cannot update somebody else's skill", 403);
    }

    let userSkill = user.skills.find((skill) => skill._id.toString() === skill_id);

    if (!userSkill) {
        errorUtils.throwError("User skill does not exist", 404);
    }

    amplitudeTracker.publishData('skill_delete',{feature_category: 'profile', skill: userSkill},req);

    await mongooseUserRepo.update({email: req.email}, {$pull: {skills: {_id: skill_id}}});

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['id','skill_id'],
    properties: {
        id: {
            type: 'string',
            minLength: 1
        },
        skill_id: {
            type: 'string',
            minLength: 1
        }
    }
};
