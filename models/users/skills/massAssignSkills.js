const mongoUsersRepo = require('../../services/mongo/mongoRepository')('users');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

const REQUEST_FIELD_LIST = ['skills_assignment'];

exports.register = function register(app) {
    app.post('/massassignskills', validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(massAssignSkills));
};

async function massAssignSkills(req, res) {

    let massAssignSkillsRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let skills = massAssignSkillsRequest.skills_assignment.split(/\r?\n/);
    for (let skill of skills) {
        let assign = skill.split(',');
        if (assign.length >= 3) {
            let email = assign[0];
            let badge = assign[2];

            email = email.toLowerCase();
            badge = badge.toLowerCase();
            let user = await mongoUsersRepo.findOne({'email': email})

            if (user) {
                console.log(user['badges']);
                if (!('badges' in user && (user['badges'].indexOf(badge) > -1))) {
                    await mongoUsersRepo.update({'email': email}, {$push: {badges: badge}})
                }
            }
        }
    }

    res.status(200).send();
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            skills_assignment: {
                type: 'string',
            },
        },
        required: ['skills_assignment'],
        additionalProperties: false
    };
}