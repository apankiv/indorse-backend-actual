const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['item_key'];

exports.register = function register(app) {
    app.post('/deletework',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(deleteWork));
};

/**
 * @swagger
 * definitions:
 *   DeleteWorkRequest:
 *     type: object
 *     properties:
 *       item_key:
 *         type: string
 */

/**
 * @swagger
 * /deletework:
 *   post:
 *     description: Deletes work from the user profile
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/DeleteWorkRequest'
 *     responses:
 *       200:
 *         description: Deletion successful
 *         schema:
 *           $ref: '#/definitions/Response'
 *       403:
 *         description: When user is not logged in
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function deleteWork(req, res) {
    let deleteWorkRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    amplitudeTracker.publishData('work_experience_delete',{feature_category: 'profile'},req);
    await userRepo.update({email: req.email}, {$pull: {work: {item_key: deleteWorkRequest.item_key}}});

    res.status(200).send({
        success: true,
        message: 'Deleted successfully'
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            item_key: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['item_key'],
        additionalProperties: false
    };
}