const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const stringUtils = require('../../services/blob/stringUtils');

const REQUEST_FIELD_LIST = ['company_name'];

exports.register = function register(app) {
    app.post('/findcompany',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(findWork));
};


/**
 * @swagger
 * definitions:
 *   CompanyQuery:
 *     type: object
 *     properties:
 *       company_name:
 *         type: string
 *   CompanyQueryResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *       schools:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Company'
 */

/**
 * @swagger
 * /findcompany:
 *   post:
 *     description: Searches for company based on criteria
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/CompanyQuery'
 *     responses:
 *       200:
 *         description: Search successful
 *         schema:
 *           type: object
 *           $ref: '#/definitions/CompanyQueryResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function findWork(req, res) {
    let findRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let companyNames = await mongooseCompanyRepo
        .findAll({company_name: stringUtils.createSafeRegexObject(findRequest.company_name)});

    res.status(200).send({
        success: true,
        companies: companyNames
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            company_name: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['company_name'],
        additionalProperties: false
    };
}