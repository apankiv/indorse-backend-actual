const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const cryptoUtils = require('../../services/common/cryptoUtils');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const checkSkillInMongo = require('../skills/checkSkillInMongo');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const addCompanyToWork = require('./addCompanyToWork');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['company_name', 'item_key', 'title', 'location', 'company_id', 'start_date', 'end_date',
    'currently_working', 'activities', 'description', 'skills'];

exports.register = function register(app) {
    app.post('/updatework',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(updateWork));
};

async function updateWork(req, res) {
    let companyUpdateRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    if (companyUpdateRequest.skills)
        companyUpdateRequest.skills = await checkSkillInMongo.checkThatSkillsInMongo(companyUpdateRequest.skills);

    if (companyUpdateRequest.item_key) {
        let userFromMongo = await mongooseUserRepo.findOne({'work.item_key': companyUpdateRequest.item_key});

        let i = 0;
        let work = userFromMongo.work[i];
        while(i < userFromMongo.work.length && work.item_key !== companyUpdateRequest.item_key) {
            i++;
            work = userFromMongo.work[i];
        }

        if (work.company_id !== companyUpdateRequest.company_id) {
            await addCompanyToMongoIfNotThere(companyUpdateRequest);
        }
        amplitudeTracker.publishData('work_experience_update',{feature_category: 'profile', company: companyUpdateRequest},req);

        await mongooseUserRepo.update(
            {'work.item_key': companyUpdateRequest.item_key},
            {$set: {'work.$': companyUpdateRequest}});
    } else {

        await addCompanyToMongoIfNotThere(companyUpdateRequest);

        companyUpdateRequest.item_key = cryptoUtils.generateItemKey();

        amplitudeTracker.publishData('work_experience_add',{feature_category: 'profile', company: companyUpdateRequest},req);
        await mongooseUserRepo.update({email: req.email}, {$push: {work: companyUpdateRequest}})
    }

    await addCompanyToWork.addCompany(companyUpdateRequest);

    res.status(200).send({
        success: true,
        work: companyUpdateRequest,
        message: 'Updated successfully'
    });
}

async function addCompanyToMongoIfNotThere(companyUpdateRequest) {
    let company = await mongooseCompanyRepo.findOne({
        company_name:
            {$regex: '^' + companyUpdateRequest.company_name + '$', $options: 'i'}
    });

    if (company) {
        companyUpdateRequest.company_id = company._id.toString();
    } else {
        let companyNameItem = {
            company_name: companyUpdateRequest.company_name
        };

        companyUpdateRequest.company_id = await mongooseCompanyRepo.insert(companyNameItem);
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            skills:{
                type : 'array',
                items : {
                    type : 'string',
                    pattern: "^([\\w\\d\\.\\-\\s]){2,}$",
                }
            },
            company_name: {
                type: 'string',
                minLength: 1
            },
            item_key: {
                type: 'string',
                minLength: 1
            },
            company_id: {
                type: 'string',
                minLength: 1
            },
            title: {
                type: 'string',
                minLength: 1
            },
            currently_working: {
                type: 'boolean'
            },
            location: {
                type: 'string',
                minLength: 1
            },
            activities: {
                type: 'string',
                minLength: 1
            },
            description: {
                type: 'string',
                minLength: 1
            },
            start_date: {
                type: 'object',
                properties: {
                    year: {
                        type: 'number'
                    },
                    month: {
                        type: 'number'
                    }
                },
                required: ['year'],
                additionalProperties: false
            },
            end_date: {
                type: 'object',
                properties: {
                    year: {
                        type: 'number'
                    },
                    month: {
                        type: 'number'
                    }
                },
                required: ['year'],
                additionalProperties: false
            }
        },
        required: ['company_name'],
        additionalProperties: false
    };
}