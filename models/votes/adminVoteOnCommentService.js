const logger = require('../services/common/logger').getLogger();


exports.checkIfUserVoted = function checkIfUserVoted(vote, userId){
    let downVoteArr = vote.downvote.actionBy;
    let upVoteArr = vote.upvote.actionBy;
    if(downVoteArr){
        for(let i = 0 ; i < downVoteArr.length ; i ++){
            if(downVoteArr[i] === userId){
                return true;
            }
        }
    }

    if(upVoteArr){
        for(let i = 0 ; i < upVoteArr.length ; i ++){
            if(upVoteArr[i] === userId){
                return true;
            }
        }
    }

    return false;
}
/*
    dwonvote and upvote object should exist for all votes but checking if it's null just in case.
 */
exports.checkIfVoteShouldBeInvalidated = function checkIfVoteShouldBeInvalidated(vote){
    if(!vote){
        return false;
    }
    console.log("Vote checking console for temp testing purpose id " + vote._id.toString());

    if(vote.hide && vote.hide.isHidden){
        return true;
    }

    let downvoteScore = 0;
    let upvoteScore = 0;
    let downvote = vote.downvote;
    let upvote = vote.upvote;

    if(downvote){
        downvoteScore = downvote.count;
    }
    if(upvote){
        upvoteScore = upvote.count;
    }

    let totalScore = upvoteScore-downvoteScore;
    if(totalScore<0){
        logger.debug("voteId " + vote._id.toString() + " is invalidate since total score is " + totalScore);
        return true;
    }
    return false;
}