const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');

module.exports = {
    check: async function check(claimId) {
        const claim = await mongooseClaimsRepo.findOneById(claimId);
        const votesSubmitted = await mongooseVoteRepo.findAll({ claim_id: claimId, sc_vote_exists: true });
        return votesSubmitted.length === claim.aipLimit;
    },
};

