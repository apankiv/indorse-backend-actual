const settings = require('../settings');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const assessmentMagicLinkUtils = require('../services/assessmentMagicLink/assessmentMagicLinkUtils');
const VotingRound = require('../services/mongo/mongoose/schemas/votingRound');
const Claims = require('../services/mongo/mongoose/schemas/claims');
const UserAssignment = require('../services/mongo/mongoose/schemas/userAssignment');
const getVotingResultService = require('./getVotingResultService');
const assignmentResultService = require('../services/claims/assignmentResultService');
const claimResultService = require('../services/claims/claimResultService');
const sendIndorseTokenToUser = require('../services/ethereum/indorseToken/proxy/sendIndorseTokenToUser');
const jobUtils = require('../services/jobs/jobUtils');
const claimsEmailService = require('../services/common/claimsEmailService');
const timestampService = require('../services/common/timestampService');
const { sendClaimResultToPartner } = require('../services/claims/sendClaimResultToPartner');

const ONE_ETHER = 1000000000000000000;

module.exports = {

    finishVotingRoundByClaimId: async function finishVotingRoundById(claimId) {
        const round = await mongooseVotingRoundRepo.findOne({ claim_id: claimId });
        await this.finishVotingRound(round);
    },

    finishVotingRound: async function finishVotingRound(round) {
        const claim = await mongooseClaimsRepo.findOneById(round.claim_id);
        const now = timestampService.createTimestamp();

        const results = await getVotingResultService.getVotingResult(claim._id.toString());

        await mongooseVotingRoundRepo.update({ _id: round._id }, { $set: { status: VotingRound.statuses.FINISHED } });
        await mongooseVoteRepo.updateMany({ claim_id: round.claim_id }, { $set: { isVotingRoundOn: false } });

        await mongooseClaimsRepo.update({ _id: round.claim_id }, {
            $set: {
                final_status: results.indorsed,
                in_consensus: results.consensus,
                normalizedRating: results.normalizedRating,
            },
        });

        if (!results.consensus && claim.sc_deadline > (now + Number(settings.CLAIM_VOTE_PERIOD))) {
            await mongooseClaimsRepo.update({ _id: round.claim_id }, { $set: { can_be_reopened: true } });
        }
        let user = null;
        if (claim.ownerid) {
            user = await mongooseUserRepo.findOneById(claim.ownerid);
        }

        if (user) {
            if (results.indorsed) {
                if (claim.type !== Claims.types.ASSIGNMENT) {
                    let resultingLevel = await claimResultService.calculateAssignmentResult(claim);
                    let userEntity = await mongooseUserRepo.findOneEntity({email: user.email});

                    let skill, validation;

                    for (let i = 0; i < userEntity.skills.length; i++) {
                        skill = userEntity.skills[i];
                        validation = skill.validations ? skill.validations.find(v => v && v.id === round.claim_id) : null;
                        if (validation) {
                            break;
                        }
                    }

                    if (!validation) {
                        let userHasSkill;

                        if (userEntity.skills) {
                            userHasSkill = userEntity.skills.find((s) => s.skill && s.skill.name === claim.title.toLowerCase());
                        }

                        let validationToAdd = {
                            type: "claim",
                            level: resultingLevel,
                            id: claim._id.toString(),
                            validated: true
                        };

                        if (userHasSkill) {
                            let skill = userEntity.skills.find((s) => s.skill && s.skill.name === claim.title.toLowerCase());

                            if (!skill.validations) {
                                skill.validations = [];
                            }

                            skill.validations.push(validation);

                            if (validationToAdd.validated && shouldUpdateSkillLevel(skill.level, validationToAdd.level)) {
                                skill.level = validationToAdd.level;
                            }
                        } else {
                            let skill = await mongooseSkillRepo.findOne({name: claim.title.toLowerCase()});

                            userEntity.skills.push({
                                skill: skill,
                                level: resultingLevel,
                                validations: [validationToAdd]
                            });
                        }

                    } else {
                        validation.validated = results.indorsed;

                        if (validation.validated && shouldUpdateSkillLevel(skill.level, validation.level)) {
                            skill.level = validation.level;
                        }
                    }

                    await userEntity.save();
                    // await mongooseUserRepo.setValidationResult(user.email, round.claim_id, results.indorsed, resultingLevel);
                } else {

                    let resultingLevel = await assignmentResultService.calculateAssignmentResult(claim);
                    await mongooseUserRepo.setAssignmentValidationResult(user._id, round.claim_id, resultingLevel);
                    await mongooseUserAssignmentRepo.updateAssignmentResult(claim.user_assignment_id,
                        UserAssignment.states.EVALUATED, resultingLevel);
                }

                await jobUtils.updateUserAppByUserId(claim.ownerid);
            } else if (claim.type === Claims.types.ASSIGNMENT) {
                await mongooseUserAssignmentRepo.update(
                    { _id: claim.user_assignment_id }, // criteria
                    { status: UserAssignment.states.EVALUATED }, // data to set
                );
            }
            await claimsEmailService.sendClaimTalliedEmail(user.name, round.claim_id, user.email);
        } else {
            console.log(`Results sent to parter for claim (id: ${claim._id}) and (clientApp: ${(claim.clientApp && claim.clientApp.clientId) || 'clientApp not found in claim'})`);
            await sendClaimResultToPartner(claim._id).catch((error) => {
                console.log(error);
                console.log('Ignoring this error');
            });
        }
        // after everything
        if (claim.type === Claims.types.ASSIGNMENT) {
            // if it is type of assignment and update its status in affected User Assessments
            await assessmentMagicLinkUtils.markAssignmentEvaluated(claim.user_assignment_id, results.indorsed);
        } else {
            await assessmentMagicLinkUtils.markClaimEvaluated(claim._id, results.indorsed);
        }
    },
};

function shouldUpdateSkillLevel(skillLevel, validationLevel) {
    let skillLevels = ['beginner', 'intermediate', 'expert'];

    if (!skillLevel && validationLevel) return true;
    if (!skillLevel && !validationLevel) return false;

    const skillLevelIndex = skillLevels.indexOf(skillLevel);
    const validationLevelIndex = skillLevels.indexOf(validationLevel);

    return validationLevelIndex >= 0 && skillLevelIndex >= 0 && validationLevelIndex > skillLevelIndex;
}