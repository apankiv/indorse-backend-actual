/* eslint-disable no-await-in-loop */
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const skillUtils = require('../services/skills/skillUtils');
const errorUtils = require('../services/error/errorUtils');
const claims = require('../services/mongo/mongoose/schemas/claims');
const { ObjectID } = require('mongodb');
const settings = require('../settings');

module.exports = {
    getAllowedSkillsForVote: async function getAllowedSkillsForVote(voteId) {
        const vote = await mongooseVoteRepo.findOneById(voteId);
        if (!vote) errorUtils.throwError(`Vote not found with id - ${voteId}`, 404);

        const claim = await mongooseClaimsRepo.findOneById(vote.claim_id);

        if (claim.type === claims.types.ASSIGNMENT && claim.user_assignment_id) {
            // eslint-disable-next-line max-len
            const userAssignment = await mongooseUserAssignmentRepo.findOneById(claim.user_assignment_id);

            if (userAssignment && userAssignment.skills) {
                const assignmentSkillIds = userAssignment.skills.map(item => item.skill);

                const allowedSkillIdsForVoter = [];

                // eslint-disable-next-line no-restricted-syntax
                for (const assignmentSkillId of assignmentSkillIds) {
                    const canVote = await skillUtils.canUserVoteForSkillOrParentSkill(
                        vote.voter_id,
                        assignmentSkillId,
                    );
                    const tentativeSkillVoteCount = await mongooseVoteRepo.countClaimTentativeVotes(
                        vote.claim_id,
                        ObjectID(assignmentSkillId),
                    );
                    const skillVoteQuotaFull =
                        tentativeSkillVoteCount >=
                        claim.aipLimit / userAssignment.skills.length;

                    if (canVote && !skillVoteQuotaFull) {
                        allowedSkillIdsForVoter.push(assignmentSkillId);
                    }
                }

                return allowedSkillIdsForVoter;
            }
        } else if (claim.type === claims.types.BOAST) {
            const skill = await mongooseSkillRepo.findOne({ name: claim.title.toLowerCase() });

            // eslint-disable-next-line max-len
            const tentativeVoteCount = await mongooseVoteRepo.countClaimTentativeVotes(vote.claim_id);

            if (tentativeVoteCount < claim.aipLimit) {
                return [skill._id];
            }
        }

        return [];
    },
};
