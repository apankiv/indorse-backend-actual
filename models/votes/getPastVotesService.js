const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const getVoteStatusService = require('./getVoteStatusService');

module.exports = {
    getPastVotes: async function getPastVotes(user_id, pageNumber, pageSize, filters) {
        if (!pageNumber) {
            pageNumber = 1;
        }
        if (!pageSize) {
            pageSize = 10;
        }

        let skip = (pageNumber - 1) * pageSize;
        let limit = pageSize;

        let selector = {
            $or: [
                {$and: [{voter_id: user_id}, {isVotingRoundOn: false}]},
                {$and: [{voter_id: user_id}, {isVotingRoundOn: true}, {sc_vote_exists: true}]}]
        };

        // let selector = {
        //     voter_id: user_id
        // };


        if(filters) {
            selector['$and'] = [filters];
        }

        let pastVotesQuery = await Promise.all([mongooseVoteRepo.findAllSkipLimitPromise(selector, skip, limit), mongooseVoteRepo.countAllPromise(selector)]);
        let pastVotes = pastVotesQuery[0];
        let totalCount = pastVotesQuery[1];

        let pastVotesResult = await Promise.all(pastVotes.map((vote) => getVoteStatusService.getVoteStatus(vote._id, false)));

        return {
            votedClaims: pastVotesResult,
            totalPage: Math.ceil(totalCount / pageSize)
        }
    }
}

