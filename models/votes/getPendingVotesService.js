const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const Votes = require('../services/mongo/mongoose/schemas/vote');
const getVoteStatusService = require('./getVoteStatusService');

module.exports = {
    getPendingVotes: async function getPendingVotes(user_id) {
        let pendingVotes = await mongooseVoteRepo.findAll({
            $and: [
                { voter_id: user_id },
                { isVotingRoundOn: true },
                { sc_vote_exists: { $ne: true } },
            ],
        });

        pendingVotes = await Promise.all(pendingVotes.map(vote => getVoteStatusService.getVoteStatus(vote._id, true)));

        pendingVotes = pendingVotes.filter(vote => vote.voteStatus === Votes.statuses.PENDING_VOTE);

        return pendingVotes;
    },
};
