const timestampService = require('../services/common/timestampService');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');

const SECONDS_IN_HOUR = 3600;

module.exports = {

    getReward: async function getReward(vote) {
        let votingRound = await mongooseVotingRoundRepo.findOneById(vote.voting_round_id);
        let now = timestampService.createTimestamp();

        let hoursSinceCreation = Math.floor((now - votingRound.end_registration) / SECONDS_IN_HOUR);

        switch (hoursSinceCreation) {
            case 0:
            case 1:
                return 9;
            case 2:
                return 8;
            case 3:
                return 7;
            case 4:
                return 6;
            case 5:
                return 5;
            default:
                return 4;
        } 
    },

    getRewardAtTimestamp: async function getRewardAtTimestamp(vote, timestamp) {
        let votingRound = await mongooseVotingRoundRepo.findOneById(vote.voting_round_id);

        let hoursSinceCreation = Math.floor((timestamp - votingRound.end_registration) / SECONDS_IN_HOUR);

        switch (hoursSinceCreation) {
            case 0:
            case 1:
                return 9;
            case 2:
            case 3:
                return 7;
            case 4:
            case 5:
                return 5;
            default:
                return 4;
        }
    }
};
