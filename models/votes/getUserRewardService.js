const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const getRewardService = require('./getRewardService');

module.exports = {

    getRewardStatement: async function getRewardStatement(user_id) {
        let votes = await mongooseVoteRepo.findByVoterId(user_id);

        let rewardsPending = 0;
        let rewardsDistributed = 0;
        let totalVotes = 0;

        for (let curVote of votes) {
            if (curVote.sc_vote_exists === true) {
                if (!curVote.reward) {
                    curVote.reward = await getRewardService.getReward(curVote);
                }

                if (curVote.paid) {
                    rewardsDistributed = rewardsDistributed + curVote.reward;
                } else {
                    rewardsPending = rewardsPending + curVote.reward;
                }
                totalVotes = totalVotes + 1;
            }
        }

        let totalPayout = rewardsDistributed + rewardsPending;

        return {
            totalPayout: totalPayout,
            totalVotes: totalVotes,
            rewardsDistributed: rewardsDistributed,
            rewardsPending: rewardsPending
        };
    }
};

