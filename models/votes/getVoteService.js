const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../services/error/errorUtils');
const allowedSkillsForVoterService = require('./getAllowedSkillsForVoterService');

module.exports = {
    getVote: async function getVote(voteId) {
        const vote = await mongooseVoteRepo.findOneById(voteId);

        if (!vote) {
            errorUtils.throwError('Vote not found!', 404);
        }

        // eslint-disable-next-line max-len
        vote.allowedSkillIdsForVoter = await allowedSkillsForVoterService.getAllowedSkillsForVote(vote._id);

        if (vote.skillId) {
            vote.skill = await mongooseSkillRepo.findOneById(vote.skillId);
        }

        return vote;
    },
};
