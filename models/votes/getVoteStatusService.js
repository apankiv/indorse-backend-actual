const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const getVoteService = require('./getVoteService');
const getRewardService = require('./getRewardService');
const getClaimService = require('../services/claims/getClaimService');
const Votes = require('../services/mongo/mongoose/schemas/vote');
const allowedSkillsForVoterService = require('./getAllowedSkillsForVoterService');

module.exports = {
    getVoteStatus: async function getVoteStatus(voteId, isPendingVote) {
        const vote = await getVoteService.getVote(voteId);
        const claim = await mongooseClaimsRepo.findOneById(vote.claim_id);

        let voteStatus;

        if (isPendingVote) {
            // eslint-disable-next-line max-len
            const allowedSkillsForVoter = await allowedSkillsForVoterService.getAllowedSkillsForVote(voteId);
            if (allowedSkillsForVoter.length > 0 || (vote.voted_at && !vote.sc_vote_exists)) {
                // eslint-disable-next-line max-len
                // If it is a pending vote, it's "Voting Round" must be on AND it doesnt have sc_vote. Therefore, it's status is always just a pending vote
                voteStatus = Votes.statuses.PENDING_VOTE;
            } else {
                voteStatus = Votes.statuses.VOTE_MISSING;
            }
        } else {
            // If it is a past vote, there are 3 cases. Indosrse / Rejected / Vote missing
            // eslint-disable-next-line no-lonely-if
            if (!vote.voted_at) {
                voteStatus = Votes.statuses.VOTE_MISSING;
            } else if (!vote.sc_vote_exists) {
                voteStatus = Votes.statuses.VOTE_MISSING;
            } else if (vote.endorsed) {
                voteStatus = Votes.statuses.INDORSED;
            } else {
                voteStatus = Votes.statuses.REJECTED;
            }
        }

        claim.claimStatus = getClaimService.getClaimStatus(claim);
        let user = {};
        if (claim.ownerid) {
            user = await mongooseUserRepo.findOneById(claim.ownerid);
        }

        claim.ownerName = user.name || '';

        let {reward} = vote;

        if (!reward) {
            reward = await getRewardService.getReward(vote);
        }

        let returnObj = {
            feedback: vote.feedback,
            claim,
            voteStatus,
            votedAt: vote.voted_at,
            reward,
            voteId: vote._id.toString(),
        };

        if (vote.upvote) {
            returnObj.adminUpvote = vote.upvote.count;
        } else {
            returnObj.adminUpvote = 0;
        }

        if (vote.downvote) {
            returnObj.adminDownvote = vote.downvote.count;
        } else {
            returnObj.adminDownvote = 0;
        }

        return returnObj;
    },
};
