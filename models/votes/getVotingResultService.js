const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');

const MIN_RATING_PER_SCORE = 1;
const MAX_RATING_PER_SCORE = 5;

// eslint-disable-next-line
const linearScale = (num, in_min, in_max, out_min, out_max) => {
    // Here:
    // in_min and in_max is the range from which the number is given
    // out_min and out_max is the range on which the number is mapped
    return ((num - in_min) * ((out_max - out_min) / (in_max - in_min))) + out_min;
};

function normalizeRating(averageRatingArray) {
    if (!Array.isArray(averageRatingArray)) throw new Error('Pass array of numbers');
    const numberOfRatings = averageRatingArray.length;
    const minScorePerClaim = numberOfRatings * MIN_RATING_PER_SCORE;
    const maxScorePerClaim = numberOfRatings * MAX_RATING_PER_SCORE;
    let sum = 0;
    averageRatingArray.forEach((elem) => {
        sum += parseFloat(elem);
    });
    const normalisedRating = linearScale(sum, minScorePerClaim, maxScorePerClaim, 1, 10);
    return Math.round(normalisedRating) || 1;
};


module.exports = {
    getVotingResult: async function getVotingResult(claimId) {
        const claim = await mongooseClaimsRepo.findOneById(claimId);

        const yesVotes = await mongooseVoteRepo.findAll({
            claim_id: claim._id.toString(),
            endorsed: true,
            sc_vote_exists: true,
        });

        const allVotes = await mongooseVoteRepo.findAll({
            claim_id: claim._id.toString(),
            sc_vote_exists: true,
        });

        const consensusThreshold = Math.floor(claim.aipLimit / 2) + 1;
        return {
            indorsed: allVotes.length >= consensusThreshold && yesVotes.length > (allVotes.length / 2),
            consensus: allVotes.length >= consensusThreshold,
            normalizedRating: this.extractNormalisedRating(allVotes),
        };
    },
    extractNormalisedRating: (votes = []) => {
        const accumulater = {
            quality: 0,
            designPatterns: 0,
            testCoverage: 0,
            readability: 0,
            extensibility: 0,
            completion: 0,
        };
        votes.forEach((vote) => {
            if (vote.feedback) {
                let { feedback } = vote;
                feedback = feedback || {};
                accumulater.quality += feedback.quality;
                accumulater.designPatterns += feedback.designPatterns;
                accumulater.testCoverage += feedback.testCoverage;
                accumulater.readability += feedback.readability;
                accumulater.extensibility += feedback.extensibility;
                accumulater.completion += feedback.completion;
            }
        });
        const averageRating = {
            quality: (votes.length && Math.floor(accumulater.quality / votes.length)) || 0,
            designPatterns: (votes.length && Math.floor(accumulater.designPatterns / votes.length)) || 0,
            testCoverage: (votes.length && Math.floor(accumulater.testCoverage / votes.length)) || 0,
            readability: (votes.length && Math.floor(accumulater.readability / votes.length)) || 0,
            extensibility: (votes.length && Math.floor(accumulater.extensibility / votes.length)) || 0,
            completion: (votes.length && Math.floor(accumulater.completion / votes.length)) || 0,
        };
        const filteredNonZeroRating = (Object.values(averageRating) || []).filter(Boolean);
        return normalizeRating(filteredNonZeroRating);
    },
};
