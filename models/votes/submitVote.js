const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const getVoteService = require('./getVoteService');
const getClaimTierStatusService = require('../claims/getClaimTierStatusService');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const cryptoUtils = require('../services/common/cryptoUtils');
const routeUtils = require('../services/common/routeUtils');
const timestampService = require('../services/common/timestampService');
const settings = require('../settings');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const claims = require('../services/mongo/mongoose/schemas/claims');
const mongooseUserAssignmentRepo = require('../services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseClaimEventsRepo = require('../services/mongo/mongoose/mongooseClaimEvents')
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const ObjectID = require('mongodb').ObjectID;
const skillUtils = require('../../models/services/skills/skillUtils')
const eventUtils = require('../../models/services/common/eventUtils')
const rabbitMqPublish = require('../../models/services/rabbitMq/publishToExchange');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const mongooseValidatorRepo = require('../services/mongo/mongoose/mongooseValidatorRepo');

const REQUEST_FIELD_LIST = ['endorse', 'feedback', 'skillid', 'timeTakenForFeedback'];

exports.register = function register(app) {
    app.post(
        '/votes/:vote_id',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.FULL_ACCESS)),
        routeUtils.asyncMiddleware(submitVote),
    );
};

async function submitVote(req, res) {
    let voteSubmissionRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let vote_id = safeObjects.sanitize(req.param("vote_id"));
    let vote = await getVoteService.getVote(vote_id);

    voteSubmissionRequest.vote_id = vote_id;
    voteSubmissionRequest.claim_id = vote.claim_id;

    let claim = await mongooseClaimsRepo.findOne({_id: vote.claim_id});

    let validatorDoc = await mongooseValidatorRepo.findOne({user_id : req.user_id});

    if(!validatorDoc) {
        errorUtils.throwError("User is not a validator!", 400);
    }

    if(claim.validatorAssignedBytier) {
        let tiersStatus = await getClaimTierStatusService.getClaimTierStatus(claim);
        if(tiersStatus[validatorDoc.tier].numTentativeVotes + 1 > tiersStatus[validatorDoc.tier].maxVote) {
            errorUtils.throwError('Tier vote threshold exceeded maximum number of votes !', 400);
        }
    }

    amplitudeTracker.publishData('vote_submission_initialized', voteSubmissionRequest, req);

    if (vote.voter_id !== req.user_id) {
        amplitudeTracker.publishData('vote_submission_failed_not_users_vote', voteSubmissionRequest, req);
        errorUtils.throwError('Users can only modify votes they own!', 403);
    }

    if (vote.sc_vote_exists) {
        amplitudeTracker.publishData('vote_submission_failed_already_voted', voteSubmissionRequest, req);
        errorUtils.throwError('Vote is already registered in the smart contract!', 400);
    }

    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(vote.claim_id);

    let now = timestampService.createTimestamp();

    if (now > votingRound.end_voting) {
        amplitudeTracker.publishData('vote_submission_failed_deadline_passed', voteSubmissionRequest, req);
        errorUtils.throwError('Voting deadline has passed!', 400);
    }

    let isFinalVote, isFinalVoteForSkill = false;
    let noOfTentativeVotes = 0;
    //Check if this claim is for an assignment   
    if (claim.type === claims.types.ASSIGNMENT) {
        if (!voteSubmissionRequest.skillid) {
            errorUtils.throwError('Skill id missing on voting assignment', 403);
        }


        let userAssignment = await mongooseUserAssignmentRepo.findOne({_id: claim.user_assignment_id});
        if (!userAssignment) {
            errorUtils.throwError('User assignment not found', 404);
        }

        if (userAssignment.skills.findIndex(i => i.skill.toString() === voteSubmissionRequest.skillid) < 0) {
            errorUtils.throwError('Voted skill is not part of assignment!' + voteSubmissionRequest.skillid, 400);
        }

        if (!skillUtils.canUserVoteForSkillOrParentSkill(req.user_id, voteSubmissionRequest.skillid)) {
            errorUtils.throwError('User cannot vote for this skill' + voteSubmissionRequest.skillid, 403);
        }

        let votesSubmittedForClaim = await mongooseVoteRepo.findAll({$and: [{claim_id: vote.claim_id}, {skillId: ObjectID(voteSubmissionRequest.skillid)}, {voted_at: {$gt: 0}}]});
        noOfTentativeVotes = votesSubmittedForClaim.length;
        if (noOfTentativeVotes >= claim.aipLimit / userAssignment.skills.length) {
            amplitudeTracker.publishData('vote_submission_failed_max_votes', voteSubmissionRequest, req.user_id);
            errorUtils.throwError('Vote threshold exceeded maximum number of votes!', 400);
        }

        if (noOfTentativeVotes + 1 == claim.aipLimit / userAssignment.skills.length) {
            isFinalVoteForSkill = true;
        }

        // isFinalVote is used to close the votingRound. and VotingRound will be closed when an assignment received all votes
        let allVotes = await mongooseVoteRepo.findAll({$and: [{claim_id: vote.claim_id}, {voted_at: {$gt: 0}}]});
        if (allVotes.length + 1 == claim.aipLimit) {
            isFinalVote = true;
        }
    } else {
        let votesSubmittedForClaim = await mongooseVoteRepo.findAll({$and: [{claim_id: vote.claim_id}, {voted_at: {$gt: 0}}]});
        noOfTentativeVotes = votesSubmittedForClaim.length;
        if (noOfTentativeVotes >= claim.aipLimit) {
            amplitudeTracker.publishData('vote_submission_failed_max_votes', voteSubmissionRequest, req.user_id);
            errorUtils.throwError('Vote threshold exceeded maximum number of votes!', 400);
        }
        // when vote threshold is N, and if currently there are N-1 tentative votes, this vote is final vote.
        if (noOfTentativeVotes + 1 === claim.aipLimit) {
            isFinalVote = true;
            isFinalVoteForSkill = true;
        }
    }


    const votingToken = await cryptoUtils.genRandomString(32);
    if (!vote.voted_at) {
        const updateObj = {
            voted_at: now,
            endorsed: voteSubmissionRequest.endorse,
            voting_token: votingToken,
            timeTakenForFeedback: voteSubmissionRequest.timeTakenForFeedback,
        };

        let skillObj;
        if (claim.type === claims.types.ASSIGNMENT) {
            updateObj.skillId = ObjectID(voteSubmissionRequest.skillid);
            skillObj = await mongooseSkillRepo.findOneById(updateObj.skillId);
        } else {
            // if skill type is BOAST
            skillObj = await mongooseSkillRepo.findOne({name: claim.title.toLowerCase()});
        }

        updateObj.feedback = voteSubmissionRequest.feedback;

        await mongooseVoteRepo.update({_id: vote_id}, {$set: updateObj});

        noOfTentativeVotes += 1;

        const pubishMessage = {
            claimId: vote.claim_id,
            noOfTentativeVotes,
            skill: skillObj,
            isMaxVoteReached: isFinalVoteForSkill,
            voteId: vote_id,
            tiers : await getClaimTierStatusService.getClaimTierStatus(claim)
        };

        console.log(`Debug vote publish msg ${JSON.stringify(pubishMessage)}`);
        await rabbitMqPublish.publishToExchange('Vote.Updated.DLQ.Exchange', {voteSubmitted: pubishMessage});

        // Proactively setting the voting round
        if (isFinalVote) {
            await mongooseVoteRepo.updateMany({$and: [{claim_id: vote.claim_id}, {voted_at: {$exists: false}}]}, {$set: {isVotingRoundOn: false}});
        }
    }

    let claimEvent = {
        user_id: req.user_id,
        claim_id: vote.claim_id,
        voting_round_id: votingRound._id.toString(),
        event: 'vote_submitted',
        timeStamp: timestampService.createTimestamp(),
        properties: {
            endorsed: voteSubmissionRequest.endorse
        }
    };

    if (!await eventUtils.isEventRecorded(claimEvent)) {
        await mongooseClaimEventsRepo.insert(claimEvent);
    }

    amplitudeTracker.publishData('vote_submission_successful', voteSubmissionRequest, req);
    res.status(200).send({votingToken: votingToken});
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['vote_id'],
    properties: {
        vote_id: {
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};

const valuesAllowedInTimeTakenForFeedback = ['0-5', '5-15', '15-30', '30+'];

const BODY_SCHEMA = {
    type: 'object',
    required: ['endorse', 'feedback'],
    properties: {
        endorse: {
            type: 'boolean',
        },
        feedback: {
            type: 'object',
            properties: {
                quality: {
                    type: 'number',
                },
                designPatterns: {
                    type: 'number',
                },
                gitFlow: {
                    type: 'number',
                },
                completion: {
                    type: 'number',
                },
                explanation: {
                    type: 'string',
                },
                testCoverage: {
                    type: 'number',
                },
                readability: {
                    type: 'number',
                },
                extensibility: {
                    type: 'number',
                },
                yearsOfExperience: {
                    type: 'string',
                    enum: ['0-1', '1-3', '3-5', '5-8', '8+']
                }
            },
            required: ['yearsOfExperience', 'quality', 'designPatterns', 'explanation', 'testCoverage', 'readability', 'extensibility'],
            additionalProperties: false,
        },
        timeTakenForFeedback: {
            type: 'string',
            enum: valuesAllowedInTimeTakenForFeedback,
        },
    },
};