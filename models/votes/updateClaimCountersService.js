const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');

module.exports = {

    updateCounters: async function updateCounters(vote, claim) {
        if (!claim.endorse_count) {
            claim.endorse_count = 0;
        }

        if (!claim.flag_count) {
            claim.flag_count = 0;
        }

        if (vote.endorsed) {
            await mongooseClaimsRepo.update(
                { _id: vote.claim_id },
                { endorse_count: claim.endorse_count + 1 }
            );
        } else {
            await mongooseClaimsRepo.update(
                { _id: vote.claim_id },
                { flag_count: claim.flag_count + 1 }
            );
        }
    }
};