const safeObjects = require('../services/common/safeObjects');
const mongoWizardRepo = require('../services/mongo/mongoRepository')('wizards');
const routeUtils = require('../services/common/routeUtils');
const stringUtils = require('../services/blob/stringUtils');

exports.register = function register(app) {
    app.get('/wizards',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    let searchParam, pageNumber, pageSize, sortObj;

    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.pageNumber) {
        pageNumber = safeObjects.sanitize(req.query.pageNumber);
    } else {
        pageNumber = 1;
    }

    if (req.query.pageSize) {
        pageSize = safeObjects.sanitize(req.query.pageSize);
    } else {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    if (req.query.sort) {
        let sortParam = safeObjects.sanitize(req.query.sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            name: 1
        }
    }

    let wizardCursor, matchingWizards;

    if (searchParam) {
        wizardCursor = await mongoWizardRepo.findAllWithCursor(
            {
                $or: [{name: stringUtils.createSafeRegexObject(searchParam)},
                    {title: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingWizards = await wizardCursor.count();
    } else {
        wizardCursor = await mongoWizardRepo.findAllWithCursor({name: {$exists: true}});
    }

    let sortedWizards = await wizardCursor.sort(sortObj).skip(skip).limit(limit).toArray();
    let wizardTotalCursor = await mongoWizardRepo.findAllWithCursor({name: {$exists: true}});

    let responseObj = {
        wizards: sortedWizards,
        totalWizards: await wizardTotalCursor.count()
    };

    if (matchingWizards) {
        responseObj.matchingWizards = matchingWizards;
    }

    res.status(200).send(responseObj);
}


