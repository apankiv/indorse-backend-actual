const mongooseWizardsRepo = require('../services/mongo/mongoose/mongooseWizardsRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.get('/wizards/:wizard_name',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getWizard));
};

async function getWizard(req, res) {
    let wizard_name = safeObjects.sanitize(req.params.wizard_name);

    let wizard = await mongooseWizardsRepo.findOneByName(wizard_name);

    if (!wizard) {
        errorUtils.throwError("Wizard not found", 404);
    }

    res.status(200).send(wizard);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['wizard_name'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};

