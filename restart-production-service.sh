#!/bin/bash

set -e

ECS_REGION="ap-southeast-1"

echo "Configuring AWS..."
aws --version
aws configure set default.region $ECS_REGION
aws configure set default.output json
echo "AWS configured!"

aws ecs update-service --cluster CI-production --service production-backend --task-definition production-backend --force-new-deployment