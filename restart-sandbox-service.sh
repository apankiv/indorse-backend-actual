#!/bin/bash

set -e

ECS_REGION="ap-southeast-1"
CLUSTER_NAME="CI-sandbox"
SERVICE_NAME="sandbox-backend"
TASK_DEFINITION="sandbox-backend"

echo "Configuring AWS..."
aws --version
aws configure set default.region $ECS_REGION
aws configure set default.output json
echo "AWS configured!"

aws ecs update-service --cluster $CLUSTER_NAME --service $SERVICE_NAME --task-definition $TASK_DEFINITION --force-new-deployment