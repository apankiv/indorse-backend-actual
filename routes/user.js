const routeUtils = require('../models/services/common/routeUtils');
const UserProfileController = require('../controllers/user/profile');

class User {
    static register(app) {
        app.post('/me', routeUtils.asyncMiddleware(UserProfileController.get));
    }
}

module.exports = User;
