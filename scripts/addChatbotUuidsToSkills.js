const chatbotSkills = require('./chatbot.json');
const mongooseSkillRepo = require('../models/services/mongo/mongoose/mongooseSkillRepo');
const async = require('async');

async function migrate() {
    return new Promise((resolve, reject) => {
        const skillsToUpdateWithChatbotIds = getSkillsToUpdate();
        console.log(
            'These skills will be attempted to be updated with chatbot uuids.',
            JSON.stringify(skillsToUpdateWithChatbotIds),
        );
        const q = async.queue(async (skill) => {
            try {
                await updateSkillChatbotId(skill);
            } catch (err) {
                reject(err);
            }
        }, 1);
        q.push(skillsToUpdateWithChatbotIds);
        q.drain = () => resolve(true);
    });
}

function getSkillsToUpdate() {
    const skillsToUpdate = [
        'react.js',
        'node.js',
        'vue.js',
    ];
    return chatbotSkills.filter(chatbotUuidSkill => skillsToUpdate
        .indexOf(chatbotUuidSkill.skillName) !== -1);
}

async function updateSkillChatbotId(skillWithChatboId) {
    console.log('Getting skill from db', JSON.stringify(skillWithChatboId));
    const query = {
        name: skillWithChatboId.skillName,
        parents: { $exists: true },
        'validation.chatbot_uuid': { $exists: false },
    };
    const skill = await mongooseSkillRepo.findOne(query);
    if (!skill) {
        console.error(`Skill not available in database ${JSON.stringify(skillWithChatboId)} \
with query : ${JSON.stringify(query)}`);
        return null;
    }
    console.log(`Attempting update for ${JSON.stringify(skillWithChatboId)}`);
    const updateResult = await mongooseSkillRepo.update(
        { _id: skill._id.toString() },
        {
            'validation.chatbot_uuid': skillWithChatboId.chatbotUuid,
            'validation.chatbot': true,
        },
    );
    console.log(`Update complete with result : ${JSON.stringify(updateResult)}`);
    return updateResult;
}


(async () => {
    try {
        if (require.main !== module) {
            console.error('Script to be run as main directly.');
            return;
        }
        await migrate();
        console.log('DONE');
        process.exit(0);
    } catch (err) {
        console.error(err.stack);
        process.exit(1);
    }
})();
