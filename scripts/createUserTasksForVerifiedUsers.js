const mongooseUserRepo = require('../models/services/mongo/mongoose/mongooseUserRepo');
const userTaskHandler = require('../models/services/userTask/userTaskHandler');

async function migrate() {
    const criteriaVerifiedUser = {
        verified: true,
        isDeleted: {
            $ne: true,
        },
    };
    const projectionVerifiedUser = {
        _id: 1,
    };
    const users = await mongooseUserRepo.findAll(criteriaVerifiedUser, projectionVerifiedUser);
    console.log(`Users Length ${users.length}`);
    for (let i = 0; i < users.length; i += 1) {
        if (users[i]) {
            const userId = users[i]._id;
            await userTaskHandler.updateUserTasksStatus(userId, false).catch((error) => {
                console.log(`User: ${users[i]._id}`);
                console.log('Error: ', error);
            });
            console.log(`Success User ${i}: ${users[i]._id}`);
        } else {
            console.log(`Skipped User ${i}: ${users[i]._id}`);
        }
    }
    console.log("User Batch Update Ended !");
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log('Error in migration: ', e);
    }
})();
