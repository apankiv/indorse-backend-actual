'use strict'

const mongooseCompanyRepo = require('../models/services/mongo/mongoose/mongooseCompanyRepo');

async function migrate () {

    const companies = await mongooseCompanyRepo.findAll({});
    try{

        for(let i = 0 ; i < companies.length ; i ++){
            let company = companies[i];
            if(company.credits && company.credits.assessmentAssignment){
                // do nothing
            }else{
                let creditLimit = {
                    $set: {
                        credits: {
                            assessmentAssignment: {
                                limit: 0,
                                used: 0,
                                limitReachedTimestamp: 1
                            }
                        }
                    }
                };
                await mongooseCompanyRepo.update({_id: company._id}, creditLimit);
            }
        }
    }catch(e){
        console.log('error while migrating company default credit ' + e);
    }
};

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();