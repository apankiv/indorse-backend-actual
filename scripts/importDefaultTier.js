'use strict'

const mongooseValidatorRepo = require('../models/services/mongo/mongoose/mongooseValidatorRepo');

async function migrate () {
  /*

 var defaultData = {
    $set: {
            tier: '_2'
     }
        }

db.getCollection("validators").updateMany({},defaultData);

   */
    try{
        let defaultData = {
            $set: {
                tier: '_2'
            }
        };
        await mongooseValidatorRepo.updateMany({}, defaultData);
    }catch(e){
        console.log('error while migrating vote default tier ' + e);
    }
};

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();