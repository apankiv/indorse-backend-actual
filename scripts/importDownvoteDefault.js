'use strict'

const mongooseVoteRepo = require('../models/services/mongo/mongoose/mongooseVoteRepo');

async function migrate () {
    /*
        var defaultData2 = {
  		$set:{
  		  "upvote" : {"count":0, "actionBy":[]},
  		  "downvote": {"count":0, "actionBy":[]},
  		  "hide":{"isHidden":false, "actionBy":[]}
  		  }

  		};
  		
db.getCollection("votes").updateMany({},defaultData2);
     */
    try{
        let defaultData = {
            $set:{
                "upvote" : {"count":0, "actionBy":[]},
                "downvote": {"count":0, "actionBy":[]},
                "hide":{"isHidden":false, "actionBy":[]}
            }
        }
        await mongooseVoteRepo.updateMany({}, defaultData);
    }catch(e){
        console.log('error while migrating vote default credit ' + e);
    }
};

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();