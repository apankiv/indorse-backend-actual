const mongooseClaimsRepo = require('../models/services/mongo/mongoose/mongooseClaimsRepo');

async function migrate() {
    const criteriaClaimsAll = {
        approved: {
            $exists: true,
        },
    };
    const claims = await mongooseClaimsRepo.findAll(criteriaClaimsAll);
    console.log(`Claims Length ${claims.length}`);
    for (let i = 0; i < claims.length; i += 1) {
        if (claims[i]) {
            const criteriaClaim = {
                _id: claims[i]._id,
            };
            const updateObject = {
                $set: {
                    aipLimit: 10,
                },
            };
            await mongooseClaimsRepo.update(criteriaClaim, updateObject).catch((error) => {
                console.log(`claim: ${claims[i]._id}`);
                console.log('Error: ', error);
            });
            console.log(`Success Claim ${i}: ${claims[i]._id}`);
            console.log('claims[i].aipLimit: ', claims[i].aipLimit);
        } else {
            console.log(`Skipped Claim ${i}: ${claims[i]._id}`);
        }
    }

    console.log("Claim Batch Update Ended !");
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log('Error in migration: ', e);
    }
})();
