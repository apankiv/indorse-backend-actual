// This work was acheived by this query
// db.getCollection("company_names")
//     .update(
//         { magicLink: { $exists: 0 } },
//         { $set: { magicLink: { autoAllowedAssignment: true } } },
//         { multi: true }
//     );
// db.getCollection("assessmentmagiclinks")
//     .update(
//         { autoAllowedAssignment: { $exists: 0 } },
//         { $set: { autoAllowedAssignment: true } },
//         { multi: true }
//     );
const config = require('config');
const mongooseCompanyRepo = require('../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseAssessmentMagicLinkRepo = require('../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');

async function migrate() {
    const criteriaCompanyAll = {};
    const companies = await mongooseCompanyRepo.findAll(criteriaCompanyAll);
    console.log(`Company Length ${companies.length}`);
    for (let i = 0; i < companies.length; i += 1) {
        if (companies[i]) {
            const criteriaCompany = {
                _id: companies[i]._id,
            };
            const updateObject = {
                $set: {
                    magicLink: {
                        autoAllowedAssignment: true,
                    },
                },
            };
            await mongooseCompanyRepo.update(criteriaCompany, updateObject).catch((error) => {
                console.log(`company: ${companies[i].pretty_id}`);
                console.log('Error: ', error);
            });
            console.log(`Success Company ${i}: ${companies[i].pretty_id}`);
        } else {
            console.log(`Skipped Company ${i}: ${companies[i].pretty_id}`);
        }
    }

    console.log("Company Batch Update Ended !");
    const criteriaMagicLinkAll = {};
    const magicLinks = await mongooseAssessmentMagicLinkRepo.findAll(criteriaMagicLinkAll);
    console.log(`MagicLink Length: ${magicLinks && magicLinks.length}`);
    for (let i = 0; i < magicLinks.length; i += 1) {
        if (magicLinks[i] && magicLinks[i].autoAllowedAssignment !== true) {
            const criteriaMagicLink = {
                _id: magicLinks[i]._id,
            };
            const updateObjectMagicLink = {
                $set: {
                    autoAllowedAssignment: true,
                },
            };
            await mongooseAssessmentMagicLinkRepo.update(criteriaMagicLink, updateObjectMagicLink).catch((error) => {
                console.log(`magicLink: ${magicLinks[i]._id}`);
                console.log('Error: ', error);
            });
            console.log(`MagicLink updated ${i}: ${magicLinks[i]._id}`);
        } else {
            console.log(`MagicLink skipped ${i}: ${magicLinks[i]._id}`);
        }
    }
    console.log("MagicLink Batch Update Ended !");
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log('Error in migration: ', e);
    }
})();
