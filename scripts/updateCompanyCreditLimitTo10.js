'use strict'

const mongooseCompanyRepo = require('../models/services/mongo/mongoose/mongooseCompanyRepo');

async function migrate () {

    const companies = await mongooseCompanyRepo.findAll({});
    try{

        for(let i = 0 ; i < companies.length ; i ++){
            let company = companies[i];
            if(company.credits && company.credits.assessmentAssignment && company.pretty_id){
                let creditLimit = {
                    $set: {
                        credits: {
                            assessmentAssignment: {
                                limit: 10,
                                used: 0,
                                limitReachedTimestamp: null
                            }
                        }
                    }
                };
                await mongooseCompanyRepo.update({_id: company._id}, creditLimit);
            }
        }
    }catch(e){
        console.log('error while migrating company default credit ' + e);
    }
};

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();