const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const config = require('config');
const auth = require('./models/services/auth/auth');
const bearerToken = require('express-bearer-token');
const swaggerJSDoc = require('swagger-jsdoc');
const path = require('path');
const methodOverride = require('method-override');
const morgan = require('morgan');
const routeConfig = require('./routeConfig');
const validationErrorHandler = require('./models/services/error/jsonSchemaValidationErrorHandler');
const genericErrorHandler = require('./models/services/error/genericErrorHandler');
const rabbitMq = require('./models/services/rabbitMq/rabbitMq');
const jwt = require('jsonwebtoken');
const settings = require('./models/settings');
const logger = require('./models/services/common/logger').getLogger('ErrorMiddleware');
const slackService = require('./models/services/common/slackService');
const errorUtils = require('./models/services/error/errorUtils');
const graphiqlExpress = require('apollo-server-express').graphiqlExpress;
const graphqlExpress = require('apollo-server-express').graphqlExpress;
const depthLimit = require('graphql-depth-limit');
const apolloUploadExpress = require('apollo-upload-server').apolloUploadExpress;


const graphQLSchema = require('./graphql/schema');
const createServer = require('http').createServer;

const execute = require('graphql').execute;
const subscribe = require('graphql').subscribe;
const SubscriptionServer = require('subscriptions-transport-ws').SubscriptionServer;
const fs = require('fs');
const uuidv1 = require('uuid/v1');

global.appRoot = path.resolve(__dirname);

app.use(helmet());
app.use(bodyParser.json({limit: 10241020, type: 'application/json'}));

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(methodOverride());
app.use(morgan('combined'));
app.use(bearerToken());
app.use(auth);

const cors = require('cors');

app.use(cors());

app.use('/graphql', bodyParser.json(), apolloUploadExpress(), graphqlExpress(async (req, res, params) => ({
        schema: graphQLSchema,
        context: {
            req: req,
            res: res
        },
        validationRules: [depthLimit(10)],
        formatError : function(err) {

            const requestID = uuidv1();

            let bug = {
                stack: err.stack,
                url: req.url,
                method: req.method,
                body : JSON.stringify(req.body),
                requestID: requestID,
                email: req.email
            };

            logger.error(err.message, bug);

            const responseData = {
                message: err.message,
                requestID: requestID,
                success: false,
                code: err.originalError && err.originalError.code,
                locations: err.locations,
                path: err.path
            };

            let code;

            if(err.originalError && err.originalError.errors && err.originalError.errors[0] &&
                err.originalError.errors[0].extensions) {
                code = err.originalError.errors[0].extensions;
            }

            if (errorUtils.isHttpCodeAllowedInResponse(code)) {
                responseData.code = code;
            } else {
                responseData.message = "Oops something went wrong! Please send this identifier to our support email and we will investigate " +
                    "the issue: " + requestID;
                responseData.code = 500;
                slackService.reportBug(bug);
            }

            return responseData;
        }
    })
));


app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PATCH, DELETE, PUT");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Amp-Device-Id, X-Amp-Session-Id, x-recaptcha-token");

    next();
});

routeConfig.configureRoutes(app);


const swaggerDefinition = {
    info: {
        title: 'Node Swagger API',
        version: '1.0.0',
        description: 'Demonstrating how to describe a RESTful API with Swagger',
    },
    host: 'localhost:' + config.get('server.port'),
    basePath: '/',
};

// options for the swagger docs
const swaggerOptions = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['./models/users/*.js', './models/users/**/*.js', './models/users/**/**/*.js'],
};

// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(swaggerOptions);

app.get('/swagger.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});

app.use(validationErrorHandler.handleError);

app.use(genericErrorHandler.handleError);

app.use('/graphiql',
    graphiqlExpress({
        endpointURL: '/graphql',
        subscriptionsEndpoint: `${settings.WS_HOST_NAME}/subscriptions`
    }));

const ws = createServer(app);
let subscriptionServer;

ws.listen(config.get('server.port'), () => {
    console.log(`Client is connected to server via websocket on the port: ${config.get('server.port')}`);
})

if (!subscriptionServer) {
    try{
        subscriptionServer = new SubscriptionServer({
            execute,
            subscribe,
            schema: graphQLSchema,
            onConnect: (connectionParams, webSocket) => {
                console.log('Client <-> Server websocket connection is established.');
                return {
                    authorization: connectionParams.authorization
                };
            }
        }, {
            server: ws,
            path: '/subscriptions'
        })
    }catch(e){
        console.log('subscription connection error');
    }

}


ws.on('error', (err) => {
    if (err.code !== 'ECONNRESET') {
        throw err;
    } else {
        console.log('ECONNRESET ERROR');
    }
});


ws.on('open', function open() {
    console.log('connected');
});

ws.on('close', function close() {
    console.log('disconnected');
});

console.log('server running on port ' + config.get('server.port') + ' succesfully');

rabbitMq.getRabbitMq().then(function () {
}).catch((err) => {
    console.log("Couldn't connect to RabbitMq!");
    process.exit(1);
});

module.exports = app;
