import './library/upgradeable/Upgradeable.sol';

contract MerkleClaims is Upgradeable {
    //TODO : Check if needed
    mapping (bytes32 => bytes32) public rootByHash;

    event NewRootAdded(bytes32 keyHash,bytes32 root);

    function addMerkleRoot(bytes32 keyHash, bytes32 root) public onlyOwner {
        require(rootByHash[keyHash]==0);
        rootByHash[keyHash] = root;
        emit NewRootAdded(keyHash,root);
    }
}
