pragma solidity ^0.4.24;

contract VoteSignatureVerifier {

    uint256 public constant chainId = 1;
    bytes32 public constant salt = 0xf2d857f4a3edcb9b78b4d503bfe733db1e3f6cdc2b7971ee739626c97e86a558;

    string public constant EIP712_DOMAIN  = "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)";
    string public constant VOTE_TYPE = "Vote(string claim_id,string decision,uint256 timestamp,string version,string skill)";
    string public constant DAPP_NAME = "Indorse";
    string public constant VERSION = "1";

    bytes32 public constant EIP712_DOMAIN_TYPEHASH = keccak256(abi.encodePacked(EIP712_DOMAIN));
    bytes32 public constant VOTE_TYPEHASH = keccak256(abi.encodePacked(VOTE_TYPE));

    bytes32 public DOMAIN_SEPARATOR;

    constructor() public {
        DOMAIN_SEPARATOR = keccak256(abi.encode(
            EIP712_DOMAIN_TYPEHASH,
            keccak256(DAPP_NAME),
            keccak256(VERSION),
            chainId,
            this,
            salt
        ));
    }
    
    function verify(string claim_id, string decision, uint256 timestamp,
        string version, string skill, uint8 sigV, bytes32 sigR, bytes32 sigS) public view returns (address) {
        bytes32 voteHash = keccak256(abi.encodePacked(
                "\x19\x01",
                DOMAIN_SEPARATOR,
                keccak256(abi.encode(
                    VOTE_TYPEHASH,
                    keccak256(claim_id),
                    keccak256(decision),
                    timestamp,
                    keccak256(version),
                    keccak256(skill)
                ))
            ));

        return ecrecover(voteHash, sigV, sigR, sigS);
    }
}