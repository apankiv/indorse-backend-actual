//Example schema string "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)"
module.exports.parse = async function parse(schemaStr) {

    let bracerLess = schemaStr.substring(schemaStr.indexOf("(") + 1, schemaStr.length - 1);

    let schema = [];

    bracerLess.split(",").forEach(arg => {
        let split = arg.split(" ");
        schema.push({
            name: split[1],
            type: split[0]
        })
    });

    return schema;
};
