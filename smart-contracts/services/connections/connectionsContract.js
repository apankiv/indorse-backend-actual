let scHelper = require('../scHelper')
const logger = require("../../../models/services/common/logger").getLogger();

module.exports.createVirtualEntity = async function createVirtualEntity() {
    return await scHelper.logAndQueueContractTx("Connections", "createVirtualEntity")
}

module.exports.createVirtualEntityAndConnection = async function createVirtualEntityAndConnection(connectionTo,connectionType,direction) {
    return await scHelper.logAndQueueContractTx("Connections", "createVirtualEntityAndConnection", connectionTo, connectionType, direction)
}

module.exports.addConnection =async function addConnection(entity,connectionTo,connectionType,direction){
    return await scHelper.logAndQueueContractTx("Connections", "addConnection", entity, connectionTo, connectionType, direction);

}

//TODO  : Untested
module.exports.editEntity = async function editEntity(entity,activeFlag, ipfsData){
    return await scHelper.logAndQueueContractTx("Connections", "editEntity", entity, activeFlag, ipfsData);
}

//TODO  : Untested
module.exports.transferOwnerPush = async function transferOwnerPush(entity, newOwner) {
    return await scHelper.logAndQueueContractTx("Connections", "transferOwnerPush", entity, newOwner);
}

//TODO  : Untested
module.exports.transferEntityOwnerPull = async function transferEntityOwnerPull(entity){
    return await scHelper.logAndQueueContractTx("Connections", "transferEntityOwnerPull", entity);
}

//TODO  : Untested
module.exports.editConnection = async function editConnection(entity,connectionTo,connectionType,direction,active,ipfsData,expiration){
    return await scHelper.logAndQueueContractTx("Connections", "editConnection", entity, connectionTo, connectionType, direction, active, ipfsData, expiration);
}

module.exports.removeConnection = async function removeConnection(entity, connectionTo, connectionType){
    return await scHelper.logAndQueueContractTx("Connections", "removeConnection", entity, connectionTo, connectionType);
}

module.exports.getConnection = async function getConnection(entity, connectionTo, connectionType){
    let response = await scHelper.logAndCallContract("Connections", "getConnection", entity, connectionTo, connectionType)
    let result = {
        entityActive : response[0],
        connectionEntityActive : response[1],
        connectionActive : response[2],
        data : response[3],
        direction : response[4],
        expiration : response[5]
    }

    logger.debug("Response details : " + JSON.stringify(result));
    return result;
};

module.exports.getEntity = async function getEntity(entity){
    let response = await scHelper.logAndCallContract("Connections", "getEntity", entity);
    let result = {
        active : response[0],
        transferOwnerTo : response[1],
        data : response[2],
        owner : response[3]
    };
    logger.debug("Response details : " + JSON.stringify(result));
    return result;
};