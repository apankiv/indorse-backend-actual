const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const mongo = require('mongodb');
const logger = require('../../../../models/services/common/logger').getLogger();
const ObjectID = mongo.ObjectID;

async function updateTransactionHash(txDocument,hash){
    let txCallbackData = txDocument.tx_metadata.callback_data;
    logger.log("Metadata received : " + JSON.stringify(txCallbackData));

    if (txCallbackData && txCallbackData.type && txCallbackData.company_id && txCallbackData.user_id){
        
        switch (txCallbackData.type){
            case "COMPANY_ADVISOR":                
                console.log("Setting transaction hash for the advisor : " + txCallbackData.user_id + " : " + hash + " : " + txCallbackData.company_id);
                await mongooseCompanyRepo.setCompanyAdvisorTxHash(txCallbackData.company_id, txCallbackData.user_id, hash);    
                break;
            case "COMPANY_TEAM_MEMBER":
                await mongooseCompanyRepo.teamMember.setCompanyTxHash(txCallbackData.company_id, txCallbackData.user_id, hash);
                break;
        }
    }
}

exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    logger.debug("Executing addTx handler")
    let txStatus = parseInt(txReceipt.status, 16);

    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let connectionAddedEvent = events.connectionAdded;
        if (!connectionAddedEvent) {
            const ERROR_MESSAGE = 'Connection added event was not present';
            await mongooseTransactionRepo.update({ _id: transactionDocument._id }
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }
        await updateTransactionHash(transactionDocument, txReceipt.transactionHash);
        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    }  
};