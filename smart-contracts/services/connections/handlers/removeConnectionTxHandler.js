const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');

exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    let txStatus = parseInt(txReceipt.status, 16);

    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let connectionRemovedEvent = events.connectionRemoved;
        if (!connectionRemovedEvent) {
            const ERROR_MESSAGE = 'connectionRemoved event was not present';
            await mongooseTransactionRepo.update({ _id: transactionDocument._id }
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }
        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);  
    }      
};