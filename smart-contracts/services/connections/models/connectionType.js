module.exports = {

    IS_ADVISOR_OF : {
        name : 'isAdvisorOf',
        value : '0x30ed9383ab64b27cb4b70035e743294fe1a1c83eaf57eca05033b523d1fa4261'
    },

    IS_PARTNERED_WITH : {
        name : 'isPartneredWith',
        value : '0xffe72ffb7d5cc4224f27ea8ad324f4b53b37835a76fc2b627b3d669180b75ecc'
    },

    IS_EMPLOYEE_OF : {
        name : 'isEmployeeOf',
        value : '0xa64b51178a7ee9735fb96d8e7ffdebb455b02beb3b1e17a709b5c1beef797405'
    },

    IS_COLLEGUE_OF : {
        name : 'isColleagueOf',
        value : '0x0079ca0c877589ba53b2e415a660827390d2c2a62123cef473009d003577b7f6'
    },

    IS_TEAM_MEMBER_OF : {
        name: 'isTeamMemberOf',
        value: '0xdbfc541a9ab9bb8db5348b9ed17aa2c5c00886ff40b19f3446c8fd4cf34700ae'
    }
};