const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const timestampService = require('../../../models/services/common/timestampService');
const finishVotingRoundsService = require('../../../models/votes/finishVotingRoundsService');
const slackService = require('../../../models/services/common/slackService');
const votingRound = require('../../../models/services/mongo/mongoose/schemas/votingRound');
const checkClaimsReputationJob = require('./checkClaimsReputationJob');

module.exports = {

    processOutstandingClaims: async function processOutstandingClaims() {
        const now = timestampService.createTimestamp();
        try {
            const finishingRounds = await mongooseVotingRoundRepo.findAll({
                $and: [
                    { end_voting: { $lt: now } },
                    { status: votingRound.statuses.IN_PROGRESS },
                ],
            });

            for (const round of finishingRounds) {
                await finishVotingRoundsService.finishVotingRound(round);
            }

            await checkClaimsReputationJob.checkClaimsReputation();
        } catch (e) {
            slackService.reportCronBug(e);
        }
    },
};
