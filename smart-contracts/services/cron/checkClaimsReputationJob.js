const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const timestampService = require('../../../models/services/common/timestampService');
const getVotingResultService = require('../../../models/votes/getVotingResultService');
const logger = require('../../../models/services/common/logger').getLogger();
const eventUtils = require('../../../models/services/common/eventUtils');
const mongooseClaimEventsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimEvents');
const {states: userAssignmentStates} = require('../../../models/services/mongo/mongoose/schemas/userAssignment');

module.exports = {

    checkClaimsReputation: async function checkClaimsReputation() {

        let now = timestampService.createTimestamp();

        //Check for all claims whose sc_deadline has passed and have not yet been updated for reputation calculation
        try {
            console.log("CRON Generating rewards now : " + now);
            let claims = await mongooseClaimsRepo.findAll({
                submission_event_computed: false,
                sc_deadline: {$lt: now}
            });
            for (let claim of claims) {
                //Find voting round for this claim id
                let result = await getVotingResultService.getVotingResult(claim._id.toString());
                console.log("CRON Result is " + result.indorsed + " For claim " + claim._id.toString());
                let votingRounds = await mongooseVotingRoundRepo.findAll({claim_id: claim._id.toString()});
                for (let votingRound of votingRounds) {
                    console.log("CRON Voting round is " + votingRound._id.toString());
                    let voters = await mongooseVoteRepo.findAll({
                        $and: [{voting_round_id: votingRound._id.toString()},
                            {claim_id: votingRound.claim_id},
                            {voted_at: {$exists: true}}
                        ]
                    });

                    for (let voter of voters) {
                        //Find what he voted
                        //Add claimEvent for viewed
                        let claimEvent = {
                            user_id: voter.voter_id,
                            claim_id: voter.claim_id,
                            voting_round_id: voter.voting_round_id,
                            timestamp: now
                        };

                        if ((voter.endorsed && result.indorsed) || (!voter.endorsed && !result.indorsed)) {
                            claimEvent.event = 'vote_in_consensus'
                        } else {
                            claimEvent.event = 'vote_not_in_consensus'
                        }

                        if (!await eventUtils.isEventRecorded(claimEvent)) {
                            await mongooseClaimEventsRepo.insert(claimEvent)
                        }
                    }
                }

                await mongooseClaimsRepo.update({_id: claim._id.toString()}, {$set: {submission_event_computed: true}});
            }

        } catch (error) {
            logger.info("Unable to calculate reputation : reason : " + error.message + error.stack)
        }
    }
};