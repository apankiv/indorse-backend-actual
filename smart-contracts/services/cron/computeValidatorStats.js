const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const timestampService = require('../../../models/services/common/timestampService');
const slackService = require('../../../models/services/common/slackService');
const ObjectId = require('mongodb').ObjectID;
const getVotingResultService = require('../../../models/votes/getVotingResultService');

const SECONDS_IN_DAY = 86400;

module.exports = {

    calculate: async function calculate() {
        try {
            let validatorCursor = await mongooseValidatorRepo.findAllWithCursor();

            for (let validator = await validatorCursor.next(); validator != null; validator = await validatorCursor.next()) {

                validator = validator._doc;

                let now = await timestampService.createTimestamp();

                //TOTALS
                let summary = await calculateVaildatorStats(validator);

                summary.global_stats = await calculateVaildatorStatsForSkills(validator, 0, now);
                summary.daily_stats = await calculateVaildatorStatsForSkills(validator, now - SECONDS_IN_DAY, now);
                summary.weekly_stats = await calculateVaildatorStatsForSkills(validator, now - 7 * SECONDS_IN_DAY, now);
                summary.monthly_stats = await calculateVaildatorStatsForSkills(validator, now - 31 * SECONDS_IN_DAY, now);
                summary.yearly_stats = await calculateVaildatorStatsForSkills(validator, now - 365 * SECONDS_IN_DAY, now);

                await mongooseValidatorRepo.update({_id: validator._id}, summary);
            }
        } catch (e) {
            slackService.reportCronBug(e);
        }
    }
};

async function calculateVaildatorStats(validator) {

    let votes = await mongooseVoteRepo.findAll({
        voter_id: validator.user_id,
        voted_at: {$gt: 0},
        sc_vote_exists: true
    });

    let invited = await mongooseVoteRepo.count({
        voter_id: validator.user_id,
    });

    for (let vote of votes) {
        vote.claim = await mongooseClaimsRepo.findOneById(ObjectId(vote.claim_id));
    }

    let vote_participation = 0;
    let indorsed = 0;
    let flagged = 0;
    let rewards = 0;
    let consensus = 0;
    let is_consensus_claim = 0;

    for (let i = 0; i < votes.length; i++) {
        let vote = votes[i];
        if (vote.endorsed) {
            indorsed++;
        } else {
            flagged++;
        }

        if (vote.reward) {
            rewards = rewards + vote.reward;
        }

        vote_participation++;

        let claimResult;
        if (typeof vote.claim.in_consensus === "undefined") {
            claimResult = await getVotingResultService.getVotingResult(vote.claim_id.toString());
            let isConsensus = claimResult.consensus;
            await mongooseClaimsRepo.update({_id: ObjectId(vote.claim_id)}, {in_consensus: isConsensus});
            vote.claim.in_consensus = isConsensus;
        }

        if (vote.claim.in_consensus === true) {
            if (vote.endorsed === true && vote.claim.final_status === true) {
                consensus++;
            }
            if (vote.endorsed === false && vote.claim.final_status === false) {
                consensus++;
            }
            is_consensus_claim++;
        }
    }


    /*
        No.vote on consensus claim = number of user votes on claim that is finalized and has result (either indorse / flag)
        Consensus percentage = Number of user votes on conensus claim and vote is equal to claim's final result / No.vote on consensus claim
     */
    let consensus_percentage;
    if (is_consensus_claim === 0) {
        consensus_percentage = 0;
    } else {
        consensus_percentage = Math.round((consensus / is_consensus_claim) * 100);
    }

    return {
        vote_participation: vote_participation,
        indorsed: indorsed,
        flagged: flagged,
        rewards: rewards,
        consensus: consensus,
        consensus_percentage: consensus_percentage,
        invited : invited
    }
}

async function calculateVaildatorStatsForSkills(validator, startTimestamp, endTimestamp) {

    let votingRoundIds = await mongooseVotingRoundRepo.findAllFields({
        end_registration: {$gte: startTimestamp, $lte: endTimestamp}
    }, "_id");

    votingRoundIds = votingRoundIds.map(v => v._id.toString());

    let votes = await mongooseVoteRepo.findAll({
        voter_id: validator.user_id,
        voting_round_id: {$in: votingRoundIds}
    });

    let skills = {};

    for (let vote of votes) {
        vote.claim = await mongooseClaimsRepo.findOneById(ObjectId(vote.claim_id));

        let skillName;

        if (vote.skillId) {
            let skill = mongooseSkillRepo.findOneById(vote.skillId);
            skillName = skill.name;
        } else {
            skillName = vote.claim.title.toLowerCase();
        }

        if (!skills[skillName]) {
            skills[skillName] = {
                invites: [vote],
                votes: []
            };

            if (vote.sc_vote_exists === true) {
                skills[skillName].votes.push(vote);
            }

        } else {
            if (vote.sc_vote_exists === true) {
                skills[skillName].votes.push(vote);
            }
            skills[skillName].invites.push(vote);
        }
    }

    let results = {};
    let promises = [];

    Object.keys(skills).forEach(function (skillName) {
        promises.push((async () => {

            let vote_participation = 0;
            let indorsed = 0;
            let flagged = 0;
            let rewards = 0;
            let consensus = 0;
            let is_consensus_claim = 0;

            var votesToSumUp = skills[skillName].votes;

            for (let i = 0; i < votesToSumUp.length; i++) {
                let vote = votesToSumUp[i];
                if (vote.endorsed) {
                    indorsed++;
                } else {
                    flagged++;
                }

                if (vote.reward) {
                    rewards = rewards + vote.reward;
                }

                vote_participation++;

                let claimResult;
                if (typeof vote.claim.in_consensus === "undefined") {
                    claimResult = await
                        getVotingResultService.getVotingResult(vote.claim_id.toString());
                    let isConsensus = claimResult.consensus;
                    await
                        mongooseClaimsRepo.update({_id: ObjectId(vote.claim_id)}, {in_consensus: isConsensus});
                    vote.claim.in_consensus = isConsensus;
                }

                if (vote.claim.in_consensus === true) {
                    if (vote.endorsed === true && vote.claim.final_status === true) {
                        consensus++;
                    }
                    if (vote.endorsed === false && vote.claim.final_status === false) {
                        consensus++;
                    }
                    is_consensus_claim++;
                }
            }

            /*
                No.vote on consensus claim = number of user votes on claim that is finalized and has result (either indorse / flag)
                Consensus percentage = Number of user votes on conensus claim and vote is equal to claim's final result / No.vote on consensus claim
             */
            let consensus_percentage;

            if (is_consensus_claim === 0) {
                consensus_percentage = 0;
            } else {
                consensus_percentage = Math.round((consensus / is_consensus_claim) * 100);
            }

            results[skillName] = {
                vote_participation: vote_participation,
                indorsed: indorsed,
                flagged: flagged,
                rewards: rewards,
                consensus: consensus,
                consensus_percentage: consensus_percentage,
                invited: skills[skillName].invites.length
            }
        })());
    });

    await Promise.all(promises);

    return results;
}