const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const timestampService = require('../../../models/services/common/timestampService');
const slackService = require('../../../models/services/common/slackService');
const googleSheetsService = require('../../../models/services/common/googleSheetsService');

const SECONDS_IN_24_HOURS = 86400;

module.exports = {

    reportStats: async function reportStats(reportType) {
        try {
            let now = timestampService.createTimestamp();


            let report = await createClaimsReport(mongooseClaimsRepo, reportType);
            report.indorsePecentage = report.indorsed/report.approved

            if(reportType !== 'TEST'){
                slackService.reportClaimStats(report);
            }
            await googleSheetsService.writeDailyReport(report, reportType);
            // TODO: Save report for historical purpose
            return report;
        } catch(error) {
            console.log("reportStats error:",error);
            slackService.reportCronBug(error);
        }
    }
};



// Function to create a claims report for a given period
const createClaimsReport = (db, period) => new Promise(async (resolve, reject) => {
    const endDateTimeUnix = new Date().getTime() / 1000;
    let startDateTimeUnix = new Date().getTime() / 1000;
    // Set start time depending on period / end time defaults to passed command line argument
    if (period === 'D') {
        // One day period
        startDateTimeUnix = endDateTimeUnix - 86400;
    } else if (period === 'W') {
        // One week period
        startDateTimeUnix = endDateTimeUnix - (86400 * 7);
    } else if (period === 'M') {
        // One month period
        startDateTimeUnix = endDateTimeUnix - (86400 * 30);
    } else if (period === '90D') {
        // Three month period
        startDateTimeUnix = endDateTimeUnix - (86400 * 30 * 3);
    }

    // Calculate the time before which claims should have finished their voting periods
    const votingCutOff = Math.round((new Date().getTime() - 172800000) / 1000);

    const startDateTime = String(new Date(startDateTimeUnix * 1000).toLocaleString('en-GB', { timeZone: 'Asia/Singapore' }))+ " (SGT)";
    const endDateTime = String(new Date(endDateTimeUnix * 1000).toLocaleString('en-GB', { timeZone: 'Asia/Singapore' }))+ " (SGT)";

    // Start creating a new record to store reporting data for period
    const newRecord = {
        duration: period,
        startDateTimeUnix,
        endDateTimeUnix,
        startDateTime,
        endDateTime,
    };

    let baseQueryParams;

    // Set up the query parameters for getting the different data sets of claims - should be working after 12-Sep-2018
    if ((period === 'M' || period === '90D') && Math.round(new Date().getTime()) < 1536681600000) {
        // Temporary Hack for Monthly figures as we don't yet have a full months worth of created_at timestamps
        baseQueryParams = { $or: [{ $and: [{ created_at: { $gt: startDateTimeUnix } }, { created_at: { $lt: endDateTimeUnix } }] }, { $and: [{ approved: { $gt: startDateTimeUnix } }, { approved: { $lt: endDateTimeUnix } }] }, { $and: [{ disapproved: { $gt: startDateTimeUnix } }, { disapproved: { $lt: endDateTimeUnix } }] }] };
    } else {
        baseQueryParams = { $and: [{ created_at: { $gt: startDateTimeUnix } }, { created_at: { $lt: endDateTimeUnix } }] };
    }

    // Obtain the list of Claim IDs first - we will use these in all subsequent queries
    //const allClaimIDs = await findDocsClaimIDs(db, 'claims', baseQueryParams);

    const allClaimIDs = await mongooseClaimsRepo.findAll(baseQueryParams);
    newRecord.claimIDs = allClaimIDs.length > 0 ? allClaimIDs : [];
    let allClaimIDArray = allClaimIDs.map(claims => claims._id);
    let allClaimIDStringArray = allClaimIDs.map(claims => String(claims._id) );
    const approvedQueryParams = { $and: [{ _id: { $in: allClaimIDArray } }, { approved: { $exists: true } }] };
    const disapprovedQueryParams = { $and: [{ _id: { $in: allClaimIDArray} }, { disapproved: { $exists: true } }] };
    const indorsedQueryParams = { $and: [{ _id: { $in: allClaimIDArray} }, { final_status: true }] };
    const flaggedQueryParams = { $and: [{ _id: { $in: allClaimIDArray } }, { final_status: false }] };
    const inProgressQueryParams = { $and: [{ _id: { $in: allClaimIDArray } }, { approved: { $exists: true } }, { approved: { $gt: votingCutOff } }] };
    const noConsensusQueryParams = { $and: [{ _id: { $in: allClaimIDArray } }, { approved: { $exists: true } }, { approved: { $lt: votingCutOff } }, { final_status: { $exists: false } }] };

    try {
        const allClaims = await mongooseClaimsRepo.findAll(baseQueryParams);
        newRecord.submitted = allClaims ? allClaims.length : 0;

        const approvedClaims = await mongooseClaimsRepo.findAll(approvedQueryParams);
        newRecord.approved = approvedClaims ? approvedClaims.length : 0;

        const disapprovedClaims = await mongooseClaimsRepo.findAll(disapprovedQueryParams);
        newRecord.disapproved = disapprovedClaims ? disapprovedClaims.length : 0;

        newRecord.pending = newRecord.submitted - (newRecord.approved + newRecord.disapproved);

        const indorsedClaims = await mongooseClaimsRepo.findAll(indorsedQueryParams);
        newRecord.indorsed = indorsedClaims ? indorsedClaims.length : 0;

        const flaggedClaims = await mongooseClaimsRepo.findAll(flaggedQueryParams);
        newRecord.flagged = flaggedClaims ? flaggedClaims.length : 0;

        const inProgressClaims = await mongooseClaimsRepo.findAll(inProgressQueryParams);
        newRecord.inProgress = inProgressClaims ? inProgressClaims.length : 0;

        const noConsensusClaims = await mongooseClaimsRepo.findAll(noConsensusQueryParams);
        newRecord.noConsensus = noConsensusClaims ? noConsensusClaims.length : 0;

        // Set up the query parameters for getting the different data sets of votes
        const votesSentQueryParams = { claim_id: { $in: allClaimIDStringArray } };
        const votesDatabaseQueryParams = { $and: [{ voted_at: { $exists: true }}, { claim_id: { $in: allClaimIDStringArray } }] };
        const votesBlockchainQueryParams = { $and: [{ sc_vote_exists: true }, { claim_id: { $in: allClaimIDStringArray } }] };

        const votesSent = await mongooseVoteRepo.findAll(votesSentQueryParams);
        newRecord.votesSent = votesSent ? votesSent.length : 0;

        // votes DB ->
        const votesDatabase = await mongooseVoteRepo.findAll(votesDatabaseQueryParams);
        newRecord.votesDatabase = votesDatabase ? votesDatabase.length : 0;

        const votesBlockchain = await mongooseVoteRepo.findAll(votesBlockchainQueryParams);
        newRecord.votesBlockchain = votesBlockchain ? votesBlockchain.length : 0;

        newRecord.avgVotesPerClaim = newRecord.approved > 0 ? (newRecord.votesBlockchain / newRecord.approved).toFixed(4) : 0;
        newRecord.createdAt = new Date();
        let signups = await mongooseUserRepo
            .findAll({timestamp : {$gt : startDateTimeUnix}});
        newRecord.signups = signups.length;

        resolve(newRecord);
    } catch (err) {
        console.log(`An error occurred: ${JSON.stringify(err)}`);
        reject(err);
    }
});

