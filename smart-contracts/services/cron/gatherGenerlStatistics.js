const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const timestampService = require('../../../models/services/common/timestampService');
const slackService = require('../../../models/services/common/slackService');
const googleSheetsService = require('../../../models/services/common/googleSheetsService');
const SECONDS_IN_24_HOURS = 86400;

module.exports = {

    reportStats: async function reportStats() {
        try {
            let now = timestampService.createTimestamp();
            let dayAgoTimestamp = now - SECONDS_IN_24_HOURS;
            let report = {};
            let validatorsNotified = await mongooseVoteRepo
                .findAll({notified_at : {$gt : dayAgoTimestamp}});
            let validatorsVoted = await mongooseVoteRepo
                .findAll({voted_at : {$gt : dayAgoTimestamp}});
            let txSubmitted = await mongooseVoteRepo
                .findAll({tx_timestamp : {$gt : dayAgoTimestamp}});
            let txSuccessful = await mongooseVoteRepo
                .findAll({tx_success_timestamp : {$gt : dayAgoTimestamp}});
            let claimsCreated = await mongooseClaimsRepo
                .findAll({created_at : {$gt : dayAgoTimestamp}});
            let signups = await mongooseUserRepo
                .findAll({timestamp : {$gt : dayAgoTimestamp}});
            let claimDraftsCreated = await mongooseClaimDraftsRepo
                .findAll({created_at : {$gt : dayAgoTimestamp}});
            let claimDraftsFinalized = await mongooseClaimDraftsRepo
                .findAll({finalized_at : {$gt : dayAgoTimestamp}});
            let claimsApproved = await mongooseClaimsRepo
                .findAll({approved : {$gt : dayAgoTimestamp}});
            let claimsDisapproved = await mongooseClaimsRepo
                .findAll({disapproved : {$gt : dayAgoTimestamp}});

            report.validatorsNotified = validatorsNotified.length;
            report.validatorsVoted = validatorsVoted.length;
            report.txSubmitted = txSubmitted.length;
            report.txSuccessful = txSuccessful.length;
            report.claimsCreated = claimsCreated.length;
            report.signups = signups.length;
            report.claimDraftsCreated = claimDraftsCreated.length;
            report.claimDraftsFinalized = claimDraftsFinalized.length;
            report.claimsApproved = claimsApproved.length;
            report.claimsDisapproved = claimsDisapproved.length;

            slackService.reportStats(report);
            return report;
        } catch(error) {
            console.log("reportStats error:",error);
            slackService.reportCronBug(error);
        }
    }
};