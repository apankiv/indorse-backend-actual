const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const settings = require('../../../models/settings');
const scHelper = require('../scHelper');
const handlerRegistry = require('../handlerRegistry');
const logger = require('../../../models/services/common/logger').getLogger();
const lambdaSigner = require('../lambdaSignerClient');
const errorUtils = require("../../../models/services/error/errorUtils")
const web3Initializer = require('../initializers/web3')
const slackService = require('../../../models/services/common/slackService');

async function signTransaction(rawTranasction, contractName) {
    logger.debug("Contract name is " + contractName);
    switch (contractName) {
        case "Connections":
            return await lambdaSigner.connectionsLambda(rawTranasction);
            break;
        case "IndorseToken":
            return await lambdaSigner.indorseTokenLambda(rawTranasction);
            break;
        case "NaiveClaims":
        case "MerkleClaims":        
            return await lambdaSigner.claimsLambda(rawTranasction);
            break;
        default:
            errorUtils.throwError('Unable to sign transaction for ' + contractName, 500);
    }
}

exports.processTransactions = async function processTransactions() {
    try {
        logger.debug("Starting transaction processing...")
        let web3 = await web3Initializer.getWeb3();
        let hash;
        const cursor = await mongooseTransactionRepo.findAllWithStatus('PENDING');
        for (let doc = await cursor.next(); doc != null; doc = await cursor.next()) {
            let txDoc = doc._doc;
            logger.debug("Processing transaction " + JSON.stringify(txDoc));
            hash = txDoc.tx_hash;
            let receipt = await web3.eth.getTransactionReceipt(txDoc.tx_hash);
            logger.debug("Receipt is : " + JSON.stringify(receipt));
            if (receipt) {                
                await handlerRegistry[doc.tx_metadata.contract][doc.tx_metadata.function_name].handleTx(receipt, txDoc);
            } else {
                let result = await mongooseEthNwRepo.findOneByNetwork(settings.ETH_NETWORK);
                let updateObject = {};
                logger.debug('Count ' + result.max_inspection_count + ' ' + txDoc.inspected_count);
                if (Number(txDoc.inspected_count) >= Number(result.max_inspection_count)) {
                    updateObject['status'] = 'SUSPENDED';
                } else {
                    updateObject['inspected_count'] = Number(txDoc.inspected_count) + 1;
                }
                await mongooseTransactionRepo.update({_id: txDoc._id}
                    , {
                        $set: updateObject
                    });
            }
        }
    } catch (e) {
        slackService.reportCronBug(e);
    }
};


exports.sendTransactions = async function sendTransactions() {
    try {
        logger.debug("Starting transaction sending...")
        let pendingTxCount = await
            mongooseTransactionRepo.countAllWithStatus('PENDING'); //TODO : group by contract type
        if (pendingTxCount > 0) {
            logger.error("Pending count is " + pendingTxCount + " , Not queuing more transactions");
            return;
        }


        //select first one to send
        let txToSend = await
            mongooseTransactionQueueRepo.findOne({$query: {"status": "QUEUED"}, $orderby: {_id: -1}}); //TODO : Curosr loop by contract type
        if (!txToSend)
            return;
        console.log('Trying to send tx ' + JSON.stringify(txToSend));

        let MEDIAN_GAS_MULTIPLIER = 2;
        let web3 = await
            web3Initializer.getWeb3();
        let nonceHex = web3.toHex(web3.eth.getTransactionCount(txToSend.tx_raw.lambda_signer)); //TODO : Handle synchro
        let gasLimit = txToSend.gasLimit;
        let ethNw = await
            mongooseEthNwRepo.findOneByNetwork(settings.ETH_NETWORK);
        let gasPrice = web3.toWei(ethNw.max_gas_price, 'gwei');
        let medianGWEIGasPrice = web3.fromWei(web3.eth.gasPrice, 'gwei') * MEDIAN_GAS_MULTIPLIER;
        logger.debug("Nonce used :" + nonceHex);
        logger.debug("Network median gas price in GWEI :" + medianGWEIGasPrice);
        logger.debug("Multiplier used :" + MEDIAN_GAS_MULTIPLIER);
        logger.debug("Max default gas price in GWEI :" + ethNw.max_gas_price);

        if (ethNw.max_gas_price === undefined) {
            logger.error("Max gas price is undefined for the network " + settings.ETH_NETWORK);
            errorUtils.throwError("Max gas price is undefined for network " + settings.ETH_NETWORK, 500);
        }

        if (Number(medianGWEIGasPrice) < Number(ethNw.max_gas_price)) {
            gasPrice = web3.eth.gasPrice * MEDIAN_GAS_MULTIPLIER;
            logger.info("Setting gas price to the network median");
        }
        let gasPriceHex = web3.toHex(gasPrice);

        let rawTx = {
            nonce: nonceHex,
            gasPrice: gasPriceHex,
            gasLimit: txToSend.tx_raw.gasLimit,
            to: txToSend.tx_raw.to,
            lambda_signer: txToSend.tx_raw.lambda_signer,
            value: 0x0,
            data: txToSend.tx_raw.data
        }
        logger.debug("Raw transaction dump : " + JSON.stringify(rawTx));

        let hex_serialized = await
            signTransaction(rawTx, txToSend.tx_metadata.contract);
        let result = {}
        let hash = await
            web3.eth.sendRawTransaction(hex_serialized);
        result["txHash"] = hash;
        result["rawTx"] = rawTx;
        logger.info("Sending transaction hash  = " + hash);


        let mongoResult = await
            mongooseTransactionRepo.insert({
                tx_hash: result.txHash,
                tx_raw: result.rawTx,
                initiating_user_id: txToSend.initiating_user_id,
                tx_metadata: txToSend.tx_metadata,
                status: 'PENDING',
                inspected_count: 0
            });

        let updateObj = {
            status: 'SUBMITTED',
            submission_id: mongoResult
        }

        await
            mongooseTransactionQueueRepo.update({_id: txToSend._id}
                , {
                    $set: updateObj
                });

        return result.txHash;
    } catch (e) {
        slackService.reportCronBug(e);
    }
};
