const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseValidatorRankRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRanksRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const slackService = require('../../../models/services/common/slackService');

module.exports = {

    calculateAndSort: async function calculate() {
        try {
            let validatorCursor = await mongooseValidatorRepo.findAllWithCursor();
            let curTimestamp = (new Date().getTime() / 1000);
            let oneWeekAgoTimestamp = curTimestamp - 604800;

            let validatorArray = [];
            for (let validator = await validatorCursor.next(); validator != null; validator = await validatorCursor.next()) {
                let votes = await mongooseVoteRepo.findAll({voter_id: validator.user_id, voted_at: {$gt: oneWeekAgoTimestamp},
                    sc_vote_exists : true,}); //TODO: Find out time complexity of findAll
                let totalScore = votes.reduce((accumulator, vote) => {
                    if (vote.reward) {
                        accumulator.reward += vote.reward;
                    }
                    return accumulator;
                }, {
                    reward: 0
                });
                validatorArray.push({user_id: validator.user_id, scoreThisWeek: totalScore.reward});
            }
            validatorArray.sort((a,b) => b.scoreThisWeek - a.scoreThisWeek);
            let validatorRankDocs = {rewardOneWeekRank: validatorArray, timestamp: curTimestamp};
            await mongooseValidatorRankRepo.insert(validatorRankDocs);
            // Then assign badges to top 3
            await assignThreeBadges(validatorArray);

        } catch (e) {
            slackService.reportCronBug(e);
        }
    }
};

async function assignThreeBadges(validatorArray){
    let badges = [];
    badges.push('weeklyTop1ValidatorByReward');
    badges.push('weeklyTop2ValidatorByReward');
    badges.push('weeklyTop3ValidatorByReward');

    for(var i = 0 ; i < 3 ; i ++){
        var curUserId = validatorArray[i].user_id;
        await mongooseUserRepo.update({_id: curUserId}, {$addToSet: {badges: badges[i]}}); // no duplicates 
    }
}