const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const slackService = require('../../../models/services/common/slackService');
const emailService = require('../../../models/services/common/emailService');
const ObjectId = require('mongodb').ObjectID;
const getVotingResultService = require('../../../models/votes/getVotingResultService');

module.exports = {
    updateValidatorDownvoteScore: async function updateValidatorDownvoteScore() {
        try {

            const allValidators = await mongooseValidatorRepo.findAll({});
            console.log('running downvote')
            for (let i = 0; i < allValidators.length; i++) {
                let validatorUserId = allValidators[i].user_id;

                let criteria = {
                    $and: [
                        {voter_id: validatorUserId},
                        {$or: [{'downvote.count': {$gt: 0}}, {'hide.isHidden': true}]}
                    ]
                }


                const allDownvotedVotesForValidator = await mongooseVoteRepo.findAll(criteria);
                let downvoteCommentCounter = 0;
                let commentArr = [];
                for (let j = 0; j < allDownvotedVotesForValidator.length; j++) {
                    let voteWithDownvote = allDownvotedVotesForValidator[j];
                    let isDownvoted = voteWithDownvote.upvote.count - voteWithDownvote.downvote.count;
                    let isHidden = voteWithDownvote.hide.isHidden;
                    if (isDownvoted < 0 || isHidden) {
                        downvoteCommentCounter = downvoteCommentCounter + 1;
                        let commentObj = {};
                        commentObj.claimId = voteWithDownvote.claim_id;
                        let feedback = voteWithDownvote.feedback;
                        if(feedback){
                            commentObj.comment = feedback.explanation;
                        }else{
                            commentObj.comment = '';
                        }
                        commentObj.votedAt = voteWithDownvote.voted_at;

                        commentArr.push(commentObj);
                    }
                }

                let currentDownvoteScore = allValidators[i].totalDownvote;
                await mongooseValidatorRepo.update({_id: allValidators[i]._id}, {totalDownvote: downvoteCommentCounter});
                if ((currentDownvoteScore !== downvoteCommentCounter) && downvoteCommentCounter >= 3) {
                    let warnedValidator = await mongooseUserRepo.findOneById(validatorUserId);
                    await emailService.sendAdminValidatorWarning(warnedValidator.username, downvoteCommentCounter, warnedValidator.email, commentArr);
                }
            }
        } catch (e) {
            slackService.reportCronBug(e);
        }
    }

};