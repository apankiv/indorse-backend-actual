const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const timestampService = require('../../../models/services/common/timestampService');
const claimEmailService = require('../../../models/services/common/claimsEmailService');
const slackService = require('../../../models/services/common/slackService');

module.exports = {

    sendReminderEmails: async function sendReminderEmails() {
        try {
            // eslint-disable-next-line
            console.log('SENDING REMINDER EMAILS');
            const now = timestampService.createTimestamp();

            const ongoingRounds = await mongooseVotingRoundRepo.findAll({
                $and: [
                    { voters_notified: true },
                    { status: 'in_progress' },
                ],
            });

            for (let roundIndex = 0; roundIndex < ongoingRounds.length; roundIndex += 1) {
                const round = ongoingRounds[roundIndex];
                // eslint-disable-next-line
                const claim = await mongooseClaimsRepo.findOneById(round.claim_id);
                // eslint-disable-next-line
                const votesSubmittedForClaim = await mongooseVoteRepo.findAll({ $and: [{ claim_id: round.claim_id }, { voted_at: { $gt: 0 } }] });

                if (votesSubmittedForClaim < claim.aipLimit) {
                    const totalVotingTime = round.end_voting - round.end_registration;
                    const timeRemaining = round.end_voting - now;
                    if (timeRemaining < totalVotingTime / 4) {
                        if (!round.notification_75_sent) {
                            // eslint-disable-next-line
                            await remindVotingRound(round, '75%', round.end_voting);
                            // eslint-disable-next-line
                            await mongooseVotingRoundRepo.update({ _id: round._id }, { $set: { notification_75_sent: true } });
                        }
                    } else if (timeRemaining < totalVotingTime / 2) {
                        if (!round.notification_50_sent) {
                            // eslint-disable-next-line
                            await remindVotingRound(round, '50%', round.end_voting);
                            // eslint-disable-next-line
                            await mongooseVotingRoundRepo.update({ _id: round._id }, { $set: { notification_50_sent: true } });
                        }
                    } else if ((4 * timeRemaining) < (3 * totalVotingTime)) {
                        if (!round.notification_25_sent) {
                            // eslint-disable-next-line
                            await remindVotingRound(round, '25%', round.end_voting);
                            // eslint-disable-next-line
                            await mongooseVotingRoundRepo.update({ _id: round._id }, { $set: { notification_25_sent: true } });
                        }
                    }
                }
            }
        } catch (err) {
            // eslint-disable-next-line
            console.log(`ERROR DURING SENDING REMINDER EMAILS ${err}`);
            slackService.reportCronBug(err);
        }
    },
};

async function remindVotingRound(round, percentage, deadline) {
    const votes = await mongooseVoteRepo.findByVotingRoundId(round._id.toString());
    // eslint-disable-next-line
    for (const vote of votes) {
        if (!vote.voted_at) {
            // eslint-disable-next-line
            const userToRemind = await mongooseUserRepo.findOneById(vote.voter_id);
            claimEmailService.sendVoteReminder(vote.claim_id, userToRemind.email, percentage, new Date(deadline * 1000));
        }
    }
}
