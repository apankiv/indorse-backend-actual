module.exports = {
    Connections: {
        createVirtualEntity: require('./connections/handlers/createVirtualEntityTxHandler'),
        createVirtualEntityAndConnection: require('./connections/handlers/createVirtualEntityAndConnectionTxHandler'),
        removeConnection: require('./connections/handlers/removeConnectionTxHandler'),
        addConnection: require('./connections/handlers/addConnectionTxHandler')
    },
    IndorseToken: {
        transfer: require('./indorseToken/handlers/transferTxHandler')
    },
    MerkleClaims: {
        addMerkleRoot: require('./merkleClaims/handlers/addMerkleRootTxHandler')
    }    
};
