const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseClaimsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const logger = require('../../../../models/services/common/logger').getLogger();

async function updateTransactionHash(txDocument, hash){
    let txCallbackData = txDocument.tx_metadata.callback_data;
    logger.debug("Metadata received : " + JSON.stringify(txCallbackData));

    let claim_id = txCallbackData.claim_id;

    await mongooseClaimsRepo.update({_id : claim_id}, {rewarded_at: timestampService.createTimestamp()});

    return;
}


exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    logger.debug("Executing transferTx handler")
    let txStatus = parseInt(txReceipt.status, 16);

    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let transferEvent = events.transfer;
        if (!transferEvent) {
            const ERROR_MESSAGE = 'Transfer event was not present';
            await mongooseTransactionRepo.update({ _id: transactionDocument._id }
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }
        await updateTransactionHash(transactionDocument, txReceipt.transactionHash);
        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    } else {
        throw new Error("Unknow transaction status encountered");
    }
};