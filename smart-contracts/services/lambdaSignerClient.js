const EthereumTx = require('ethereumjs-tx');
const settings = require('../../models/settings');
const web3initializer = require('./initializers/web3');
const logger = require('../../models/services/common/logger').getLogger();
const AWS = require('aws-sdk');
const emailService = require('../../models/services/common/emailService');
const timestampService = require('../../models/services/common/timestampService');
const indorseTokenContract = require('../../smart-contracts/services/indorseToken/indorseTokenContract');
const sigUtil = require('eth-sig-util');

const lambda = new AWS.Lambda({region: settings.AWS_LAMBDA_REGION});

exports.connectionsLambda = async function connectionsLambda(txParams) {
    if (settings.LAMBDA_ENABLED) {
        let lambdaAddress = txParams.lambda_signer;
        etherBalanceCheck(lambdaAddress);
        return await signer(txParams, settings.CONNECTIONS_SIGNER_LAMBDA_NAME);
    } else {
        return await mockSigner(txParams, settings.CONNECTIONS_MOCK_PK)
    }
};

exports.indorseTokenLambda = async function indorseTokenLambda(txParams) {
    if (settings.LAMBDA_ENABLED) {
        let lambdaAddress = txParams.lambda_signer;
        etherBalanceCheck(lambdaAddress);
        indBalanceCheck(lambdaAddress);
        return await signer(txParams, settings.INDORSETOKEN_SIGNER_LAMBDA_NAME);
    } else {
        return await mockSigner(txParams, settings.INDORSETOKEN_MOCK_PK)
    }
};

exports.claimsLambda = async function claimsLambda(txParams) {
    if (settings.LAMBDA_ENABLED) {
        let lambdaAddress = txParams.lambda_signer;
        etherBalanceCheck(lambdaAddress);
        return await signer(txParams, settings.CLAIMS_SIGNER_LAMBDA_NAME);
    } else {
        return await mockSigner(txParams, settings.CLAIMS_MOCK_PK)
    }
};

exports.claimPayloadLambda = async function claimPayloadLambda(data) {
    if (settings.LAMBDA_ENABLED) {
        return await payloadSigner(data, settings.CLAIMS_PAYLOAD_SIGNER_LAMBDA_NAME);
    } else {
        return await mockPayloadSigner(data, settings.CLAIMS_MOCK_PK);
    }
};

async function signer(txParams, lambdaName) {
    const params = {
        // ClientContext: "MyApp",
        FunctionName: lambdaName,
        InvocationType: "RequestResponse",
        LogType: "Tail",
        Payload: JSON.stringify(txParams)
    };
    let response = await new Promise((resolve, reject) => lambda.invoke(params, function (err, data) {
        if (err) reject(err);
        else resolve(data);
    }));

    let result = response.Payload.replace(/"/g, "");

    return result;
}

async function mockSigner(txParams, mockPK) {
    const tx = new EthereumTx(txParams);
    tx.sign(Buffer.from(mockPK, 'hex'));
    return '0x' + tx.serialize().toString('hex');
}

async function payloadSigner(data, lambdaName) {
    const params = {
        // ClientContext: "MyApp",
        FunctionName: lambdaName,
        InvocationType: "RequestResponse",
        LogType: "Tail",
        Payload: JSON.stringify(data)
    };
    let response = await new Promise((resolve, reject) => lambda.invoke(params, function (err, data) {
        if (err) reject(err);
        else resolve(data);
    }));

    return JSON.parse(response.Payload);
}

async function mockPayloadSigner(data, mockPK) {
    const sig = sigUtil.signTypedData(Buffer.from(mockPK, 'hex'), {data: data});

    const signature = sig.substring(2);
    const r = "0x" + signature.substring(0, 64);
    const s = "0x" + signature.substring(64, 128);
    const v = parseInt(signature.substring(128, 130), 16);

    return {
        r: r, s: s, v: v
    }
}

let lastEtherBalanceEmailSent = {};

async function etherBalanceCheck(address) {
    let timestamp = timestampService.createTimestamp();
    let lastSent = lastEtherBalanceEmailSent[address] ? lastEtherBalanceEmailSent[address] : 0;
    if ((timestamp - lastSent) > settings.BALANCE_CHECK_EMAIL_INTERVAL) {
        let web3 = await web3initializer.getWeb3();
        let balance = web3.fromWei(web3.eth.getBalance(address), 'ether').toNumber();
        if (balance < settings.ETHER_BALANCE_CHECK) {
            logger.debug('The lambda signer at adddress ', address, ' is low on Ether, please top up ASAP');
            emailService.sendEtherCheckEmail(address, settings.ENVIRONMENT);
        }
        lastEtherBalanceEmailSent[address] = timestamp;
    }
}

let lastINDBalanceEmailSent = {};

async function indBalanceCheck(address) {
    let timestamp = timestampService.createTimestamp();
    let lastSent = lastINDBalanceEmailSent[address] ? lastINDBalanceEmailSent[address] : 0;
    if ((timestamp - lastSent) > settings.BALANCE_CHECK_EMAIL_INTERVAL) {
        let balanceBigNumber = await indorseTokenContract.balanceOf(address);
        let balance = balanceBigNumber.toNumber();

        if (balance < settings.IND_BALANCE_CHECK) {
            logger.debug('The lambda signer at adddress ', address, ' is low on IND, please top up ASAP');
            emailService.sendINDCheckEmail(address, settings.ENVIRONMENT);
        }
        lastINDBalanceEmailSent[address] = timestamp;
    }
}
