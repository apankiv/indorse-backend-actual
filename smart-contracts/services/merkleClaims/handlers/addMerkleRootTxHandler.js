const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseClaimsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseClaimSignaturesRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimSignaturesRepo');
const mongooseVoteSignaturesRepo = require('../../../../models/services/mongo/mongoose/mongooseVoteSignaturesRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const logger = require('../../../../models/services/common/logger').getLogger();
const mongo = require('mongodb');
const ObjectID = mongo.ObjectID;


async function updateMerkelizedStatus(txDocument) {
    let txCallbackData = txDocument.tx_metadata.callback_data;
    console.log("\n\n\nTransaction metadata is " + JSON.stringify(txCallbackData));
    let merk_claim_sig_ids = txCallbackData.merk_claim_sig_ids;
    let merk_vote_sig_ids = txCallbackData.merk_vote_sig_ids;
    let claim_ids = txCallbackData.claim_ids;

    if (merk_claim_sig_ids.length > 0) {
        var ObjIds = merk_claim_sig_ids.map(function (e) {
            return ObjectID(e);
        });

        await mongooseClaimSignaturesRepo.updateMany({ _id: { $in: ObjIds } }, { $set: { merkelized: true } })

    }

    if (merk_vote_sig_ids.length > 0) {
        var ObjIds = merk_vote_sig_ids.map(function (e) {
            return ObjectID(e);
        });
        await mongooseVoteSignaturesRepo.updateMany({ _id: { $in: ObjIds } }, { $set: { merkelized: true } });
    }

    if (claim_ids.length > 0){
        var ObjIds = claim_ids.map(function (e) {
            return ObjectID(e);
        });    
        await mongooseClaimsRepo.updateMany({ _id: { $in: ObjIds } }, { $set: { merkelized: true } });
    }
}

exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    let txStatus = parseInt(txReceipt.status, 16);
    if (txStatus === 1) {      
        await updateMerkelizedStatus(transactionDocument);
        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    }
};