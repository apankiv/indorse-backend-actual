var scHelper = require('../scHelper')

module.exports.addMerkleRoot = async function addMerkleRoot(timeKey,rootHash ) {
    return await scHelper.logAndQueueContractTx("MerkleClaims", "addMerkleRoot", timeKey, rootHash)
}


module.exports.getRootByTimeKey = async function getRootByTimeKey(key) {
    let response = await scHelper.logAndCallContract("MerkleClaims", "rootByTimeKey", key);
    return response;
}

module.exports.getOwner = async function getOwner() {
    let response = await scHelper.logAndCallContract("MerkleClaims", "owner");    
    return response;
}