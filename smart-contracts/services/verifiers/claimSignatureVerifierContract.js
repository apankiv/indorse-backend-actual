const scHelper = require('../scHelper');

module.exports.getChainId = async function getChainId() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "chainId")
};

module.exports.getSalt = async function getSalt() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "salt")
};

module.exports.getEIP712_DOMAIN = async function getEIP712_DOMAIN() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "EIP712_DOMAIN")
};

module.exports.getCLAIM_TYPE = async function getCLAIM_TYPE() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "CLAIM_TYPE")
};

module.exports.getDAPP_NAME = async function getDAPP_NAME() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "DAPP_NAME")
};

module.exports.getVERSION = async function getVERSION() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "VERSION")
};

module.exports.getEIP712_DOMAIN_TYPEHASH = async function getEIP712_DOMAIN_TYPEHASH() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "EIP712_DOMAIN_TYPEHASH")
};

module.exports.getCLAIM_TYPEHASH = async function getClaim_TYPEHASH() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "CLAIM_TYPEHASH")
};

module.exports.getDOMAIN_SEPARATOR = async function getDOMAIN_SEPARATOR() {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "DOMAIN_SEPARATOR")
};

module.exports.verify = async function verify(claimant_id, proof, timestamp, deadline, skills, kind, voters, claim_id,
                                              version, sigV, sigR, sigS) {
    return await scHelper.logAndCallContract("ClaimSignatureVerifier", "verify",
        claimant_id, proof, timestamp, deadline, skills, kind, voters, claim_id, version, sigV, sigR, sigS)
};