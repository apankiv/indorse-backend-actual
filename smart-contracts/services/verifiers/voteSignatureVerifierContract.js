const scHelper = require('../scHelper');

module.exports.getChainId = async function getChainId() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "chainId")
};

module.exports.getSalt = async function getSalt() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "salt")
};

module.exports.getEIP712_DOMAIN = async function getEIP712_DOMAIN() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "EIP712_DOMAIN")
};

module.exports.getVOTE_TYPE = async function getVOTE_TYPE() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "VOTE_TYPE")
};

module.exports.getDAPP_NAME = async function getDAPP_NAME() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "DAPP_NAME")
};

module.exports.getVERSION = async function getVERSION() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "VERSION")
};

module.exports.getEIP712_DOMAIN_TYPEHASH = async function getEIP712_DOMAIN_TYPEHASH() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "EIP712_DOMAIN_TYPEHASH")
};

module.exports.getVOTE_TYPEHASH = async function getVOTE_TYPEHASH() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "VOTE_TYPEHASH")
};

module.exports.getDOMAIN_SEPARATOR = async function getDOMAIN_SEPARATOR() {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "DOMAIN_SEPARATOR")
};

module.exports.verify = async function getChainId(signer, claim_id, decision, timestamp, version, skill, sigV, sigR, sigS) {
    return await scHelper.logAndCallContract("VoteSignatureVerifier", "verify",
        signer, claim_id, decision, timestamp, version, skill, sigV, sigR, sigS)
};