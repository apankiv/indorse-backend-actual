process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const config = require('config');
const db = require('../db');
const settings = require('../../models/settings');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const assignmentTestHelper = require('./testHelper');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const votesTestHelper = require('../votes/testHelper');

const { expect } = chai;
chai.use(chaiHttp);


describe('Assignment : test votes on multiskilled assignment', function() {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        db.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        db.drop(done);
    });

    it(`Votes after threshold of ${settings.CLAIM_VALIDATOR_COUNT} [CLAIM_VALIDATOR_COUNT]
        should not be allowed when voting done serially`, async () => {
        const { skills, claim, validators } =
            await assignmentTestHelper.createAndReleaseAssignmentClaim();

        let errMessage = null;
        try {
            await votesTestHelper.addVotesSerially(claim, validators, skills);
        } catch (err) {
            errMessage = err.message;
        }

        expect(errMessage).to.equal('Bad Request', 'No Bad Request is returned from /votes api' +
            ' when voting is done serially ');

        const countOfVotes = await mongooseVoteRepo.countAll({
            endorsed: true,
        });
        expect(countOfVotes).to.equal(
            settings.CLAIM_VALIDATOR_COUNT,
            'Incorrect number of votes when voting is done serially',
        );
    });

    it(`Votes after threshold of ${settings.CLAIM_VALIDATOR_COUNT} [CLAIM_VALIDATOR_COUNT]
        should not be allowed when voting done concurrently`, async () => {
        const { skills, claim, validators } =
            await assignmentTestHelper.createAndReleaseAssignmentClaim();

        let errMessage = null;
        try {
            await votesTestHelper.addVotes(claim, validators, skills);
        } catch (err) {
            errMessage = err.message;
        }

        expect(errMessage).to.equal('Bad Request', 'No Bad Request is returned from /votes api' +
            ' when voting is done concurrently ');

        const countOfVotes = await mongooseVoteRepo.countAll({
            endorsed: true,
        });
        expect(countOfVotes).to.equal(
            settings.CLAIM_VALIDATOR_COUNT,
            'Incorrect number of votes when voting is done concurrently',
        );
    });
});
