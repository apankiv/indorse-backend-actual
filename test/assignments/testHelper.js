const chai = require('chai');
const testUtils = require('../testUtils');
const server = require('../../server');
const authenticationWrapper = require('../authenticationWrapper');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const skillsTestHelper = require('../skills/testHelper');
const validatorsTestHelper = require('../validators/testHelper');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRolesRepo = require('../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseAssignmentRepo = require('../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const claimTestUtils = require('../services/claims/claimTestUtils');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');

class TestHelper {
    static async createAndReleaseAssignmentClaim() {
        const { tokenObj } = await authenticationWrapper.createAndGetAdminUser();
        const tags = [
            testUtils.generateRandomString(),
            testUtils.generateRandomString(),
        ];
        const parentSkill = skillsTestHelper.getRandomSkill();
        parentSkill._id = await mongooseSkillRepo.insert(parentSkill);
        const skill1 = skillsTestHelper.getRandomSkill(tags);
        skill1.parents = [parentSkill._id];
        skill1._id = await mongooseSkillRepo.insert(skill1);

        const skill2 = skillsTestHelper.getRandomSkill(tags);
        skill2.parents = [parentSkill._id];
        skill2._id = await mongooseSkillRepo.insert(skill2);

        const skills = [skill1, skill2];
        const randomJobRole = testUtils.generateRandomJobRole();
        randomJobRole.skillTags = tags;
        const randomJobRoleId = await mongooseJobRolesRepo.insert(randomJobRole);
        const randomAssignment = testUtils.generateRandomAssignment();
        randomAssignment.jobRoleId = randomJobRoleId;

        await TestHelper.createAssignment(randomAssignment, tokenObj);
        const assignment = await mongooseAssignmentRepo.findOne({
            title: randomAssignment.title,
        });
        await TestHelper.startAssignment(assignment._id, tokenObj);
        const validators = await validatorsTestHelper.createValidators(skills);
        await TestHelper.finishAssignment(assignment._id, skills, tokenObj);

        const claim = await mongooseClaimsRepo.findOne({
            type: 'ASSIGNMENT', assignment_id: assignment._id,
        });
        await claimTestUtils.approveAndRelease(claim._id);
        await claimCreationWrapper.createClaimInQueue(claim);
        await claimCreationWrapper.releaseClaimInQueue(claim._id);
        return { skills, claim, validators };
    }

    static createAssignment(assignment, tokenObj) {
        return chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({
                query: `
                        mutation CreateAssignment($ass: AssignmentInput!) {
                            createAssignment(assignment: $ass) {
                                title
                            }
                        }`,
                variables: { ass: assignment },
            });
    }

    static async startAssignment(assignmentId, tokenObj) {
        await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({
                query: `
                    mutation startAssignment($assignmentID: String!) {
                        startAssignment(assignmentID: $assignmentID) {
                            started_on
                        }
                    }`,
                variables: { assignmentID: assignmentId },
            });
    }

    static finishAssignment(assignmentId, skills, tokenObj) {
        const variables = {
            assignmentID: assignmentId,
            form: {
                proof: 'https://github.com/ensdomains/ens',
                skills: [
                    {
                        skillTag: skills[0].tags[0],
                        skillId: skills[0]._id,
                    },
                    {
                        skillTag: skills[1].tags[1],
                        skillId: skills[1]._id,
                    },
                ],
            },
        };
        return chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({
                query: `
                    mutation finishAssignment($assignmentID: String!, $form :FinishAssignmentForm! ) {
                        finishAssignment(assignmentID: $assignmentID, form: $form) {
                             _id
                            assignment_id
                        }     
                    }`,
                variables,
            });
    }


}

module.exports = TestHelper;
