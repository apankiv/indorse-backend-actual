const mongoUserRepo = require('./../models/services/mongo/mongoRepository')('users');
const chai = require('chai');
const server = require('../server');
const testUtils = require('./testUtils');
const chaiHttp = require('chai-http');

const expect = chai.expect;
const should = chai.should();

chai.use(chaiHttp);


exports.signupVerifyAuthenticate = signupVerifyAuthenticate;

/**
 *
 * @param user to signup
 * @param tokenObj tokenObj.token will hold auth token
 * @returns {Promise.<created user obj>}
 */
async function signupVerifyAuthenticate(user, tokenObj) {
    let response = await chai.request(server)
        .post('/signup')
        .send(user);

    expect(response).to.have.status(200);

    let userFromMongo = await mongoUserRepo.findOne({email: user.email.toLowerCase()});
    let verifyToken = userFromMongo.verify_token;

    let verifyUserRequest = {
        email: user.email,
        verify_token: verifyToken
    };

    response = await chai.request(server)
        .post('/verify-email')
        .send(verifyUserRequest);

    expect(response).to.have.status(200);

    response = await chai.request(server)
        .post('/login')
        .send({
            email: user.email,
            password: user.password
        });


    expect(response).to.have.status(200);
    should.exist(response.body.token)
    tokenObj.token = response.body.token;
    tokenObj.user = userFromMongo;

    response = await chai.request(server)
        .post('/me')
        .send({email: user.email})
        .set('Authorization', 'Bearer ' + tokenObj.token)

    expect(response).to.have.status(200);

    return response.body.profile;
};

exports.justSignUpNoVerify = async function justSignUpNoVerify(user) {
    let response = await chai.request(server)
        .post('/signup')
        .send(user);

    expect(response).to.have.status(200);

};

exports.signupVerifyAuthenticateWithRole = async function signupVerifyAuthenticateWithRole(user, tokenObj, role) {
    let userProfile = await signupVerifyAuthenticate(user, tokenObj);
    await mongoUserRepo.update({email: user.email}, {$set: {role: role}});

    return userProfile;
};

exports.createAndGetAdminUser = async function () {
    const adminUser = testUtils.generateRandomUser();
    const tokenObj = {};
    await exports.signupVerifyAuthenticateWithRole(
        adminUser,
        tokenObj,
        'admin',
    );
    return { adminUser, tokenObj };
}
