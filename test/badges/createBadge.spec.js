process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoBadgesRepo = require('../../models/services/mongo/mongoRepository')('badges');
const mongooseBadgeRepo = require('../../models/services/mongo/mongoose/mongooseBadgeRepo');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.createBadge', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /badges', () => {

        it('it should create a badge given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();


            let createBadgeRequest = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let res = await chai.request(server)
                .post('/badges')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createBadgeRequest);

            res.should.have.status(200);

            let badgeFromMongo = res.body;

            badgeFromMongo.id.should.equal(createBadgeRequest.pretty_id);
            badgeFromMongo.name.should.equal(createBadgeRequest.badge_name);
            badgeFromMongo.description.should.equal(createBadgeRequest.description);
            badgeFromMongo.extensions_cta_text.text.should.equal(createBadgeRequest.cta_text);
            badgeFromMongo.extensions_cta_link.link.should.equal(createBadgeRequest.cta_link);
        });

        it('it should fail if pretty_id already in use', async () => {

            let createBadgeRequest = testUtils.generateRandomBadge();
            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await chai.request(server)
                .post('/badges')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createBadgeRequest);

            try {
                await chai.request(server)
                    .post('/badges')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if badge_name is missing', async () => {

            let createBadgeRequest = testUtils.generateRandomBadge();
            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createBadgeRequest.badge_name;

            try {
                await chai.request(server)
                    .post('/badges')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail if pretty_id is missing', async () => {

            let createBadgeRequest = testUtils.generateRandomBadge();
            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createBadgeRequest.pretty_id;

            try {
                await chai.request(server)
                    .post('/badges')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail if logo_data is missing', async () => {

            let createBadgeRequest = testUtils.generateRandomBadge();
            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createBadgeRequest.logo_data;

            try {
                await chai.request(server)
                    .post('/badges')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            await authenticationWrapper.justSignUpNoVerify(normalUser);

            try {
                await chai.request(server)
                    .post('/badges')
                    .send(createBadgeRequest);

                new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .post('/badges')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});