process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http')
const DB = require('../db');
const mongoBadgeRepo = require('../../models/services/mongo/mongoRepository')('badges');
const badgeCreationWrapper = require('./badgeCreationWrapper');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.deleteBadge', function () {

    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('DELETE /badges/:pretty_id', () => {

        it('it should delete a badge given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            let tokenObj = {};

            await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);

            let res = await chai.request(server)
                .delete('/badges/' + createBadgeRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.status.should.equal(200);

            let deletedBadge = await mongoBadgeRepo.findOne({id : createBadgeRequest.pretty_id});

            should.not.exist(deletedBadge);
        });

        it('it should fail if user is not logged in', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            await authenticationWrapper.justSignUpNoVerify(normalUser);

            try {
                await chai.request(server)
                    .delete('/badges/' + createBadgeRequest.pretty_id)
                    .send(createBadgeRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not an admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .delete('/badges/' + createBadgeRequest.pretty_id)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createBadgeRequest);

                new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

    })
});