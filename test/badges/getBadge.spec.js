process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http')
const badgeCreationWrapper = require('./badgeCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.getBadge', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /badges/:pretty_id', () => {

        it('it should retrieve a badge given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            let tokenObj = {};

            await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);

            let res = await chai.request(server)
                .get('/badges/' + createBadgeRequest.pretty_id)
                .send();

            let retrievedBadge = res.body;
            expect(createBadgeRequest.pretty_id).to.equal(retrievedBadge.id);
        });

        it('it should return 404 for non-existing badge', async () => {

            let createBadgeRequest = testUtils.generateRandomBadge();

            try {
                let res = await chai.request(server)
                    .get('/badges/' + createBadgeRequest.pretty_id)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});