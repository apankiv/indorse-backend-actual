process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const badgeCreationWrapper = require('./badgeCreationWrapper');
const config = require('config');
chai.use(chaiHttp);

describe('badges.massAssignBadge', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /badges/massassign', () => {

        it('it should mass assign badges to 2 users. After that remval should also work', async () => {

            let adminUser = testUtils.generateRandomUser();
            let user1 = testUtils.generateRandomUser();
            let user2 = testUtils.generateRandomUser();

            let massAssRequest = {
                emails: [user1.email, user2.email],
                badges: [testUtils.generateRandomString(), testUtils.generateRandomString(), testUtils.generateRandomString()],
                assign: 1
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await authenticationWrapper.signupVerifyAuthenticate(user1, {});
            await authenticationWrapper.signupVerifyAuthenticate(user2, {});

            for(let badgeId of massAssRequest.badges) {
                let randomBadge = testUtils.generateRandomBadge();
                randomBadge.pretty_id = badgeId;
                await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
            }

            let res = await chai.request(server)
                .post('/badges/massassign')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(massAssRequest);

            res.should.have.status(200);

            let user1FromMongo = await mongooseUserRepo.findOneByEmail(user1.email);
            let user2FromMongo = await mongooseUserRepo.findOneByEmail(user2.email);

            user1FromMongo.badges.should.deep.equal(massAssRequest.badges);
            user2FromMongo.badges.should.deep.equal(massAssRequest.badges);

            massAssRequest.assign = 0;

            res = await chai.request(server)
                .post('/badges/massassign')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(massAssRequest);

            res.should.have.status(200);

            user1FromMongo = await mongooseUserRepo.findOneByEmail(user1.email);
            user2FromMongo = await mongooseUserRepo.findOneByEmail(user2.email);

            user1FromMongo.badges.length.should.equal(0);
            user2FromMongo.badges.length.should.equal(0);
        });


    })
});