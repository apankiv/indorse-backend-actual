process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoBadgeRepo = require('../../models/services/mongo/mongoRepository')('badges');
const mongooseBadgeRepo = require('../../models/services/mongo/mongoose/mongooseBadgeRepo');
const authenticationWrapper = require('../authenticationWrapper');
const badgeCreationWrapper = require('./badgeCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.updateBadge', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('PATCH /badges/:pretty_id', () => {

        it('it should update badge fields when request is correct', async () => {
            let adminUser = testUtils.generateRandomUser();

            let createBadgeRequest = testUtils.generateRandomBadge();

            let badgeUpdate = testUtils.generateRandomBadgeUpdate();

            let tokenObj = {};


            await badgeCreationWrapper.authenticateAndCreateBadge(adminUser,
                createBadgeRequest, tokenObj);

            let res = await chai.request(server)
                .patch('/badges/' + createBadgeRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(badgeUpdate);

            res.status.should.equal(200);

            let badgeFromMongo = res.body;

            badgeFromMongo.id.should.equal(createBadgeRequest.pretty_id);
            badgeFromMongo.name.should.equal(badgeUpdate.badge_name);
            badgeFromMongo.description.should.equal(badgeUpdate.description);
            badgeFromMongo.extensions_cta_text.text.should.equal(badgeUpdate.cta_text);
            badgeFromMongo.extensions_cta_link.link.should.equal(badgeUpdate.cta_link);
        });

        it('it should return 404 for non-existing badge', async () => {

            let adminUser = testUtils.generateRandomUser();

            let badgeUpdate = testUtils.generateRandomBadgeUpdate();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            try {
                await chai.request(server)
                    .patch('/badges/blah')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(badgeUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});