process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const settings = require('../../models/settings');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../authenticationWrapper');
const userChatbotCompleteWrapper = require('./userChatbotCompleteWrapper');

chai.use(chaiHttp);

describe('chatbot.acceptScore', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /chatbot/scores', () => {

        it('it should not assign any skill when user has no skill object and not pass the chatbot', async () => {

            let user = testUtils.generateRandomUser();
            let chatbotUuid = testUtils.generateRandomString(16);
            let skillName = 'javascript'
            let skillObject = {
                name: skillName,
                category: "technology",
                validation: {
                    "chatbot": true
                },
                __v: 0
            }
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin'); // TODO : Change it to profile

            await mongooseSkillRepo.insert({name: skillName, category: 'tech', validation: {chatbot: true, chatbot_uuid:chatbotUuid}});

            let res = await chai.request(server)
                .get('/chatbot/get/' + skillName)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            should.exist(res.body.chatbotUuid);
            should.exist(res.body.oauth_key);

            let body = {
                email: createdUser._id,
                payload_type: 'interview completed',
                chatbot_uuid: chatbotUuid,
                total_score: 15
            }

            res = await chai.request(server)
                .post('/chatbot/scores')
                .set('X-API-Key', settings.CHATBOT_PRIVATE_KEY)
                .send(body);

            let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);
            let userFirstSkill = userUpdated.skills[0];
            should.not.exist(userFirstSkill);

        });

        it('it should assign intermediate level validation for javascript', async () => {

            let user = testUtils.generateRandomUser();
            let skillName = 'javascript'

            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin'); // TODO : Change it to profile


            await userChatbotCompleteWrapper.userChatbotComplete(createdUser._id,70,'javascript');
            let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);
            let userFirstSkill = userUpdated.skills[0];
            expect(userFirstSkill.level).to.equal('beginner');
            should.exist(userFirstSkill.validations);
            expect(userFirstSkill.validations[0].validated).to.equal(true);
            expect(userFirstSkill.validations[0].type).to.equal('quizbot');
            expect(userFirstSkill.skill.name).to.equal(skillName);
            should.exist(userFirstSkill.validations[0].validatedAt);
            should.exist(userFirstSkill.validations[0].chatbotScore);

        });

        it('it should not allow to update score if there is no valid private key', async () => {

            let user = testUtils.generateRandomUser();
            let chatbotUuid = testUtils.generateRandomString(16);
            let skillName = 'javascript'
            let skillObject = {
                name: skillName,
                category: "technology",
                validation: {
                    "chatbot": true
                },
                __v: 0
            }
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin'); // TODO : Change it to profile


            await mongooseSkillRepo.insert({name: skillName, category: 'tech', validation: {chatbot: true, chatbot_uuid:chatbotUuid}});
            let res = await chai.request(server)
                .get('/chatbot/get/' + skillName)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            should.exist(res.body.chatbotUuid);
            should.exist(res.body.oauth_key);

            let body = {
                email: createdUser._id,
                payload_type: 'interview completed',
                chatbot_uuid: chatbotUuid,
                total_score: 100
            }
            try{
                res = await chai.request(server)
                    .post('/chatbot/scores')
                    .set('X-API-Key', 'mrHacker')
                    .send(body);

            }catch(e){
                expect(e.status).to.equal(403);
                let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);
                let userFirstSkill = userUpdated.skills[0];
                should.not.exist(userFirstSkill);
            }


        });



        it('it should assign beginner level validation for user who already has chatbot beginner validation', async () => {

            let user = testUtils.generateRandomUser();
            let chatbotUuid = testUtils.generateRandomString(16);
            let skillName = 'javascript'
            let skillObject = {
                name: skillName,
                category: "technology",
                validation: {
                    "chatbot": true
                },
                __v: 0
            }
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin'); // TODO : Change it to profile


            await mongooseSkillRepo.insert({name: skillName, category: 'tech', validation: {chatbot: true, chatbot_uuid:chatbotUuid}});
            let res = await chai.request(server)
                .get('/chatbot/get/' + skillName)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            should.exist(res.body.chatbotUuid);
            should.exist(res.body.oauth_key);

            let requestFromImpress = {
                email: createdUser._id,
                payload_type: 'interview completed',
                chatbot_uuid: chatbotUuid,
                total_score: 68
            }

            res = await chai.request(server)
                .post('/chatbot/scores')
                .set('X-API-Key', settings.CHATBOT_PRIVATE_KEY)
                .send(requestFromImpress);

            let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);
            let userFirstSkill = userUpdated.skills[0];
            expect(userFirstSkill.level).to.equal('beginner');
            should.exist(userFirstSkill.validations);
            expect(userFirstSkill.validations[0].validated).to.equal(true);
            expect(userFirstSkill.validations[0].type).to.equal('quizbot');
            expect(userFirstSkill.validations[0].level).to.equal('beginner');
            expect(userFirstSkill.skill.name).to.equal(skillName);

            let requestFromImpressSecondAttempt = {
                email: createdUser._id,
                payload_type: 'interview completed',
                chatbot_uuid: chatbotUuid,
                total_score: 80
            }

            res = await chai.request(server)
                .post('/chatbot/scores')
                .set('X-API-Key', settings.CHATBOT_PRIVATE_KEY)
                .send(requestFromImpressSecondAttempt);

            let userUpdated2 = await mongooseUserRepo.findOneById(createdUser._id);
            let userSecondSkill = userUpdated2.skills[0];
            expect(userSecondSkill.level).to.equal('beginner');
            should.exist(userSecondSkill.validations);


        });


    })
});

