process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const checkClaimsJob = require('../../smart-contracts/services/cron/checkClaimsJob');
const settings = require('../../models/settings');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../authenticationWrapper');

chai.use(chaiHttp);
const ObjectID = require('mongodb').ObjectID;

describe('claims.createClaim', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;

    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /chatbot/get', () => {

        it('it should return oauth token and chatbot uuid', async () => {

            let user = testUtils.generateRandomUser();
            let chatbotUuid = testUtils.generateRandomString(16);
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');
            await mongooseSkillRepo.insert({name: 'javascript', category: 'tech', validation: {chatbot: true, chatbot_uuid:chatbotUuid}});

            let skill2 = await mongooseSkillRepo.findOne({"validation.chatbot_uuid" : chatbotUuid});

            let res = await chai.request(server)
                .get('/chatbot/get/javascript')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            should.exist(res.body.chatbotUuid);
            should.exist(res.body.oauth_key);
            let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);
            should.exist(userUpdated.oauth_key)


        });


    })
});

