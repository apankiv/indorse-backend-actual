process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server')
const chaiHttp = require('chai-http');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const settings = require('../../models/settings');
const testUtils = require('../testUtils');
chai.use(chaiHttp);


exports.userChatbotComplete = async function userChatbotComplete(userId, totalScore, chatbotSkillName) {
    let chatbotUuid = testUtils.generateRandomString(16);
    await setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
    await updateUserChatbotState(userId,totalScore, chatbotUuid);
};

exports.userChatbotCompleteNoChatbotInitialize = async function userChatbotCompleteNoChatbotInitialize(userId, totalScore, chatbotUuid) {
    await updateUserChatbotState(userId,totalScore, chatbotUuid);
};

async function setupChatbotWithSkillName(skillName, chatbotUuid){
    await mongooseSkillRepo.insert({name: skillName, category: 'tech', validation: {chatbot: true, chatbot_uuid:chatbotUuid}});
}
exports.setupChatbotWithSkillName = setupChatbotWithSkillName;
async function updateUserChatbotState(userId, totalScore, chatbotUuid) {
    let body = {
        email: userId,
        payload_type: 'interview completed',
        chatbot_uuid: chatbotUuid,
        total_score: totalScore
    }

    res = await chai.request(server)
        .post('/chatbot/scores')
        .set('X-API-Key', settings.CHATBOT_PRIVATE_KEY)
        .send(body);
}
