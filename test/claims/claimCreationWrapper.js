process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const settings = require('../../models/settings');
const claimReleaserJob = require('../../models/services/crons/claimReleaserJob');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');
const queueClaimApprovalService = require('../../models/claims/queueClaimApprovalService');
const mongooseClaimQueueRepo = require('../../models/services/mongo/mongoose/mongooseClaimQueueRepo');
const testUtils = require('../testUtils');

chai.use(chaiHttp);

exports.createValidatorSet = async function createValidatorSet(setSize, skillsArray) {
    const validators = [];

    for(let skill of skillsArray) {
        await mongooseSkillRepo.insert({
            name: skill.toLowerCase(),
            category: "test",
            validation: {
                aip: true
            },
        });
    }

    for (let i = 0; i < setSize; i += 1) {
        const randomUser = testUtils.generateRandomUser();
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
        let createdUser = await mongooseUserRepo.findOneByEmail(randomUser.email);
        let randomEthAddress = testUtils.generateRandomEthAddress();

        await mongooseUserRepo.update({email : createdUser.email}, {$set : {ethaddress : randomEthAddress}});

        let validator = {
            user_id : createdUser._id.toString(),
            skills : skillsArray,
            ethaddress: randomEthAddress,
            username : randomUser.username,
            email : randomUser.email,
            tier : '_2'
        };

        validators.push({user_id : createdUser._id.toString(),tokenObj : tokenObj, ethaddress : randomEthAddress} );
        await mongooseValidatorRepo.insert(validator)
    }
    return validators;
};

exports.authenticateAndCreateClaim = async function authenticateAndCreateBadge(user, claim, tokenObj) {
    const createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');
    await mongooseSkillRepo.insert({ name: claim.title.toLowerCase(), category: 'tech' });
    const randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({ email: createdUser.email }, { $set: { ethaddress: randomEthAddress } });

    await createClaim(claim, tokenObj);

    return createdUser;
};

exports.authenticateAdminAndCreateClaim = async function authenticateAdminAndCreateClaim(user, claim, tokenObj) {
    const createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');

    const randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({ email: createdUser.email }, { $set: { ethaddress: randomEthAddress } });

    await createClaim(claim, tokenObj);

    return createdUser;
};

exports.createClaimInQueue = function createClaimInQueue(claim) {
    return queueClaimApprovalService.queueClaimApproval(claim);
}

exports.releaseClaimInQueue = function releaseClaimInQueue(claimId) {
    return mongooseClaimQueueRepo.update(
        { claim_id: claimId },
        { released: true },
    );
}

async function createClaim(claim, tokenObj) {
    const res = await chai.request(server)
        .post('/claims')
        .set('Authorization', `Bearer ${tokenObj.token}`)
        .send(claim);

    await mongooseClaimsRepo.approveClaim(res.body.claim.claim_id, 'test', [{
        tier: '_2', noOfValidator: settings.CLAIM_VALIDATOR_COUNT
    }]);

    await claimReleaserJob.releaseClaims();
}

exports.createClaim = createClaim;
