process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const claimCreationWrapper = require('./claimCreationWrapper');
const timestampService = require('../../models/services/common/timestampService')
const mongooseRewardsRepo = require('../../models/services/mongo/mongoose/mongooseRewardsRepo');
const mongooseClaimEventsRepo = require('../../models/services/mongo/mongoose/mongooseClaimEvents');
const mongooseClaimRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const eventUtils = require('../../models/services/common/eventUtils')
const indorseTokenDeployer = require('../smart-contracts/indorseToken/indorseTokenDeployer');
const mongooseTransactionQueueRepo = require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const resolverLoopWrapper = require('../smart-contracts/cron/resolverLoopWrapper');
const indorseTokenFacade = require('../../smart-contracts/services/indorseToken/indorseTokenContract');
const indorseTokenTransferExecutor = require('../../models/services/ethereum/indorseToken/txExecutors/transferExecutor');
const settings = require('../../models/settings');
const { updateValidatorCommentActionType } = require('../../graphql/resolvers/votesAdmin/updateValidatorComment');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const voteTestHelper = require('../votes/voteTesthelper');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');


chai.use(chaiHttp);


describe('Claim reputation rewards', function () {

    beforeEach(async (done) => {
        await indorseTokenDeployer.deploy();
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(async (done) => {
        console.log('dropping database');
        DB.drop(done);        
        //done();
    });

    async function createClaimVotes(eventToCreate,numOfEvents,userId){
        let claimEvent = {
            user_id :userId,
            event: eventToCreate,
            claim_id:'5be5ac4e3b9a9c10730ed53f',
            voting_round_id:'5be5ac4e3b9a9c10730ed53f',
            timestamp: timestampService.createTimestamp()
        }

        for (i = 0; i < numOfEvents;i++){
            await mongooseClaimEventsRepo.insert(claimEvent);
        }
    }

    async function ensureReward(validator,reward){
        let resultVal = await mongooseRewardsRepo.findOne({ user_id: validator });
        resultVal.reward.should.equal(reward);
    }

    async function ensureNoReward(validator) {
        let resultVal = await mongooseRewardsRepo.findOne({ user_id: validator });
        should.not.exist(resultVal);
    }

    it.skip('it should return payout data', async () => {
        //Gerate validators
        let validators = await claimCreationWrapper.createValidatorSet(10, ['Javascript']);

        for(i=0;i<9;i++){
            validator = validators[i]

            let userId = validator.user_id
            let userAddress = validator.ethaddress
            let initiatingUserId = "initiatingUserID123"
            let valueToSend = 1000
            let metadata = {
                claim_id: "claimID123"
            }
            let userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(0, "should start with 0 IND tokens")
            await indorseTokenTransferExecutor.execute(userId, userAddress, valueToSend, initiatingUserId, metadata);

            let queueDoc = await mongooseTransactionQueueRepo.findOne({ initiating_user_id: initiatingUserId, status: 'QUEUED' });
            should.exist(queueDoc, "transaction was not found");

            await resolverLoopWrapper.processTransactions();

            userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(valueToSend, "should have had IND tokens transfered to")

        }


        //Setup votes : 50% in consensus
        await createClaimVotes('vote_in_consensus',5,validators[0].user_id);
        await createClaimVotes('vote_not_in_consensus', 5, validators[0].user_id);

        //Setup votes : 20% in consensus
        await createClaimVotes('vote_in_consensus', 2, validators[1].user_id);
        await createClaimVotes('vote_not_in_consensus', 8, validators[1].user_id);


        //Setup votes : 70% in consensus
        await createClaimVotes('vote_in_consensus', 7, validators[2].user_id);
        await createClaimVotes('vote_not_in_consensus', 3, validators[2].user_id);

        //Generate rewards
        await eventUtils.geneateReputationRewards();

        //Check if correct rewards were distributed
        await ensureReward(validators[2].user_id,20);
        await ensureReward(validators[0].user_id,10);

        //wait a few secs to elapse
        await testUtils.wait(1000);

        //generate 50% consensus
        await createClaimVotes('vote_in_consensus', 5, validators[3].user_id);
        await createClaimVotes('vote_not_in_consensus', 5, validators[3].user_id);

        //generate 30% consensus
        await createClaimVotes('vote_in_consensus', 3, validators[4].user_id);
        await createClaimVotes('vote_not_in_consensus', 7, validators[4].user_id);

        //Re-Generate rewards
        await eventUtils.geneateReputationRewards();
        //Only the last two validators must be selected for rewards
        await ensureReward(validators[3].user_id, 20);
        await ensureReward(validators[4].user_id, 10);
        await testUtils.wait(1000);

        //generate 10% consensus , Min needed is 20
        await createClaimVotes('vote_in_consensus', 1, validators[5].user_id);
        await createClaimVotes('vote_not_in_consensus', 9, validators[5].user_id);

        //Re-Generate rewards
        await eventUtils.geneateReputationRewards();

        let resultVal = await mongooseRewardsRepo.findOne({ user_id: validators[5].user_id });
        should.not.exist(resultVal);

        await testUtils.wait(1000);

        //generate 30% consensus , Min needed is 20
        await createClaimVotes('vote_in_consensus', 3, validators[6].user_id);
        await createClaimVotes('vote_not_in_consensus',7 , validators[6].user_id);

        //Re-Generate rewards
        await eventUtils.geneateReputationRewards();
        await ensureReward(validators[6].user_id, 20);


        //generate 100% consensus , but no ind balance
        await createClaimVotes('vote_in_consensus', 10, validators[9].user_id);
        await eventUtils.geneateReputationRewards();
        await ensureNoReward(validators[9].user_id);
    })



    it.skip("should store and calculate reputation for all ", async () => {

        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
        let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
        await mongooseUserRepo.update({ _id: firstUser._id }, { role: 'admin' });


        let createdUser = await mongooseUserRepo.findOneByEmail(user.email);        
        let createdUserId = String(createdUser._id);

        let date = new Date();
        let timestamp = timestampService.normalizeToStartOfDay(date)

        //Hack to have 4 votes for same validator
        await mongooseVoteRepo.updateMany({}, { voter_id: validators[0].user_id, voted_at: timestamp, sc_vote_exists: true });

        let votes =await mongooseVoteRepo.findAll({});
        
        //Vote 0
        await mongooseVoteRepo.update({_id : votes[0]._id}, {$set : {"upvote.count": 5 , "downvote.count" : 2 }});

        //vote 1
        await mongooseVoteRepo.update({ _id: votes[1]._id }, { $set: { "upvote.count": 3, "downvote.count": 5 } });

        //Vote 2
        await mongooseVoteRepo.update({ _id: votes[2]._id }, { $set: { "hide.isHidden": true} });        
        
        

        //generate 50% vote consensus
        await createClaimVotes('vote_in_consensus', 7, validators[0].user_id);
        await createClaimVotes('vote_not_in_consensus', 3, validators[0].user_id);


        //Generate rewards
        await eventUtils.geneateReputationRewards();
        console.log("Validator is " + validators[0].user_id )
        let validator = await mongooseValidatorRepo.findOne({ user_id: validators[0].user_id });        
        validator.reputation_data[0].score.should.equal(5)


        await mongooseVoteRepo.update({ _id: votes[3]._id }, { $set: { "upvote.count": 5, "downvote.count": 2 } });
        await eventUtils.geneateReputationRewards();
        validator = await mongooseValidatorRepo.findOne({ user_id: validators[0].user_id });        
        validator.reputation_data[1].score.should.equal(11.25)

    })


});