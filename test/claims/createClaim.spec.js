process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const checkClaimsJob = require('../../smart-contracts/services/cron/checkClaimsJob');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');

const testUtils = require('../testUtils');

const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');

chai.use(chaiHttp);

describe('claims.createClaim', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /claims', () => {
        it('it should create a claim given valid request', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(
                user,
                createClaimRequest,
                tokenObj,
            );
            let createdClaims = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            let [createdClaim] = createdClaims;
            should.exist(createdUser._id);
            createdClaim.title.should.equal(createClaimRequest.title);
            createdClaim.desc.should.equal(createClaimRequest.desc);
            createdClaim.proof.should.equal(createClaimRequest.proof);
        });

        it('it should create a claim given valid request for admin', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            const validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(
                user,
                createClaimRequest,
                tokenObj,
            );

            let createdClaims = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            let [createdClaim] = createdClaims;
            should.exist(createdUser._id);

            createdClaim.title.should.equal(createClaimRequest.title);
            createdClaim.desc.should.equal(createClaimRequest.desc);
            createdClaim.proof.should.equal(createClaimRequest.proof);
        });
    });
});

