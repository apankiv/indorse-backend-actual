process.env.NODE_ENV = 'test';

const server = require('../../server');
const chai = require('chai');
const config = require('config');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const checkClaimsJob = require('../../smart-contracts/services/cron/checkClaimsJob');
const settings = require('../../models/settings');
const partnerClaimCreationWrapper = require('./partnerClaimCreationWrapper');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');

const DB = require('../db');
const testUtils = require('../testUtils');

const should = chai.should();

chai.use(chaiHttp);

describe('claims.createPartnerClaim', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach(async (done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /create-partner-claim', function () {

        it('it should create a claim given valid request', async () => {
            const createPartnerClaimRequest = testUtils.generateRandomPartnerClaimCreationRequest();
            const clientAppConfig = {
                verifyURL: '',
                resultURL: '',
                clientId: createPartnerClaimRequest.partnerClientId,
                auth: 'basic',
            };
            const clientAppObject = await partnerClaimCreationWrapper.setupClientApp(clientAppConfig);
            const adminUser = testUtils.generateRandomUser();
            await partnerClaimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);

            const tokenObj = {};
            await partnerClaimCreationWrapper.authenticateAndCreatePartnerClaim(adminUser, createPartnerClaimRequest, tokenObj);
            let createdClaim = await mongooseClaimsRepo.findOne({'clientApp.clientId': clientAppObject.client_id});
            should.exist(createdClaim.clientApp);
            should.not.exist(createdClaim.owner_id);
            createdClaim.title.should.equal(createPartnerClaimRequest.skillPrettyId);
            createdClaim.desc.should.equal(createPartnerClaimRequest.description);
            createdClaim.proof.should.equal(createPartnerClaimRequest.proof);
            const {clientApp} = createdClaim;
            clientApp.userId.should.equal(createPartnerClaimRequest.partnerUserId);
            clientApp.clientId.should.equal(createPartnerClaimRequest.partnerClientId);
            clientApp.state.should.equal(createPartnerClaimRequest.state);
            let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim._id);

            should.exist(votingRound);

            const votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

            votes.length.should.equal(settings.CLAIM_VALIDATOR_COUNT);

            for (let voteIndex = 0; voteIndex < votes.length; voteIndex += 1) {
                const vote = votes[voteIndex];
                vote.claim_id.should.equal(createdClaim._id.toString());
            }

            await mongooseVotingRoundRepo.update({_id: votingRound._id}, {$set: {end_voting: 1}});

            await checkClaimsJob.processOutstandingClaims();
            votingRound = await mongooseVotingRoundRepo.findOne({claim_id: createdClaim._id});

            votingRound.status.should.equal('finished');
            createdClaim = await mongooseClaimsRepo.findOne(createdClaim._id);
            createdClaim.final_status.should.equal(false);
        });

        it('it should create a claim given valid request for admin', async () => {
            const createPartnerClaimRequest = testUtils.generateRandomPartnerClaimCreationRequest();
            const clientAppConfig = {
                verifyURL: '',
                resultURL: '',
                clientId: createPartnerClaimRequest.partnerClientId,
                auth: 'basic',
            };
            const clientAppObject = await partnerClaimCreationWrapper.setupClientApp(clientAppConfig);
            console.log('clientAppObject: ', clientAppObject);
            const adminUser = testUtils.generateRandomUser();
            const validators = await partnerClaimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);

            const tokenObj = {};
            await partnerClaimCreationWrapper.authenticateAndCreatePartnerClaim(adminUser, createPartnerClaimRequest, tokenObj);
            let createdClaim = await mongooseClaimsRepo.findOne({'clientApp.clientId': clientAppObject.client_id});
            should.exist(createdClaim.clientApp);
            should.not.exist(createdClaim.owner_id);
            createdClaim.title.should.equal(createPartnerClaimRequest.skillPrettyId);
            createdClaim.desc.should.equal(createPartnerClaimRequest.description);
            createdClaim.proof.should.equal(createPartnerClaimRequest.proof);
            const {clientApp} = createdClaim;
            clientApp.userId.should.equal(createPartnerClaimRequest.partnerUserId);
            clientApp.clientId.should.equal(createPartnerClaimRequest.partnerClientId);
            clientApp.state.should.equal(createPartnerClaimRequest.state);

            let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim._id);

            should.exist(votingRound);
            votingRound.claim_id.should.equal(createdClaim._id.toString());

            const votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

            votes.length.should.equal(settings.CLAIM_VALIDATOR_COUNT);

            for (let voteIndex = 0; voteIndex < votes.length; voteIndex += 1) {
                const vote = votes[voteIndex];
                vote.claim_id.should.equal(createdClaim._id.toString());
            }

            // Cast a vote
            const validatorVote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            const vote = validatorVote[0];
            await chai.request(server)
                .post(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${validators[0].tokenObj.token}`)
                .send({
                    endorse: true,
                    feedback: {
                        quality: 1,
                        designPatterns: 1,
                        gitFlow: 1,
                        explanation: 'ble',
                        testCoverage: 1,
                        readability: 1,
                        extensibility: 1,
                    },
                });

            await mongooseVotingRoundRepo.update({_id: votingRound._id}, {$set: {end_voting: 1}});

            await checkClaimsJob.processOutstandingClaims();
            votingRound = await mongooseVotingRoundRepo.findOne({claim_id: createdClaim._id});

            votingRound.status.should.equal('finished');
            createdClaim = await mongooseClaimsRepo.findOne({'clientApp.clientId': createPartnerClaimRequest.partnerClientId});
            createdClaim.final_status.should.equal(false);
        });

        it('it should fail to create a claim without the partner in the DB', async () => {
            const createPartnerClaimRequest = testUtils.generateRandomPartnerClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            await partnerClaimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            try {
                await partnerClaimCreationWrapper.authenticateAndCreatePartnerClaim(user, createPartnerClaimRequest, tokenObj);
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    });
});
