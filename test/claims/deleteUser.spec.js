process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const authenticationWrapper = require('../authenticationWrapper');
chai.use(chaiHttp);
const ObjectID = require('mongodb').ObjectID;

describe('profile.deleteUser', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;

    beforeEach(async () => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('DELETE /users/:user_id', () => {

        it('it should update notification flags for all created voting rounds', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();
            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);

            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, {});
            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await chai.request(server)
                .delete('/users/' + createdUser._id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);

            should.not.exist(createdClaim[0]);

            let deletedUser = await mongooseUserRepo.findOne({_id: createdUser._id});

            should.not.exist(deletedUser);
        });

        it('it should fail for non-admin user', async () => {

            it('it should update notification flags for all created voting rounds', async () => {

                let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
                let user = testUtils.generateRandomUser();
                let adminUser = testUtils.generateRandomUser();

                let tokenObj = {};

                await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);

                let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, {});
                await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'full_access');

                try {
                    await chai.request(server)
                        .delete('/users/' + createdUser._id)
                        .set('Authorization', 'Bearer ' + tokenObj.token)
                        .send();

                    throw new Error('Expected to fail!');
                } catch (error) {
                    error.status.should.equal(403);
                }
            });
        })
    })
});

