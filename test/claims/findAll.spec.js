process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require("../../models/services/mongo/mongoRepository")('users');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoClaimsRepo = require("../../models/services/mongo/mongoRepository")('claims');
const mongoVotesRepo = require("../../models/services/mongo/mongoRepository")('votes');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('claims.findAll', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /claimsall', () => {

        it('When no search param it should retrieve all created claims', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let claimsCount = 10;

            for (let index = 0; index < claimsCount; index++) {
                let randomClaim = testUtils.generateRandomClaim();
                delete randomClaim['ownerid'];
                await mongoClaimsRepo.insert(randomClaim);
            }

            let res = await chai.request(server)
                .get('/claimsall')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(claimsCount);
            res.body.totalClaims.should.equal(claimsCount);
        });

        it('Pagination should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let claimsCount = 10;

            for (let index = 0; index < claimsCount; index++) {
                let randomClaim = testUtils.generateRandomClaim();
                delete randomClaim['ownerid']
                await mongoClaimsRepo.insert(randomClaim);
            }

            let res = await chai.request(server)
                .get('/claimsall?pageNo=' + 1 + '&perPage=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(2);

            res = await chai.request(server)
                .get('/claimsall?pageNo=' + claimsCount + '&perPage=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(0);

            res = await chai.request(server)
                .get('/claimsall?pageNo=' + 2 + '&perPage=9')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);
        });

        it('Search should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomClaim = testUtils.generateRandomClaim();
            delete randomClaim['ownerid']

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await mongoClaimsRepo.insert(randomClaim);

            let res = await chai.request(server)
                .get('/claimsall?search=' + randomClaim.title)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);
            res.body.totalClaims.should.equal(1);
            res.body.matchingClaims.should.equal(1);

            let createdClaim = await mongoClaimsRepo.findOne({title : randomClaim.title});

            res = await chai.request(server)
                .get('/claimsall?search=' + createdClaim._id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);
            res.body.totalClaims.should.equal(1);
            res.body.matchingClaims.should.equal(1);

        });

        it('Sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomClaim = testUtils.generateRandomClaim();
            delete randomClaim['ownerid']

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await mongoClaimsRepo.insert(randomClaim);

            let res = await chai.request(server)
                .get('/claimsall?sort=title')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);
        });

        it('Reverse sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomClaim = testUtils.generateRandomClaim();
            delete randomClaim['ownerid']

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await mongoClaimsRepo.insert(randomClaim);

            let res = await chai.request(server)
                .get('/claimsall?sort=-title')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);
        });
    })
});
