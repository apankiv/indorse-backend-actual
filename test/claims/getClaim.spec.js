/* eslint-disable no-console */
/* eslint-disable func-names */
process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseClaimEvents = require('../../models/services/mongo/mongoose/mongooseClaimEvents');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');

const testUtils = require('../testUtils');

const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');

chai.use(chaiHttp);

// eslint-disable-next-line no-undef
describe('claims.getClaim', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /claims/:claim_id', () => {

        it('it should return a claim with vote for validator', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            const validators = await claimCreationWrapper.createValidatorSet(
                settings.MAX_CLAIM_VALIDATOR_COUNT,
                ['Javascript'],
            );
            const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(
                user,
                createClaimRequest,
                tokenObj,
            );
            const createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);

            const res = await chai
                .request(server)
                .get(`/claims/${createdClaim[0]._id.toString()}`)
                .set('Authorization', `Bearer ${validators[0].tokenObj.token}`)
                .send();

            res.body.claim.skills.length.should.equal(1);
            res.body.claim.skills[0].noOfTentativeVotes.should.equal(0);
            res.body.claim.status.should.equal('in_progress');
            //should.exist(res.body.votes);
            should.exist(res.body.votinground);
            res.body.claim.title.should.equal(createClaimRequest.title);
            res.body.claim.desc.should.equal(createClaimRequest.desc);
            res.body.claim.proof.should.equal(createClaimRequest.proof);

            const claimEvent = await mongooseClaimEvents.findOne({
                user_id: validators[0].user_id,
            });
            should.exist(claimEvent);

            // View again
            await chai
                .request(server)
                .get(`/claims/${createdClaim[0]._id.toString()}`)
                .set('Authorization', `Bearer ${validators[0].tokenObj.token}`)
                .send();

            const claimEvents = await mongooseClaimEvents.findAll({
                user_id: validators[0].user_id,
            });
            claimEvents.length.should.equal(1);
        });


        it('it should return a claim without vote for others', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();
            const otherUser = testUtils.generateRandomUser();

            const tokenObj = {};
            const otherTokenObj = {};

            await claimCreationWrapper.createValidatorSet(
                settings.MAX_CLAIM_VALIDATOR_COUNT,
                ['Javascript'],
            );

            const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(
                user,
                createClaimRequest,
                tokenObj,
            );
            const createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            await authenticationWrapper.signupVerifyAuthenticate(otherUser, otherTokenObj);

            const res = await chai
                .request(server)
                .get(`/claims/${createdClaim[0]._id.toString()}`)
                .set('Authorization', `Bearer ${otherTokenObj.token}`)
                .send();

            res.body.claim.status.should.equal('in_progress');
            should.not.exist(res.body.votes);
            should.not.exist(res.body.vote);
            should.exist(res.body.votinground);
            res.body.claim.title.should.equal(createClaimRequest.title);
            res.body.claim.desc.should.equal(createClaimRequest.desc);
            res.body.claim.proof.should.equal(createClaimRequest.proof);
        });

        it('it should return a title and full title of skill of a claim', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();
            const tokenObj = {};
            const parentSkill = testUtils.generateRandomSkill();
            const parentSkillId = await mongooseSkillRepo.insert({
                name: parentSkill,
                category: 'technology',
            });
            await mongooseSkillRepo.insert({
                name: createClaimRequest.title.toLowerCase(),
                category: 'technology',
                parents: [parentSkillId],
            });
            await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, [
                createClaimRequest.title,
            ]);
            const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(
                user,
                createClaimRequest,
                tokenObj,
            );
            const createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);

            const res = await chai
                .request(server)
                .get(`/claims/${createdClaim[0]._id.toString()}`)
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send();

            res.body.claim.title.should.equal(createClaimRequest.title);
            res.body.claim.full_title.should.equal(
                'I can code in Javascript at the level of ...'
            );
        });
    });
});
