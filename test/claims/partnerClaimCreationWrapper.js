process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseClientAppRepo = require('../../models/services/mongo/mongoose/mongooseClientAppRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');
const config = require('config');
const testUtils = require('../testUtils');
const claimsReleaser = require('../../models/services/crons/claimReleaserJob');

chai.use(chaiHttp);

exports.setupClientApp = async function setupClientApp({ clientId, verifyURL, resultURL, auth }) {
    // create verify hook object
    const verifyPartnerClaim = {
        url: verifyURL || `http://${config.get('server.hostname')}/fake-partner-verify?status=200`,
        method: 'POST',
    };
    // create result hook object
    const sendPartnerClaimResult = {
        url: resultURL || `http://${config.get('server.hostname')}/fake-partner-result-reciever`,
        method: 'POST',
    };
    const hooks = {};
    if (verifyPartnerClaim.url) {
        hooks.verifyPartnerClaim = verifyPartnerClaim;
    }
    if (sendPartnerClaimResult.url) {
        hooks.sendPartnerClaimResult = sendPartnerClaimResult;
    }

    const objectToInsert = {
        client_id: clientId,
        display_name: 'Partner for test',
        auth: auth || 'handshake',
        hooks,
    };
    await mongooseClientAppRepo.insert(objectToInsert);
    const clientApps = mongooseClientAppRepo.findOneByClientId(clientId);
    return clientApps;
};

exports.createValidatorSet = async function createValidatorSet(setSize, skillsArray) {
    const validators = [];

    for(let skill of skillsArray) {
        await mongooseSkillRepo.insert({
            name: skill.toLowerCase(),
            category: "test",
            validation: {
                aip: true
            },
        });
    }

    for (let i = 0; i < setSize; i += 1) {
        const randomUser = testUtils.generateRandomUser();
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
        const createdUser = await mongooseUserRepo.findOneByEmail(randomUser.email);
        const randomEthAddress = testUtils.generateRandomEthAddress();
        await mongooseUserRepo.update({ email: createdUser.email }, { $set: { ethaddress: randomEthAddress } });

        const validator = {
            user_id: createdUser._id.toString(),
            skills: skillsArray,
            ethaddress : randomEthAddress,
            username : createdUser.username,
            email : createdUser.email
        };

        validators.push({ user_id: createdUser._id.toString(), tokenObj });
        await mongooseValidatorRepo.insert(validator);
    }
    return validators;
};

exports.authenticateAndCreatePartnerClaim = async function authenticateAndCreatePartnerClaim(user, claim, tokenObj) {
    const createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');
    await mongooseSkillRepo.insert({ name: claim.skillPrettyId.toLowerCase(), category: 'tech' });
    const randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({ email: createdUser.email }, { $set: { ethaddress: randomEthAddress } });

    await createPartnerClaim(claim, tokenObj);

    return createdUser;
};

exports.authenticateAdminAndCreatePartnerClaim = async function authenticateAdminAndCreatePartnerClaim(user, claim, tokenObj) {
    const createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');

    const randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({ email: createdUser.email }, { $set: { ethaddress: randomEthAddress } });

    await createPartnerClaim(claim, tokenObj);

    return createdUser;
};

async function createPartnerClaim(claim, tokenObj) {
    const res = await chai.request(server)
        .post('/create-partner-claim')
        .send(claim);
    const { claimId } = res.body.data || {};
    // return claimId;
    await chai.request(server)
        .post(`/claims/${claimId}/approve`)
        .set('Authorization', `Bearer ${tokenObj.token}`)
        .send(claim);
    await claimsReleaser.releaseClaims();
}

exports.createPartnerClaim = createPartnerClaim;
