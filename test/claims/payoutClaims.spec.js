process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');

chai.use(chaiHttp);

describe('payoutClaims REST', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('it should return payout data', async () => {
        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

        let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim[0]._id);
        let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

        for (let voteIndex = 0; voteIndex < votes.length; voteIndex++) {
            await mongooseVoteRepo.update({_id: votes[voteIndex]._id.toString()}, {$set: {sc_vote_exists: true}})
        }

        let res = await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : false});

        console.log(JSON.stringify(res.body));

        await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : true});

        res = await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : true});

        //TODO proper testing
    })
});
