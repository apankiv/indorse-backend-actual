process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const server = require('../../server');
const should = chai.should();
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const updateDownvoteScore = require('../../smart-contracts/services/cron/updateValidatorDownvoteScore');
const mongooseMandrillConfig = require('../../models/services/mongo/mongoose/mongooseMandrillConfig');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseClaimRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const authenticationWrapper = require('../authenticationWrapper');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const testUtils = require('../testUtils');
const ObjectId = require('mongodb').ObjectID;
const settings = require('../../models/settings');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const config = require('config');
const voteTestHelper = require('../votes/voteTesthelper');

chai.use(chaiHttp);

describe.skip('UpdateValidatorDownvoteScore cron job', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;
    beforeEach(async (done) => {
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should update downvote score correctly', async () => {
        let tokenObjClaimUser = {};
        let claimUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(testUtils.generateRandomUser(), tokenObjClaimUser, 'admin');
        let claim = testUtils.generateRndomClaimCreationRequest();
        await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
        await claimCreationWrapper.createClaim(claim, tokenObjClaimUser);
        let claimIds = await mongooseClaimRepo.findByOwnerId(claimUser._id);
        let theClaimId = String(claimIds[0]._id);

        for (let i = 0; i < 9; i++) {
            let user = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(), {});
            await mongooseValidatorRepo.insert({user_id: user._id});
        }

        let validatorToTest = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(), {});
        await mongooseValidatorRepo.insert({user_id: validatorToTest._id});

        for (let j = 0; j < 10; j++) {
            await voteTestHelper.insertVoteWithVoterIdAndClaimId(validatorToTest._id, theClaimId)
        }

        let votes = await mongooseVoteRepo.findAll({voter_id: validatorToTest._id});
        let hiddenVote = votes[0];
        await mongooseVoteRepo.update({_id: hiddenVote._id}, {'hide.isHidden': true});

        let oneDownvote = votes[1];
        await mongooseVoteRepo.update({_id: oneDownvote._id}, {'upvote.count': 1});
        await mongooseVoteRepo.update({_id: oneDownvote._id}, {'downvote.count': 2});

        let secondDownvote = votes[2];
        await mongooseVoteRepo.update({_id: secondDownvote._id}, {'downvote.count': 1});


        await updateDownvoteScore.updateValidatorDownvoteScore();

        let updatedValidator = await mongooseValidatorRepo.findOne({user_id: validatorToTest._id});
        updatedValidator.totalDownvote.should.equal(3);
    });
});