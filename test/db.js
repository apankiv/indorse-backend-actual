var MongoClient = require('mongodb').MongoClient
    , async = require('async')
    , config = require('config')
    , settings = require('../models/settings.js');

var state = {
    db: null
}

exports.connect = (done) => {
    if (state.db) return done()

    var uri = settings.DATABASE_CONNECTION_STRING;

    MongoClient.connect(uri, (err, db) => {
        if (err) return done(err);
        state.db = db;
        done();
    });
};

exports.connectAsync = () => new Promise((resolve, reject) => {
    if (state.db) {
        return resolve();
    }
    return MongoClient.connect(
        settings.DATABASE_CONNECTION_STRING,
        (err, db) => {
            if (err) {
                return reject(err);
            }
            state.db = db;
            return resolve();
        },
    );
});

exports.getDB = () => {
    return state.db
}

exports.drop = (done) => {
    if (!state.db) return done()
    // This is faster then dropping the database
    state.db.collections((err, collections) => {
        async.each(collections, (collection, cb) => {
            if (collection.collectionName.indexOf('system') === 0) {
                return cb()
            }
            collection.remove(cb)
        }, done)
    })
}

exports.dropAsync = () => new Promise((resolve, reject) => {
    if (!state.db) {
        return resolve();
    }
    // This is faster then dropping the database
    return state.db.collections((err, collections) => {
        if (err) {
            return reject(err);
        }
        return async.each(
            collections,
            (collection, cb) => {
                if (collection.collectionName.indexOf('system') === 0) {
                    return cb();
                }
                return collection.remove(cb);
            },
            () => resolve(),
        );
    });
});
