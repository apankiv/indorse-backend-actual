process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const chatbotWrapper = require('../../chatbot/userChatbotCompleteWrapper');
const networkInitialize = require('../../smart-contracts/initializers/networkInitialize');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserAssessmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssessmentRepo');

chai.use(chaiHttp);

describe('admin allowAssignmentForCandidate', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await networkInitialize.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should create a magic link, user should be able to start assessment, admin shuold be able to view', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        let invitedUser = testUtils.generateRandomUser();
        let chatbotSkillName = 'javascript';
        let chatbotUuid = testUtils.generateRandomString(16);
        //Invite user via sc flow
        let tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let createdUser = await mongooseUserRepo.findOneByEmail(adminUser.email);

        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

        let jobTitle = testUtils.generateRandomString(4);
        let jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        //await createAssessmentMagicLinkWrapper.createAssessmentMagicLinkWrapper(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, tokenObj);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        await chatbotWrapper.userChatbotCompleteNoChatbotInitialize(createdUser._id, 70, chatbotUuid);

        let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);

        let allLinks = await mongooseAssessmentMagicRepo.findAll({});
        let createdMagiclink = allLinks[0].publicId;

        let allUserAssessment = await mongooseUserAssessmentRepo.findAll({});
        //let userAss = await mongooseUserAssessmentRepo.findOne({started_by: createdUser._id})
        await mongooseUserAssessmentRepo.update({startedBy: createdUser._id},{status: 'chatbotsPassed'});

        let allowAssessmentMutation = `					 
            mutation (  $publicId: String = "${createdMagiclink}", $candidateId: String = "${String(createdUser._id)}"
                        ) {
                allowAssignmentForCandidate(publicId: $publicId, candidateId: $candidateId
                                          ){
                                          
                                          status
                                          startedAt
                                          user{
                                            name
                                            email
                                          }
                                         
                                          
                                          }
             }
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: allowAssessmentMutation
            });

        let response = res.body.data.allowAssignmentForCandidate;
        response.status.should.equal('assignmentAllowed');
        response.user.email.should.equal(createdUser.email);


    });
});

