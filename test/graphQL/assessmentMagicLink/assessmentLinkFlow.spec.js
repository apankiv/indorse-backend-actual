process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const chatbotWrapper = require('../../chatbot/userChatbotCompleteWrapper');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');


chai.use(chaiHttp);

describe('magiclink e2e flow test', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should create a magic link, user should be able to start assessment, admin shuold be able to view', async () => {


        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        let invitedUser = testUtils.generateRandomUser();
        let chatbotSkillName = 'javascript';
        //Invite user via sc flow
        let tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);
        let adminUserId = await mongooseUserRepo.findOneByEmail(adminUser.email);
        const ACLObject = {
            email: adminUser.email,
            inviteId: testUtils.generateRandomString(),
            acceptedInvite: true,
            userId: adminUserId
        };
        let companyACL = [];
        companyACL.push(ACLObject);

        // update the company acl array
        const dataToSet = {
            $set: {
                acl: companyACL,
            },
        };
        // update that same company with the new array
        await mongooseCompanyRepo.update({pretty_id: createCompanyRequest.pretty_id}, dataToSet);
        let createdUser = await mongooseUserRepo.findOneByEmail(adminUser.email);

        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';
        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

        let jobTitle = testUtils.generateRandomString(4);
        let jobPostLink = testUtils.generateRandomString();

        let returnObj = await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLink(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        let chatbotUuid = returnObj.chatbotUuid;
        let allLinks = await mongooseAssessmentMagicRepo.findAll({});

        let createdMagiclink = allLinks[0].publicId;

        // start assessment
        let startAssessmentMutation = `					 
            mutation (  $publicId: String = "${createdMagiclink}",
                        ) {
                startAssessmentMagicLink(publicId: $publicId
                                          ){
                                          id
                                          title
                                          publicId
                                          chatbotSkills{
                                            _id
                                            name
                                            iconUrl
                                            tags
                                          }
                                          viewerAssessment{
                                            status
                                            startedAt
                                            user{
                                            name
                                            }
                                            userChatbotStatus{
                                                skill{
                                                    name
                                                }
                                                validated
                                                validatedAt
                                            }
                                          }
                                          }
             }
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: startAssessmentMutation
            });

        let startAssessmentResp = res.body.data.startAssessmentMagicLink;
        startAssessmentResp.chatbotSkills.length.should.equal(1);
        startAssessmentResp.title.should.equal(jobTitle);
        should.exist(startAssessmentResp.viewerAssessment.startedAt);
        startAssessmentResp.viewerAssessment.user.name.should.equal(createdUser.name);
        startAssessmentResp.viewerAssessment.status.should.equal('chatbotsPending');
        should.exist(startAssessmentResp.viewerAssessment.startedAt);
        startAssessmentResp.viewerAssessment.userChatbotStatus.length.should.equal(0);


        // take quiz
        await chatbotWrapper.userChatbotCompleteNoChatbotInitialize(createdUser._id, 70, chatbotUuid);
        let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);

        // viwer view
        let viewUserAssessment = `					 
            query (  $publicId: String = "${createdMagiclink}",
                        ) {
                getAssessmentMagicLinkById(publicId: $publicId
                                          ){
                                          id
                                          title
                                          publicId
                                          chatbotSkills{
                                            name
                                          }
                                          viewerAssessment{
                                            status
                                            startedAt
                                            userChatbotStatus{
                                                skill{
                                                    name
                                                }
                                                validated
                                                validatedAt
                                            }
                                          }
                                          }
             }
            `;

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: viewUserAssessment
            });

        let viewerResponseAfterChatbotCompletion = res.body.data.getAssessmentMagicLinkById;
        viewerResponseAfterChatbotCompletion.title.should.equal(jobTitle);
        viewerResponseAfterChatbotCompletion.publicId.should.equal(createdMagiclink);
        viewerResponseAfterChatbotCompletion.chatbotSkills.length.should.equal(1);
        viewerResponseAfterChatbotCompletion.viewerAssessment.status.should.equal('chatbotsPassed');
        viewerResponseAfterChatbotCompletion.viewerAssessment.userChatbotStatus.length.should.equal(1);
        viewerResponseAfterChatbotCompletion.viewerAssessment.userChatbotStatus[0].validated.should.equal(true);
        should.exist(viewerResponseAfterChatbotCompletion.viewerAssessment.userChatbotStatus[0].validatedAt);
        viewerResponseAfterChatbotCompletion.viewerAssessment.userChatbotStatus[0].skill.name.should.equal(chatbotSkillName);


        // Candidate view


        let candidateUserAssesementsQuery = `					 
            query (  $publicId: String = "${createdMagiclink}",
                        ) {
                getAssessmentMagicLinkById(publicId: $publicId
                                          ){
                                              id
                                              title
                                              publicId
                                              chatbotSkills{
                                                name
                                                iconUrl
                                                tags
                                              }
                                              company{
                                                id
                                                prettyId
                                                companyName
                                              }
                                              
                                              allUserAssessments{
                                                status
                                                startedAt
                                                user{
                                                name
                                                email
                                                }
                                            }
                                          }
             }
            `;

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: candidateUserAssesementsQuery
            });

        let candidateViewResponse = res.body.data.getAssessmentMagicLinkById;
        candidateViewResponse.title.should.equal(jobTitle);
        candidateViewResponse.publicId.should.equal(createdMagiclink);
        candidateViewResponse.chatbotSkills.length.should.equal(1);
        candidateViewResponse.company.prettyId.should.equal(createCompanyRequest.pretty_id);
        candidateViewResponse.company.companyName.should.equal(createCompanyRequest.company_name);

        candidateViewResponse.allUserAssessments.length.should.equal(1);
        candidateViewResponse.allUserAssessments[0].status.should.equal('chatbotsPassed');
        candidateViewResponse.allUserAssessments[0].user.name.should.equal(createdUser.name);
        candidateViewResponse.allUserAssessments[0].user.email.should.equal(createdUser.email);

        let mutationRequest = `					 
            query (  $prettyId: String = "${createCompanyRequest.pretty_id}",
                        ) {
                getAllAssessmentMagicLinkByPrettyId(prettyId: $prettyId
                                          ){
                                              id
                                              title
                                              role{
                                                title
                                              }
                                              publicId
                                              createdAt
                                              
                                          }
             }
            `;

       res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: mutationRequest
            });

        let allMagicLinkResp = res.body.data.getAllAssessmentMagicLinkByPrettyId;
        allMagicLinkResp.length.should.equal(1);
        allMagicLinkResp[0].title.should.equal(jobTitle);
        allMagicLinkResp[0].role.title.should.equal(jobRoleObj.title);
        let timePassAfterMagicLinkCreatedInSecond = new Date()/1000 - allMagicLinkResp[0].createdAt;
        (timePassAfterMagicLinkCreatedInSecond<50).should.equal(true);
        (timePassAfterMagicLinkCreatedInSecond>0).should.equal(true);



        mutationRequest = `					 
            mutation (  $publicId: String = "${createdMagiclink}", $candidateId: String = "${String(createdUser._id)}"
                        ) {
                allowAssignmentForCandidate(publicId: $publicId, candidateId: $candidateId
                                          ){
                                            startedAt
                                          }
             }
            `;



        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: mutationRequest
            });

        console.log('a');
        console.log(res.body.data);

    });
});

