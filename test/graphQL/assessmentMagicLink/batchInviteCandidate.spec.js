process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const networkInitialize = require('../../smart-contracts/initializers/networkInitialize');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const chatbotWrapper = require('../../chatbot/userChatbotCompleteWrapper');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');

chai.use(chaiHttp);

var SC;

describe('AssessmentMagicLink.batchInviteCandidate', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await networkInitialize.initialize();
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should batchInviteCandidate by an admin user', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();
        const chatbotSkillName = 'javascript';
        const chatbotUuid = testUtils.generateRandomString(16);
        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(
            adminUser, createCompanyRequest,
            tokenObj, adminUser.username,
        );

        const jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        const jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj);

        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        const allLinks = await mongooseAssessmentMagicRepo.findAll({});
        const createdMagiclink = allLinks[0].publicId;

        const variables = {
            publicId: createdMagiclink,
            inviteEmails: ['test@test.com'],
        };
        const batchInviteCandidatesMutation = `
          mutation batchInviteCandidates($publicId: String!, $inviteEmails: [String!]!){
            batchInviteCandidates(publicId: $publicId, inviteEmails: $inviteEmails) {
              success
              errors {
                email
                index
              }
            }
          }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: batchInviteCandidatesMutation, variables });

        const response = res.body.data.batchInviteCandidates;
        response.success.should.equal(true);
        response.errors.length.should.equal(0);
    });

    it('should not batchInviteCandidate by non admin user', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();
        const chatbotSkillName = 'javascript';
        const chatbotUuid = testUtils.generateRandomString(16);
        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(
            adminUser, createCompanyRequest,
            tokenObj, adminUser.username,
        );

        const jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        const jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj);

        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        const allLinks = await mongooseAssessmentMagicRepo.findAll({});
        const createdMagiclink = allLinks[0].publicId;

        const variables = {
            publicId: createdMagiclink,
            inviteEmails: ['test@test.com'],
        };
        const batchInviteCandidatesMutation = `
          mutation batchInviteCandidates($publicId: String!, $inviteEmails: [String!]!){
            batchInviteCandidates(publicId: $publicId, inviteEmails: $inviteEmails) {
              success
              errors {
                email
                index
              }
            }
          }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${1}`)
            .send({ query: batchInviteCandidatesMutation, variables });
        chai.should().exist(res.body.errors);
    });

    it('should not batchInviteCandidate by an admin user if email is wrong', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();
        const chatbotSkillName = 'javascript';
        const chatbotUuid = testUtils.generateRandomString(16);
        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(
            adminUser, createCompanyRequest,
            tokenObj, adminUser.username,
        );

        const jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        const jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj);

        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        const allLinks = await mongooseAssessmentMagicRepo.findAll({});
        const createdMagiclink = allLinks[0].publicId;

        const variables = {
            publicId: createdMagiclink,
            inviteEmails: ['test@com'],
        };
        const batchInviteCandidatesMutation = `
          mutation batchInviteCandidates($publicId: String!, $inviteEmails: [String!]!){
            batchInviteCandidates(publicId: $publicId, inviteEmails: $inviteEmails) {
              success
              errors {
                email
                index
              }
            }
          }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: batchInviteCandidatesMutation, variables });

        const response = res.body.data.batchInviteCandidates;
        response.success.should.equal(false);
        response.errors.length.should.equal(1);
        const { errors } = response;
        errors[0].email.should.equal(variables.inviteEmails[0]);
        errors[0].index.should.equal(0);
    });
});
