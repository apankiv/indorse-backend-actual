process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');
const server = require('../../../server');

chai.use(chaiHttp);

describe('createAssessmentMagicLink test', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should create magic link for assessment', async () => {


        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        let invitedUser = testUtils.generateRandomUser();

        //Invite user via sc flow
        let tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let skillCategory = 'tech';
        let chatbotSkillName = 'javascript';
        let chatbotUuid = testUtils.generateRandomString(16);
        let chatbotSkillId = await mongooseSkillRepo.insert({
            name: chatbotSkillName,
            category: skillCategory,
            validation: {chatbot: true, chatbot_uuid: chatbotUuid}
        });

        let assignmentSkillName = 'reactJS';
        let assignmentSkillTag = 'front-end';
        let assignmentSkillId = await mongooseSkillRepo.insert({
            name: assignmentSkillName,
            category: skillCategory,
            tags: assignmentSkillTag,
            parents: chatbotSkillId
        });

        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = assignmentSkillTag;
        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

        let jobTitle = testUtils.generateRandomString(4);
        let jobPostLink = testUtils.generateRandomString();

        let skillInput = {
            skillTag : assignmentSkillTag,
            allowedSkillIds: [assignmentSkillId]

        };

        let createInput = {
            title: jobTitle,
            role: jobRoleId,
            jobPostLink: jobPostLink,
            companyPrettyId: createCompanyRequest.pretty_id,
            chatbotSkills: [chatbotSkillName],
            assignmentSkills:[skillInput]
        }


        let mutationRequest = `					 
            mutation createAssessmentMagicLink(  $title: String! ,
                        $role: String!,
                        $jobPostLink: String!,
                        $companyPrettyId: String!, 
                        $chatbotSkills: [String!],
                        $assignmentSkills: [MagicLinkSkillInput!]!  
                        ) {
                createAssessmentMagicLink(title: $title, 
                                          role: $role, 
                                          jobPostLink: $jobPostLink,
                                          companyPrettyId: $companyPrettyId, 
                                          chatbotSkills: $chatbotSkills,
                                          assignmentSkills: $assignmentSkills
                                          )
             }
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: mutationRequest,
                variables: { title: createInput.title, role: createInput.role, jobPostLink: createInput.jobPostLink,
                            companyPrettyId: createInput.companyPrettyId, chatbotSkills: createInput.chatbotSkills, assignmentSkills: createInput.assignmentSkills},
            });

        let allLinks = await mongooseAssessmentMagicRepo.findAll({});

        allLinks.length.should.equal(1);
        let createdLink = allLinks[0];
        createdLink.chatbotSkills.length.should.equal(1);
        adminUser = await mongooseUserRepo.findOneByEmail(adminUser.email);
        createdLink.createdBy.should.equal(String(adminUser._id));
        createdLink.title.should.equal(jobTitle);
        createdLink.jobPostLink.should.equal(jobPostLink);
        createdLink.companyPrettyId.should.equal(createCompanyRequest.pretty_id);

    });




});
