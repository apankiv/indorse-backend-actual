process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server')
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const resolverLoopWrapper = require('../../smart-contracts/cron/resolverLoopWrapper');
const mongooseMagicLink = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const testUtils = require('../../testUtils');

chai.use(chaiHttp);

exports.createSkillsAndAssessmentMagicLinkAndStart = async function createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRole, jobPostLink, pretty_id, chatbotSkillName, assignmentSkillName, tokenObj){
    let returnObj = await createChatbotAndAssignmentSkill(chatbotSkillName, assignmentSkillName);
    await createAssessmentMagicLink(jobTitle, jobRole, jobPostLink, pretty_id, returnObj.chatbotSkillName, returnObj.assignmentSkillTag, returnObj.assignmentSkillId, tokenObj);
    let magicLinks = await mongooseMagicLink.findAll({});
    let magicLinkId = magicLinks[0].publicId;
    await startAssessmentMagicLink(magicLinkId, tokenObj);
    return {chatbotUuid: returnObj.chatbotUuid};
}

exports.createSkillsAndAssessmentMagicLink = async function createSkillsAndAssessmentMagicLink(jobTitle, jobRole, jobPostLink, pretty_id, chatbotSkillName, assignmentSkillName, tokenObj){
    let returnObj = await createChatbotAndAssignmentSkill(chatbotSkillName, assignmentSkillName);
    await createAssessmentMagicLink(jobTitle, jobRole, jobPostLink, pretty_id, returnObj.chatbotSkillName, returnObj.assignmentSkillTag, returnObj.assignmentSkillId, tokenObj);
    return {chatbotUuid: returnObj.chatbotUuid};
}

async function createChatbotAndAssignmentSkill(chatbotSkillName, assignmentSkillName){
    let skillCategory = 'tech';

    if(!chatbotSkillName){
        chatbotSkillName = 'javascript';
    }
    if(!assignmentSkillName){
        assignmentSkillName = 'reactJS'
    }

    let chatbotUuid = testUtils.generateRandomString(16);
    let chatbotSkillId = await mongooseSkillRepo.insert({
        name: chatbotSkillName,
        category: skillCategory,
        validation: {chatbot: true, chatbot_uuid: chatbotUuid}
    });

    let assignmentSkillTag = 'front-end';
    let assignmentSkillId = await mongooseSkillRepo.insert({
        name: assignmentSkillName,
        category: skillCategory,
        tags: assignmentSkillTag,
        parents: chatbotSkillId
    });
    return {chatbotSkillName: chatbotSkillName, chatbotSkillId: chatbotSkillId, assignmentSkillId: assignmentSkillId, assignmentSkillTag: assignmentSkillTag, chatbotUuid: chatbotUuid};
}

async function createAssessmentMagicLink(jobTitle, jobRole, jobPostLink, pretty_id, chatbotSkillName, assignmentSkillTag, assignmentSkillId,  tokenObj) {
    let skillInput = {
        skillTag : assignmentSkillTag,
        allowedSkillIds: [assignmentSkillId]

    };

    let createInput = {
        title: jobTitle,
        role: jobRole,
        jobPostLink: jobPostLink,
        companyPrettyId: pretty_id,
        chatbotSkills: [chatbotSkillName],
        assignmentSkills:[skillInput]
    }


    let mutationRequest = `					 
            mutation createAssessmentMagicLink(  $title: String! ,
                        $role: String!,
                        $jobPostLink: String!,
                        $companyPrettyId: String!, 
                        $chatbotSkills: [String!],
                        $assignmentSkills: [MagicLinkSkillInput!]!  
                        ) {
                createAssessmentMagicLink(title: $title, 
                                          role: $role, 
                                          jobPostLink: $jobPostLink,
                                          companyPrettyId: $companyPrettyId, 
                                          chatbotSkills: $chatbotSkills,
                                          assignmentSkills: $assignmentSkills
                                          )
             }
            `;

    let res = await chai
        .request(server)
        .post('/graphql')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({
            query: mutationRequest,
            variables: { title: createInput.title, role: createInput.role, jobPostLink: createInput.jobPostLink,
                companyPrettyId: createInput.companyPrettyId, chatbotSkills: createInput.chatbotSkills, assignmentSkills: createInput.assignmentSkills},
        });
}

async function startAssessmentMagicLink(magicLinkId, tokenObj){
    let startAssessmentMutation = `					 
            mutation (  $publicId: String = "${magicLinkId}",
                        ) {
                startAssessmentMagicLink(publicId: $publicId
                                          ){
                                          
                                          publicId
                                         
                                          
                                          }
             }
            `;

    let res = await chai
        .request(server)
        .post('/graphql')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({
            query: startAssessmentMutation
        });
}
