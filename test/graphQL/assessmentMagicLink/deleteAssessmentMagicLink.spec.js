process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');

chai.use(chaiHttp);
describe('deleteAssessmentMagicLink', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    // Test case: Assert the expected behaviour
    it('should successfully return expected data @please put details', async (done) => {

        // creating a user
        let user = testUtils.generateRandomUser();
        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');

        const sampleInputVariable = {
            arg1: "",
            arg2 : ""
        };


        const mutationName = 'deleteAssessmentMagicLink';
        const mutationBody = `
            mutation ${mutationName}($arg1: String, $arg2: String) {
                deleteAssessmentMagicLink(arg1: $arg1, arg2: $arg2)
            }
        `;


        let body = { operationName: mutationName, query: mutationBody, variables: { id: sampleInputVariable } };

        try {

            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            createCompanyRequest.visible_to_public = true;
            let invitedUser = testUtils.generateRandomUser();

            //Invite user via sc flow
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);



            let jobRoleObj = testUtils.generateRandomJobRole();
            jobRoleObj.skillTags = 'front-end';
            let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

            let jobTitle = testUtils.generateRandomString(4);
            let jobPostLink = testUtils.generateRandomString();

            await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLink(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, null, null, tokenObj);
            let allLinks = await mongooseAssessmentMagicRepo.findAll({});
            let createdMagiclink = allLinks[0].publicId;
            allLinks[0].deleted.should.equal(false);



            let mutationRequest = `					 
            mutation (  $publicId: String = "${createdMagiclink}",
                        ) {
                deleteAssessmentMagicLink(publicId: $publicId
                                          )
             }
            `;

            let res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({
                    query: mutationRequest
                });

            let deleteResponse = res.body.data.deleteAssessmentMagicLink;


            should.exist(deleteResponse);
            deleteResponse.should.equal(true);

            let deletedMagicLink = await mongooseAssessmentMagicRepo.findOneByPublicId(createdMagiclink);
            deletedMagicLink.deleted.should.equal(true);
            done();


        } catch (error) {
            done(error);
        }
    });


})