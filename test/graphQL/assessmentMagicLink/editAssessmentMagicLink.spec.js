process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const networkInitialize = require('../../smart-contracts/initializers/networkInitialize');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const chatbotWrapper = require('../../chatbot/userChatbotCompleteWrapper');
const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');

chai.use(chaiHttp);

describe('AssessmentMagicLink.editAssessmentMagicLink', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        await networkInitialize.initialize();
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should editAssessmentMagicLink by an admin user', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();
        const chatbotSkillName = 'javascript';
        const chatbotUuid = testUtils.generateRandomString(16);
        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(
            adminUser, createCompanyRequest,
            tokenObj, adminUser.username,
        );

        const jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        const jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj);

        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        const allLinks = await mongooseAssessmentMagicRepo.findAll({});
        const createdMagiclink = allLinks[0].publicId;

        const variables = {
            publicId: createdMagiclink,
            jobPostLink: 'https://indorse.io',
            title: 'New Title',
        };
        const editAssessmentMagicLinkMutation = `
          mutation editAssessmentMagicLink($publicId: String!, $jobPostLink: String, $title: String!){
            editAssessmentMagicLink(publicId: $publicId, jobPostLink: $jobPostLink, title: $title) {
                title
                jobPostLink
            }
          }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: editAssessmentMagicLinkMutation, variables });

        const response = res.body.data.editAssessmentMagicLink;
        response.title.should.equal(variables.title);
        response.jobPostLink.should.equal(variables.jobPostLink);
    });

    it('should not editAssessmentMagicLink by non admin user', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();
        const chatbotSkillName = 'javascript';
        const chatbotUuid = testUtils.generateRandomString(16);
        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(
            adminUser, createCompanyRequest,
            tokenObj, adminUser.username,
        );

        const jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';

        const jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj);

        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await chatbotWrapper.setupChatbotWithSkillName(chatbotSkillName, chatbotUuid);
        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLinkAndStart(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        const allLinks = await mongooseAssessmentMagicRepo.findAll({});
        const createdMagiclink = allLinks[0].publicId;

        const variables = {
            publicId: createdMagiclink,
            jobPostLink: '',
            title: 'Title',
        };
        const editAssessmentMagicLinkMutation = `
          mutation editAssessmentMagicLink($publicId: String!, $jobPostLink: String, $title: String!){
            editAssessmentMagicLink(publicId: $publicId, jobPostLink: $jobPostLink, title: $title) {
                title
                jobPostLink
            }
          }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .send({ query: editAssessmentMagicLinkMutation, variables });

        chai.should().exist(res.body.errors);
    });
});
