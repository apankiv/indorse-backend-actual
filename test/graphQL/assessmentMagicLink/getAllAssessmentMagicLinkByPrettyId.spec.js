process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const expect = chai.expect;
const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');

chai.use(chaiHttp);

describe('getAllAssessmentMagicLinkByPrettyID test', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should get all magic link for company', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        const invitedUser = testUtils.generateRandomUser();

        // Invite user via sc flow
        const tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);



        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';
        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)
        
        const jobTitle = testUtils.generateRandomString(4);
        const jobPostLink = testUtils.generateRandomString();

        await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLink(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, null, null, tokenObj);
        let allLinks = await mongooseAssessmentMagicRepo.findAll({});



        const mutationRequest = `					 
            query (  $prettyId: String = "${createCompanyRequest.pretty_id}",
                        ) {
                getAllAssessmentMagicLinkByPrettyId(prettyId: $prettyId
                                          ){
                                              id
                                              title
                                              role{
                                                title
                                              }
                                              publicId
                                              createdAt
                                              
                                          }
             }
            `;

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${  tokenObj.token}`)
            .send({
                query: mutationRequest,
            });

        const allMagicLinkResp = res.body.data.getAllAssessmentMagicLinkByPrettyId;
        allMagicLinkResp.length.should.equal(1);
        allMagicLinkResp[0].title.should.equal(jobTitle);
        allMagicLinkResp[0].role.title.should.equal(jobRoleObj.title);
        const timePassAfterMagicLinkCreatedInSecond = new Date() / 1000 - allMagicLinkResp[0].createdAt;
        (timePassAfterMagicLinkCreatedInSecond < 50).should.equal(true);
        (timePassAfterMagicLinkCreatedInSecond > 0).should.equal(true);
    });
});

