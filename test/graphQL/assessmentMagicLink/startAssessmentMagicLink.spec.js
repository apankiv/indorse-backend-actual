process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const config = require('config');
const mongooseAssessmentMagicRepo = require('../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const chatbotWrapper = require('../../chatbot/userChatbotCompleteWrapper');

const createAssessmentMagicLinkWrapper = require('./createAssessmentMagicLinkWrapper');
const server = require('../../../server');
const should = chai.should();
const { status: userAssessmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssessment');

chai.use(chaiHttp);

describe('createAssessmentMagicLink test', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });



    it('should create a magic link for assessment without chatbot then start the assignment, then status should be allowedAssignment', async () => {


        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        let invitedUser = testUtils.generateRandomUser();

        //Invite user via sc flow
        let tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let skillCategory = 'tech';
        let chatbotSkillName = 'javascript';
        let chatbotUuid = testUtils.generateRandomString(16);
        let chatbotSkillId = await mongooseSkillRepo.insert({
            name: chatbotSkillName,
            category: skillCategory,
            validation: {chatbot: true, chatbot_uuid: chatbotUuid}
        });

        let assignmentSkillName = 'reactJS';
        let assignmentSkillTag = 'front-end';
        let assignmentSkillId = await mongooseSkillRepo.insert({
            name: assignmentSkillName,
            category: skillCategory,
            tags: assignmentSkillTag,
            parents: chatbotSkillId
        });

        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = assignmentSkillTag;
        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

        let jobTitle = testUtils.generateRandomString(4);
        let jobPostLink = testUtils.generateRandomString();

        let skillInput = {
            skillTag : assignmentSkillTag,
            allowedSkillIds: [assignmentSkillId]

        };

        let createInput = {
            title: jobTitle,
            role: jobRoleId,
            jobPostLink: jobPostLink,
            companyPrettyId: createCompanyRequest.pretty_id,
            assignmentSkills:[skillInput]
        }


        let mutationRequest = `					 
            mutation createAssessmentMagicLink(  $title: String! ,
                        $role: String!,
                        $jobPostLink: String!,
                        $companyPrettyId: String!, 
                        $assignmentSkills: [MagicLinkSkillInput!]!  
                        ) {
                createAssessmentMagicLink(title: $title, 
                                          role: $role, 
                                          jobPostLink: $jobPostLink,
                                          companyPrettyId: $companyPrettyId, 
                                          assignmentSkills: $assignmentSkills
                                          )
             }
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: mutationRequest,
                variables: { title: createInput.title, role: createInput.role, jobPostLink: createInput.jobPostLink,
                    companyPrettyId: createInput.companyPrettyId, assignmentSkills: createInput.assignmentSkills},
            });

        let allLinks = await mongooseAssessmentMagicRepo.findAll({});

        allLinks.length.should.equal(1);
        let createdLink = allLinks[0];
        createdLink.chatbotSkills.length.should.equal(0);
        adminUser = await mongooseUserRepo.findOneByEmail(adminUser.email);
        createdLink.createdBy.should.equal(String(adminUser._id));
        createdLink.title.should.equal(jobTitle);
        createdLink.jobPostLink.should.equal(jobPostLink);
        createdLink.companyPrettyId.should.equal(createCompanyRequest.pretty_id);


        // start assessment
        let startAssessmentMutation = `					 
            mutation (  $publicId: String = "${createdLink.publicId}",
                        ) {
                startAssessmentMagicLink(publicId: $publicId
                                          ){
                                          id
                                          title
                                          publicId
                                          chatbotSkills{
                                            _id
                                            name
                                            iconUrl
                                            tags
                                          }
                                          viewerAssessment{
                                            status
                                            startedAt
                                            user{
                                            name
                                            }
                                            userChatbotStatus{
                                                skill{
                                                    name
                                                }
                                                validated
                                                validatedAt
                                            }
                                          }
                                          }
             }
            `;

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: startAssessmentMutation
            });

        let startAssessmentResp = res.body.data.startAssessmentMagicLink;
        startAssessmentResp.chatbotSkills.length.should.equal(0);
        startAssessmentResp.title.should.equal(jobTitle);
        should.exist(startAssessmentResp.viewerAssessment.startedAt);
        startAssessmentResp.viewerAssessment.status.should.equal(userAssessmentStates.ASSIGNMENTS_VIEWED);

    });


    it('should create a magic link, then tke chatbot user should be able to start assessment', async () => {


        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;
        let invitedUser = testUtils.generateRandomUser();
        let chatbotSkillName = 'javascript';
        //Invite user via sc flow
        let tokenObj = {};
        // await mongooseDesignationRepo.insert({ designation: designationName });
        await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);
        let adminUserId = await mongooseUserRepo.findOneByEmail(adminUser.email);
        const ACLObject = {
            email: adminUser.email,
            inviteId: testUtils.generateRandomString(),
            acceptedInvite: true,
            userId: adminUserId
        };
        let companyACL = [];
        companyACL.push(ACLObject);

        // update the company acl array
        const dataToSet = {
            $set: {
                acl: companyACL,
            },
        };
        // update that same company with the new array
        await mongooseCompanyRepo.update({pretty_id: createCompanyRequest.pretty_id}, dataToSet);
        let createdUser = await mongooseUserRepo.findOneByEmail(adminUser.email);

        let jobRoleObj = testUtils.generateRandomJobRole();
        jobRoleObj.skillTags = 'front-end';
        let jobRoleId = await mongooseJobRoleRepo.insert(jobRoleObj)

        let jobTitle = testUtils.generateRandomString(4);
        let jobPostLink = testUtils.generateRandomString();

        let returnObj = await createAssessmentMagicLinkWrapper.createSkillsAndAssessmentMagicLink(jobTitle, jobRoleId, jobPostLink, createCompanyRequest.pretty_id, chatbotSkillName, null, tokenObj);
        let chatbotUuid = returnObj.chatbotUuid;
        let allLinks = await mongooseAssessmentMagicRepo.findAll({});

        let createdMagiclink = allLinks[0].publicId;


        // take quiz
        await chatbotWrapper.userChatbotCompleteNoChatbotInitialize(createdUser._id, 70, chatbotUuid);
        let userUpdated = await mongooseUserRepo.findOneById(createdUser._id);


        // start assessment
        let startAssessmentMutation = `					 
            mutation (  $publicId: String = "${createdMagiclink}",
                        ) {
                startAssessmentMagicLink(publicId: $publicId
                                          ){
                                          id
                                          title
                                          publicId
                                          chatbotSkills{
                                            _id
                                            name
                                            iconUrl
                                            tags
                                          }
                                          viewerAssessment{
                                            status
                                            startedAt
                                            user{
                                            name
                                            }
                                            userChatbotStatus{
                                                skill{
                                                    name
                                                }
                                                validated
                                                validatedAt
                                            }
                                          }
                                          }
             }
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: startAssessmentMutation
            });

        let startAssessmentResp = res.body.data.startAssessmentMagicLink;
        startAssessmentResp.chatbotSkills.length.should.equal(1);
        startAssessmentResp.title.should.equal(jobTitle);
        should.exist(startAssessmentResp.viewerAssessment.startedAt);
        startAssessmentResp.viewerAssessment.user.name.should.equal(createdUser.name);
        startAssessmentResp.viewerAssessment.status.should.equal('assignmentAllowed');
        should.exist(startAssessmentResp.viewerAssessment.startedAt);
        startAssessmentResp.viewerAssessment.userChatbotStatus.length.should.equal(1);

    });


});

