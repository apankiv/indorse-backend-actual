process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRolesRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');

chai.use(chaiHttp);

describe('createAssignment test', function() {
    beforeEach(done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test createAssignment endpoint', async () => {
        try {
            const skillTag = testUtils.generateRandomString();
            const skill = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag];

            await mongooseSkillRepo.insert(skill);

            const randomJobRole = testUtils.generateRandomJobRole();
            randomJobRole.skillTags = [skillTag];
            const randomJobRoleId = await mongooseJobRolesRepo.insert(
                randomJobRole
            );

            let adminUser = testUtils.generateRandomUser();
            let randomAss = testUtils.generateRandomAssignment();
            randomAss.jobRoleId = randomJobRoleId;


            let mutationRequest = `					 
            mutation CreateAssignment($ass: AssignmentInput!) {
                createAssignment(assignment: $ass) {
                    title
                }
             }
            `;

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                tokenObj,
                'admin'
            );

            let res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({
                    query: mutationRequest,
                    variables: { ass: randomAss },
                });

            res.should.have.status(200);
            let inserted = await mongooseAssignmentRepo.findOne({
                title: randomAss.title,
            });
            should.exist(inserted);
        } catch (err) {
            console.log(err.response.body.errors);
            throw err;
        }
    });
});
