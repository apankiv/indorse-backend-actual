process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const claimCreationWrapper = require('./../../claims/claimCreationWrapper');
const settings = require('../../../models/settings');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const assignmentResultService = require('../../../models/services/claims/assignmentResultService');
const claimTestUtils = require('../../services/claims/claimTestUtils');

chai.use(chaiHttp);

describe('finishAssignment test', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test finish assignment endpoint', async () => {
        try {
            let adminUser = testUtils.generateRandomUser();
            const randomAss = testUtils.generateRandomAssignment();
            const randomRole = testUtils.generateRandomJobRole();
            const roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.job_role = roleid;

            randomAss.duration = -1;
            const id = await mongooseAssignmentRepo.insert(randomAss);
            const tokenObj = {};

            adminUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                tokenObj,
                'admin',
            );

            const mutationRequest = `
                mutation startAssignment($assignmentID: String!) {
                    startAssignment(assignmentID: $assignmentID) {
                        started_on
                    }
                }
            `;

            let res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({
                    query: mutationRequest,
                    variables: { assignmentID: id, proof: 'https://github.com/ensdomains/ens' },
                });

            res.should.have.status(200);

            // add 2 skills and 2 parent skills
            const skillTag1 = 'tag1';
            const skill = {
                name: 'javascript',
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag1];
            skill.validation = {
                aip: true,
            };
            const skillid1 = await mongooseSkillRepo.insert(skill);

            const skillTag2 = 'tag2';
            const skill2 = {
                name: 'python',
                category: testUtils.generateRandomSkill(),
            };
            skill2.tags = [skillTag2];
            skill2.validation = {
                aip: true,
            };
            const skillid2 = await mongooseSkillRepo.insert(skill2);

            const skillTag3 = 'tag3';
            const skill3 = {
                name: 'jschild',
                category: testUtils.generateRandomSkill(),
            };
            skill3.tags = [skillTag3];
            skill3.parents = [skillid1];
            const skillid3 = await mongooseSkillRepo.insert(skill3);

            const skillTag4 = 'tag4';
            const skill4 = {
                name: 'pychild',
                category: testUtils.generateRandomSkill(),
            };
            skill4.tags = [skillTag4];
            skill4.parents = [skillid2];
            const skillid4 = await mongooseSkillRepo.insert(skill4);

            const variables = {
                assignmentID: id,
                form: {
                    proof: 'https://github.com/ensdomains/ens',
                    skills: [
                        {
                            skillTag: 'tag3',
                            skillId: skillid3,
                        },
                        {
                            skillTag: 'tag4',
                            skillId: skillid4,
                        },
                    ],
                },
            };

            // Add skill on user
            const updateObj = [];

            const obj = {
                skill: {
                    name: 'pychild',
                    category: 'tmp',
                    level: 'beginner',
                    _id: skillid4,
                },
                level: 'beginner',
                validations: [
                    {
                        type: 'claim',
                        level: 'beginner',
                        id: 'fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73',
                        validated: true,
                    },
                ],
            };
            updateObj.push(obj);

            await mongooseUserRepo.update(
                { email: adminUser.email },
                { $set: { skills: updateObj } },
            );

            // create 4 users/validators
            const validatorsJs = await claimCreationWrapper.createValidatorSet(
                settings.CLAIM_VALIDATOR_COUNT / 2,
                ['Javascript'],
            );
            const validatorsPython = await claimCreationWrapper.createValidatorSet(
                settings.CLAIM_VALIDATOR_COUNT / 2,
                ['Python'],
            );

            const mutationFinishAssignment = `
                mutation finishAssignment($assignmentID: String!, $form :FinishAssignmentForm! ) {
                    finishAssignment(assignmentID: $assignmentID, form: $form){
                        _id
                        assignment_id
                    }              
                }
            `;

            const res2 = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({ query: mutationFinishAssignment, variables });

            // Approve assignment
            const claim = await mongooseClaimsRepo.findOne({ type: 'ASSIGNMENT', user_assignment_id: res2.body.data.finishAssignment._id });

            await claimTestUtils.approveAndRelease(claim._id);

            const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id);

            for (testedValidator of validatorsJs) {
                const votes = await mongooseVoteRepo.findByVotingRoundIdAndVoter(votingRound._id.toString(), testedValidator.user_id);
                const vote = votes[0];
                res = await chai
                    .request(server)
                    .post(`/votes/${vote._id.toString()}`)
                    .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                    .send({
                        endorse: true,
                        feedback: {
                            quality: 2,
                            designPatterns: 2,
                            completion: 2,
                            testCoverage: 2,
                            readability: 2,
                            extensibility: 2,
                            explanation: 'ble',
                        },
                        skillid: skillid3,
                    });

                const votingToken = res.body.votingToken;

                let updatedVote = await mongooseVoteRepo.findOneById(vote._id);

                updatedVote.endorsed.should.equal(true);
                should.exist(updatedVote.voted_at);
                updatedVote.voting_token.should.equal(votingToken);

                // Simulate vote in blockchain
                await mongooseVoteRepo.update({ _id: vote._id }, { txStatus: 'SUCCESS', sc_vote_exists: true });
                updatedVote = await mongooseVoteRepo.findOneById(vote._id);
            }

            for (testedValidator of validatorsPython) {
                const votes = await mongooseVoteRepo.findByVotingRoundIdAndVoter(votingRound._id.toString(), testedValidator.user_id);
                const vote = votes[0];
                res = await chai
                    .request(server)
                    .post(`/votes/${  vote._id.toString()}`)
                    .set('Authorization', `Bearer ${  testedValidator.tokenObj.token}`)
                    .send({
                        endorse: true,
                        feedback: {
                            quality: 4,
                            designPatterns: 4,
                            gitFlow: 4,
                            completion: 4,
                            testCoverage: 4,
                            readability: 4,
                            extensibility: 4,
                            explanation: 'ble',
                        },
                        skillid: skillid4,
                    });

                // Simulate vote in blockchain
                await mongooseVoteRepo.update({ _id: vote._id }, { txStatus: 'SUCCESS', sc_vote_exists: true });
            }

            const assignmentResult = await assignmentResultService.calculateAssignmentResult(claim);
            await mongooseUserRepo.setAssignmentValidationResult(
                adminUser._id,
                claim._id,
                assignmentResult,
            );
            await mongooseUserAssignmentRepo.updateAssignmentResult(
                claim.user_assignment_id,
                'evaluated',
                assignmentResult,
            );

            // Check case
            // User should have 2 skills
            const resultUser = await mongooseUserRepo.findOneByEmail(adminUser.email);
            for (let i = 0; i < resultUser.skills.length; i += 1) {
                const skill = resultUser.skills[i];
                if (skill.skill._id.toString() === skillid4.toString()) {
                    skill.level.should.equal('expert');
                } else if (skill.skill._id.toString() === skillid3.toString()) {
                    skill.level.should.equal('beginner');
                }
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});
