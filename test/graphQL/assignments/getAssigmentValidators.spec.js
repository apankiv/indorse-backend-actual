process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const claimCreationWrapper  = require('./../../claims/claimCreationWrapper')
const settings = require('../../../models/settings');
const randomValidatorService = require('../../../models/claims/randomValidatorService')
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');

chai.use(chaiHttp);

describe('startAssignment test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
        //done();
    });

    it('should fail to get validator set for assignment', async () => {
        //Total validators = 4
        //1 validator for python &js
        //1 validator for python
        //1 validator for js



        try {
            //add 2 skills and 2 parent skills
            const skillTag1 = "tag1";
            const skill = {
                name: "javascript",
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag1];
            skill.validation = {
                aip: true
            }
            let skillid1 =  await mongooseSkillRepo.insert(skill);


            const skillTag2 = "tag2";
            const skill2 = {
                name: "python",
                category: testUtils.generateRandomSkill(),
            };
            skill2.tags = [skillTag2];
            skill2.validation = {
                aip: true
            }            
            let skillid2 = await mongooseSkillRepo.insert(skill2);

            const skillTag3 = "tag3";
            const skill3 = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill3.tags = [skillTag3];
            skill3.parents = [skillid1]
            let skillid3 = await mongooseSkillRepo.insert(skill3);


            const skillTag4 = "tag4";
            const skill4 = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill4.tags = [skillTag4];
            skill4.parents = [skillid2]
            let skillid4 = await mongooseSkillRepo.insert(skill4);    

            //create 4 users/validators
            let tokenObj1 = {}
            let tokenObj2 = {}
            let tokenObj3 = {}
            let tokenObj4 = {}                        
            let randomUser1 = testUtils.generateRandomUser();
            let randomUser2 = testUtils.generateRandomUser();
            let randomUser3 = testUtils.generateRandomUser();

            let id1 = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser1, tokenObj1, 'full_access');
            let id2 = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser2, tokenObj2, 'full_access');
            let id3 = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser3, tokenObj3, 'full_access');

            
            let randomEthAddress = testUtils.generateRandomEthAddress();
            await mongooseUserRepo.update({ email: randomUser1.email }, { $set: { ethaddress: randomEthAddress } })            
            await mongooseUserRepo.update({ email: randomUser2.email }, { $set: { ethaddress: randomEthAddress } })           
            await mongooseUserRepo.update({ email: randomUser3.email }, { $set: { ethaddress: randomEthAddress } })

            let validator1 = {
                user_id: id1._id.toString(),
                skills: ['Javascript', 'Python']
            };            
            await mongooseValidatorRepo.insert(validator1)
            
            
            let validator2 = {
                user_id: id2._id.toString(),
                skills: ['Javascript']
            };
            await mongooseValidatorRepo.insert(validator2)

            let validator3 = {
                user_id: id3._id.toString(),
                skills: ['Python']
            };
            await mongooseValidatorRepo.insert(validator3)
          

            let skills = [
                {skillTag: "bla",skill : skillid3},
                {skillTag: "bla",skill : skillid4}
            ]

            await randomValidatorService.getAssignmentValidators(skills,"5bc752d62604af0b9cd91e36");

        } catch (err) {
             expect(err).to.exist;
         }
    });   


    it('should get correct validator set for assigment', async () => {
        //Total validators = 4
        //4 validator for python
        //2 validator for js

        try {
            //add 2 skills and 2 parent skills
            const skillTag1 = "tag1";
            const skill = {
                name: "javascript",
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag1];
            skill.validation = {
                aip: true
            }
            let skillid1 = await mongooseSkillRepo.insert(skill);


            const skillTag2 = "tag2";
            const skill2 = {
                name: "python",
                category: testUtils.generateRandomSkill(),
            };
            skill2.tags = [skillTag2];
            skill2.validation = {
                aip: true
            }
            let skillid2 = await mongooseSkillRepo.insert(skill2);

            const skillTag3 = "tag3";
            const skill3 = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill3.tags = [skillTag3];
            skill3.parents = [skillid1]
            let skillid3 = await mongooseSkillRepo.insert(skill3);


            const skillTag4 = "tag4";
            const skill4 = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill4.tags = [skillTag4];
            skill4.parents = [skillid2]
            let skillid4 = await mongooseSkillRepo.insert(skill4);

            //create 4 users/validators
            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT/2, ['Javascript']);
            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Python']);


            let skills = [
                { skillTag: "bla", skill: skillid3 },
                { skillTag: "bla", skill: skillid4 }
            ]
            let valSet = await randomValidatorService.getAssignmentValidators(skills, "5bc752d62604af0b9cd91e36");
            valSet.length.should.equal(6);



        } catch (err) {
            console.log(err)
            throw err;
        }
    });      
});
