process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

chai.use(chaiHttp);

describe('getAssignmentBySkill test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test getAssignmentBySkill endpoint', async () => {

        try {

            let adminUser = testUtils.generateRandomUser();
            let randomAss = testUtils.generateRandomAssignment();
            let randomRole = testUtils.generateRandomJobRole();
            let roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.job_role = roleid;
            let id = await mongooseAssignmentRepo.insert(randomAss);

            let queryRequest = `					 
            query GetAssignment($id: String) {
                getAssignment(id: $id) {
                    title
                }
             }
            `;

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: queryRequest, variables: {id: id}});

            res.should.have.status(200);
            res.body.data.getAssignment.title.should.equal(randomAss.title);
        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});