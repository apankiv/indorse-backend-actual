process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

chai.use(chaiHttp);

describe('Assignments.getAssignments', function() {
    beforeEach(async done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive all assignments', async () => {
        let adminUser = testUtils.generateRandomUser();
        let randomAss = testUtils.generateRandomAssignment();

        let randomRole = testUtils.generateRandomJobRole();
        let roleid = await mongooseJobRoleRepo.insert(randomRole);
        randomAss.job_role = roleid;
        let id = await mongooseAssignmentRepo.insert(randomAss);

        let getAllAssignmentsQuery = `query {
                getAssignments(pageNumber: 1, pageSize: 10) {
                    assignments {
                        title
                    }
                totalAssignments                
                }
            }`;

        let res = await chai
            .request(server)
            .post('/graphql')
            .send({ query: getAllAssignmentsQuery });

        res.body.data.getAssignments.assignments.length.should.equal(1);
    });
});
