process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const { expect } = chai;
const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRolesRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

chai.use(chaiHttp);

describe('getAssignmentsBySkill. test', () => {
    beforeEach(done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test getAssignmentsBySkill endpoint', async () => {
        try {
            const skillTag = testUtils.generateRandomString();
            const skill = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag];

            await mongooseSkillRepo.insert(skill);

            const adminUser = testUtils.generateRandomUser();

            const randomJobRole = testUtils.generateRandomJobRole();
            randomJobRole.skillTags = [skillTag];
            const randomJobRoleId = await mongooseJobRolesRepo.insert(
                randomJobRole
            );

            const randomAss = testUtils.generateRandomAssignment();
            randomAss.jobRole = randomJobRoleId;
            await mongooseAssignmentRepo.insert(randomAss);


            const queryRequest = `				 
            query GetAssignmentsBySkill($skill: String) {
                getAssignmentsBySkill(skill: $skill) {
                    title
                }
             }
            `;

            const tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                tokenObj,
                'admin'
            );

            const res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({
                    query: queryRequest,
                    variables: { skill: skill.name },
                });

            res.should.have.status(200);
            res.body.data.getAssignmentsBySkill.length.should.equal(1);
        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});
