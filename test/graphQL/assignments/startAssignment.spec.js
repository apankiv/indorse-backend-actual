process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
chai.use(chaiHttp);

describe('startAssignment test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test startAssignment endpoint', async () => {

        try {
            let adminUser = testUtils.generateRandomUser();
            let randomAss = testUtils.generateRandomAssignment();
            let randomRole = testUtils.generateRandomJobRole();
            let roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.job_role = roleid;
            

            randomAss.duration = -1;
            let id = await mongooseAssignmentRepo.insert(randomAss);
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let mutationRequest = `					 
            mutation startAssignment($assignmentID: String!) {
                startAssignment(assignmentID: $assignmentID) {
                    started_on
                }
             }
            `;

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest, variables: {assignmentID: id}});

            res.should.have.status(200);

        } catch (err) {
            // console.log(err);
            throw err;
        }
    });
    
    xit('should not be able to startAssignment for anonymous user', async() => {});
});