process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');

chai.use(chaiHttp);

describe('updateAssignment test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test updateAssignment endpoint', async () => {

        try {

            let adminUser = testUtils.generateRandomUser();
            let randomAss = testUtils.generateRandomAssignment();

            let randomRole = testUtils.generateRandomJobRole();
            let roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.jobRoleId = roleid;
            let id = await mongooseAssignmentRepo.insert(randomAss);

            let mutationRequest = `					 
            mutation CreateAssignment($ass: AssignmentInput!) {
                createAssignment(assignment: $ass) {
                    title
                }
             }
            `;

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest, variables: {ass: randomAss}});

            res.should.have.status(200);
            let inserted = await mongooseAssignmentRepo.findOne({title: randomAss.title});
            should.exist(inserted);

            mutationRequest = `					 
            mutation UpdateAssignment($id : String!, $ass: AssignmentInput!) {
                updateAssignment(assignmentID : $id, assignment: $ass) {
                    title
                }
             }
            `;

            randomAss.title = "BLE";

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest, variables: {id: inserted._id.toString(), ass: randomAss}});

            res.should.have.status(200);
            let updated = await mongooseAssignmentRepo.findOne({title: randomAss.title});


            mutationRequest = `					 
            mutation DisableAssignment($id : String!, $status : Boolean) {
                toggleStatusAssignment(assignmentID : $id, disabled : $status) {
                    disabled
                }
             }
            `;

            randomAss.title = "BLE";

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest, variables: {id: inserted._id.toString(), status : true}});

            res.should.have.status(200);
            let disabled = await mongooseAssignmentRepo.findOne({title: randomAss.title});
        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});