process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');


chai.use(chaiHttp);

describe('createBadge test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should fail createBadge when user is not authenticated', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadgeGraphQL();

        let mutationRequest = `					 
            mutation (
                $id: String = "${createBadgeRequest.id}",
                $name: String = "${createBadgeRequest.name}",
                $description: String = "${createBadgeRequest.description}", 
                $logo_data: String = "${createBadgeRequest.logo_data}",
                $extensions_cta_text: String = "${createBadgeRequest.extensions_cta_text}",
                $extensions_cta_link: URL ="${createBadgeRequest.extensions_cta_link}",
                $chatbot_uuid: String = "${createBadgeRequest.chatbot_uuid}") {
					    createBadge(id: $id, 
					                name: $name, 
					                description: $description,
					                logo_data: $logo_data, 
					                extensions_cta_text: $extensions_cta_text, 
					                extensions_cta_link: $extensions_cta_link,
					                chatbot_uuid: $chatbot_uuid){
					        id
					        name
					        description
					    }
					 }
            `;


        let tokenObj = {};

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .send({query: mutationRequest});

        res.body.errors[0].message.should.equal("Must be logged in");

    });



    it('should test createBadge endpoint', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadgeGraphQL();

        let mutationRequest = `					 
            mutation (
                $id: String = "${createBadgeRequest.id}",
                $name: String = "${createBadgeRequest.name}",
                $description: String = "${createBadgeRequest.description}", 
                $logo_data: String = "${createBadgeRequest.logo_data}",
                $extensions_cta_text: String = "${createBadgeRequest.extensions_cta_text}",
                $extensions_cta_link: URL ="${createBadgeRequest.extensions_cta_link}",
                $chatbot_uuid: String = "${createBadgeRequest.chatbot_uuid}") {
					    createBadge(id: $id, 
					                name: $name, 
					                description: $description,
					                logo_data: $logo_data, 
					                extensions_cta_text: $extensions_cta_text, 
					                extensions_cta_link: $extensions_cta_link,
					                chatbot_uuid: $chatbot_uuid){
					        id
					        name
					        description
					        chatbot_uuid
					    }
					 }
            `;



        let tokenObj = {};

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: mutationRequest});

        res.should.have.status(200);

        let badgeFromMongo = res.body.data.createBadge;

        badgeFromMongo.id.should.equal(createBadgeRequest.id);
        badgeFromMongo.name.should.equal(createBadgeRequest.name);
        badgeFromMongo.description.should.equal(createBadgeRequest.description);
        badgeFromMongo.chatbot_uuid.should.equal(createBadgeRequest.chatbot_uuid);

    });


    it('should fail when badgeID is empty string', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadgeGraphQL();

        let mutationRequest = `					 
            mutation (
                $name: String = "",
                $description: String = "${createBadgeRequest.description}", 
                $logo_data: String = "${createBadgeRequest.logo_data}",
                $extensions_cta_text: String = "${createBadgeRequest.extensions_cta_text}",
                $extensions_cta_link: URL ="${createBadgeRequest.extensions_cta_link}",
                $chatbot_uuid: String = "${createBadgeRequest.chatbot_uuid}") {
					    createBadge(id: $id, 
					                name: $name, 
					                description: $description,
					                logo_data: $logo_data, 
					                extensions_cta_text: $extensions_cta_text, 
					                extensions_cta_link: $extensions_cta_link,
					                chatbot_uuid: $chatbot_uuid){
					        id
					        name
					        description
					        chatbot_uuid
					    }
					 }
            `;



        let tokenObj = {};

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest});


        } catch (error) {
            error.message.should.equal("Bad Request");
            error.status.should.equal(400);
        }

    });

    it('should fail when logo_data is missing', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadgeGraphQL();

        let mutationRequest = `					 
            mutation (
                $name: String = "",
                $description: String = "${createBadgeRequest.description}", 
                $extensions_cta_text: String = "${createBadgeRequest.extensions_cta_text}",
                $extensions_cta_link: URL ="${createBadgeRequest.extensions_cta_link}",
                $chatbot_uuid: String = "${createBadgeRequest.chatbot_uuid}") {
					    createBadge(id: $id, 
					                name: $name, 
					                description: $description,
					                extensions_cta_text: $extensions_cta_text, 
					                extensions_cta_link: $extensions_cta_link,
					                chatbot_uuid: $chatbot_uuid){
					        id
					        name
					        description
					        chatbot_uuid
					    }
					 }
            `;



        let tokenObj = {};

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationRequest});

            throw new Error('Expected to fail!');
        } catch (error) {
            error.message.should.equal("Bad Request");
        }

    });

    it('should fail when badgeID is already in use', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadgeGraphQL();

        let mutationRequest = `					 
            mutation (
                $id: String = "${createBadgeRequest.id}",
                $name: String = "${createBadgeRequest.name}",
                $description: String = "${createBadgeRequest.description}", 
                $logo_data: String = "${createBadgeRequest.logo_data}",
                $extensions_cta_text: String = "${createBadgeRequest.extensions_cta_text}",
                $extensions_cta_link: URL ="${createBadgeRequest.extensions_cta_link}",
                $chatbot_uuid: String = "${createBadgeRequest.chatbot_uuid}") {
					    createBadge(id: $id, 
					                name: $name, 
					                description: $description,
					                logo_data: $logo_data, 
					                extensions_cta_text: $extensions_cta_text, 
					                extensions_cta_link: $extensions_cta_link,
					                chatbot_uuid: $chatbot_uuid){
					        id
					        name
					        description
					        chatbot_uuid
					    }
					 }
            `;



        let tokenObj = {};

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: mutationRequest});
        res.should.have.status(200);

        let badgeFromMongo = res.body.data.createBadge;

        badgeFromMongo.id.should.equal(createBadgeRequest.id);
        badgeFromMongo.name.should.equal(createBadgeRequest.name);
        badgeFromMongo.description.should.equal(createBadgeRequest.description);
        badgeFromMongo.chatbot_uuid.should.equal(createBadgeRequest.chatbot_uuid);

        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: mutationRequest});
        res.body.errors[0].message.should.equal("This badge id is already in use");
        should.not.exist(res.body.data.createBadge);
    });
});