process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');
const badgeUtils = require('../../../models/services/badge/badgeUtils')


chai.use(chaiHttp);

describe('get all badges by filter, sort', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive all the badges when no search param is given', async () => {

        let adminUser = testUtils.generateRandomUser();
        let getAllBadgesQuery = `query {
                allBadges {
                    badges{
                        id
                        name
                    }
                    totalBadges                
                }
                }
            `;

        let tokenObj = {};

        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let badgeCount = 10;

        for (let index = 0; index < badgeCount; index++) {
            let randomBadge = testUtils.generateRandomBadge();
            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getAllBadgesQuery});

        res.body.data.allBadges.badges.length.should.equal(badgeCount);
    });

    it('should return the badges with pagination', async () => {

        let adminUser = testUtils.generateRandomUser();


        let getAllBadgesQueryFirstPage = `query {
                allBadges (pageNumber: 1, pageSize: 2) {
                    badges{
                        id
                        name
                    }
                    totalBadges                
                }
                }
            `;

        let getAllBadgesQueryNonExistingPage = `query {
                allBadges (pageNumber: 6, pageSize: 2) {
                    badges{
                        id
                        name
                    }
                    totalBadges                
                }
                }
            `;



        let tokenObj = {};

        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let badgeCount = 10;
        let badgeArray = [];

        for (let index = 0; index < badgeCount; index++) {
            let randomBadge = testUtils.generateRandomBadge();
            randomBadge.badge_name = index+randomBadge.badge_name; // ordering
            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
            badgeArray.push(randomBadge);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getAllBadgesQueryFirstPage});

        res.body.data.allBadges.badges.length.should.equal(2);
        res.body.data.allBadges.totalBadges.should.equal(10);


        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getAllBadgesQueryNonExistingPage});
        res.body.data.allBadges.badges.length.should.equal(0);
        res.body.data.allBadges.totalBadges.should.equal(10);
    });


    it('should sort the badges in ascending by name', async () => {

        let adminUser = testUtils.generateRandomUser();


        let getAllBadgesQuery = `query {
                allBadges (sort: ${badgeUtils.sortParams.ascending.badgeName}) {
                    badges{
                        id
                        name
                    }
                    totalBadges                
                }
                }
            `;



        let tokenObj = {};

        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let badgeCount = 3;
        let badgeArray = [];

        for (let index = 0; index < badgeCount; index++) {
            let randomBadge = testUtils.generateRandomBadge();
            randomBadge.badge_name = index+randomBadge.badge_name; // ordering
            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
            badgeArray.push(randomBadge);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getAllBadgesQuery});

        res.body.data.allBadges.badges.length.should.equal(badgeCount);
        res.body.data.allBadges.badges[0].name.should.equal(badgeArray[0].badge_name);
        res.body.data.allBadges.badges[1].name.should.equal(badgeArray[1].badge_name);
        res.body.data.allBadges.badges[2].name.should.equal(badgeArray[2].badge_name);
    });


    it('should search and return a badge', async () => {

        let adminUser = testUtils.generateRandomUser();
        let randomBadge = testUtils.generateRandomBadge();


        let getAllBadgesQuery = `query {
                allBadges (search: "${randomBadge.pretty_id}") {
                    badges{
                        id
                        name
                    }
                    totalBadges                
                }
                }
            `;

        let tokenObj = {};

        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        await badgeCreationWrapper.createBadge(randomBadge, tokenObj);



        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getAllBadgesQuery});

        res.body.data.allBadges.badges.length.should.equal(1);
        res.body.data.allBadges.badges[0].name.should.equal(randomBadge.badge_name);
    });


});