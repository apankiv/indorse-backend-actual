process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');

chai.use(chaiHttp);

describe('get a specific badge test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test getBadge endpoint', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadge();
        let tokenObj = {};

        let queryString = `query {
                badge(id: "${createBadgeRequest.pretty_id}"){
                    id
                    description
                    extensions_cta_text{
                        text
                    }
                }
                }
            `;


        await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .send({query: queryString});

        should.exist(res.body.data.badge);
        res.body.data.badge.id.should.equal(createBadgeRequest.pretty_id);
        res.body.data.badge.description.should.equal(createBadgeRequest.description);
        res.body.data.badge.extensions_cta_text.text.should.equal(createBadgeRequest.cta_text);
    });

    it('should not return badge for not existing badge', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createBadgeRequest = testUtils.generateRandomBadge();
        let tokenObj = {};

        let queryString = `query {
                badge(id: "${createBadgeRequest.pretty_id}"){
                    id
                    description
                    extensions_cta_text{
                        text
                    }
                }
                }
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .send({query: queryString});
        should.not.exist(res.body.data.badge);
    });

});