process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');

chai.use(chaiHttp);

describe('massAssignBadge test', function () {

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);

    });

    afterEach((done) => {
        console.log('dropping database');
        //networkInterface.close()
        DB.drop(done);
    });

    it('it should assign badges to two users', async () => {

        let adminUser = testUtils.generateRandomUser();
        let user1 = testUtils.generateRandomUser();
        let user2 = testUtils.generateRandomUser();

        let massAssignRequest = {
            emails: [user1.email, user2.email],
            badgeIds: [testUtils.generateRandomString(), testUtils.generateRandomString(), testUtils.generateRandomString()],
            assignType: "ASSIGN"
        };

        let massAssignBadgeQuery = `mutation ($emails: [EmailAddress] = ${JSON.stringify(massAssignRequest.emails)}
                                              $badgeIds: [String] = ${JSON.stringify(massAssignRequest.badgeIds)}) {
                massAssignBadges (emails: $emails, badgeIds: $badgeIds, assignType: ASSIGN )
                }
            `;

        let tokenObj = {};


        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        await authenticationWrapper.signupVerifyAuthenticate(user1, {});
        await authenticationWrapper.signupVerifyAuthenticate(user2, {});

        for (let badgeId of massAssignRequest.badgeIds) {
            let randomBadge = testUtils.generateRandomBadge();
            randomBadge.pretty_id = badgeId;
            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: massAssignBadgeQuery});

        res.body.data.massAssignBadges.should.equal(true);


    });

});
