process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const timestampService = require('../../../models/services/common/timestampService')
const mongooseRewardsRepo = require('../../../models/services/mongo/mongoose/mongooseRewardsRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const eventUtils = require('../../../models/services/common/eventUtils')
const server = require('../../../server');
const settings = require('../../../models/settings');
const {tiers} = require('../../../models/services/mongo/mongoose/schemas/validator');
const approveClaim = require('../../../models/claims/approveClaimService');
chai.use(chaiHttp);
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

describe('approveClaim', function () {
    this.timeout(config.get('test.timeout'));


    beforeEach(async () => {
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });


    it('it should approve a claim', async () => {

        let claimCreateReq = testUtils.generateRndomClaimCreationRequest();
        let tokenObj = {};
        let user = testUtils.generateRandomUser();

        let createdUser =  await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj,'admin');

        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);

        let res = await chai.request(server)
            .post('/claims')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send(claimCreateReq);

        let claim = await mongooseClaimRepo.findOne({ownerid: createdUser._id});
        let claimId = claim._id.toString();
        const mutationRequest = `mutation approveClaim ($claimId: String!, 
                                                        $tiers: [TierValidatorInput!]!
                                                        ){
            approveClaim(claimId: $claimId, tiers: $tiers){
                approveResult
                companyName
            }
                            
        }`;


        let tiersInput = [{
            tier: tiers.TIER1,
            noOfValidator: 1
        }, {
            tier: tiers.TIER2,
            noOfValidator: 1
        },{
            tier: tiers.TIER3,
            noOfValidator: 1
        }];

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: mutationRequest,
                variables: {claimId: claimId, tiers: tiersInput}
            });

        let result = res.body.data.approveClaim;
        console.log(result);
        claim = await mongooseClaimRepo.findOne({ownerid: createdUser._id});
        await approveClaim.approveClaim(claim);
        //claimCreateReq.force = true;


    })

});
