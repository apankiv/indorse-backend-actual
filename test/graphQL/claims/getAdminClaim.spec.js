process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const createClaimWrapper = require('../../claims/claimCreationWrapper');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const config = require('config');
const claimTestHelper = require('./testHelper');
const settings = require('../../../models/settings');

chai.use(chaiHttp);
describe('adminClaim query', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should return admin claim details ', async (done) => {

        // creating a user
        let user = testUtils.generateRandomUser();
        let tokenObj = {};

        let claimCreationRequest = testUtils.generateRndomClaimCreationRequest();
        let validators = await createClaimWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await createClaimWrapper.authenticateAndCreateClaim(user, claimCreationRequest, tokenObj); // create one claim

        let createdClaim = await mongooseClaimRepo.findAll({});
        let claimId = createdClaim[0]._id;


        const gqlQuery = `
            query {
                getAdminClaim(claimId: "${claimId}"){
                    noOfValidator
                    tierValidatorDetails{
                        tier
                        minReward
                        maxReward
                    }
                }
            }
        `;


        try {

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: gqlQuery});

            let responseData = res.body.data.getAdminClaim;
            console.log(JSON.stringify(responseData));

            done();


        } catch (error) {
            done(error);
        }
    });

})