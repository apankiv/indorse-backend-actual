process.env.NODE_ENV = 'test';
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const config = require('config');
const claimTestHelper = require('./testHelper');
const authenticationWrapper = require('../../authenticationWrapper');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const testUtils = require('../../testUtils');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongoose = require('mongoose');

const { expect } = chai;
chai.use(chaiHttp);

describe('getAdminClaims', function test() {
    this.timeout(config.get('test.timeout'));
    const tokenObj = {};
    before(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
        await contractSettingInitializer.initialize();
        await claimTestHelper.createClaims(12);
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            testUtils.generateRandomUser(),
            tokenObj,
            'admin',
        );
    });

    after(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should return 200 with valid response body', async () => {
        const query = `query {
            getAdminClaims(pageSize: 4) {
                claims {
                    _id
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        expect(res.status).to.equal(200, 'getAdminClaims query should return 200 status code');
        expect(res.body).to.be.an('object', 'body should exist');
        expect(res.body.data).to.be.an('object', 'body.data should exist');
        expect(res.body.data.getAdminClaims).to.be.an('object', 'getAdminClaims should exist');
    });

    it('should return 4 claims as per pageSize param', async () => {
        const query = `query {
            getAdminClaims(pageSize: 4) {
                claims {
                    _id
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        expect(res.body.data.getAdminClaims.claims)
            .to.have.lengthOf(4, 'four claims should be returned');
    });

    it('should return correct counts without search param', async () => {
        const query = `query {
            getAdminClaims {
                totalClaims
                matchingClaims
                claims {
                    _id
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        expect(res.body.data.getAdminClaims.totalClaims)
            .to.equal(12, 'totalClaims should be equal to 12');
        expect(res.body.data.getAdminClaims.matchingClaims)
            .to.equal(12, 'matchingClaims should be equal to 12');
    });

    it('should return correct counts with search param', async () => {
        const query = `query {
            getAdminClaims (searchStr: "Javascript") {
                totalClaims
                matchingClaims
                claims {
                    _id
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        expect(res.body.data.getAdminClaims.totalClaims)
            .to.equal(12, 'totalClaims should be equal to 12');
        expect(res.body.data.getAdminClaims.matchingClaims)
            .to.equal(3, 'matchingClaims should be equal to 3');
        expect(res.body.data.getAdminClaims.claims)
            .to.have.lengthOf(3, '3 claims should be there in body');
    });

    it('should sort the claims in asc sorting order based on sortParam(title)', async () => {
        const query = `query {
            getAdminClaims (sortParam: title, sortDir: asc, pageSize: 12) {
                totalClaims
                matchingClaims
                claims {
                    title
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        const claimsArray = res.body.data.getAdminClaims.claims;
        const sortedArray = JSON.parse(JSON.stringify(res.body.data.getAdminClaims.claims));
        sortedArray.sort((a, b) => {
            if (a.title > b.title) {
                return 1;
            } else if (a.title < b.title) {
                return -1;
            }
            return 0;
        });
        claimsArray.forEach((claim, index) => {
            expect(claim.title)
                .to.equal(sortedArray[index].title, 'claims should be sorted on title in asc order');
        });
    });

    it('should sort the claims in desc sorting order based on sortParam(title)', async () => {
        const query = `query {
            getAdminClaims (sortParam: title, sortDir: desc, pageSize: 12) {
                totalClaims
                matchingClaims
                claims {
                    title
                }
            }
        }`;
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        const claimsArray = res.body.data.getAdminClaims.claims;
        const sortedArray = JSON.parse(JSON.stringify(res.body.data.getAdminClaims.claims));
        sortedArray.sort((a, b) => {
            if (a.title > b.title) {
                return -1;
            } else if (a.title < b.title) {
                return 1;
            }
            return 0;
        });
        claimsArray.forEach((claim, index) => {
            expect(claim.title)
                .to.equal(sortedArray[index].title, 'claims should be sorted on title in desc order');
        });
    });

    it('should sort the claims in asc sorting order based on sortParam(created_at)', async () => {
        const query = `query {
            getAdminClaims (sortParam: created_at, sortDir: asc, pageSize: 12) {
                totalClaims
                matchingClaims
                claims {
                    _id
                    title
                    created_at
                }
            }
        }`;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        // randomize created_at for claims
        await Promise.all(res.body.data.getAdminClaims.claims.map(claim => mongooseClaimRepo.update(
            { _id: new mongoose.mongo.ObjectId(claim._id) },
            { created_at: testUtils.getNearByRandomTs() },
        )));

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });

        const sortedArray = JSON.parse(JSON.stringify(res.body.data.getAdminClaims.claims));
        sortedArray.sort((a, b) => {
            if (a.created_at > b.created_at) {
                return 1;
            } else if (a.created_at < b.created_at) {
                return -1;
            }
            return 0;
        });
        res.body.data.getAdminClaims.claims.forEach((claim, index) => {
            expect(claim.created_at)
                .to.equal(sortedArray[index].created_at, 'claims should be sorted on created_at in asc order');
        });
    });

    it('should sort the claims in desc sorting order based on sortParam(created_at)', async () => {
        const query = `query {
            getAdminClaims (sortParam: created_at, sortDir: desc, pageSize: 12) {
                totalClaims
                matchingClaims
                claims {
                    _id
                    title
                    created_at
                }
            }
        }`;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        // randomize created_at for claims
        await Promise.all(res.body.data.getAdminClaims.claims.map(claim => mongooseClaimRepo.update(
            { _id: new mongoose.mongo.ObjectId(claim._id) },
            { created_at: testUtils.getNearByRandomTs() },
        )));

        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });

        const sortedArray = JSON.parse(JSON.stringify(res.body.data.getAdminClaims.claims));
        sortedArray.sort((a, b) => {
            if (a.created_at < b.created_at) {
                return 1;
            } else if (a.created_at > b.created_at) {
                return -1;
            }
            return 0;
        });
        res.body.data.getAdminClaims.claims.forEach((claim, index) => {
            expect(claim.created_at)
                .to.equal(sortedArray[index].created_at, 'claims should be sorted on created_at in desc order');
        });
    });

    it('should paginate the claims correctly', async () => {
        const query = `query GetAdminClaimsAlias ($pageNumber: Int, $pageSize: Int) {
            getAdminClaims(pageSize: $pageSize, pageNumber: $pageNumber) {
                totalClaims
                matchingClaims
                claims {
                    _id
                    title
                }
            }
        }`;
        const pageSize = 3;
        const totalPagesToFetch = 6;
        const pageFetchPromises = [];
        for (let i = 1; i <= totalPagesToFetch; i += 1) {
            pageFetchPromises.push(chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({ query, variables: { pageNumber: i, pageSize } }));
        }
        const pages = await Promise.all(pageFetchPromises);
        /**
         * There are 12 claims, so first four pages should have 3 claims
         * when pageSize is 3
         */
        const firstFourPages = pages.splice(0, 4);
        firstFourPages.forEach((page) => {
            expect(page.body.data.getAdminClaims.claims)
                .to.have.lengthOf(3, 'first four pages should have length 3');
        });


        /**
         * fifth, sixth and subsequent pages should be empty
         */
        pages.forEach((page) => {
            expect(page.body.data.getAdminClaims.claims)
                .to.have.lengthOf(0, 'fifth and sixth pages should be empty');
        });


        /**
         * Items should not be repeated in claims array during pagination
         * Each page should have different set of claims
         */
        const claimIdMap = {};
        firstFourPages.forEach((page) => {
            page.body.data.getAdminClaims.claims.forEach((claim) => {
                expect(!!claimIdMap[claim._id]).to.equal(false, 'claims should not repeat in pages');
                claimIdMap[claim._id] = true;
            });
        });
    });

    it('should return votes property', async () => {
        const query = `query {
            getAdminClaims(pageSize: 4) {
                claims {
                    _id
                    votes {
                        invited
                        registered
                        indorsed
                        flagged
                    }
                }
            }
        }`;
        const votesCounters = ['invited', 'registered', 'indorsed', 'flagged'];
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query });
        res.body.data.getAdminClaims.claims
            .forEach((claim) => {
                expect(claim.votes)
                    .to.be.an('object').that.has.all.keys(...votesCounters);
            });
    });

    it('search should work on MongoId', async () => {
        const query = `query GetAdminClaimsAlias ($searchStr: String) {
            getAdminClaims(searchStr: $searchStr) {
                totalClaims
                matchingClaims
                claims {
                    _id
                    title
                }
            }
        }`;
        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query, variables: { searchStr: '' } });
        const [claim] = res.body.data.getAdminClaims.claims;

        /**
         * Search for the claim obtained with _id(MongoId)
         */
        res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query, variables: { searchStr: claim._id } });
        expect(res.body.data.getAdminClaims.claims)
            .to.have.lengthOf(1, 'Only one claim should be returned based on MongoId');
        expect(res.body.data.getAdminClaims.claims[0]._id)
            .to.equal(claim._id, 'Returned claim should have correct MongoId');
    });
});
