process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

chai.use(chaiHttp);

describe('getReleaseDetails', function test() {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should return queue details', async () => {

        const request = `
            query {
                getReleaseDetails(void: true) {
                    nextDate
                    cronString
                    queueSize
                }
            }
        `;

        let res = await chai.request(server)
            .post('/graphql')
            .send({
                query: request
            });

        res.status.should.equal(200);
    });
});
