process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mandrilRepo = require('../../../models/services/mongo/mongoose/mongooseMandrillConfig');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseRewardsRepo = require('../../../models/services/mongo/mongoose/mongooseRewardsRepo');

chai.use(chaiHttp);

describe('payoutClaims GraphQL', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach(async (done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    

    it('it should return payout data', async () => {

        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        let requestTrue = `					 
            mutation (
                $pay: Boolean = true) {
					    payoutClaims(pay: $pay
					                )
					 }
            `;
        let requestFalse = `					 
            mutation (
                $pay: Boolean = false) {
					    payoutClaims(pay: $pay
					                )
					 }
            `;

        await mandrilRepo.insert(mandrilToRemove);

        let mandrillTemplate = await mandrilRepo.findOneByAction(mandrilToRemove.action);
        console.log('here ' + JSON.stringify(mandrillTemplate));
        let validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj); // create one claim
        await mongooseUserRepo.update({email:user.email}, {isSuperAdmin:true});
        let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim[0]._id);
        let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

        for (let voteIndex = 0; voteIndex < votes.length; voteIndex++) {
            await mongooseVoteRepo.update({_id: votes[voteIndex]._id.toString()}, {$set: {sc_vote_exists: true}})
        } // after this loop, there will be one vote that needs to be paid

        //Add reputation payouts.
        let rewardData = {
            reward: 20,
            calculation_timestamp: 0,
            paid: false
        }

        for(validator of validators){
            rewardData.user_id =  validator.user_id,
            await mongooseRewardsRepo.insert(rewardData);
        }

        //Add user as well to rewards (non existing row for reputation case)
        rewardData.user_id = createdUser._id.toString()
        await mongooseRewardsRepo.insert(rewardData);

        votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);
        try{
            let firstRes = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: requestFalse});
            firstRes.body.data.payoutClaims.should.equal(true)
            console.log('1 '+ JSON.stringify(firstRes.body));
            for (validator of validators) {
                let rewardRow = await mongooseRewardsRepo.findOne({ user_id: validator.user_id })
                rewardRow.paid.should.equal(false);
            }

            let secondRes = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: requestTrue});
            console.log('2 '+JSON.stringify(secondRes.body));
            secondRes.body.data.payoutClaims.should.equal(true)

            let thirdRes = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: requestTrue});

            thirdRes.body.data.payoutClaims.should.equal(true)           

            console.log('3 ' + JSON.stringify(thirdRes.body));
        }catch(e){
            console.log('e ' + JSON.stringify(e))
            throw e;
        }
        /*
        TODO : Enable when rewards based on reputation are enabled again
        for (validator of validators){
            let rewardRow = await mongooseRewardsRepo.findOne({ user_id: validator.user_id }) 
            rewardRow.paid.should.equal(true);
        }
        let rewardRow = await mongooseRewardsRepo.findOne({ user_id: createdUser._id.toString() }) 
        rewardRow.paid.should.equal(true);
        */
    })

});
