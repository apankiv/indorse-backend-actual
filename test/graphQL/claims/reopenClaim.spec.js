process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const testUtils = require('../../testUtils');

const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const checkClaimsJob = require('../../../smart-contracts/services/cron/checkClaimsJob');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

chai.use(chaiHttp);

describe('reopenClaim', function test() {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should reopen a claim', async () => {
        const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        const user = testUtils.generateRandomUser();

        const tokenObj = {};
        const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        const createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj); // create one claim
        await mongooseUserRepo.update({ email: user.email }, { role: 'admin' });
        const createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        await mongooseVotingRoundRepo.findOneByClaimID(createdClaim[0]._id);
        await mongooseClaimsRepo.update({ _id: createdClaim[0]._id.toString() }, { $set: { sc_deadline: 66666666666 } });
        await mongooseVotingRoundRepo.update({ claim_id: createdClaim[0]._id }, { $set: { end_voting: 0 } });

        const request = `
            mutation ($id: String!) {
                reopenClaim(claimId: $id)
            }
        `;

        await checkClaimsJob.processOutstandingClaims();

        const testedValidator = validators[0];
        const votedValidator = validators[1];
        await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
        let votesToBeCasted = await mongooseVoteRepo.findByVoterId(votedValidator.user_id);

        const voteToBeCasted = votesToBeCasted[0];
        await mongooseVoteRepo.update({ _id: String(voteToBeCasted._id) }, { sc_vote_exists: true });

        votesToBeCasted = await mongooseVoteRepo.findByVoterId(votedValidator.user_id);
        votesToBeCasted[0].isVotingRoundOn.should.equal(false);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({
                query: request,
                variables: {
                    id: createdClaim[0]._id.toString(),
                },
            });

        const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
        const vote = votes[0];
        vote.isVotingRoundOn.should.equal(true);

        res = await chai.request(server)
            .post(`/votes/${vote._id.toString()}`)
            .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
            .send({
                endorse: true,
                feedback: {
                    quality: 1,
                    designPatterns: 1,
                    gitFlow: 1,
                    explanation: 'ble',
                    testCoverage: 1,
                    readability: 1,
                    extensibility: 1,
                },
            });

        res.status.should.equal(200);
    });
});
