process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const createClaimWrapper = require('../../claims/claimCreationWrapper');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const config = require('config');

chai.use(chaiHttp);
describe('setTags', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    // Test case: Assert the expected behaviour
    it('should successfully update tags of a claim and return true', async (done) => {

        // creating a user
        let user = testUtils.generateRandomUser();
        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');
        let v1Tag = ["v1-naive"];

        let claimCreationRequest = testUtils.generateRndomClaimCreationRequest();
        await createClaimWrapper.createValidatorSet(5, ['Javascript']);
        await createClaimWrapper.createClaim(claimCreationRequest,tokenObj);
        let createdClaim = await mongooseClaimRepo.findOne({title: claimCreationRequest.title});
        let claimId = createdClaim._id;
        const mutationName = 'claims_setTags';
        const mutationBody = `
            mutation ($claimId: String = "${claimId}" $tags : [String] = ${JSON.stringify(v1Tag)}){
                claims_setTags(claimId: $claimId, tags: $tags)
            }
        `;

        let body = { operationName: mutationName, mutation: mutationBody  };

        try {

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationBody});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.claims_setTags.should.equal(true);
            let claimTest = await mongooseClaimRepo.findOneById(claimId);
            claimTest.tags[0].should.equal(v1Tag[0]);
            done();


        } catch (error) {
            done(error);
        }
    });

    it('should successfully update tags of two requests and return true', async (done) => {

        // creating a user
        let user = testUtils.generateRandomUser();
        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'admin');

        let claimCreationRequest = testUtils.generateRndomClaimCreationRequest();
        await createClaimWrapper.createValidatorSet(5, ['Javascript']);
        await createClaimWrapper.createClaim(claimCreationRequest,tokenObj);
        let createdClaim = await mongooseClaimRepo.findOne({title: claimCreationRequest.title});
        let claimId = createdClaim._id;
        console.log('claim id ' + claimId)
        let v1Tag = ["v1-naive"];
        let v2Tag = ["v1-naive","v2-merkle"];

        const mutationName = 'claims_setTags';
        const mutationBodyFirst = `
            mutation ($claimId: String = "${claimId}" $tags : [String] = ${JSON.stringify(v1Tag)}){
                claims_setTags(claimId: $claimId, tags: $tags)
            }
        `;

        const mutationBodySecond = `
            mutation ($claimId: String = "${claimId}" $tags : [String] = ${JSON.stringify(v2Tag)}){
                claims_setTags(claimId: $claimId, tags: $tags)
            }
        `;

        try {

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationBodyFirst});
            let responseData = res.body.data;

            should.exist(responseData);
            let claimTest = await mongooseClaimRepo.findOneById(claimId);
            claimTest.tags[0].should.equal(v1Tag[0]);

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: mutationBodySecond});

            claimTest = await mongooseClaimRepo.findOneById(claimId);
            claimTest.tags[0].should.equal(v2Tag[0]);
            claimTest.tags[1].should.equal(v2Tag[1]);
            done();


        } catch (error) {
            done(error);
        }
    });

})