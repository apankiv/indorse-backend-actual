const testUtils = require('../../testUtils');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const ValidatorTestHelper = require('../../validators/testHelper');

class TestHelper {
    static async createClaims(numClaims = 10) {
        const skills = [{ name: 'Javascript' }, { name: 'Python' }, { name: 'Ruby' }, { name: 'Golang' }];
        await ValidatorTestHelper.createValidators(skills, 12);
        const claimCreationPromises = [];
        for (let i = 0; i < numClaims; i += 1) {
            const skill = skills[i % skills.length].name;
            claimCreationPromises.push(TestHelper.createOneRandomClaim(skill));
        }
        return await Promise.all(claimCreationPromises);
    }

    static createOneRandomClaim(title) {
        return claimCreationWrapper.authenticateAndCreateClaim(
            testUtils.generateRandomUser(),
            testUtils.generateRndomClaimCreationRequest(title),
            {},
        );
    }
}

module.exports = TestHelper;
