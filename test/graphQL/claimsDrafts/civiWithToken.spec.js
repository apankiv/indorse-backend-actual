process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../server');
const should = chai.should();
const DB = require('../../db');
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const testUtils = require('../../testUtils');
const sinon = require('sinon')
const civicUtils = require("../../../models/services/social/civicUtils");
const createClaimDraftWrapper = require('./createClaimDraftWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');

chai.use(chaiHttp);

describe('civiWithToken.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should be able to sign up with Civic on Indorse platform', async () => {
        let claimDraft = await createClaimDraftWrapper.createClaimDraftNonExistingUser();

        let civicEmail = claimDraft.email;

        let authSignup = {
            civic: {
                id_token: 'tokenCivic'
            }
        };

        let civicUserData = [
            {
                name: 'email',
                value: civicEmail
            },
            {
                name: 'phone',
                value: '+44 223001202'
            }
        ]

        let civicUid = 'civicuid';

        let stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData() {
            return [civicEmail, civicUid, civicUserData];
        });

        let signupToken;
        try {
            await chai.request(server)
                .post('/auth/civic')
                .send(authSignup);

            throw new Error("'Expected to fail");
        } catch (error) {
            error.status.should.equal(404);
            signupToken = error.response.body.token;
        }

        let userSignup = {
            username: 'tester',
            name: 'bob',
            civic: {
                signupToken: signupToken
            },
            claimToken : claimDraft.token
        };

        let res = await chai.request(server)
            .post('/signup/civic')
            .send(userSignup);

        res.should.have.status(200);

        let mongoDoc = await mongoUserRepo.findOne({'email': civicEmail});
        mongoDoc.email.should.equal(civicEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.civic_uid.should.equal(civicUid);
        mongoDoc.civicParams.length.should.equal(2);

        await chai.request(server)
            .post('/auth/civic')
            .send(authSignup)

        res.should.have.status(200);
        stubCivic.restore();

        let finalizedDraft = await mongooseClaimDraftsRepo.findOneByEmail(civicEmail);
        finalizedDraft.finalized.should.equal(true);
    });
});
