process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo')
const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const settings = require('../../../models/settings');

chai.use(chaiHttp);

describe('createClaimDraft test', function () {
    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should create draft for existing user', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createDraftRequest = testUtils.generateRandomClaimDraftCreationRequest();
        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, [createDraftRequest.title.charAt(0).toUpperCase() +
        createDraftRequest.title.substr(1)]);

        let mutationRequest = `                  
            mutation {
                  createClaimDraft(email: "${createdUser.email}",title:"${createDraftRequest.title}",desc:"${createDraftRequest.desc}",proof:"${createDraftRequest.proof}"){
                    userExists
                  }
                }
            `;

        let res = await chai.request(server)
            .post('/graphql')
            .send({query: mutationRequest});
        res.body.data.createClaimDraft.userExists.should.equal(true);

        let createdDraft = await mongooseClaimDraftsRepo.findOneByEmail(createdUser.email);



        should.exist(createdDraft);
        should.exist(createdDraft.token);

        let mutationRequest2 = `					 
            mutation (
                $token: String= "${createdDraft.token}"){
					    submitClaimDraftToken(token: $token){
                            email
		                    draftFinalized
                            userExists
					    }
					 }
            `;

        res = await chai.request(server)
            .post('/graphql')
            .send({query: mutationRequest2});

        res.body.data.submitClaimDraftToken.email.should.equal(adminUser.email);
        res.body.data.submitClaimDraftToken.draftFinalized.should.equal(true);
    });
});
