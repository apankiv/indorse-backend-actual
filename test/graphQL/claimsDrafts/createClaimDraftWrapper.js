const chai = require('chai');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const settings = require('../../../models/settings');

exports.createClaimDraftNonExistingUser = async function createClaimDraftNonExistingUser() {

    let createDraftRequest = testUtils.generateRandomClaimDraftCreationRequest();
    await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, [createDraftRequest.title.charAt(0).toUpperCase() +
    createDraftRequest.title.substr(1)]);
    let randomEmail = testUtils.generateRandomEmail();

    let mutationRequest = `                  
            mutation {
                  createClaimDraft(email: "${randomEmail}",title:"${createDraftRequest.title}",desc:"${createDraftRequest.desc}",proof:"${createDraftRequest.proof}"){
                    userExists
                  }
                }
            `;

    await chai.request(server)
        .post('/graphql')
        .send({query: mutationRequest});

    let createdDraft = await mongooseClaimDraftsRepo.findOneByEmail(randomEmail);

    should.exist(createdDraft);
    should.exist(createdDraft.token);
    
    let skills_array = Array(createDraftRequest.title.charAt(0).toUpperCase() + createDraftRequest.title.substr(1));

    //Insert test validators for this skill
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    
    return createdDraft;
};
