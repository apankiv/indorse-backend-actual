process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../server');
const should = chai.should();
const DB = require('../../db');
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const testUtils = require('../../testUtils');
const sinon = require('sinon')
const createClaimDraftWrapper = require('./createClaimDraftWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const gUtils = require("../../../models/services/social/googleService");

chai.use(chaiHttp)
// actual token
const IDTokenA = 'realtoken';


// all the test cases shuold skip the validation part.
describe('googleSignupWithToken.spec.js', function () {

    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should signup a user', async () => {
        let claimDraft = await createClaimDraftWrapper.createClaimDraftNonExistingUser();

        let testEmail = claimDraft.email;

        let googleUid = 'abc';

        let stub = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
            return [testEmail, googleUid]
        });

        let user_signup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: IDTokenA
            },
            claimToken : claimDraft.token
        };

        let res = await chai.request(server)
            .post('/signup/google')
            .send(user_signup);

        res.should.have.status(200);

        let finalizedDraft = await mongooseClaimDraftsRepo.findOneByEmail(testEmail);
        finalizedDraft.finalized.should.equal(true);

        stub.restore();
    });
});
