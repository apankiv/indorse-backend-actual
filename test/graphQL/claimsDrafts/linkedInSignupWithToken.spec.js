process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../server');
const should = chai.should();
const DB = require('../../db');
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const testUtils = require('../../testUtils');
const sinon = require('sinon')
const createClaimDraftWrapper = require('./createClaimDraftWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const gUtils = require("../../../models/services/social/googleService");
const request = require('request')
const linkedInUtils = require("../../../models/services/social/linkedInService");
const linkedInSignup = require("../../../models/users/auth/social/linkedInSignup");
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp)

describe('linkedInSignupWithToken.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should signup a user with token', async () => {
        let claimDraft = await createClaimDraftWrapper.createClaimDraftNonExistingUser();

        let linkedInUid = 'abcdefg'
        let name = 'firstname lastname'
        let username = 'something'
        let stubGetSocialParams;
        let testLinkedInParams = [
            {
                name: 'num-connections',
                value: 300
            },
            {
                name: 'num-connections-capped',
                value: true
            },
            {
                name: 'picture-url',
                value: true
            }
        ]

        let email  = claimDraft.email;

        let userSignupStep1 = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };


        let res;
        let stubGetAccessToken, stubGetUserInfo;
        let signupToken;
        stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns(testLinkedInParams);
        stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, name, email]
        });

        try {
            res = await chai.request(server)
                .post('/auth/linked-in')
                .send(userSignupStep1);

            throw new Error("Expected to fail")
        } catch (error) {
            error.should.have.status(404);
            error.response.body.name.should.equal(name);
            should.exist(error.response.body.token)
            signupToken = error.response.body.token;
            stubGetAccessToken.restore();
            stubGetUserInfo.restore();
        }
        let userSignupStep2 = {
            name: name,
            username: username,
            linkedIn: {
                signupToken: signupToken
            },
            claimToken : claimDraft.token
        };

        res = await chai.request(server)
            .post('/signup/linked-in')
            .send(userSignupStep2);

        res.should.have.status(200);
        should.exist(res.body.token);

        let mongoDoc = await mongoUserRepo.findOne({'email': email});
        mongoDoc.email.should.equal(email);
        mongoDoc.username.should.equal(userSignupStep2.username);
        mongoDoc.name.should.equal(userSignupStep2.name);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);
        mongoDoc.linkedInParams.length.should.equal(3);
        should.exist(mongoDoc.confidenceScore.linkedInScore);
        should.exist(mongoDoc.confidenceScore.aggregateScore);

        let finalizedDraft = await mongooseClaimDraftsRepo.findOneByEmail(email);
        finalizedDraft.finalized.should.equal(true);

        stubGetSocialParams.restore();
    });
});
