// process.env.NODE_ENV = 'test';
//
// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const server = require('../../../server');
// const should = chai.should();
// const DB = require('../../db');
// const config = require('config');
// const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
// const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
// const testUtils = require('../../testUtils');
// const sinon = require('sinon')
// const createClaimDraftWrapper = require('./createClaimDraftWrapper');
// const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
// const gUtils = require("../../../models/services/social/googleService");
//
// const request = require('request')
// const linkedInUtils = require("../../../models/services/social/linkedInService");
// const linkedInSignup = require("../../../models/users/auth/social/linkedInSignup");
// const invitationWrapper = require('../../companies/advisors/invitationWrapper');
// const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
// const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
//
// chai.use(chaiHttp);
//
// describe('uportWithToken.spec.js', function () {
//     this.timeout(config.get('test.timeout'));
//
//     beforeEach((done) => {
//         console.log('connecting to database');
//         DB.connect(done);
//     });
//
//     afterEach((done) => {
//         console.log('dropping database');
//         DB.drop(done);
//     });
//
//     it('should be able to sign up with uport on Indorse platform', async () => {
//         let claimDraft = await createClaimDraftWrapper.createClaimDraftNonExistingUser();
//
//         let uportEmail = claimDraft.email;
//
//         let uportUid = 'uportuid';
//
//         let userSignup = {
//             username: 'tester',
//             name: 'bob',
//             email: uportEmail,
//             uport: {
//                 "public_key": uportUid,
//                 "field2": "Value2"
//             },
//             claimToken : claimDraft.token
//         };
//
//         let userLinkingUport = {
//             uport: {
//                 "public_key": uportUid,
//                 "field2": "Value2"
//             },
//         }
//
//         let res = await chai.request(server)
//             .post('/signup/uport')
//             .send(userSignup);
//
//         res.should.have.status(200);
//
//         let mongoDoc = await mongoUserRepo.findOne({ 'email': uportEmail});
//         mongoDoc.email.should.equal(uportEmail);
//         mongoDoc.username.should.equal(userSignup.username);
//         mongoDoc.uport_uid.should.equal(uportUid);
//         mongoDoc.uportParams.length.should.equal(2);
//
//
//         await chai.request(server)
//             .post('/auth/uport')
//             .send(userLinkingUport)
//
//         res.should.have.status(200);
//
//         let finalizedDraft = await mongooseClaimDraftsRepo.findOneByEmail(uportEmail);
//         finalizedDraft.finalized.should.equal(true);
//     });
// });
