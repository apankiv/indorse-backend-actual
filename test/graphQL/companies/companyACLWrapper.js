const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

const _helper = {};

_helper
    .addUserToCompanyACL = function addUserToCompanyACL(companyId, { _id, email }, inviteStatus) {
        const criteria = {
            _id: companyId,
        };
        const dataToSet = {
            acl: [{
                userId: _id,
                email,
                acceptedInvite: inviteStatus,
            }],
        };
        return mongoCompanyRepo.update(criteria, dataToSet);
    };

module.exports = _helper;
