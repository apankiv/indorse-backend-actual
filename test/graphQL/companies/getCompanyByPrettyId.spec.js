process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Company.getCompanyByPrettyId', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should get a company by prettyId if requested by an admin user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const getCompanyByPrettyIdRequest =
            `query {
                getCompanyByPrettyId(prettyId: "${createCompanyRequest.pretty_id}") {
                    id
                    companyName
                    prettyId
                    acl {
                        user {
                            username
                        }
                        acceptedInvite
                    }
                }
            }`;

        const adminUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getCompanyByPrettyIdRequest });
        const returnedCompanyData = res.body.data.getCompanyByPrettyId;
        returnedCompanyData.id.should.equal(companyId);
        returnedCompanyData.companyName.should.equal(createCompanyRequest.company_name);
        returnedCompanyData.prettyId.should.equal(createCompanyRequest.pretty_id);
    });

    it('should not get a company by id if requested by non admin user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const getCompanyByPrettyIdRequest =
            `query {
                getCompanyByPrettyId(prettyId: "${createCompanyRequest.pretty_id}") {
                    id
                    companyName
                    prettyId
                }
            }`;

        const regularUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getCompanyByPrettyIdRequest });
        should.exist(res.body.errors);
    });
});
