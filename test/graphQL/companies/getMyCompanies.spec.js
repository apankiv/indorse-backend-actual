process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyACLWrapper = require('./companyACLWrapper');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Company.getMyCompanies', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should get my company if requested by an company ACL user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const normalUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'profile_access');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, true);
        const getMyCompaniesRequest = `
            query {
                getMyCompanies {
                    id
                    companyName
                    prettyId
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getMyCompaniesRequest });
        const returnedCompanyData = res.body.data.getMyCompanies;
        returnedCompanyData.length.should.equal(1);
        const companyData = returnedCompanyData[0];
        companyData.companyName.should.equal(createCompanyRequest.company_name);
        companyData.prettyId.should.equal(createCompanyRequest.pretty_id);
    });

    it('should not get my company if requested by non company ACL user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const normalUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'profile_access');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, false);
        const getMyCompaniesRequest = `
            query {
                getMyCompanies {
                    id
                    companyName
                    prettyId
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getMyCompaniesRequest });
        should.exist(res.body.data);
        should.equal(res.body.data.getMyCompanies.length, 0);
    });
});
