process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyACLWrapper = require('./companyACLWrapper');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Company.removeCompanyACL', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should remove invited user from company if requested by an admin', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const adminUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, false);
        const removeCompanyACLRequest = `
            mutation {
                removeCompanyACL(prettyId: "${createCompanyRequest.pretty_id}", email: "${adminUser.email}") {
                    id
                    companyName
                    prettyId
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: removeCompanyACLRequest });
        const companyData = res.body.data.removeCompanyACL;
        companyData.companyName.should.equal(createCompanyRequest.company_name);
        companyData.prettyId.should.equal(createCompanyRequest.pretty_id);
    });

    it('should remove company ACL user from company if requested by an admin', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const adminUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, true);
        const removeCompanyACLRequest = `
            mutation {
                removeCompanyACL(prettyId: "${createCompanyRequest.pretty_id}", email: "${adminUser.email}") {
                    id
                    companyName
                    prettyId
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: removeCompanyACLRequest });
        const companyData = res.body.data.removeCompanyACL;
        companyData.companyName.should.equal(createCompanyRequest.company_name);
        companyData.prettyId.should.equal(createCompanyRequest.pretty_id);
    });

    it('should not remove Company ACL user if requested by non admin user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const normalUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'profile_access');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, false);
        const removeCompanyACLRequest = `
            mutation {
                removeCompanyACL(prettyId: "${createCompanyRequest.pretty_id}", email: "${normalUser.email}") {
                    id
                    companyName
                    prettyId
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: removeCompanyACLRequest });
        should.exist(res.body.errors);
    });
});
