process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const server = require('../../../server');

const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Company.restrictAssignmentsForCompany', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should restrict assignment for company if requested by an admin', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const adminUser = testUtils.generateRandomUser();

        await mongoCompanyRepo.insert(createCompanyRequest);
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        const restrictAssignmentsForCompanyRequest = `
            mutation {
                restrictAssignmentsForCompany(companyPrettyId: "${createCompanyRequest.pretty_id}", restrictAssignment: true) {
                    id
                    companyName
                    prettyId
                    magicLink {
                        assignmentRestricted
                    }
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: restrictAssignmentsForCompanyRequest });
        const companyData = res.body.data.restrictAssignmentsForCompany;
        companyData.companyName.should.equal(createCompanyRequest.company_name);
        companyData.prettyId.should.equal(createCompanyRequest.pretty_id);
        const magicLinkConfig = companyData.magicLink;
        magicLinkConfig.assignmentRestricted.should.equal(true);
    });

    it('should unrestrict assignment from company if requested by an admin', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const adminUser = testUtils.generateRandomUser();

        await mongoCompanyRepo.insert(createCompanyRequest);
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        const restrictAssignmentsForCompanyRequest = `
            mutation {
                restrictAssignmentsForCompany(companyPrettyId: "${createCompanyRequest.pretty_id}", restrictAssignment: false) {
                    id
                    companyName
                    prettyId
                    magicLink {
                        assignmentRestricted
                    }
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: restrictAssignmentsForCompanyRequest });
        const companyData = res.body.data.restrictAssignmentsForCompany;
        companyData.companyName.should.equal(createCompanyRequest.company_name);
        companyData.prettyId.should.equal(createCompanyRequest.pretty_id);
        const magicLinkConfig = companyData.magicLink;
        magicLinkConfig.assignmentRestricted.should.equal(false);
    });

    it('should not restrict user if requested by non admin user', async () => {
        const createCompanyRequest = testUtils.generateRandomCompany();
        const normalUser = testUtils.generateRandomUser();

        await mongoCompanyRepo.insert(createCompanyRequest);
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'profile_access');
        const restrictAssignmentsForCompanyRequest = `
            mutation {
                restrictAssignmentsForCompany(companyPrettyId: "${createCompanyRequest.pretty_id}", restrictAssignment: false) {
                    id
                    companyName
                    prettyId
                    magicLink {
                        assignmentRestricted
                    }
                }
            }`;
        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: restrictAssignmentsForCompanyRequest });
        should.exist(res.body.errors);
    });
});
