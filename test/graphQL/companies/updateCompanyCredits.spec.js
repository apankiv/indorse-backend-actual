process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const companyCreationWrapper = require('../../../test/companies/companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const companyMongooseSchema = require('../../../models/services/mongo/mongoose/schemas/company');

const server = require('../../../server');

chai.use(chaiHttp);

describe('update company credit test', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should update company assessment-assignment credit and it should be viewable on getCompanyByPrettyID', async () => {
        const adminUser = testUtils.generateRandomUser();
        const createCompanyRequest = testUtils.generateRandomCompany();
        createCompanyRequest.visible_to_public = true;

        const tokenObj = {};
        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest, tokenObj, adminUser.username);

        const creditUpdateForm = {
            creditType: companyMongooseSchema.creditType.ASSESSMENT_ASSIGNMENT,
            delta: 10,
            reason: testUtils.generateRandomString(),

        };

        const createInput = {
            companyPrettyId: createCompanyRequest.pretty_id,
            form: creditUpdateForm,
        };

        let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);


        const mutationRequest = `
            mutation updateCompanyCredits($companyPrettyId: String!, $form: CreditUpdateForm!) {
                updateCompanyCredits(companyPrettyId: $companyPrettyId, form: $form){
                    assessmentAssignment{
                        limit
                        used
                    }
                }
            }
        `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({
                query: mutationRequest,
                variables: { companyPrettyId: createInput.companyPrettyId, form: createInput.form },
            });
        updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        updatedCompany.credits.assessmentAssignment.used.should.equal(0);
        updatedCompany.credits.assessmentAssignment.limit.should.equal(20); // 20 because default is 10


        const getCompanyByPrettyIdRequest =
            `query {
                getCompanyByPrettyId(prettyId: "${createCompanyRequest.pretty_id}") {
                    id
                    companyName
                    prettyId
                    companyCredit{
                        assessmentAssignment{
                            limit
                            used
                        }
                        updates{
                          creditType
                          delta
                          reason
                          timestamp
                          user{
                            email
                          }
                        }
                    }
                }
            }`;


        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getCompanyByPrettyIdRequest });

        updatedCompany = res.body.data.getCompanyByPrettyId;
        updatedCompany.companyCredit.assessmentAssignment.used.should.equal(0);
        updatedCompany.companyCredit.assessmentAssignment.limit.should.equal(20); // 20 because default is 10
    });
});
