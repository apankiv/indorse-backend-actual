process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');

const { expect } = chai;
chai.use(chaiHttp);

describe('JobRoles.createJobRole', () => {
    beforeEach(async done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create a job role with valid data', async () => {
        // Arrange
        const form = testUtils.generateRandomJobRoleForm();

        const adminUser = testUtils.generateRandomUser();

        const adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            adminUser,
            adminTokenObj,
            'admin'
        );

        // Act
        const operationName = 'createJobRole';
        const mutationGql = `
            mutation ${operationName}($form: CreateJobRoleForm!) {
                createJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };

        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${adminTokenObj.token}`)
            .send(reqData);

        // Assert
        res.status.should.equal(200);
        const resData = res.body.data[operationName];

        expect(resData._id).to.exist;
        expect(resData.title).to.equal(form.title);
        expect(resData.description).to.equal(form.description);
        expect(resData.prettyId).to.equal(form.prettyId);
    });

    it('should fail with invalid data', async () => {
        // Arrange
        const form = testUtils.generateRandomJobRoleForm();

        const adminUser = testUtils.generateRandomUser();

        const adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            adminUser,
            adminTokenObj,
            'admin'
        );

        // Act
        const operationName = 'createJobRole';
        const mutationGql = `
            mutation ${operationName}($form: CreateJobRoleForm!) {
                createJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        form.title = null;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };

        // Assert
        let res;

        try {
            res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${adminTokenObj.token}`)
                .send(reqData);
        } catch (err) {
            expect(err).to.exist;
        } finally {
            expect(res).to.not.exist;
        }
    });

    it('should fail for non admin', async () => {
        // Arrange
        const form = testUtils.generateRandomJobRoleForm();

        const user = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            user,
            tokenObj,
            'profile_access',
        );

        // Act
        const operationName = 'createJobRole';
        const mutationGql = `
            mutation ${operationName}($form: CreateJobRoleForm!) {
                createJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };

        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send(reqData);

        // Assert
        expect(res.body.errors).to.exist;
    });
});
