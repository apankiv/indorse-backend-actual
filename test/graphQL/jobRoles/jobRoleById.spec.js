process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobRolesRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

const { expect } = chai;
const should = chai.should();
chai.use(chaiHttp);

describe('JobRoles.jobRoleById', () => {
    beforeEach(async done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should job role correctly', async () => {
        // Arrange
        const adminUser = testUtils.generateRandomUser();
        const skillTag1 = 'tag1';
        const skillTag2 = 'tag2';
        const skillTag3 = 'tag3';

        const randomJobRole = testUtils.generateRandomJobRole();
        randomJobRole.skillTags = [skillTag2, skillTag3];
        const randomJobRoleId = await mongooseJobRolesRepo.insert(
            randomJobRole
        );

        //Create skills
        let skill = {
            name: testUtils.generateRandomSkill(),
            category: testUtils.generateRandomSkill(),
        };

        skill.tags = [skillTag1, skillTag2];
        await mongooseSkillRepo.insert(skill);

        skill.tags = [skillTag1];
        await mongooseSkillRepo.insert(skill);

        skill.tags = [skillTag3];
        let skillid = await mongooseSkillRepo.insert(skill);

        const adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            adminUser,
            adminTokenObj,
            'admin'
        );

        //Add one skill to user
        let updateObj = [];
        let obj = {
            skill: {
                name: 'test',
                category: 'tmp',
                level: 'beginner',
                _id: skillid,
            },
        };
        updateObj.push(obj);

        await mongoUserRepo.update(
            { email: email },
            { $set: { skills: updateObj } }
        );
        
        let mongoUser = await mongoUserRepo.findOneByEmail(adminUser.email);

        let query = `
            query jobRoleById($id: String!){
                jobRoleById(id: $id){
                    _id
                    title
                    description
                    iconUrl
                    skills{
                        _id
                        name
                        tags
                        iconUrl
                        userHasSkill
                    }
                    skillTags
                }
            }            
            `;

        let res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${adminTokenObj.token}`)
            .send({ query, variables: { id: randomJobRoleId } });
        
        const returnedJobRole = res.body.data.jobRoleById;

        returnedJobRole._id.should.equal(randomJobRoleId);
        returnedJobRole.title.should.equal(randomJobRole.title);
        returnedJobRole.description.should.equal(randomJobRole.description);
        should.exist(returnedJobRole.iconUrl);

        returnedJobRole.skills.length.should.equal(2);
        returnedJobRole.skillTags.length.should.equal(2);

        for (const jobRoleSkill of returnedJobRole.skills) {
            if (jobRoleSkill._id === skillid) {
                jobRoleSkill.userHasSkill.should.equal(true);
            }
        }
    });
});
