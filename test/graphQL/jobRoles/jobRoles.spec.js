process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseJobRoleRepo = require('../../../models/services/mongo/mongoose/mongooseJobRoleRepo');


chai.use(chaiHttp);

describe('getJobRoles test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should test getJobRoles endpoint', async () => {

        try {

            let adminUser = testUtils.generateRandomUser();
            let randomRole = testUtils.generateRandomJobRole();
            let id = await mongooseJobRoleRepo.insert(randomRole);

            let queryRequest = `					 
            query GetJobRoles {
                jobRoles {
                    _id
                    iconUrl
                    description
                    title
                }
            }
            `;

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: queryRequest});

            res.should.have.status(200);
            let jobRoles = res.body.data.jobRoles;
            jobRoles.length.should.equal(1);
            should.exist(jobRoles[0].iconUrl)
            should.exist(jobRoles[0].description)
            should.exist(jobRoles[0].title)
            should.exist(jobRoles[0]._id)  

        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});