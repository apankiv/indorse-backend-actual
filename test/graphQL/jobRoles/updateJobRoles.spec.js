process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');

const { expect } = chai;
chai.use(chaiHttp);

const prettyId = testUtils.generateRandomString();

describe('JobRoles.updateJobRole', () => {
    beforeEach(async done => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach(done => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should update a job role with valid data', async () => {
        await createJobRole();

        const form = testUtils.generateRandomJobRoleForm();
        form.prettyId = prettyId;

        const adminUser = testUtils.generateRandomUser();

        const adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            adminUser,
            adminTokenObj,
            'admin',
        );

        // Act
        const operationName = 'updateJobRole';
        const mutationGql = `
            mutation ${operationName}($form: UpdateJobRoleForm!) {
                updateJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };
        
        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${adminTokenObj.token}`)
            .send(reqData);

        // Assert
        res.status.should.equal(200);

        const resData = res.body.data[operationName];

        expect(resData._id).to.exist;
        expect(resData.title).to.equal(form.title);
        expect(resData.description).to.equal(form.description);
        expect(resData.prettyId).to.equal(form.prettyId);
    });

    it('should fail with invalid data', async () => {
        await createJobRole();

        const form = testUtils.generateRandomJobRoleForm();
        form.prettyId = prettyId;
        form.title = null;

        const adminUser = testUtils.generateRandomUser();

        const adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            adminUser,
            adminTokenObj,
            'admin',
        );

        // Act
        const operationName = 'updateJobRole';
        const mutationGql = `
            mutation ${operationName}($form: UpdateJobRoleForm!) {
                updateJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };

        // Assert
        let res;

        try {
            res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${adminTokenObj.token}`)
                .send(reqData);
        } catch (err) {
            expect(err).to.exist;
        } finally {
            expect(res).to.not.exist;
        }
    });

    it('should fail for non admin', async () => {
        // Arrange
        await createJobRole();

        const form = testUtils.generateRandomJobRoleForm();
        form.prettyId = prettyId;

        const user = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(
            user,
            tokenObj,
            'profile_access',
        );

        // Act
        const operationName = 'updateJobRole';
        const mutationGql = `
            mutation ${operationName}($form: UpdateJobRoleForm!) {
                updateJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

        const reqData = {
            operationName,
            query: mutationGql,
            variables: { form },
        };

        const res = await chai
            .request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send(reqData);

        // Assert
        expect(res.body.errors).to.exist;
    });
});

async function createJobRole() {
    const form = testUtils.generateRandomJobRoleForm();
    form.prettyId = prettyId;

    const adminUser = testUtils.generateRandomUser();

    const adminTokenObj = {};
    await authenticationWrapper.signupVerifyAuthenticateWithRole(
        adminUser,
        adminTokenObj,
        'admin',
    );

    // Act
    const operationName = 'createJobRole';
    const mutationGql = `
            mutation ${operationName}($form: CreateJobRoleForm!) {
                createJobRole(form: $form) {
                    _id
                    prettyId
                    title
                    description
                } 
            }
        `;

    const reqData = {
        operationName,
        query: mutationGql,
        variables: { form },
    };

    const res = await chai
        .request(server)
        .post('/graphql')
        .set('Authorization', `Bearer ${adminTokenObj.token}`)
        .send(reqData);

    expect(res).to.exist;
    expect(res.body).to.exist;
    expect(res.body.data).to.exist;
    expect(res.body.data[operationName]).to.exist;
    expect(res.body.data[operationName]._id).to.exist;
}
