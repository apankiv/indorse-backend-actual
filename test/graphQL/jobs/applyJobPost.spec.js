process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');

chai.use(chaiHttp);

describe('Jobs.applyJobPost', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);      
    });

    it('should apply a new job successfully', async () => {
        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            const skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    chatbot: true
                }
            }
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name });
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        //Add both skills to users object
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    level:"beginner",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "claim",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                {
                    type: "claim",
                    level: "expert",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({ email: email }, { $set: { skills: updateObj } })
        let mongoUser =await mongoUserRepo.findOneByEmail(email);


        //Start application        
        const operationName2 = 'startApplicationTest';
        const mutationGql2 = `
            mutation ${operationName2}($id: String!) {
                startApplicationJobPost(id: $id) 
            }
        `;

        let body2 = { operationName: operationName2, query: mutationGql2, variables: { id: jobId } };
        let res2 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body2);               
               

        //Apply job               
        const operationName = 'applyJobPostTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                applyJobPost(id: $id) {
                    url
                    email
                } 
            }
        `; 

      
        let body = { operationName: operationName, query: mutationGql, variables: { id: jobId } };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);



        
        const returnedJobPost = res.body.data.applyJobPost;
        should.exist(returnedJobPost.url)
        should.exist(returnedJobPost.email)
        let userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString()});        
        userJob.job.should.equal(jobId);


        //Re-Apply job should not fail
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);           
        
        
        //Get job and applied status should be true
        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    applicationStatus
                }
            }            
            `;
        

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ query: getJobByID });

        let returnData = res.body.data.jobById;        
        returnData.applicationStatus.should.equal('user_applied')

    });
});