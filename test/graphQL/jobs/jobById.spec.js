process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');

chai.use(chaiHttp);

describe('Jobs.jobById', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive the job by Id', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                }
            }            
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getJobByID});

        let returnData = res.body.data.jobById;
        returnData.id.should.equal(jobId);
    });



    it('should not retrive email/app link for user not meeting skills ', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation : {
                    chatbot : true
                }
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        skillIds.pop();
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'profile_access');

        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                    applicationLink
                    contactEmail
                }
            }            
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getJobByID });

        let returnData = res.body.data.jobById;
        returnData.id.should.equal(jobId);
        should.not.exist(returnData.contactEmail);
        should.not.exist(returnData.applicationLink);        
    });


    it('should retrive email/app link for user meeting skills ', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation : {
                    chatbot : true
                }
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'profile_access');
        let updateObj = [];
        //Add both skills to users object
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "claim",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "claim",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                }, 
                {
                        type: "claim",
                        level: "expert",
                        id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                        validated: true
                }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({ email: email}, {$set :{skills : updateObj}})
        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                    applicationLink
                    contactEmail
                }
            }            
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getJobByID });

        let returnData = res.body.data.jobById;
        returnData.id.should.equal(jobId);
        returnData.contactEmail.should.equal(jobPost.contactEmail);
        returnData.applicationLink.should.equal(jobPost.applicationLink);
    });


    it('should not retrive email/app link for user meeting skills partially', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation : {
                    chatbot : true
                }
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        skillIds.pop();
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'profile_access');
        let updateObj = [];
        //Add both skills to users object
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "claim",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            }
            updateObj.push(obj);
        }

        updateObj[0].validations[0].validated=false;

        await mongoUserRepo.update({ email: email }, { $set: { skills: updateObj } })
        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                    applicationLink
                    contactEmail
                }
            }            
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getJobByID });

        let returnData = res.body.data.jobById;
        returnData.id.should.equal(jobId);
        should.not.exist(returnData.contactEmail);
        should.not.exist(returnData.applicationLink);  

    });



    it('should not fail if not logged in', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                }
            }            
            `;


        let res = await chai.request(server)
            .post('/graphql')
            .send({query: getJobByID});

        let returnData = res.body.data.jobById;
        returnData.id.should.equal(jobId);
        should.not.exist(returnData.contactEmail);
        should.not.exist(returnData.applicationLink);          


    });

    it('should fail if job is not approved', async () => {

        let jobPost = await testUtils.createRandomJob();

        jobPost.approved.approved = false;

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();
        let jobId = await mongoJobRepo.insert(jobPost);

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'profile_acess');

        let getJobByID =
            `
            query {
                jobById(id:` + '"' + jobId + '"' + `) {
                    id
                    title
                    experienceLevel
                    description
                }
            }            
            `;

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: getJobByID});        
        should.exist(res.body.errors);
    });
});