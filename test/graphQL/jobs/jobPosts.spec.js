process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const jobUtils = require('./jobsUtils');

chai.use(chaiHttp);

describe('Jobs.jobPosts', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            await jobUtils.createTextIndex();
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive all approved jobs', async () => {

        let jobPost, unapprovedobPost;
        [jobPost, unapprovedobPost, {}, {}] = await insertApprovedAndUnapprovedJob();

        let getAllJobsQuery =
            `query {
                jobPosts(pageNumber: 1, pageSize: 10) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                totalJobPosts                
                }
            }`;

        let regularUser = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getAllJobsQuery });


        res.body.data.jobPosts.jobPosts.length.should.equal(1);
    });

    it('should filter jobs', async () => {

        let jobPost, unapprovedobPost, skillIds, createCompanyRequest;
        [jobPost, unapprovedobPost, skillIds, createCompanyRequest] = await insertApprovedAndUnapprovedJob();

        let operationName = 'filterJobPosts';

        let getAllJobsQuery =
            `query  ${operationName}($pageNumber: Int!, $pageSize: Int!, $search: String) {
                jobPosts(pageNumber: $pageNumber, pageSize: $pageSize, search: $search) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                    matchingJobPosts
                    totalJobPosts                
                }
            }`;

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        // Correct title
        let variables = { pageNumber: 1, pageSize: 1, search: jobPost.title };

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(1);
        res.body.data.jobPosts.matchingJobPosts.should.equal(1);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Wrong title
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.title + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(0);
        res.body.data.jobPosts.matchingJobPosts.should.equal(0);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Correct location
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.location };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(1);
        res.body.data.jobPosts.matchingJobPosts.should.equal(1);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Wrong location
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.location + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(0);
        res.body.data.jobPosts.matchingJobPosts.should.equal(0);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Correct companyName
        variables = { pageNumber: 1, pageSize: 1, search: createCompanyRequest.companyName };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(1);
        res.body.data.jobPosts.matchingJobPosts.should.equal(1);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Wrong companyName
        variables = { pageNumber: 1, pageSize: 1, search: createCompanyRequest.companyName + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(0);
        res.body.data.jobPosts.matchingJobPosts.should.equal(0);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Correct skill name
        // Correct skill name
        let skill = mongoSkillsRepo.findOneById(skillIds[0].id);
        variables = { pageNumber: 1, pageSize: 1, search: skill.name };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });
        res.body.data.jobPosts.jobPosts.length.should.equal(1);
        res.body.data.jobPosts.matchingJobPosts.should.equal(1);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);

        // Wrong skill name
        variables = { pageNumber: 1, pageSize: 1, search: skill.name + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.jobPosts.jobPosts.length.should.equal(0);
        res.body.data.jobPosts.matchingJobPosts.should.equal(0);
        res.body.data.jobPosts.totalJobPosts.should.equal(1);
    });

    it('should sort jobs', async () => {

        let jobPost1 = await testUtils.createRandomJob();
        let jobPost2 = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            const skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            }
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost1.skills = skillIds;
        jobPost2.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost1.title = '1' + jobPost1.title;
        jobPost1.location = '1' + jobPost1.location;
        jobPost1.experienceLevel = 'intern';
        jobPost1.submitted.at = testUtils.getCurrentTimestamp();
        jobPost2.updated.at = jobPost1.submitted.at + 1;
        jobPost1.company = {};
        jobPost1.company.id = companyId;
        jobPost1.company.name = createCompanyRequest.company_name;
        jobPost1.company.description = testUtils.generateRandomString();
        jobPost1._id = await mongoJobRepo.insert(jobPost1);

        let unapprovedJobPost1 = copyObj(jobPost1);
        delete unapprovedJobPost1._id;
        unapprovedJobPost1.approved.approved = false;
        unapprovedJobPost1._id = await mongoJobRepo.insert(unapprovedJobPost1);

        jobPost2.title = '2' + jobPost2.title;
        jobPost2.location = '2' + jobPost2.location;
        jobPost2.experienceLevel = 'junior';
        jobPost2.submitted.at = jobPost1.submitted.at + 1;
        jobPost2.updated.at = jobPost2.submitted.at + 1;
        jobPost2.company = {};
        jobPost2.company.id = companyId;
        jobPost2.company.name = createCompanyRequest.company_name;
        jobPost2.company.description = testUtils.generateRandomString();
        jobPost2._id = await mongoJobRepo.insert(jobPost2);

        let unapprovedJobPost2 = copyObj(jobPost2);
        delete unapprovedJobPost2._id;
        unapprovedJobPost2.approved.approved = false;
        unapprovedJobPost2._id = await mongoJobRepo.insert(unapprovedJobPost2);

        let operationName = 'sortJobPosts';

        let getAllJobsQuery =
            `query  ${operationName}($pageNumber: Int!, $pageSize: Int!, $sort: JobsSortParam) {
                jobPosts(pageNumber: $pageNumber, pageSize: $pageSize, sort: $sort) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                    totalJobPosts                
                }
            }`;

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        // Default order: submitted at desc
        let variables = {pageNumber: 1, pageSize: 2};
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost1._id);

        // Submitted at asc
        variables.sort = 'asc_submitted_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost2._id);


        // Title asc
        variables.sort = 'asc_title';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Title desc
        variables.sort = 'desc_title';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Experience asc
        variables.sort = 'asc_experience_level';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Experience desc
        variables.sort = 'desc_experience_level';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Location asc
        variables.sort = 'asc_location';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Location desc
        variables.sort = 'desc_location';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Updated at asc
        variables.sort = 'asc_updated_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.jobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Updated at desc
        variables.sort = 'desc_updated_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.jobPosts.jobPosts[0].id.should.equal(jobPost2._id);
    });
});

async function insertApprovedAndUnapprovedJob() {
    let jobPost = await testUtils.createRandomJob();

    let skillIds =[]
    for (i=0;i<3;++i){
        let skillToAdd = {
            name: testUtils.generateRandomString(),
            category: testUtils.generateRandomString()
        };
        let id = await mongoSkillsRepo.insert(skillToAdd);
        skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
    }

    jobPost.skills = skillIds;

    let createCompanyRequest = testUtils.generateRandomCompany();
    let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

    jobPost.company = {};
    jobPost.company.id = companyId;
    jobPost.company.name = createCompanyRequest.company_name;
    jobPost.company.description = testUtils.generateRandomString();
    await mongoJobRepo.insert(jobPost);

    let unapprovedJobPost = jobPost;
    unapprovedJobPost.approved.approved = false;

    await mongoJobRepo.insert(unapprovedJobPost);

    return [jobPost, unapprovedJobPost, skillIds, createCompanyRequest];
}

function copyObj (obj) {
    return JSON.parse(JSON.stringify(obj));
}