const mongoose = require('../../../models/services/mongo/mongoose/mongooseDB');
const jobsSchema = require('../../../models/services/mongo/mongoose/schemas/job').schema;
let Job = mongoose.model('jobs', jobsSchema);

module.exports.createTextIndex = async function createTextIndex() {
    await Job.collection.dropIndexes();
    await Job.collection.createIndex({
        title: "text",
        description: "text",
        location: "text",
        "company.name": "text",
        "skills.name": "text",
    });
}