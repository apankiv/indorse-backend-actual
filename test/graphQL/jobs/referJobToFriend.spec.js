process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseUserJobsReferralRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsReferralRepo');

chai.use(chaiHttp);

describe('Jobs.referJobToFriend', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should refer a job to friend successfully', async () => {
        let jobPost = await testUtils.createRandomJob();              

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');

        let candidateToCreate = testUtils.generateRandomUser();

        const variables = {
            email: candidateToCreate.email,
            name : "hello",
            jobId : jobId
        };
        
        const operationName = 'referJobToFriend';
        const mutationGql = `
        mutation ${operationName}($email: EmailAddress! , $name : String!, $jobId: String!) {
            referJobToFriend(email: $email, name : $name, jobId : $jobId)
        }
        `;        


        let body = { operationName: operationName, query: mutationGql, variables: variables };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        res.body.data.referJobToFriend.should.equal('user_referred_sucesfully');



        //generate a new user
        let user2 = testUtils.generateRandomUser();
        let tokenObj2 = {};
        let createdUser2 = await authenticationWrapper.signupVerifyAuthenticateWithRole(user2, tokenObj2, 'profile_access');        

        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body);

        res.body.data.referJobToFriend.should.equal('candidate_already_referred_by_another_user');
        let count = await mongooseUserJobsReferralRepo.count({candidateEmail :variables.email});
        count.should.equal(2);
        
        
        //Admin query on the job
        let adminUser = testUtils.generateRandomUser();
        let adminToken = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, adminToken, 'admin');



        const operationName4 = 'adminListReferrals';
        const queryGql4 = `
        query ${operationName4}($jobId: String!) {
            adminListReferralsByJobId(jobId: $jobId){
                referrals {
                    user {
                        userId
                        email
                        name
                        username
                    }
                    candidate {
                        userId
                        email
                        name
                        username
                    }
                    referredAt                
                }
                totalUserJobReferrals           
            }
        }
    `;

        let body4 = { operationName: operationName4, query: queryGql4, variables: { jobId: jobId } };

        let res4 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + adminToken.token)
            .send(body4);   

        let result4 = res4.body.data.adminListReferralsByJobId.referrals[0]
        should.exist(result4.user.userId)
        should.exist(result4.user.email)
        should.exist(result4.user.name)
        should.exist(result4.user.username)
        should.exist(result4.candidate.email)
        should.exist(result4.candidate.name)
        should.not.exist(result4.candidate.userId)
        should.not.exist(result4.candidate.username)
        should.exist(result4.referredAt);
        res4.body.data.adminListReferralsByJobId.totalUserJobReferrals.should.equal(2);     



        //Start application        
        const operationNameApp = 'startApplicationTest';
        const mutationGqlApp = `
            mutation ${operationNameApp}($id: String!) {
                startApplicationJobPost(id: $id) 
            }
        `;

        bodyApp = { operationName: operationNameApp, query: mutationGqlApp, variables: { id: jobId } };
        let resapp = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(bodyApp);

       
        //Apply job
        let candidateToken = {}
        await authenticationWrapper.signupVerifyAuthenticateWithRole(candidateToCreate, candidateToken, 'profile_access');
        const operationName3 = 'applyJobPostTest';
        const mutationGql3 = `
        mutation ${operationName3}($id: String!) {
            applyJobPost(id: $id) {
                url
                email
            } 
        }
        `;



        let body3 = { operationName: operationName3, query: mutationGql3, variables: { id: jobId } };
        let res3 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body3);
    
       

        //Check if flag is returned in result         
        operationName5 = 'jobApplicantsByIdTest';
        const queryGql5 = `
            query ${operationName5}($jobId: String!, $pageNumber: Int, $pageSize: Int) {
                jobApplicantsById(jobId: $jobId, pageNumber: $pageNumber, pageSize: $pageSize) {
                    applicants {
                        id
                        appliedViaReferral
                    }
                }
            }
        `;


        body5 = { operationName: operationName5, query: queryGql5, variables: { jobId: jobId, pageNumber: 1, pageSize: 10 } };
        res5 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + adminToken.token)
            .send(body5);

        const jobApplicantsById = res5.body.data.jobApplicantsById;        
        jobApplicantsById.applicants[0].appliedViaReferral.should.equal(true);

    });



    it('should not allow user to refer himself for a job successfully', async () => {
        let jobPost = await testUtils.createRandomJob();

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');


        const variables = {
            email: user.email,
            name: "hello",
            jobId: jobId
        };

        const operationName = 'referJobToFriend';
        const mutationGql = `
        mutation ${operationName}($email: EmailAddress! , $name : String!, $jobId: String!) {
            referJobToFriend(email: $email, name : $name, jobId : $jobId)
        }
        `;

        await mongoUserRepo.update({ email: user.email }, { ethaddress: '0x267be1C1D684F78cb4F6a176C4911b741E4Ffdc0' })

        let body = { operationName: operationName, query: mutationGql, variables: variables };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        res.body.data.referJobToFriend.should.equal('user_cannot_refer_self');
    });




});