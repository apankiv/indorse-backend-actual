process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');


chai.use(chaiHttp);

describe('Jobs.updateJobPost', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should update job details as per inputs passed', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobToUpdate = await mongoJobRepo.insert(jobPost);

        delete skillIds[0].name;
        const variables = {
            id: jobToUpdate,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    id: companyId,
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: [skillIds[0]],
                location: "London",
                campaign: {
                    title: "Running a campaign",
                    description: "takes a lot of effort",
                    enabled: true
                },
            }
        };

       
        const operationName = "updateJobPost";
        const query = `
        mutation ${operationName}($id: String!, $form: JobPostForm) {
            updateJobPost(id: $id, form: $form) {
                id
                title
                experienceLevel
                monthlySalary
                description
                company{
                    description
                    id
                    name
                }
                contactEmail
                applicationLink
                location
                skills{
                    id
                }
                campaign {
                    title
                    description
                    enabled
                }
            }
        }
        `;
            
        const body = { operationName: operationName, query: query, variables: variables };      
        
        let tokenObj = {};
        let adminUser = testUtils.generateRandomUser();             
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);
        
        res.status.should.equal(200);
        let returnValues = res.body.data.updateJobPost;
        returnValues.title.should.equal(variables.form.title)
        returnValues.experienceLevel.should.equal(variables.form.experienceLevel)
        returnValues.description.should.equal(variables.form.description)
        returnValues.monthlySalary.should.equal(variables.form.monthlySalary)
        returnValues.company.description.should.equal(variables.form.company.description)
        returnValues.contactEmail.should.equal(variables.form.contactEmail)
        returnValues.applicationLink.should.equal(variables.form.applicationLink)
        returnValues.skills.length.should.equal(1)
        returnValues.skills[0].id.should.equal(variables.form.skills[0].id)
        returnValues.location.should.equal(variables.form.location)
        returnValues.campaign.title.should.equal(variables.form.campaign.title)
        returnValues.campaign.description.should.equal(variables.form.campaign.description)
    });

    it('should create a new company if not present', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobToUpdate = await mongoJobRepo.insert(jobPost);

        delete skillIds[0].name;
        const variables = {
            id: jobToUpdate,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    name :"Non existing company",
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: [skillIds[0]],
                location: "London"
            }
        };


        const operationName = "updateJobPost";
        const query = `
        mutation ${operationName}($id: String!, $form: JobPostForm) {
            updateJobPost(id: $id, form: $form) {
                id
                title
                experienceLevel
                monthlySalary
                description
                company{
                    description
                    id
                    name
                }
                contactEmail
                applicationLink
                location
                skills{
                    id
                }
            }
        }
        `;

        const body = { operationName: operationName, query: query, variables: variables };

        let tokenObj = {};
        let adminUser = testUtils.generateRandomUser();
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        res.status.should.equal(200);
        let returnValues = res.body.data.updateJobPost;
        returnValues.company.description.should.equal(variables.form.company.description)

        let count = await mongoCompanyRepo.findOne({ company_name: variables.form.company.name});
        should.exist(count);


    });


    it('should fail for non-admin', async () => {
        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobToUpdate = await mongoJobRepo.insert(jobPost);

        delete skillIds[0].name;
        const variables = {
            id: jobToUpdate,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    id: companyId,
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: [skillIds[0]],
                location: "London"
            }
        };


        const operationName = "updateJobPost";
        const query = `
        mutation ${operationName}($id: String!, $form: JobPostForm) {
            updateJobPost(id: $id, form: $form) {
                id
                title
                experienceLevel
                monthlySalary
                description
                company{
                    description
                    id
                    name
                }
                contactEmail
                applicationLink
                location
                skills{
                    id
                }
            }
        }
        `;

        const body = { operationName: operationName, query: query, variables: variables };

        let tokenObj = {};
        let regularUser = testUtils.generateRandomUser();
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        should.exist(res.body.errors);
    })
});