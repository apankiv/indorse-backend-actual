process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');


chai.use(chaiHttp);

describe('Jobs.updateJobPostStatus', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should update status of job to approved/disapproved', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobToUpdate = await mongoJobRepo.insert(jobPost);


        const mutationGql = `
                        mutation {
                        updateJobPostStatus(id:` + '"' + jobToUpdate + '"' + `,status:true){
                            id
                            approved{
                                by
                                at
                                approved
                            }
                        }
                        }`;



        let tokenObj = {};

        let adminUser = testUtils.generateRandomUser();
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: mutationGql });

        res.body.data.updateJobPostStatus.id.should.equal(jobToUpdate);
        res.body.data.updateJobPostStatus.approved.approved.should.equal(true);
    });

    it('should fail for non-admin', async () => {
        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            };
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {
            id: companyId,
            name: createCompanyRequest.company_name,
            description: testUtils.generateRandomString()
        };
        let jobToUpdate = await mongoJobRepo.insert(jobPost);


        const mutationGql = `
                        mutation {
                        updateJobPostStatus(id:` + '"' + jobToUpdate + '"' + `,status:true){
                            id
                            approved{
                                by
                                at
                                approved
                            }
                        }
                        }`;



        let tokenObj = {};

        let regularUser = testUtils.generateRandomUser();
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: mutationGql });

        should.exist(res.body.errors);
    })
});