process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');

chai.use(chaiHttp);

describe('Jobs.userAppliedJobPosts', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive all jobs applied by user', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            const skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    chatbot: true
                }
            }
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name });
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        //Add both skills to users object
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    level: "beginner",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "quizbot",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                },
                {
                    type: "claim",
                    level: "expert",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({ email: email }, { $set: { skills: updateObj } })
        let mongoUser = await mongoUserRepo.findOneByEmail(email);

        const operationName = 'startApplicationTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                startApplicationJobPost(id: $id) 
            }
        `;

        let body = { operationName: operationName, query: mutationGql, variables: { id: jobId } };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);


        let userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_can_apply');


        //Re-apply
        let resReapply = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        
        should.exist(resReapply.body.errors);



        //Create a new user
        let user2 = testUtils.generateRandomUser();

        let tokenObj2 = {};
        let createdUser2 = await authenticationWrapper.signupVerifyAuthenticateWithRole(user2, tokenObj2, 'profile_access');


        let body2 = { operationName: operationName, query: mutationGql, variables: { id: jobId } };
        let res2 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body2);

        let mongoUser2 = await mongoUserRepo.findOneByEmail(user2.email);
        let userJob2 = await mongoUserJobsRepo.findOne({ user: mongoUser2._id.toString() })
        userJob2.status.should.equal('user_pending_validations');


        let userAppliedJobPosts = 
        `query {
                    userAppliedJobPosts(pageNumber: 1, pageSize: 10) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                            userStatus {
                                claimInProgress
                                claimValidated
                                quizbot
                            }
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                        applicationStatus
                        applicationStarted
                    }
                totalJobPosts            
                }
            }`;


            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ query: userAppliedJobPosts });

            let data = res.body.data.userAppliedJobPosts.jobPosts[0];
            data.skills.length.should.equal(2)
            data.skills[0].userStatus.claimValidated.should.equal('expert')
            data.skills[0].userStatus.quizbot.should.equal('beginner')
            data.applicationStarted.should.equal(true);

    }); 


    it('user job status should change when new skill is added to job', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            const skillToAdd = {
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    aip: true
                }
            }
            let id = await mongoSkillsRepo.insert(skillToAdd);
            skillIds.push({ id: id, level: 'beginner', name: skillToAdd.name });
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.name = createCompanyRequest.company_name;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        //Add both skills to users object
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    level: "beginner",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "quizbot",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                },
                {
                    type: "claim",
                    level: "expert",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({ email: email }, { $set: { skills: updateObj } })
        let mongoUser = await mongoUserRepo.findOneByEmail(email);

        const operationName = 'startApplicationTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                startApplicationJobPost(id: $id) 
            }
        `;


        let body = { operationName: operationName, query: mutationGql, variables: { id: jobId } };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);
          


        let userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_can_apply');

        //Update job to add a new skill. User's status should change to user_pending_validations
        let newSkill = await mongoSkillsRepo.insert({
            name: testUtils.generateRandomString(), 
            category: testUtils.generateRandomString(), 
            validation: {
                aip: true
            }
        });

        
        const variables = {
            id: jobId,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    id: companyId,
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: [{id : newSkill, level : 'expert'}],
                location: "London",
                campaign: {
                    title: "Running a campaign",
                    description: "takes a lot of effort",
                    enabled: true
                },
            }
        };


        //Update job post with new skills
        const operationName2 = "updateJobPost";
        const query2 = `
        mutation ${operationName2}($id: String!, $form: JobPostForm) {
            updateJobPost(id: $id, form: $form) {
                skills{
                    id
                }
            }
        }
        `;

        const body2 = { operationName: operationName2, query: query2, variables: variables };
        let tokenObj2 = {};
        let adminUser = testUtils.generateRandomUser();
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj2, 'admin');


        let res2 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body2);
    

        //User status should go back to pending
        userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_pending_validations');


        for (let skill of skillIds) {
            delete skill.name;
        }

        //re-update with old skills
        const variables3 = {
            id: jobId,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    id: companyId,
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: skillIds,
                location: "London",
                campaign: {
                    title: "Running a campaign",
                    description: "takes a lot of effort",
                    enabled: true
                },
            }
        };


        const body3 = { operationName: operationName2, query: query2, variables: variables3 };

        let res3 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body3);

        //User status should go back to apply
        userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_can_apply'); 
        
        

        // Update to a new skill with chatbot only
        //Update job to add a new skill. User's status should change to user_pending_validations
        let newSkillCb = await mongoSkillsRepo.insert({
            name: testUtils.generateRandomString(),
            category: testUtils.generateRandomString(),
            validation: {
                chatbot: true
            }
        });


        const variables4 = {
            id: jobId,
            form: {
                title: "New Job",
                experienceLevel: "intern",
                description: "Developers wanted!",
                monthlySalary: "100 USD",
                company: {
                    id: companyId,
                    description: "New description of job"
                },
                contactEmail: "New@email.com",
                applicationLink: "www.internet.com",
                skills: [{ id: newSkillCb, level: 'expert' }],
                location: "London",
                campaign: {
                    title: "Running a campaign",
                    description: "takes a lot of effort",
                    enabled: true
                },
            }
        };


        //Update job post with new skills
        const body4 = { operationName: operationName2, query: query2, variables: variables4 };
        let res4 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body4);


        //User status should go back to pending
        userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_can_apply');

        //Update with beginner skill for chatbot validation

        // Update to a new skill with chatbot only
        //Update job to add a new skill. User's status should change to user_pending_validations
        let newSkillCb2 = await mongoSkillsRepo.insert({
            name: testUtils.generateRandomString(),
            category: testUtils.generateRandomString(),
            validation: {
                chatbot: true
            }
        });


        //Update skills
        variables4.form.skills = [{ id: newSkillCb2, level: 'beginner' }]
        
        //Update job post with new skills
        const body5 = { operationName: operationName2, query: query2, variables: variables4 };
        let res5 = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj2.token)
            .send(body5);


        //User status should go back to pending
        userJob = await mongoUserJobsRepo.findOne({ user: mongoUser._id.toString() })
        userJob.job.should.equal(jobId);
        userJob.status.should.equal('user_pending_validations');




    }); 
});