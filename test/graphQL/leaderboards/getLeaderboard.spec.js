process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const computeValidatorStats = require('../../../smart-contracts/services/cron/computeValidatorStats');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const timestampService = require('../../../models/services/common/timestampService');
const authenticationWrapper = require('../../authenticationWrapper');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const testUtils = require('../../testUtils');
const ObjectId = require('mongodb').ObjectID;
const settings = require('../../../models/settings');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const config = require('config');

chai.use(chaiHttp);

describe('Leaderboards.getLeaderboard', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;
    beforeEach(async (done) => {
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        console.log('connecting to database');
        DB.connect(done);
    });

    beforeEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive leaderboard', async (done) => {
        let tokenObjClaimUser = {};
        let claimUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(testUtils.generateRandomUser(), tokenObjClaimUser, 'admin');
        let claim = testUtils.generateRndomClaimCreationRequest();
        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        await claimCreationWrapper.createClaim(claim, tokenObjClaimUser);
        let claimIds = await mongooseClaimRepo.findByOwnerId(claimUser._id);
        let theClaimId = String(claimIds[0]._id);

        let now = timestampService.createTimestamp();

        let tokenObjAdmin = {};
        let adminUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(testUtils.generateRandomUser(), tokenObjAdmin, 'admin');
        let users = [];

        for (let i = 0; i < 10; i++) {
            let user = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(), {});

            let randomEthAddress = testUtils.generateRandomEthAddress();

            users.push(user);
            await mongooseValidatorRepo.insert({
                user_id: user._id.toString(),
                ethaddress: randomEthAddress,
                username: user.username,
                email: user.email
            });

            let roundId = await mongooseVotingRoundRepo.insert({
                claim_id: 'x',
                end_registration: now,
                end_voting: now,
                status: 'x'
            });

            for (let j = 0; j < 10; j++) {
                await mongooseVoteRepo.insert({
                    reward: 1,
                    indorsed: testUtils.generateRandomBoolean(),
                    voter_id: user._id,
                    voting_round_id: roundId,
                    claim_id: theClaimId,
                    sc_vote_exists: true,
                    isVotingRoundOn: false,
                    voted_at: 1,
                    feedback: {
                        quality: 1
                    }
                })
            }
        }

        try {
            await computeValidatorStats.calculate();

            let getLeaderboard =
                `query {
                getLeaderboard(pageNumber: 1, pageSize: 10) {
                    leaderboard {
                        vote_participation
                        user_id
                        rewards
                        indorsed
                        flagged
                        consensus_percentage
                    }
                totalValidators                
                }
            }`;


            let res = await chai.request(server)
                .post('/graphql')
                .send({query: getLeaderboard});

            res.body.data.getLeaderboard.leaderboard.length.should.equal(10);
            should.exist(res.body.data.getLeaderboard.leaderboard[0].consensus_percentage);

            let getValidatorDetails =
                `query {
                getValidatorDetails(userId : "${users[0]._id.toString()}") {
                    votedClaims {
                        votedAt
                        feedback{
                            isCommentHidden
                            isAdminVoteOnComment
                            voteHiddenUsername
                        }
                    }
                    validator {
                        user_id
                    }
                    totalPage
                }
            }`;

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObjAdmin.token)
                .send({query: getValidatorDetails});

            should.exist(res.body.data.getValidatorDetails.votedClaims);
            should.exist(res.body.data.getValidatorDetails.validator);

            let getAdminValidatorsList =
                `query {
                getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation", period : daily, skill : "javascript" ) {
                    leaderboard {
                        vote_participation
                        user_id
                        rewards
                        indorsed
                        flagged
                        consensus_percentage
                        tier
                    }
                totalValidators
                }
            }`;

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObjAdmin.token)
                .send({query: getAdminValidatorsList});

            res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            //
            // getAdminValidatorsList =
            //     `query {
            //     getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation", period : weekly, skill : "javascript" ) {
            //         leaderboard {
            //             vote_participation
            //             user_id
            //             rewards
            //             indorsed
            //             flagged
            //             consensus_percentage
            //         }
            //     totalValidators
            //     }
            // }`;
            //
            // res = await chai.request(server)
            //     .post('/graphql')
            //     .send({ query: getAdminValidatorsList });
            //
            // res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            //
            // getAdminValidatorsList =
            //     `query {
            //     getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation", period : monthly, skill : "javascript" ) {
            //         leaderboard {
            //             vote_participation
            //             user_id
            //             rewards
            //             indorsed
            //             flagged
            //             consensus_percentage
            //         }
            //     totalValidators
            //     }
            // }`;
            //
            // res = await chai.request(server)
            //     .post('/graphql')
            //     .send({ query: getAdminValidatorsList });
            //
            // res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            //
            // getAdminValidatorsList =
            //     `query {
            //     getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation", period : yearly, skill : "javascript" ) {
            //         leaderboard {
            //             vote_participation
            //             user_id
            //             rewards
            //             indorsed
            //             flagged
            //             consensus_percentage
            //         }
            //     totalValidators
            //     }
            // }`;
            //
            // res = await chai.request(server)
            //     .post('/graphql')
            //     .send({ query: getAdminValidatorsList });
            //
            // res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            //
            // getAdminValidatorsList =
            //     `query {
            //     getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation", skill : "javascript" ) {
            //         leaderboard {
            //             vote_participation
            //             user_id
            //             rewards
            //             indorsed
            //             flagged
            //             consensus_percentage
            //         }
            //     totalValidators
            //     }
            // }`;
            //
            // res = await chai.request(server)
            //     .post('/graphql')
            //     .send({ query: getAdminValidatorsList });
            //
            // res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            //
            // getAdminValidatorsList =
            //     `query {
            //     getAdminValidatorsList(pageNumber: 1, pageSize: 10, sort : "vote_participation") {
            //         leaderboard {
            //             vote_participation
            //             user_id
            //             rewards
            //             indorsed
            //             flagged
            //             consensus_percentage
            //         }
            //     totalValidators
            //     }
            // }`;
            //
            // res = await chai.request(server)
            //     .post('/graphql')
            //     .send({ query: getAdminValidatorsList });
            //
            // res.body.data.getAdminValidatorsList.leaderboard.length.should.equal(10);
            // should.exist(res.body.data.getAdminValidatorsList.leaderboard[0].consensus_percentage);
            done();
        } catch (e) {
            console.log(e);
            done(e);
        }

    });
});