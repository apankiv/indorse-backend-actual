process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseValidatorRankRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRanksRepo');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const claimWrapper = require('../../claims/claimCreationWrapper');
const settings = require('../../../models/settings');
const sortValidators = require('../../../smart-contracts/services/cron/sortValidators');

chai.use(chaiHttp);
describe.skip('getTopRanks', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    // Test case: Assert the expected behaviour
    it('should successfully return 3 top users with highest reward within a week', async (done) => {

        // creating a user
        let curTimestamp = (new Date().getTime() / 1000);
        for (let i = 0; i < 10; i++) {
            let user = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(),{});
            await mongooseValidatorRepo.insert({user_id : user._id});
            for (let j = 0; j < 10; j++) {
                await mongooseVoteRepo.insert({
                    reward : testUtils.generateRandomNumber(),
                    indorsed : testUtils.generateRandomBoolean(),
                    voter_id : user._id,
                    voting_round_id : "x",
                    claim_id : "x",
                    sc_vote_exists : true,
                    voted_at : curTimestamp-10
                })
            }
        }

        await sortValidators.calculateAndSort();
        let fakeDocs = {rewardOneWeekRank: [], timestamp: curTimestamp-500}

        await mongooseValidatorRankRepo.insert(fakeDocs);
        let test = await mongooseValidatorRankRepo.findAll({});


                const queryName = 'getTopRanks';
                const queryBody = `
                    query {
                    leaderboards_getTopRanks (numberOfRankers:3) {
                   
                            user_id
                            name
                            username
                            img_url
                            current_role
                            rewards
                        
                    }
                    }
                `;

                let body = { query: queryBody };

                try {


                    let res = await chai.request(server)
                        .post('/graphql')
                        .send(body);
                    let responseData = res.body.data;

                    should.exist(responseData);
                    responseData.leaderboards_getTopRanks.length.should.equal(3);
                    let firstRankerResp = responseData.leaderboards_getTopRanks[0];
                    let secondRankerResp = responseData.leaderboards_getTopRanks[1];
                    let thirdRankerResp = responseData.leaderboards_getTopRanks[2];

                    let firstRankerReward = firstRankerResp.rewards.replace(">$", "");
                    let secondRankerReward = secondRankerResp.rewards.replace(">$", "");
                    let thirdRankerReward = thirdRankerResp.rewards.replace(">$", "");

                    (firstRankerReward-secondRankerReward>=0).should.equal(true);
                    (secondRankerReward-thirdRankerReward>=0).should.equal(true);


                    let firstUser = await mongooseUserRepo.findOneById(firstRankerResp.user_id);
                    firstUser.badges[0].should.equal('weeklyTop1ValidatorByReward')
                    let secondUser = await mongooseUserRepo.findOneById(secondRankerResp.user_id);
                    secondUser.badges[0].should.equal('weeklyTop2ValidatorByReward')
                    let thirdUser = await mongooseUserRepo.findOneById(thirdRankerResp.user_id);
                    thirdUser.badges[0].should.equal('weeklyTop3ValidatorByReward')


                    done();


                } catch (error) {
                    done(error);
                }

    });
})