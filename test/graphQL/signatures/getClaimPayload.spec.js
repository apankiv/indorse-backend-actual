process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

const settings = require('../../../models/settings');
const EVM_REVERT_ERR = 'VM Exception while processing transaction: revert';
const yesHash = '0x5950ebe73a617667a66f67d525282c827c82ef4d89ae8dcd8336d013773b6b7f';
const noHash = '0x1544e7eb480cc28803f7ff893bd999e62fb759c7d172fc27414594c5d9c925f2';
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseClaimSignaturesRepo = require('../../../models/services/mongo/mongoose/mongooseClaimSignaturesRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const checkClaimsJob = require('../../../smart-contracts/services/cron/checkClaimsJob');
const resolverLoop = require('../../../smart-contracts/services/cron/resolverLoop');
const claimSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/claimSignatureVerifierContract');
const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const mandrilRepo = require('../../../models/services/mongo/mongoose/mongooseMandrillConfig');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const web3 = require('../../smart-contracts/initializers/testWeb3Provider');
const sigUtil = require('eth-sig-util');
const ethUtil = require('ethereumjs-util');

chai.use(chaiHttp);

describe.skip('getClaimPayload', function () {
    this.timeout(config.get('test.timeout'));

    var voteVerifierSC;
    var claimVerifierSC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [voteVerifierSC, claimVerifierSC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should retrieve correct claim payload', async () => {

        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        let validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj); // create one claim

        let testedValidator = validators[0];
        let votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
        let vote = votes[0];
        let claim = await mongooseClaimsRepo.findOneById(vote.claim_id);
        let sig = await mongooseClaimSignaturesRepo.findOne({'payload.claim_id' : claim._id.toString()});

        let signerAddress = await claimSignatureVerifierContract.verify(
            sig.payload.claimant_id, sig.payload.proof, sig.payload.timestamp, sig.payload.deadline, sig.payload.skills,
            sig.payload.kind, sig.payload.voters, sig.payload.claim_id, sig.payload.version,
            sig.signature.v, sig.signature.r, sig.signature.s
        );
        signerAddress = signerAddress.toLowerCase();
        signerAddress.should.equal("0x3798Db4fB4719A16cAd492221c77F4e99b6F5d5F".toLowerCase());

    })
});