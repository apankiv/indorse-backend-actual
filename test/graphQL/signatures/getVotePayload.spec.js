process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

const settings = require('../../../models/settings');
const EVM_REVERT_ERR = 'VM Exception while processing transaction: revert';
const yesHash = '0x5950ebe73a617667a66f67d525282c827c82ef4d89ae8dcd8336d013773b6b7f';
const noHash = '0x1544e7eb480cc28803f7ff893bd999e62fb759c7d172fc27414594c5d9c925f2';
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const checkClaimsJob = require('../../../smart-contracts/services/cron/checkClaimsJob');
const resolverLoop = require('../../../smart-contracts/services/cron/resolverLoop');
const voteSignatureVerifierContract = require('../../../smart-contracts/services/verifiers/voteSignatureVerifierContract');
const mandrilRepo = require('../../../models/services/mongo/mongoose/mongooseMandrillConfig');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const web3 = require('../../smart-contracts/initializers/testWeb3Provider');
const sigUtil = require('eth-sig-util');
const ethUtil = require('ethereumjs-util');

chai.use(chaiHttp);

describe('getVotePayload', function () {
    this.timeout(config.get('test.timeout'));

    var voteVerifierSC;
    var claimVerifierSC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [voteVerifierSC, claimVerifierSC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should retrieve correct vote payload', async () => {

        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        let validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj); // create one claim
        await mongooseUserRepo.update({email: user.email}, {role: "admin"});

        let request = `					 
            query ($voteId: String!, $decision: String!, $skill : String!) {
					    getVotePayload(voteId: $voteId, decision: $decision, skill: $skill) {
					        types {
					            EIP712Domain {
					                name
					                type
					            }
					            Vote {
					                name 
					                type
					            }
					        }
                            domain {
                                 name
                                 version
                                 chainId
                                 verifyingContract
                                 salt
                            }
                            primaryType
                            message {
                                 claim_id
                                 decision
                                 timestamp
                                 version
                                 skill
                            }
					    } 
					 }
            `;


        let testedValidator = validators[0];
        let votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
        let vote = votes[0];

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: request, variables: {
                    voteId: vote._id.toString(), decision: "YES", skill: "TEST"
                }
            });

        // let result = await new Promise((resolve, reject) => web3.currentProvider.sendAsync(
        //     {
        //         method: "eth_signTypedData",
        //         params: [web3.eth.accounts[0], res.body.data.getVotePayload],
        //         from: web3.eth.accounts[0]
        //     }, function (err, result) {
        //         if (err) {
        //             reject(err);
        //         }
        //         resolve(result);
        //     }));


        const privateKey = ethUtil.sha3('cow');
        const address = ethUtil.bufferToHex(ethUtil.privateToAddress(privateKey));

        let data = res.body.data.getVotePayload;
        const sig = sigUtil.signTypedData(privateKey, {data: data});

        // const signature = result.result.substring(2);
        const signature = sig.substring(2);
        const r = "0x" + signature.substring(0, 64);
        const s = "0x" + signature.substring(64, 128);
        const v = parseInt(signature.substring(128, 130), 16);

        let voteMsg = res.body.data.getVotePayload.message;

        // , claim_id, decision, timestamp, version, skill, sigV, sigR, sigS
        // let isValid = await voteSignatureVerifierContract.verify(web3.eth.accounts[0],
        //     voteMsg.claim_id, voteMsg.decision, voteMsg.timestamp, voteMsg.version, voteMsg.skill, v, r, s
        // );

        let signerAddress = await voteSignatureVerifierContract.verify(
            voteMsg.claim_id, voteMsg.decision, voteMsg.timestamp, voteMsg.version, voteMsg.skill, v, r, s
        );

        let sep = await voteSignatureVerifierContract.getDOMAIN_SEPARATOR();

        console.log(sep);
        console.log(signerAddress);

        signerAddress.should.equal(address);

        request = `					 
            mutation ($voteId: String!, $message: VoteMsgInput!, $v: Int!, $r: String!, $s: String!) {
					    submitVoteSignature(voteId: $voteId, message: $message, v: $v, r: $r, s: $s) 
					 }
            `;

        await mongooseVoteRepo.update({_id : vote._id}, {$set : {voted_at : 555, endorsed : true}});
        await mongooseUserRepo.update({_id : testedValidator.user_id}, {$set : {ethaddress : address}});

        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
            .send({
                query: request, variables: {
                    voteId: vote._id.toString(), message: voteMsg, v: v, r: r, s: s
                }
            });

        console.log(res)
    })
});

// module.exports = `
// 	type Argument {
// 	    name: String!
// 	    type: String!
// 	}
//
// 	type Types {
// 	    EIP712Domain : [Argument]!
// 	    Vote: [Argument]!
// 	}
//
// 	type VoteMsg {
// 	    claim_id: String!
// 	    decision: String!
// 	    timestamp: Int!
// 	    version: String!
// 	    skill: String!
// 	}
//
// 	type Domain {
// 	    name: String!
// 	    version: String!
// 	    chainId: String!
// 	    verifyingContract: String!
// 	    salt: String!
// 	}
//
// 	type VotePayload {
// 	    types: Types!
// 	    domain: Domain!
// 	    primaryType: String!
// 	    message: VoteMsg
// 	}
//
// 	type Query {
// 		getVotePayload(voteId: String!, decision: String!): VotePayload
// 	}
// `;
