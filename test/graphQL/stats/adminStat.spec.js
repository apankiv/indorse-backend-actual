process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo')
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo')
const githubService = require('../../../models/services/social/githubService');
const mongooseUserGithubRepo = require('../../../models/services/mongo/mongoose/mongooseUserGithubRepo')
const mongooseClaimGithubRepo = require('../../../models/services/mongo/mongoose/mongooseClaimGithubsRepo')

chai.use(chaiHttp);

let adminStatsQuery = `query {
                            adminStats{
                                userStats {
                                  oneDay
                                  oneWeek
                                  oneMonth
                                  oneYear
                                  overall
                                }
                                claimsCreated {
                                  oneDay
                                  oneWeek
                                  oneMonth
                                  oneYear
                                  overall
                                }
                                claimsFlagged {
                                  oneDay
                                  oneWeek
                                  oneMonth
                                  oneYear
                                  overall
                                }
                                claimsApproved {
                                  oneDay
                                  oneWeek
                                  oneMonth
                                  oneYear
                                  overall
                                }
                                claimsIndorsed {
                                  oneDay
                                  oneWeek
                                  oneMonth
                                  oneYear
                                  overall
                                }
                            }
                        }`

describe('stats dashboard', function() {
    this.timeout(15000);
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('Should return total claims', async () => {


        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};

        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let claimsCount = 10;

        for (let index = 0; index < claimsCount; index++) {
            let randomClaim = testUtils.generateRandomClaim();
            // delete randomClaim['ownerid'];
            await mongooseClaimsRepo.insert(randomClaim);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: adminStatsQuery });

        res.body.data.adminStats.claimsCreated.overall.should.equal(claimsCount);
        // res.body.totalClaims.should.equal(claimsCount);
    });

    it('should not return any results for a non-admin user', async () => {
        let randomUser = testUtils.generateRandomUser();
        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

        let claimsCount = 10;

        for (let index = 0; index < claimsCount; index++) {
            let randomClaim = testUtils.generateRandomClaim();
            // delete randomClaim['ownerid'];
            await mongooseClaimsRepo.insert(randomClaim);
        }
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: adminStatsQuery });
        res.body.errors[0].message.should.equal("Insufficient role, requires:admin");
    });
});