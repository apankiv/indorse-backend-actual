process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo')
const githubService = require('../../../models/services/social/githubService');
const mongooseUserGithubRepo = require('../../../models/services/mongo/mongoose/mongooseUserGithubRepo')
const mongooseClaimGithubRepo = require('../../../models/services/mongo/mongoose/mongooseClaimGithubsRepo')

chai.use(chaiHttp);
describe('getClaimantGithub query AND githubService (parseGithubUrlExtractClaimantData) ', function () {
    this.timeout(15000);

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should parse URL of https://github.com/jaeseokan94/indorse-github-service-test and save correct data into claimGitub collection', async (done) => {
        this.timeout(15000);
        let user = testUtils.generateRandomUser();
        let githubUsername = 'jaeseokan94';
        let githubUid = testUtils.generateRandomString();
        let claimId = testUtils.generateRandomString();
        try {
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
            let githubUserAndRepoDataObj = {
                id: githubUid,
                login: githubUsername
            }
            await mongooseUserRepo.update({email: user.email}, {$set: {github_uid: githubUid}});
            user.github_uid = githubUid;
            await githubService.updateGithubDataOfUser(githubUserAndRepoDataObj, user);
            await githubService.parseGithubUrlExtractClaimantData(githubUid, 'https://github.com/jaeseokan94/indorse-github-service-test', claimId)
            let claimGithubDetils = await mongooseClaimGithubRepo.findOneByClaimId(claimId);

            claimGithubDetils.claim_id.should.equal(claimId);
            claimGithubDetils.totalNumberOfCommits.should.equal(Number(1));
            claimGithubDetils.totalNumberOfLinesAdded.should.equal(Number(2));
            claimGithubDetils.totalNumberOfLinesDeleted.should.equal(Number(0));
            claimGithubDetils.isClaimantOwner.should.equal(true);
            claimGithubDetils.commitHistoryArray.length.should.equal(1);
            claimGithubDetils.recentFiveCommitLinks.length.should.equal(1);
            claimGithubDetils.githubUsername.should.equal(githubUsername);
            should.exist(claimGithubDetils.claimantProfileURL);

            done();

        } catch (error) {
            done(error);
        }

    });

    it('should retrieve claim github data with getClaimantGithub query', async (done) => {
        this.timeout(15000);

        let claimId = testUtils.generateRandomString();
        let totalNumberOfCommits = 1;
        let totalNumberOfLinesAdded = 10;
        let totalNumberOfLinesDeleted = 20;
        let isFork = false;
        let isClaimantOwner = false;
        let forkUrl = testUtils.generateRandomString();
        let githubUrl = testUtils.generateRandomString();
        let commitHistoryArray = [{week: 1, noOfCommits: 1}, {week: 5, noOfCommits: 1}];
        let githubUsername = testUtils.generateRandomString();
        let claim = {
            claim_id: claimId,
            githubUsername: githubUsername,
            totalNumberOfCommits: totalNumberOfCommits,
            totalNumberOfLinesAdded: totalNumberOfLinesAdded,
            totalNumberOfLinesDeleted: totalNumberOfLinesDeleted,
            isFork: isFork,
            forkUrl: forkUrl,
            commitHistoryArray: commitHistoryArray,
            isClaimantOwner: isClaimantOwner,
            isReturnAllCommit: false,
            githubRepoURL: githubUrl
        }

        let getClaimantGithubRequest = `					 
          query($claimId :String = "${claimId}"){
  getClaimantGithub(filter: {claim_id: $claimId}){
    schemaType {
      claim_id
      githubRepoURL
      githubUsername
      totalNumberOfCommits
      totalNumberOfLinesAdded
      totalNumberOfLinesDeleted
      createdAt
      forkUrl
      isFork
      isClaimantOwner
      isReturnAllCommit
      commitHistoryAllArray
      _id
      recentFiveCommitLinks{
        commitUrl
        commitMessage
      }
      commitHistoryArray{
        week
        noOfCommits
      }
    }
    
  }
}
            `;
        try {

            let claimInserted = await mongooseClaimGithubRepo.insert(claim);

            let res = await chai.request(server)
                .post('/graphql')
                .send({query: getClaimantGithubRequest});
            let claimGithubDetils = res.body.data.getClaimantGithub.schemaType;
            claimGithubDetils.claim_id.should.equal(claimId);
            claimGithubDetils.totalNumberOfCommits.should.equal(totalNumberOfCommits);
            claimGithubDetils.totalNumberOfLinesAdded.should.equal(totalNumberOfLinesAdded);
            claimGithubDetils.totalNumberOfLinesDeleted.should.equal(totalNumberOfLinesDeleted);
            claimGithubDetils.isClaimantOwner.should.equal(isFork);
            claimGithubDetils.commitHistoryArray.length.should.equal(commitHistoryArray.length);
            claimGithubDetils.commitHistoryAllArray.length.should.equal(0);
            claimGithubDetils.isClaimantOwner.should.equal(isClaimantOwner);
            claimGithubDetils.githubUsername.should.equal(githubUsername)
            claimGithubDetils.forkUrl.should.equal(forkUrl)
            claimGithubDetils.githubRepoURL.should.equal(githubUrl)
            done();

        } catch (error) {
            done(error);
        }

    });

    it('should fail claimID is not given in the input', async (done) => {

        let claimId = testUtils.generateRandomString();
        let totalNumberOfCommits = 1;
        let totalNumberOfLinesAdded = 10;
        let totalNumberOfLinesDeleted = 20;
        let isFork = false;
        let githubUsername = testUtils.generateRandomString();
        let isClaimantOwner = false;
        let commitHistoryArray = [{week: 1, noOfCommits: 2}];
        let claim = {
            githubUsername: githubUsername,
            claim_id: claimId,
            totalNumberOfCommits: totalNumberOfCommits,
            totalNumberOfLinesAdded: totalNumberOfLinesAdded,
            totalNumberOfLinesDeleted: totalNumberOfLinesDeleted,
            isFork: isFork,
            commitHistoryArray: commitHistoryArray,
            isClaimantOwner: isClaimantOwner,
            isReturnAllCommit: false,
            githubRepoURL: 'https://github.com/indorseio/code'
        }

        let getClaimantGithubRequest = `					 
          query {
  getClaimantGithub(){
    schemaType {
      claim_id
      totalNumberOfCommits
      totalNumberOfLinesAdded
      totalNumberOfLinesDeleted
      createdAt
      isFork
      isClaimantOwner
      _id
      isReturnAllCommit
      githubRepoURL
      recentFiveCommitLinks{
        commitUrl
        commitMessage
      }
      commitHistoryArray{
        week
        noOfCommits
      }
    }
    
  }
}
            `;
        try {

            await mongooseClaimGithubRepo.insert(claim);

            let res = await chai.request(server)
                .post('/graphql')
                .send({query: getClaimantGithubRequest});
            throw new Error("Expected to fail!");


        } catch (error) {
            error.status.should.equal(400);
            done();
        }

    });


})

