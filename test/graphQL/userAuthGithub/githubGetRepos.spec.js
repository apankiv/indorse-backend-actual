process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo')
const githubService = require('../../../models/services/social/githubService');
const mongooseUserGithubRepo = require('../../../models/services/mongo/mongoose/mongooseUserGithubRepo')
const sinon = require('sinon')


chai.use(chaiHttp);
describe('Github GetRepos test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should fetch all repositories when language is not defined', async (done) => {
        let user = testUtils.generateRandomUser();
        let tokenObj = {};
        let githubUid = testUtils.generateRandomString();


        let userGithubObj = {
            "login" : "jaeseokan94",
            "id" : githubUid,
            "bio" : "I enjoy algorithms, competitive programming and machine learning. ",
            "name" : "Jae Seok Jay An",
            "company" : "",
            "createdAt" : "2015-07-28T09:27:15Z",
            "avatarUrl" : "https://avatars2.githubusercontent.com/u/13536257?v=4",
            "isBountyHunter" : false,
            "isHireable" : true,
            "isEmployee" : false,
            "websiteUrl" : "https://uk.linkedin.com/pub/jae-seok-an/b8/ab/478",
            "email" : "jaeseokan94@gmail.com",
            "languages" : {
                "nodes" : [
                    {
                        "name" : "JavaScript"
                    },
                    {
                        "name" : "Python"
                    }
                ]
            },
            "repositories" : {
                "edges" : [
                    {
                        "node" : {
                            "updatedAt" : "2015-10-31T11:22:36Z",
                            "createdAt" : "2015-09-26T11:19:54Z",
                            "forkCount" : 7,
                            "isPrivate" : "false",
                            "isArchived" : false,
                            "description" : null,
                            "projectsUrl" : "https://github.com/ambujone/UnityInsurance/projects",
                            "name" : "UnityInsurance",
                            "languages" : {
                                "nodes" : [
                                    {
                                        "name" : "JavaScript"
                                    }
                                    ]
                            },

                            "owner" : {
                                "id" : "MDQ6VXNlcjM2NDk3NDQ="
                            }
                        }
                    },
                    {
                        "node" : {
                            "updatedAt" : "2016-07-27T07:02:52Z",
                            "createdAt" : "2015-09-28T13:02:26Z",
                            "forkCount" : 0,
                            "isPrivate" : "false",
                            "isArchived" : false,
                            "description" : "minimum fuction of twitter.",
                            "projectsUrl" : "https://github.com/jaeseokan94/twitter-ruby/projects",
                            "name" : "twitter-ruby",
                            "languages" : {
                                "nodes" : [
                                    {
                                        "name" : "Python"
                                    },
                                    {
                                        "name" : "CoffeeScript"
                                    }
                                ]
                            },
                            "stargazers" : [
                                {
                                    "totalCount" : 3,
                                }
                            ],
                            "owner" : {
                                "id" : "MDQ6VXNlcjEzNTM2MjU3"
                            }
                        }
                    },
                    {
                        "node" : {
                            "updatedAt" : "2015-10-02T17:10:09Z",
                            "createdAt" : "2015-10-02T13:39:01Z",
                            "forkCount" : 1,
                            "isPrivate" : "false",
                            "isArchived" : false,
                            "description" : null,
                            "projectsUrl" : "https://github.com/oOPa/gitsoc/projects",
                            "name" : "gitsoc",
                            "languages" : {
                                "nodes" : [
                                    {
                                        "name" : "HTML"
                                    },
                                    {
                                        "name" : "JavaScript"
                                    },
                                    {
                                        "name" : "Python"
                                    }
                                ]
                            },
                            "stargazers" : [
                                {
                                    "totalCount" : 1
                                }
                            ],
                            "owner" : {
                                "id" : "MDQ6VXNlcjU4Nzg2NTA="
                            }
                        }
                    }

                ]
            }
        }

        await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
        await mongooseUserRepo.update({email: user.email},{github_uid: githubUid});
        await mongooseUserGithubRepo.insert(userGithubObj);
        let fetchRepoRequest = `					 
            query {
					    githubGetRepos(languages: []
					                ){
					        name
					        username
                            avatar
                            repos{
                                title   
                                url
                            }
				
					    }
					 }
            `;



        try {
            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: fetchRepoRequest});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.githubGetRepos.repos.length.should.equal(3);


            done();


        } catch (error) {
            done(error);
        }

    });



    it('should fetch 2 JavaScript repositories', async (done) => {
    let user = testUtils.generateRandomUser();
    let tokenObj = {};
    let githubUid = testUtils.generateRandomString();


    let userGithubObj = {
        "login" : "jaeseokan94",
        "id" : githubUid,
        "bio" : "I enjoy algorithms, competitive programming and machine learning. ",
        "name" : "Jae Seok Jay An",
        "company" : "",
        "createdAt" : "2015-07-28T09:27:15Z",
        "avatarUrl" : "https://avatars2.githubusercontent.com/u/13536257?v=4",
        "isBountyHunter" : false,
        "isHireable" : true,
        "isEmployee" : false,
        "websiteUrl" : "https://uk.linkedin.com/pub/jae-seok-an/b8/ab/478",
        "email" : "jaeseokan94@gmail.com",
        "languages" : {
            "nodes" : [
                {
                    "name" : "JavaScript"
                },
                {
                    "name" : "Python"
                }
            ]
        },
        "repositories" : {
            "edges" : [
                {
                    "node" : {
                        "updatedAt" : "2015-10-31T11:22:36Z",
                        "createdAt" : "2015-09-26T11:19:54Z",
                        "forkCount" : 7,
                        "isPrivate" : "false",
                        "isArchived" : false,
                        "description" : null,
                        "projectsUrl" : "https://github.com/ambujone/UnityInsurance/projects",
                        "name" : "UnityInsurance",
                        "languages" : {
                            "nodes" : [
                                {
                                    "name" : "JavaScript"
                                }
                            ]
                        },

                        "owner" : {
                            "id" : "MDQ6VXNlcjM2NDk3NDQ="
                        }
                    }
                },
                {
                    "node" : {
                        "updatedAt" : "2016-07-27T07:02:52Z",
                        "createdAt" : "2015-09-28T13:02:26Z",
                        "forkCount" : 0,
                        "isPrivate" : "false",
                        "isArchived" : false,
                        "description" : "minimum fuction of twitter.",
                        "projectsUrl" : "https://github.com/jaeseokan94/twitter-ruby/projects",
                        "name" : "twitter-ruby",
                        "languages" : {
                            "nodes" : [
                                {
                                    "name" : "Python"
                                },
                                {
                                    "name" : "CoffeeScript"
                                }
                            ]
                        },
                        "stargazers" : [
                            {
                                "totalCount" : 3,
                            }
                        ],
                        "owner" : {
                            "id" : "MDQ6VXNlcjEzNTM2MjU3"
                        }
                    }
                },
                {
                    "node" : {
                        "updatedAt" : "2015-10-02T17:10:09Z",
                        "createdAt" : "2015-10-02T13:39:01Z",
                        "forkCount" : 1,
                        "isPrivate" : "false",
                        "isArchived" : false,
                        "description" : null,
                        "projectsUrl" : "https://github.com/oOPa/gitsoc/projects",
                        "name" : "gitsoc",
                        "languages" : {
                            "nodes" : [
                                {
                                    "name" : "HTML"
                                },
                                {
                                    "name" : "JavaScript"
                                },
                                {
                                    "name" : "Python"
                                }
                            ]
                        },
                        "stargazers" : [
                            {
                                "totalCount" : 1
                            }
                        ],
                        "owner" : {
                            "id" : "MDQ6VXNlcjU4Nzg2NTA="
                        }
                    }
                }

            ]
        }
    }

    await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
    await mongooseUserRepo.update({email: user.email},{github_uid: githubUid});
    await mongooseUserGithubRepo.insert(userGithubObj);
    let fetchRepoRequest = `					 
            query {
					    githubGetRepos(languages: ["javascript"]
					                ){
					        name
					        username
                            avatar
                            repos{
                                title   
                                url
                            }
				
					    }
					 }
            `;



    try {
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: fetchRepoRequest});
        let responseData = res.body.data;

        should.exist(responseData);
        responseData.githubGetRepos.repos.length.should.equal(2);


        done();


    } catch (error) {
        done(error);
    }

});

})

