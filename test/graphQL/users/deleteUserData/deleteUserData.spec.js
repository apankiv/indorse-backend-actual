process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../../db');
const expect = chai.expect;
const server = require('../../../../server');
const should = chai.should();
const testUtils = require('../../../testUtils');
const authenticationWrapper = require('../../../authenticationWrapper');
const mongoUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo')


chai.use(chaiHttp);

describe('users.deleteUserData', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    }); 

    it('Should scramble users data in platform', async () => {
        try {

            let adminUser = testUtils.generateRandomUser();
            let tokenObjAdmin = {};

            let randomUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObjAdmin, 'admin');
            let userAdm = await mongoUserRepo.findOneByEmail(adminUser.email);

            await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'profile_access');
            let userRandom = await mongoUserRepo.findOneByEmail(randomUser.email);

            const variables1 = {
                userid: userRandom._id.toString()
            };                   

            const operationName = 'deleteUserData';
            const mutationGql = `
                mutation ${operationName}($userid: String!) {
                    deleteUserData(userid:$userid)
                }
            `;

            let body1 = { operationName: operationName, query: mutationGql, variables: variables1};
            try{

            
                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObjAdmin.token)
                    .send(body1);          

            
                
                console.log("Scrambled : " + userRandom._id.toString());

                res.body.errors[0].message.should.equal('User has not requested deletion!');

                //Update deletion via query
                let requestToDelete = {
                    request_type: 'getdata',
                    timestamp : 10000
                }

                await mongoUserRepo.update({ email: randomUser.email }, { $push: { data_request: requestToDelete } });

                res = await chai.request(server)
                        .post('/graphql')
                        .set('Authorization', 'Bearer ' + tokenObjAdmin.token)
                        .send(body1);

                res.body.errors[0].message.should.equal('User has not requested deletion!');


                requestToDelete.request_type = 'delete'

                await mongoUserRepo.update({ email: randomUser.email }, { $push: { data_request: requestToDelete } });            

                res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObjAdmin.token)
                    .send(body1);
                                
                
                console.log("Done " + JSON.stringify(res.body));
                res.body.data.deleteUserData.should.equal(true)


            } catch(e){
                console.log("Exception " + JSON.stringify(e))
            }

        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});
