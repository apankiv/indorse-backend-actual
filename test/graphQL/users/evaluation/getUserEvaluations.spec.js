process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../../db');
const server = require('../../../../server');
const testUtils = require('../../../testUtils');
const authenticationWrapper = require('../../../authenticationWrapper');
const companyCreationWrapper = require('../../../../test/companies/companyCreationWrapper');
const mongooseAssessmentMagicRepo = require('../../../../models/services/mongo/mongoose/mongooseAssessmentMagicLinkRepo');
const networkInitialize = require('../../../smart-contracts/initializers/networkInitialize');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const chatbotWrapper = require('../../../chatbot/userChatbotCompleteWrapper');

chai.use(chaiHttp);

let SC;

describe('User.getUserEvaluation', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await networkInitialize.initialize();
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should getUserEvaluation of an admin user', async () => {
        const { tokenObj } = await authenticationWrapper.createAndGetAdminUser();
        const getUserEvaluationQuery = `
            query getUserEvaluations {
                getUserEvaluations {
                    claims {
                        totalClaims
                        claims {
                            _id
                        }
                        matchingClaims
                    }
                    assignments {
                        matchingAssignments
                        totalAssignments
                        assignments {
                            _id
                        }
                    }
                    chatbots {
                        chatbots {
                            skill {
                                _id
                            }
                        }
                        totalChatbots
                        matchingChatbots
                    }
                }
            }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getUserEvaluationQuery });
        const response = res.body.data.getUserEvaluations;
        const { claims, assignments, chatbots } = response;
        claims.totalClaims.should.equal(0);
        claims.claims.length.should.equal(0);
        claims.matchingClaims.should.equal(0);
        assignments.totalAssignments.should.equal(0);
        assignments.assignments.length.should.equal(0);
        assignments.matchingAssignments.should.equal(0);
        chatbots.totalChatbots.should.equal(0);
        chatbots.chatbots.length.should.equal(0);
        chatbots.matchingChatbots.should.equal(0);
    });

    it('should getUserEvaluation of a non admin user', async () => {
        const user = testUtils.generateRandomUser();
        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
        const getUserEvaluationQuery = `
            query getUserEvaluations {
                getUserEvaluations {
                    claims {
                        totalClaims
                        claims {
                            _id
                        }
                        matchingClaims
                    }
                    assignments {
                        matchingAssignments
                        totalAssignments
                        assignments {
                            _id
                        }
                    }
                    chatbots {
                        chatbots {
                            skill {
                                _id
                            }
                        }
                        totalChatbots
                        matchingChatbots
                    }
                }
            }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', `Bearer ${tokenObj.token}`)
            .send({ query: getUserEvaluationQuery });
        const response = res.body.data.getUserEvaluations;
        const { claims, assignments, chatbots } = response;
        claims.totalClaims.should.equal(0);
        claims.claims.length.should.equal(0);
        claims.matchingClaims.should.equal(0);
        assignments.totalAssignments.should.equal(0);
        assignments.assignments.length.should.equal(0);
        assignments.matchingAssignments.should.equal(0);
        chatbots.totalChatbots.should.equal(0);
        chatbots.chatbots.length.should.equal(0);
        chatbots.matchingChatbots.should.equal(0);
    });

    it('should not getUserEvaluation of a non login user', async () => {
        const getUserEvaluationQuery = `
            query getUserEvaluations {
                getUserEvaluations {
                    claims {
                        totalClaims
                        claims {
                            _id
                        }
                        matchingClaims
                    }
                    assignments {
                        matchingAssignments
                        totalAssignments
                        assignments {
                            _id
                        }
                    }
                    chatbots {
                        chatbots {
                            skill {
                                _id
                            }
                        }
                        totalChatbots
                        matchingChatbots
                    }
                }
            }
        `;

        const res = await chai.request(server)
            .post('/graphql')
            .send({ query: getUserEvaluationQuery });
        chai.should().exist(res.body.errors);
    });
});
