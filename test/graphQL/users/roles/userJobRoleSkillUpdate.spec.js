process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../../db');
const expect = chai.expect;
const server = require('../../../../server');
const should = chai.should();
const testUtils = require('../../../testUtils');
const authenticationWrapper = require('../../../authenticationWrapper');
const mongoUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRolesRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo')
const mongooseSkillRepo = require('../../../../models/services/mongo/mongoose/mongooseSkillRepo')


chai.use(chaiHttp);

describe('users.jobRole Skill Update', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    }); 

    it('user skill selection should include skills from all tags', async () => {
        try {
                // Arrange
                const adminUser = testUtils.generateRandomUser();
                const skillTag1 = 'tag1';
                const skillTag2 = 'tag2';
                const skillTag3 = 'tag3';

                const randomJobRole = testUtils.generateRandomJobRole();
                randomJobRole.skillTags = [skillTag2, skillTag3];
                const randomJobRoleId = await mongooseJobRolesRepo.insert(
                    randomJobRole
                );

                //Create skills
                let skill = {
                    name: testUtils.generateRandomSkill(),
                    category: testUtils.generateRandomSkill(),
                };

                skill.tags = [skillTag1, skillTag2];
                let skillid1  = await mongooseSkillRepo.insert(skill);

                skill.tags = [skillTag1];
                let skillid2 = await mongooseSkillRepo.insert(skill);

                skill.tags = [skillTag3];
                let skillid3 = await mongooseSkillRepo.insert(skill);


                const adminTokenObj = {};
                await authenticationWrapper.signupVerifyAuthenticateWithRole(
                    adminUser,
                    adminTokenObj,
                    'admin'
                );

                //Add one skill to user
                let updateObj = [];
                let obj = {
                    skill: {
                        name: 'test',
                        category: 'tmp',
                        level: 'beginner',
                        _id: skillid1,
                    },
                };
                updateObj.push(obj);

                await mongoUserRepo.update(
                    { email: email },
                    { $set: { skills: updateObj } }
                );


                let variables1 = {
                    roleid: randomJobRoleId,
                    skillIds: [skillid1]
                };             

                const operationName = 'userJobRoleSkillUpdate';
                const mutationGql = `
                mutation ${operationName}($roleid : String! , $skillIds: [String]!) {
                    userJobRoleSkillUpdate(roleid : $roleid , skillIds: $skillIds)
                }
            `;

            let body1 = { operationName: operationName, query: mutationGql, variables: variables1 };

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send(body1); 

            res.body.errors[0].message.should.equal("Please select atleast one skill from each tag - tag3")


            let variables2 = {
                roleid: randomJobRoleId,
                skillIds: [skillid1, skillid3]
            };           

            let body2 = { operationName: operationName, query: mutationGql, variables: variables2 };

            let res2 = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send(body2); 

            res2.body.data.userJobRoleSkillUpdate.should.equal(true);
            user = await mongoUserRepo.findOneByEmail(adminUser.email);
            user.skills.length.should.equal(2);

        } catch (err) {
            console.log(err);
            throw err;
        }
    });


    it('should not overwrite existing validated skill from user', async () => {
        try {
            //Unslected case

            // Arrange
            const adminUser = testUtils.generateRandomUser();
            const skillTag1 = 'tag1';
            const skillTag2 = 'tag2';
            const skillTag3 = 'tag3';

            const randomJobRole = testUtils.generateRandomJobRole();
            randomJobRole.skillTags = [skillTag2, skillTag3];
            const randomJobRoleId = await mongooseJobRolesRepo.insert(
                randomJobRole
            );

            //Create skills
            let skill = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };

            skill.tags = [skillTag1, skillTag2];
            let skillid1 = await mongooseSkillRepo.insert(skill);

            skill.tags = [skillTag1];
            let skillid2 = await mongooseSkillRepo.insert(skill);

            skill.tags = [skillTag3];
            let skillid3 = await mongooseSkillRepo.insert(skill);

            const adminTokenObj = {};
            await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                adminTokenObj,
                'admin'
            );

            //Add one skill to user
            let updateObj = [];
            let obj = {
                skill: {
                    name: 'test',
                    category: 'tmp',
                    level: 'beginner',
                    _id: skillid2,
                },
                validations: [{
                    type: "claim",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                {
                    type: "claim",
                    level: "expert",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            };
            updateObj.push(obj);

            await mongoUserRepo.update(
                { email: email },
                { $set: { skills: updateObj } }
            );


            const operationName = 'userJobRoleSkillUpdate';
            const mutationGql = `
                mutation ${operationName}($roleid : String! , $skillIds: [String]!) {
                    userJobRoleSkillUpdate(roleid : $roleid , skillIds: $skillIds)
                }
            `;

            let variables1 = {
                roleid: randomJobRoleId,
                skillIds: [skillid3,skillid1]
            };

            let body1 = { operationName: operationName, query: mutationGql, variables: variables1 };

            let res1 = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send(body1);

            
            res1.body.data.userJobRoleSkillUpdate.should.equal(true);
            user = await mongoUserRepo.findOneByEmail(adminUser.email); 
            
            
            user.skills.length.should.equal(3);

        } catch (err) {
            console.log(err);
            throw err;
        }
    });



    it('should not overwrite existing validated skill from user', async () => {
        try {
            //Selected case

            // Arrange
            const adminUser = testUtils.generateRandomUser();
            const skillTag1 = 'tag1';
            const skillTag2 = 'tag2';
            const skillTag3 = 'tag3';

            const randomJobRole = testUtils.generateRandomJobRole();
            randomJobRole.skillTags = [skillTag1, skillTag3];
            const randomJobRoleId = await mongooseJobRolesRepo.insert(
                randomJobRole
            );

            //Create skills
            let skill = {
                name: testUtils.generateRandomSkill(),
                category: testUtils.generateRandomSkill(),
            };

            skill.tags = [skillTag1, skillTag2];
            let skillid1 = await mongooseSkillRepo.insert(skill);

            skill.tags = [skillTag1];
            let skillid2 = await mongooseSkillRepo.insert(skill);

            skill.tags = [skillTag3];
            let skillid3 = await mongooseSkillRepo.insert(skill);


            const adminTokenObj = {};
            await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                adminTokenObj,
                'admin'
            );

            //Add one skill to user
            let updateObj = [];
            let obj = {
                skill: {
                    name: 'test',
                    category: 'tmp',
                    level: 'beginner',
                    _id: skillid2,
                },
                validations: [{
                    type: "claim",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                {
                    type: "claim",
                    level: "expert",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: true
                }]
            };
            updateObj.push(obj);

            await mongoUserRepo.update(
                { email: email },
                { $set: { skills: updateObj } }
            );


            const operationName = 'userJobRoleSkillUpdate';
            const mutationGql = `
                mutation ${operationName}($roleid : String! , $skillIds: [String]!) {
                    userJobRoleSkillUpdate(roleid : $roleid , skillIds: $skillIds)
                }
            `;

            let variables1 = {
                roleid: randomJobRoleId,
                skillIds: [skillid3, skillid2]
            };

            let body1 = { operationName: operationName, query: mutationGql, variables: variables1 };

            let res1 = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send(body1);


            res1.body.data.userJobRoleSkillUpdate.should.equal(true);
            user = await mongoUserRepo.findOneByEmail(adminUser.email);
            user.skills.length.should.equal(2);

            for(skill of user.skills){
                if (skill.skill._id.toString() == skillid2){
                    should.exist(skill.validations)
                }
            }

        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});
