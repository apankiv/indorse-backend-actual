process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../../db');
const expect = chai.expect;
const server = require('../../../../server');
const should = chai.should();
const testUtils = require('../../../testUtils');
const authenticationWrapper = require('../../../authenticationWrapper');
const mongoUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseJobRoleRepo = require('../../../../models/services/mongo/mongoose/mongooseJobRoleRepo')


chai.use(chaiHttp);

describe('users.roleUpdate', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    }); 

    it('Should update user role correctly', async () => {
        try {

            let adminUser = testUtils.generateRandomUser();
            let randomRole = testUtils.generateRandomJobRole();
            let roleid = await mongooseJobRoleRepo.insert(randomRole);
            let roleid2 = await mongooseJobRoleRepo.insert(testUtils.generateRandomJobRole());
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            let user = await mongoUserRepo.findOneByEmail(adminUser.email);

            const variables1 = {
                role: roleid,
            };                   

            const operationName = 'RoleUpdate';
            const mutationGql = `
                mutation ${operationName}($role: String!) {
                    userJobRoleUpdate(role:$role)
                }
            `;

            let body1 = { operationName: operationName, query: mutationGql, variables: variables1};

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(body1);          

           
            //Add same role again
            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(body1);                

            user = await mongoUserRepo.findOneByEmail(adminUser.email);
            user.job_roles.length.should.equal(1);            

            const variables2 = {
                role: roleid2,
            };             

            let body2 = { operationName: operationName, query: mutationGql, variables: variables2 };
            //Add new role
            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(body2);     
                
            user = await mongoUserRepo.findOneByEmail(adminUser.email);
            user.job_roles.length.should.equal(1);

        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});
