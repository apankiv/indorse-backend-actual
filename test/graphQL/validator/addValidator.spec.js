process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseValidatorRepo =  require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const skillTestHelper = require('../../users/skills/skillTestHelper');

chai.use(chaiHttp);

describe('addValidator resolver', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('addValidator tests', () => {

        it('it should add a validator', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'Javascript';
            await skillTestHelper.insertAIPSkill(skillNameToAdd.toLowerCase());
            // insert validator
            let tokenObj = {};
            let userToCreate = testUtils.generateRandomUser();
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            await mongooseUserRepo.update({_id: user._id}, {role: 'admin'});
            let ethAddress = testUtils.generateRandomEthAddress();
            await mongooseUserRepo.update({_id: user._id}, {ethaddress: ethAddress});
            const gql = `
                mutation addValidator{
                   addValidator(userId: "${user._id}", skills: [${skillNameToAdd}])
                        
                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.addValidator;
                let newValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                should.exist(newValidator);


                done();
            }catch(e){
                done(e);
            }

        });

        it('it should add a validator with csharp', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'C#';
            await skillTestHelper.insertAIPSkill(skillNameToAdd.toLowerCase());
            // insert validator
            let tokenObj = {};
            let userToCreate = testUtils.generateRandomUser();
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            let ethAddress = testUtils.generateRandomEthAddress();
            await mongooseUserRepo.update({_id: user._id}, {role: 'admin', ethaddress: ethAddress});
            const gql = `
                mutation addValidator{
                   addValidator(userId: "${user._id}", skills: [CSHARP])
                        
                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.addValidator;
                let newValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                should.exist(newValidator);


                done();
            }catch(e){
                done(e);
            }

        });




    });
});