process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const DB = require('../../db');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const mongooseValidatorRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const skillTestHelper = require('../../users/skills/skillTestHelper');
const validatorTestHelper = require('./validatorTestHelper');
const validatorSchema = require('../../../models/services/mongo/mongoose/schemas/validator');

chai.use(chaiHttp);

describe('updateValidatorTiers resolver', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('deleteValidator tests', () => {


        it('it should mark validator as delete true', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'Javascript';
            let userToCreate = testUtils.generateRandomUser();
            let tokenObj = {};
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            await validatorTestHelper.createSkillAndValidator(skillNameToAdd, user._id ,tokenObj);


            const gql = `
                mutation deleteValidator{
                   deleteValidator(userId: "${user._id}")                        
                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.deleteValidator;
                let deletedValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                deletedValidator.delete.should.equal(true);
                deletedValidator.skills.length.should.equal(0);

                done();
            }catch(e){
                done(e);
            }

        });



    });
});