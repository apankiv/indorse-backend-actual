process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const DB = require('../../db');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const skillTestHelper = require('../../users/skills/skillTestHelper');
const validatorTestHelper = require('./validatorTestHelper');
const mongooseValidatorRepo =  require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const mongooseUserRepo =  require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const validatorSchema = require('../../../models/services/mongo/mongoose/schemas/validator')

chai.use(chaiHttp);

describe('updateValidatorSkill resolver', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('updateValidator tests', () => {

        it('it should add a new skill to a validator', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'Javascript';
            let userToCreate = testUtils.generateRandomUser();
            let tokenObj = {};
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            await validatorTestHelper.createSkillAndValidator(skillNameToAdd, user._id ,tokenObj);

            let newSkillNameToAdd = 'Ruby';
            await skillTestHelper.insertAIPSkill(newSkillNameToAdd.toLowerCase());
            await mongooseUserRepo.update({_id: user._id}, {role: 'admin'});


            const gql = `
                mutation updateValidator{
                   updateValidator(userId: "${user._id}", skills: [${skillNameToAdd},${newSkillNameToAdd}], tier: ${validatorSchema.tiers.TIER1}  )                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidator;
                let updatedValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                updatedValidator.skills.length.should.equal(2);
                updatedValidator.skills[1].should.equal(newSkillNameToAdd);
                updatedValidator.tier.should.equal(validatorSchema.tiers.TIER1);



                done();
            }catch(e){
                done(e);
            }

        });


        it('it should remove a skill to a validator', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'Javascript';
            let userToCreate = testUtils.generateRandomUser();
            let tokenObj = {};
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            await validatorTestHelper.createSkillAndValidator(skillNameToAdd, user._id ,tokenObj);
            await mongooseUserRepo.update({_id: user._id}, {role: 'admin'});


            const gql = `
                mutation updateValidator{
                   updateValidator(userId: "${user._id}", skills: [] )
                        
                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidator;
                let updatedValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                updatedValidator.skills.length.should.equal(0);

                done();
            }catch(e){
                done(e);
            }

        });

        it('it should update a validators tier to tier1', async (done) => {

            // create a skill javascript
            let skillNameToAdd = 'Javascript';
            let userToCreate = testUtils.generateRandomUser();
            let tokenObj = {};
            let user = await authenticationWrapper.signupVerifyAuthenticate(userToCreate, tokenObj);
            await validatorTestHelper.createSkillAndValidator(skillNameToAdd, user._id ,tokenObj);
            await mongooseUserRepo.update({_id: user._id}, {role: 'admin'});


            const gql = `
                mutation updateValidator{
                   updateValidator(userId: "${user._id}", tier: ${validatorSchema.tiers.TIER1} )                        
                   
                }
            `;

            try{

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidator;
                let updatedValidator = await mongooseValidatorRepo.findOne({user_id: user._id});
                updatedValidator.tier.should.equal(validatorSchema.tiers.TIER1);
                updatedValidator.skills.length.should.equal(1);
                done();
            }catch(e){
                done(e);
            }

        });



    });
});