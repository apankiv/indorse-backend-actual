const skillTestHelper = require('../../users/skills/skillTestHelper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const testUtils = require('../../testUtils');
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');

exports.createSkillAndValidator = async function createSkillAndValidator(skillName, userId, tokenObj){
    let ethAddress = testUtils.generateRandomEthAddress();
    await skillTestHelper.insertAIPSkill(skillName.toLowerCase());
    await mongooseUserRepo.update({_id: userId}, {ethaddress: ethAddress, role: 'admin'});

    const gql = `
                mutation addValidator{
                   addValidator(userId: "${userId}", skills: [${skillName}])
                        
                   
                }
            `;

    let res = await chai.request(server)
        .post('/graphql')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({query: gql});
}