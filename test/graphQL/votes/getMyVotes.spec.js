process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const DB = require('../../db');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');

chai.use(chaiHttp);

describe.skip('getUserVotesService.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('votes_getPastVotedClaims', () => {

        it('it should return user votes', async (done) => {

            const gql = `
                query votes_getPastVotedClaims {
                    votes_getPastVotedClaims(pageNumber : 1, pageSize: 5){
                        votedClaims{
                            claim{
                                ownerName
                                title
                                level
                                _id
                                claimStatus
                            }
                            voteStatus
                            votedAt
                            reward 
                            voteId
                        }
                        totalPage
                        }
                        
                    
                }
            `;

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let createClaimRequest2 = testUtils.generateRndomClaimCreationRequest();
            let createClaimRequest3 = testUtils.generateRndomClaimCreationRequest();

            let user = testUtils.generateRandomUser();
            let user2 = testUtils.generateRandomUser();
            let user3 = testUtils.generateRandomUser();

            let tokenObj = {};
            let tokenObj2 = {};
            let tokenObj3 = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
            let secondUser = await claimCreationWrapper.authenticateAndCreateClaim(user2, createClaimRequest2, tokenObj2);
            let claim2 = await mongooseClaimRepo.findByOwnerId(secondUser._id);
            let thirdUser = await claimCreationWrapper.authenticateAndCreateClaim(user3, createClaimRequest3, tokenObj3);
            let claim3 = await mongooseClaimRepo.findByOwnerId(thirdUser._id);

            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);
            await mongooseVoteRepo.update({_id:vote[0]._id},{voter_id:createdUserId, sc_vote_exists: true, isVotingRoundOn: true}); // pastClaim
            await mongooseVoteRepo.update({_id:vote[1]._id},{voter_id:createdUserId, sc_vote_exists: false, isVotingRoundOn: false}); // pastClaim
            await mongooseVoteRepo.update({_id:vote[2]._id},{voter_id:createdUserId, sc_vote_exists: false, isVotingRoundOn: true}); //pendingClaim

            try{
                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.votes_getPastVotedClaims;
                console.log(JSON.stringify(result))
                result.totalPage.should.equal(1);
                result.votedClaims.length.should.equal(2);
                result.votedClaims[0].claim._id.should.equal(String(claim2[0]._id));
                result.votedClaims[1].claim._id.should.equal(String(claim1[0]._id));
                done();
            }catch(e){
                done(e);
            }

        });

    });
});