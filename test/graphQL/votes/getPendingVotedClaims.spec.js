process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const config = require('config');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const settings = require('../../../models/settings');

chai.use(chaiHttp);
describe('getPendingVotedClaims',async function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    // Test case: Assert the expected behaviour
    it('should successfully return 1 pending vote claim ', async (done) => {

        const gql = `
                query votes_getPendingVotedClaims {
                    votes_getPendingVotedClaims(void: true){
                        claim{
                                ownerName
                                title
                                level
                                _id
                                claimStatus
                            }
                            voteStatus
                            votedAt
                            reward 
                            voteId
                    }
                }
            `;

        try {
            let userCreationRequest = testUtils.generateRandomUser();

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let createClaimRequest2 = testUtils.generateRndomClaimCreationRequest();
            let createClaimRequest3 = testUtils.generateRndomClaimCreationRequest();

            let user = testUtils.generateRandomUser();
            let user2 = testUtils.generateRandomUser();
            let user3 = testUtils.generateRandomUser();

            let tokenObj = {};
            let tokenObj2 = {};
            let tokenObj3 = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
            let secondUser = await claimCreationWrapper.authenticateAndCreateClaim(user2, createClaimRequest2, tokenObj2);
            let claim2 = await mongooseClaimRepo.findByOwnerId(secondUser._id);
            let thirdUser = await claimCreationWrapper.authenticateAndCreateClaim(user3, createClaimRequest3, tokenObj3);
            let claim3 = await mongooseClaimRepo.findByOwnerId(thirdUser._id);
            await mongooseClaimRepo.updateClaimID(String(claim3[0]._id),1);

            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);
            await mongooseVoteRepo.update({_id:vote[0]._id},{voter_id:createdUserId, sc_vote_exists: true, isVotingRoundOn: true}); // pastClaim
            await mongooseVoteRepo.update({_id:vote[1]._id},{voter_id:createdUserId, sc_vote_exists: false, isVotingRoundOn: false}); // pastClaim
            await mongooseVoteRepo.update({_id:vote[2]._id},{voter_id:createdUserId, isVotingRoundOn: true}); //pendingClaim


            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: gql});

            let pendingClaims = res.body.data.votes_getPendingVotedClaims;
            pendingClaims.length.should.equal(1);
            let thePendingClaim = claim3[0];
            pendingClaims[0].claim._id.should.equal(String(thePendingClaim._id));
            pendingClaims[0].claim.title.should.equal(thePendingClaim.title);
            pendingClaims[0].claim.level.should.equal(thePendingClaim.level);
            pendingClaims[0].claim.ownerName.should.equal(user3.name);

            done();


        } catch (error) {
            done(error);
        }
    });

})