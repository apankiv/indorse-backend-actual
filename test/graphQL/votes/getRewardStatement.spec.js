process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');

chai.use(chaiHttp);
describe('getRewardStatement', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should successfully return reward statement given user has 3 votes 1 voted and paid, 1 voted and unpaid, 1 not voted', async (done) => {


        try {
            let rewardPaid = testUtils.generateRandomNumber();
            let rewardUnpaid = testUtils.generateRandomNumber();
            let userCreationRequest = testUtils.generateRandomUser();
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(userCreationRequest, tokenObj);
            const gql = `
                query votes_getRewardStatement {
                    votes_getRewardStatement(void: true){
                        rewardsDistributed
                        rewardsPending
                        totalPayout
                        totalVotes
                    }
                }
            `;
            let votePaid = {
                claim_id: testUtils.generateRandomString(),
                voter_id: createdUser._id,
                voting_round_id: testUtils.generateRandomString(),
                isVotingRoundOn: true,
                reward: rewardPaid,
                sc_vote_exists: true,
                paid: true
            };
            let voteUnpaid = {
                claim_id: testUtils.generateRandomString(),
                voter_id: createdUser._id,
                voting_round_id: testUtils.generateRandomString(),
                isVotingRoundOn: true,
                reward: rewardUnpaid,
                sc_vote_exists: true,
                paid: false
            };
            let voteNotVoted = {
                claim_id: testUtils.generateRandomString(),
                voter_id: createdUser._id,
                voting_round_id: testUtils.generateRandomString(),
                isVotingRoundOn: true,
                sc_vote_exists: false
            };

            await mongooseVoteRepo.insert(votePaid);
            await mongooseVoteRepo.insert(voteUnpaid);
            await mongooseVoteRepo.insert(voteNotVoted);

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({query: gql});


            let rewardStatement = res.body.data.votes_getRewardStatement;
            rewardStatement.rewardsDistributed.should.equal(rewardPaid);
            rewardStatement.rewardsPending.should.equal(rewardUnpaid);
            rewardStatement.totalPayout.should.equal(rewardUnpaid+rewardPaid);
            rewardStatement.totalVotes.should.equal(2);

            done();


        } catch (error) {
            done(error);
        }
    });


})