process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimEvents = require('../../../models/services/mongo/mongoose/mongooseClaimEvents');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');

const should = chai.should();
chai.use(chaiHttp);

describe('voteSubmitted subscription', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /votes/:vote_id', () => {
        it('it should submit a vote for a claim if user is one of voters', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            const testedValidator = validators[0];
            await testUtils.wait(1000);

            const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            const vote = votes[0];

            let res = await chai.request(server)
                .post(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send({
                    endorse: true,
                    feedback: {
                        quality: 1,
                        designPatterns: 1,
                        explanation: 'ble',
                        testCoverage: 1,
                        readability: 1,
                        extensibility: 1,
                    },
                });
            const { votingToken } = res.body;
            const updatedVote = await mongooseVoteRepo.findOneById(vote._id);

            updatedVote.endorsed.should.equal(true);
            should.exist(updatedVote.voted_at);
            updatedVote.voting_token.should.equal(votingToken);

            const claimEvents = await mongooseClaimEvents.findAll({ user_id: testedValidator.user_id });
            claimEvents.length.should.equal(1);

            res = await chai.request(server)
                .get(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send();

            const subs = `
                subscription($claimId: String = "${vote.claim_id}") {
                    voteSubmitted(claimId: $claimId){
                        claimId
                    }
                }
            `;

            res = await chai
                .request(server)
                .post('/graphql')
                .send({
                    query: subs,
                });
            console.log(`res ${res.body.data.voteSubmitted}`);
        });
    });
});
