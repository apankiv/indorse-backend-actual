process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings');
const claimCreationWrapper = require('../../claims/claimCreationWrapper');
const DB = require('../../db');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../../smart-contracts/verifiers/contractSettingInitializer');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const {updateValidatorCommentActionType} = require('../../../graphql/resolvers/votesAdmin/updateValidatorComment');
const voteTestHelper = require('../../votes/voteTesthelper');
const mongooseValidatorRepo =  require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');
const adminVoteOnCommentService = require('../../../models/votes/adminVoteOnCommentService');

chai.use(chaiHttp);

describe.skip('updateValidatorComment resolver', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('updateValidatorComment tests', () => {

        it('it should downvote a validator vote', async (done) => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
            await mongooseUserRepo.update({_id: firstUser._id},{role:'admin'});


            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);

            const gql = `
                mutation updateValidatorComment{
                   updateValidatorComment(voteId: "${vote[0]._id}", actionOnComment: ${updateValidatorCommentActionType.DOWNVOTE}){
                        noOfUpvote
                        noOfDownvote
                        isCommentHidden
                        isAdminVoteOnComment
                        voteId
                   }
                }
            `;
            try{

                await voteTestHelper.updateVoteWithFeedbackAndSC(vote[0]._id.toString());

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidatorComment;
                result.noOfDownvote.should.equal(1);
                result.isAdminVoteOnComment.should.equal(true);
                result.isCommentHidden.should.equal(false);
                result.voteId.should.equal(vote[0]._id.toString());
                result.noOfUpvote.should.equal(0);


                done();
            }catch(e){
                done(e);
            }

        });

        it('it should hide a validator vote', async (done) => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
            await mongooseUserRepo.update({_id: firstUser._id},{role:'admin'});


            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);

            const gql = `
                mutation updateValidatorComment{
                   updateValidatorComment(voteId: "${vote[0]._id}", actionOnComment: ${updateValidatorCommentActionType.HIDE}){
                        noOfUpvote
                        noOfDownvote
                        isCommentHidden
                        isAdminVoteOnComment
                        voteId
                        voteHiddenUsername
                  
                   }
                }
            `;
            try{

                await voteTestHelper.updateVoteWithFeedbackAndSC(vote[0]._id.toString());

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidatorComment;
                result.noOfDownvote.should.equal(0);
                result.isAdminVoteOnComment.should.equal(false);
                result.isCommentHidden.should.equal(true);
                result.voteId.should.equal(vote[0]._id.toString());
                result.noOfUpvote.should.equal(0);
                result.voteHiddenUsername.should.equal(firstUser.username);

                done();
            }catch(e){
                done(e);
            }

        });

        it('it should not allow normal user to hide a comment', async (done) => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);
            await mongooseUserRepo.update({_id: firstUser._id},{role:'admin'});


            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);
            await mongooseUserRepo.update({_id: firstUser._id},{role:'full_access'});

            const gql = `
                mutation updateValidatorComment{
                   updateValidatorComment(voteId: "${vote[0]._id}", actionOnComment: ${updateValidatorCommentActionType.HIDE}){
                        noOfUpvote
                        noOfDownvote
                        isCommentHidden
                        isAdminVoteOnComment
                        voteId
                        voteHiddenUsername
                  
                   }
                }
            `;
            try{

                await voteTestHelper.updateVoteWithFeedbackAndSC(vote[0]._id.toString());

                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidatorComment;
                should.not.exist(result);


                done();
            }catch(e){
                done(e);
            }

        });




        it('it shouldnt allow downvoting twice by sameuser', async (done) => {



            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();
            let tokenObj = {};


            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let firstUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let claim1 = await mongooseClaimRepo.findByOwnerId(firstUser._id);


            let createdUser = await mongooseUserRepo.findOneByEmail(user.email);
            let vote = await mongooseVoteRepo.findByVoterId(validators[0].user_id);
            let createdUserId = String(createdUser._id);
            await voteTestHelper.updateVoteWithFeedbackAndSC(vote[0]._id);

            const gql = `
                mutation updateValidatorComment{
                   updateValidatorComment(voteId: "${vote[0]._id}", actionOnComment: ${updateValidatorCommentActionType.UPVOTE}){
                        noOfUpvote
                        noOfDownvote
                        isCommentHidden
                        isAdminVoteOnComment
                        voteId
                   }
                }
            `;
            try{
                let res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});
                let result = res.body.data.updateValidatorComment;
                result.noOfDownvote.should.equal(0);
                result.noOfUpvote.should.equal(1);
                result.isAdminVoteOnComment.should.equal(true);
                result.isCommentHidden.should.equal(false);
                result.voteId.should.equal(vote[0]._id.toString());
                res = await chai.request(server)
                    .post('/graphql')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({query: gql});

                let updateValidatorCommentResp = res.body.data.updateValidatorComment;
                should.not.exist(updateValidatorCommentResp);

                done();
            }catch(e){
                done(e);
            }

        });


        it('test checkIfVoteShouldBeInvalidated module - being used for payout module', async (done) => {

            let tokenObjClaimUser = {};
            let claimUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(testUtils.generateRandomUser(),tokenObjClaimUser , 'admin');
            let claim = testUtils.generateRndomClaimCreationRequest();
            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.createClaim(claim, tokenObjClaimUser);
            let claimIds = await mongooseClaimRepo.findByOwnerId(claimUser._id);
            let theClaimId = String(claimIds[0]._id);

            for (let i = 0; i < 9; i++) {
                let user = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(),{});
                await mongooseValidatorRepo.insert({user_id : user._id});
            }

            let validatorToTest = await authenticationWrapper.signupVerifyAuthenticate(testUtils.generateRandomUser(),{});
            await mongooseValidatorRepo.insert({user_id : validatorToTest._id});

            for (let j = 0; j < 10; j++) {
                await voteTestHelper.insertVoteWithVoterIdAndClaimId(validatorToTest._id, theClaimId)
            }


            try{
                let votes = await mongooseVoteRepo.findAll({voter_id: validatorToTest._id });
                let hiddenVote = votes[0];
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(hiddenVote).should.equal(false);
                await mongooseVoteRepo.update({_id: hiddenVote._id}, {'hide.isHidden':true});
                let invalidVote1 = await mongooseVoteRepo.findOneById(hiddenVote._id);
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(invalidVote1).should.equal(true);


                let oneDownvote = votes[1];
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(oneDownvote).should.equal(false);
                await mongooseVoteRepo.update({_id: oneDownvote._id},  {'upvote.count':1});
                await mongooseVoteRepo.update({_id: oneDownvote._id}, {'downvote.count':2});
                let invalidVote2 = await mongooseVoteRepo.findOneById(oneDownvote._id);
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(invalidVote2).should.equal(true);


                let secondDownvote = votes[2];
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(secondDownvote).should.equal(false);
                await mongooseVoteRepo.update({_id: secondDownvote._id}, {'downvote.count':1});
                let invalidVote3 = await mongooseVoteRepo.findOneById(secondDownvote._id);
                adminVoteOnCommentService.checkIfVoteShouldBeInvalidated(invalidVote3).should.equal(true);


                done();
            }catch(e){
                done(e);
            }

        });


    });
});