const { expect } = require('chai');
const uuidV1 = require('uuid/v1');
const helperCollection = require('../../helpers/collection');

describe('helpers/collection.js', () => {
    it('convertCollectionToMap() : should return map of objects with default key(id)', () => {
        const collection = [
            { _id: uuidV1(), name: 'avocado' },
            { _id: uuidV1(), name: 'bilberry' },
            { _id: uuidV1(), name: 'cherry' },
            { _id: uuidV1(), name: 'damson' },
        ];
        const map = helperCollection.convertCollectionToMap(collection);
        expect(map).to.have.all.keys(...collection.map(object => object._id));
    });

    it('convertCollectionToMap() ' +
        ': should return map with value as objects without modification', () => {
        const collection = [
            { _id: uuidV1(), name: 'avocado' },
            { _id: uuidV1(), name: 'bilberry' },
            { _id: uuidV1(), name: 'cherry' },
            { _id: uuidV1(), name: 'damson' },
        ];
        const map = helperCollection.convertCollectionToMap(collection);
        collection.forEach((object) => {
            expect(map[object._id]).to.equal(object, 'map should have object without modification');
        });
    });

    it('convertCollectionToMap() : should return map of objects with specified key', () => {
        const collection = [
            { myId: uuidV1(), name: 'avocado' },
            { myId: uuidV1(), name: 'bilberry' },
            { myId: uuidV1(), name: 'cranberry' },
            { myId: uuidV1(), name: 'damson' },
        ];
        const map = helperCollection.convertCollectionToMap(collection, 'myId');
        expect(map).to.have.all.keys(...collection.map(object => object.myId));
    });

    it('convertCollectionToMap() ' +
        ': should not throw if keys are missing (throwIfKeyNotFound = false)', () => {
        const collection = [
            { myId: uuidV1(), name: 'avocado' },
            { myId: uuidV1(), name: 'bilberry' },
            { myId: uuidV1(), name: 'cranberry' },
            { myId: uuidV1(), name: 'damson' },
            { missingKey: uuidV1(), name: 'elderberry' },
        ];
        const map = helperCollection.convertCollectionToMap(collection, 'myId');
        expect(map)
            .to.have.all.keys(...collection
                .map(object => object.myId)
                .filter(el => !!el));
    });

    it('convertCollectionToMap() ' +
        ': should throw if keys are missing (throwIfKeyNotFound = true)', () => {
        const collection = [
            { myId: uuidV1(), name: 'avocado' },
            { myId: uuidV1(), name: 'bilberry' },
            { myId: uuidV1(), name: 'cranberry' },
            { myId: uuidV1(), name: 'damson' },
            { missingKey: uuidV1(), name: 'elderberry' },
        ];
        expect(() => helperCollection.convertCollectionToMap(collection, 'myId', true))
            .to.throw();
    });

    it('convertCollectionToMap() ' +
        ': should always throw if collection param is not an array', () => {
        const collectionNotAnArray = { this_is_not: 'an array' };
        expect(() => helperCollection.convertCollectionToMap(collectionNotAnArray))
            .to.throw();
    });

    it('convertCollectionToMap() ' +
        ': should return missing keys in keyNotFoundObjects param passed by reference', () => {
        const collection = [
            { myId: uuidV1(), name: 'avocado' },
            { myId: uuidV1(), name: 'banana' },
            { myId: uuidV1(), name: 'cranberry' },
            { myId: uuidV1(), name: 'damson' },
        ];
        const missingKeyObjects = [
            { missingKey1: uuidV1(), name: 'elderberry' },
            { missingKey2: uuidV1(), name: 'feijoa' },
            { missingKey3: uuidV1(), name: 'gooseberry' },
        ];
        const objectsWithMissingKeys = [];
        helperCollection.convertCollectionToMap(
            collection.concat(missingKeyObjects),
            'myId',
            false,
            objectsWithMissingKeys,
        );
        expect(objectsWithMissingKeys).to.have.lengthOf(
            missingKeyObjects.length,
            `${missingKeyObjects.length} objects are having missing keys`,
        );
        expect(objectsWithMissingKeys).to.have.members(
            missingKeyObjects,
            `should have missing elements ${JSON.stringify(missingKeyObjects)}`,
        );
    });
});
