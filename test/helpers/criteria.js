const { expect } = require('chai');
const { Criteria, FIELD_TYPE } = require('../../helpers/criteria');

describe('helpers/criteria.js', () => {
    it('addField : should add exact match field', () => {
        const criteria = new Criteria();
        criteria.addField('name', 'henry', FIELD_TYPE.EXACT);
        expect(criteria.get()).to.be.an('object', 'criteria should be object');
        expect(criteria.get()).to.include({ name: 'henry' });
    });

    it('addField : should add regex match field', () => {
        const criteria = new Criteria();
        criteria.addField('name', 'mark', FIELD_TYPE.REGEX);
        expect(criteria.get()).to.have.all.keys('name');
        expect(criteria.get().name).to.have.all.keys('$regex');
        expect(criteria.get().name.$regex instanceof RegExp).to.equal(true);
    });

    it('addField : should add valid mongoId exact match field', () => {
        const validMongoId = '5c415c47233d25bbcb4b2849';
        const criteria = new Criteria();
        criteria.addField('id', validMongoId, FIELD_TYPE.MONGO_ID);
        expect(criteria.get()).to.have.all.keys('id');
        expect(criteria.get().id.toString()).to.equal(validMongoId.toString());
    });

    it('addField : should not add invalid mongoId exact match field', () => {
        const invalidMongoId = 'invalidMongoId';
        const criteria = new Criteria();
        criteria.addField('id', invalidMongoId, FIELD_TYPE.MONGO_ID);
        expect(criteria.get()).to.not.have.keys('id');
        expect(Object.keys(criteria.get()).length).to.equal(0);
    });

    it('addField : should add list match field', () => {
        const criteria = new Criteria();
        const names = ['air', 'ball', 'cup'];
        criteria.addField('names', names, FIELD_TYPE.LIST);
        expect(criteria.get()).to.have.all.keys('names');
        expect(criteria.get().names).to.have.all.keys('$in');
        expect(criteria.get().names.$in).to.be.an('array');
        expect(criteria.get().names.$in).to.have.lengthOf(names.length);
        expect(criteria.get().names.$in).to.include.members(names);
    });

    it('addField : should throw when unknown field type is given', () => {
        const criteria = new Criteria();
        expect(() => {
            criteria.addField('name', 'henry', 'UNKNOWN_FIELD_TYPE');
        }).to.throw();
    });

    it('should return the correct criteria', () => {
        const validMongoId = '5c415c47233d25bbcb4b2849';
        const criteria = new Criteria();
        criteria.addField('id', validMongoId, FIELD_TYPE.MONGO_ID);
        criteria.addField('email', 'henry@ford.com', FIELD_TYPE.EXACT);
        criteria.addField('city', 'henry', FIELD_TYPE.REGEX);
        criteria.addField('orderIds', [1, 2, 3], FIELD_TYPE.LIST);
        expect(criteria.get()).to.have.all.keys(['id', 'email', 'city', 'orderIds']);
    });

    it('isHavingAnyField : should return the true if criteria has fields', () => {
        const criteria = new Criteria();
        criteria.addField('email', 'henry@ford.com', FIELD_TYPE.EXACT);
        expect(criteria.isHavingAnyField()).to.equal(true);
    });

    it('isHavingAnyField : should return the false if criteria does not have fields', () => {
        const criteria = new Criteria();
        criteria.addField('id', 'invalidMongoId', FIELD_TYPE.MONGO_ID);
        expect(criteria.isHavingAnyField()).to.equal(false);
    });

    it('isObjectIdValid : should return true if id is mongo id', () => {
        const validMongoId = '5c415c47233d25bbcb4b2849';
        expect(Criteria.isObjectIdValid(validMongoId)).to.equal(true);
    });

    it('isObjectIdValid : should return false if id is not a valid mongo id', () => {
        expect(Criteria.isObjectIdValid('invalidMongoId')).to.equal(false);
    });

    it('getList : should return list if list is passed', () => {
        expect(Criteria.getList(['a', 'b', 'c'])).to.be.an('array');
        expect(Criteria.getList(['a', 'b', 'c'])).to.have.lengthOf(3);
    });

    it('getList : should return list if string is passed', () => {
        expect(Criteria.getList('a')).to.be.an('array');
        expect(Criteria.getList(['a'])).to.have.lengthOf(1);
    });
});
