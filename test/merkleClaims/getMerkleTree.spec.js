process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const merkleContractSettingInitializer = require('../smart-contracts/merkleClaims/contractSettingInitializer');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteSignatureRepo = require('../../models/services/mongo/mongoose/mongooseVoteSignaturesRepo');
const mongooseClaimSignatureRepo = require('../../models/services/mongo/mongoose/mongooseClaimSignaturesRepo');
const resolverLoop = require('../../smart-contracts/services/cron/resolverLoop');
const timestampService = require('../../models/services/common/timestampService')
const merkleUtils = require('../../models/services/common/merkleUtils')
const mongooseMerkleTreeRepo = require('../../models/services/mongo/mongoose/mongooseMerkleTreeRepo')    
const duplicationGuardService = require('../../models/services/ethereum/duplicationGuardService') 
const mongooseTransactionRepo = require('../../models/services/mongo/mongoose/mongooseTransactionRepo')       
const mongooseTransactionQueueRepo = require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo') 
const merkleClaimContract = require('../../models/services/ethereum/merkleClaims/proxy/merkleClaimsProxy')
const mongooseJobRoleRepo = require('../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseAssignmentRepo = require('../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const checkClaimsJob = require('../../smart-contracts/services/cron/checkClaimsJob');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const assignmentResultService = require('../../models/services/claims/assignmentResultService');
const authenticationWrapper = require('../authenticationWrapper');
const claimReleaserJob = require('../../models/services/crons/claimReleaserJob')



chai.use(chaiHttp);

describe('getVotePayload', function () {
    this.timeout(config.get('test.timeout'));
    
    var voteVerifierSC;
    var claimVerifierSC;

    beforeEach(async () => {
        console.log('connecting to database');
        merkleSC = await merkleContractSettingInitializer.initialize();
        [voteVerifierSC, claimVerifierSC] = await contractSettingInitializer.initialize();
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });


    it('should return merkle tree data to interface', async()=> {
        //Push data into claims collection
        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
        let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        createdClaim = createdClaim[0];

        //Push data into claim sig collection
        for (i = 0; i < 3; i++) {
            let votePayload = {
                payload: {
                    claim_id: createdClaim._id.toString(),
                    decision: "Indorsed",
                    timestamp : 100,                
                    skill: "Java,Python",
                    version: "1.0"
                },
                signature: {
                    r: "0xb15249d80b5d56dafaf1f37a855effd8cc67c75a4dbcfbbcae1bd22f3d9012b3",
                    s: "0x7d176032535f18e1f94e7ca0242d67e3809612b6c7b6d19f37a9f68e305bcc3c",
                    v: 28
                },
                user_id: "100",
                ethaddress: "0xC6dE3f28118C6215A1Dc52f8Ae577f1337680E33",
                merkelized : false
            }
            await mongooseVoteSignatureRepo.insert(votePayload);   
        }    


        //Push Root
        let date = new Date();
        await merkleUtils.pushMerkleRoot(date);

        var toDate = Math.floor(date/1000);     
        
        let txQueue = await mongooseTransactionQueueRepo.findOne({ "tx_metadata.contract": "MerkleClaims" });    
        let duplicateQueue = await duplicationGuardService.checkForDuplicates(txQueue.tx_metadata, "cron");    
        duplicateQueue.should.equal(true)             

        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();        
        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();          

       

        let rootResp = await merkleClaimContract.getRootByTimeKey(toDate)        
        should.exist(rootResp);

        let merkleTree = await mongooseMerkleTreeRepo.findOne({ rootHash: rootResp.toString()})
        should.exist(merkleTree);    
    
        let request = `					 
            query ($timestamp: Int, $claim_id: String) {
                    getMerkleTree(timestamp: $timestamp, claim_id: $claim_id) {
                            claim_ids
                            roothash
                            timestamp_to
                            timestamp_from
                            leaves{
                                data
                                type
                                claim {
                                    id
                                    type
                                    title
                                    level
                                    owner
                                    username
                                    endorse_count
                                    flag_count
                                    claim_status
                                    created_at
                                    tags
                                    proof
                                }
                                vote{
                                    id
                                    claim_id
                                    endorsed
                                    timestamp
                                    skill
                                }
                            }
					} 
				}
            `;

            let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: request, variables: {
                    timestamp: merkleTree.timestamp
                }
            });
            
            should.not.exist(res.body.errors)
   
        //Push data into vote sig collection
        for (i = 0; i < 3; i++) {
            let votePayload = {
                payload: {
                    claim_id: createdClaim._id.toString(),
                    decision: "Indorsed",
                    timestamp : 100,                
                    skill: "Java,Python",
                    version: "1.0"
                },
                signature: {
                    r: "0xb15249d80b5d56dafaf1f37a855effd8cc67c75a4dbcfbbcae1bd22f3d9012b3",
                    s: "0x7d176032535f18e1f94e7ca0242d67e3809612b6c7b6d19f37a9f68e305bcc3c",
                    v: 28
                },
                user_id: "100",
                ethaddress: "0xC6dE3f28118C6215A1Dc52f8Ae577f1337680E33",
                merkelized : false
            }
            await mongooseVoteSignatureRepo.insert(votePayload);   
        }   

        console.log("\n\n\n Date to change : " + date);
        let newDate  = new Date();
        newDate.setDate(newDate.getDate()-20);
      
        
        await merkleUtils.pushMerkleRoot(newDate);


        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();        
        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions(); 


        let res2 = await chai.request(server)
        .post('/graphql')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({
            query: request, variables: {
                claim_id: createdClaim._id.toString()
            }
        });            

        res2.body.data.getMerkleTree.length.should.equal(2);      
          
    });   
    
    
    it('should be able to return assignment in merkle tree', async () => {
        try {
            let adminUser = testUtils.generateRandomUser();
            let randomAss = testUtils.generateRandomAssignment();
            let randomRole = testUtils.generateRandomJobRole();
            let roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.job_role = roleid;

            randomAss.duration = -1;
            let id = await mongooseAssignmentRepo.insert(randomAss);
            let tokenObj = {};

            adminUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(
                adminUser,
                tokenObj,
                'admin'
            );

            let mutationRequest = `					 
            mutation startAssignment($assignmentID: String!) {
                startAssignment(assignmentID: $assignmentID) {
                    started_on
                }
             }
            `;

            res = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({
                    query: mutationRequest,
                    variables: { assignmentID: id, proof: 'https://github.com/ensdomains/ens' },
                });

            res.should.have.status(200);

            //add 2 skills and 2 parent skills
            const skillTag1 = 'tag1';
            const skill = {
                name: 'javascript',
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag1];
            skill.validation = {
                aip: true,
            };
            let skillid1 = await mongooseSkillRepo.insert(skill);

            const skillTag2 = 'tag2';
            const skill2 = {
                name: 'python',
                category: testUtils.generateRandomSkill(),
            };
            skill2.tags = [skillTag2];
            skill2.validation = {
                aip: true,
            };
            let skillid2 = await mongooseSkillRepo.insert(skill2);

            const skillTag3 = 'tag3';
            const skill3 = {
                name: 'jschild',
                category: testUtils.generateRandomSkill(),
            };
            skill3.tags = [skillTag3];
            skill3.parents = [skillid1];
            let skillid3 = await mongooseSkillRepo.insert(skill3);

            const skillTag4 = 'tag4';
            const skill4 = {
                name: 'pychild',
                category: testUtils.generateRandomSkill(),
            };
            skill4.tags = [skillTag4];
            skill4.parents = [skillid2];
            let skillid4 = await mongooseSkillRepo.insert(skill4);

            const variables = {
                assignmentID: id,
                form: {
                    proof: 'https://github.com/ensdomains/ens',
                    skills: [
                        {
                            skillTag: 'tag3',
                            skillId: skillid3,
                        },
                        {
                            skillTag: 'tag4',
                            skillId: skillid4,
                        },
                    ],
                },
            };

            //Add skill on user
            let updateObj = [];

            let obj = {
                skill: {
                    name: 'pychild',
                    category: 'tmp',
                    level: 'beginner',
                    _id: skillid4,
                },
                level: 'beginner',
                validations: [
                    {
                        type: 'claim',
                        level: 'beginner',
                        id: 'fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73',
                        validated: true,
                    },
                ],
            };
            updateObj.push(obj);

            await mongooseUserRepo.update(
                { email: adminUser.email },
                { $set: { skills: updateObj } }
            );

            //create 4 users/validators
            let validatorsJs = await claimCreationWrapper.createValidatorSet(
                settings.CLAIM_VALIDATOR_COUNT / 2,
                ['Javascript']
            );
            let validatorsPython = await claimCreationWrapper.createValidatorSet(
                settings.CLAIM_VALIDATOR_COUNT / 2,
                ['Python']
            );

            let mutationFinishAssignment = `					 
            mutation finishAssignment($assignmentID: String!, $form :FinishAssignmentForm! ) {
                finishAssignment(assignmentID: $assignmentID, form: $form){
                    _id
                    assignment_id
                }              
             }
            `;

            res2 = await chai
                .request(server)
                .post('/graphql')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ query: mutationFinishAssignment, variables: variables });

            //Approve assignment
            let claim = await mongooseClaimsRepo.findOne({ type: 'ASSIGNMENT', user_assignment_id: res2.body.data.finishAssignment._id });


            await chai
                .request(server)
                .post('/claims/' + claim._id + '/approve')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(claim);

            await claimReleaserJob.releaseClaims()

            const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id);

            console.log("Voting round is : " + JSON.stringify(votingRound));

            for (testedValidator of validatorsJs) {
                let votes = await mongooseVoteRepo.findByVotingRoundIdAndVoter(votingRound._id.toString(), testedValidator.user_id);
                let vote = votes[0];
                res = await chai
                    .request(server)
                    .post('/votes/' + vote._id.toString())
                    .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                    .send({
                        endorse: true,
                        feedback: {
                            quality: 2,
                            designPatterns: 2,
                            gitFlow: 2,
                            completion: 2,
                            testCoverage: 2,
                            explanation: 'ble',
                            readability : 2,
                            extensibility : 2
                        },
                        skillid: skillid3,
                    });

                let votingToken = res.body.votingToken;

                let updatedVote = await mongooseVoteRepo.findOneById(vote._id);

                updatedVote.endorsed.should.equal(true);
                should.exist(updatedVote.voted_at);
                updatedVote.voting_token.should.equal(votingToken);

                // Simulate vote in blockchain
                await mongooseVoteRepo.update({ _id: vote._id }, { txStatus: 'SUCCESS', sc_vote_exists: true });
                updatedVote = await mongooseVoteRepo.findOneById(vote._id);
            }

            for (testedValidator of validatorsPython) {
                let votes = await mongooseVoteRepo.findByVotingRoundIdAndVoter(votingRound._id.toString(), testedValidator.user_id);
                let vote = votes[0];
                res = await chai
                    .request(server)
                    .post('/votes/' + vote._id.toString())
                    .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                    .send({
                        endorse: true,
                        feedback: {
                            quality: 3,
                            designPatterns: 3,
                            gitFlow: 3,
                            completion: 3,
                            testCoverage: 3,
                            explanation: 'ble',
                            readability : 3,
                            extensibility : 3
                        },
                        skillid: skillid4,
                    });

                // Simulate vote in blockchain
                await mongooseVoteRepo.update({ _id: vote._id }, { txStatus: 'SUCCESS', sc_vote_exists: true });
            }

            let assignmentResult = await assignmentResultService.calculateAssignmentResult(claim);
            await mongooseUserRepo.setAssignmentValidationResult(
                adminUser._id,
                claim._id,
                assignmentResult
            );
            await mongooseUserAssignmentRepo.updateAssignmentResult(
                claim.user_assignment_id,
                'evaluated',
                assignmentResult
            );

            //Check case
            //User should have 2 skills
            const resultUser = await mongooseUserRepo.findOneByEmail(adminUser.email);
            for (i = 0; i < resultUser.skills.length; i++) {
                const skill = resultUser.skills[i];
                if (skill.skill._id.toString() === skillid4.toString()) {
                    skill.level.should.equal('intermediate');
                } else if (skill.skill._id.toString() === skillid3.toString()) {
                    skill.level.should.equal('beginner');
                }
            }

        //Above test case is same as finishAssignment.spec.js
        //Set claim to exprire in the past
        //and post a root to chain for this claim


        let date = new Date();        

        //Push some votes
        for (i = 0; i < 3; i++) {
            let votePayload = {
                payload: {
                    claim_id: claim._id,
                    decision: "Indorsed",
                    timestamp : 100,                
                    skill: "Java,Python",
                    version: "1.0"
                },
                signature: {
                    r: "0xb15249d80b5d56dafaf1f37a855effd8cc67c75a4dbcfbbcae1bd22f3d9012b3",
                    s: "0x7d176032535f18e1f94e7ca0242d67e3809612b6c7b6d19f37a9f68e305bcc3c",
                    v: 28
                },
                user_id: "100",
                ethaddress: "0xC6dE3f28118C6215A1Dc52f8Ae577f1337680E33"
            }
            await mongooseVoteSignatureRepo.insert(votePayload);   
        }    

        //Push data into vote sig collection

        let claimPayload = {
            payload : {
                claim_id: claim._id,
                claimant_id : "U100",
                proof : "test.com",
                timestamp : 100,
                deadline : 200,
                skills : "Java,Python",
                kind: "ASSIGNMENT",
                voters : "0,1",
                version : "1.0"
            },
            signature: {
                r: "0xb15249d80b5d56dafaf1f37a855effd8cc67c75a4dbcfbbcae1bd22f3d9012b3",
                s: "0x7d176032535f18e1f94e7ca0242d67e3809612b6c7b6d19f37a9f68e305bcc3c",
                v: 28
            },
        }
        await mongooseClaimSignatureRepo.insert(claimPayload);   


        //Push Root
        let newDate  = new Date();
        newDate.setDate(newDate.getDate()-10);
      
        await merkleUtils.pushMerkleRoot(newDate);



        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();        
        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions(); 



        //Request for claim in query
        let request = `					 
            query ($timestamp: Int, $claim_id: String) {
                    getMerkleTree(timestamp: $timestamp, claim_id: $claim_id) {
                            claim_ids
                            roothash
                            timestamp_to
                            timestamp_from
                            leaves{
                                data
                                type
                                claim {
                                    id
                                    type
                                    title
                                    level
                                    owner
                                    username
                                    endorse_count
                                    flag_count
                                    claim_status
                                    created_at
                                    tags
                                    proof
                                }
                                vote{
                                    id
                                    claim_id
                                    endorsed
                                    timestamp
                                    skill
                                }
                            }
					} 
				}
            `;

            let resTree = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({
                query: request, variables: {
                    claim_id: claim._id                    
                }
            });
            

            should.not.exist(resTree.body.errors)
            
            let found  = false
            for(tree of resTree.body.data.getMerkleTree ){
                for(leaf of tree.leaves ){
                    if(leaf.type ==='claim'){
                        found = true;
                         leaf.claim.title.should.equal("jschild,pychild")
                    }
                }  
            }
            found.should.equal(true);

        } catch (err) {
            console.log(err);
            throw err;
        }
    });
});