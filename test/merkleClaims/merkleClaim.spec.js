process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/merkleClaims/contractSettingInitializer');
const naiveContractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const verifierContractsInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const settings = require('../../models/settings');
const merkleClaimContract = require('../../models/services/ethereum/merkleClaims/proxy/merkleClaimsProxy')
const resolverLoop = require('../../smart-contracts/services/cron/resolverLoop');
const mongooseClaimSignatureRepo = require('../../models/services/mongo/mongoose/mongooseClaimSignaturesRepo')
const mongooseVoteSignatureRepo = require('../../models/services/mongo/mongoose/mongooseVoteSignaturesRepo')
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo')
const claimCreationWrapper = require('../claims/claimCreationWrapper')
const merkleUtils = require('../../models/services/common/merkleUtils')   
const timestampService = require('../../models/services/common/timestampService')    
const ethereumjsUtil = require('ethereumjs-util')
const mongooseMerkleTreeRepo = require('../../models/services/mongo/mongoose/mongooseMerkleTreeRepo')    
const duplicationGuardService = require('../../models/services/ethereum/duplicationGuardService') 
const mongooseTransactionRepo = require('../../models/services/mongo/mongoose/mongooseTransactionRepo')       
const mongooseTransactionQueueRepo = require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo') 
const crypto = require('crypto') 
var fastRoot = require('merkle-lib/fastRoot')


chai.use(chaiHttp);

describe('Merkle claims ', function () {
    this.timeout(config.get('test.timeout'));
    var merkleSC;
    beforeEach(async () => {
        console.log('connecting to database');
        merkleSC = await contractSettingInitializer.initialize();
        //TODO remove after merge
        [naiveSC, tallySC] = await naiveContractSettingInitializer.initialize();
        [voteVerifierSC, claimVerifierSC] = await verifierContractsInitializer.initialize();
        await DB.connectAsync();
    });

    afterEach(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });


    it('it should post a merkle root', async () => {
        let root = "0x1111111111111111111111111111111111111111111111111111111111111111"  
        let date = new Date();         
        let ts = timestampService.createPastTimestamp(date,30);        
        await merkleClaimContract.getOwner();
        let metadata = {
            claim_ids_metadata : {
                claim_ids :[],
                merk_claim_sig_ids : [],
                merk_vote_sig_ids :[]
            }
        }

        await merkleClaimContract.addMerkleRoot(ts, root, metadata);        
        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();            
        let rootResp = await merkleClaimContract.getRootByTimeKey(ts)
        rootResp.should.equal(root);
    })

    it('should create and post root based on claims setup', async()=> {
        //Push data into claims collection
        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
        let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        createdClaim = createdClaim[0];

        //Push data into claim sig collection
        for (i = 0; i < 3; i++) {
            let votePayload = {
                payload: {
                    claim_id: createdClaim._id.toString(),
                    decision: "Indorsed",
                    timestamp : 100,                
                    skill: "Java,Python",
                    version: "1.0"
                },
                signature: {
                    r: "0xb15249d80b5d56dafaf1f37a855effd8cc67c75a4dbcfbbcae1bd22f3d9012b3",
                    s: "0x7d176032535f18e1f94e7ca0242d67e3809612b6c7b6d19f37a9f68e305bcc3c",
                    v: 28
                },
                user_id: "100",
                ethaddress: "0xC6dE3f28118C6215A1Dc52f8Ae577f1337680E33",
                merkelized : false
            }
            await mongooseVoteSignatureRepo.insert(votePayload);   
        }    


        //Expire claim
        let date = new Date();        

        //Push Root
        await merkleUtils.pushMerkleRoot(date);

        var toDate =  Math.floor(date/1000);  
        
        let txQueue = await mongooseTransactionQueueRepo.findOne({ "tx_metadata.contract": "MerkleClaims" });    
        let duplicateQueue = await duplicationGuardService.checkForDuplicates(txQueue.tx_metadata, "cron");    
        duplicateQueue.should.equal(true)             

        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();        
        await resolverLoop.sendTransactions();
        await resolverLoop.processTransactions();          

      

        let rootResp = await merkleClaimContract.getRootByTimeKey(toDate)        
        should.exist(rootResp);

        let merkleTree = await mongooseMerkleTreeRepo.findOne({ rootHash: rootResp.toString()})
        should.exist(merkleTree);        

        //Check a duplicate transaction
        let tx = await mongooseTransactionRepo.findOne({ "tx_metadata.contract" : "MerkleClaims"});        
        console.log(JSON.stringify(tx));
        should.exist(tx);
        let duplicate = await duplicationGuardService.checkForDuplicates(tx.tx_metadata,"cron");        
        duplicate.should.equal(true);       
    });

    it('comparing different libraries for merkle implentation' , async()=>{
        function sha256(data) {
            return crypto.createHash('sha256').update(data).digest()
        }        
        var abcde = ['a', 'a','a','a'];     
        var data = abcde.map(x => new Buffer(x, 'hex'))
        let root = await merkleUtils.getMerkleRoot(abcde);
        let hashedLeaves = abcde.map(leaf => sha256(leaf))
        var fRoot = fastRoot(hashedLeaves, sha256)        
        root.should.equal(fRoot.toString('hex'));       
    })


});
