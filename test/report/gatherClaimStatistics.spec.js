process.env.NODE_ENV = 'test';
const chai = require('chai');
const DB = require('../db');
const server = require('../../server');

const testUtils = require('../testUtils');
const config = require('config');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const settings = require('../../models/settings');
const gatherClaimStatisticsService = require('../../smart-contracts/services/cron/gatherClaimStatistics');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

describe('gatherClaimStatistics.spec.js', function test() {
    this.timeout(config.get('test.timeout'));
    before(async () => {
        await contractSettingInitializer.initialize();
        console.log('connecting to database');
        await DB.connectAsync();
    });

    after(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });


    it('should return 2 submitted claims', async () => {
        const user = testUtils.generateRandomUser();
        const user2 = testUtils.generateRandomUser();

        const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        const createClaimRequest2 = testUtils.generateRndomClaimCreationRequest();

        const tokenObj = {};
        const tokenObj2 = {};

        const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
        console.log('expected');
        await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
        console.log('unexpected');

        await claimCreationWrapper.authenticateAndCreateClaim(user2, createClaimRequest2, tokenObj2);
        const testedValidator = validators[0];
        await testUtils.wait(1000);

        const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
        const vote = votes[0];

        await chai.request(server)
            .post(`/votes/${vote._id.toString()}`)
            .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
            .send({
                endorse: true,
                feedback: {
                    quality: 1,
                    designPatterns: 1,
                    explanation: 'ble',
                    testCoverage: 3,
                    readability: 2,
                    extensibility: 2,
                },
            });
        await testUtils.wait(1000);

        const report = await gatherClaimStatisticsService.reportStats('D');

        report.pending.should.equal(0);
        report.votesSent.should.equal(settings.MAX_CLAIM_VALIDATOR_COUNT * 2);
        report.votesDatabase.should.equal(1);
        report.submitted.should.equal(2);
        report.approved.should.equal(2);
    });
});
