process.env.NODE_ENV = 'test';
const chai = require('chai');
const DB = require('../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../testUtils');
const config = require('config');
const resolver = require('../../smart-contracts/services/cron/resolverLoop');
const mongooseTransactionRepo = require('../../models/services/mongo/mongoose/mongooseTransactionRepo');
const gatherClaimStatisticsService = require('../../smart-contracts/services/cron/gatherGenerlStatistics');
const authenticateSignupWrapper = require('../authenticationWrapper')

describe('gatherGeneral.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    before(async () => {
        console.log('connecting to database');
        await DB.connectAsync();
    });

    after(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should return sign_up user as 1', async () => {
        let user = testUtils.generateRandomUser();
        await authenticateSignupWrapper.justSignUpNoVerify(user);

        let report = await gatherClaimStatisticsService.reportStats();
        report.signups.should.equal(1);
    });

});