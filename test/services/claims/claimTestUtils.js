const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const settings = require('../../../models/settings');
const {approveClaim} = require('../../../models/claims/approveClaimService');

const _test = {};

_test.approveAndRelease = async (claimId) => {
    let claim = await mongooseClaimsRepo.findOneById(claimId);
    await mongooseClaimsRepo.approveClaim(claimId, 'test', [{
        tier: '_2', noOfValidator: settings.CLAIM_VALIDATOR_COUNT
    }]);
    claim = await mongooseClaimsRepo.findOneById(claimId);
    await approveClaim(claim);
};

module.exports = _test;
