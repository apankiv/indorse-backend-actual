process.env.NODE_ENV = 'test';

const { getAccessPermissions } = require('../../../models/services/companies/getAccessPermissions');
const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');

const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const companyACLWrapper = require('../../graphQL/companies/companyACLWrapper');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const _ = require('lodash');

chai.use(chaiHttp);

describe('Company.getAccessPermissions', () => {
    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(async () => {
            done();
        });
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });
    it('admin user should have admin permissions for company', async () => {
        const adminPermissions = {
            'features.write': true,
            'features.read': true,
            'basic.write': true,
            'basic.read': true,
            'magicLink.write': true,
            'magicLink.read': true,
            'candidates.write': false,
            'candidates.read': true,
            'acl.write': true,
            'acl.read': true,
            'clientApps.write': true,
            'clientApps.read': true,
            'partnerClaims.write': false,
            'partnerClaims.read': true,
            'credits.write': true,
            'credits.read': true,
        };
        const createCompanyRequest = testUtils.generateRandomCompany();
        // add feature to company
        const features = {
            magicLink: true,
            partnerClaims: true,
        };
        createCompanyRequest.features = features;
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const adminUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, false);
        const admin = tokenObj.user;
        const permission = await getAccessPermissions({ userId: admin._id, companyId });
        const permissionPaths = Object.keys(adminPermissions);
        permissionPaths.forEach((permissionPath) => {
            const value = _.get(permission, permissionPath, false);
            value.should.equal(adminPermissions[permissionPath]);
        });
    });
    it('non admin user should have no permissions for company', async () => {
        const normalPermissions = {
            'features.write': false,
            'features.read': false,
            'basic.write': false,
            'basic.read': true,
            'magicLink.write': false,
            'magicLink.read': false,
            'candidates.write': false,
            'candidates.read': false,
            'acl.write': false,
            'acl.read': false,
            'clientApps.write': false,
            'clientApps.read': false,
            'partnerClaims.write': false,
            'partnerClaims.read': false,
            'credits.write': false,
            'credits.read': false,
        };
        const createCompanyRequest = testUtils.generateRandomCompany();
        // add feature to company
        const features = {
            magicLink: true,
            partnerClaims: true,
        };
        createCompanyRequest.features = features;
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const normalUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full-access');
        const normalReturnedUser = tokenObj.user;
        const permission = await getAccessPermissions({ userId: normalReturnedUser._id, companyId });
        const permissionPaths = Object.keys(normalPermissions);
        permissionPaths.forEach((permissionPath) => {
            const value = _.get(permission, permissionPath, false);
            value.should.equal(normalPermissions[permissionPath]);
        });
    });
    it('company admin user should have company admin permissions for company', async () => {
        const companyAdminPermissions = {
            'features.write': false,
            'features.read': true,
            'basic.write': true,
            'basic.read': true,
            'magicLink.write': true,
            'magicLink.read': true,
            'candidates.write': false,
            'candidates.read': true,
            'acl.write': false,
            'acl.read': true,
            'clientApps.write': true,
            'clientApps.read': true,
            'partnerClaims.write': false,
            'partnerClaims.read': true,
            'credits.write': false,
            'credits.read': true,
        };
        const createCompanyRequest = testUtils.generateRandomCompany();
        // add feature to company
        const features = {
            magicLink: true,
            partnerClaims: true,
        };
        createCompanyRequest.features = features;
        const companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        const companyAdminUser = testUtils.generateRandomUser();

        const tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(companyAdminUser, tokenObj, 'full-access');
        await companyACLWrapper.addUserToCompanyACL(companyId, tokenObj.user, false);
        const returnedCompanyAdminUser = tokenObj.user;
        const permission = await getAccessPermissions({ userId: returnedCompanyAdminUser._id, companyId });
        const permissionPaths = Object.keys(companyAdminPermissions);
        permissionPaths.forEach((permissionPath) => {
            const value = _.get(permission, permissionPath, false);
            value.should.equal(companyAdminPermissions[permissionPath]);
        });
    });
});
