process.env.NODE_ENV = 'test';
const chai = require('chai');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const assert = chai.assert;
const testUtils = require('../../testUtils');
const config = require('config');

const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseUserAssignmentRepo = require('../../../models/services/mongo/mongoose/mongooseUserAssignmentRepo');
const timestampService = require('../../../models/services/common/timestampService');
const { states: userAssignmentStates } = require('../../../models/services/mongo/mongoose/schemas/userAssignment');

const assignmentReminderJob = require('../../../models/services/crons/assignmentReminderJob');
let validAssignmentReminder = null;

async function initialize() {
  let randomAssignment = testUtils.generateRandomAssignment();
  let randomUser = testUtils.generateRandomUsers()
  
  const user_id = await mongooseUserRepo.insert(randomUser);
  const assignment_id = await mongooseAssignmentRepo.insert(randomAssignment);
  
  // valid dataset 
  const userAssignment7DaysBack =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.STARTED,
    started_on: timestampService.timestampDaysBack(7),
    owner_id: user_id
  }

  // invalid dateset 
  const userAssignment2DaysBack =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.STARTED,
    started_on: timestampService.timestampDaysBack(2),
    owner_id: user_id
  }
  const userAssignment7DaysBackNotStarted =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.SUBMITTED,
    started_on: timestampService.timestampDaysBack(7),
    owner_id: user_id
  }
  const userAssignment14DaysBack =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.STARTED,
    started_on: timestampService.timestampDaysBack(14),
    owner_id: user_id
  }
  const userAssignment14DaysBackNotStarted =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.EVALUATED,
    started_on: timestampService.timestampDaysBack(14),
    owner_id: user_id
  }
  const userAssignment30DaysBack =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.STARTED,
    started_on: timestampService.timestampDaysBack(30),
    owner_id: user_id
  }
  const userAssignment31DaysBack =  {
    assignment_id: assignment_id,
    status: userAssignmentStates.STARTED,
    started_on: timestampService.timestampDaysBack(31),
    owner_id: user_id
  }

  // Valid dataset 
  validAssignmentReminder = await mongooseUserAssignmentRepo.insert(userAssignment7DaysBack);

  // Invalid dataset
  await mongooseUserAssignmentRepo.insert(userAssignment2DaysBack);
  await mongooseUserAssignmentRepo.insert(userAssignment7DaysBackNotStarted);
  await mongooseUserAssignmentRepo.insert(userAssignment14DaysBack);
  await mongooseUserAssignmentRepo.insert(userAssignment14DaysBackNotStarted); 
  await mongooseUserAssignmentRepo.insert(userAssignment30DaysBack);
  await mongooseUserAssignmentRepo.insert(userAssignment31DaysBack);

  const all = await mongooseUserAssignmentRepo.findAll()
  console.log( 'Total user assignments [Before]: ', all.length);
}

describe('assignmentReminderJob.spec.js', function () {
    this.timeout(config.get('test.timeout'));
    before(async () => {
      console.log('connecting to database');
      await DB.connectAsync();
      await initialize();
    });

    after(async () => {
        console.log('dropping database');
        await DB.dropAsync();
    });

    it('should list user assignments which are started 7', async () => {
      // const all = await mongooseUserAssignmentRepo.findAll()
      // expect(all.length).to.equal(7)  
      await assignmentReminderJob.sendAssignmentReminders();

      const assignmentsForReminder = await mongooseUserAssignmentRepo.getAssignmentsForReminder();
      expect(assignmentsForReminder.length).to.equal(1)
      
    });

    it('should update the reminderSentAt timestamp', async () => {
      const currentTimestamp = timestampService.createTimestamp()
      await assignmentReminderJob.sendAssignmentReminders();
      const assignmentsForReminder = await mongooseUserAssignmentRepo.getAssignmentsForReminder();
      const assignment = await mongooseUserAssignmentRepo.findOneById(validAssignmentReminder);
      assert.isNotNull(assignment.reminderSentAt);
      expect(assignment.reminderSentAt).to.be.gte(currentTimestamp);
    });
    
    it('should Not update the reminderSentAt if already sent', async () => {
      const firstTimestamp = timestampService.createTimestamp()
      // console.log('[first run at ]', firstTimestamp);
      await testUtils.delay(1000);
      await assignmentReminderJob.sendAssignmentReminders();
     
      const assignmentsForReminder = await mongooseUserAssignmentRepo.getAssignmentsForReminder();
      let assignment = await mongooseUserAssignmentRepo.findOneById(validAssignmentReminder);      
      assert.isNotNull(assignment.reminderSentAt);
      // console.log('[assignment reminderSentAt ]', assignment.reminderSentAt);
      expect(assignment.reminderSentAt).to.be.gte(firstTimestamp);
      
      await testUtils.delay(1000);
      const secondTimestamp = timestampService.createTimestamp()
      // console.log('[second run at ]', secondTimestamp);
      await assignmentReminderJob.sendAssignmentReminders();
      
      assignment = await mongooseUserAssignmentRepo.findOneById(validAssignmentReminder);      
      assert.isNotNull(assignment.reminderSentAt);
      // console.log('[assignment reminderSentAt ]', assignment.reminderSentAt);
      expect(assignment.reminderSentAt).to.be.lte(secondTimestamp);
    });
});
