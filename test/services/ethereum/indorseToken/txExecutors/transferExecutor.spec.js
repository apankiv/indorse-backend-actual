process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
const mongooseTransactionQueueRepo = require('../../../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const indorseTokenTransferExecutor = require('../../../../../models/services/ethereum/indorseToken/txExecutors/transferExecutor');
const indorseTokenFacade = require('../../../../../smart-contracts/services/indorseToken/indorseTokenContract');
const indorseTokenDeployer = require('../../../../smart-contracts/indorseToken/indorseTokenDeployer');
const resolverLoopWrapper = require('../../../../smart-contracts/cron/resolverLoopWrapper');

chai.use(chaiHttp);

describe('indorseToken.transferExecutor.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
        await indorseTokenDeployer.deploy();
        console.log('connecting to database');
        await DB.connectAsync();

    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('execute', () => {
        it('should execute a transaction', async () => {
            let userId = "userID123"
            let userAddress = "0xdc3f961b733EEE8a370977d9854F64D86121AC31"
            let initiatingUserId = "initiatingUserID123"
            let valueToSend = 1000
            let metadata = {
                claim_id: "claimID123"
            }
            let userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(0, "should start with 0 IND tokens")

            await indorseTokenTransferExecutor.execute(userId, userAddress, valueToSend, initiatingUserId, metadata);

            let queueDoc = await mongooseTransactionQueueRepo.findOne({initiating_user_id: initiatingUserId, status : 'QUEUED'});
            should.exist(queueDoc, "transaction was not found");

            await resolverLoopWrapper.processTransactions();

            userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(valueToSend, "should have had IND tokens transfered to")
        })

        it('should try execute a transaction twice which will only transfer once', async () => {
            let userId = "userID123"
            let userAddress = "0x1530df3e1c69501d4ecb7e58eb045b90de158873"
            let initiatingUserId = "initiatingUserID123"
            let valueToSend = 1000
            let metadata = {
                claim_id: "claimID123"
            }
            let userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(0, "should start with 0 IND tokens")

            await indorseTokenTransferExecutor.execute(userId, userAddress, valueToSend, initiatingUserId, metadata);
            await indorseTokenTransferExecutor.execute(userId, userAddress, valueToSend, initiatingUserId, metadata);

            let queueDoc = await mongooseTransactionQueueRepo.findAll({initiating_user_id: initiatingUserId, status : 'QUEUED'});
            queueDoc.length.should.equal(1, "Only one transaction should be found in the queue");
            queueDoc[0].initiating_user_id.should.equal(initiatingUserId, "transaction was not found");

            await resolverLoopWrapper.processTransactions();
            await resolverLoopWrapper.processTransactions();

            userINDbalance = await indorseTokenFacade.balanceOf(userAddress);
            userINDbalance.toNumber().should.equal(valueToSend, "should have had IND tokens transfered to")
        })

        it('should send IND tokens to two users with the same metadata', async () => {
            let userId1 = "userID1"
            let userId2 = "userID2"
            let userAddress1 = "0xf63036962d6955bd08d86b565bf12a8d4acd01c6"
            let userAddress2 = "0x2c456521703fac1cd181384677c176bf1a79ca8b"
            let valueToSend1 = 1000
            let valueToSend2 = 3000
            let initiatingUserId = "initiatingUserID1"
            let metadata = {
                claim_id: "claimID123"
            }
            let userINDbalance1 = await indorseTokenFacade.balanceOf(userAddress1);
            userINDbalance1.toNumber().should.equal(0, "should start with 0 IND tokens")
            let userINDbalance2 = await indorseTokenFacade.balanceOf(userAddress2);
            userINDbalance2.toNumber().should.equal(0, "should start with 0 IND tokens")

            await indorseTokenTransferExecutor.execute(userId1, userAddress1, valueToSend1, initiatingUserId, metadata);
            await indorseTokenTransferExecutor.execute(userId2, userAddress2, valueToSend2, initiatingUserId, metadata);

            let queueDoc = await mongooseTransactionQueueRepo.findAll({initiating_user_id: initiatingUserId, status : 'QUEUED'});
            queueDoc.length.should.equal(2, "Both transactions should be found in the queue");

            await resolverLoopWrapper.processTransactions();

            userINDbalance1 = await indorseTokenFacade.balanceOf(userAddress1);
            userINDbalance2 = await indorseTokenFacade.balanceOf(userAddress2);
            if (userINDbalance1.toNumber() !== valueToSend1 && userINDbalance2.toNumber() !== valueToSend2) {
                throw new Error("one of the accounts should now be funded");
            }

            await resolverLoopWrapper.processTransactions();

            userINDbalance1 = await indorseTokenFacade.balanceOf(userAddress1);
            userINDbalance2 = await indorseTokenFacade.balanceOf(userAddress2);
            userINDbalance1.toNumber().should.equal(valueToSend1, "Account 1 should now be funded")
            userINDbalance2.toNumber().should.equal(valueToSend2, "Account 2 should now be funded")
        })
    })
});
