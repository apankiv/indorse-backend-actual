const testUtils = require('../testUtils');

class TestHelper {
    static getRandomSkill(tags = [], numTags = 2) {
        const skillTags = tags;
        if (skillTags.length === 0) {
            for (let i = 0; i < numTags; i += 1) {
                skillTags.push(testUtils.generateRandomString());
            }
        }
        return {
            name: testUtils.getRandomSkillWithFirstLetterCapital(),
            category: testUtils.generateRandomSkill(),
            tags: skillTags,
            validation: {
                aip: true,
            },
        };
    }
}
module.exports = TestHelper;
