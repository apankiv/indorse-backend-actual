const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const resolver = require('../../../smart-contracts/services/cron/resolverLoop')
  


exports.processTransactions = async function processTransactions(){
    //Find all Queued
    await resolver.sendTransactions();
    await resolver.processTransactions();
}

exports.onlySendTransactions = async function onlySendTransactions() {
    await resolver.sendTransactions();
}


exports.clearAllTransactionQueues = async function clearAllTransactionQueues() {
    await mongooseTransactionRepo.deleteAll();
    await mongooseTransactionQueueRepo.deleteAll();
}