const web3 = require('./testWeb3Provider');
const settings = require('../../../models/settings');

// let initialized = false;

module.exports.initialize = async function initialize() {
    console.log("Sending ETH to [all] the mock lambda addresses ");
    let connectionsLambdaAddress = settings.CONNECTIONS_MOCK_ADDRESS;
    if (web3.eth.getBalance(connectionsLambdaAddress) < web3.toWei(.5, 'ether')) {
        await web3.eth.sendTransaction({
            from: web3.eth.accounts[0],
            to: connectionsLambdaAddress,
            value: web3.toWei(.5, 'ether')
        });
    }
    let indorseTokenLambdaAddress = settings.INDORSETOKEN_MOCK_ADDRESS;
    if (web3.eth.getBalance(indorseTokenLambdaAddress) < web3.toWei(.5, 'ether')) {
        await web3.eth.sendTransaction({
            from: web3.eth.accounts[0],
            to: indorseTokenLambdaAddress,
            value: web3.toWei(.5, 'ether')
        });
    }

    let claimsLambdaAddress = settings.CLAIMS_MOCK_ADDRESS;
    if (web3.eth.getBalance(claimsLambdaAddress) < web3.toWei(.5, 'ether')) {
        await web3.eth.sendTransaction({
            from: web3.eth.accounts[0],
            to: claimsLambdaAddress,
            value: web3.toWei(.5, 'ether')
        });
    }
}