const web3 = require('../initializers/testWeb3Provider');
const merkleClaimsDeploymentService = require('./merkleClaimDeploymentService'); 
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const mongooseContractsRepo = require('../../../models/services/mongo/mongoose/mongooseContractsRepo');
const settings = require('../../../models/settings')
const mochaLambdaInitializer = require('../initializers/mockLambdaInitialize');
const networkInitialize = require('../initializers/networkInitialize');


exports.initialize = async function initialize() {

    await mochaLambdaInitializer.initialize();
    await networkInitialize.initialize();

    let claimsSC = await merkleClaimsDeploymentService.deploy();


    web3.eth.defaultAccount = web3.eth.accounts[0];
    let senderAddress = settings.CLAIMS_MOCK_ADDRESS;

    if (web3.eth.getBalance(senderAddress) < 1000000000000000000) {
        await web3.eth.sendTransaction({from: web3.eth.accounts[0], to: senderAddress, value: 1000000000000000000})
    }

    await mongooseEthNwRepo.insert({
        network: 'LOCAL',
        http_provider: 'http://localhost:8545',
        max_gas_price: 1,
        max_inspection_count : 20
    });

    console.log("Merkle Claims address is " + claimsSC.address);
    await mongooseContractsRepo.insert({
        contract_name: 'MerkleClaims',
        caller_address: senderAddress,
        contract_address: claimsSC.address,
        network: 'LOCAL'
    });
    return claimsSC;
};

