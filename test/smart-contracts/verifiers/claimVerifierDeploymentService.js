const web3 = require('../initializers/testWeb3Provider');
const nVerifier = require('../../../smart-contracts/build/contracts/ClaimSignatureVerifier');
const abi = nVerifier.abi;

exports.abi = abi;

exports.deploy = async function deploy() {
    let nClaimVerifierContract = web3.eth.contract(abi);

    let claimVerifier = await new Promise((resolve, reject) => nClaimVerifierContract.new(
        {
            from: web3.eth.accounts[0],
            data: nVerifier.bytecode,
            gas: '4700000',
            abi : abi
        }, function (error, contract) {
            if (error) {
                reject(error);
            }
            if (contract.address) {
                resolve(contract);
            }
        }));

    return claimVerifier;
};