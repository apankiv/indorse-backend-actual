const web3 = require('../initializers/testWeb3Provider');
const voteVerifierDeploymentService = require('./voteVerifierDeploymentService');
const claimVerifierDeploymentService = require('./claimVerifierDeploymentService');
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const mongooseContractsRepo = require('../../../models/services/mongo/mongoose/mongooseContractsRepo');
const settings = require('../../../models/settings');
const mochaLambdaInitializer = require('../initializers/mockLambdaInitialize');
const networkInitialize = require('../initializers/networkInitialize');

exports.initialize = async function initialize() {

    await mochaLambdaInitializer.initialize();
    await networkInitialize.initialize();

    let voteVerifierSC = await voteVerifierDeploymentService.deploy();
    let claimVerifierSC = await claimVerifierDeploymentService.deploy();

    web3.eth.defaultAccount = web3.eth.accounts[0];
    let senderAddress = settings.CLAIMS_MOCK_ADDRESS;

    if (web3.eth.getBalance(senderAddress) < 1000000000000000000) {
        await web3.eth.sendTransaction({from: web3.eth.accounts[0], to: senderAddress, value: 1000000000000000000})
    }

    await mongooseEthNwRepo.insert({
        network: 'LOCAL',
        http_provider: 'http://localhost:8545',
        max_gas_price: 1,
        max_inspection_count : 20
    });

    console.log("ClaimSignatureVerifier address is " + voteVerifierSC.address);

    await mongooseContractsRepo.insert({
        contract_name: 'ClaimSignatureVerifier',
        caller_address: senderAddress,
        contract_address: claimVerifierSC.address,
        network: 'LOCAL'
    });

    await mongooseContractsRepo.insert({
        contract_name: 'VoteSignatureVerifier',
        caller_address: senderAddress,
        contract_address: voteVerifierSC.address,
        network: 'LOCAL'
    });

    return [voteVerifierSC, claimVerifierSC];
};