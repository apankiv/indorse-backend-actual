const chai = require('chai');
const cryptoUtils = require('../models/services/common/cryptoUtils');
const mongoSkillsRepo = require('../models/services/mongo/mongoRepository')('skills');
const expect = chai.expect;
const keythereum = require("keythereum");

function generateRandomString(length = 12) {
    return cryptoUtils.genRandomString(length);
}

function generateRandomEmail() {
    return generateRandomString() + '@another.com';
}

function generateRandomNumber() {
    return (Math.floor(Math.random() * 1000));
}

function generateRandomBoolean() {
    return Math.random() > 0.5;
}

function generateRndomClaimCreationRequest(title = 'Javascript') {
    return {
        title,
        desc: this.generateRandomString(),
        proof: this.generateRandomString(),
    };
}

function generateRandomPartnerClaimCreationRequest() {
    return {
        skillPrettyId: 'Javascript',
        description: this.generateRandomString(),
        proof: `https://${this.generateRandomString()}`,
        partnerClientId: this.generateRandomString(),
        partnerUserId: this.generateRandomString(),
        state: this.generateRandomString(),
    };
}


function getSignupWizard() {

    return {
        "_id": "5aaa78ba8c9db173ab78ed10",
        "name": "signUp",
        "title": "Step by step guide to get new users onboard",
        "description": "Step by step guide to get new users onboard",
        "steps": [
            {
                "number": 1,
                "title": "Welcome",
                "description": "welcome to indorse",
                "component": "WelcomePage",
                "canSkip": true,
                "canFinish": true,
                "includeInProgress": false
            },
            {
                "number": 2,
                "title": "interests",
                "description": "This will help us understand your needs better",
                "component": "InterestsForm",
                "canSkip": true,
                "canFinish": false,
                "includeInProgress": true
            },
            {
                "number": 3,
                "title": "skills",
                "component": "SkillsForm",
                "canSkip": true,
                "canFinish": false,
                "includeInProgress": true
            },
            {
                "number": 4,
                "title": "Connect social accounts",
                "description": "Social accounts",
                "component": "SocialLinksForm",
                "canSkip": true,
                "canFinish": true,
                "includeInProgress": true
            },
            {
                "number": 5,
                "title": "You earned a badge",
                "description": "Earned a badge",
                "component": "EarnedBadgePage",
                "canSkip": false,
                "canFinish": true,
                "includeInProgress": false
            }
        ]
    }
}

function generateRandomUser() {

    return {
        name: generateRandomString(),
        username: generateRandomString(),
        email: generateRandomEmail(),
        password: generateRandomString()
    }
}

function generateRandomUserSkill() {

    return {
        name: generateRandomString(),
        category: "intermediate"
    }
}


function generateRandomJobRole() {
    return {
        title: generateRandomString(),
        description: generateRandomString(),
        iconUrl: generateRandomString(),
        prettyId: generateRandomString(),
        skillTags: [generateRandomString(), generateRandomString()],
    };
}

function generateRandomJobRoleForm() {
    const { iconUrl, ...data } = this.generateRandomJobRole();
    data.iconData = generateRandomString();
    return data;
}

function generateRandomAssignment() {
    return {
        duration: generateRandomString(),
        description : generateRandomString(),
        title : generateRandomString(),
        scope: generateRandomString(),
        features : [generateRandomString(),generateRandomString(),generateRandomString()]
    }
}

function generateRandomWorkEntry() {

    return {
        company_name: generateRandomString(),
        title: generateRandomString(),
        location: generateRandomString(),
    }
}

function generateRandomSocialLink() {
    return {
        type: generateRandomString(),
        url: generateRandomString()
    }
}

function generateRandomSocialLinkReplacementRequest() {
    return {
        url: generateRandomString()
    }
}

function generateRandomCompany() {
    return {
        additional_data: { field: generateRandomString() },
        pretty_id: generateRandomString(),
        company_name: generateRandomString(),
        logo_data: generateRandomString(),
        description: generateRandomString(),
        cover_data: generateRandomString(),
        social_links: [generateRandomSocialLink()],
        email: generateRandomEmail(),
        tagline: generateRandomString(),
        features: {
            magicLink: true,
            partnerClaims: true,
        },
    };
}

function generateRandomBadge() {

    return {
        pretty_id: generateRandomString(),
        badge_name: generateRandomString(),
        logo_data: generateRandomString(),
        description: generateRandomString(),
        cta_text: generateRandomString(),
        cta_link: generateRandomString(),
    }

}


function generateRandomSkill() {

    return generateRandomString()
}


function generateRandomBadgeGraphQL() {

    return {
        id: generateRandomString(),
        name: generateRandomString(),
        logo_data: generateRandomString(),
        description: generateRandomString(),
        extensions_cta_text: generateRandomString(),
        extensions_cta_link: 'https://' + generateRandomString() + '.com',
        chatbot_uuid: generateRandomString()
    }

}
//TODO clear out this function so we only use one
function generateRandomJob(skills, company) {
    return {
        title: generateRandomString(),
        experienceLevel: 'junior',
        description: generateRandomString(60),
        monthlySalary: generateRandomString(),
        location: generateRandomString(),
        contactEmail: generateRandomEmail(),
        applicationLink:"http://www." + generateRandomString(),
        campaign: {
            title: generateRandomString(),
            description: generateRandomString(),
            enabled: true
        },
        skills: skills,
        company: company
    }

}

function generateRandomBadgeUpdate() {

    return {
        badge_name: generateRandomString(),
        logo_data: generateRandomString(),
        description: generateRandomString(),
        cta_text: generateRandomString(),
        cta_link: generateRandomString(),
    }

}

function generateRandomClaim() {

    return {
        title: generateRandomString(),
        desc: generateRandomString(),
        proof: generateRandomString(),
        state: generateRandomString(),
        ownerid: generateRandomString()
    }
}

function generateRandomVoteSubmitRequest(endorse = true, feedback) {
    if(!feedback){
        feedback = {
            quality: 1,
            designPatterns: 1,
            gitFlow: 1,
            explanation: "ble",
            testCoverage: 1
        }
    }
    return {
        endorse: endorse, feedback: feedback
    }
}

function generateRandomClaimDraftCreationRequest() {

    return {
        title: generateRandomString(),
        desc: generateRandomString(),
        proof: 'https://' + generateRandomString() + '.com',
        level : 'beginner'
    }
}

function generateRandomCompanyUpdate() {

    return {
        additional_data: {field: generateRandomString()},
        company_name: generateRandomString(),
        logo_data: generateRandomString(),
        description: generateRandomString(),
        cover_data: generateRandomString(),
        email: generateRandomEmail()
    }
}

function generateRandomEthAddress() {
    let params = {keyBytes: 32, ivBytes: 16};
    let dk = keythereum.create(params);
    return keythereum.privateKeyToAddress(dk.privateKey);
}

function generateRandomSchool() {

    return {
        school_name: generateRandomString()
    }
}


function generateRandomSkill() {

    return generateRandomString();
}

function getRandomSkillWithFirstLetterCapital() {
    const randomString = generateRandomString();
    return `${randomString.charAt(0).toUpperCase()}${randomString.slice(1)}`;
}

function generateRandomSkills(count) {

    let generatedSkills = [];

    for (let index = 0; index < count; index++) {
        generatedSkills.push(generateRandomSkill())
    }

    return {
        skills: generatedSkills
    };
}

function generateRandomDesignation() {
    return generateRandomString();
}


function generateRandomLanguage() {

    let allowedProficiencies = ['elementary', 'limited_working', 'general_professional', 'advanced_professional', 'native'];

    return {
        language: generateRandomString(),
        proficiency: allowedProficiencies[Math.floor(Math.random() * allowedProficiencies.length)]
    }
}

function getCurrentTimestamp() {
    return Math.round(new Date().getTime() / 1000.0);
}

function getDistantFutureTimestamp() {
    return Math.round(new Date().getTime() / 1000.0) + 1;
}

function getOneDayFutureTimestamp() {
    return Math.round(new Date().getTime() / 1000.0) + (24 * 60 * 60);
}

function delay(duration) {	
  return new Promise(function(resolve, reject){
    setTimeout(function(){
      resolve();
    }, duration)
  });
};

exports.generateRandomNumber = generateRandomNumber;
exports.generateRandomBoolean = generateRandomBoolean;
exports.generateRandomString = generateRandomString;
exports.generateRandomEmail = generateRandomEmail;
exports.generateRandomUser = generateRandomUser;
exports.generateRandomWorkEntry = generateRandomWorkEntry;
exports.generateRandomSchool = generateRandomSchool;
exports.generateRandomCompany = generateRandomCompany;
exports.generateRandomBadge = generateRandomBadge;
exports.generateRandomBadgeGraphQL = generateRandomBadgeGraphQL;
exports.generateRandomLanguage = generateRandomLanguage;
exports.generateRandomSkill = generateRandomSkill;
exports.generateRandomSkills = generateRandomSkills;
exports.getRandomSkillWithFirstLetterCapital = getRandomSkillWithFirstLetterCapital;
exports.generateRandomCompanyUpdate = generateRandomCompanyUpdate;
exports.generateRandomBadgeUpdate = generateRandomBadgeUpdate;
exports.generateRandomSocialLink = generateRandomSocialLink;
exports.generateRandomSocialLinkReplacementRequest = generateRandomSocialLinkReplacementRequest;
exports.getSignupWizard = getSignupWizard;
exports.generateRandomDesignation = generateRandomDesignation;
exports.generateRandomClaim = generateRandomClaim;
exports.getCurrentTimestamp = getCurrentTimestamp;
exports.getDistantFutureTimestamp = getDistantFutureTimestamp;
exports.generateRandomEthAddress = generateRandomEthAddress;
exports.generateRndomClaimCreationRequest = generateRndomClaimCreationRequest;
exports.getOneDayFutureTimestamp = getOneDayFutureTimestamp;
exports.generateRandomUserSkill = generateRandomUserSkill;
exports.generateRandomClaimDraftCreationRequest = generateRandomClaimDraftCreationRequest;
exports.generateRandomJob = generateRandomJob;
exports.generateRandomAssignment = generateRandomAssignment;
exports.generateRandomJobRole = generateRandomJobRole;
exports.generateRandomJobRoleForm = generateRandomJobRoleForm;
exports.generateRandomVoteSubmitRequest = generateRandomVoteSubmitRequest;
exports.delay = delay;
exports.generateRandomPartnerClaimCreationRequest = generateRandomPartnerClaimCreationRequest;

/**
 *
 * @param count
 * @returns {Array}
 */
exports.generateRandomUsers = function generateRandomUsers(count) {
    let generatedUsers = [];

    for (let index = 0; index < count; index++) {
        generatedUsers.push(generateRandomUser())
    }

    return generatedUsers;
};

exports.wait = ms => new Promise(resolve => setTimeout(resolve, ms));

exports.addSkillsToMongoCollection = async function addSkillsToMongoCollection(skills) {
    skills.forEach(async (skill) => {
        let skillsDocument = {name: skill};
        await mongoSkillsRepo.insert(skillsDocument)
    })
}

exports.getNearByRandomTs = function getNearByRandomTs(millis = 1000) {
    const currentTs = Math.round(new Date().getTime() / 1000);
    const randomNum = Math.round(Math.random() * millis);
    if (Math.ceil(Math.random() * 2) % 2) {
        return currentTs + randomNum;
    }
    return currentTs - randomNum;
}

exports.createRandomJob = async function createRandomJob() {
    const jobPost = {
        title: generateRandomString(),
        experienceLevel: "intern",
        description: generateRandomString(),
        monthlySalary: generateRandomString(),
        contactEmail: generateRandomEmail(),
        applicationLink: "http://" + generateRandomString() +".com",
        location: generateRandomString(),
        campaign: {
            title: generateRandomString(),
            description: generateRandomString(),
            enabled: true
        },
        submitted: {
            by: generateRandomString()           
        },
        approved: {
            by: generateRandomString(),
            approved: true
        },
        updated: {
            by: generateRandomString()          
        }
    }
    return jobPost;
}
