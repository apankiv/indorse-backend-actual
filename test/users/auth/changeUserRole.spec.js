process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.changeUserRole', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /changeuserrole', () => {

        it('it should change user role', async (done) => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let normalUser = {
                name: 'Dude',
                username: 'd00d',
                email: 'doodsky@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);
                await authenticationWrapper.justSignUpNoVerify(normalUser);

                await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});
                let normalUserFromMongo = await mongoUserRepo.findOne({email: normalUser.email});
                await mongoUserRepo.update({email: normalUser.email}, {$set: {role: 'no_access'}});

                let newRole = 'full_access';

                let res = await chai.request(server)
                    .post('/changeuserrole')
                    .set('Authorization', 'Bearer ' + token)
                    .send({role: newRole, user_id: normalUserFromMongo._id});

                res.should.have.status(200);

                let updatedNormalUser = await mongoUserRepo.findOne({email: normalUser.email});

                updatedNormalUser.role.should.equal(newRole);
                res.body.profile.name.should.equal(normalUser.name);
                res.body.profile.email.should.equal(normalUser.email);
                res.body.profile.username.should.equal(normalUser.username);
                should.not.exist(res.body.profile.pass);
                should.not.exist(res.body.profile.salt);
                should.not.exist(res.body.profile.tokens);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('it should fail for invalid role', async (done) => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let normalUser = {
                name: 'Dude',
                username: 'd00d',
                email: 'doodsky@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {

                await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);
                await authenticationWrapper.justSignUpNoVerify(normalUser);

                await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

                let normalUserFromMongo = await mongoUserRepo.findOne({email: normalUser.email});
                await mongoUserRepo.update({email: normalUser.email}, {$set: {role: 'no_access'}});

                let newRole = 'clown';

                await chai.request(server)
                    .post('/changeuserrole')
                    .set('Authorization', 'Bearer ' + token)
                    .send({role: newRole, user_id: normalUserFromMongo.user_id});

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        });

        it('should fail for non-admin user', async (done) => {

            let normalUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {

                let createdUser = await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

                await chai.request(server)
                    .post('/changeuserrole')
                    .set('Authorization', 'Bearer ' + token)
                    .send({role: 'admin', user_id: createdUser.user_id});

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        })
    })
});