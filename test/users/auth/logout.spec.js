process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
chai.use(chaiHttp);

describe('users.logout', function () {

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /logout', () => {

        it('should log out authenticated user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/logout')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({});

                await chai.request(server)
                    .post('/logout')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({});

                done(new Error("Expected error!"));
            } catch (error) {
                done();
            }
        })

        it('should fail for unauthenticated user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.justSignUpNoVerify(user);

                await chai.request(server)
                    .post('/logout')
                    .send({});

                done(new Error("Expected error!"));
            } catch (error) {
                done();
            }
        })

    })
});
