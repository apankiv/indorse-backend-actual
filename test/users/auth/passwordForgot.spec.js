process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');

chai.use(chaiHttp);

describe('users.passwordForgot', function () {

    this.timeout(10000);

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /password/forgot', () => {

        it('should fail if user does not exist', async (done) => {

            try {
                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: 'some@email.com'});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(404);
                done();
            }
        });

        it('should fail if email is missing', async (done) => {

            try {
                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: ''});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(422);
                done();
            }
        });

        it('should fail for unverified user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            try {
                await authenticationWrapper.justSignUpNoVerify(user);

                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: user.email});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });

        it('should create token and timestamp for existing user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: user.email});

                let userFromMongo = await mongoUserRepo.findOne({email: user.email});

                should.exist(userFromMongo.pass_verify_token);
                should.exist(userFromMongo.pass_verify_timestamp);

                done();
            } catch (error) {
                done(error);
            }
        })

    })
});