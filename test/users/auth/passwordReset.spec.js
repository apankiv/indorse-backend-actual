process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');

chai.use(chaiHttp);

describe('users.passwordReset', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /password/reset', () => {

        it('should fail if user does not exist', async (done) => {

            let passwordResetRequest = {
                email: 'some@email.com',
                pass_token: 'sometoken',
                password: 'somepassword'
            };

            try {
                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });

        it('should fail if email is missing', async (done) => {

            let passwordResetRequest = {
                pass_token: 'sometoken',
                password: 'somepassword'
            };

            try {
                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(422);
                done();
            }
        });

        it('should fail if pass_token is missing', async (done) => {

            let passwordResetRequest = {
                email: 'some@email.com',
                password: 'somepassword'
            };

            try {
                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(422);
                done();
            }
        });

        it('should fail if password is missing', async (done) => {

            let passwordResetRequest = {
                email: 'some@email.com',
                pass_token: 'sometoken'
            };

            try {
                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(422);
                done();
            }
        });

        it('should fail if to password forgot request has not been sent by the user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordResetRequest = {
                email: user.email,
                pass_token: 'sometoken',
                password: 'somepassword'
            };

            try {

                await authenticationWrapper.signupVerifyAuthenticate(user, {});

                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });

        it('should should successfully change password', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {

                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: user.email});

                let userFromMongo = await mongoUserRepo.findOne({email: user.email});

                let passwordResetRequest = {
                    email: user.email,
                    pass_token: userFromMongo.pass_verify_token,
                    password: 'newPassword'
                };

                let res = await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                res.status.should.equal(200);

                let loginResult = await chai.request(server)
                    .post('/login')
                    .send({email: user.email, password: passwordResetRequest.password});

                loginResult.status.should.equal(200);
                should.exist(loginResult.body.token);

                userFromMongo = await mongoUserRepo.findOne({email: user.email});

                should.not.exist(userFromMongo.pass_verify_token);
                should.not.exist(userFromMongo.pass_verify_timestamp);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('should should not change password if token is invalid', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };


            try {

                await authenticationWrapper.signupVerifyAuthenticate(user, {});

                await chai.request(server)
                    .post('/password/forgot')
                    .send({email: user.email});


                let passwordResetRequest = {
                    email: user.email,
                    pass_token: 'invalidToken',
                    password: 'newPassword'
                };

                await chai.request(server)
                    .post('/password/reset')
                    .send(passwordResetRequest);

                await chai.request(server)
                    .post('/login')
                    .send({email: user.email, password: passwordResetRequest.password});

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });


    })
});
