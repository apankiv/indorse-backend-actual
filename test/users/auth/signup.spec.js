process.env.NODE_ENV = 'test';

const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../server')
    , should = chai.should()
    , DB = require('../../db')
    , testUtils = require('../../testUtils')
    , authenticationWrapper = require('../../authenticationWrapper')
    , config = require('config');
chai.use(chaiHttp)

describe('/POST signup', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should fail if user exists', async (done) => {
        let user = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            password: 'password'
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await chai.request(server)
                .post('/signup')
                .send(user);

            done(new Error("Expected failure!"));
        } catch (error) {
            done();
        }
    });

    it('should throw error if username or password is missing', async (done) => {
        let user = {
            email: 'p@example.com'
        };

        try {
            await chai.request(server)
                .post('/signup')
                .send(user);

            done(new Error("Expected failure!"));
        } catch (error) {
            done();
        }
    });



    it('should create a new user in database if user is created successfully', async (done) => {
        let user = {
            name: 'Another Person',
            username: 'username',
            email: 'person@another.com',
            password: 'password'
        };

        try {
            let res = await chai.request(server)
                .post('/signup')
                .send(user);

            await testUtils.wait(config.get('test.bcTransactionTime'));

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.message.should.equal('Verification email sent successfully');
            let users = DB.getDB().collection('users');
            let item = await users.findOne({email: user.email});
            item.name.should.equal(user.name);
            item.email.should.equal(user.email);
            const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');

            let mongoDoc = await mongoUserRepo.findOne({
                '$or': [{'email': user.email},
                    {'username': user.username}]
            });
            mongoDoc.email.should.equal(user.email);
            mongoDoc.username.should.equal(user.username);
            await testUtils.wait(config.get('test.bcTransactionTime'));
            done();
        } catch (error) {
            done(error);
        }
    })

    it('username should be case insensitive', async (done) => {
        let user = {
            name: 'Person',
            username: 'UsErNaMe',
            email: 'p@example.com',
            password: 'password'
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let res = await chai.request(server)
                .post('/me')
                .send({username: user.username.toLowerCase()});

            let userFromResponse = res.body.profile;

            userFromResponse.username.should.equal(user.username.toLowerCase());

            res = await chai.request(server)
                .post('/me')
                .send({username: user.username.toUpperCase()});

            userFromResponse = res.body.profile;

            userFromResponse.username.should.equal(user.username.toLowerCase());

            done();
        } catch (error) {
            done(error);
        }
    });
});
