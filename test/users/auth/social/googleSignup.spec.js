// process.env.NODE_ENV = 'test';
//
// const chai = require('chai')
//     , chaiHttp = require('chai-http')
//     , server = require('../../../../server')
//     , should = chai.should()
//     , DB = require('../../../db')
//     , testUtils = require('../../../testUtils')
//     , config = require('config')
//     , sinon = require('sinon')
//     , gUtils = require("../../../../models/services/social/googleService");
// ;
//
// const contractSettingInitializer = require('../../../smart-contracts/connections/contractSettingInitializer');
// const invitationWrapper = require('../../../companies/advisors/invitationWrapper');
// const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
// const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
// chai.use(chaiHttp)
// // actual token
// const IDTokenA = 'realtoken';
//
//
// // all the test cases shuold skip the validation part.
// describe('googleSignup.spec.js', function () {
//     this.timeout(config.get('test.timeout'));
//
//     let testEmail = 'Bob@indorse.io'
//     let testEmailInDB = testEmail.toLowerCase(); // an email with upper cases will be stored as all lower case. Hence, if you want to compare the mongo email value, you should use this value instead.
//     let googleUid = 'abc'
//     let stub;
//     before(() => {
//         stub = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
//             return [testEmail, googleUid]
//         });
//     });
//
//     after(() => {
//         stub.restore();
//     });
//
//     let SC;
//
//     beforeEach(async (done) => {
//         console.log('connecting to database');
//         SC = await contractSettingInitializer.initialize();
//         DB.connect(done);
//     });
//
//     afterEach((done) => {
//         console.log('dropping database');
//         DB.drop(done);
//     });
//
//
//     // 3. signup
//     it('should signup a user', async () => {
//
//         let adminUser = testUtils.generateRandomUser();
//
//         let createCompanyRequest = testUtils.generateRandomCompany();
//
//         let user_signup = {
//             username: 'tester',
//             name: 'bob',
//             google: {
//                 id_token: IDTokenA
//             }
//         };
//
//         await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, createCompanyRequest, {}, testEmail);
//
//         let res = await chai.request(server)
//             .post('/signup/google')
//             .send(user_signup);
//         res.should.have.status(200);
//
//         let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
//         mongoDoc.email.should.equal(testEmailInDB);
//         mongoDoc.username.should.equal(user_signup.username);
//
//         let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
//
//         let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(testEmail);
//
//         companyFromMongo.advisors.length.should.equal(1);
//
//         let advisorFromMongo = companyFromMongo.advisors[0];
//
//         should.exist(advisorFromMongo.creation_timestamp);
//         should.not.exist(advisorFromMongo.email);
//         advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
//
//
//     });
//
// });
