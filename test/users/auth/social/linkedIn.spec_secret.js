process.env.NODE_ENV = 'test_secret'
const mongo = require('mongodb')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const testUtils = require('../../../testUtils')
const config = require('config')
const sinon = require('sinon')
const linkedInUtils = require("../../../../models/services/social/linkedInService");
const googleAuthenticationWrapper = require('../../../services/auth/googleAuthenticationWrapper')


const mongoUserRepo = require('../../../../models/services/mongo/mongoRepository')('users');

chai.use(chaiHttp)

describe('linkedIn.spec_secret.js', function () {
    this.timeout(config.get('test.timeout'));


    let linkedInUid = 'abcdefg'
    let name = 'firstname lastname'
    let email = 'bob@indorse.io'
    let username = 'something'
    let linkedInAccessToken = config.get('auth.linkedInAccessTokn') //put your access token (the token has life of 60 days)

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should calculate the confidence score with Jae Seok An when linking, and should return ', async () => {

        let googleUid = 'abc';
        let testEmail = 'bob@tfbnw.net';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLink = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }
        let tokenObj = {};

        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns(linkedInAccessToken);
        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let res = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLink);
        res.should.have.status(200);
        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
        Number(mongoDoc.confidenceScore.linkedInScore.score).should.equal(Number(7.14));
        mongoDoc.confidenceScore.linkedInScore.isVerified.should.equal(true);

        stubGetAccessToken.restore();
    });

    it('should calculate the confidence score with Jae Seok An when logging in, and should return ', async () => {
        let testEmail = 'jaeseokn@gmail.com';

        let user_signup = {
            username : 'imtester',
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns(linkedInAccessToken);

        let res = await chai.request(server)
            .post('/auth/linked-in')
            .send(user_signup);

        res.should.have.status(200);

        res = await chai.request(server)
            .post('/auth/linked-in')
            .send(user_login);

        res.should.have.status(200);
        should.exist(res.body.token);

        res.should.have.status(200);
        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
        Number(mongoDoc.confidenceScore.linkedInScore.score).should.equal(Number(7.14));
        mongoDoc.confidenceScore.linkedInScore.isVerified.should.equal(true);

        stubGetAccessToken.restore();
    });

});
