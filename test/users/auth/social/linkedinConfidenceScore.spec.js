process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const googleAuthenticationWrapper = require('../../../services/auth/googleAuthenticationWrapper')
const config = require('config')
const sinon = require('sinon')
const fbUtils = require("../../../../models/services/social/facebookService");
const linkedInUtils = require("../../../../models/services/social/linkedInService");
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

chai.use(chaiHttp) 

describe('linkedinConfidenceScore.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to the database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);        
    });

    it('should link Indorse account (that is signed up via Google) with LinkedIn', async () => {
        let googleUid = 'abc';
        let linkedInUid = 'def';
        let testEmail = 'bob@gmail.com';


        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let testLinkedInParams = [
        {
            name: 'num-connections',
            value: 300
        },
        {
            name: 'num-connections-capped',
            value: true
        },
        {
            name: 'summary',
            value: true
        },
        {
            name: 'picture-url',
            value: true 
        }
        ]


        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, userSignup, testEmail]
        });
        let stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns(testLinkedInParams);

        let res = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        Number(mongoDoc.confidenceScore.linkedInScore.score).should.equal(Number(10));
        Number(mongoDoc.confidenceScore.aggregateScore.score).should.equal(Number(5));
        mongoDoc.confidenceScore.linkedInScore.isVerified.should.equal(true);
        mongoDoc.confidenceScore.aggregateScore.isVerified.should.equal(true);

        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
        stubGetSocialParams.restore();
    });
});
