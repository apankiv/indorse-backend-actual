process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../../server');
const should = chai.should();
const DB = require('../../../db');
const googleAuthenticationWrapper = require('../../../services/auth/googleAuthenticationWrapper');
const facebookAuthenticationWrapper = require('../../../services/auth/facebookAuthenticationWrapper');
const config = require('config');
const sinon = require('sinon');
const gUtils = require("../../../../models/services/social/googleService");
const fbUtils = require("../../../../models/services/social/facebookService");
const civicUtils = require("../../../../models/services/social/civicUtils");
const uportUtils = require("../../../../models/services/social/uportUtils");
const linkedInUtils = require("../../../../models/services/social/linkedInService");
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');


chai.use(chaiHttp)

describe('socialLinking.spec.js', function () {
    this.timeout(config.get('test.timeout'));


    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);        
    });

    it('should link Indorse account (that is signed up via Google) with LinkedIn', async () => {
        let googleUid = 'abc';
        let linkedInUid = 'def';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, userSignup, testEmail]
        });
        let stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns();

        let res = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);
        mongoDoc.google_uid.should.equal(googleUid);

        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
        stubGetSocialParams.restore();
    });

    it('should not link Indorse account (that is signed up via Google and having ethereum address on profile) with Airbitz', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4=';
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
        let buf_v = '28'
        let signature_buf = {buf_r, buf_s, buf_v};

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signature_buf
        };

        let userProfileUpdate = {
            name: 'tester',
            bio: 'about myself Im a tester',
            ethaddress: '0x8af51b3415e58c55c2c11663cabc7ef8bad518ee'
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let res = await chai.request(server)
            .post('/updateprofile')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userProfileUpdate);

        res.should.have.status(200);

        try {
            res = await chai.request(server)
                .post('/link/airbitz')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userLinkingRequest);


            throw new Error('Expecting to fail');
        } catch (error) {
            error.should.have.status(400);
        }
    });


    it('should link Indorse account (that is signed up via Google) with Airbitz', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4=';
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
        let buf_v = '28'
        let signature_buf = {buf_r, buf_s, buf_v};

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signature_buf
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let res = await chai.request(server)
            .post('/link/airbitz')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.is_airbitz_user.should.equal(true);
        mongoDoc.google_uid.should.equal(googleUid);
    });

    it('should link Indorse account (that is signed up via Google) with Facebook', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            facebook: {
                accessToken: 'token',
                userID: 'userID'
            }
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").returns();
        let stubFacebookVar = sinon.stub(fbUtils, "getFacebookSocialParams").returns(null);

        let res = await chai.request(server)
            .post('/link/facebook')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);
        stubFacebook.restore();
        stubFacebookVar.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(userLinkingRequest.facebook.userID);
        mongoDoc.google_uid.should.equal(googleUid);
    });

    it('should link Indorse account (that is signed up via Google) with Facebook (Should be able to link without email)', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            facebook: {
                accessToken: 'token',
                userID: 'userID'
            }
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").returns();
        let stubFacebookVar = sinon.stub(fbUtils, "getFacebookSocialParams").returns(null);

        let res = await chai.request(server)
            .post('/link/facebook')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);
        stubFacebook.restore();
        stubFacebookVar.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(userLinkingRequest.facebook.userID);
        mongoDoc.google_uid.should.equal(googleUid);
    });

    it('should fail link Indorse account (that is signed up via Google) with Facebook since ' +
        'facebook_uid exists. This users facebookConfidenceScore should not be updated ', async () => {
        let googleUid = 'abc';
        let facebookUid = 'thisisfacebookUid'
        let testEmail = 'bob@indorse.io';
        let testEmailFacebookUser = 'alice@indorse.io'
        let facebookToken = 'thisisfacebookToken'

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userSignupFacebook = {
            username: 'userB',
            name: 'alice',
            facebook: {
                accessToken: facebookToken,
                userID: facebookUid
            }
        }

        let userLinkingRequest = {
            facebook: {
                accessToken: facebookToken,
                userID: facebookUid
            }
        };

        let tokenObjUserA = {};
        let tokenObjUserB = {};
        let stubFacebookVar, stubFacebook;

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObjUserA);
        stubFacebookVar = sinon.stub(fbUtils, "getFacebookSocialParams").returns(null);

        await facebookAuthenticationWrapper.facebookAuthenticate(userSignupFacebook, testEmailFacebookUser, tokenObjUserB);

        stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").returns();

        try {
            let res = await chai.request(server)
                .post('/link/facebook')
                .set('Authorization', 'Bearer ' + tokenObjUserA.token)
                .send(userLinkingRequest);

            throw new Error("Expected to fail")
        } catch (error) {
            error.should.have.status(400);
            stubFacebook.restore();
            stubFacebookVar.restore();
        }

        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.google_uid.should.equal(googleUid);
        should.not.exist(mongoDoc.facebook_uid);
        should.not.exist(mongoDoc.confidenceScore)
    });

    it('should link Indorse account (that is signed up via Facebook) with Google', async () => {
        let email = 'bob@facebook.com'
        let facebookUid = 'userID'
        let googleUid = 'googeUID'
        let userSignup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: 'tokenFacebook',
                userID: facebookUid
            }
        };

        let userLinkingRequest = {
            google: {
                id_token: 'tokenGoogle'
            }
        };
        let testFaceBookParams = [{
            name: 'likes',
            value: 100
        },
            {
                name: 'is_verfified',
                value: true
            },
            {
                name: 'verfified',
                value: true
            },
            {
                name: 'friends',
                value: 215
            }
        ]

        let tokenObj = {};

        let stubFacebook = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);

        await facebookAuthenticationWrapper.facebookAuthenticate(userSignup, email, tokenObj);

        let stubGoogle = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
            return [email, googleUid]
        });
        let res = await chai.request(server)
            .post('/link/google')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);
        stubGoogle.restore();
        stubFacebook.restore();
        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(email);
        mongoDoc.email.should.equal(email);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(facebookUid);
        mongoDoc.google_uid.should.equal(googleUid);
    });


    it('should fail when user is not logged in (situation : there is a valid user signed up with facebook.' +
        'This user sent a request to link to Google with valid accessToken which returns google email ' +
        'that is matching with existing facebook_uid. However, this request is made from someone who is not logged in.)', async () => {
        let email = 'bob@facebook.com'
        let facebookUid = 'userID'
        let userSignup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: 'tokenFacebook',
                userID: facebookUid
            }
        };

        let userLinkingRequest = {
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let stubGoogle = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
            return [email, 'tobestubbed']
        });
        let tokenObj = {};
        let stubFacebook = sinon.stub(fbUtils, "getFacebookSocialParams").returns();
        await facebookAuthenticationWrapper.facebookAuthenticate(userSignup, email, tokenObj);
        stubFacebook.restore();

        try {
            // making a request without JWT
            let res = await chai.request(server)
                .post('/link/google')
                .send(userLinkingRequest);

            throw new Error("Expected to fail")
        } catch (error) {
            stubGoogle.restore();
            error.should.have.status(400)
        }
    });


    it('should link Indorse account (that is signed up via Facebook) with LinkedIn, ' +
        'and other user should not be able to signup with the identical LinkedIn_uid' +
        ' (username exist in signup request). ', async () => {
        let email = 'bob@facebook.com'
        let facebookUid = 'userID'
        let linkedInUid = 'linkedInUid'
        let userSignup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: 'tokenFacebook',
                userID: facebookUid
            }
        };

        let userLinkingRequest = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };


        let tokenObj = {};

        let stubFacebook = sinon.stub(fbUtils, "getFacebookSocialParams").returns();
        await facebookAuthenticationWrapper.facebookAuthenticate(userSignup, email, tokenObj);
        stubFacebook.restore();

        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns();
        let stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, 'randomstring', '']
        });

        let res = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);
        stubGetAccessToken.restore();
        stubGetSocialParams.restore();
        stubGetUserInfo.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(email);
        mongoDoc.email.should.equal(email);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(facebookUid);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);


        let userSignupRequestLinkedIn = {
            username: 'uniqueUsername',
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, 'bob', 'newUser@indorse.io']
        });

        try {
            let res = await chai.request(server)
                .post('/auth/linked-in')
                .send(userSignupRequestLinkedIn);

            throw new Error("Expected to fail")
        } catch (error) {
            error.should.have.status(400);
            stubGetAccessToken.restore();
            stubGetUserInfo.restore();
        }
    });


    it('should link Indorse account (that is signed up via Facebook) with LinkedIn, ' +
        'and the user should be able to login via linkedIn '
        , async () => {
            let email = 'bob@facebook.com'
            let facebookUid = 'userID'
            let linkedInUid = 'linkedInUid'
            let userSignup = {
                username: 'tester',
                name: 'bob',
                facebook: {
                    accessToken: 'tokenFacebook',
                    userID: facebookUid
                }
            };

            let userLinkingRequest = {
                linked_in: {
                    code: 'code',
                    state: 'state',
                    redirect_uri: 'http://localhost:4000/linked-in-callback.html'
                }
            };

            let tokenObj = {};
            let stubGetAccessToken, stubGetUserInfo, stubGetSocialParams

            let stubFacebook = sinon.stub(fbUtils, "getFacebookSocialParams").returns();
            await facebookAuthenticationWrapper.facebookAuthenticate(userSignup, email, tokenObj);
            stubFacebook.restore();

            stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
            stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns();
            stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
                return [linkedInUid, 'randomstring', '']
            });

            let res = await chai.request(server)
                .post('/link/linked-in')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userLinkingRequest);

            res.should.have.status(200);

            // attempt to signup without username -> This will login the user
            let signupToken;

            let userSignupRequestLinkedIn = {
                linked_in: {
                    code: 'code',
                    state: 'state',
                    redirect_uri: 'http://localhost:4000/linked-in-callback.html'
                }
            };

            res = await chai.request(server)
                .post('/auth/linked-in')
                .send(userSignupRequestLinkedIn);
            res.should.have.status(200);
            stubGetAccessToken.restore();
            stubGetUserInfo.restore();
            stubGetSocialParams.restore();
        });


    it('should link Indorse account (that is signed up via Google) with Facebook and LinkedIn', async () => {
        let googleUid = 'abc';
        let linkedInUid = 'def'
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequestFB = {
            facebook: {
                accessToken: 'token',
                userID: 'userID'
            }
        };

        let userLinkingRequestLinkedIn = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").returns();
        let stubFacebookVar = sinon.stub(fbUtils, "getFacebookSocialParams").returns(null);

        let res = await chai.request(server)
            .post('/link/facebook')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequestFB);
        stubFacebook.restore();
        stubFacebookVar.restore();

        res.should.have.status(200);

        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns();
        let stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, 'randomstring', '']
        });
        let resLinkedIn = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequestLinkedIn);

        stubGetAccessToken.restore();
        stubGetSocialParams.restore();
        stubGetUserInfo.restore();

        resLinkedIn.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(userLinkingRequestFB.facebook.userID);
        mongoDoc.google_uid.should.equal(googleUid);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);
    });


    it('should link Indorse account (that is signed up via Google) with Civic', async () => {
        let googleUid = 'abc';
        let linkedInUid = 'def'
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingCivic = {
            civic : {
                id_token: 'tokenCivic'
            }
        }
        
        let civicUserData = [
            {
                name :'email',
                value: 'civicuser@civic.com'
            },
            {
                name : 'phone',
                value: '+44 223001202'
            }
        ]

        let civicEmail = 'civicuser@civic.com';
        let civicUid = 'civicuid'

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
        let stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData(){
            return [civicEmail,civicUid,civicUserData];
        });


        let res = await chai.request(server)
            .post('/link/civic')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingCivic);
        stubCivic.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.civic_uid.should.equal(civicUid);
    });


    it('should fail to link Indorse account (that is signed up via Google) with Civic if Civic accout is associated already', async () => {
        //User 1
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        //User 2
        let googleUid2 = 'mno';        
        let testEmail2 = 'jill@gmail.com';
        
        let user2Signup = {
            username: 'tester2',
            name: 'Jill',
            google: {
                id_token: 'tokenGoogle2'
            }
        };        

        let userLinkingCivic = {
            civic: {
                id_token: 'tokenCivic'
            }
        }

        let civicUserData = [
            {
                name: 'email',
                value: 'civicuser@civic.com'
            },
            {
                name: 'phone',
                value: '+44 223001202'
            }
        ]

        let civicEmail = 'civicuser@civic.com';
        let civicUid = 'civicuid'

        let tokenObj = {};
        let tokenObj2 = {};
        let stubCivic;

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
        await mongooseUserRepo.update({ 'email': testEmail }, { '$set': { 'civic_uid': civicUid }});

        stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData() {
            return [civicEmail, civicUid, civicUserData];
        });

        await googleAuthenticationWrapper.googleAuthenticate(user2Signup, testEmail2, googleUid2, tokenObj2);

        try {
            await chai.request(server)
                .post('/link/civic')
                .set('Authorization', 'Bearer ' + tokenObj2.token)
                .send(userLinkingCivic);

            throw Error('Executed multiple linking of one Civic id to multiple accounts');
        } catch (error) {
            stubCivic.restore();
            error.status.should.equal(400);
        }
    });

    it('should link Indorse account (that is signed up via Google) with uport', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingUport = {
            uport: {
                "@context": "http://schema.org",
                "@type": "Person",
                "public_key": "0x040ad8dc6c15d547c6814c333770f1d956666b7066addb4b938fc89f71e5eccb729f9b5192321347e09fb265c459b9464ae65361779dbeb9e9f37b3ea0e7545278",
                "publicEncKey": "WJtg8T80nFLN4pWg1JNkm/88qHbL4D32ktcM7ZIddn8=",
                "name": "Mark Le",
                "phone": "+6594465904",
                "country": "SG",
                "pushToken(pin)": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NksifQ.eyJpc3MiOiIyb2paeExYNkFUdW5WVkNXUTRCS1RTVWVMcjkzejR0UGdKbiIsImlhdCI6MTUyMjgyODE5MSwiYXVkIjoiMm9oUE50bnZQb0dhSnh2Wm1wV0Y1bnRHYXE2R25SMnRSYWYiLCJ0eXBlIjoibm90aWZpY2F0aW9ucyIsInZhbHVlIjoiYXJuOmF3czpzbnM6dXMtd2VzdC0yOjExMzE5NjIxNjU1ODplbmRwb2ludC9BUE5TL3VQb3J0LzU5ODg2NTY2LTA1YWItMzQwZi1hZTQ2LTM0ZTIxYzljZjdmOCIsImV4cCI6MTUyNDEyNDE5MX0.OWkZBJbFL0bBEUUMDWUYb6Kg4YTs6ALKjJ2VIgEGGudRXuKaCYTCmDA4MwwipPPR8eK8mYOgVZOkCNaQ_pQBSQ",
                "address": "2ojZxLX6ATunVVCWQ4BKTSUeLr93z4tPgJn",
                "networkAddress": "2ojZxLX6ATunVVCWQ4BKTSUeLr93z4tPgJn"
            }
        }
      
        let uportUid = '0x040ad8dc6c15d547c6814c333770f1d956666b7066addb4b938fc89f71e5eccb729f9b5192321347e09fb265c459b9464ae65361779dbeb9e9f37b3ea0e7545278'

        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
        let stubUport = sinon.stub(uportUtils, "validateAndGetUserData").callsFake(function validateData() {
            return [uportUid, userLinkingUport.uport];
        });

        let res = await chai.request(server)
            .post('/link/uport')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingUport);
        stubUport.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.uport_uid.should.equal(uportUid);
    });
});
