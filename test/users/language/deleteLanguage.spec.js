process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);

describe('users.deleteLanguage', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    after((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /deletelanguage', () => {
        it('after updating language delete should remove the update', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            let response = await chai.request(server)
                .post('/me')
                .send({email: user.email})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let profile = response.body.profile;

            let item_key = profile.language[0].item_key;

            await chai.request(server)
                .post('/deletelanguage')
                .set('Authorization', 'Bearer ' + token)
                .send({item_key: item_key})

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.language.length.should.equal(0);
        })

        it('should fail if no item key in request', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                await chai.request(server)
                    .post('/deletelanguage')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({});

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('should fail for unauthenticated user', async () => {

            try {
                await chai.request(server)
                    .post('/deletelanguage')
                    .send({item_key : 'test'});

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    });
});