process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoLanguageRepository = require('../../../models/services/mongo/mongoRepository')('languages');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
const testUtils = require('../../testUtils');
chai.use(chaiHttp);

describe('users.findLanguage', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /findlanguage', () => {

        it('should return 0 languages if 0 language matches in db', async () => {
            let res = await chai.request(server)
                .post('/findlanguage')
                .send({language: 'test'});

            res.body.languages.length.should.be.equal(0);
        });

        it('should return 1 language if 1 language matches in db', async () => {

            let languageInDB = testUtils.generateRandomLanguage();

            await mongoLanguageRepository.insert(languageInDB);

            let res = await chai.request(server)
                .post('/findlanguage')
                .send({language: languageInDB.language});

            res.body.languages.length.should.be.equal(1);
            res.body.languages[0].language.should.be.equal(languageInDB.language);
        });

        it('should fail if no language in request', async () => {

            try {
                await chai.request(server)
                    .post('/findlanguage')
                    .send({});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        })

    })
});
