process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoLanguageRepo = require('../../../models/services/mongo/mongoRepository')('languages');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.updateLanguage', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /updatelanguage', () => {

        it('if there is no language item it create language item and update the user', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.language.length.should.equal(1);
            userFromMongo.language[0].language.should.equal(languageUpdate.language);
            userFromMongo.language[0].proficiency.should.equal(languageUpdate.proficiency);

            should.exist(userFromMongo.language[0].language_id);
        });

        it('if there is item_key in request it should only update user entry', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();
            let languageUpdate2 = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            let item_key = userFromMongo.language[0].item_key;
            languageUpdate2.item_key = item_key;
            languageUpdate2.language_id = userFromMongo.language[0].language_id;

            response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate2);

            response.should.have.status(200);

            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.language.length.should.equal(1);
            userFromMongo.language[0].proficiency.should.equal(languageUpdate2.proficiency);

            should.exist(userFromMongo.language[0].item_key);
        });


        it('if the language already exists it should not create language item, it should only update the user', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            languageUpdate.item_key = testUtils.generateRandomString();

            await mongoLanguageRepo.insert(languageUpdate);

            delete languageUpdate.item_key;
            delete languageUpdate._id;

            let response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.language[0].language.should.equal(languageUpdate.language);

            should.exist(userFromMongo.language[0].language_id);

            let languageFromMongo = await mongoLanguageRepo.findOne({language: languageUpdate.language});

            should.not.exist(languageFromMongo.keyPair);
        });

        it('it should be able to update language twice', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();
            let languageUpdate2 = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.language[0].language.should.equal(languageUpdate.language);

            should.exist(userFromMongo.language[0].language_id);

            response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate2);

            response.should.have.status(200);
            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.language.length.should.equal(2);

            userFromMongo.language[1].language.should.equal(languageUpdate2.language);

            should.exist(userFromMongo.language[1].language_id);
        });

        it('it should language if user has no bigchain profile', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();
            let languageUpdate2 = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
            await mongoUserRepo.update({email: user.email}, {$set: {keyPair: null}});

            let response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            let item_key = userFromMongo.language[0].item_key;
            languageUpdate2.item_key = item_key;
            languageUpdate2.language_id = userFromMongo.language[0].language_id;

            response = await chai.request(server)
                .post('/updatelanguage')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(languageUpdate2);

            response.should.have.status(200);

            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.language.length.should.equal(1);
            userFromMongo.language[0].proficiency.should.equal(languageUpdate2.proficiency);

            should.exist(userFromMongo.language[0].language_id);

            let languageFromMongo = await mongoLanguageRepo.findOne({language: languageUpdate.language});
            should.exist(languageFromMongo);
        });

        it('it should fail for unauthenticated user', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            try {
                await chai.request(server)
                    .post('/updatelanguage')
                    .send(languageUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail when there is no language in request', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            delete languageUpdate.language;

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                await chai.request(server)
                    .post('/updatelanguage')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(languageUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail when there is no proficiency in request', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            delete languageUpdate.proficiency;

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                await chai.request(server)
                    .post('/updatelanguage')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(languageUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail when there is illegal proficiency value in request', async () => {

            let languageUpdate = testUtils.generateRandomLanguage();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            languageUpdate.proficiency = 'rekt';

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                await chai.request(server)
                    .post('/updatelanguage')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(languageUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

    });
});
