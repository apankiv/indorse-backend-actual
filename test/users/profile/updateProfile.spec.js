process.env.NODE_ENV = 'test';

const mongoUserRepo = require("../../../models/services/mongo/mongoRepository")('users');
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings')
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');

chai.use(chaiHttp);

describe('users.updateProfile', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('/POST updateprofile', () => {

        it('should successfully update ethAddress (This test case need to be removed when metamask is implemented)', async () => {

            let userA = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(userA, tokenObj);

            let userAProfileUpdate = {
                name: 'tester',
                bio: 'about myself Im a tester',
                username: 'shouldnotchange',
                ethaddress: '0x8af51b3415e58c55c2c11663cabc7ef8bad518ee',
                social_links : [
                    {'type' : 'facebook' , 'value' : 'www.facebook.com/user'},
                    { 'type': 'github', 'value': 'www.github.com/user' },
                    { 'type': 'twitter', 'value': 'www.TWITTER.com/user' },
                    { 'type': 'linkedIn', 'value': 'www.linkedIn.com/user' },
                    { 'type': 'youtube', 'value': 'www.youtube.com/user' }]
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userAProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: userA.email});

            should.exist(userFromMongo);

            userFromMongo.email.should.equal(userA.email);
            userFromMongo.bio.should.equal(userAProfileUpdate.bio);
            userFromMongo.username.should.equal(userA.username);
            userFromMongo.ethaddress.should.equal(userAProfileUpdate.ethaddress);
            userFromMongo.social_links.length.should.equal(5);
            for (val of userFromMongo.social_links){
                switch (val.type) {
                    case 'facebook':
                        val.value.should.equal('www.facebook.com/user')
                        continue;
                    case 'twitter' :
                        val.value.should.equal('www.twitter.com/user')
                        continue;
                    case 'youtube':
                        val.value.should.equal('www.youtube.com/user')
                        continue;
                }
            }
        });

        it('should fail to update profile when social link is greater than 128 chars', async (done) => {

            let userA = testUtils.generateRandomUser();

            let tokenObj = {};


            let socialProfile = 'www.facebook.com.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
            socialProfile = socialProfile + socialProfile + socialProfile + socialProfile;

            await authenticationWrapper.signupVerifyAuthenticate(userA, tokenObj);

            let userAProfileUpdate = {
                name: 'tester',
                bio: 'about myself Im a tester',
                username: 'shouldnotchange',
                ethaddress: '0x8af51b3415e58c55c2c11663cabc7ef8bad518ee',
                social_links: [
                    { 'type': 'facebook', 'value': socialProfile },
                ]
            };

            try {
                let res = await chai.request(server)
                    .post('/updateprofile')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(userAProfileUpdate);
                done(new Error("This error should not execute"))
            } catch (error) {
                try {
                    error.status.should.equal(400)
                    done();
                } catch (error) {
                    done(error)
                }
            }

        });

        it('should successfully update name and bio', async () => {

            let userA = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(userA, tokenObj);

            let userAProfileUpdate = {
                name: 'tester',
                bio: 'about myself Im a tester',
                username: 'shouldnotchange'
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userAProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: userA.email});

            should.exist(userFromMongo);

            userFromMongo.email.should.equal(userA.email);
            userFromMongo.bio.should.equal(userAProfileUpdate.bio);
            userFromMongo.username.should.equal(userA.username);
        });

        it('should not allow script', async () => {

            let userA = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(userA, tokenObj);

            let userAProfileUpdate = {
                name: 'tester',
                bio: '"><ScRIpt src=//ddd.aa></sCriPt>',
                username: 'shouldnotchange'
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userAProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: userA.email});

            should.exist(userFromMongo);

            userFromMongo.email.should.equal(userA.email);
            userFromMongo.bio.should.not.equal(userAProfileUpdate.bio);
            userFromMongo.username.should.equal(userA.username);
        });

        it('should handle entities', async () => {

            let userA = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(userA, tokenObj);

            let userAProfileUpdate = {
                name: 'tester',
                bio: '&',
                username: 'shouldnotchange'
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userAProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: userA.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(userA.email);
            userFromMongo.bio.should.equal(userAProfileUpdate.bio);
            userFromMongo.username.should.equal(userA.username);
        });


        it('should update upload picture', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            if (uploadsAreEnabled()) {

                let userProfileUpdate = {
                    name: 'tester',
                    bio: 'about myself Im a tester',
                    img_data: 'R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=='
                };

                let res = await chai.request(server)
                    .post('/updateprofile')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(userProfileUpdate);


                res.should.have.status(200);

                let userFromMongo = await mongoUserRepo.findOne({email: user.email});

                should.exist(userFromMongo);
                should.exist(userFromMongo.keyPair);
                should.exist(userFromMongo.keyPair.publicKey);
                should.exist(userFromMongo.keyPair.privateKey);

                userFromMongo.email.should.equal(user.email);
                userFromMongo.name.should.equal(userProfileUpdate.name);
                userFromMongo.email.should.equal(user.email);
                userFromMongo.bio.should.equal(userProfileUpdate.bio);
                userFromMongo.username.should.equal(userProfileUpdate.username);
            }
        })

        it('should update a username if user has none', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await mongoUserRepo.update({email: user.email}, {$set: {username: null}});

            let userProfileUpdate = {
                name: 'tester',
                bio: 'about myself Im a tester',
                username: 'username'
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);

            userFromMongo.email.should.equal(user.email);
            userFromMongo.name.should.equal(userProfileUpdate.name);
            userFromMongo.email.should.equal(user.email);
            userFromMongo.bio.should.equal(userProfileUpdate.bio);
            userFromMongo.username.should.equal(userProfileUpdate.username);
        });

        it('should not update a username if user has one', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userProfileUpdate = {
                name: 'tester',
                bio: 'about myself Im a tester',
                username: 'newusername'
            };

            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userProfileUpdate);

            res.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);
            userFromMongo.name.should.equal(userProfileUpdate.name);
            userFromMongo.email.should.equal(user.email);
            userFromMongo.bio.should.equal(userProfileUpdate.bio);
            userFromMongo.username.should.equal(user.username);
        });

        // See ID-978
        it('should not fail if no bio in request', async (done) => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            try {

                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let userProfileUpdate = {
                    name: 'tester',
                    username: user.username
                };

                let res = await chai.request(server)
                    .post('/updateprofile')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(userProfileUpdate);

                res.should.have.status(200);
                let userFromMongo = await mongoUserRepo.findOne({email: user.email});

                userFromMongo.email.should.equal(user.email);
                userFromMongo.name.should.equal(userProfileUpdate.name);
                userFromMongo.username.should.equal(userProfileUpdate.username);

                done();
            } catch (error) {
                done(error)

            }
        });


        it('should fail if no user name in request', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userProfileUpdate = {
                bio: 'about myself Im a tester',
                username: 'username'
            };

            try {
                await chai.request(server)
                    .post('/updateprofile')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(userProfileUpdate);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

    })
});

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'test_tmp';
}