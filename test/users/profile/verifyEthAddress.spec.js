process.env.NODE_ENV = 'test';

const mongooseUserRepo = require("../../../models/services/mongo/mongoose/mongooseUserRepo");
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings')
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);
const Web3 = new require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));


describe('users.updateProfile', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /users/ethaddress', () => {


        it('should add a new verified ether address if user has none', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0]
            let msg = new Buffer(settings.INDORSE_SIGN_MSG)
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'))

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            res.body.ethaddressVerified.should.equal(true)
            res.body.ethaddress.should.equal(newEthaddress)
            res.body.success.should.equal(true)

            let userFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);

            should.exist(userFromMongo);

            userFromMongo.ethaddress.should.equal(newEthaddress);
            userFromMongo.ethaddressverified.should.equal(true);
        });

        it('should return ether address verification status on /me endpoint', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            res = await chai.request(server)
                .post('/me')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            let profile = res.body.profile;
            profile.ethaddress.should.equal(newEthaddress);
            profile.ethaddressverified.should.equal(true);
        });

        it('should verify ether address for user with unverified address', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            await mongooseUserRepo.update({email: newUser.email}, {$set: {ethaddress: newEthaddress}});

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);

            should.exist(userFromMongo);

            userFromMongo.ethaddress.toLowerCase().should.equal(newEthaddress.toLowerCase());
            userFromMongo.ethaddressverified.should.equal(true);
        });

        it('should verify ether address for user with unverified address with verified flag = false', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            await mongooseUserRepo.update({email: newUser.email}, {$set: {ethaddress: newEthaddress, ethaddressverified: false}})

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);

            should.exist(userFromMongo);

            userFromMongo.ethaddress.should.equal(newEthaddress);
            userFromMongo.ethaddressverified.should.equal(true);
        });

        it('should fail if ether address is already verified', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(400);
            }

        });

        it('should fail if ether address is invalid', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};


            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: '0xfffffffffffffffffffffffffffffffffffffffZ',
                signature: signedData,
            };

            try {
                let res = await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(422);
            }

        });

        it('should fail if ether address is does not match the profile', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let otherEthaddress = web3.eth.accounts[1];
            await mongooseUserRepo.update({email: newUser.email}, {$set: {ethaddress: otherEthaddress}})

            try {
                 await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(400);
            }

        });

        it('should fail if signature does not match address', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let incorrectEthaddress = web3.eth.accounts[1];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(incorrectEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(400);
            }

        });

        it('should fail if signature address is invalid', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData+'Z',
            };

            try {
                let res = await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(422);
            }

        });

        it('should fail if ethaddress is already taken', async () => {

            let newUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};
            let tokenObjExistingUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);
            await authenticationWrapper.signupVerifyAuthenticate(existingUser, tokenObjExistingUser);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            let res = await chai.request(server)
                .post('/users/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObjExistingUser.token)
                .send(updateRequest);

            res.status.should.equal(200);

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if not logged in', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            let newEthaddress = web3.eth.accounts[0]
            let msg = new Buffer(settings.INDORSE_SIGN_MSG)
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'))

            let updateRequest = {
                ethaddress: newEthaddress,
                signature: signedData,
            };

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('should fail if no ethaddress in request', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            let newEthaddress = web3.eth.accounts[0];
            let msg = new Buffer(settings.INDORSE_SIGN_MSG);
            let signedData = web3.eth.sign(newEthaddress, '0x' + msg.toString('hex'));

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send({});

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('should fail if no signature in request', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            try {
                await chai.request(server)
                    .post('/users/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send({});

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(422);
            }
        });
    })
});
