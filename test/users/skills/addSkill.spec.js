process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.addSkill', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });
    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });
    

    describe('POST /users/:id/skills', () => {

        it('it should fail is the skill does not exist', async (done) => {

            let randomUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let skillInDb = testUtils.generateRandomUserSkill();

            try {
                let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name, level: 'intermediate'});

                done(new Error('Expecting to fail'));
            } catch (error) {
                done();
            }
        });

        it('it should fail if user already has a skill', async (done) => {

            let randomUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let skillInDb = testUtils.generateRandomUserSkill();

            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
            await mongooseSkillRepo.insert(skillInDb);

            try {

            await chai.request(server)
                .post('/users/' + createdUser._id + '/skills')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({name: skillInDb.name, level: 'intermediate'});
            } catch (e) {
                done(e);
            }

            try {
                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name, level: 'intermediate'});

                done(new Error('Expecting to fail'));
            } catch (error) {
                done();
            }
        });

        it('it should fail if user already has a skill (providing first request has lowercase, and second request has uppercased skill name)', async (done) => {

            let randomUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let skillInDb = testUtils.generateRandomUserSkill();

            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
            await mongooseSkillRepo.insert(skillInDb);

            try {

                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name.toLowerCase(), level: 'intermediate'});
            } catch (e) {
                done(e);
            }

            try {
                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name.toUpperCase(), level: 'intermediate'});

                done(new Error('Expecting to fail'));
            } catch (error) {
                done();
            }
        });



        it('should add skill', async (done) => {

            let randomUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let skillInDb = testUtils.generateRandomUserSkill();

            try {
                let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
                await mongooseSkillRepo.insert(skillInDb);

                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name, level: 'intermediate'});

                let updatedUser = await mongooseUserRepo.findOne({email: randomUser.email});

                updatedUser.skills.length.should.equal(1);

                done();
            } catch (error) {
                done(error);
            }
        })
    })
});