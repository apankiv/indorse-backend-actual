process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoRepository')('skills');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.adminAddSkills', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /skills', () => {

        it('it should add a new skill', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            let skillInDb = {
                name: 'test',
                category : 'test'
            };


            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            let res = await chai.request(server)
                .post('/skills')
                .set('Authorization', 'Bearer ' + token)
                .send(skillInDb);

            res.should.have.status(200);

            let skillInMongo = await mongoSkillsRepo.findOne(skillInDb)

            skillInMongo.name.should.equal(skillInDb.name.toLowerCase())
        });

        it('it should fail at adding skill that already exists', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            await mongoSkillsRepo.insert({'name': skillInDb.name});

            try {
                await chai.request(server)
                    .post('/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(skillInDb);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(400);
            }
        });


        it('should fail for non-admin user', async () => {

            let normalUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

            try {
                await chai.request(server)
                    .post('/skills')
                    .set('Authorization', 'Bearer ' + token)
                    .send(skillInDb);

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(403);
            }
        })
    })
});