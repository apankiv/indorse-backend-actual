process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoRepository')('skills');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
const ObjectID = require('mongodb').ObjectID;
chai.use(chaiHttp);

describe('skills.adminDeleteSkills', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('DELETE /skills/:id', () => {

        it('it should fail is the skill does not exist', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            let skillInDb = {
                name: 'test',
                category : 'test'
            };


            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            try {
                await chai.request(server)
                    .delete('/skills/5b290a27830442289bcfe74e')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(skillInDb);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should delete a skill in the database', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            let mongoResult = await mongoSkillsRepo.insert(skillInDb);
            let skillId = ObjectID(mongoResult.insertedIds[0].id).toHexString();

            let res = await chai.request(server)
                .delete('/skills/' + skillId)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let skillInMongo = await mongoSkillsRepo.findOne(skillInDb)

            should.not.exist(skillInMongo);
        });


        it('should fail for non-admin user', async () => {

            let normalUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

            let mongoResult = await mongoSkillsRepo.insert(skillInDb);
            let skillId = ObjectID(mongoResult.insertedIds[0].id).toHexString();

            try {
                await chai.request(server)
                    .delete('/skills/' + skillId)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(403);
            }
        })
    })
});