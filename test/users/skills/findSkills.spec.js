process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoRepository')('skills');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.findSkill', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('GET /admin/skills', () => {

        it('should return 0 skills if 0 skills matches in db', async () => {

            let res = await chai.request(server)
                .get('/skills')
                .query({name: 'test'})
                .send();

            res.body.skills.length.should.be.equal(0);
        })

        it('should return 1 skill if 1 skill matches in db', async () => {

            let skillInDb = {
                name: 'test',
                category: 'test'
            };

            await mongoSkillsRepo.insert(skillInDb);

            let res = await chai.request(server)
                .get('/skills')
                .query({name: skillInDb.name})
                .send();

            res.body.skills.length.should.be.equal(1);
            res.body.skills[0].name.should.be.equal(skillInDb.name);
        });

        it('should return 2 skills if 2 skills matches in db', async () => {

            let skillInDbAlpha = {
                name: 'please',
                category: 'hello'
            };
            let skillInDbBeta = {
                name: 'please2',
                category: 'yes'
            };

            await mongoSkillsRepo.insert(skillInDbAlpha);
            await mongoSkillsRepo.insert(skillInDbBeta);

            let res = await chai.request(server)
                .get('/skills')
                .query({name: skillInDbAlpha.name})
                .send();

            res.body.skills.length.should.be.equal(2);
        })

        it('should return skill c++', async () => {

            let skillInDb = {
                name: 'c++',
                category: 'test'
            };

            await mongoSkillsRepo.insert(skillInDb);

            let res = await chai.request(server)
                .get('/skills')
                .query({name: skillInDb.name})
                .send();

            res.body.skills.length.should.be.equal(1);
            res.body.skills[0].name.should.be.equal(skillInDb.name);
        });

        it('should return skill c and other c related skills. and c should be returned in the first indexx of array ', async () => {

            let skill = {
                name: 'c',
                category: 'test'
            };
            let skillA = {
                name: 'c++',
                category: 'test'
            };
            let skillB = {
                name: 'c#',
                category: 'test'
            };
            let skillC = {
                name: 'javascript',
                category: 'test'
            };
            await mongoSkillsRepo.insert(skill);
            await mongoSkillsRepo.insert(skillA);
            await mongoSkillsRepo.insert(skillB);
            await mongoSkillsRepo.insert(skillC);


            let res = await chai.request(server)
                .get('/skills')
                .query({name: skill.name})
                .send();

            res.body.skills.length.should.be.equal(4);
            res.body.skills[0].name.should.be.equal(skill.name);
        });

        it('should not throw an error ', async () => {

            let skill = {
                name: 'artificial integlligence',
                category: 'test'
            };
            let skillA = {
                name: 'artficial',
                category: 'test'
            };
            let skillB = {
                name: 'c#',
                category: 'test'
            };
            let skillC = {
                name: 'javascript',
                category: 'test'
            };
            await mongoSkillsRepo.insert(skill);
            await mongoSkillsRepo.insert(skillA);
            await mongoSkillsRepo.insert(skillB);
            await mongoSkillsRepo.insert(skillC);


            let res = await chai.request(server)
                .get('/skills')
                .query({name: 'art'})
                .send();

        });



    });
});
