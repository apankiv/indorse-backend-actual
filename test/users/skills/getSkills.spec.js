process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoRepository')('skills');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.getSkills', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /admin/skills/:pageNo/:perPage', () => {

        it('it should not return any skills', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let res = await chai.request(server)
                .get('/admin/skills/1/10')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            res.body.skills.length.should.be.equal(0);
        });

        it('it should fetch 1 skill', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await mongoSkillsRepo.insert(skillInDb);

            let res = await chai.request(server)
                .get('/admin/skills/1/10')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            res.body.skills.length.should.be.equal(1);
            res.body.skills[0].name.should.be.equal(skillInDb.name);
        });

        it('it should fetch the first 10 skill', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let i;
            let skillsAddedToDatabase = [];
            for (i = 0; i < 10; i++) {
                let skillInDb = {
                    name: 'test'+i.toString(),
                };
                skillsAddedToDatabase.push(skillInDb);

                await mongoSkillsRepo.insert(skillInDb);
            }

            let res = await chai.request(server)
                .get('/admin/skills/1/10')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            res.body.skills.length.should.be.equal(10);
            res.body.skills[0].name.should.be.equal(skillsAddedToDatabase[0].name);
            res.body.skills[1].name.should.be.equal(skillsAddedToDatabase[1].name);
            res.body.skills[9].name.should.be.equal(skillsAddedToDatabase[9].name);
        });

        it('it should fetch the second 10 skill', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let i;
            let skillsAddedToDatabase = [];
            for (i = 20; i < 40; i++) { // If run from i=0 i<20 this fails as the numbers 1-9 are not prefized with 0 eg 01, 02
                let skillInDb = {
                    name: 'test'+i.toString(),
                };
                skillsAddedToDatabase.push(skillInDb);

                await mongoSkillsRepo.insert(skillInDb);
            }

            let res = await chai.request(server)
                .get('/admin/skills/2/10')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            res.body.skills.length.should.be.equal(10);
            res.body.skills[0].name.should.be.equal(skillsAddedToDatabase[10].name);
            res.body.skills[1].name.should.be.equal(skillsAddedToDatabase[11].name);
            res.body.skills[9].name.should.be.equal(skillsAddedToDatabase[19].name);
        });


        it('it should fetch 2 out of 3 skills based on a query', async () => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let skillInDbA = {
                name: 'test1',
            };
            let skillInDbB = {
                name: 'test11',
            };
            let skillInDbC = {
                name: 'test2',
            };

            await mongoSkillsRepo.insert(skillInDbA);
            await mongoSkillsRepo.insert(skillInDbB);
            await mongoSkillsRepo.insert(skillInDbC);

            let res = await chai.request(server)
                .get('/admin/skills/1/10/')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .query({search: 'test1'})
                .send();

            res.should.have.status(200);

            res.body.skills.length.should.be.equal(2);
            res.body.skills[0].name.should.be.equal(skillInDbA.name);
            res.body.skills[1].name.should.be.equal(skillInDbB.name);
        });


        it('should fail for non-admin user', async () => {

            let normalUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

            try {
                let res = await chai.request(server)
                    .get('/admin/skills/1/10')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(403);
            }
        })
    })
});