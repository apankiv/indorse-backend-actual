const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');


exports.insertAIPSkill = async function insertAIPSkill(skillName){
    await mongooseSkillRepo.insert({name: skillName, category: 'tech', validation: {aip: true}});
}