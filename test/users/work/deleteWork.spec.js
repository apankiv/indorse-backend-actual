process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);

describe('users.deleteWork', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database')
        DB.connect(done)
    })

    after((done) => {
        console.log('dropping database')
        DB.drop((done));
    })

    describe('POST /deletework', () => {
        it('after updating work delete should remove the update', async () => {

            let workUpdate = testUtils.generateRandomWorkEntry();

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            let response = await chai.request(server)
                .post('/me')
                .send({email: user.email})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let profile = response.body.profile;

            let item_key = profile.work[0].item_key;

            await chai.request(server)
                .post('/deletework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({item_key: item_key});

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.work.length.should.equal(0);
        })

        it('should fail if no item key in request', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                await chai.request(server)
                    .post('/deletework')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({test: 'test'});

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(422);
            }
        });
    });
});