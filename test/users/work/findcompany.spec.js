process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const chaiHttp = require('chai-http');
const testUtils = require('./../../testUtils');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.findcompany', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /findcompany', () => {

        it('should return 0 companies if 0 companies matches in db', async () => {
            let res = await chai.request(server)
                .post('/findcompany')
                .send({company_name: 'test'});

            res.body.companies.length.should.be.equal(0);
        });


        it('should return 1 company if 1 company matches in db', async () => {

            let companyIdDB = {
                company_name: 'test'
            };

            await mongoCompanyRepo.insert(companyIdDB);

            let res = await chai.request(server)
                .post('/findcompany')
                .send({company_name: companyIdDB.company_name});

            res.body.companies.length.should.be.equal(1);
            res.body.companies[0].company_name.should.be.equal(companyIdDB.company_name);
        });

        it('should return 2 companies if 2 company matches in db', async () => {

            let companyInDbAlpha = {
                company_name: 'test'
            };

            let companyInDbBeta = {
                company_name: 'test2'
            };

            await mongoCompanyRepo.insert(companyInDbAlpha);
            await mongoCompanyRepo.insert(companyInDbBeta);

            let res = await chai.request(server)
                .post('/findcompany')
                .send({company_name: companyInDbAlpha.company_name});

            res.body.companies.length.should.be.equal(2);
        });

        it('should return 2 companies if 2 company matches in db - case insensitive search', async () => {

            let companyInDbAlpha = {
                company_name: 'test'
            };

            let companyInDbBeta = {
                company_name: 'test2'
            };

            await mongoCompanyRepo.insert(companyInDbAlpha);
            await mongoCompanyRepo.insert(companyInDbBeta);

            let res = await chai.request(server)
                .post('/findcompany')
                .send({company_name: companyInDbAlpha.company_name.toUpperCase()});

            res.body.companies.length.should.be.equal(2);
        });

    })
});
