const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');

class TestHelper {
    static async createValidators(skills, numberOfExtraValidators = 10) {
        const skillVotingQuota = Math.floor(settings.CLAIM_VALIDATOR_COUNT / skills.length);
        let remainingQuota = settings.CLAIM_VALIDATOR_COUNT - (skillVotingQuota * skills.length);
        const validatorCreationPromises = [];
        skills.forEach((skill) => {
            validatorCreationPromises.push(claimCreationWrapper.createValidatorSet(
                skillVotingQuota,
                [skill.name],
            ));
        });
        // add extra validators to make count more than CLAIM_VALIDATOR_COUNT
        // extra validators should not be sucessfull in voting
        remainingQuota += numberOfExtraValidators;
        for (let i = 0; i < remainingQuota; i += 1) {
            validatorCreationPromises.push(claimCreationWrapper.createValidatorSet(
                1,
                [skills[i % skills.length].name],
            ));
        }
        const validatorsResult = await Promise.all(validatorCreationPromises);
        return validatorsResult.reduce((acc, curr) => acc.concat(curr));
    }
}

module.exports = TestHelper;
