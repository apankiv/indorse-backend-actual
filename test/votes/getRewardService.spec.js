process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const getRewardService = require('../../models/votes/getRewardService');
const timestampService = require('../../models/services/common/timestampService');


chai.use(chaiHttp);

describe('votes.getMyVotes', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe.skip('getRewardService unit tests', () => {

        it('it should return the reward value according to the payment logic indicated on ticket ID-1503', async () => {

        let now = timestampService.createTimestamp();
        let SECONDS_IN_HOUR = 3600;
        let claimHourAgo = testUtils.generateRandomClaim();
        let claimTwoHourAgo = testUtils.generateRandomClaim();
        let claimThreeHourAgo = testUtils.generateRandomClaim();
        let claimFiveHourAgo = testUtils.generateRandomClaim();
        let claimFiveHour59minutesAgo = testUtils.generateRandomClaim();
        let claimSixHourAgo = testUtils.generateRandomClaim();
        let claimSevenHourAgo = testUtils.generateRandomClaim();

        claimHourAgo.approved = now - SECONDS_IN_HOUR-20; // approved a little bit more than an hour ago
        claimTwoHourAgo.approved = now - 2*SECONDS_IN_HOUR -20;
        claimThreeHourAgo.approved = now - 3*SECONDS_IN_HOUR-20;
        claimFiveHourAgo.approved = now - 5*SECONDS_IN_HOUR-20;
        claimFiveHour59minutesAgo.approved = now -6*SECONDS_IN_HOUR+15;
        claimSixHourAgo.approved = now - 6*SECONDS_IN_HOUR-20;
        claimSevenHourAgo.approved = now - 7*SECONDS_IN_HOUR-20;

        let claimHourAgoId = await mongooseClaimsRepo.insert(claimHourAgo);
        let claimTwoHourAgoId = await mongooseClaimsRepo.insert(claimTwoHourAgo);
        let claimThreeHourAgoId = await mongooseClaimsRepo.insert(claimThreeHourAgo);
        let claimFiveHourAgoId = await mongooseClaimsRepo.insert(claimFiveHourAgo);
        let claimFiveHour59minutesAgoId = await mongooseClaimsRepo.insert(claimFiveHour59minutesAgo);
        let claimSixHourAgoId = await mongooseClaimsRepo.insert(claimSixHourAgo);
        let claimSevenHourAgoId = await mongooseClaimsRepo.insert(claimSevenHourAgo);

        let voteHourAgo = {
            claim_id: claimHourAgoId
        }
        let voteTwoHourAgo = {
            claim_id: claimTwoHourAgoId
        }
        let voteThreeHourAgo = {
            claim_id: claimThreeHourAgoId
        }
        let voteFiveHourAgo = {
            claim_id: claimFiveHourAgoId
        }
        let voteFiveHour59minutesAgo = {
            claim_id: claimFiveHour59minutesAgoId
        }
        let voteSixHourAgo = {
            claim_id: claimSixHourAgoId
        }
        let voteSevenHourAgo = {
            claim_id: claimSevenHourAgoId
        }
        let rewardForOnehourAgo = await getRewardService.getReward(voteHourAgo);
        let rewardForTwohourAgo = await getRewardService.getReward(voteTwoHourAgo);
        let rewardForThreehourAgo = await getRewardService.getReward(voteThreeHourAgo);
        let rewardForFivehourAgo = await getRewardService.getReward(voteFiveHourAgo);
        let rewardForFiveHour59minutesAgo = await getRewardService.getReward(voteFiveHour59minutesAgo);
        let rewardForSixhourAgo = await getRewardService.getReward(voteSixHourAgo);
        let rewardForSevenhourAgo = await getRewardService.getReward(voteSevenHourAgo);
        rewardForOnehourAgo.should.equal(10);
        rewardForTwohourAgo.should.equal(8);
        rewardForThreehourAgo.should.equal(8);
        rewardForFivehourAgo.should.equal(6);
        rewardForSixhourAgo.should.equal(5);
        rewardForSevenhourAgo.should.equal(5);
        rewardForFiveHour59minutesAgo.should.equal(6);


    });
});
});