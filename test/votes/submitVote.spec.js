process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseAssignmentRepo = require('../../models/services/mongo/mongoose/mongooseAssignmentRepo');
const mongooseJobRoleRepo = require('../../models/services/mongo/mongoose/mongooseJobRoleRepo');
const mongooseClaimEvents = require('../../models/services/mongo/mongoose/mongooseClaimEvents');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');

const testUtils = require('../testUtils');

const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/verifiers/contractSettingInitializer');
const claimTestUtils = require('../services/claims/claimTestUtils');

chai.use(chaiHttp);

describe('votes.submitVote', function test() {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /votes/:vote_id', () => {
        it('it should submit a vote for a claim if user is one of voters', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();

            const tokenObj = {};

            const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            const testedValidator = validators[0];
            await testUtils.wait(1000);

            const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            const vote = votes[0];

            let res = await chai.request(server)
                .post(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send({
                    endorse: true,
                    feedback: {
                        quality: 1,
                        designPatterns: 1,
                        gitFlow: 1,
                        explanation: 'ble',
                        testCoverage: 1,
                        readability: 2,
                        extensibility: 2,
                    },
                });

            const { votingToken } = res.body;
            const updatedVote = await mongooseVoteRepo.findOneById(vote._id);

            updatedVote.endorsed.should.equal(true);
            should.exist(updatedVote.voted_at);
            updatedVote.voting_token.should.equal(votingToken);

            const claimEvents = await mongooseClaimEvents.findAll({ user_id: testedValidator.user_id });
            claimEvents.length.should.equal(1);

            res = await chai.request(server)
                .get(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send();
        });


        it('it should submit a vote for an assignment claim', async () => {
            const adminUser = testUtils.generateRandomUser();
            const randomAss = testUtils.generateRandomAssignment();
            const randomRole = testUtils.generateRandomJobRole();
            const roleid = await mongooseJobRoleRepo.insert(randomRole);
            randomAss.job_role = roleid;

            randomAss.duration = -1;
            const id = await mongooseAssignmentRepo.insert(randomAss);

            const tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);

            let mutationRequest = `
                mutation startAssignment($assignmentID: String!) {
                    startAssignment(assignmentID: $assignmentID) {
                        started_on
                    }
                }
            `;

            let res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({ query: mutationRequest, variables: { assignmentID: id } });

            res.should.have.status(200);

            const skillTag1 = randomRole.skillTags[0];
            const skill = {
                name: 'javascript',
                category: testUtils.generateRandomSkill(),
            };
            skill.tags = [skillTag1];
            skill.validation = {
                aip: true,
            };
            const skillid1 = await mongooseSkillRepo.insert(skill);

            const skillTag3 = testUtils.generateRandomString();
            const skill3 = {
                name: 'jschild',
                category: testUtils.generateRandomSkill(),
            };
            skill3.tags = [skillTag3];
            skill3.parents = [skillid1];
            const skillid3 = await mongooseSkillRepo.insert(skill3);

            const variables = {
                assignmentID: id,
                form: {
                    proof: 'https://ble.ble',
                    skills: [{
                        skillTag: skillTag3,
                        skillId: skillid3,
                    }],
                },
            };

            mutationRequest = `
                mutation FinishAssignment($assignmentID: String!, $form :FinishAssignmentForm!) {
                    finishAssignment(assignmentID: $assignmentID, form: $form) {
                        owner_id
                    }
                }
            `;

            res = await chai.request(server)
                .post('/graphql')
                .set('Authorization', `Bearer ${tokenObj.token}`)
                .send({ query: mutationRequest, variables });

            const claim = await mongooseClaimsRepo.findOne({ type: 'ASSIGNMENT' });
            // Claim Approve
            await claimTestUtils.approveAndRelease(claim._id);

            res.should.have.status(200);

            const testedValidator = validators[0];
            await testUtils.wait(1000);

            const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            const vote = votes[0];

            res = await chai.request(server)
                .post(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send({
                    endorse: true,
                    skillid: skillid3,
                    feedback: {
                        quality: 1,
                        designPatterns: 1,
                        gitFlow: 1,
                        completion: 1,
                        explanation: 'ble',
                        testCoverage: 1,
                        readability: 2,
                        extensibility: 2,
                    },
                });

            const { votingToken } = res.body;

            const updatedVote = await mongooseVoteRepo.findOneById(vote._id);

            updatedVote.endorsed.should.equal(true);
            should.exist(updatedVote.voted_at);
            updatedVote.voting_token.should.equal(votingToken);

            res = await chai.request(server)
                .get(`/votes/${vote._id.toString()}`)
                .set('Authorization', `Bearer ${testedValidator.tokenObj.token}`)
                .send();
        });

        it('it should fail to submit vote if user is not the voter', async () => {
            const createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            const user = testUtils.generateRandomUser();
            const randomUser = testUtils.generateRandomUser();

            const tokenObj = {};
            const tokenObjRandomUser = {};
            await authenticationWrapper.signupVerifyAuthenticate(randomUser, tokenObjRandomUser);

            const validators = await claimCreationWrapper.createValidatorSet(settings.MAX_CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            const testedValidator = validators[0];
            const votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            const vote = votes[0];

            try {
                await chai.request(server)
                    .post(`/votes/${vote._id.toString()}`)
                    .set('Authorization', `Bearer ${tokenObjRandomUser.token}`)
                    .send({
                        endorse: true,
                        feedback: {
                            quality: 1,
                            designPatterns: 1,
                            gitFlow: 1,
                            explanation: 'ble',
                            testCoverage: 1,
                            readability: 2,
                            extensibility: 2,
                        },
                    });
                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    });
});
