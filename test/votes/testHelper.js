const chai = require('chai');
const server = require('../../server');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');

class TestHelper {
    static async addVotesSerially(claim, validators, skills) {
        const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id);
        const allVoteResults = [];
        let oneVoteResult = null;
        for (let i = 0; i < validators.length; i += 1) {
            oneVoteResult = await TestHelper.addOneVote( // eslint-disable-line no-await-in-loop
                claim,
                validators[i],
                skills[i % skills.length],
                votingRound,
            );
            allVoteResults.push(oneVoteResult);
        }
        return allVoteResults;
    }

    static async addVotes(claim, validators, skills) {
        const votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id);
        const addVotePromises = [];
        for (let i = 0; i < validators.length; i += 1) {
            addVotePromises.push(TestHelper.addOneVote(
                claim,
                validators[i],
                skills[i % skills.length],
                votingRound,
            ));
        }
        return Promise.all(addVotePromises);
    }

    static async addOneVote(claim, validator, skill, votingRound) {
        const vote = await mongooseVoteRepo.findOne({
            voting_round_id: votingRound._id.toString(),
            voter_id: validator.user_id,
        });
        return chai
            .request(server)
            .post(`/votes/${vote._id.toString()}`)
            .set('Authorization', `Bearer ${validator.tokenObj.token}`)
            .send({
                endorse: true,
                feedback: {
                    quality: 2,
                    designPatterns: 2,
                    completion: 2,
                    testCoverage: 2,
                    readability: 2,
                    extensibility: 2,
                    explanation: 'random explanation',
                    yearsOfExperience :'0-1'
                },
                skillid: skill._id,
            });
    }
}

module.exports = TestHelper;
