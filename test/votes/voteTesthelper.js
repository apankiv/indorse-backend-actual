const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const testUtils = require('../testUtils');
module.exports.updateVoteWithFeedbackAndSC = async function updateVoteWithFeedbackAndSC(voteId){
    await mongooseVoteRepo.update({_id: voteId}, { sc_vote_exists: true , endorse: true,
        feedback: {
            quality: 1,
            designPatterns: 1,
            gitFlow: 1,
            explanation: 'ble',
            testCoverage: 1,
            readability: 2,
            extensibility: 2,
        }});
}


module.exports.insertVoteWithVoterIdAndClaimId = async function updateVoteWithFeedbackAndSC(voterId, claimId) {
    await mongooseVoteRepo.insert({
        reward : 1,
        indorsed : testUtils.generateRandomBoolean(),
        voter_id : voterId,
        voting_round_id : "x",
        claim_id : claimId,
        sc_vote_exists : true,
        voted_at : 1
    })
}
